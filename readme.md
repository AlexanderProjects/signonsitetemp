# Introduction

This documentation provides a guide of how the project is setup and how we publish our builds in play store.

# Architecture

This project follows clean architecture approach which is compose of three main layouts

- Data(External dependencies)
- Domain (business logic, use cases)
- UI (External interfaces)

![Clean Architecture diagram](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

This diagram explains how the project is design. However we have exceptions for notification where we are passing the contexts to an external dependency.

Note: This project has legacy code that is not part of this architecture because it wasn't conceive initially. However we have plans to refactor these components in the future and as part of this refactoring would be including this migration.

# Project setup

- This android app only runs using Android Studio version 4.0 above.
    
- Gradle version com.android.tools.build:gradle:4.2.2
    
- Min sdk version:21
    
- compileSdkVersion 30
    
- build tools version 30.0.3
    
- Also we have created customs build types that are configured in build.gradle which are release, betaRelease, staging and debug.
    

# Important dependencies

- Dagger - documentation [link](https://developer.android.com/training/dependency-injection/dagger-android#:~:text=In%20Android%2C%20you%20usually%20create,context%20available%20in%20the%20graph. "dagger documentation")
- RxJava/RxKotlin/ - documentation [link](https://github.com/ReactiveX/RxAndroid "Rxjava Android")
- Retrofit - documentation [link](https://square.github.io/retrofit/ "retrofit")
- RealmDB - documentation [link](https://realm.io/ "Realm")

# UI test

- This project has ui testing only for passport. However they are not reliable due to depends on the device that are you running the test. So it is necessary do some adjustments in the code in order pass those tests.

# CI/CD

- App center build - branch develop - [![Build status](https://build.appcenter.ms/v0.1/397e7a2c-6e51-4aea-8ec2-0cafbfc59d45/branches/develop/badge)](https://appcenter.ms)
- App center build - branch master - [![Build status](https://build.appcenter.ms/v0.1/397e7a2c-6e51-4aea-8ec2-0cafbfc59d45/branches/master/badge)](https://appcenter.ms)
- Circle CI build - branch master - [![CircleCI](https://circleci.com/bb/signonsite/signonsite-android/tree/master.svg?style=svg)](https://circleci.com/bb/signonsite/signonsite-android/tree/master)

# Deployment in Play Store

This procedure is manual at the moment however SOS team is planing to do this process automatically, so that this manual procedure could change in the near future.

### Deployment process

- Create a signed [bundle](https://developer.android.com/guide/app-bundle "bundle") using Android Studio and the keys stored in the repository
- Create an prod APK and check that app is working in production before release a new app version

# Documentation

- [Manual testing documentation](https://signonsite.atlassian.net/wiki/spaces/SIGNONSITE/pages/322011148/SignOnSite+Mobile+App+Testing "manual testing")