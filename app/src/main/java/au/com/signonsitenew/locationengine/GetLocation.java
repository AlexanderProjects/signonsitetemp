package au.com.signonsitenew.locationengine;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 *  Wrapper for this asynchronous shitfit that makes a blocking
 *  one shot location getter. Not thread safe.
 *
 *  Please just imagine this class has a nice little method called
 *  getLocation that returns a location or null on failure.
 */
public class GetLocation {
    private Location mLocation = null;
    private Context mContext;
    final private String LOG = "LocEng-" + GetLocation.class.getSimpleName();

    public GetLocation(Context context) {
        mContext = context;
    }

    public Location getLocation() {
        Location lastLocation = mLocation;
        mLocation = null;
        Thread thread = new GetLocationThread(mContext, this);
        thread.start();

        try {
            thread.join(5000);
        } catch (Exception e) {
            SLog.d(LOG, "Thread join failed: " + e.toString());
        }

        Location gotLocation = mLocation;
        if(gotLocation == null) {
            return lastLocation;
        } else {
            SLog.d(LOG, "Got one shot location: " + gotLocation.toString());
            return gotLocation;
        }
    }

    class GetLocationThread extends Thread implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private Context mContext;
        private GoogleApiClient mGoogleApiClient;
        private GetLocation mParent;
        private Looper mLooper;

        public GetLocationThread(Context context, GetLocation parent) {
            mParent = parent;
            mContext = context;
        }

        @Override
        public void run (){
            Looper.prepare();
            mLooper = Looper.myLooper();
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .setHandler(new Handler(mLooper))
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
            Looper.loop();
        }

        @Override
        public void onConnected(Bundle connectionHint) {
            SLog.d(LOG, "Connection success");
            try {
                mParent.mLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
            }
            catch (SecurityException e) {
                SLog.d(LOG, "User has either disabled app access to location or turned off location on their phone");
                DiagnosticsManager.logEvent(mContext,
                        DiagnosticLog.Tag.LOC_NO_ACCESS,
                        null,
                        null);
            }
            mLooper.quit();
        }

        @Override
        public void onConnectionFailed(ConnectionResult result) {
            SLog.d(LOG, "Connection failed");
            mLooper.quit();
        }

        @Override
        public void onConnectionSuspended(int n) {
            mLooper.quit();
        }
    }
}
