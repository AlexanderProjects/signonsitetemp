package au.com.signonsitenew.locationengine;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import java.util.Collections;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;


/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */


interface DisposableLocationObserver {
    void disposeLocation();
}
public class LocationFilter extends LocationCallback implements DisposableLocationObserver {
    private static final String LOG = "LocEng-" + LocationFilter.class.getSimpleName();
    private static final double ACCURACY_THRESHOLD = 500.0;

    protected Context mContext;
    private PublishSubject<Location> mLocationSubject;
    private LocationEngineBridge mBridge;
    private LocationFilterDelegate mDelegate;
    private Disposable observerLocationDisposable;

    public LocationFilter(Context context, LocationEngineBridge locationEngineBridge, LocationFilterDelegate delegate) {
        mContext = context;
        mBridge = locationEngineBridge;
        mDelegate = delegate;
        mLocationSubject = PublishSubject.create();

        // Filter out locations that are deemed not accurate enough

        observerLocationDisposable = mLocationSubject
                .filter(location -> {
                    boolean accurate = location.getAccuracy() < ACCURACY_THRESHOLD;
                    if (accurate) {
                        SLog.d(LOG, "Keeping location: " + location.getAccuracy());
                    }
                    else {
                        SLog.d(LOG, "Location inaccuracy too high, dropping: " + location.getAccuracy());
                    }
                    return location.getAccuracy()< ACCURACY_THRESHOLD;
                })
                // Map the location into TimeStampedRegionTuple Objects
                .map(location -> new TimeStampedRegionTuple(location, locationToRegions(location), System.currentTimeMillis()))
                // Convert locations into TimeStampedRegionTuples - to create objects we can track how long the user is inside of
                .scan(new RegionMaintainedTimeTracker(),(tracker, event) -> {
                    long theTime = System.currentTimeMillis();
                    Hashtable<RegionMarker, Long> next = new Hashtable<>();
                    for (RegionMarker region : event.mRegions) {
                        Long timeInRegion = tracker.mTimers.get(region);
                        if (timeInRegion == null) {
                            next.put(region, theTime);
                        }
                        else {
                            SLog.d(LOG, "Have spent: " + ((theTime - timeInRegion) / 1000) +
                                    " seconds in: " + region.mRegionId);
                            DiagnosticsManager.logEvent(mContext,
                                    DiagnosticLog.Tag.LOC_POLLING,
                                    tracker.mLocation,
                                    String.valueOf(region.mRegionId));

                            next.put(region, timeInRegion);
                        }
                    }
                    return new RegionMaintainedTimeTracker(event.mLocation, next);
                })
                // Add regions that the user has been inside of for 60 seconds in a row to a new HashSet
                .map(tracker -> {
                    HashSet<RegionMarker> regions = new HashSet<>();
                    long theTime = System.currentTimeMillis();
                    for (RegionMarker region : Collections.list(tracker.mTimers.keys())) {
                        long timeSpent = tracker.mTimers.get(region);
                        double seconds = (theTime - timeSpent) / 1000.0;
                        if (seconds >= 60.0) {
                            regions.add(region);
                        }
                    }
                    return new LocationAndRegions(tracker.mLocation, regions);
                })
                // Only return the list of regions that the user has been inside of for 60 seconds straight if it is not empty
                .filter(locationAndRegions -> !locationAndRegions.mRegions.isEmpty())
                // Pick the site the user is inside. If they are outside, pick null.
                .map(locationAndRegions -> {
                    Set<RegionMarker> regions = locationAndRegions.mRegions;
                    Integer site = mBridge.getCurrentSite();
                    Location location = locationAndRegions.mLocation;

                    if (site != null) {
                        // pick the site if we are still in it
                        RegionMarker region = new RegionMarker(site);
                        if (regions.contains(region)) {
                            return new LocationAndRegion(location, region);
                        }
                    }

                    // either we aren't signed on, or we are outside the current site
                    // try pick a site
                    RegionMarker outsideCurrentSite = new RegionMarker();
                    for (RegionMarker region : regions) {
                        if (!region.equals(outsideCurrentSite)) {
                            // Potentially could pick closest instead?
                            return new LocationAndRegion(location, region);
                        }
                    }

                    if (regions.contains(outsideCurrentSite)) {
                        return new LocationAndRegion(location, outsideCurrentSite);
                    }

                    // THIS should NEVER happen, it should create an exception
                    // maybe just throw an exception
                    SLog.wtf(LOG, "WTF");
                    return null;
                })
                // Hold onto the region ID, until it changes
                .distinctUntilChanged((locationAndRegion) -> locationAndRegion.mRegionMarker.getRegionId())
                // We have an event we should react to. Determine whether to sign off or on.
                .subscribe(locationAndRegion -> {
                    SLog.d(LOG, "BEEN IN REGION MORE THAN 60 SECONDS!!! " + locationAndRegion.mRegionMarker.getRegionId());
                    Location location = locationAndRegion.mLocation;
                    Integer regionCurrentlyWithin = locationAndRegion.mRegionMarker.getRegionId();

                    if (regionCurrentlyWithin == null) {
                        mDelegate.locationFilterShouldSignOff(location);
                    } else {
                        mDelegate.locationFilterShouldSignOn(regionCurrentlyWithin, location);
                    }
                });

    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        if (locationResult == null) {
            return;
        }
        for (Location location : locationResult.getLocations()) {
            Log.i(LOG, "Holy fuck it's a location(s)!! " + locationResult.getLocations().toString()
                    + ", on thread: " + Thread.currentThread().getName());

            mLocationSubject.onNext(location);
        }
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability) {
        Log.i(LOG, "Locations are available");
    }

    @Override
    public void disposeLocation() {
        observerLocationDisposable.dispose();
    }

    private class RegionMarker {
        private Integer mRegionId = null;

        public RegionMarker(long regionId) {
            Long toTransform = regionId;
            mRegionId = toTransform.intValue();
        }
        public RegionMarker() {
            mRegionId = null;
        }

        @Override
        public boolean equals(Object object) {
            if(object == null) {
                return false;
            }
            if(!(object instanceof RegionMarker)) {
                return false;
            }
            RegionMarker marker = (RegionMarker) object;

            if (mRegionId != null) {
                return marker.getRegionId() != null && mRegionId.intValue() == marker.getRegionId().intValue();
            } else {
                return marker.getRegionId() == null;
            }
        }

        @Override
        public int hashCode() {
            if (mRegionId == null) {
                return -1;
            } else {
                return mRegionId.hashCode();
            }
        }

        /*
            return may be null, in which case it is an "outsideMarker"
         */
        public Integer getRegionId() {
            return mRegionId;
        }
    }

    private class TimeStampedRegionTuple {
        private Location mLocation;
        private Set<RegionMarker> mRegions;
        long mTimeReceived;

        public TimeStampedRegionTuple(Location location, Set<RegionMarker> regions, long timeReceived) {
            mLocation = location;
            mRegions = regions;
            mTimeReceived = timeReceived;
        }
    }

    private class RegionMaintainedTimeTracker {
        Location mLocation;
        Dictionary<RegionMarker, Long> mTimers;

        public RegionMaintainedTimeTracker() {
            mLocation = null;
            mTimers = new Hashtable<>();
        }

        public RegionMaintainedTimeTracker(Location location, Dictionary<RegionMarker,Long> timers) {
            mLocation = location;
            mTimers = timers;
        }
    }

    private class LocationAndRegions {
        Location mLocation;
        Set<RegionMarker> mRegions;

        public LocationAndRegions(Location location, Set<RegionMarker> regions) {
            mLocation = location;
            mRegions = regions;
        }
    }

    private class LocationAndRegion {
        Location mLocation;
        RegionMarker mRegionMarker;

        public LocationAndRegion(Location location, RegionMarker regionMarker) {
            mLocation = location;
            mRegionMarker = regionMarker;
        }
    }

    private Set<RegionMarker> locationToRegions(Location location) {
        Region[] regions = mBridge.getRegions();

        Set<RegionMarker> set = new HashSet<>();

        Integer currentSite = mBridge.getCurrentSite();
        Boolean stillInCurrentSite = false;

        for(Region region : regions) {
            SLog.i(LOG, region.name + ", " + String.valueOf(region.id));

            if(region.sortOfIntersects(location,currentSite)) {
                if(currentSite != null && currentSite == region.id) {
                    stillInCurrentSite = true;
                }
                set.add(new RegionMarker(region.id));
            }
        }

        if(currentSite != null && !stillInCurrentSite) {
            SLog.d(LOG, "Inserting outside current site marker");
            set.add(new RegionMarker());
        }
        else {
            SLog.d(LOG, "Not inserting outside current site marker");
        }

        return set;
    }

    public interface LocationFilterDelegate {
        void locationFilterShouldSignOn(int region, Location location);
        void locationFilterShouldSignOff(Location location);
    }
}