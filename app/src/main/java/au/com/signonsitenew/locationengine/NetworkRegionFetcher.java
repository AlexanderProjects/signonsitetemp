package au.com.signonsitenew.locationengine;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */

public class NetworkRegionFetcher {
    private static final String LOG = "LocEng-" + NetworkRegionFetcher.class.getSimpleName();
    protected Context mContext;

    public NetworkRegionFetcher(Context context) {
        mContext = context;
    }

    // blocking network call wrapper
    // TODO add parameters
    public Region[] getRegions(Location location) {
        ArrayList<Region> regions = new ArrayList<>();
        try {
            SOSAPI api = new SOSAPI(mContext);
            String regionsResult = api.getInductedRegionsBlocking(location);
            SLog.i(LOG, "inducted region results: " + regionsResult);
            JSONObject json = new JSONObject(regionsResult);
            JSONArray sites = json.getJSONArray("sites");

            int length = sites.length();
            Log.i(LOG, "number of regions: " + length);
            for (int i=0; i < length; ++i) {
                JSONObject site = sites.getJSONObject(i);

                Location loc = new Location("");
                loc.setLatitude(site.getDouble("region_lat"));
                loc.setLongitude(site.getDouble("region_long"));
                Region region = new Region();
                region.id = site.getInt("id");
                region.address = site.getString("site_address");
                region.center = loc;
                region.radius = site.getDouble("region_radius");
                region.name = site.getString("name");
                region.manager = site.getString("manager_name");
                region.points = site.getString("points");

                regions.add(region);
            }

        } catch (Exception e) {
            SLog.e(LOG, "Couldn't get Regions!!!!!! " + e.toString());
        }

        return regions.toArray(new Region[regions.size()]);
    }
}


