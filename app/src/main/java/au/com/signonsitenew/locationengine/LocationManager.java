package au.com.signonsitenew.locationengine;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import java.util.concurrent.Semaphore;
import au.com.signonsitenew.jobscheduler.DeviceInfoJobService;
import au.com.signonsitenew.jobscheduler.DiagnosticLogFlushJobService;
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService;
import au.com.signonsitenew.jobscheduler.SOSBootReceiver;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Initialises all location stuff. Allows service to be started and stopped.
 *
 *   Requires manifest stuff:
 *       RegionFetcherService
 *       LocationService
 *       Location permissions
 *       Intent on boot permissions - see below
 *       Declare boot intent receiver - http://developer.android.com/training/scheduling/alarms.html
 *
 *   Here's what happens typically:
 *       Alarm is set up to trigger RegionMarker fetching service
 *       Region fetching service fetches Regions from server, sets up geofence using geofence manager
 *       Geofence triggers location service, passes results through location filter
 *       Location filter signs on or off appropriately
 *       When this happens, sends out a local notification
 *       If the main activity is alive at this time, the interface is updated
 *
 *   To stop the services:
 *       Disable boot intent reception
 *       Delete the alarm
 *       Delete the geofences
 *       Kill the location service (if it is alive)
 *
 *   To start the services:
 *       Enable boot intent reception
 *       Start RegionMarker RegionFetcher alarm
 *
 *   Components and their responsibilities:
 *       SoSBootReceiver - gets notified when device is started up and sets alarm for periodic RegionMarker fetching
 *       GetLocation - gets last location from google api, used for RegionMarker fetching call
 *       RegionFetcherManager - configures the environment to call RegionFetcherService periodically (currently using alarms)
 *       RegionFetcherService - fetches Regions from server, passes these Regions to geofence manager
 *       NetworkRegionFetcher - just a wrapper for blocking calls to region_list.php, used by RegionFetcherService
 *       GeofenceManager - adds or removes Regions from geofence monitoring. These RegionMarker events trigger LocationService
 *       LocationService - when a geofence intent is received, runs for 5 minutes on high power gps mode passing all locations to LocationFilter
 *       LocationFilter - interprets all gps locations and decides when to sign on/off. Implements core algorithm. Delegates sign on/off calls to LocationEngineBridge
 *       LocationEngineBridge - handles all data sharing between our 3 services. Also handles sign on/off: makes network calls and notifies main activity if it is up
 *       NetworkSignOnOff - does the network calls for signing on and off
 *       LocationManager - this class. Provides methods for bringing up and tearing down the system.
 *
 *   Potential (not implemented components):
 *       LocationServicesToggler - Listens (how?) for auto sign on change, emergency change, logged on/off state change. Toggles location services accordingly.
 */

public class LocationManager {
    private static final String LOG = "LocEng-" + LocationManager.class.getSimpleName();
    private Context mContext;
    private GeofenceManager mGeofenceManager;
    private LocationEngineBridge mBridge;
    private static boolean running = false;
    private static Semaphore mutex = new Semaphore(1);

    public LocationManager(Context context, GeofenceManager geofenceManager, LocationEngineBridge locationEngineBridge) {
        SLog.d(LOG, "Building location manager!");
        mContext = context;
        mGeofenceManager = geofenceManager;
        mBridge = locationEngineBridge;
    }

    public void refresh() {
        SLog.d(LOG, "Got refresh");
        try {
            // running is a static variable. use mutex to protect from shared access
            mutex.acquire();
        } catch (Exception e) {
            SLog.e(LOG, "Couldn't acquire mutex");
            return;
        }
        boolean should = mBridge.shouldAct();
        if(should != running) {
            running = should;
            if(running) {
                SLog.d(LOG, "starting services");
                startServices();
            }
            else {
                SLog.d(LOG, "stopping services");
                stopServices();
            }
        }
        mutex.release();
    }

    public void startServices() {
        // enable alarm on boot
        ComponentName receiver = new ComponentName(mContext, SOSBootReceiver.class);
        PackageManager pm = mContext.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        // enable alarms
        RegionFetcherJobService.schedule(mContext);
        DiagnosticLogFlushJobService.schedule(mContext);
        DeviceInfoJobService.schedule(mContext);
        String s = "";
    }

    /*
        This wont stop LocationService, but will stop its worker thread.
        It shouldn't ever do anything then unless it gets an intent.
     */
    public void stopServices() {
        // remove boot notification
        ComponentName receiver = new ComponentName(mContext, SOSBootReceiver.class);
        PackageManager pm = mContext.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        // Stop fetching regions in background
        SLog.d(LOG, "cancelling the region fetcher job service");
        RegionFetcherJobService.cancel(mContext);

        // remove geofences
        new Thread() {
            @Override
            public void run() {
                SLog.d(LOG, "calling geofence stop");
                if(mGeofenceManager.stop()) {
                    SLog.d(LOG, "removed geofences");
                } else {
                    SLog.e(LOG, "FAILED TO REMOVE GEOFENCES");
                }
            }
        }.start();

        // We leave the regions stored in realm, as the device users this information when the user is signed on

        // stop location service
        SLog.d(LOG, "stopping location service");
        Intent stop = new Intent(mContext, LocationService.class);
        stop.setAction(LocationService.doStop);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mContext.startForegroundService(stop);
        }
        else {
            mContext.startService(stop);
        }
        SLog.d(LOG, "stopped services");
    }

    public void monitorLocationForTwoMinutes() {
        if(!mBridge.shouldAct()) {
            SLog.d(LOG, "Monitor location for two minutes called, but location manager not active. Doing nothing.");
            return;
        }
        SLog.d(LOG, "Making intent to start two minute monitoring.");
        Intent start = new Intent(mContext, LocationService.class);
        start.setAction(LocationService.doTwoMinutes);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mContext.startForegroundService(start);
        }
        else {
            mContext.startService(start);
        }

    }
}
