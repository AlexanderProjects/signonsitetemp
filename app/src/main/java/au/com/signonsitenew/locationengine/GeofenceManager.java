package au.com.signonsitenew.locationengine;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import javax.inject.Inject;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.Synchroniser;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */

public class GeofenceManager {
    private static final String LOG = "LocEng-" + GeofenceManager.class.getSimpleName();
    private Intent mIntent = null;
    private PendingIntent mPendingIntent = null;
    private Context mContext = null;
    private GeofenceAdder geofenceAdder;

    @Inject
    public GeofenceManager(Context context,GeofenceAdder geofenceAdder) {
        SLog.d(LOG, "Constructing......");
        mContext = context;
        this.geofenceAdder =geofenceAdder;
    }

    public boolean stop() {
        return new GeofenceDeleter(mContext, getPendingIntent()).sync();
    }

    public Boolean setRegions(Region[] regions, boolean shouldTrigger) {
        new GeofenceDeleter(mContext, getPendingIntent()).sync();

        return geofenceAdder.setParameters(getPendingIntent(), regions, shouldTrigger).sync();
    }

    public Boolean setCurrentRegion(Region region) {
        Region[] regions = {region};
        return geofenceAdder.setParameters(getPendingIntent(), regions, false).sync();
    }
    public Boolean unsetCurrentRegion(Region region) {
        Region[] regions = {region};
        return geofenceAdder.setParameters(getPendingIntent(), regions, true).sync();
    }


    private Intent getIntent() {
        if(mIntent == null) {
            mIntent = new Intent(mContext, LocationService.class);
        }
        return mIntent;
    }

    private PendingIntent getPendingIntent() {
        if(mPendingIntent == null) {
            mPendingIntent = PendingIntent.getService(mContext, 0, getIntent(),
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return mPendingIntent;
    }
}

/*
    Synchronises geofence deletion. new GeofenceDeleter().sync();
 */
class GeofenceDeleter extends Synchroniser<Boolean> {
    private Context mContext;
    private PendingIntent mIntent;
    private GeofencingClient mGeofencingClient;
    private final String TAG = "LocEng-" + GeofenceDeleter.class.getSimpleName();

    public GeofenceDeleter(Context context, PendingIntent intent){
        mContext = context;
        mIntent = intent;
        mGeofencingClient = LocationServices.getGeofencingClient(mContext);
    }

    @Override
    public void work() {
        SLog.d(TAG, "WORK STARTING");
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .build();
        SLog.d(TAG, "blocking connect: " + Thread.currentThread().getName());
        ConnectionResult result = mGoogleApiClient.blockingConnect();
        SLog.d(TAG, "blocking connect done");

        if(!result.isSuccess()) {
            setResult(false);
            return;
        }

        SLog.d(TAG, "calling remove geofences");

        mGeofencingClient.removeGeofences(mIntent)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        SLog.d(TAG, "Deleted regions / geofences!");
                        setResult(true);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        SLog.d(TAG, "Failed to remove regions / geofences with exception :" + e.toString());
                        setResult(false);
                    }
                });

    }
}

