package au.com.signonsitenew.locationengine;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import javax.inject.Inject;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import dagger.android.AndroidInjection;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * This service waits for intents.
 *
 * When it gets one, if it is already running, it ignores the new intent.
 *
 * To run, it starts a thread that polls GPS constantly. It kills that thread after 5 minutes.
 *
 * The actual location filtering logic is in LocationFilter.
 *
 * The thread it spawns starts a message loop that:
 *      connects to google api
 *      on connect, checks settings
 *      if settings are correct, starts location monitor
 *      LocationFilter gets these locations
 *
 * When asked to stop, it just halts its message loop and returns (this kills the man)
 */
public class LocationService extends Service {
    private static final String LOG = "LocEng-" + LocationService.class.getSimpleName();

    public final Semaphore mLocationThreadAlive = new Semaphore(1);
    public final Semaphore mLocationThreadCleanup = new Semaphore(1);
    public LocationThread mLocationThread = null;
    private Timer mStopTimer = null;
    private Semaphore mStopTimerMutex = new Semaphore(1);
    private static final long FIVE_MINUTES_MILLIS = Constants.FIVE_MINS_MILLIS;
    private static final long TWO_MINUTES_MILLIS = Constants.TWO_MINS_MILLIS;
    public static final String doTwoMinutes = "SOSLocationTwoMinutes";
    public static final String doStop = "SOSLocationStop";

    @Inject
    SignOnStatusUseCaseImpl signOnStatusUseCaseImpl;

    @Inject
    LocationEngineBridge locationEngineBridge;


    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
        // Start Foreground Notification
        showForegroundNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SLog.d(LOG, "onStartCommand");

        if(intent == null) {
            SLog.d(LOG, "called with null intent");
            stopSelf();
            return START_STICKY;
        }

        String action = intent.getAction();

        if(action != null) {
            if (action.equals(doTwoMinutes)) {
                SLog.d(LOG, "Running location services for two minutes!");
                tryStartLocationThread(TWO_MINUTES_MILLIS);
                return START_STICKY;
            }
            if (action.equals(doStop)) {
                SLog.d(LOG, "Stopping location services!");
                cleanupLocationThread();
                return START_STICKY;
            }
        }

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            SLog.d(LOG, "stopping location service! got non-geofence intent");
            cleanupLocationThread();
            return START_STICKY;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition != Geofence.GEOFENCE_TRANSITION_ENTER &&
                geofenceTransition != Geofence.GEOFENCE_TRANSITION_EXIT) {

            SLog.d(LOG, "stopping location service! got non-(entry/exit) intent");
            cleanupLocationThread();
            return START_STICKY;
        }

        // Geofence transition was a type of interest, ensure local state on device is synchronised with server
        if(signOnStatusUseCaseImpl != null)
            signOnStatusUseCaseImpl.getUserStatusAsync(aBoolean ->{
                SLog.d(LocationService.class.getName(), "SignOn Status: " + aBoolean);
                return null;
            });

        SLog.i(LOG, String.valueOf(geofencingEvent.getTriggeringGeofences().size()) + " fences triggered");
        for(Geofence fence : geofencingEvent.getTriggeringGeofences()) {
            SLog.i(LOG, "Fence causing trigger: " + fence.toString());
            if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                String message = "Geofence enter, RegionMarker: " + fence.getRequestId() + ", at: "
                        + geofencingEvent.getTriggeringLocation();
                SLog.d(LOG, message);

                // Check if we are already polling for an exit event
                boolean entranceAfterExit = FenceTracker.s
                        .entranceForExitedRegion(fence.getRequestId(), geofencingEvent.getGeofenceTransition());
                Integer site = locationEngineBridge.getCurrentSite();
                if (site != null && entranceAfterExit) {
                    SLog.d(LOG, "Stopping location service! We entered an event during polling soon after exiting");
                    cleanupLocationThread();
                    return START_STICKY;
                }

                FenceTracker.s.didEnter(fence.getRequestId());

                // Log the entrance event
                Log.i(LOG, "request ID: " + String.valueOf(fence.getRequestId()));
                JSONObject jsonData = new JSONObject();
                try {
                    jsonData.put("email", new SessionManager(this).getCurrentUser().get(Constants.USER_EMAIL));
                    jsonData.put("fenceId", fence.getRequestId());
                }
                catch (JSONException e) {
                    SLog.e(LOG, "JSON Exception Occurred: " + e.getMessage());
                }
                DiagnosticsManager.logEvent(this,
                        DiagnosticLog.Tag.LOC_ENTRY,
                        geofencingEvent.getTriggeringLocation(),
                        jsonData.toString());

                NotificationUtil.createRegionNotification(getApplicationContext(), message);
            }
            else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                String message = "Geofence exit, RegionMarker: " + fence.getRequestId() + ", at: "
                        + geofencingEvent.getTriggeringLocation();
                SLog.d(LOG, message);
                FenceTracker.s.didExit(fence.getRequestId());

                // Log the exit event
                JSONObject jsonData = new JSONObject();
                try {
                    jsonData.put("email", new SessionManager(this).getCurrentUser().get(Constants.USER_EMAIL));
                    jsonData.put("fenceId", fence.getRequestId());
                    jsonData.put("location", geofencingEvent.getTriggeringLocation().toString());
                }
                catch (JSONException e) {
                    SLog.e(LOG, "JSON Exception Occurred: " + e.getMessage());
                }
                DiagnosticsManager.logEvent(this,
                        DiagnosticLog.Tag.LOC_EXIT,
                        geofencingEvent.getTriggeringLocation(),
                        jsonData.toString());

                NotificationUtil.createRegionNotification(getApplicationContext(), message);
            }
        }

        // Do not start polling if it is an entrance event for a Region that we are already signed onto
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            // Not signed into anywhere
            Integer site = locationEngineBridge.getCurrentSite();
            if(site != null) {
                // Don't start monitoring if we received an entrance event for a site we are already
                // signed onto
                for (Geofence geofence : geofencingEvent.getTriggeringGeofences()) {
                    if (Integer.valueOf(geofence.getRequestId()).equals(site)) {
                        SLog.d(LOG, "We are already signed onto current site, not starting location " +
                                "monitoring");
                        stopSelf();
                        return START_NOT_STICKY;
                    }
                }
            }
        }

        tryStartLocationThread(FIVE_MINUTES_MILLIS);
        return START_STICKY;
    }

    private void showForegroundNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification notification = NotificationUtil.buildLocationEngineServiceNotification(this);
            startForeground(NotificationUtil.NOTIFICATION_LOCATION_SERVICE, notification);
        }
    }

    /**
        Sets a stop timer. Cancels pending stop timer too.
     */
    private void setStopTimer(long millis) {
        try {
            mStopTimerMutex.acquire();
        }
        catch (Exception e) {
            SLog.e(LOG, "Exception acquiring mutex occurred: " + e.getMessage());
            return;
        }
        if (mStopTimer != null) {
            // TODO maybe don't cancel old one if its running for longer than this one?
            mStopTimer.cancel();
        }
        mStopTimer = (new Timer(true));
        mStopTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                SLog.d(LOG, "Timer triggered!");
                LocationThread locThread = mLocationThread;
                if (locThread != null) {
                    SLog.i(LOG, "Location polling timed out");
                    DiagnosticsManager.logEvent(getApplicationContext(),
                            DiagnosticLog.Tag.LOC_POLL_NO_ACTION,
                            null,
                            null);
                    locThread.stopThread();
                    try {
                        mStopTimerMutex.acquire();
                    }
                    catch (Exception e) {
                        SLog.e(LOG, "Exception in the timer occurred: " + e.getMessage());
                        return;
                    }
                    mStopTimer = null;
                    mStopTimerMutex.release();
                }
            }
        }, millis);
        SLog.d(LOG, "set timer");
        mStopTimerMutex.release();
    }

    /**
        Note: if we successfully acquire mLocationThreadAlive, then the previous
        location thread must have finished. This means it is only possible
        to have one thread alive at a time :)
     */
    private void tryStartLocationThread(long millis) {
        Log.i(LOG, "tryStartLocationThread");
        if(!mLocationThreadAlive.tryAcquire()) {
            SLog.i(LOG, "Location thread already alive! Resetting stop timer");
            setStopTimer(millis);
            return;
        }
        // mLocationThread must be null here (mLocationThreadAlive only released after nulling)
        final LocationThread locationThread = new LocationThread(this, locationEngineBridge);
        mLocationThread = locationThread;
        mLocationThread.start();
        setStopTimer(millis);
    }

    /**
        Request location thread stops. This method is thread-safe.
     */
    private void cleanupLocationThread() {
        Log.i(LOG, "Cleaning up location thread");
        LocationThread locationThread = mLocationThread;
        if(locationThread != null) {
            Log.i(LOG, "Location Thread was not null");
            locationThread.stopThread();
        }
        stopSelf();
        Log.i(LOG, "Location Thread was null");
    }

    /**
        To be called by location thread when it is done run()ning and by
        NOTHING else
     */
    public void locationThreadQuit() {
        if (!mLocationThreadCleanup.tryAcquire()) {
            return;
        }
        SLog.i(LOG, "locationThreadQuit!");
        LocationThread locationThread = mLocationThread;
        mLocationThread = null;
        mLocationThreadCleanup.release();

        if(locationThread != null) {
            mLocationThreadAlive.release();
            stopSelf();
        }
        else {
            // this is not expected but may happen?
            SLog.i(LOG, "Location thread was null");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SLog.d(LOG, "onDestroy()");
        // not really necessary, but ???
        cleanupLocationThread();
//        stopForeground(true);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

class LocationThread extends Thread implements ConnectionCallbacks,
                   OnConnectionFailedListener,
                   LocationSettingsRequesterDelegate,
                   LocationFilter.LocationFilterDelegate {
    private static final String LOG = "LocEng-" + LocationThread.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient = null;
    private LocationService mLocationService;
    private Looper mLooper;
    private LocationEngineBridge mBridge;
    private Semaphore mStopMutex = new Semaphore(1);
    private LocationFilter mLocationCallback;


    public LocationThread(LocationService service, LocationEngineBridge locationEngineBridge) {
        this.setPriority(Thread.MAX_PRIORITY);
        mLocationService = service;
        mLocationCallback = new LocationFilter(mLocationService,locationEngineBridge, this);
        mBridge = locationEngineBridge;
    }

    public void locationFilterShouldSignOff(Location location) {
        Integer siteId = mBridge.getCurrentSite();

        mBridge.doSignOff(siteId, location);

        // decide whether or not to stop location thread
        if(!FenceTracker.s.entryForAny()) {
            SLog.i(LOG,"Stopping location thread after sign off, have no entry events!");
            stopThread();
        }
    }


    public void locationFilterShouldSignOn(int regionId, Location location) {
        // Check whether we are already signed on
        mBridge.getCurrentSite();
        mBridge.doSignOn(regionId, location);

        // decide whether or not to stop location thread
        if(!FenceTracker.s.exitForRegionOrEntryForOthers(regionId)) {
            SLog.i(LOG,"Stopping location thread after sign on, have no exit for current region or entry for others!");
            stopThread();
        }
    }

    @Override
    public void run() {
        try {
            SLog.d(LOG, "run" + Thread.currentThread().getName());
            Looper.prepare();
            mLooper = Looper.myLooper();
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(mLocationService)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .setHandler(new Handler(mLooper))
                        .build();
                mGoogleApiClient.connect();
            }
            Looper.loop();
            SLog.d(LOG, "EVENT LOOP EXITED");
        }
        catch(Exception e) {
            SLog.e(LOG, "Location thread hit exception!");
        }
        mLocationService.locationThreadQuit();
    }

    public void stopThread() {
        if(!mStopMutex.tryAcquire()) {
            return;
        }
        if (mLooper != null) {
            new Handler(mLooper).post(() -> {
                LocationServices.getFusedLocationProviderClient(mLocationService.getApplicationContext()).removeLocationUpdates(mLocationCallback);
                mGoogleApiClient.disconnect();

                SLog.d(LOG, "onThreadStop" + Thread.currentThread().getName());
                mLooper.quit();
                mLocationCallback.disposeLocation();
            });
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        SLog.d(LOG, "Connected!" + Thread.currentThread().getName());
        new LocationSettingsRequester(this, mGoogleApiClient); }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        this.stopThread();
    }

    @Override
    public void onConnectionSuspended(int n) {
        this.stopThread();
    }

    public void doHaveCorrectSettings(LocationRequest settings) {
        SLog.i(LOG, "Have correct settings - " + Thread.currentThread().getName());

        try {
            LocationServices.getFusedLocationProviderClient(mLocationService.getApplicationContext()).requestLocationUpdates(settings, mLocationCallback, mLooper);
        }
        catch (SecurityException e) {
            SLog.d(LOG, "User has either disabled app access to location or turned off location on their phone");
            DiagnosticsManager.logEvent(mLocationService.getApplicationContext(),
                    DiagnosticLog.Tag.LOC_NO_ACCESS,
                    null,
                    null);
        }
    }

    public void doNotHaveCorrectSettings() {
        SLog.e(LOG, "Settings incorrect!!!");
        stopThread();
    }
}

interface LocationSettingsRequesterDelegate {
    void doHaveCorrectSettings(LocationRequest settings);
    void doNotHaveCorrectSettings();
}

class LocationSettingsRequester {
    protected LocationSettingsRequesterDelegate delegate;

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    protected LocationSettingsRequest createLocationSettingsRequest(LocationRequest request) {
        return new LocationSettingsRequest.Builder()
                .addLocationRequest(request)
                .build();
    }

    public LocationSettingsRequester(LocationSettingsRequesterDelegate delegate, GoogleApiClient googleClient) {

        this.delegate = delegate;
        final LocationRequest request = createLocationRequest();
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleClient,
                        createLocationSettingsRequest(request));

        final LocationSettingsRequesterDelegate finalDelegate = delegate;

        result.setResultCallback(res -> {
            Status status = res.getStatus();
            if (status.getStatusCode() == LocationSettingsStatusCodes.SUCCESS) {
                finalDelegate.doHaveCorrectSettings(request);
            } else {
                finalDelegate.doNotHaveCorrectSettings();
            }
        });
    }
}