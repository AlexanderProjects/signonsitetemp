package au.com.signonsitenew.locationengine.signonoffnotifications;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */

public interface SignOnOffDelegate {
    void signedOn();
    void signedOff();
}