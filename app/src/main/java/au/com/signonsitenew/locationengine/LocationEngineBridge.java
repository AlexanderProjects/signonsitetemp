package au.com.signonsitenew.locationengine;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.locationengine.signonoffnotifications.SignOnOffNotifier;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.realm.EnrolledSite;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.services.BriefingService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 *I provided skellington Skellington SKELLINGTON MATTHEW implementations
 */
public class LocationEngineBridge {
    private static final String LOG = "LocEng-" + LocationEngineBridge.class.getSimpleName();
    private SessionManager mSession;
    protected Context mContext;
    private RealmConfiguration mRealmConfiguration;
    private GeofenceManager mGeofenceManager;
    private NetworkAutoSignOnOff networkAutoSignOnOff;

    private SignOnOffNotifier notifier = null;

    public SignOnOffNotifier getNotifier() {
        if(notifier == null) {
            notifier = new SignOnOffNotifier(mContext);
        }
        return notifier;
    }

    @Inject
    public LocationEngineBridge(Context context, GeofenceManager geofenceManager, NetworkAutoSignOnOff networkAutoSignOnOff) {
        mContext = context;
        mSession = new SessionManager(context);
        mGeofenceManager = geofenceManager;
        mRealmConfiguration = RealmManager.getRealmConfiguration();
        this.networkAutoSignOnOff = networkAutoSignOnOff;
    }

    public Integer getCurrentSite() {
        Integer siteId = mSession.getSiteId();
        if (siteId == 0) {
            return null;
        }
        else {
            return siteId;
        }
    }

    public void setCurrentSite(int siteId) {
        mSession.setSiteId(siteId);
    }

    public void clearCurrentSite() {
        mSession.clearSiteId();
    }

    public void setSignOnTime() {
        mSession.setSignOnTime();
    }

    public void clearSiteBriefing() {
        Realm realm = Realm.getDefaultInstance();
        BriefingService service = new BriefingService(realm);
        service.deleteBriefingsForSite(getCurrentSite());
        realm.close();
    }

    public Region[] getRegions() {
        Realm realm = Realm.getInstance(mRealmConfiguration);
        RealmResults<EnrolledSite> sites = realm.where(EnrolledSite.class).findAll();

        // Detach the sites from their Realm reference so we can pass them around
        ArrayList<Region> regions = new ArrayList<>();
        for (EnrolledSite site : sites) {
            Region region = new Region();
            region.id = site.getId();
            region.name = site.getName();
            region.manager = site.getManagerName();
            region.points = site.getPoints();
            region.address = site.getSiteAddress();
            Location l = new Location("");
            l.setLatitude(site.getRegionLat());
            l.setLongitude(site.getRegionLong());
            region.center = l;
            region.radius = site.getRegionRadius();
            regions.add(region);
        }
        realm.close();
        return regions.toArray(new Region[regions.size()]);
    }

    public void setInductedForSite(boolean inducted, int id) {
        Realm realm = Realm.getInstance(mRealmConfiguration);
        realm.beginTransaction();
        EnrolledSite site = realm.where(EnrolledSite.class).equalTo(Constants.REALM_SITE_ID, id).findFirst();
        site.setInducted(inducted);
        realm.commitTransaction();
        realm.close();
    }

    public void setRegions(Region[] regions) {
        Realm realm = Realm.getInstance(mRealmConfiguration);
        realm.beginTransaction();

        // save the current inducted state ... this is a bit messy
        HashMap<Long, Boolean> inductions = new HashMap<>();
        for (Region region : regions) {
            EnrolledSite realmSite = realm.where(EnrolledSite.class).equalTo("id", region.id).findFirst();
            if (realmSite != null) {
                inductions.put(region.id, realmSite.inducted());
            }
            else {
                inductions.put(region.id, false);
            }
        }

        // delete regions
        realm.where(EnrolledSite.class).findAll().deleteAllFromRealm();

        for (Region region : regions) {
            try {
                EnrolledSite enrolledSite = new EnrolledSite();
                enrolledSite.setId(region.id);
                enrolledSite.setName(region.name);
                enrolledSite.setManagerName(region.manager);
                enrolledSite.setPoints(region.points);
                enrolledSite.setSiteAddress(region.address);
                enrolledSite.setRegionLat(region.center.getLatitude());
                enrolledSite.setRegionLong(region.center.getLongitude());
                enrolledSite.setRegionRadius(region.radius);
                enrolledSite.setInducted(inductions.get(region.id));
                realm.copyToRealmOrUpdate(enrolledSite);
            }
            catch (Exception e) {
                SLog.e(LOG, "setRegions exception: " + e.getMessage());
            }
        }

        realm.commitTransaction();
        realm.close();

        return;
    }

    public boolean shouldAct() {
        boolean loggedIn = mSession.isLoggedIn();
        boolean emergency = mSession.isEmergency();
        boolean automatic = mSession.getAutoSignOnEnabled();

        SLog.i(LOG, "isLoggedIn: " + loggedIn + ", auto SignOn: " + automatic + ", emergency: " + emergency);
        return loggedIn && automatic && !emergency;
    }

    public String getEmail() {
        HashMap<String, String> user = mSession.getCurrentUser();

        return user.get(Constants.USER_EMAIL);
    }

    private Boolean compareInteger(Integer i1, Integer i2) {
        if(i1 == null) {
            return i2 == null;
        } else {
            return i2 != null && i1.equals(i2);
        }
    }

    public void doSignOn(Integer siteId, Location currLocation) {
        SLog.i(LOG, "doSignOn to: " + siteId + " current site: " + getCurrentSite() + " should act: " + shouldAct());

        SLog.i(LOG, "Site: " + siteId + ", getCurrentSite: " + getCurrentSite() + ", shouldAct: " + shouldAct());
        boolean satisfied = (siteId != null) && !compareInteger(siteId, getCurrentSite()) && shouldAct();
        SLog.i(LOG, "Conditions satisfied: " + satisfied);

        if (satisfied) {
            // Do sign on!
            SLog.d(LOG, "Making signon network call");
            Boolean inducted;
            String result = networkAutoSignOnOff.signOn(siteId, currLocation);


            if(result == "auto_signon_blocked") {
                // Create a signed on notification!
               return;
            }
            else if(result == "auto_signon_prompt") {
                // Create a signed on notification!
                return;
            }
            else if(result == "inducted")  {
                inducted = true;
            }
            else {
              inducted = false;
            }

            Log.i(LOG, "Inducted is: " + inducted);
            if(inducted == null) {
                SLog.e(LOG, "Failed to sign on!!!");
                DiagnosticsManager.logEvent(mContext, DiagnosticLog.Tag.LOC_AUTO_ON_ERR, currLocation, siteId.toString());
            }
            else {
                DiagnosticsManager.logEvent(mContext, DiagnosticLog.Tag.LOC_AUTO_ON, currLocation, siteId.toString());
                setInductedForSite(inducted, siteId);

                // Now that our current site has changed, the geofences need to get bigger or smaller...
                Region[] regions = getRegions();
                Integer oldSite = getCurrentSite();
                for(Region region : regions) {
                    if(region.id == siteId) {
                        // put hysteresis and initial_trigger_exit on this region
                        mGeofenceManager.setCurrentRegion(region);
                    }
                    if(oldSite != null && oldSite == region.id) {
                        // remove hysteresis from this region
                        mGeofenceManager.unsetCurrentRegion(region);
                    }
                }

                // Update data model!
                setCurrentSite(siteId);
                setSignOnTime();

                // Send Local Broadcast that user has signed on
                getNotifier().signOn();
            }
        }
    }

    public void doSignOff(Integer siteId, Location location) {
        SLog.i(LOG, "doSignOff");
        boolean satisfied = getCurrentSite() != null && shouldAct();
        SLog.i(LOG, "Conditions satisfied: " + satisfied + ". currentSite: "
                + getCurrentSite() + ", shouldAct: " + shouldAct());

        if (getCurrentSite() != null && shouldAct()) {
            // Do sign off!
            SLog.i(LOG, "Signing off");

            Boolean signedOff;
            String result = networkAutoSignOnOff.signOff(siteId, location);


            if(result == "auto_signoff_blocked") {
                Log.i(LOG, "Couldn't signOff as: " + result);
                return;
            }
            else if(result == "auto_signoff_prompt") {
                Log.i(LOG, "Couldn't signOff as: " + result);
                return;
            }
            else if(result == "signed_off")  {
                signedOff = true;
            }
            else {
                signedOff = false;
            }

            Log.i(LOG, "Signed off was: " + signedOff);
            if(!signedOff) {
                SLog.e(LOG, "Failed to sign off!!!");
                DiagnosticsManager.logEvent(mContext, DiagnosticLog.Tag.LOC_AUTO_OFF_ERR, location, getCurrentSite().toString());
            }
            else {
                // Log diagnostic
                JSONObject jsonData = new JSONObject();
                try {
                    if (siteId != null) {
                        jsonData.put("siteId", siteId.toString());
                    }
                    else {
                        jsonData.put("siteId", "unknown");
                    }
                }
                catch (JSONException e) {
                    SLog.e(LOG, "A JSON Exception occurred: " + e.getMessage());
                }
                DiagnosticsManager.logEvent(mContext, DiagnosticLog.Tag.LOC_AUTO_OFF, location, jsonData.toString());

                // Update data model!
                clearSiteBriefing();
                clearCurrentSite();

                // Now that our current site has changed, the geofences need to get bigger or smaller...
                Region[] regions = getRegions();
                for(Region region : regions) {
                    if(siteId != null && siteId == region.id) {
                        // remove hysteresis from this region
                        mGeofenceManager.unsetCurrentRegion(region);
                    }
                }
                // Send to main activity
                getNotifier().signOff();
            }
        }
    }
}


