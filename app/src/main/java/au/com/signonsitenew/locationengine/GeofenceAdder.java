package au.com.signonsitenew.locationengine;

import android.app.PendingIntent;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

import javax.inject.Inject;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import au.com.signonsitenew.utilities.Synchroniser;

/*
    Synchronises geofence addition. new GeofenceAdder().sync();
 */
public class GeofenceAdder extends Synchroniser<Boolean> {
    private Context mContext;
    private GeofencingClient mGeofencingClient;
    private PendingIntent mPendingIntent;
    private final String TAG = "LocEng-" + GeofenceAdder.class.getSimpleName();
    private Region[] mRegions;

    private boolean mShouldTrigger;
    private SessionManager sessionManager;

    private final long EXPIRY_TIME = 1000 * 60 * 60 * 24; // one day TODO: try unlimited

    @Inject
    public GeofenceAdder(Context context, SessionManager sessionManager){
        mContext = context;
        this.sessionManager = sessionManager;
    }

    public GeofenceAdder setParameters(PendingIntent intent, Region[] regions, boolean shouldTrigger){
        mPendingIntent = intent;
        mRegions = regions;
        mShouldTrigger = shouldTrigger;
        mGeofencingClient = LocationServices.getGeofencingClient(mContext);
        return this;
    }

    @Override
    public void work() {
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .build();
        ConnectionResult result = mGoogleApiClient.blockingConnect();
        if(!result.isSuccess()) {
            setResult(false);
            return;
        }

        // Build fences
        ArrayList<Geofence> fences = new ArrayList<>(mRegions.length);

        Integer currentSite = sessionManager.getSiteId();

        for(Region region : mRegions) {
            float radius = (float) region.getGeofenceRadius(currentSite);
            SLog.d(TAG, "Adding RegionMarker with id: " + region.id + ", and radius: " + radius);
            fences.add(new Geofence.Builder()
                    .setRequestId("" + region.id)
                    .setCircularRegion(
                            region.center.getLatitude(),
                            region.center.getLongitude(),
                            radius
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());
        }

        // Build request to add fences
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        if(mShouldTrigger) {
            SLog.d(TAG, "Setting initial trigger enter");
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        }
        else {
            SLog.d(TAG, "Setting initial trigger exit");
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_EXIT);
        }

        builder.addGeofences(fences);
        GeofencingRequest request = builder.build();

        // Make request to add fences
        try {
            mGeofencingClient.addGeofences(request, mPendingIntent)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences added
                            SLog.d(TAG, "Regions / geofences added successfully");
                            setResult(true);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Geofences failed to be added
                            SLog.d(TAG, "Failed to add regions / geofences with exception: " + e.toString());
                            setResult(false);
                        }
                    });
        }
        catch (SecurityException e) {
            SLog.d(LOG, "User has either disabled app access to location or turned off location on their phone");
            DiagnosticsManager.logEvent(mContext,
                    DiagnosticLog.Tag.LOC_NO_ACCESS,
                    null,
                    null);
        }
    }
}
