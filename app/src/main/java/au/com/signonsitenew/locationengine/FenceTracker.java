package au.com.signonsitenew.locationengine;

import com.google.android.gms.location.Geofence;

import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * This class keeps track of whether or not a user is still within a region or not. The SOS Location
 * Engine utilises this to determine whether GPS polling should remain active or disengage.
 *
 * For example, if a user signs on to a region but is still within another region of interest, the
 * engine will stay active, however if a user signs off and is no longer within any regions, the
 * engine will disengage.
 */

public class FenceTracker {

    private final String LOG = "LocEng-" + FenceTracker.class.getSimpleName();
    private Map<Integer,FenceEvent> mEvents;
    public static FenceTracker s = new FenceTracker();
    private static final int FIVE_MINUTES_IN_MILLIS = 1000 * 60 * 5;

    private FenceTracker() {
        mEvents = new HashMap<Integer,FenceEvent>();
    }

    // is transition older than 5 minutes?
    private boolean isOld(FenceEvent event) {
        return System.currentTimeMillis() - event.time > FIVE_MINUTES_IN_MILLIS;
    }

    // remove all transitions older than 5 minutes
    private void purgeOld() {
        HashMap<Integer,FenceEvent> newMap = new HashMap<>();

        for(Map.Entry<Integer,FenceEvent> entry : mEvents.entrySet()) {
            if(!isOld(entry.getValue())) {
                SLog.i(LOG, "Not purging geofence event since it is less than 5 mins old");
                newMap.put(entry.getKey(),entry.getValue());
            }
        }

        mEvents = newMap;
    }

    /**
     * Takes a geofence ID in string format and converts it to an integer
     * @param geofenceId - String input of geofence ID
     * @return - The goefence ID in Integer form.
     */
    private Integer parseRegionId(String geofenceId) {
        try {
            Integer id = Integer.parseInt(geofenceId);
            return id;
        }
        catch (Exception e) {
            SLog.e(LOG, "Exception with parsing RegionId: " + e.getMessage());
            return null;
        }
    }

    public boolean entranceForExitedRegion(String geofenceId, Integer transitionType) {
        Integer regionId = parseRegionId(geofenceId);

        // If event is an entrance, check for an exit event for the same region
        if (regionId != null && transitionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
            // Check if there is a history for that geofence in the fence tracker
            if (mEvents.containsKey(regionId)) {
                if (mEvents.get(regionId).transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    // The event will only exist if it were less than 5 mins old, so stop polling
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Add's a geofence event to the fence tracker
     * @param regionId the ID of the region
     * @param transition the transition type of the event
     */
    private void addEvent(String regionId, int transition) {
        Integer id = parseRegionId(regionId);
        if(id == null) {
            SLog.e(LOG, "Failed to add region could not parse ID");
            return;
        }

        FenceEvent event = new FenceEvent();
        event.transition = transition;
        event.time = System.currentTimeMillis();
        mEvents.put(id, event);
        SLog.i(LOG, "Added region event");
    }

    public void didEnter(String id) {
        addEvent(id, Geofence.GEOFENCE_TRANSITION_ENTER);
    }

    public void didExit(String id) {
        addEvent(id, Geofence.GEOFENCE_TRANSITION_EXIT);
    }

    // returns true if we have an entry event for any region in the last 5 minutes
    // this is the condition for not stopping monitoring upon signoff
    public boolean entryForAny() {
        purgeOld();

        SLog.i(LOG, "Number of events in mEvents: " + String.valueOf(mEvents.entrySet().size()));
        for(Map.Entry<Integer, FenceEvent> entry : mEvents.entrySet()) {
            SLog.i(LOG, "Fence event in tracker: " + entry.getValue().transition);
            if(entry.getValue().transition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                SLog.i(LOG, "Entry for site within last 5 mins, keep running");
                return true;
            }
        }

        return false;
    }

    // returns true if we have an exit event for region or an entry event for any other region
    // this is the condition for not stopping monitoring upon signon
    public boolean exitForRegionOrEntryForOthers(int regionId) {
        purgeOld();

        for(Map.Entry<Integer,FenceEvent> entry : mEvents.entrySet()) {
            if(entry.getKey() == regionId) {
                if(entry.getValue().transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                    SLog.i(LOG, "Exit for current site, keep running");
                    return true;
                }
            }
            else {
                if(entry.getValue().transition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    SLog.i(LOG, "Entry for other site, keep running");
                    return true;
                }
            }
        }

        return false;
    }

    class FenceEvent {
        int transition;
        long time;
    }
}
