package au.com.signonsitenew.locationengine.signonoffnotifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import au.com.signonsitenew.jobscheduler.RegionFetcherJobService;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */

public class SignOnOffNotifier extends BroadcastReceiver {
    private final String SIGN_ON_ACTION = "SOS_SIGN_ON_INTENT";
    private final String SIGN_OFF_ACTION = "SOS_SIGN_OFF_INTENT";
    private final String LOG = "LocEng-" + SignOnOffNotifier.class.getSimpleName();

    private LocalBroadcastManager mBroadcastManager;
    private SignOnOffDelegate mDelegate = null;

    public SignOnOffNotifier(Context context) {
        RegionFetcherJobService.schedule(context);
        mBroadcastManager = LocalBroadcastManager.getInstance(context);
    }

    public void signOn() {
        SLog.d(LOG, "sending signon notification");
        Intent intent = new Intent(SIGN_ON_ACTION);
        mBroadcastManager.sendBroadcast(intent);
    }

    public void signOff() {
        SLog.d(LOG, "sending signon notification");
        Intent intent = new Intent(SIGN_OFF_ACTION);
        mBroadcastManager.sendBroadcast(intent);
    }

    public void setDelegate(SignOnOffDelegate delegate) {
        mDelegate = delegate;
        IntentFilter filter = new IntentFilter(SIGN_ON_ACTION);
        filter.addAction(SIGN_OFF_ACTION);
        mBroadcastManager.registerReceiver(this, filter);
    }

    public void unsetDelegate() {
        mBroadcastManager.unregisterReceiver(this);
        mDelegate = null;
    }

    public void onReceive(Context context, Intent intent) {
        SLog.d(LOG, "Got notification!");
        if(mDelegate != null) {
            switch(intent.getAction()) {
                case SIGN_OFF_ACTION:
                    SLog.d(LOG, "was signed off");
                    mDelegate.signedOff();
                    break;
                case SIGN_ON_ACTION:
                    SLog.d(LOG, "was signed on");
                    mDelegate.signedOn();
                    break;
            }
        }
    }
}
