package au.com.signonsitenew.locationengine;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import com.birbit.android.jobqueue.JobManager;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;
import javax.inject.Inject;
import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.domain.models.CompanyPropertyType;
import au.com.signonsitenew.domain.models.CompanySegmentProperties;
import au.com.signonsitenew.domain.models.SignOnOffType;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.jobs.offline.SignOffJob;
import au.com.signonsitenew.jobs.offline.SignOnJob;
import au.com.signonsitenew.realm.services.BriefingService;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.realm.services.UserAbilitiesService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * This class contains blocking network calls to the sign on and off functions. These calls are
 * exclusively referenced by the Location Manager and are purposely designed to be blocking to
 * ensure that the backgrounded location engine runs as expected. Ie not updating the UI or making
 * additional calls before a result is returned from the network call.
 */

public class NetworkAutoSignOnOff {
    private static final String LOG = "LocEng-" + NetworkAutoSignOnOff.class.getSimpleName();

    private SignOffJob signOffJob;
    private SignOnJob signOnJob;
    private SOSAPI mAPI;
    private Context context;
    private NotificationUseCase notificationUseCase;
    private SessionManager sessionManager;
    private AnalyticsEventDelegateService analyticsEventDelegateService;


    @Inject
    public NetworkAutoSignOnOff(Context context,SessionManager sessionManager,NotificationUseCase notificationUseCase, SignOffJob signOffJob, SignOnJob signOnJob, AnalyticsEventDelegateService analyticsEventDelegateService) {
        this.sessionManager = sessionManager;
        mAPI = new SOSAPI(context);
        this.signOffJob = signOffJob;
        this.signOnJob = signOnJob;
        this.notificationUseCase = notificationUseCase;
        this.context = context;
        this.analyticsEventDelegateService = analyticsEventDelegateService;
    }

    /**
     * Performs a thread-blocking network request to sign a user onto site in the background.
     * @param siteId   - the ID of the site the device is attempting to sign onto
     * @param location - the GPS location of the device
     */

    public String signOn(Integer siteId, Location location) {
        try {
            String signedOnResult = mAPI.signOnSiteBlocking(true, siteId, location);
            SLog.d(LOG, "Sign on got: " + signedOnResult);
            JSONObject json = new JSONObject(signedOnResult);

            if(json.getString(Constants.JSON_STATUS).equals(Constants.JSON_SIGNON_BLOCKED)) {
                return "auto_signon_blocked";
            }

            if(json.getString(Constants.JSON_STATUS).equals(Constants.JSON_SIGNON_PROMPT)) {
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable myRunnable = () -> notificationUseCase.isUserSignedOn(true).setSiteId(siteId).hasPrompt(true).buildSiteNotifications();
                mainHandler.post(myRunnable);
                return "auto_signon_prompt";
            }

            if(json.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                analyticsEventDelegateService.signedOn(SignOnOffType.Auto, new CompanySegmentProperties(CompanyPropertyType.Auto,null),false);
                Realm realm = Realm.getDefaultInstance();
                UserAbilitiesService abilitiesService = new UserAbilitiesService(realm);
                SiteSettingsService siteSettingsService = new SiteSettingsService(realm);
                UserService userService = new UserService(realm);

                abilitiesService.setAbilities(new JSONObject(signedOnResult));
                siteSettingsService.createSiteSettings(new JSONObject(signedOnResult));
                long userId = userService.getCurrentUserId();

                if (!json.isNull(Constants.BRIEFING_ID)) {
                    BriefingService service = new BriefingService(realm);
                    service.storeBriefing(json.getInt(Constants.BRIEFING_ID), siteId, json.getString(Constants.BRIEFING), json.getString(Constants.BRIEFING_AUTHOR), json.getString(Constants.BRIEFING_DATE), json.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK));
                    realm.close();
                }
                sessionManager.setSignOnTime();

                // Check user's induction status
                boolean formAvailable = json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE);
                if (formAvailable) {
                    // Inductions are enabled on site - grab the form
                    SiteInductionService service = new SiteInductionService(realm);
                    if (json.isNull(Constants.JSON_INDUCTION)) {
                        // User has not completed an induction
                        Date date = new Date();
                        android.text.format.DateFormat df = new android.text.format.DateFormat();
                        String formattedDate = df.format("yyyy-MM-dd hh:mm:ss", date).toString();
                        service.createOrUpdateInduction(null, userId, siteId, null, null, formattedDate);
                    }
                    else {
                        // User is inducted
                        JSONObject induction = json.getJSONObject(Constants.JSON_INDUCTION);
                        int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                        String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                        JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                        String inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                        String updatedAt = state.getString(Constants.JSON_IND_STATE_AT);
                        service.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, updatedAt);
                    }
                }

                realm.close();
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable myRunnable = () -> {
                    // Create a signed on notification!
                    try {
                        if(json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE) && !json.isNull(Constants.BRIEFING_ID))
                            notificationUseCase
                                    .isUserSignedOn(true)
                                    .setSiteId(siteId)
                                    .hasPrompt(false)
                                    .setState(notificationUseCase.convertDocumentBooleansToAttendanceState(
                                            json.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK),
                                            json.getBoolean("inducted"),
                                            true,
                                            json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE)))
                                    .buildSiteNotifications();
                        else if(!json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE) && !json.isNull(Constants.BRIEFING_ID))
                            notificationUseCase
                                    .isUserSignedOn(true)
                                    .setSiteId(siteId)
                                    .hasPrompt(false)
                                    .setState(notificationUseCase.convertDocumentBooleansToAttendanceState(
                                            json.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK),
                                            false,
                                            true,
                                            false))
                                    .buildSiteNotifications();
                        else if(json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE) && json.isNull(Constants.BRIEFING_ID))
                            notificationUseCase
                                    .isUserSignedOn(true)
                                    .setSiteId(siteId)
                                    .hasPrompt(false)
                                    .setState(notificationUseCase.convertDocumentBooleansToAttendanceState(
                                            false,
                                            json.getBoolean("inducted"),
                                            false,
                                            true))
                                    .buildSiteNotifications();
                        else if(!json.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE) && json.isNull(Constants.BRIEFING_ID))
                            notificationUseCase
                                    .isUserSignedOn(true)
                                    .setSiteId(siteId)
                                    .hasPrompt(false)
                                    .setState(notificationUseCase.convertDocumentBooleansToAttendanceState(
                                            false,
                                            false,
                                            false,
                                            false))
                                    .buildSiteNotifications();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                };
                mainHandler.post(myRunnable);
                EventBus.getDefault().post(new StatusEvent(Constants.EVENT_SIGN_ON));

                if(json.getBoolean("inducted")) {
                    return "inducted";
                }
                else {
                    return "non_inducted";
                }
            }
        }
        catch (Exception e) {
            SLog.e(LOG, "Couldn't sign on!!!!!! " + e.toString());

            // Set job to attempt sign on in better network conditions
            JobManager jobManager = SOSApplication.getInstance().getJobManager();
            SLog.i(LOG, "Adding background SignOn job");
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();
            jobManager.addJobInBackground(signOnJob.setParameters(LOG, siteId, latitude, longitude));
        }
        return null;
    }

    /**
     * Performs a thread-blocking network request to sign a user onto site in the background.
     */
    public String signOff(Integer siteId, Location location) {
        try {
            String signedOffResult = mAPI.signOffSiteBlocking(true, siteId, location);
            SLog.d(LOG, "Sign off got: " + signedOffResult);
            JSONObject json = new JSONObject(signedOffResult);

            if(json.getString(Constants.JSON_STATUS).equals(Constants.JSON_SIGNOFF_BLOCKED)) {
                return "auto_signoff_blocked";
            }
            else if(json.getString(Constants.JSON_STATUS).equals(Constants.JSON_SIGNOFF_PROMPT)) {
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable myRunnable = () -> notificationUseCase.isUserSignedOn(false).setSiteId(siteId).hasPrompt(true).buildSiteNotifications();
                mainHandler.post(myRunnable);
                EventBus.getDefault().post(new StatusEvent(Constants.EVENT_SIGN_OFF));
                return "auto_signoff_prompt";
            }
            else if (json.getBoolean("signedOff")) {
                analyticsEventDelegateService.signedOff(SignOnOffType.Auto);
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable myRunnable = () ->  notificationUseCase.isUserSignedOn(false).setSiteId(siteId).hasPrompt(false).buildSiteNotifications();
                mainHandler.post(myRunnable);
                EventBus.getDefault().post(new StatusEvent(Constants.EVENT_SIGN_OFF));
                return "signed_off";
            }
            else {
                return "not_signed_off";
            }
        }
        catch (Exception e) {
            SLog.e(LOG, "Couldn't sign off!!!!!!" + e.toString());
            // Set job to attempt sign off in better network conditions
            JobManager jobManager = SOSApplication.getInstance().getJobManager();
            SLog.i(LOG, "Adding background SignOff job");
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();
            jobManager.addJobInBackground(signOffJob.setParameters(LOG, siteId, latitude, longitude));
        }
        return "not_signed_off";
    }
}
