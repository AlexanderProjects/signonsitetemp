package au.com.signonsitenew.events;
import au.com.signonsitenew.models.BadgeNotification;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBusPassport {
    private PublishSubject<BadgeNotification> bus = PublishSubject.create();

    public void send(BadgeNotification notification) {
        bus.onNext(notification);
    }

    public Observable<BadgeNotification> toObservable() {
        return bus;
    }
}
