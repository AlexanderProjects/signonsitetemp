package au.com.signonsitenew.events;

/**
 * Created by Krishan Caldwell on 8/07/2016.
 */
public class LocationPermissionGrantedEvent {
    public Boolean locPermissionGranted;

    public LocationPermissionGrantedEvent(Boolean locPermissionGranted) {
        this.locPermissionGranted = locPermissionGranted;
    }

    @Override
    public String toString() {
        return "Location Permission Granted Event : { locPermissionGranted : " + locPermissionGranted + " }";
    }
}
