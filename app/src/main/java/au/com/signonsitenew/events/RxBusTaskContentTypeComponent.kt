package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.ComponentTypeForm
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusTaskContentTypeComponent {
    private val busTaskComponentTypeForm = PublishSubject.create<ComponentTypeForm>()

    fun sendComponentTypeForm(componentTypeForm: ComponentTypeForm) {
        busTaskComponentTypeForm.onNext(componentTypeForm)
    }
    fun toTaskCheckBoxResponsesObservable(): Observable<ComponentTypeForm> {
        return busTaskComponentTypeForm
    }
}