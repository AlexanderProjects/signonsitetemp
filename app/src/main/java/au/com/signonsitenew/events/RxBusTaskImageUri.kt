package au.com.signonsitenew.events

import android.net.Uri
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusTaskImageUri {
    private val busImageUri = PublishSubject.create<Uri>()

    fun sendImageUri(imageUri: Uri) {
       busImageUri.onNext(imageUri)
    }
    fun toImageUriObservable(): Observable<Uri> {
        return busImageUri
    }
}