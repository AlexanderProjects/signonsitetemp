package au.com.signonsitenew.events;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class InductionEvent {
    public final String eventType;

    public InductionEvent(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "SiteInductionInformation Event : { EventType : " + eventType + " }";
    }
}
