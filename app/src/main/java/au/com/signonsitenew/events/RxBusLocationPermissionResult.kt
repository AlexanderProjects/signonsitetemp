package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.RequestPermission
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusLocationPermissionResult {
    private val busLocationPermission = PublishSubject.create<RequestPermission>()

    fun sendLocationPermissionResult(requestPermission: RequestPermission) {
        busLocationPermission.onNext(requestPermission)
    }

    fun toLocationPermissionObservable(): Observable<RequestPermission> {
        return busLocationPermission
    }
}