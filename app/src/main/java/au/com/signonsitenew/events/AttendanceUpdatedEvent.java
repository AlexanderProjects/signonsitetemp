package au.com.signonsitenew.events;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class AttendanceUpdatedEvent {
    public final String eventType;

    public AttendanceUpdatedEvent(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "Status Event : { EventType : " + eventType + " }";
    }
}
