package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.PermitInfo
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusUpdateTeamTab {
    private val updatePermitTeamTabBus = PublishSubject.create<PermitInfo>()

    fun sendPermitTeamTabBus(value: PermitInfo){
        return updatePermitTeamTabBus.onNext(value)
    }

    fun toSendPermitTeamTabBusObservable(): Observable<PermitInfo> {
        return updatePermitTeamTabBus
    }
}