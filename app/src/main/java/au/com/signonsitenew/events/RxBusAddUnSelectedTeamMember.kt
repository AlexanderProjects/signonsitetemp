package au.com.signonsitenew.events

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusAddUnSelectedTeamMember {
    private val busTeamMember = PublishSubject.create<Any>()

    fun sendUnSelectedMembers(o: Any) {
        busTeamMember.onNext(o)
    }

    fun toUnSelectedMembersObservable(): Observable<Any> {
        return busTeamMember
    }
}