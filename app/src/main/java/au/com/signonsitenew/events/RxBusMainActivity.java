package au.com.signonsitenew.events;


import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBusMainActivity {
    private PublishSubject<Integer> bus = PublishSubject.create();

    public void send(Integer counter) {
        bus.onNext(counter);
    }

    public Observable<Integer> toObservable() {
        return bus;
    }
}
