package au.com.signonsitenew.events;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class StatusEvent {
    public final String eventType;

    public StatusEvent(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "Status Event : { EventType : " + eventType + " }";
    }
}
