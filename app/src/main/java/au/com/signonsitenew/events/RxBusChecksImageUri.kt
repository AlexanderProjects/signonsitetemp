package au.com.signonsitenew.events

import android.net.Uri
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusChecksImageUri {
    private val busImageUri = PublishSubject.create<Uri>()

    fun sendImageUriInChecks(imageUri: Uri) {
        busImageUri.onNext(imageUri)
    }
    fun toImageUriInChecksObservable(): Observable<Uri> {
        return busImageUri
    }
}