package au.com.signonsitenew.events;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class BriefingEvent {
    public final String eventType;

    public BriefingEvent(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "Briefing Event : { EventType : " + eventType + " }";
    }
}
