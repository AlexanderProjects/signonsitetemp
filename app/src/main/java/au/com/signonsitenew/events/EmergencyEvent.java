package au.com.signonsitenew.events;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class EmergencyEvent {
    public final Boolean isEmergency;
    public Boolean isInitiator;

    public EmergencyEvent(Boolean isEmergency, Boolean isInitiator) {
        this.isEmergency = isEmergency;
        this.isInitiator = isInitiator;
    }

    @Override
    public String toString() {
        return "Status Event : { isEmergency : " + isEmergency + ", isInitiator : " + isInitiator + " }";
    }
}
