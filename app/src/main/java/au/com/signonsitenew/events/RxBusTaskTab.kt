package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.PermitInfo
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusTaskTab {
    private val busTaskTabUpdate = PublishSubject.create<PermitInfo>()

    fun sendSelectedMembers(permitInfo: PermitInfo) {
        busTaskTabUpdate.onNext(permitInfo)
    }
    fun toTaskTabObservable(): Observable<PermitInfo> {
        return busTaskTabUpdate
    }
}