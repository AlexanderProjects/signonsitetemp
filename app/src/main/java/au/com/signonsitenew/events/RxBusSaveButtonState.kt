package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.SaveButtonState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusSaveButtonState {
    private val busSaveButtonState = PublishSubject.create<SaveButtonState>()

    fun sendSaveButtonState(value: SaveButtonState) {
        busSaveButtonState.onNext(value)
    }
    fun toSendSaveButtonObservable(): Observable<SaveButtonState> {
        return busSaveButtonState
    }
}