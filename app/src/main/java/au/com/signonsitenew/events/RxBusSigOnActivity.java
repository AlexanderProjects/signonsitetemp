package au.com.signonsitenew.events;

import au.com.signonsitenew.models.BadgeNotification;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBusSigOnActivity {
    private PublishSubject<BadgeNotification> bus = PublishSubject.create();

    public void send(BadgeNotification notifier) {
        bus.onNext(notifier);
    }

    public Observable<BadgeNotification> toObservable() {
        return bus;
    }
}
