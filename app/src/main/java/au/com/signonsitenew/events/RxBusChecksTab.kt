package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusChecksTab {
    private val busChecksTabUpdate = PublishSubject.create<PermitContentTypeState>()

    fun sendSelectedMembers(permitContentTypeState: PermitContentTypeState) {
        busChecksTabUpdate.onNext(permitContentTypeState)
    }
    fun toTaskTabObservable(): Observable<PermitContentTypeState> {
        return busChecksTabUpdate
    }
}