package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.ComponentTypeForm
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusUserPermitResponses {
    private val busUserResponses = PublishSubject.create<ComponentTypeForm>()

    fun sendUserPermitResponse(value: ComponentTypeForm) {
        busUserResponses.onNext(value)
    }
    fun toSendUserPermitResponseObservable(): Observable<ComponentTypeForm> {
        return busUserResponses
    }
}