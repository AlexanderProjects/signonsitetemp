package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.ComponentTypeForm
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusChecksContentTypeComponent {
    private val busChecksComponentTypeForm = PublishSubject.create<ComponentTypeForm>()

    fun sendComponentTypeForm(componentTypeForm: ComponentTypeForm) {
        busChecksComponentTypeForm.onNext(componentTypeForm)
    }
    fun toChecksCheckBoxResponsesObservable(): Observable<ComponentTypeForm> {
        return busChecksComponentTypeForm
    }
}