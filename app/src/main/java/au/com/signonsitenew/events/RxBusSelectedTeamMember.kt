package au.com.signonsitenew.events

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusSelectedTeamMember {

    private val busAddTeamMember = PublishSubject.create<Any>()

    fun sendSelectedMembers(o: Any) {
        busAddTeamMember.onNext(o)
    }
    fun toSelectedMembersObservable(): Observable<Any> {
        return busAddTeamMember
    }

}