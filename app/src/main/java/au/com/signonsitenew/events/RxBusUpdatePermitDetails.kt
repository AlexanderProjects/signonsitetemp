package au.com.signonsitenew.events

import au.com.signonsitenew.domain.models.PermitInfo
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusUpdatePermitDetails {
    private val updatePermitDetailsBus = PublishSubject.create<PermitInfo>()

    fun sendPermitDetailsBus(value:PermitInfo){
        return updatePermitDetailsBus.onNext(value)
    }

    fun toSendPermitDetailsBusObservable(): Observable<PermitInfo> {
        return updatePermitDetailsBus
    }
}