package au.com.signonsitenew.events

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBusUpdateCtaState {
    private val busUpdateCtaState = PublishSubject.create<Any>()

    fun sendUpdateCtaState(o: Any) {
        busUpdateCtaState.onNext(o)
    }

    fun toSendUpdateCtaUpdateObservable(): Observable<Any> {
        return busUpdateCtaState
    }
}