package au.com.signonsitenew;

import android.annotation.SuppressLint;
import android.content.Context;;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import androidx.multidex.MultiDex;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.datadog.android.Datadog;
import com.datadog.android.DatadogConfig;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import net.danlew.android.joda.JodaTimeAndroid;
import java.util.UUID;
import au.com.signonsitenew.di.components.DaggerApplicationComponent;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.reactivex.plugins.RxJavaPlugins;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Application class for instantiating singleton instances of helper classes.
 */

public class SOSApplication extends DaggerApplication {

    public static final String LOG = SOSApplication.class.getSimpleName();

    // Singleton instance of the application class for easy access in other places
    public static SOSApplication sInstance;

    // Global request queue for Volley
    private RequestQueue mRequestQueue;

    private JobManager mJobManager;

    private boolean isEmergency = false;


    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = RealmManager.getRealmConfiguration();
        Realm.setDefaultConfiguration(realmConfiguration);

        JodaTimeAndroid.init(this);

        DatadogConfig config = new DatadogConfig.Builder(
                BuildConfig.DATA_DOG_TOKEN,
                BuildConfig.BUILD_TYPE,
                UUID.nameUUIDFromBytes(
                        BuildConfig.APPLICATION_ID.getBytes())
        ).setServiceName("signonsite-android").build();
        Datadog.initialize(this, config);
        Datadog.setVerbosity(Log.VERBOSE);

        // Initialise the singleton
        sInstance = this;
        configureJobManager(); // Configures priority job queue
        //RxJava errors
        RxJavaPlugins.setErrorHandler(throwable -> SLog.d(Constants.ERROR_RX_LOG_TITLE, throwable.getMessage()));


        //Send user id to Firebase Crashlytics
        UserService userService = new UserService(Realm.getDefaultInstance());
        if (userService.getCurrentUserId() != 0)
            FirebaseCrashlytics.getInstance().setUserId(String.valueOf(userService.getCurrentUserId()));
        //Retrieve asynchronously push notification token for analytics
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful())
                return;
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SOS_SHARED_PREF, MODE_PRIVATE);
            sharedPreferences.edit().putString(Constants.FIREBASE_PUSH_NOTIFICATION_TOKEN, task.getResult()).apply();
        });
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().application(this).build();
    }


    public void setEmergency(boolean isEmergency) {
        this.isEmergency = isEmergency;
    }

    /**
     * @return SoSApplication singleton instance.
     */
    public static synchronized SOSApplication getInstance() {
        return sInstance;
    }

    /**
     * @return the Volley Request queue, the queue will be created if it is null.
     */
    public RequestQueue getRequestQueue() {
        // Lazy initialise the request queue, the queue instance will be created when it is
        // accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if LOG TAG is specified then it is used,
     * else Default LOG is used.
     *
     * @param request
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> request, String tag) {
        // Set the default log tag if it is empty
        request.setTag(TextUtils.isEmpty(tag) ? LOG : tag);

        VolleyLog.d("Adding request to queue: %s", request.getUrl());

        // Set default RETRY policy
        DefaultRetryPolicy defaultRetryPolicy = createDefaultRetryPolicy();
        request.setRetryPolicy(defaultRetryPolicy);

        getRequestQueue().add(request);
    }

    /**
     * Adds the specified request to the global queue using the Default LOG TAG.
     *
     * @param request
     */
    public <T> void addToRequestQueue(Request<T> request) {
        // set the default log tag if it is empty
        request.setTag(LOG);

        // Set default RETRY policy
        DefaultRetryPolicy defaultRetryPolicy = createDefaultRetryPolicy();
        request.setRetryPolicy(defaultRetryPolicy);

        getRequestQueue().add(request);
    }

    /**
     * Cancels all pending requests by the specified LOG TAG, it is important to specify a LOG TAG
     * so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private DefaultRetryPolicy createDefaultRetryPolicy() {

        if (isEmergency) {
            return new DefaultRetryPolicy(Constants.INITIAL_TIMEOUT_FOR_EMERGENCY, Constants.RETRY_ATTEMPTS_FOR_EMERGENCY, Constants.BACKOFF_MULTIPLIER_FOR_EMERGENCY);
        }
        int timeout = Constants.INITIAL_TIMEOUT;
        int retryAttempts = Constants.RETRY_ATTEMPTS;
        float backoffMultiplier = Constants.BACKOFF_MULTIPLIER;

        return new DefaultRetryPolicy(timeout, retryAttempts, backoffMultiplier);
    }

    public synchronized JobManager getJobManager() {
        if (mJobManager == null) {
            configureJobManager();
        }
        return mJobManager;
    }

    private void configureJobManager() {
        Configuration.Builder builder = new Configuration.Builder(this)
//                .customLogger(new CustomLogger() {
//                    private static final String TAG = "JOBS";
//                    @Override
//                    public boolean isDebugEnabled() {
//                        return true;
//                    }
//
//                    @Override
//                    public void d(String text, Object... args) {
//                        Log.d(TAG, String.format(text, args));
//                    }
//
//                    @Override
//                    public void e(Throwable t, String text, Object... args) {
//                        Log.e(TAG, String.format(text, args), t);
//                    }
//
//                    @Override
//                    public void e(String text, Object... args) {
//                        Log.e(TAG, String.format(text, args));
//                    }
//
//                    @Override
//                    public void v(String text, Object... args) {
//
//                    }
//                })
                .minConsumerCount(1) // always keep at least one consumer alive
                .maxConsumerCount(3) // up to 3 consumers at a time
                .loadFactor(3) // 3 jobs per consumer
                .consumerKeepAlive(120); // wait 2 minutes

        mJobManager = new JobManager(builder.build());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
