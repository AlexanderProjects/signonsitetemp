package au.com.signonsitenew.jobs.offline;


import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.realm.services.BriefingService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * A job to call the SignOnSite API and sign a user off from site. The location engine will
 * determine if a user should sign off and will attempt to sign them off at that point in time.
 * However, should the network call return a failed status, then this job will be called to
 * perform the job at a later time when network stability is established.
 *
 * Persists to the disk if network is unavailable.
 */
public class SignOffJob extends Job {
    private static final String LOG = SignOffJob.class.getSimpleName();
    private static final int PRIORITY = Constants.HIGH;
    private NotificationUseCase notificationUseCase;
    private Integer mSiteId;
    private Double mLat;
    private Double mLon;

    @Inject
    public SignOffJob(NotificationUseCase notificationUseCase) {
        // Require network connectivity and should be persisted in case the application exits before
        // the job is completed.
        super(new Params(PRIORITY).requireNetwork().persist().addTags(LOG));
        this.notificationUseCase = notificationUseCase;

    }

    public SignOffJob setParameters(String tag, Integer siteId, Double lat, Double lon){
        mSiteId = siteId;
        mLat = lat;
        mLon = lon;
        Log.i(LOG, "SignOffJob created with Tag: " + tag);
        return this;
    }

    @Override
    public void onAdded() {
        // Job has been saved to disk
        // This is a good place to dispatch a UI event to indicate the job will eventually run.
        SLog.i(LOG, "Job has been added to disk - will run when network is available");
    }

    @Override
    public void onRun() throws Throwable {
        SLog.i(LOG, "Job is running");
        new SOSAPI(getApplicationContext())
                .signOffSite(true, mSiteId, mLat, mLon, new SOSAPI.VolleySuccessCallback() {

                    @Override
                    public void onResponse(String response) {
                        SLog.i(LOG, "Response from " + Constants.URL_SIGN_OFF + ": " + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);

                            if (jsonResponse.getBoolean("signedOff")) {

                                // Delete briefing details
                                Realm realm = Realm.getDefaultInstance();
                                BriefingService service = new BriefingService(realm);
                                Log.i("realm briefing object", String.valueOf(mSiteId));
                                service.deleteBriefingsForSite(mSiteId);
                                realm.close();

                                // Fire Event
                                EventBus.getDefault().post(new StatusEvent(Constants.EVENT_SIGN_OFF));

                                Handler mainHandler = new Handler(Looper.getMainLooper());
                                Runnable runnable = () -> notificationUseCase.hasPrompt(false).setSiteId(mSiteId).isUserSignedOn(false).buildSiteNotifications();
                                mainHandler.post(runnable);
                            }
                        }
                        catch (JSONException e) {
                            SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                        }
                    }
                }, null);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        // An error occurred in onRun.
        // Return value determines whether this job should RETRY or cancel. You can further
        // specify a backoff strategy or change the job's priority. You can also apply the
        // delay to the whole group to preserve jobs' running order.
        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        // Job has exceeded RETRY attempts or shouldReRunOnThrowable() has decided to cancel.
    }
}
