package au.com.signonsitenew.jobs.offline;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.Nullable;
import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * A job to Sign a user onto site. The Location Engine will attempt to sign a user onto site if
 * it determines that all conditions have been satisfied. However, there is the potential for the
 * network call to fail. If it should, this job will be invoked as a fail safe, and will wait for
 * proper network conditions to be established.
 *
 * Persists to the disk if network is unavailable.
 */
public class SignOnJob extends Job {
    private static final String LOG = SignOnJob.class.getSimpleName();
    private static final int PRIORITY = Constants.HIGH;
    private NotificationUseCase notificationUseCase;
    private Integer mSiteId;
    private Double mLat;
    private Double mLon;

    public SignOnJob(NotificationUseCase notificationUseCase) {
        // Require network connectivity and should be persisted in case the application exits before
        // the job is completed.
        super(new Params(PRIORITY).requireNetwork().persist().addTags(LOG).singleInstanceBy(LOG));
        this.notificationUseCase = notificationUseCase;

    }

    public SignOnJob setParameters(String tag, Integer siteId, Double lat, Double lon){
        mSiteId = siteId;
        mLat = lat;
        mLon = lon;
        Log.i(LOG, "SignOnJob created with Tag: " + tag);
        return this;
    }

    @Override
    public void onAdded() {
        // Job has been saved to disk
        // This is a good place to dispatch a UI event to indicate the job will eventually run.
        SLog.i(LOG, "Job has been added to disk - will run when network is available");
    }

    @Override
    public void onRun() throws Throwable {
        SLog.i(LOG, "Job is running");
        new SOSAPI(getApplicationContext())
                .signOnSite(true, mSiteId, mLat, mLon, new SOSAPI.VolleySuccessCallback() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(LOG, "Response from " + Constants.URL_SIGN_ON + ": " + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);

                            JSONObject jsonData = new JSONObject();
                            jsonData.put(Constants.USER_EMAIL, new SessionManager(getApplicationContext()).getCurrentUser().get(Constants.USER_EMAIL));
                            jsonData.put("response", response);

                            if (jsonResponse.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                SLog.i(LOG, "User automatically signed onto site through background job");

                                // Fire event to let device know the user has signed on
                                EventBus.getDefault().post(new StatusEvent(Constants.EVENT_SIGN_ON));
                                Handler mainHandler = new Handler(Looper.getMainLooper());
                                Runnable runnable = () -> notificationUseCase.hasPrompt(false).setSiteId(mSiteId).isUserSignedOn(true).buildSiteNotifications();
                                mainHandler.post(runnable);
                            }
                        }
                        catch (JSONException e) {
                            SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                        }
                    }
                });

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        // An error occurred in onRun.
        // Return value determines whether this job should RETRY or cancel. You can further
        // specify a backoff strategy or change the job's priority. You can also apply the
        // delay to the whole group to preserve jobs' running order.
        return RetryConstraint.createExponentialBackoff(runCount, 1000);
    }

    @Override
    protected int getRetryLimit() {
        // Overriding this allows us to set our own RETRY limit. The default is 20.
        return 3;
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        // Job has exceeded RETRY attempts or shouldReRunOnThrowable() has decided to cancel.
    }
}
