package au.com.signonsitenew.realm;

import javax.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class SiteInduction extends RealmObject {

    private Integer id;
    @PrimaryKey
    private long userId;
    @Index
    private long siteId; // Site that the induction relates to
    private String state;
    private String type;
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "SiteInduction{" +
                "id=" + id +
                ", userId=" + userId +
                ", siteId=" + siteId +
                ", state='" + state + '\'' +
                ", type='" + type + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}
