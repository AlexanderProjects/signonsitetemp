package au.com.signonsitenew.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */
public class Company extends RealmObject {
    @PrimaryKey private long id;
    private String name;

    public long getCompanyId() {
        return id;
    }

    public void setCompanyId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
