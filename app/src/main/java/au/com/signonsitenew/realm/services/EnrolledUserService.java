package au.com.signonsitenew.realm.services;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.realm.EnrolledUser;
import au.com.signonsitenew.utilities.Constants;
import io.realm.Realm;

/**
 * Created by kcaldwell on 8/11/17.
 */

public class EnrolledUserService {

    private static final String TAG = EnrolledUserService.class.getSimpleName();

    private static final String USER_ID = "id";

    private Realm mRealm;

    public EnrolledUserService(Realm realm) {
        mRealm = realm;
    }

    public void createOrUpdateEnrolledUser(JSONObject person) {
        try {
            SiteInductionService inductionService = new SiteInductionService(mRealm);
            SiteSettingsService siteSettingsService = new SiteSettingsService(mRealm);
            long siteId = siteSettingsService.getSiteId();
            // Get the user's induction status
            long userId = person.getLong(Constants.JSON_VISITOR_ID);
            String inductionState;
            if (!person.isNull(Constants.JSON_INDUCTION)) {
                // User has an induction object
                JSONObject induction = person.getJSONObject(Constants.JSON_INDUCTION);
                int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                String inductionUpdated = state.getString(Constants.JSON_IND_STATE_AT);

                inductionService.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, inductionUpdated);
            }
            else {
                inductionState = Constants.DOC_INDUCTION_INCOMPLETE;
            }

            mRealm.beginTransaction();
            EnrolledUser enrolment = new EnrolledUser();
            enrolment.setId(userId);
            enrolment.setFirstName(person.getString(Constants.JSON_VISITOR_FIRST_NAME));
            enrolment.setLastName(person.getString(Constants.JSON_VISITOR_LAST_NAME));
            enrolment.setPhoneNumber(person.getString(Constants.JSON_VISITOR_PHONE));
            enrolment.setCompanyName(person.getString(Constants.JSON_VISITOR_COMPANY_NAME));
            enrolment.setInductionStatus(inductionState);
            mRealm.copyToRealmOrUpdate(enrolment);
            mRealm.commitTransaction();
        }
        catch (JSONException e) {
            Log.e(TAG, "JSONException Occurred: " + e.getMessage());
        }
    }

    public void deleteAllEnrolledUsers() {
        mRealm.beginTransaction();
        mRealm.where(EnrolledUser.class).findAll().deleteAllFromRealm();
        mRealm.commitTransaction();
    }

    public void deleteEnrolledUser(long userId) {
        mRealm.beginTransaction();
        EnrolledUser user = mRealm.where(EnrolledUser.class).equalTo(USER_ID, userId).findFirst();
        if (user != null) {
            user.deleteFromRealm();
        }
        mRealm.commitTransaction();
    }
}
