package au.com.signonsitenew.realm.services;

import au.com.signonsitenew.realm.User;
import io.realm.Realm;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Company to check and set roles for users.
 */
public class UserService {
    public static final String LOG = UserService.class.getSimpleName();

    private Realm mRealm;

    public UserService(Realm realm) {
       mRealm = realm;
    }

    public void createUser(long userId,
                           String firstName,
                           String lastName,
                           String email,
                           Long companyId,
                           String companyName,
                           String phoneNumber,
                           String authToken) {
        mRealm.beginTransaction();
        mRealm.where(User.class).findAll().deleteAllFromRealm();
        User user = new User();
        user.setId(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setCompanyId(companyId);
        user.setCompanyName(companyName);
        user.setPhoneNumber(phoneNumber);
        user.setAuthToken(authToken);
        user.setNotificationsToken(null); // Have to contact Google about this separately
        mRealm.copyToRealmOrUpdate(user);
        mRealm.commitTransaction();
    }

    public long getCurrentUserId() {
        User user = mRealm.where(User.class).findFirst();
        if (user == null) {
            return 0;
        }
        return user.getId();
    }

    public void updateAuthToken(String token) {
        User user = mRealm.where(User.class).findFirst();

        if (user == null) {
            return;
        }

        mRealm.beginTransaction();
        user.setAuthToken(token);
        mRealm.commitTransaction();
    }

    public void updateNotificationToken(String token) {
        User user = mRealm.where(User.class).findFirst();
        mRealm.beginTransaction();
        user.setAuthToken(token);
        mRealm.commitTransaction();
    }

}
