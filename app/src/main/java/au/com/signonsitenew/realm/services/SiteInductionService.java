package au.com.signonsitenew.realm.services;

import android.util.Log;

import au.com.signonsitenew.realm.SiteInduction;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by SignOnSite on 1/09/17.
 *
 * Class of helper methods to store data in Realm without repeating code everywhere...
 */

public class SiteInductionService {

    private static final String LOG = SiteInductionService.class.getSimpleName();
    private static final String SITE_ID = "siteId";
    private static final String USER_ID = "userId";

    private Realm mRealm;

    public SiteInductionService(Realm realm) {
        mRealm = realm;
    }

    public void createOrUpdateInduction(Integer id,
                                        long userId,
                                        long siteId,
                                        String type,
                                        String state,
                                        String updatedAt) {
        // First check if this induction already exists
        SiteInduction existingInduction = mRealm.where(SiteInduction.class)
                                                        .equalTo(SITE_ID, siteId)
                                                        .equalTo(USER_ID, userId)
                                                        .findFirst();
        if (existingInduction != null) {
            // Remove induction
            mRealm.beginTransaction();
            existingInduction.deleteFromRealm();
            mRealm.commitTransaction();
        }

        Log.i(LOG, "Storing new induction");
        // New induction, store
        mRealm.beginTransaction();
        SiteInduction induction = new SiteInduction();
        induction.setId(id);
        induction.setUserId(userId);
        induction.setSiteId(siteId);
        induction.setState(state);
        induction.setType(type);
        induction.setUpdatedAt(updatedAt);
        mRealm.copyToRealmOrUpdate(induction);
        mRealm.commitTransaction();
    }

    public SiteInduction getSiteInductionForUser(long userId) {
        SiteSettingsService settingsService = new SiteSettingsService(mRealm);
        Long siteId = settingsService.getSiteId();
        if (siteId == null || siteId == 0) {
            return null;
        }

        if (userHasInductionOnSite(userId, siteId)) {
            return mRealm.where(SiteInduction.class)
                    .equalTo(USER_ID, userId)
                    .equalTo(SITE_ID, siteId)
                    .findFirst();
        }
        else {
            return null;
        }
    }

    private boolean userHasInductionOnSite(long userId, long siteId) {
        SiteInduction induction = mRealm.where(SiteInduction.class)
                                        .equalTo(USER_ID, userId)
                                        .equalTo(SITE_ID, siteId)
                                        .findFirst();
        return induction != null;
    }

    public void deleteInductionForUserOnSite(long userId, long siteId) {
       SiteInduction induction = mRealm.where(SiteInduction.class)
                                                        .equalTo(USER_ID, userId)
                                                        .equalTo(SITE_ID, siteId)
                                                        .findFirst();

        if (induction != null) {
            mRealm.beginTransaction();
            induction.deleteFromRealm();
            mRealm.commitTransaction();
        }
    }

    public void deleteInductionsForSite(long siteId) {
        RealmResults<SiteInduction> siteInductions = mRealm.where(SiteInduction.class).equalTo(SITE_ID, siteId).findAll();
        if (siteInductions.size() < 1) {
            return;
        }

        mRealm.beginTransaction();
        siteInductions.deleteAllFromRealm();
        mRealm.commitTransaction();
    }

}
