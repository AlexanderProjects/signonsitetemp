package au.com.signonsitenew.realm.services;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.realm.SiteSettings;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;

/**
 * Created by Krishan Caldwell on 29/06/2016.
 *
 * Company to check and set roles for users.
 */
public class SiteSettingsService {
    public static final String TAG = SiteSettingsService.class.getSimpleName();

    private Realm mRealm;

    public SiteSettingsService(Realm realm) {
       mRealm = realm;
    }

    public void createSiteSettings(JSONObject jsonData) {
        boolean inductionsEnabled;
        boolean manualCompanyRegistrationRequired;

        try {
            // Clear any existing settings
            mRealm.beginTransaction();
            mRealm.where(SiteSettings.class).findAll().deleteAllFromRealm();
            mRealm.commitTransaction();
            inductionsEnabled = jsonData.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE);
            manualCompanyRegistrationRequired = jsonData.getBoolean(Constants.JSON_MAN_COMPANY_REGISTRATION);

            Long siteId = jsonData.getLong(Constants.JSON_SITE_ID);
            String timeZone = jsonData.getString(Constants.JSON_SITE_TIMEZONE);

            mRealm.beginTransaction();
            SiteSettings settings = new SiteSettings();
            settings.setSiteId(siteId);
            settings.setInductionsEnabled(inductionsEnabled);
            settings.setManualCompanyRegistrationEnabled(manualCompanyRegistrationRequired);
            settings.setSiteTimeZone(timeZone);
            mRealm.copyToRealm(settings);
            mRealm.commitTransaction();
        }
        catch (JSONException e) {
            SLog.e(TAG, "JSONException Occurred. " + e.getMessage());
        }
    }

    public boolean siteInductionsEnabled() {
        SiteSettings settings = mRealm.where(SiteSettings.class).findFirst();
        return settings != null && settings.isInductionsEnabled();
    }

    public Long getSiteId() {
        SiteSettings settings = mRealm.where(SiteSettings.class).findFirst();

        if (settings == null) {
            return 0L;
        }
        else {
            return settings.getSiteId();
        }
    }

    public boolean companyRegistrationRequired() {
        SiteSettings settings = mRealm.where(SiteSettings.class).findFirst();

        return settings != null && settings.manualCompanyRegistrationEnabled();
    }

    public void setCompanyRegistrationCompleted() {
        SiteSettings settings = mRealm.where(SiteSettings.class).findFirst();
        if (settings == null) {
            return;
        }
        mRealm.beginTransaction();
        settings.setManualCompanyRegistrationEnabled(false);
        mRealm.commitTransaction();
    }

}
