package au.com.signonsitenew.realm;



import androidx.annotation.Nullable;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Model for storing event logs. Each model contains a:
 *   category - the part of the app that the log relates to.
 *   tag - the event specific to the log.
 *   latitude - the user's GPS latitude at the time the log was recorded.
 *   longitude - the user's GPS longitude at the time the log was recorded.
 *   accuracy - the user's GPS accuracy at the time the log was recorded. This will vary significantly
 *              depending on if the user's location was through a region event or from a GPS reading.
 *   jsonData - additional JSON data specific to the event that we wish to store on our server, such
 *              as siteID.
 *   timestamp - as logs will be stored and sent in batches from the device to save power, the
 *               timestamp of each log, particularly with respect to location, becomes important.
 */
public class DiagnosticLog extends RealmObject {
    private String tag;
    private double latitude;
    private double longitude;
    private double accuracy;
    private String userEmail;
    private float batteryLevel;
    private boolean networkAvailable;
    @Nullable
    private String jsonData;
    private Date timestamp;

    public String getTag() {
        return tag;
    }

    public void setTag(DiagnosticLog.Tag tag) {
        this.tag = tag.toString();
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public float getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(float batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public boolean getNetworkAvailable() {
        return networkAvailable;
    }

    public void setNetworkAvailable(boolean networkAvailable) {
        this.networkAvailable = networkAvailable;
    }

    @Nullable
    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(@Nullable String jsonData) {
        this.jsonData = jsonData;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        if (jsonData == null) {
            return "Tag: " + getTag() + ", latitude: " + latitude + ", longitude: " + longitude +
                    ", accuracy: " + accuracy + ", timestamp: " + timestamp.toString();
        }
        else {
            return "Tag: " + getTag() + ", latitude: " + latitude + ", longitude: " + longitude +
                    ", accuracy: " + accuracy + ", timestamp: " + timestamp.toString() + ", jsonData: " + jsonData;
        }
    }

    public enum Category {
        LOCATION_ENGINE,
        ERROR,
        REGION_EVENT,
        NOTIFICATION,
        SIGN_ON,
        SIGN_OFF,
        LOCATION_INFO,
        SUPPORT,
        NOTIFICATIONS,
        SITE_EMERGENCY;
    }

    public enum Tag {
        ALPHA_2_ERROR,
        BATT_OPT_IGNORE_REQUEST,
        DND_OVERRIDE,
        FORGOT_PASSWORD,
        GOT_NOTIFICATION,
        LOC_REGISTER,
        LOC_MANUAL_ON,
        LOC_MANUAL_OFF,
        LOC_AUTO_ON,
        LOC_AUTO_OFF,
        LOC_MANUAL_ON_ERR,
        LOC_MANUAL_OFF_ERR,
        LOC_AUTO_ON_ERR,
        LOC_AUTO_OFF_ERR,
        LOC_REGION,
        LOC_ENTRY,
        LOC_EXIT,
        LOC_POLLING,
        LOC_POLL_NO_ACTION,
        LOC_PERMISSION_REQUEST,
        LOC_NO_ACCESS,
        LOGOUT,
        NETWORK_ERROR,
        NETWORK_FAIL_RESPONSE,
        NOTIFICATION_REGISTRATION,
        REFETCH_TRIGGER,
        SITE_LIST_RESULT,
        VIS_REG_SEARCH,
        VIS_REG_ACTION,
        VIS_REG_USER;
    }
}
