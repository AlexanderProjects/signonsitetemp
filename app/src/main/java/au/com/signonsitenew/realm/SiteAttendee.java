package au.com.signonsitenew.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class SiteAttendee extends RealmObject {
    @PrimaryKey private Long userId;
    private String firstName;
    private String lastName;
    private String company;
    private String phoneNumber;
    private String inductionStatus;
    private Boolean enrolmentState;
    private String checkInTime;
    private boolean markedSafe;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getInductionStatus() {
        return inductionStatus;
    }

    public void setInductionStatus(String inductionStatus) {
        this.inductionStatus = inductionStatus;
    }

    public Boolean getEnrolmentState() {
        return enrolmentState;
    }

    public void setEnrolmentState(Boolean enrolmentState) {
        this.enrolmentState = enrolmentState;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public boolean isMarkedSafe() {
        return markedSafe;
    }

    public void setMarkedSafe(boolean markedSafe) {
        this.markedSafe = markedSafe;
    }

    public String getName() {
        return firstName + " " + lastName;
    }
}
