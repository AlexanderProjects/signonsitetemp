package au.com.signonsitenew.realm.services;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.models.Site;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.Briefing;
import au.com.signonsitenew.realm.EnrolledUser;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.utilities.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by krishan on 25/11/17.
 */

public class SiteAttendeeService {
    private static final String LOG = SiteAttendeeService.class.getSimpleName();

    private static final String SITE_ID = "siteId";
    private static final String USER_ID = "userId";

    private Realm mRealm;

    public SiteAttendeeService(Realm realm) {
        mRealm = realm;
    }

    public void createOrUpdateSiteAttendee(JSONObject person) {
        try {
            SiteInductionService inductionService = new SiteInductionService(mRealm);
            SiteSettingsService siteSettingsService = new SiteSettingsService(mRealm);
            long siteId = siteSettingsService.getSiteId();
            // Get the user's induction status
            long userId = person.getLong(Constants.JSON_VISITOR_ID);
            String inductionState;
            if (!person.isNull(Constants.JSON_INDUCTION)) {
                // User has an induction object
                JSONObject induction = person.getJSONObject(Constants.JSON_INDUCTION);
                int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                String inductionUpdated = state.getString(Constants.JSON_IND_STATE_AT);

                inductionService.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, inductionUpdated);
            }
            else {
                inductionState = Constants.DOC_INDUCTION_INCOMPLETE;
            }

            mRealm.beginTransaction();
            SiteAttendee attendee = new SiteAttendee();
            attendee.setUserId(person.getLong(Constants.JSON_VISITOR_ID));
            attendee.setFirstName(person.getString(Constants.JSON_VISITOR_FIRST_NAME));
            attendee.setLastName(person.getString(Constants.JSON_VISITOR_LAST_NAME));
            attendee.setPhoneNumber(person.getString(Constants.JSON_VISITOR_PHONE));
            attendee.setCompany(person.getString(Constants.JSON_VISITOR_COMPANY_NAME));
            attendee.setEnrolmentState(person.getBoolean(Constants.JSON_VISITOR_HAS_ACTIVE_ENROLMENT));
            attendee.setInductionStatus(inductionState);
            attendee.setCheckInTime(null);
            attendee.setMarkedSafe(false);
            mRealm.copyToRealmOrUpdate(attendee);
            mRealm.commitTransaction();
        }
        catch (JSONException e) {
            Log.e(LOG, "JSONException Occurred: " + e.getMessage());
        }
    }

    public SiteAttendee getSiteAttendee(long userId) {
        return mRealm.where(SiteAttendee.class)
                                    .equalTo(USER_ID, userId)
                                    .findFirst();
    }

    public boolean userOnSite(long userId) {
        SiteAttendee attendee = getSiteAttendee(userId);

        return attendee != null;
    }

    public void deactivateEnrolment(long userId) {
        SiteSettingsService settingsService = new SiteSettingsService(mRealm);
        long siteId = settingsService.getSiteId();

        SiteAttendee attendee = mRealm.where(SiteAttendee.class)
                                    .equalTo(USER_ID, userId)
                                    .findFirst();

        if (attendee != null) {
            mRealm.beginTransaction();
            attendee.setEnrolmentState(false);
            mRealm.commitTransaction();

            // Also remove induction if they have one
            SiteInductionService inductionService = new SiteInductionService(mRealm);
            inductionService.deleteInductionForUserOnSite(userId, siteId);
        }
    }

    public void deleteAllSiteAttendees() {
        RealmResults attendees = mRealm.where(SiteAttendee.class).findAll();
        if (attendees.size() < 1) {
            return;
        }
        mRealm.beginTransaction();
        attendees.deleteAllFromRealm();
        mRealm.commitTransaction();
    }
}
