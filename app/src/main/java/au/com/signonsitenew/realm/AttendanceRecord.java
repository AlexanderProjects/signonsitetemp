package au.com.signonsitenew.realm;


import androidx.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Krishan Caldwell on 9/08/2016.
 */
public class AttendanceRecord extends RealmObject {
    @PrimaryKey private Long id;
    @Index private Long userId;
    private Long siteId;
    private String checkInTime;
    @Nullable
    private String checkOutTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    @Nullable
    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(@Nullable String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }
}
