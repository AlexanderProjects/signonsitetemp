package au.com.signonsitenew.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Krishan Caldwell on 19/08/2016.
 */
public class EnrolledUser extends RealmObject {
    @PrimaryKey private Long id;
    private String firstName;
    private String lastName;
    private String companyName;
    private String inductionStatus;
    private String phoneNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInductionStatus() {
        return inductionStatus;
    }

    public void setInductionStatus(String inductionStatus) {
        this.inductionStatus = inductionStatus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "EnrolledUser{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", inductionStatus='" + inductionStatus + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
