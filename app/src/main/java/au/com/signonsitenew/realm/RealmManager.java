package au.com.signonsitenew.realm;

import android.util.Log;

import au.com.signonsitenew.utilities.Constants;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.FieldAttribute;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * A class for managing Realm Schema Versions and Migrations.
 *
 * To add a new Realm class:
 * First, create the class and add fields. Then, in the increment Constants.REALM_SCHEMA_VERSION.
 * Once this is done, write the Realm migration below in the migration function. You should read
 * the Realm documentation to get an understanding of how Realm operates.
 *
 * As part of DRY (Don't Repeat Yourself), and making sure that it's easier to keep track of if
 * Realm instances have been left open, it is recommended that all Realm operations be performed
 * through a Realm service in the au.com.signonsitenew.realm.services package.
 */
public class RealmManager {
    private static final String TAG = "Migration";

    // Set up migrations in case they are needed
    private static RealmMigration realmMigration = new RealmMigration() {
        @Override
        public void migrate(final DynamicRealm realm, long oldVersion, long newVersion) {
            // DynamicRealm exposes an editable schema
            RealmSchema schema = realm.getSchema();

            // Migrate from version 1 to 2: Add Visits class
            if (oldVersion == 1) {
                schema.create("Visit")
                        .addField("visitId", int.class, FieldAttribute.PRIMARY_KEY)
                        .addField("userId", int.class)
                        .addField("siteId", int.class)
                        .addField("checkInTime", String.class)
                        .addField("checkOutTime", String.class);
                if (schema.contains("Site")) {
                    schema.rename("Site", "EnrolledSite");
                }
                oldVersion++;
            }

            // Migrate from version 2 to 3: Add EnrolledUsers class
            if (oldVersion == 2) {
                schema.create("EnrolledUser")
                        .addField("userId", long.class)
                        .addField("firstName", String.class)
                        .addField("lastName", String.class)
                        .addField("phoneNumber", String.class)
                        .addField("companyName", String.class);
                oldVersion++;
            }

            if (oldVersion == 3) {
                // Change name of Site to Associated Site
                if (schema.contains("Site")) {
                    schema.rename("Site", "EnrolledSite");
                }

                // Removed unused Configuration Class if it exists
                if (schema.contains("Configuration")) {
                    schema.remove("Configuration");
                }

                // Add Credential Class if it does not exist
                if (!schema.contains("Credential")) {
                    schema.create("Credential")
                            .addField("id", long.class, FieldAttribute.PRIMARY_KEY)
                            .addField("userId", Long.class)
                            .addField("email", String.class)
                            .addField("credentialType", String.class)
                            .addField("frontPhotoUri", String.class)
                            .addField("backPhotoUri", String.class)
                            .addField("whitecardNumber", String.class)
                            .addField("whitecardIssueDate", String.class)
                            .addField("whitecardState", String.class);
                }

                // Remove CredentialField Class if it exists
                if (schema.contains("CredentialField")) {
                    schema.remove("CredentialField");
                }

                // Add DiagnosticTag if it does not exist
                if (!schema.contains("DiagnosticTag")) {
                    schema.create("DiagnosticTag")
                            .addField("tagType", String.class);
                }

                // Remove Feature Table if it exists
                if (schema.contains("Feature")) {
                    schema.remove("Feature");
                }

                // Add Role table if it does not exist
                if (!schema.contains("Role")) {
                    schema.create("Role")
                            .addField("userId", String.class, FieldAttribute.PRIMARY_KEY)
                            .addField("role", int.class)
                            .addField("siteId", Integer.class);
                }
                else {
                    // If it already has a role, change the siteId int to Integer
                    if (schema.get("Role").hasField("siteId")) {
                        schema.get("Role")
                                .addField("siteIdTemp", Integer.class)
                                .removeField("siteId")
                                .renameField("siteIdTemp", "siteId");
                    }

                    // Remove no longer used roleName field
                    if (schema.get("Role").hasField("roleName")) {
                        schema.get("Role").removeField("roleName");
                    }

                    if (!schema.get("Role").hasField("userId")) {
                        schema.get("Role")
                                .addField("userId", int.class, FieldAttribute.PRIMARY_KEY);
                    }
                }

                // Add SiteVisitor if it does not exist
                if (!schema.contains("SiteVisitor")) {
                    schema.create("SiteVisitor")
                            .addField("userId", int.class, FieldAttribute.PRIMARY_KEY)
                            .addField("firstName", String.class)
                            .addField("lastName", String.class)
                            .addField("company", String.class)
                            .addField("phoneNumber", String.class)
                            .addField("checkInTime", String.class)
                            .addField("markedSafe", boolean.class);
                }

                // Add User if it does not exist
                if (!schema.contains("User")) {
                    schema.create("User")
                            .addField("id", Integer.class, FieldAttribute.PRIMARY_KEY)
                            .addField("firstName", String.class)
                            .addField("lastName", String.class)
                            .addField("email", String.class)
                            .addField("companyId", Integer.class)
                            .addField("companyName", String.class)
                            .addField("phoneNumber", String.class);
                }

                oldVersion++;
            }

            if (oldVersion == 4) {
                if (schema.get("User").hasPrimaryKey()) {
                    schema.get("User")
                            .removePrimaryKey();
                }
                schema.remove("User");
                schema.create("User")
                        .addField("id", Integer.class, FieldAttribute.PRIMARY_KEY)
                        .addField("firstName", String.class)
                        .addField("lastName", String.class)
                        .addField("email", String.class)
                        .addField("companyId", Integer.class)
                        .addField("companyName", String.class)
                        .addField("phoneNumber", String.class);

                oldVersion++;
            }

            if (oldVersion == 5) {
                schema.get("Role")
                        .removePrimaryKey();

                oldVersion++;
            }

            if (oldVersion == 6) {
                schema.create("ManagedCompany")
                        .addField("companyName", String.class);

                oldVersion++;
            }
            /***************************************************************************************
             * 16/8/2017
             * To rectify potential migration issues that users of early versions in Realm may
             * encounter. Some types were incorrectly set to boxed types instead of primitives,
             * others were also never set explicitly to be indexed, or nullable. This was done only
             * by model annotations previously, but are now enforced as of Realm 3.7.1
             *
             * Used this as a chance to re-visit some models.
             **************************************************************************************/
            if (oldVersion == 7) {
                // Rename EnrolledUser.userId to EnrolledUser.id, make field type boxed and a PK
                schema.get("EnrolledUser")
                        .addField("idTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Long oldId = obj.getLong("userId");
                                obj.setLong("idTmp", oldId);
                            }
                        })
                        .removeField("userId")
                        .renameField("idTmp", "id")
                        .addPrimaryKey("id");

                // Changed AssociatedSite to EnrolledSite
                if (schema.contains("AssociatedSite")) {
                    schema.rename("AssociatedSite", "EnrolledSite");
                }

                if (schema.contains("EnrolledSite")) {
                    schema.get("EnrolledSite")
                            .addField("idTmp", Long.class)
                            .addField("inductedTmp", Boolean.class)
                            .transform(new RealmObjectSchema.Function() {
                                @Override
                                public void apply(DynamicRealmObject obj) {
                                    Integer oldId = obj.getInt("id");
                                    Boolean oldInd = obj.getBoolean("inducted");
                                    obj.setLong("idTmp", oldId);
                                    obj.setBoolean("inductedTmp", oldInd);
                                }
                            })
                            .removeField("id")
                            .renameField("idTmp", "id")
                            .addPrimaryKey("id")
                            .removeField("inducted")
                            .renameField("inductedTmp", "inducted");
                }

                // Specifically set Role.siteId as Nullable.
                schema.get("Role")
                        .addField("siteIdTmp", Long.class)
                        .addField("userIdTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Integer oldSiteId = obj.getInt("siteId");
                                int oldUserId = obj.getInt("userId");
                                obj.setLong("siteIdTmp", Long.valueOf(oldSiteId));
                                obj.setLong("userIdTmp", oldUserId);
                            }
                        })
                        .removeField("siteId")
                        .renameField("siteIdTmp", "siteId")
                        .removeField("userId")
                        .renameField("userIdTmp", "userId");

                // Fix up properties in User
                schema.get("User")
                        .addField("idTmp", Long.class)
                        .addField("companyIdTmp", Integer.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Integer oldId = obj.getInt("id");
                                Integer oldCompId = obj.getInt("companyId");
                                obj.setLong("idTmp", Long.valueOf(oldId));
                                obj.setInt("companyIdTmp", oldCompId);
                            }
                        })
                        .removeField("id")
                        .renameField("idTmp", "id")
                        .addPrimaryKey("id")
                        .removeField("companyId")
                        .renameField("companyIdTmp", "companyId");

                if (!schema.get("User").isNullable("companyId")) {
                    schema.get("User").setNullable("companyId", true);
                }

                // Change SiteVisitor.id to be of type Long
                schema.get("SiteVisitor")
                        .addField("idTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Integer tempId = obj.getInt("userId");
                                obj.setLong("idTmp", Long.valueOf(tempId));
                            }
                        })
                        .removeField("userId")
                        .renameField("idTmp", "userId")
                        .addPrimaryKey("userId");

                schema.get("Visit")
                        .addField("idTmp", Long.class)
                        .addField("userTmp", Long.class)
                        .addField("siteTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                int oldId = obj.getInt("visitId");
                                int oldUserId = obj.getInt("userId");
                                int oldSiteId = obj.getInt("siteId");
                                obj.setLong("idTmp", oldId);
                                obj.setLong("userTmp", oldUserId);
                                obj.setLong("siteTmp", oldSiteId);
                            }
                        })
                        .removeField("visitId")
                        .renameField("idTmp", "id")
                        .addPrimaryKey("id")
                        .removeField("userId")
                        .renameField("userTmp", "userId")
                        .removeField("siteId")
                        .renameField("siteTmp", "siteId");

                schema.get("Credential")
                        .addField("idTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                long oldId = obj.getLong("id");
                                obj.setLong("idTmp", oldId);
                            }
                        })
                        .removeField("id")
                        .renameField("idTmp", "id")
                        .addPrimaryKey("id");

                oldVersion++;
            }

            if (oldVersion == 8) {
                // Rename EnrolledUser.userId to EnrolledUser.id, make field type boxed and a PK
                schema.get("User")
                        .addField("companyIdTmp", Long.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Integer oldCompId = obj.getInt("companyId");
                                obj.setLong("companyIdTmp", oldCompId);
                            }
                        })
                        .removeField("companyId")
                        .renameField("companyIdTmp", "companyId");

                if (!schema.get("User").isNullable("companyId")) {
                    schema.get("User").setNullable("companyId", true);
                }

                oldVersion++;
            }

            /***************************************************************************************
             * 16/8/2017
             * Added the Briefing and SiteInduction models to the Realm Schema.
             **************************************************************************************/
            if (oldVersion == 9) {
                Log.i(TAG, oldVersion + "");
                schema.create("Briefing")
                        .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                        .addField("acknowledged", boolean.class)
                        .addField("siteId", long.class)
                        .addField("briefingText", String.class)
                        .addField("author", String.class)
                        .addField("createdAt", String.class);

                schema.create("SiteInduction")
                        .addField("id", int.class)
                        .addField("userId", long.class)
                        .addField("siteId", long.class, FieldAttribute.INDEXED)
                        .addField("state", String.class)
                        .addField("type", String.class)
                        .addField("updatedAt", String.class);

                oldVersion++;
            }

            /***************************************************************************************
             * 20/10/2017
             * Added an SiteInduction state to Site Visitors
             **************************************************************************************/
            if (oldVersion == 10) {
                Log.i(TAG, oldVersion + "");
                schema.get("SiteVisitor")
                        .addField("inductionStatus", String.class);

                oldVersion++;
            }

            /***************************************************************************************
             * 20/10/2017
             * Added an SiteInduction state to Enrolled Users
             **************************************************************************************/
            if (oldVersion == 11) {
                Log.i(TAG, oldVersion + "");
                schema.get("EnrolledUser")
                        .addField("inductionStatus", String.class);

                oldVersion++;
            }

            /***************************************************************************************
             * 9/11/2017
             * Changed SiteVisitor to SiteAttendee, and Visit to AttendanceRecord
             **************************************************************************************/
            if (oldVersion == 12) {
                Log.i(TAG, oldVersion + "");
                schema.rename("SiteVisitor", "SiteAttendee");
                schema.rename("Visit", "AttendanceRecord");

                oldVersion++;
            }

            /***************************************************************************************
             * 25/11/2017
             * Changed Role to ManagerSettings
             * Added the SiteSettings class
             * Added JWT to User class
             **************************************************************************************/
            if (oldVersion == 13) {
                Log.i(TAG, oldVersion + "");
                schema.rename("Role", "ManagerSettings");

                schema.create("SiteSettings")
                        .addField("siteId", Long.class)
                        .addField("inductionsEnabled", boolean.class);

                schema.get("User")
                        .addField("authToken", String.class)
                        .addField("notificationsToken", String.class);

                oldVersion++;
            }

            /***************************************************************************************
             * 29/11/2017
             * Made userId a PrimaryKey, and made id optional in the SiteInduction model
             * Added
             **************************************************************************************/
            if (oldVersion == 14) {
                schema.get("SiteInduction")
                        .addPrimaryKey("userId")
                        .addField("idTmp", Integer.class)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Integer id = obj.getInt("id");
                                obj.setLong("idTmp", id);
                            }
                        })
                        .removeField("id")
                        .renameField("idTmp", "id");

                oldVersion++;
            }

            /***************************************************************************************
             * 29/11/2017
             * Added an index to userId in the AttendanceRecord model
             * Added enrolmentState to the SiteAttendee model
             **************************************************************************************/
            if (oldVersion == 15) {
                Log.i(TAG, oldVersion + "");
                schema.get("AttendanceRecord")
                        .addField("userIdTmp", Long.class, FieldAttribute.INDEXED)
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                Long oldUserId = obj.getLong("userId");
                                obj.setLong("userIdTmp", oldUserId);
                            }
                        })
                        .removeField("userId")
                        .renameField("userIdTmp", "userId");

                schema.get("SiteAttendee")
                        .addField("enrolmentState", Boolean.class);

                oldVersion++;
            }
            /***************************************************************************************
             * 22/05/2018
             * Create new class called UserAbilities
             * Set default values to false
             * Migrate/Map old values if they exist
             * Remove ManagerSettings class
             **************************************************************************************/
            if (oldVersion == 16) {
                Log.i(TAG, oldVersion + "");
                schema.create("UserAbilities")
                        .addField("userId", long.class)
                        .addField("siteId", long.class)
                        .addField("evacuate", boolean.class)
                        .addField("visitorRegister", boolean.class)
                        .addField("manageUserInductions", boolean.class);

                schema.get("ManagerSettings")
                        .transform(new RealmObjectSchema.Function() {
                            @Override
                            public void apply(DynamicRealmObject obj) {
                                if (!obj.isNull("role")) {
                                    int role = obj.getInt("role");
                                    Long userId = obj.getLong("userId");
                                    Long siteId = obj.getLong("siteId");
                                    switch (role) {
                                        case 1: // Site Manager
                                            DynamicRealmObject managerAbilities = realm.createObject("UserAbilities");
                                            managerAbilities.setLong("userId", userId);
                                            managerAbilities.setLong("siteId", siteId);
                                            managerAbilities.setBoolean("evacuate", true);
                                            managerAbilities.setBoolean("visitorRegister", true);
                                            managerAbilities.setBoolean("manageUserInductions", true);
                                            break;
                                        case 2: // Site Supervisor
                                            DynamicRealmObject supervisorAbilities = realm.createObject("UserAbilities");
                                            supervisorAbilities.setLong("userId", userId);
                                            supervisorAbilities.setLong("siteId", siteId);
                                            supervisorAbilities.setBoolean("evacuate", false);
                                            supervisorAbilities.setBoolean("visitorRegister", true);
                                            supervisorAbilities.setBoolean("manageUserInductions", false);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        });

                schema.remove("ManagerSettings");

                oldVersion++;
            }
            /**
             * 23/10/2018
             * Update SiteSettings to include a flag for new site registration being enabled.
             */
            if (oldVersion == 17) {
                schema.get("SiteSettings")
                        .addField("manualCompanyRegistration", boolean.class);

                oldVersion++;
            }
            /**
             * 1/11/2018
             * Add a new Realm Object called Company
             */
            if (oldVersion == 18) {
                schema.create("Company")
                        .addField("id", long.class, FieldAttribute.PRIMARY_KEY)
                        .addField("name", String.class);

                oldVersion++;
            }
            /**
             * 28/11/2018
             * Add field to UserAbilities to store whether a user can update briefings or not
             */
            if (oldVersion == 19) {
                schema.get("UserAbilities")
                        .addField("canUpdateBriefing", Boolean.class);

                oldVersion++;
            }
            /**
             * 30/11/2020
             * Add field to SiteSettings to store site time zone
             */
            if(oldVersion == 20){
                schema.get("SiteSettings")
                        .addField("siteTimeZone", String.class);
                oldVersion++;
            }
            /**
             * 15/07/2021
             * Add field to UserAbilities to store whether a user can manage permits
             */
            if (oldVersion == 21) {
                schema.get("UserAbilities")
                        .addField("hasManagerUserPermitPermission", Boolean.class);

                oldVersion++;

                schema.get("UserAbilities").transform(obj -> {
                    if(obj.isNull("hasManagerUserPermitPermission")) {
                        DynamicRealmObject abilities = realm.createObject("UserAbilities");
                        abilities.setBoolean("hasManagerUserPermitPermission", false);
                    }
                });
            }
        }
    };

    /**
     * Fetches the Realm Configuration and handles migrations as is necessary.
     * Currently the models store no data that is necessary to retrieve between versions, so deleting
     * the Realm is fine.
     * @return a realmConfiguration object.
     */
    public static RealmConfiguration getRealmConfiguration() {

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Constants.REALM_CONFIG_NAME)
                .schemaVersion(Constants.REALM_SCHEMA_VERSION)
                .migration(realmMigration)
                .build();

        return realmConfiguration;
    }

}
