package au.com.signonsitenew.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright © SignOnSite. All rights reserved.
 */
public class SiteSettings extends RealmObject {

    private Long siteId;
    private boolean inductionsEnabled;
    private boolean manualCompanyRegistration;
    private String siteTimeZone;

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public boolean isInductionsEnabled() {
        return inductionsEnabled;
    }

    public void setInductionsEnabled(boolean inductionsEnabled) {
        this.inductionsEnabled = inductionsEnabled;
    }

    public void setSiteTimeZone(String siteTimeZone){
        this.siteTimeZone = siteTimeZone;
    }

    public String getSiteTimeZone(){ return  siteTimeZone; }

    public boolean manualCompanyRegistrationEnabled() {
        return manualCompanyRegistration;
    }

    public void setManualCompanyRegistrationEnabled(boolean manualCompanyRegistration) {
        this.manualCompanyRegistration = manualCompanyRegistration;
    }

    @Override
    public String toString() {
        return "SiteSettings{" +
                "siteId=" + siteId +
                ", inductionsEnabled=" + inductionsEnabled +
                ", manualCompanyRegistration=" + manualCompanyRegistration +
                '}';
    }
}
