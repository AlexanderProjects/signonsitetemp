package au.com.signonsitenew.realm.services;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import au.com.signonsitenew.events.BriefingEvent;
import au.com.signonsitenew.realm.Briefing;
import au.com.signonsitenew.utilities.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by SignOnSite on 1/09/17.
 *
 * Class of helper methods to store data in Realm without repeating code everywhere...
 */

public class BriefingService {

    private static final String LOG = BriefingService.class.getSimpleName();

    // Field Constants
    private static final String BRIEFING_ID = "id";
    private static final String BRIEFING_SITE_ID = "siteId";

    private Realm mRealm;

    public BriefingService(Realm realm) {
        mRealm = realm;
    }

    public void storeBriefing(int briefingId,
                              int siteId,
                              String briefingText,
                              String author,
                              String briefingDate,
                              boolean needsAcknowledgement) {
        if (mRealm == null) {
            return;
        }
        // First check if this briefing already exists
        Briefing realmBriefing = mRealm.where(Briefing.class).equalTo(BRIEFING_ID, briefingId).findFirst();
        if (realmBriefing != null) {
            Log.i(LOG, "Briefing already stored");
            // Briefing already stored, update acknowledgement in case it has changed
            mRealm.beginTransaction();
            realmBriefing.setAcknowledged(!needsAcknowledgement);
            mRealm.commitTransaction();
            return;
        }

        // Check if briefings of the same type are already on site and need to be replaced
        RealmResults<Briefing> briefings = mRealm
                .where(Briefing.class)
                .equalTo(BRIEFING_SITE_ID, siteId)
                .findAll();
        if (briefings.size() > 0) {
            // Remove briefings
            mRealm.beginTransaction();
            briefings.deleteAllFromRealm();
            mRealm.commitTransaction();
        }

        Log.i(LOG, "Storing new briefing");
        // New briefing, store
        mRealm.beginTransaction();
        Briefing briefing = new Briefing();
        briefing.setId(briefingId);
        briefing.setAcknowledged(!needsAcknowledgement);
        briefing.setSiteId(siteId);
        briefing.setBriefingText(briefingText);
        briefing.setAuthor(author);
        briefing.setCreatedAt(briefingDate);
        mRealm.copyToRealmOrUpdate(briefing);
        mRealm.commitTransaction();

        // Fire off an event and post notification to let app know new briefing is available
        EventBus.getDefault().post(new BriefingEvent(Constants.EVENT_NEW_BRIEFING));
    }

    public Briefing getBriefingForSite(long siteId) {
        Briefing briefing = mRealm.where(Briefing.class).equalTo(BRIEFING_SITE_ID, siteId).findFirst();
        mRealm.close();
        return briefing;
    }

    public String getBriefingContent(long briefingId) {
        Briefing briefing = mRealm.where(Briefing.class).equalTo(BRIEFING_ID, briefingId).findFirst();

        return briefing == null ? "" : briefing.getBriefingText();
    }

    public boolean getBriefingAcknowledged(int briefingId) {
        Briefing briefing = mRealm.where(Briefing.class).equalTo(BRIEFING_ID, briefingId).findFirst();
        return briefing == null || briefing.isAcknowledged();
    }

    public void setBriefingAcknowledged(int briefingId) {
        Briefing briefing = mRealm.where(Briefing.class).equalTo(BRIEFING_ID, briefingId).findFirst();

        if (briefing == null) {
            return;
        }

        mRealm.beginTransaction();
        briefing.setAcknowledged(true);
        mRealm.commitTransaction();
    }

    private void deleteBriefing(int briefingId) {
        Briefing briefing = mRealm.where(Briefing.class).equalTo(BRIEFING_ID, briefingId).findFirst();
        if (briefing == null) {
            return;
        }
        mRealm.beginTransaction();
        briefing.deleteFromRealm();
        mRealm.commitTransaction();
    }

    public void deleteBriefingsForSite(int siteId) {
        RealmResults briefings = mRealm.where(Briefing.class).equalTo(BRIEFING_SITE_ID, siteId).findAll();
        if (briefings.size() < 1) {
            return;
        }
        mRealm.beginTransaction();
        briefings.deleteAllFromRealm();
        mRealm.commitTransaction();
    }

    public void deleteAllBriefings() {
        RealmResults briefings = mRealm.where(Briefing.class).findAll();
        if (briefings.size() < 1) {
            return;
        }
        mRealm.beginTransaction();
        briefings.deleteAllFromRealm();
        mRealm.commitTransaction();
    }

}
