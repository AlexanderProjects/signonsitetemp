package au.com.signonsitenew.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Model to represent sites that a user is enrolled into.
 */
public class EnrolledSite extends RealmObject {

    @PrimaryKey
    private Long id;
    private String name;
    private String points; // Points are list of key value pairs, decode later
    private String managerName;
    private String siteAddress;
    private double regionLat;
    private double regionLong; // Centre coordinates for site
    private double regionRadius;
    private Boolean inducted;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getSiteAddress() {
        return siteAddress;
    }

    public void setSiteAddress(String siteAddress) {
        this.siteAddress = siteAddress;
    }

    public double getRegionLat() {
        return regionLat;
    }

    public void setRegionLat(double regionLat) {
        this.regionLat = regionLat;
    }

    public double getRegionLong() {
        return regionLong;
    }

    public void setRegionLong(double regionLong) {
        this.regionLong = regionLong;
    }

    public double getRegionRadius() {
        return regionRadius;
    }

    public void setRegionRadius(double regionRadius) {
        this.regionRadius = regionRadius;
    }

    public boolean inducted() {
        return inducted;
    }

    public void setInducted(boolean inducted) {
        this.inducted = inducted;
    }
}
