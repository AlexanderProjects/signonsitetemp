package au.com.signonsitenew.realm;

import javax.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Krishan Caldwell on 10/07/2016.
 */
public class Credential extends RealmObject {
    @PrimaryKey private Long id; // Allows for updating of object
    @Nullable private Long userId; // Use boxed variant so this field can be null
    private String email; // Only have this because IDs aren't set until user signs on...
    private String credentialType;
    private String frontPhotoUri;
    private String backPhotoUri;
    private String whitecardNumber;
    private String whitecardIssueDate;
    private String whitecardState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCredentialType() {
        return credentialType;
    }

    public void setCredentialType(String credentialType) {
        this.credentialType = credentialType;
    }

    public String getFrontPhotoUri() {
        return frontPhotoUri;
    }

    public void setFrontPhotoUri(String frontPhotoUri) {
        this.frontPhotoUri = frontPhotoUri;
    }

    public String getBackPhotoUri() {
        return backPhotoUri;
    }

    public void setBackPhotoUri(String backPhotoUri) {
        this.backPhotoUri = backPhotoUri;
    }

    public String getWhitecardNumber() {
        return whitecardNumber;
    }

    public void setWhitecardNumber(String whitecardNumber) {
        this.whitecardNumber = whitecardNumber;
    }

    public String getWhitecardIssueDate() {
        return whitecardIssueDate;
    }

    public void setWhitecardIssueDate(String whitecardIssueDate) {
        this.whitecardIssueDate = whitecardIssueDate;
    }

    public String getWhitecardState() {
        return whitecardState;
    }

    public void setWhitecardState(String whitecardState) {
        this.whitecardState = whitecardState;
    }
}
