package au.com.signonsitenew.realm;

import io.realm.RealmObject;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class ManagedCompany extends RealmObject {
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
