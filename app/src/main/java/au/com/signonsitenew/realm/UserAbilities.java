package au.com.signonsitenew.realm;

import io.realm.RealmObject;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Realm model to store what abilities a user has access to.
 */
public class  UserAbilities extends RealmObject {

    private long userId;
    private long siteId;
    private boolean evacuate;
    private boolean visitorRegister;
    private boolean manageUserInductions;
    private Boolean hasManagerUserPermitPermission;
    private Boolean canUpdateBriefing;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public boolean canEvacuate() {
        return evacuate;
    }

    public void setEvacuate(boolean evacuate) {
        this.evacuate = evacuate;
    }

    public boolean canVisitorRegister() {
        return visitorRegister;
    }

    public void setVisitorRegister(boolean visitorRegister) {
        this.visitorRegister = visitorRegister;
    }

    public void setHasManagerUserPermitPermission(boolean hasManagerUserPermitPermission){
        this.hasManagerUserPermitPermission = hasManagerUserPermitPermission;
    }

    public boolean canManagerUserHasPermitPermission(){
        return hasManagerUserPermitPermission;
    }

    public boolean canManageUserInductions() {
        return manageUserInductions;
    }

    public void setManageUserInductions(boolean manageUserInductions) { this.manageUserInductions = manageUserInductions; }

    public Boolean getCanUpdateBriefing() {
        return canUpdateBriefing;
    }

    public void setCanUpdateBriefing(Boolean canUpdateBriefing) {
        this.canUpdateBriefing = canUpdateBriefing;
    }
}
