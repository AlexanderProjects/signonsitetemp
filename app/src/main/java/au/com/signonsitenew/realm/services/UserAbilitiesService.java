package au.com.signonsitenew.realm.services;


import org.json.JSONException;
import org.json.JSONObject;
import au.com.signonsitenew.realm.UserAbilities;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;

/**
 * Created by Krishan Caldwell on 29/06/2018.
 *
 * Service to handle user abilities for a site.
 */
public class UserAbilitiesService {
    private static final String LOG = UserAbilitiesService.class.getSimpleName();
    private static final String USER_ID = "userId";
    private static final String SITE_ID = "siteId";

    private Realm mRealm;

    public UserAbilitiesService(Realm realm) {
       mRealm = realm;
    }

    public void setAbilities(JSONObject jsonData) {

        try {
            // Clear any existing settings
            mRealm.beginTransaction();
            mRealm.where(UserAbilities.class).findAll().deleteAllFromRealm();
            mRealm.commitTransaction();

            JSONObject abilitiesObject = jsonData.getJSONObject(Constants.JSON_ABILITIES);

            boolean canEvacuate = abilitiesObject.getBoolean(Constants.JSON_EVACUATION_ABILITY);
            boolean canVisitorRegister = abilitiesObject.getBoolean(Constants.JSON_VISITOR_REGISTER_ABILITY);
            boolean canManageUserInductions = abilitiesObject.getBoolean(Constants.JSON_MANAGE_USER_INDUCTIONS_ABILITY);
            boolean canManageBriefings = abilitiesObject.getBoolean(Constants.JSON_MANAGE_BRIEFINGS_ABILITY);
            boolean canManagerHasPermits = abilitiesObject.getBoolean(Constants.JSON_MANAGE_PERMITS );

            long userId = jsonData.getLong(Constants.JSON_USER_ID);
            long siteId = jsonData.getLong(Constants.JSON_SITE_ID);

            mRealm.beginTransaction();
            UserAbilities abilities = new UserAbilities();
            abilities.setUserId(userId);
            abilities.setSiteId(siteId);
            abilities.setEvacuate(canEvacuate);
            abilities.setVisitorRegister(canVisitorRegister);
            abilities.setManageUserInductions(canManageUserInductions);
            abilities.setCanUpdateBriefing(canManageBriefings);
            abilities.setHasManagerUserPermitPermission(canManagerHasPermits);
            mRealm.copyToRealm(abilities);
            mRealm.commitTransaction();

        }
        catch (JSONException e) {
            SLog.e(LOG, "JSONException Occurred. " + e.getMessage());
        }
    }


    public boolean hasManagerPermissions(long userId, long siteId) {
        UserAbilities abilities = mRealm.where(UserAbilities.class)
                .equalTo(USER_ID, userId)
                .equalTo(SITE_ID, siteId)
                .findFirst();
        if (abilities == null) {
            return false;
        }
        // If they have access to inductions, they must have access to the visitor register.
        return abilities.canEvacuate() || abilities.canVisitorRegister();
    }

    public boolean canAccessEvacuations(long userId, long siteId) {
        UserAbilities abilities = mRealm.where(UserAbilities.class)
                                        .equalTo(USER_ID, userId)
                                        .equalTo(SITE_ID, siteId)
                                        .findFirst();
        return abilities != null && abilities.canEvacuate();
    }

    public boolean canAccessVisitorRegister(long userId, long siteId) {
        UserAbilities abilities = mRealm.where(UserAbilities.class)
                                        .equalTo(USER_ID, userId)
                                        .equalTo(SITE_ID, siteId)
                                        .findFirst();
        return abilities != null && abilities.canVisitorRegister();
    }

    public boolean canAccessUserInductions(long userId, long siteId) {
        UserAbilities abilities = mRealm.where(UserAbilities.class)
                                        .equalTo(USER_ID, userId)
                                        .equalTo(SITE_ID, siteId)
                                        .findFirst();
        return abilities != null && abilities.canManageUserInductions();
    }

}
