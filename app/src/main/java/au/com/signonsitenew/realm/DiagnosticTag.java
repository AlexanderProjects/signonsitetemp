package au.com.signonsitenew.realm;

import io.realm.RealmObject;

/**
 * Created by Krishan Caldwell on 13/07/2016.
 */
public class DiagnosticTag extends RealmObject {
    private String tagType;

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }
}
