package au.com.signonsitenew.realm.services;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.events.BriefingEvent;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.Briefing;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.utilities.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by SignOnSite on 1/09/17.
 *
 * Class of helper methods to store data in Realm without repeating code everywhere...
 */

public class AttendanceRecordService {

    private static final String LOG = AttendanceRecordService.class.getSimpleName();

    // Field Constants
//    private static final String ATTENDANCE_ = "id";
//    private static final String BRIEFING_SITE_ID = "siteId";

    private Realm mRealm;

    public AttendanceRecordService(Realm realm) {
        mRealm = realm;
    }

    public void createOrUpdateAttendanceRecord(JSONObject attendanceRecord) {
        try {
            mRealm.beginTransaction();
            AttendanceRecord attendance = new AttendanceRecord();
            attendance.setId(attendanceRecord.getLong(Constants.JSON_VISIT_ID));
            attendance.setUserId(attendanceRecord.getLong(Constants.JSON_VISIT_USER_ID));
            attendance.setSiteId(attendanceRecord.getLong(Constants.JSON_VISIT_SITE_ID));
            attendance.setCheckInTime(attendanceRecord.getString(Constants.JSON_VISIT_CHECKIN));
            attendance.setCheckOutTime(attendanceRecord.getString(Constants.JSON_VISIT_CHECKOUT));
            mRealm.copyToRealmOrUpdate(attendance);
            mRealm.commitTransaction();
        }
        catch (JSONException e) {
            Log.e(LOG, "JSONException Occurred: " + e.getMessage());
        }
    }

    public void deleteAllAttendanceRecords() {
        RealmResults attendanceRecords = mRealm.where(AttendanceRecord.class).findAll();
        if (attendanceRecords.size() < 1) {
            return;
        }
        mRealm.beginTransaction();
        attendanceRecords.deleteAllFromRealm();
        mRealm.commitTransaction();
    }

}
