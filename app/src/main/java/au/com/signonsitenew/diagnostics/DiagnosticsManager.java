package au.com.signonsitenew.diagnostics;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import au.com.signonsitenew.api.API;
import au.com.signonsitenew.api.DiagnosticLogs;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.DiagnosticLog.Tag;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NetworkUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import au.com.signonsitenew.utilities.StatusCheckers;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Class for managing the Diagnostic Event Log lifecycle, including storing, aggregating and
 * flushing to our server.
 */
public class DiagnosticsManager {
    private static final String TAG = DiagnosticsManager.class.getSimpleName();

    public static void logEvent(Context context, Tag locTag, @Nullable Location location, @Nullable String jsonData) {
        // If total number of logs is 1000, remove the oldest one to ensure user's device does not get flooded.
        RealmConfiguration config = RealmManager.getRealmConfiguration();
        Realm realm = Realm.getInstance(config);
        long numLogs = realm.where(DiagnosticLog.class).count();
        SLog.i(TAG, "number of diagnostic logs: " + numLogs);
        if (numLogs >= 5000) {
            realm.beginTransaction();
            RealmResults<DiagnosticLog> logs = realm.where(DiagnosticLog.class).findAll();
            logs.deleteLastFromRealm();
            realm.commitTransaction();
            SLog.i(TAG, "Deleted oldest realm log as there were over 5000");
        }

        // Add the new log
        Log.i(TAG, "Logging location info with tag: " + locTag.toString());
        float batteryLevel = StatusCheckers.getBatteryLevel(context);
        boolean networkAvailable = NetworkUtil.networkIsConnected(context);
        realm.beginTransaction();
        DiagnosticLog locLog = realm.createObject(DiagnosticLog.class);
        locLog.setTag(locTag);
        if (location != null) {
            locLog.setLatitude(location.getLatitude());
            locLog.setLongitude(location.getLongitude());
            locLog.setAccuracy(location.getAccuracy());
        }
        locLog.setUserEmail(new SessionManager(context).getCurrentUser().get(Constants.USER_EMAIL));
        locLog.setBatteryLevel(batteryLevel);
        locLog.setNetworkAvailable(networkAvailable);
        locLog.setTimestamp(new Date());
        if (jsonData != null) {
            locLog.setJsonData(jsonData);
        }
        Log.i(TAG, "Log contents: " + locLog.toString());
        realm.commitTransaction();
        realm.close();
        Log.i(TAG, "Logged location info successfully");
    }

    /**
     * Sends all Location Logs that have been collected over the course of time to the server
     * and removes them from local storage once successfully posted.
     *
     * This call should fire if and only if diagnostics has been opted into by the user.
     */
    public static void flushLogs(Context context, final SuccessCallback callback) {
        SessionManager sessionManager = new SessionManager(context);
        if (!sessionManager.diagnosticsIsEnabled()) {
            // The user has not enabled diagnostics, stop here.
            Log.i(TAG, "User has not enabled diagnostics, returning early");
            return;
        }
        // Get currently requested Categories and Tags
        final ArrayList<DiagnosticLog> toDelete = new ArrayList<>();
        RealmConfiguration config = RealmManager.getRealmConfiguration();
        final Realm realm = Realm.getInstance(config);
        RealmResults<DiagnosticLog> diagnosticLogs = realm.where(DiagnosticLog.class).findAll();
        SLog.i(TAG, "Number of diagnostic logs stored: " + diagnosticLogs.size());

        // If results are not empty, flush the logs
        if (!diagnosticLogs.isEmpty()) {

            // Convert logs to JSON and send PUT request with data to server
            int preparedCount = 0;
            JSONArray jsonArray = new JSONArray();
            JSONObject data = new JSONObject();
            for (DiagnosticLog locLog : diagnosticLogs) {
                // check with tag manager if we should flush it first
                if(!DiagnosticsTagManager.shared.shouldFlushTag(locLog.getTag())) continue;

                // Convert timestamp to format like: 2016-05-05 13:00:00
                Date unformattedTime = locLog.getTimestamp();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String timestamp = dateFormat.format(unformattedTime);
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("tag", locLog.getTag());
                    jsonObject.put("loc_lat", locLog.getLatitude());
                    jsonObject.put("loc_long", locLog.getLongitude());
                    jsonObject.put("loc_acc", locLog.getAccuracy());
                    if (locLog.getJsonData() != null) {
                        jsonObject.put("json", locLog.getJsonData());
                    }
                    else {
                        jsonObject.put("json", "null");
                    }
                    jsonObject.put("batt_level", locLog.getBatteryLevel());
                    jsonObject.put("network_available", locLog.getNetworkAvailable());
                    jsonObject.put("loc_time", timestamp);
                    jsonObject.put("timestamp", timestamp);
                    jsonObject.put("loc_email", locLog.getUserEmail());
                    jsonArray.put(jsonObject);
                    toDelete.add(locLog);
                    preparedCount++;
                }
                catch (JSONException e) {
                    SLog.e(TAG, "A JSON Exception occurred with logs: " + e.getMessage());
                }
            }
            SLog.i(TAG, "Diagnostic Logs prepared: " + preparedCount);
            final int toSendCount = preparedCount;
            // Create the JSON object
            try {
                data.put("email", new SessionManager(context).getCurrentUser().get(Constants.USER_EMAIL));
                data.put("logs", jsonArray);
            }
            catch (JSONException e) {
                SLog.e(TAG, "A JSON Exception occurred with data: " + e.getMessage());
            }

            DiagnosticLogs.post(context, jsonArray, new API.ResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    SLog.i(TAG, "Flush response: " + response);
                    try {
                        if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            // Logs were successfully sent to server, remove locally
                            int deleteCount = 0;
                            realm.beginTransaction();
                            for(DiagnosticLog delete: toDelete) {
                                if (delete.isValid()) {
                                    delete.deleteFromRealm();
                                    deleteCount++;
                                }
                            }
                            realm.commitTransaction();
                            realm.close();
                            SLog.i(TAG, "Logs stored on server and flushed locally");
                            SLog.i(TAG, "Diagnostic logs deleted after being sent to server: " + deleteCount);

                            if (deleteCount != toSendCount) {
                                SLog.e(TAG, "Number of logs sent does NOT match number deleted. Num sent: " + toSendCount + ", deleted: " + deleteCount);
                            }
                            if (callback != null) {
                                callback.onFlushComplete(response);
                            }
                        }
                        else {
                            SLog.e(TAG, "Location Logs were not stored on server correctly");
                        }
                    }
                    catch (JSONException e) {
                        SLog.e(TAG, "A JSON Exception occurred: " + e.getMessage());
                    }
                }
            }, new API.ErrorCallback() {
                @Override
                public void onError() {
                    SLog.e(TAG, "A volley error occurred when attempting to flush logs");
                }
            });
        }
    }

    public interface SuccessCallback {
        void onFlushComplete(JSONObject response);
    }
}
