package au.com.signonsitenew.diagnostics;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import au.com.signonsitenew.realm.DiagnosticTag;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;
import io.realm.RealmResults;

import au.com.signonsitenew.SOSApplication;

import android.app.Application;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class DiagnosticsTagManager {
    /*
        Define tags and their categories here.
     */
    private static String TagRegistered = "LOC_REGISTER";
    private static String TagManualSignOn = "LOC_MANUAL_ON";
    private static String TagManualSignOff = "LOC_MANUAL_OFF";
    private static String TagAutoSignOn = "LOC_AUTO_ON";
    private static String TagAutoSignOff = "LOC_AUTO_OFF";
    private static String TagManualSignOnErr = "LOC_MANUAL_ON_ERR";
    private static String TagManualSignOffErr = "LOC_MANUAL_OFF_ERR";
    private static String TagAutoSignOnErr = "LOC_AUTO_ON_ERR";
    private static String TagAutoSignOffErr = "LOC_AUTO_OFF_ERR";
    private static String TagRegionFetch = "LOC_REGION";
    private static String TagRegionEntry = "LOC_ENTRY";
    private static String TagRegionExit = "LOC_EXIT";
    private static String TagPollingLocation = "LOC_POLLING";
    private static String TagBadPoll = "LOC_POLL_NO_ACTION";
    private static String TagNoLocAccess = "LOC_NO_ACCESS";
    private static String TagSignificantChange = "LOC_SIG";
    private static String TagNetworkError = "NETWORK_ERROR";
    private static String TagRefetchTrigger = "REFETCH_TRIGGER";
    private static String TagLocationPermissionsChanged = "LOC_PERMISSIONS_IOS";
    private static String TagForgotPassword = "FORGOT_PASSWORD";
    private static String TagNotificationRegistration = "IOS_NOTIFICATION_REGISTRATION";
    private static String TagNotification = "GOT_NOTIFICATION";
    private static String TagFeedback = "FEEDBACK";
    private static String TagLogOut = "LOGOUT";
    private static String TagLocationPermissionRequest = "LOC_PERMISSION_REQUEST";
    private static String TagBatteryOptimisationPermissionRequest = "BATT_OPT_IGNORE_REQUEST";
    private static String TagSiteListResult = "SITE_LIST_RESULT";
    private static String TagAlpha2Error = "ALPHA_2_ERROR";

    private static String CategoryLocationEngine = "LOC_ENG";
    private static String CategoryLocationInfo = "LOC_INFO";
    private static String CategoryPermissions = "PERMISSIONS";
    private static String CategoryNetwork = "NETWORK";
    private static String CategorySupport = "SUPPORT";
    private static String CategoryNotifications = "NOTIFICATIONS";
    private static String CategorySiteEmergency = "SITE_EMERGENCY";
    private static String CategoryError = "ERROR";
    private static String CategoryButtonPress = "BUTTON_PRESS";

    /*
        Define each tag's categories.
     */
    private TagMap tagCategoryRelation = new TagMap(new TagMap.Entry[] {
            new TagMap.Entry(TagRegistered, new String[] { CategoryLocationEngine, CategoryLocationInfo }),
            new TagMap.Entry(TagManualSignOn, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagManualSignOff, new String[] {  CategoryLocationEngine, CategoryLocationInfo }),
            new TagMap.Entry(TagAutoSignOn, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagAutoSignOff, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagManualSignOnErr, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagManualSignOffErr, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagAutoSignOnErr, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagAutoSignOffErr, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagRegionFetch, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagRegionEntry, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagRegionExit, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagPollingLocation, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagBadPoll, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagNoLocAccess, new String[] { CategoryPermissions, CategoryLocationEngine, CategoryLocationInfo }),
            new TagMap.Entry(TagSignificantChange, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagNetworkError, new String[] { CategoryNetwork, CategoryError }),
            new TagMap.Entry(TagRefetchTrigger, new String[] { CategoryLocationEngine, CategoryLocationInfo  }),
            new TagMap.Entry(TagLocationPermissionsChanged, new String[] { CategoryLocationEngine, CategoryLocationInfo, CategoryPermissions }),
            new TagMap.Entry(TagForgotPassword, new String[] { CategorySupport, CategoryButtonPress }),
            new TagMap.Entry(TagNotificationRegistration, new String[] { CategorySupport, CategoryNotifications }),
            new TagMap.Entry(TagNotification, new String[] { CategorySupport, CategoryNotifications, CategorySiteEmergency }),
            new TagMap.Entry(TagFeedback, new String[] { CategorySupport }),
            new TagMap.Entry(TagLogOut, new String[] { CategoryButtonPress, CategorySupport }),
            new TagMap.Entry(TagLocationPermissionRequest, new String[] { CategoryLocationEngine, CategoryLocationInfo, CategoryPermissions }),
            new TagMap.Entry(TagBatteryOptimisationPermissionRequest, new String[] { CategoryLocationEngine, CategoryLocationInfo, CategoryPermissions }),
            new TagMap.Entry(TagSiteListResult, new String[] { CategoryLocationEngine, CategoryLocationInfo, CategoryPermissions }),
            new TagMap.Entry(TagAlpha2Error, new String[] { CategoryError }),
    });

    /**
        Instead of initialising this class, use the shared instance.

        This lets us cache stuff in memory instead of always reading from persistent storage.
     */
    public static DiagnosticsTagManager shared = new DiagnosticsTagManager();

    /**
        Diagnostics manager should call this to decide whether or not to flush a tag.

        Currently called in DiagnosticsManager.flushLogs. TODO make sure this is ok
        I.E: if(!DiagTagManager.shared.shouldFlushTag(locLog.getTag())) continue;
     */
    public boolean shouldFlushTag(String tag) {
        return tagCache.contains(tag);
    }

    /**
        At some point, the versions/android response should be passed to this function.
        It defines how to parse the tag/category aspect of the response.
        It will compute all of the tags we should be sending to the server, and persist them.

        This is currently called in SoSAPI.postPhoneAndAppInfo. TODO make sure this is ok
     */
    public void handleVersionsResponse(JSONObject json) {
        // this is the set we are building up to store
        HashSet<String> tags = new HashSet<>();
        // Handle parsing tags field of response.
        SLog.i(TAG, "handleVersionsResponse");
        try {
            JSONArray jsonTagArray = json.getJSONArray("tags");
            for(int i = 0; i < jsonTagArray.length(); ++i) {
                try {
                    String tag = jsonTagArray.getString(i);
                    SLog.i(TAG, "handleVersionsResponse got tag: " + tag);
                    if(tag != null) {
                        tags.add(tag);
                    }
                } catch (Exception e) {
                    SLog.e(TAG,"Error iterating through tag array: " + e.getLocalizedMessage());
                }
            }
        } catch (Exception e) {
            SLog.e(TAG,"Error getting tags from versions response: " + e.getLocalizedMessage());
        }

        // Handle parsing categories field of response.
        try {
            // this is an array of array of categories
            JSONArray categoriesies = json.getJSONArray("categories");
            for(int i = 0; i < categoriesies.length(); ++i) {
                try {
                    // this is an array of categories, we are to include all tags that contain
                    // all of these categories

                    JSONArray categoriesJSON = categoriesies.getJSONArray(i);
                    SLog.i(TAG, "handleVersionsResponse got categories array with length: " + categoriesies.length() + ". and it looks like: " + categoriesies.toString());
                    Set<String> categories = new HashSet<>();
                    for(int j = 0; j < categoriesJSON.length(); ++j) {
                        try {
                            String category = categoriesJSON.getString(j);
                            SLog.i(TAG, "handleVersionsResponse got category: " + category);
                            categories.add(category);

                        } catch (Exception e) {
                            SLog.e(TAG,"Error with list of categories: " + e.getLocalizedMessage());
                        }
                    }
                    // built set of categories, now find appropriate tags
                    // and add them to our tags set
                    tags.addAll(tagCategoryRelation.categoriesToTags(categories));
                } catch (Exception e) {
                    SLog.e(TAG,"Error with list of list of categories: " + e.getLocalizedMessage());
                }
            }
        } catch (Exception e) {
            SLog.e(TAG,"Error handling categories from versions response: " + e.getLocalizedMessage());
        }

        // we added all tags in response to tags
        // for each array of categories, we added all tags that contain all of those categories to tags
        // now just store tags !
        storeTags(tags);
    }

    // logging TAG
    private String TAG = "DiagTagManager";
    // cache tags in memory (write through)
    private Set<String> tagCache;
    private Context mContext;
    // hide initialiser, we only want one to be alive
    private DiagnosticsTagManager() {
        setContext();
        tagCache = retrieveTags();
    }
    private boolean setContext() {
        Context c = null;
        Application a = SOSApplication.sInstance;
        if(a != null) c = a.getApplicationContext();

        if(c == null) {
            if(mContext == null) {
                SLog.e(TAG, "Couldn't set app context...");
                return false;
            }
        } else {
            mContext = c;
        }
        return true;
    }
    /*
        Get persisted tags. Should only be called from initialiser, using write through cache.

        Please don't return null.

        TODO
     */
    private Set<String> retrieveTags() {
        if(!setContext()) {
            return new HashSet<>();
        }
        // FIXME: Add context to initialiser
        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
        RealmResults<DiagnosticTag> realmTags = realm.where(DiagnosticTag.class).findAll();

        HashSet<String> tags = new HashSet<>();
        for (DiagnosticTag realmTag : realmTags) {
            tags.add(realmTag.getTagType());
        }
        realm.close();

        return tags;
    }

    /*
        Persist tags.

        TODO
     */
    private void storeTags(Set<String> tags) {
        // write tags to memory cache
        tagCache = tags;
        if(!setContext()) {
            return;
        }
        // log some nice stuff and store in realm
        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
        for(String tag : tags) {
            realm.beginTransaction();
            DiagnosticTag realmTag = realm.createObject(DiagnosticTag.class);
            realmTag.setTagType(tag);
            realm.commitTransaction();
            SLog.i(TAG, "Storing tag: " + tag);
        }
        realm.close();
    }

    /*
        Tags and categories have a many to many relationship, which
        we may need to query both ways. This defines how to represent
        and query that relationship efficiently.

        Could have written this generically, but it's a bit more effort
        and we probably won't need it again? Just slap 'er in here.
     */
     private static class TagMap {
        // tag to categories
        private HashMap<String,Set<String>> forward;
        // category to tags
        private HashMap<String,Set<String>> backward;

        private static class Entry {
            String tag; Set<String> categories;
            public Entry(String tag, String[] categories) {
                this.tag=tag;
                this.categories = new HashSet<>(Arrays.asList(categories));
            }
        }

        /*
            returns the set of all tags belonging to a category
         */
        public Set<String> categoryToTags(String category) {
            if (backward.containsKey(category)) return backward.get(category);
            else return new HashSet<>();
        }

        /*
            Returns all the tags that fall in ALL of the provided categories
         */
        public Set<String> categoriesToTags(Set<String> categories) {
            Set<String> tags = null;
            for(String c: categories) {
                if(tags == null) {
                    tags = categoryToTags(c);
                } else {
                    tags.retainAll(categoryToTags(c));
                }
            }
            if(tags == null) {
                return new HashSet<>();
            } else {
                return tags;
            }
        }

        private TagMap(Entry[] entries) {
            forward = new HashMap<>();
            backward = new HashMap<>();

            for(Entry entry : entries) {
                // populate forward mapping
                if(forward.containsKey(entry.tag)) {
                    forward.get(entry.tag).addAll(entry.categories);
                } else {
                    forward.put(entry.tag, entry.categories);
                }
                // populate backward mapping
                for(String category : entry.categories) {
                    if(backward.containsKey(category)) {
                        backward.get(category).add(entry.tag);
                    } else {
                        HashSet<String> backset = new HashSet<>();
                        backset.add(entry.tag);
                        backward.put(category,backset);
                    }
                }
            }
        }
    }
}


