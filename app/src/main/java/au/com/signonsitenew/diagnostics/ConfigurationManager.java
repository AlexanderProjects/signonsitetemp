package au.com.signonsitenew.diagnostics;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * This class handles the information stored within the Configuration Realm Table, updating and
 * retrieving it. In cases where data is incomplete, it enforces a default behaviour upon the device.
 */
public class ConfigurationManager {
}
