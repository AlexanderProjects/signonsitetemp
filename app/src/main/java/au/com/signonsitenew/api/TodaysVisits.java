package au.com.signonsitenew.api;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.events.AttendanceUpdatedEvent;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.services.SiteAttendeeService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class TodaysVisits {
    private static final String LOG = TodaysVisits.class.getSimpleName();
    private static final String url = Constants.URL_TODAYS_VISITS;

    public static void get(final Context context,
                           final API.ResponseCallback responseCallback,
                           final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(session.getSiteId()));

        API.get(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                            else {
                                // Visitors currently signed on retrieved
                                Realm mRealm = Realm.getDefaultInstance();
                                SiteAttendeeService attendeeService = new SiteAttendeeService(mRealm);
                                JSONArray visitsArray = response.getJSONArray(Constants.JSON_VISITS);
                                JSONArray visitorsArray = response.getJSONArray(Constants.JSON_VISITORS);

                                // Store Visitors
                                mRealm.beginTransaction();
                                mRealm.where(SiteAttendee.class).findAll().deleteAllFromRealm();
                                mRealm.where(AttendanceRecord.class).findAll().deleteAllFromRealm();
                                mRealm.commitTransaction();

                                for (int i=0; i<visitorsArray.length(); i++) {
                                    JSONObject person = visitorsArray.getJSONObject(i);
                                    attendeeService.createOrUpdateSiteAttendee(person);
                                }

                                mRealm.beginTransaction();
                                // Store AttendanceRecord Information
                                for (int i=0; i<visitsArray.length(); i++) {
                                    JSONObject instance = visitsArray.getJSONObject(i);

                                    AttendanceRecord visit = new AttendanceRecord();
                                    visit.setId(instance.getLong(Constants.JSON_VISIT_ID));
                                    visit.setUserId(instance.getLong(Constants.JSON_VISIT_USER_ID));
                                    visit.setSiteId(instance.getLong(Constants.JSON_VISIT_SITE_ID));
                                    visit.setCheckInTime(instance.getString(Constants.JSON_VISIT_CHECKIN));
                                    visit.setCheckOutTime(instance.getString(Constants.JSON_VISIT_CHECKOUT));
                                    mRealm.copyToRealmOrUpdate(visit);
                                }
                                mRealm.commitTransaction();
                                mRealm.close();

                                EventBus.getDefault().post(new AttendanceUpdatedEvent(Constants.EVENT_SIGN_ON));
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (responseCallback != null) {
                            responseCallback.onResponse(response);
                        }
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        if (errorCallback != null) {
                            errorCallback.onError();
                        }
                    }
                });
    }

}
