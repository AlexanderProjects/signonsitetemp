package au.com.signonsitenew.api;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import au.com.signonsitenew.R;
import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.diagnostics.DiagnosticsTagManager;
import au.com.signonsitenew.realm.DiagnosticLog.Tag;
import au.com.signonsitenew.realm.EnrolledSite;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.services.BriefingService;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.realm.services.UserAbilitiesService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * [[[[ DEPRECATED ]]]]
 * This was a not well considered implementation idea in the past. To make it easier to separate
 * concerns, and avoid a whole lot of repetition (adding JWT tokens to every network call), network
 * requests should be moved into their own classes as shown inside the api package.
 *
 * This class contains the generic API calls to the SignOnSite mobile backend.
 */
public class SOSAPI {

    protected Context mContext;
    private SessionManager mSession;

    public static final String LOG = SOSAPI.class.getSimpleName();

    public SOSAPI(Context context) {
        mContext = context;
        mSession = new SessionManager(context);
    }

    /**
     * Authenticates the user into the app.
     * @param email - the user's email
     * @param password - the user's password
     * @param successCallback - callback for when data is returned from the server
     */
    public void loginUser(final String email,
                          final String password,
                          final VolleySuccessCallback successCallback,
                          final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_LOGIN);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_LOGIN,
                response -> {
                    SLog.i(LOG, "Login response: " + response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if(!jsonResponse.getString(Constants.JSON_RESPONSE_STATUS).equals(Constants.RATE_LIMITED)){
                            if (!jsonResponse.getBoolean("loggedIn")) {
                                JSONObject jsonData = new JSONObject();
                                JSONObject jsonParams = new JSONObject();
                                jsonData.put("url", Constants.URL_LOGIN);
                                jsonData.put(Constants.USER_EMAIL, email);
                                jsonData.put("params", jsonParams);
                                jsonData.put("error", response);

                            } else {
                                // Login successful, store JWT
                                String jwt = jsonResponse.getString(Constants.AUTH_TOKEN);
                                Log.i(LOG, "JWT stored: " + jwt);
                                mSession.storeToken(jwt);
                            }
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_LOGIN);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_LOGIN);
                        jsonParams.put(Constants.USER_EMAIL, email);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, email);
                params.put(Constants.USER_PASSWORD, password);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Returns the closest 20 regions to the user's sent location. This includes whether the user
     * is inducted into the region and whether the site foreman has ticked them off as being
     * inducted.
     * @param location - the user's current location
     * @param successCallback - interface callback to implement app logic within
     */
    public void getNearbyRegions(final Location location,
                                 final VolleySuccessCallback successCallback,
                                 final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_NEARBY_REGION_LIST);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_NEARBY_REGION_LIST,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_NEARBY_REGION_LIST + ": " + response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (!jsonResponse.getString("msg").equals("Sites available")) {
                            JSONObject jsonData = new JSONObject();
                            jsonData.put("url", Constants.URL_NEARBY_REGION_LIST);
                            jsonData.put("error", response);
                            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));

                            DiagnosticsManager.logEvent(mContext,
                                               Tag.NETWORK_FAIL_RESPONSE,
                                               location,
                                               jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.e(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_NEARBY_REGION_LIST);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_NEARBY_REGION_LIST);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        if (location != null) {
                            jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                            jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                        }
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                Log.i(LOG, "region params: " + params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * This call is a synchronous call, ie blocking.
     *
     * Retrieves a maximum of 100 regions, closest to the user's location for which the user is
     * inducted into. If the user is inducted into less than 100 regions, this will return the
     * relevant number of regions. Android can monitor a maximum of 100 geofences.
     * @param location - the user's current location
     */
    public String getInductedRegionsBlocking(final Location location) {
        SLog.i(LOG, "Calling Blocking " + Constants.URL_INDUCTED_REGION_LIST);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_INDUCTED_REGION_LIST, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.NUM_SITES_REQUESTED, String.valueOf(100));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                params.put(Constants.CAN_BLOCK_AUTO_SIGNON, Boolean.toString(true));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);

        JSONObject jsonData = new JSONObject();
        JSONObject jsonParams = new JSONObject();
        try {
            jsonData.put("url", Constants.URL_INDUCTED_REGION_LIST);
            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
            jsonData.put("params", jsonParams);

            return future.get(30, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            SLog.e(LOG, "Retrieve inducted regions blocking call was interrupted. " + e.getMessage());
            try {
                jsonData.put("url", Constants.URL_INDUCTED_REGION_LIST);
                jsonData.put("error", e.getMessage());
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());
        }
        catch (ExecutionException e) {
            SLog.e(LOG, "Retrieve inducted regions blocking call failed. " + e.getMessage());
            try {
                jsonData.put("url", Constants.URL_INDUCTED_REGION_LIST);
                jsonData.put("error", e.getMessage());
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());
        }
        catch (TimeoutException e) {
            SLog.e(LOG, "Retrieve inducted regions blocking call timed out. " + e.getMessage());
            try {
                jsonData.put("url", Constants.URL_INDUCTED_REGION_LIST);
                jsonData.put("error", e.getMessage());
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());
        }
        catch (JSONException e) {
            SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
        }
        return null;
    }

    /**
     * Signs the user onto a specified site.
     * @param automatic - flag to determine whether attempt is automatic or manual
     * @param siteId - the id of the site which the user is attempting to sign onto
     * @param location - the user's current location
     * @param successCallback - interface callback to implement app logic within
     */
    public void signOnSite(final boolean automatic,
                           final Integer siteId,
                           final Location location,
                           final VolleySuccessCallback successCallback,
                           final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_ON);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_ON,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_SIGN_ON + ": " + response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        JSONObject jsonData = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("response", response);

                        if (jsonResponse.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            SLog.i(LOG, "User manually signed onto site");

                            Realm realm = Realm.getDefaultInstance();

                            UserService userService = new UserService(realm);
                            long userId = userService.getCurrentUserId();

                            // Set the user permissions
                            UserAbilitiesService abilitiesService = new UserAbilitiesService(realm);
                            abilitiesService.setAbilities(jsonResponse);

                            SiteSettingsService siteSettingsService = new SiteSettingsService(realm);
                            siteSettingsService.createSiteSettings(jsonResponse);

                            // Fetch briefing details
                            Integer briefingId = jsonResponse.isNull(Constants.JSON_BRIEFING_ID) ? null : jsonResponse.getInt(Constants.JSON_BRIEFING_ID);
                            String briefing = jsonResponse.isNull(Constants.JSON_BRIEFING_TEXT) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_TEXT);
                            String briefingSetBy = jsonResponse.isNull(Constants.JSON_BRIEFING_NAME) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_NAME);
                            String briefingDate = jsonResponse.isNull(Constants.JSON_BRIEFING_PUBLISHED) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_PUBLISHED);
                            Boolean needsAcknowledgement = jsonResponse.isNull(Constants.JSON_BRIEFING_NEEDS_ACK) ? null : jsonResponse.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK);

                            // Persist briefing details - the service will decide if notification should be set
                            if (briefingId != null) {
                                BriefingService service = new BriefingService(realm);
                                service.storeBriefing(briefingId, siteId, briefing, briefingSetBy, briefingDate, needsAcknowledgement);
                            }

                            // Check user's induction status
                            boolean formAvailable = jsonResponse.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE);
                            if (formAvailable) {
                                // Inductions are enabled on site - grab the form
                                SiteInductionService service = new SiteInductionService(realm);
                                if (jsonResponse.isNull(Constants.JSON_INDUCTION)) {
                                    // User has not completed an induction
                                    Date date = new Date();
                                    android.text.format.DateFormat df = new android.text.format.DateFormat();
                                    String formattedDate = df.format("yyyy-MM-dd hh:mm:ss", date).toString();
                                    service.createOrUpdateInduction(null, userId, siteId, null, null, formattedDate);
                                }
                                else {
                                    // User is inducted
                                    JSONObject induction = jsonResponse.getJSONObject(Constants.JSON_INDUCTION);
                                    int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                                    String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                                    JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                                    String inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                                    String updatedAt = state.getString(Constants.JSON_IND_STATE_AT);
                                    service.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, updatedAt);
                                }
                            }
                            realm.close();

                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_ON,
                                    location,
                                    jsonData.toString());
                        }
                        else {
                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_ON_ERR,
                                    location,
                                    jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_SIGN_ON);

                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_SIGN_ON);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                        if (location != null) {
                            jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                            jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                        }
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());

                    if (errorCallback != null) {
                        errorCallback.onResponse();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Signs the user onto a specified site.
     * @param automatic - flag to determine whether attempt is automatic or manual
     * @param siteId - the id of the site which the user is attempting to sign onto
     * @param location - the user's current location
     * @param successCallback - interface callback to implement app logic within
     */
    public void signOnSite(final boolean automatic,
                           final Integer siteId,
                           final Location location,
                           final String companyName,
                           final VolleySuccessCallback successCallback,
                           final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_ON);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_ON,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_SIGN_ON + ": " + response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("response", response);

                        if (jsonResponse.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            SLog.i(LOG, "User manually signed onto site");

                            Realm realm = Realm.getDefaultInstance();

                            UserService userService = new UserService(realm);
                            long userId = userService.getCurrentUserId();

                            // Set the user permissions
                            UserAbilitiesService abilitiesService = new UserAbilitiesService(realm);
                            abilitiesService.setAbilities(jsonResponse);

                            SiteSettingsService siteSettingsService = new SiteSettingsService(realm);
                            siteSettingsService.createSiteSettings(jsonResponse);

                            // Fetch briefing details
                            Integer briefingId = jsonResponse.isNull(Constants.JSON_BRIEFING_ID) ? null : jsonResponse.getInt(Constants.JSON_BRIEFING_ID);
                            String briefing = jsonResponse.isNull(Constants.JSON_BRIEFING_TEXT) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_TEXT);
                            String briefingSetBy = jsonResponse.isNull(Constants.JSON_BRIEFING_NAME) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_NAME);
                            String briefingDate = jsonResponse.isNull(Constants.JSON_BRIEFING_PUBLISHED) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_PUBLISHED);
                            Boolean needsAcknowledgement = jsonResponse.isNull(Constants.JSON_BRIEFING_NEEDS_ACK) ? null : jsonResponse.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK);

                            // Persist briefing details - the service will decide if notification should be set
                            if (briefingId != null) {
                                BriefingService service = new BriefingService(realm);
                                service.storeBriefing(briefingId, siteId, briefing, briefingSetBy, briefingDate, needsAcknowledgement);
                            }

                            // Check user's induction status
                            boolean formAvailable = jsonResponse.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE);
                            if (formAvailable) {
                                // Inductions are enabled on site - grab the form
                                SiteInductionService service = new SiteInductionService(realm);
                                if (jsonResponse.isNull(Constants.JSON_INDUCTION)) {
                                    // User has not completed an induction
                                    Date date = new Date();
                                    android.text.format.DateFormat df = new android.text.format.DateFormat();
                                    String formattedDate = df.format("yyyy-MM-dd hh:mm:ss", date).toString();
                                    service.createOrUpdateInduction(null, userId, siteId, null, null, formattedDate);
                                }
                                else {
                                    // User is inducted
                                    JSONObject induction = jsonResponse.getJSONObject(Constants.JSON_INDUCTION);
                                    int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                                    String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                                    JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                                    String inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                                    String updatedAt = state.getString(Constants.JSON_IND_STATE_AT);
                                    service.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, updatedAt);
                                }
                            }
                            realm.close();

                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_ON,
                                    location,
                                    jsonData.toString());
                        }
                        else {
                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_ON_ERR,
                                    location,
                                    jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_SIGN_ON);

                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_SIGN_ON);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                        if (location != null) {
                            jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                            jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                        }
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());

                    if (errorCallback != null) {
                        errorCallback.onResponse();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.JSON_VISITOR_COMPANY_NAME, companyName);
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }


    public void signOnSite(final boolean automatic,
                           final Integer siteId,
                           final Double lat,
                           final Double lon,
                           final VolleySuccessCallback successCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_ON);

        Location loc = new Location("derp");
        loc.setLatitude(lat);
        loc.setLongitude(lon);
        final Location location = loc;

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_ON,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(LOG, "Response from " + Constants.URL_SIGN_ON + ": " + response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);

                            JSONObject jsonData = new JSONObject();
                            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                            jsonData.put("response", response);

                            if (jsonResponse.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                SLog.i(LOG, "User manually signed onto site");

                                Realm realm = Realm.getDefaultInstance();

                                UserService userService = new UserService(realm);
                                long userId = userService.getCurrentUserId();

                                // Check if user has site management privileges
                                UserAbilitiesService abilitiesService = new UserAbilitiesService(realm);
                                abilitiesService.setAbilities(jsonResponse);

                                SiteSettingsService siteSettingsService = new SiteSettingsService(realm);
                                siteSettingsService.createSiteSettings(jsonResponse);

                                // Fetch briefing details
                                Integer briefingId = jsonResponse.isNull(Constants.JSON_BRIEFING_ID) ? null : jsonResponse.getInt(Constants.JSON_BRIEFING_ID);
                                String briefing = jsonResponse.isNull(Constants.JSON_BRIEFING_TEXT) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_TEXT);
                                String briefingSetBy = jsonResponse.isNull(Constants.JSON_BRIEFING_NAME) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_NAME);
                                String briefingDate = jsonResponse.isNull(Constants.JSON_BRIEFING_PUBLISHED) ? null : jsonResponse.getString(Constants.JSON_BRIEFING_PUBLISHED);
                                Boolean needsAcknowledgement = jsonResponse.isNull(Constants.JSON_BRIEFING_NEEDS_ACK) ? null : jsonResponse.getBoolean(Constants.JSON_BRIEFING_NEEDS_ACK);

                                // Persist briefing details - the service will decide if notification should be set
                                if (briefingId != null) {
                                    BriefingService service = new BriefingService(realm);
                                    service.storeBriefing(briefingId, siteId, briefing, briefingSetBy, briefingDate, needsAcknowledgement);
                                }

                                // Check user's induction status
                                boolean formAvailable = jsonResponse.getBoolean(Constants.JSON_INDUCTION_FORM_AVAILABLE);
                                if (formAvailable) {
                                    // Inductions are enabled on site - grab the form
                                    SiteInductionService service = new SiteInductionService(realm);
                                    if (jsonResponse.isNull(Constants.JSON_INDUCTION)) {
                                        // User has not completed an induction
                                        Date date = new Date();
                                        android.text.format.DateFormat df = new android.text.format.DateFormat();
                                        String formattedDate = df.format("yyyy-MM-dd hh:mm:ss", date).toString();
                                        service.createOrUpdateInduction(null, userId, siteId, null, null, formattedDate);
                                    }
                                    else {
                                        // User is inducted
                                        JSONObject induction = jsonResponse.getJSONObject(Constants.JSON_INDUCTION);
                                        int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                                        String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                                        JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                                        String inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                                        String updatedAt = state.getString(Constants.JSON_IND_STATE_AT);
                                        service.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, updatedAt);
                                    }
                                }
                                realm.close();

                                DiagnosticsManager.logEvent(mContext,
                                        Tag.LOC_MANUAL_ON,
                                        location,
                                        jsonData.toString());
                            }
                            else {
                                DiagnosticsManager.logEvent(mContext,
                                        Tag.LOC_MANUAL_ON_ERR,
                                        location,
                                        jsonData.toString());
                            }
                        }
                        catch (JSONException e) {
                            SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                        }
                        if (successCallback != null) {
                            successCallback.onResponse(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SLog.e(LOG, "A volley error occurred with: " + Constants.URL_SIGN_ON);

                        JSONObject jsonData = new JSONObject();
                        JSONObject jsonParams = new JSONObject();
                        try {
                            jsonData.put("url", Constants.URL_SIGN_ON);
                            jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                            jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                            if (location != null) {
                                jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                                jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                            }
                            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                            jsonData.put("params", jsonParams);
                            jsonData.put("error", "VolleyError");
                        }
                        catch (JSONException e) {
                            SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                        }
                        DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, location, jsonData.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Signs the user onto a specified site.
     * @param automatic - flag to determine whether attempt is automatic or manual
     * @param siteId - the id of the site which the user is attempting to sign onto
     * @param location - the user's current location
     */
    public String signOnSiteBlocking(final boolean automatic,
                                     final Integer siteId,
                                     final Location location) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_ON);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_ON, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                params.put(Constants.CAN_BLOCK_AUTO_SIGNON, Boolean.toString(true));

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);

        try {
            return future.get(30, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            SLog.e(LOG, "Sign On blocking call was interrupted. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_ON);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_ON_ERR, location, jsonData.toString());
        }
        catch (ExecutionException e) {
            SLog.e(LOG, "Sign On blocking call failed. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_ON);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_ON_ERR, location, jsonData.toString());
        }
        catch (TimeoutException e) {
            SLog.e(LOG, "Sign On blocking call timed out. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_ON);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_ON_ERR, location, jsonData.toString());
        }
        return null;
    }

    /**
     * Signs a user off a given site.
     * @param automatic - flag to determine if the sign off attempt is automatic or not
     * @param siteId - the ID fo the site that the user is signing off from
     * @param location - the user's current location
     * @param successCallback - interface callback to implement app logic within
     */
    public void signOffSite(final boolean automatic,
                  @Nullable final Integer siteId,
                  @Nullable final Location location,
                            final VolleySuccessCallback successCallback,
                            final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_OFF);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_OFF,
                response -> {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        JSONObject jsonData = new JSONObject();
                        jsonData.put("response", response);
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));

                        if (jsonResponse.getBoolean(Constants.JSON_SIGNED_OFF)) {
                            Log.i(LOG, "Response from " + Constants.URL_SIGN_OFF + ": " + response);

                            // Remove induction notification
                            NotificationUtil.cancelNotifications(mContext, NotificationUtil.NOTIFICATION_INDUCTION_ID);

                            // Delete briefing details
                            Realm realm = Realm.getDefaultInstance();
                            BriefingService service = new BriefingService(realm);
                            if (siteId != null) {
                                service.deleteBriefingsForSite(siteId);
                            }
                            else {
                                service.deleteAllBriefings();
                            }
                            realm.close();

                            mSession.clearSiteId();

                            DiagnosticsManager.logEvent(mContext,
                                               Tag.LOC_MANUAL_OFF,
                                               location,
                                               jsonData.toString());
                        }
                        else {
                            DiagnosticsManager.logEvent(mContext,
                                               Tag.LOC_MANUAL_OFF_ERR,
                                               location,
                                               jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    SLog.i(LOG, "User sign off attempt response: " + response);
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_SIGN_OFF);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_SIGN_OFF);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                        if (siteId != null) {
                            jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                        }
                        if (location != null) {
                            jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                            jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                        }
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }

                    DiagnosticsManager.logEvent(mContext, Tag.LOC_MANUAL_OFF_ERR, location, jsonData.toString());

                    if (errorCallback != null) {
                        errorCallback.onResponse();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                if (siteId != null) {
                    params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                }
                if (location != null) {
                    params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                    params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                }
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                params.put(Constants.CAN_BLOCK_AUTO_SIGNON, Boolean.toString(true));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    public void signOffSite(final boolean automatic,
                            @Nullable final Integer siteId,
                            @Nullable final Double lat,
                            @Nullable final Double lon,
                            final VolleySuccessCallback successCallback,
                            final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_OFF);

        Location loc = new Location("derp");
        loc.setLatitude(lat);
        loc.setLongitude(lon);

        final Location location = loc;

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_OFF,
                response -> {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        JSONObject jsonData = new JSONObject();
                        jsonData.put("response", response);
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));

                        if (jsonResponse.getBoolean(Constants.JSON_SIGNED_OFF)) {
                            Log.i(LOG, "Response from " + Constants.URL_SIGN_OFF + ": " + response);

                            // Remove induction notification
                            NotificationUtil.cancelNotifications(mContext, NotificationUtil.NOTIFICATION_INDUCTION_ID);
                            mSession.clearSiteId();

                            // Delete briefing details
                            Realm realm = Realm.getDefaultInstance();
                            BriefingService service = new BriefingService(realm);
                            if (siteId != null) {
                                service.deleteBriefingsForSite(siteId);
                            }
                            else {
                                service.deleteAllBriefings();
                            }
                            realm.close();

                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_OFF,
                                    location,
                                    jsonData.toString());
                        }
                        else {
                            DiagnosticsManager.logEvent(mContext,
                                    Tag.LOC_MANUAL_OFF_ERR,
                                    location,
                                    jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    SLog.i(LOG, "User sign off attempt response: " + response);
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_SIGN_OFF);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_SIGN_OFF);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                        if (siteId != null) {
                            jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                        }
                        if (location != null) {
                            jsonParams.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                            jsonParams.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                        }
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }

                    DiagnosticsManager.logEvent(mContext, Tag.LOC_MANUAL_OFF_ERR, location, jsonData.toString());

                    if (errorCallback != null) {
                        errorCallback.onResponse();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String,String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                if (siteId != null) {
                    params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                }
                if (location != null) {
                    params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                    params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                }
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                params.put(Constants.CAN_BLOCK_AUTO_SIGNON, Boolean.toString(true));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * This is a synchronous call, ie blocking.
     *
     * Signs a user off a given site.
     * @param automatic - flag to determine if the sign off attempt is automatic or not
     * @param siteId - the ID fo the site that the user is signing off from
     * @param location - the user's current location
     */
    public String signOffSiteBlocking(final boolean automatic,
                            @Nullable final Integer siteId,
                            @Nullable final Location location) {
        SLog.i(LOG, "Calling " + Constants.URL_SIGN_OFF);

        RequestFuture<String> future = RequestFuture.newFuture();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_SIGN_OFF, future, future) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                if (siteId != null) {
                    params.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                }
                if (location != null) {
                    params.put(Constants.USER_LATITUDE, Double.toString(location.getLatitude()));
                    params.put(Constants.USER_LONGITUDE, Double.toString(location.getLongitude()));
                }
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                // Is attempt automatic
                params.put(Constants.JSON_AUTOMATIC, String.valueOf(automatic));
                params.put(Constants.CAN_BLOCK_AUTO_SIGNON, Boolean.toString(true));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);

        try {
            return future.get(30, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            SLog.e(LOG, "Sign Off blocking call was interrupted. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_OFF);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_OFF_ERR, location, jsonData.toString());
        }
        catch (ExecutionException e) {
            SLog.e(LOG, "Sign Off blocking call failed. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_OFF);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_OFF_ERR, location, jsonData.toString());
        }
        catch (TimeoutException e) {
            SLog.e(LOG, "Sign Off blocking call timed out. " + e.getMessage());
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                jsonData.put("url", Constants.URL_SIGN_OFF);
                jsonData.put("error", e.getMessage());
            }
            catch (JSONException e1) {
                e1.printStackTrace();
            }
            DiagnosticsManager.logEvent(mContext, Tag.LOC_AUTO_OFF_ERR, location, jsonData.toString());
        }
        return null;
    }

    /**
     * Retrieves the user's current employer
     * @param successCallback - interface callback to implement app logic within
     */
    public void retrieveCurrentEmployer(@Nullable final VolleySuccessCallback successCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_CURRENT_EMPLOYER);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_CURRENT_EMPLOYER,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_CURRENT_EMPLOYER + ": " + response);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        Log.i(LOG, "json response of current employer " + ": " + jsonResponse);

                        if (jsonResponse.getString("msg").equals("Returned a company")) {
                            Integer companyId = jsonResponse.getInt(Constants.JSON_COMPANY_ID);

                            Log.i(LOG, "this is company id : " + companyId);
                            String companyName = jsonResponse.getString(Constants.USER_COMPANY_NAME);

                            Log.i(LOG, "this is the company name " + companyName);

                            Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
                            User realmUser = realm.where(User.class).findFirst();

                            Log.i(LOG, "this is the realm user " + realmUser);
                            if (realmUser != null) {
                                realm.beginTransaction();
                                realmUser.setCompanyId(Long.valueOf(companyId));
                                realmUser.setCompanyName(companyName);
                                realm.copyToRealmOrUpdate(realmUser);
                                realm.commitTransaction();
                            }
                            realm.close();

                            mSession.updateUserDetail(Constants.USER_COMPANY_ID, String.valueOf(companyId));
                            mSession.updateUserDetail(Constants.USER_COMPANY, companyName);
                        }
                        else {
                            JSONObject jsonData = new JSONObject();
                            jsonData.put("url", Constants.URL_CURRENT_EMPLOYER);
                            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                            jsonData.put("error", response);

                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.e(LOG, "JSON Exception occurred: " + e.getMessage());
                        DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, response);
                    }
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_CURRENT_EMPLOYER);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_CURRENT_EMPLOYER);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Registers a new user to the SignOnSite service.
     * @param successCallback - interface callback to implement app logic within
     */
    public void registerNewUser(final VolleySuccessCallback successCallback,
                                final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_REGISTER_USER);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_REGISTER_USER,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_REGISTER_USER + ": " + response);
                    Map<String, String> user = mSession.getCurrentUser();

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        JSONObject jsonParams = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put("url", Constants.URL_REGISTER_USER);
                        jsonParams.put("emp", user.get(Constants.USER_COMPANY));
                        jsonParams.put("fname", user.get(Constants.USER_FIRST_NAME));
                        jsonParams.put("lname", user.get(Constants.USER_LAST_NAME));
                        jsonParams.put(Constants.USER_PHONE, user.get(Constants.USER_PHONE));
                        jsonData.put("params", jsonParams.toString());

                        if (!jsonResponse.getBoolean("registered")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                        else {
                            // Registered successfully, store the JWT
                            String jwt = jsonResponse.getString(Constants.AUTH_TOKEN);
                            Log.i(LOG, "JWT stored: " + jwt);
                            mSession.storeToken(jwt);
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_REGISTER_USER);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_REGISTER_USER);
                    Map<String, String> user = mSession.getCurrentUser();
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_REGISTER_USER);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.USER_EMP, user.get(Constants.USER_COMPANY));
                        jsonParams.put("fname", user.get(Constants.USER_FIRST_NAME));
                        jsonParams.put("lname", user.get(Constants.USER_LAST_NAME));
                        jsonParams.put(Constants.USER_PHONE, user.get(Constants.USER_PHONE));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                Map<String, String> user = mSession.getCurrentUser();
                Map<String, String> details = mSession.getRegistrationDetails();

                // Add relevant parameters to API call
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put("fname", user.get(Constants.USER_FIRST_NAME));
                params.put("lname", user.get(Constants.USER_LAST_NAME));
                params.put(Constants.USER_PHONE, user.get(Constants.USER_PHONE));
                params.put(Constants.USER_LOCALE, details.get(Constants.REGISTER_LOCALE));
                params.put("cardnum", "");
                params.put("issueDate", "");
                params.put("state", "");
                params.put("password", details.get(Constants.REGISTER_PASS));
                params.put(Constants.JSON_EMPLOYER, user.get(Constants.USER_COMPANY));

                SLog.d("REGISTER", params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Checks whether the user entered email is already in use by an account.
     * @param email - the user's proposed email
     * @param successCallback - interface callback to implement app logic within
     */
    public void checkEmailInUse(final String email,
                                final VolleySuccessCallback successCallback,
                                final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_EMAIL_IN_USE);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_EMAIL_IN_USE,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_EMAIL_IN_USE + ": " + response);
                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_EMAIL_IN_USE);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_EMAIL_IN_USE);
                        jsonParams.put("email", email);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, email);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Updates the user's personal details.
     * @param email - the user's email address
     * @param firstName - the submitted first name
     * @param lastName - the submitted last name
     * @param phoneNumber - the user's phone number
     * @param successCallback - interface callback to implement app logic within
     */
    public void editPersonalDetails(final String email,
                                    final String firstName,
                                    final String lastName,
                                    final String phoneNumber,
                                    final String countryCode,
                                    final VolleySuccessCallback successCallback,
                                    final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_EDIT_PERSONAL_DETAILS);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_EDIT_PERSONAL_DETAILS,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_EDIT_PERSONAL_DETAILS + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonParams = new JSONObject();

                        JSONObject jsonData = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_EDIT_PERSONAL_DETAILS);
                        jsonParams.put(Constants.USER_FIRST_NAME, firstName);
                        jsonParams.put(Constants.USER_LAST_NAME, lastName);
                        jsonParams.put(Constants.USER_PHONE, phoneNumber);
                        jsonParams.put(Constants.USER_LOCALE, countryCode);
                        jsonData.put("params", jsonParams.toString());
                        jsonData.put("response", response);

                        if (!jsonResponse.getBoolean("update_ok")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_EDIT_PERSONAL_DETAILS);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_EDIT_PERSONAL_DETAILS);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_EDIT_PERSONAL_DETAILS);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.USER_FIRST_NAME, firstName);
                        jsonParams.put(Constants.USER_LAST_NAME, lastName);
                        jsonParams.put(Constants.USER_PHONE, phoneNumber);
                        jsonParams.put(Constants.USER_LOCALE, countryCode);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, email);
                params.put(Constants.USER_FIRST_NAME, firstName);
                params.put(Constants.USER_LAST_NAME, lastName);
                params.put(Constants.USER_PHONE, phoneNumber);
                params.put(Constants.USER_LOCALE, countryCode);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Endpoint to update user's passwords.
     * @param oldpassword - a user's old password
     * @param newPassword1 - the user's new password
     * @param newPassword2 - the user's new password
     * @param successCallback - interface callback to implement app logic within
     */
    public void changeUserPassword(final String oldpassword,
                                   final String newPassword1,
                                   final String newPassword2, //wtf?
                                   final VolleySuccessCallback successCallback,
                                   final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_CHANGE_PASSWORD);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_CHANGE_PASSWORD,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_CHANGE_PASSWORD + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("response", response);
                        jsonData.put("url", Constants.URL_CHANGE_PASSWORD);
                        if (!jsonResponse.getBoolean("update_ok")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_CHANGE_PASSWORD);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_CHANGE_PASSWORD);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_CHANGE_PASSWORD);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.USER_OLD_PASSWORD, oldpassword);
                params.put(Constants.USER_NEW_PASSWORD_1, newPassword1);
                params.put(Constants.USER_NEW_PASSWORD_2, newPassword2);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Fetches a user's white card info using their email
     * @param successCallback - interface callback to implement app logic within
     */
    public void getWhitecardInfo(final VolleySuccessCallback successCallback,
                                 final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_GET_WHITECARD_INFO);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_GET_WHITECARD_INFO,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_GET_WHITECARD_INFO + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_GET_WHITECARD_INFO);
                        jsonData.put("response", response);

                        if (!jsonResponse.getBoolean("result_ok")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_GET_WHITECARD_INFO);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_GET_WHITECARD_INFO);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_GET_WHITECARD_INFO);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Update's a user's white card information.
     * @param cardNumber - user's whitecard number
     * @param cardIssueDate - user's white card issue date
     * @param cardState - user's white card issuing State
     * @param successCallback - interface callback to implement app logic within
     */
    public void updateWhitecardInfo(final String cardNumber,
                                    final String cardIssueDate,
                                    final String cardState,
                                    final VolleySuccessCallback successCallback,
                                    final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_EDIT_WHITECARD_INFO);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_EDIT_WHITECARD_INFO,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_EDIT_WHITECARD_INFO + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        JSONObject jsonParams = new JSONObject();
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_EDIT_WHITECARD_INFO);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.REGISTER_CARD_NUMBER, cardNumber);
                        jsonParams.put(Constants.REGISTER_CARD_DATE, cardIssueDate);
                        jsonParams.put(Constants.REGISTER_STATE, cardState);
                        jsonData.put("params", jsonParams.toString());
                        jsonData.put("response", response);

                        if (!jsonResponse.getBoolean("update_ok")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_EDIT_WHITECARD_INFO);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_EDIT_WHITECARD_INFO);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_EDIT_WHITECARD_INFO);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.REGISTER_CARD_NUMBER, cardNumber);
                        jsonParams.put(Constants.REGISTER_CARD_DATE, cardIssueDate);
                        jsonParams.put(Constants.REGISTER_STATE, cardState);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.REGISTER_CARD_NUMBER, cardNumber);
                params.put(Constants.REGISTER_CARD_DATE, cardIssueDate);
                params.put(Constants.REGISTER_STATE, cardState);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Returns a list of all employers in the SignOnSite system, so that user's may select
     * which company they work for.
     * @param successCallback - interface callback to implement app logic within
     */
    public void getAllCompanies(final VolleySuccessCallback successCallback,
                                final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_GET_ALL_EMPLOYERS);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_GET_ALL_EMPLOYERS,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_GET_ALL_EMPLOYERS + ": " + response);
                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_GET_ALL_EMPLOYERS);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_GET_ALL_EMPLOYERS);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Updates the user's employer
     * @param newEmployer - the new company that the user is submitting
     * @param successCallback - interface callback to implement app logic within
     */
    public void updateEmployer(final String newEmployer,
                               final VolleySuccessCallback successCallback,
                               final VolleyErrorCallback errorCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_EDIT_USER_COMPANIES);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_EDIT_USER_COMPANIES,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_EDIT_USER_COMPANIES + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();
                        JSONObject jsonParams = new JSONObject();

                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("response", response);
                        jsonData.put("url", Constants.URL_EDIT_USER_COMPANIES);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.USER_EMP, newEmployer);
                        jsonData.put("params", jsonParams);

                        if (!jsonResponse.getBoolean("update_ok")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_EDIT_USER_COMPANIES);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_EDIT_USER_COMPANIES);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_EDIT_USER_COMPANIES);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.USER_EMP, newEmployer);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onResponse();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.JSON_EMPLOYER, newEmployer);
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Sends the received token from the Google Cloud Messaging servers to the SignOnSite servers to
     * be stored. This token allows for our servers to send push notifications to the phone.
     * @param token - GCM token
     * @param successCallback - interface callback to implement app logic within
     */
    public void sendGCMTokenToServer(final String token,
                                     final VolleySuccessCallback successCallback) {
        SLog.i(LOG, "Calling " + Constants.URL_UPDATE_GCM_TOKEN);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_UPDATE_GCM_TOKEN,
                response -> {
                    Log.i(LOG, "Response from " + Constants.URL_UPDATE_GCM_TOKEN + ": " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        JSONObject jsonParams = new JSONObject();
                        JSONObject jsonData = new JSONObject();

                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("response", response);
                        jsonData.put("url", Constants.URL_UPDATE_GCM_TOKEN);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.GCM_TOKEN, token);
                        jsonData.put("params", jsonParams);

                        if (jsonResponse.getBoolean("token updated")) {
                            DiagnosticsManager.logEvent(mContext, Tag.NOTIFICATION_REGISTRATION, null, jsonData.toString());
                        }
                        else {
                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    } catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred with " + Constants.URL_UPDATE_GCM_TOKEN);
                    }

                    successCallback.onResponse(response);
                },
                error -> {
                    SLog.e(LOG, "A volley error occurred with: " + Constants.URL_UPDATE_GCM_TOKEN);
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_UPDATE_GCM_TOKEN);
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonParams.put(Constants.GCM_TOKEN, token);
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.GCM_TOKEN, token);
                params.put(Constants.GCM_DEVICE_TYPE, mContext.getString(R.string.device_type));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }

    /**
     * Sends information regarding the user's phone and the application version to the SoS server.
     * @param successCallback - interface callback to implement app logic within
     */
    public void postPhoneAndAppInfo(@Nullable final VolleySuccessCallback successCallback) {
        Log.i(LOG, "Calling " + Constants.URL_DEVICE_INFO);

        StringRequest request = new StringRequest(Request.Method.POST, Constants.URL_DEVICE_INFO,
                response -> {
                    Log.i(LOG, "Response received from " + Constants.URL_DEVICE_INFO + ": "
                            + response);
                    JSONObject JSONResponse;
                    try {
                        JSONResponse = new JSONObject(response);
                        JSONObject jsonData = new JSONObject();

                        SLog.i(LOG, "Passing JSONObject to tag manager for handling.");
                        DiagnosticsTagManager.shared.handleVersionsResponse(JSONResponse);
                        if (JSONResponse.getString("current_version_code_android") != null) {
                            int mostRecentVersion = JSONResponse.getInt("current_version_code_android");
                            mSession.setPlayStoreVersion(mostRecentVersion);
                            // Check if app is running latest version
                            PackageInfo pInfo = null;
                            try {
                                pInfo = mContext.getPackageManager()
                                        .getPackageInfo(mContext.getPackageName(), 0);
                                SLog.i(LOG, "App version code: " + String.valueOf(pInfo.versionCode));
                            }
                            catch (PackageManager.NameNotFoundException e) {
                                SLog.e(LOG, e.getMessage());
                            }

                            // Store JWT
                            String jwt = JSONResponse.getString(Constants.AUTH_TOKEN);
                            Log.i(LOG, "JWT stored: " + jwt);
                            Realm realm = Realm.getDefaultInstance();
                            UserService service = new UserService(realm);
                            service.updateAuthToken(jwt);
                            mSession.storeToken(jwt);
                        }
                        else {
                            jsonData.put("url", Constants.URL_DEVICE_INFO);
                            jsonData.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                            jsonData.put("response", response);

                            DiagnosticsManager.logEvent(mContext, Tag.NETWORK_FAIL_RESPONSE, null, jsonData.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.d(LOG, "A JSON Exception occurred: " + e.getMessage());
                    }
                    if (successCallback != null) {
                        successCallback.onResponse(response);
                    }
                },
                error -> {
                    Log.e(LOG, "There was a volley error with " + Constants.URL_DEVICE_INFO);
                    JSONObject jsonData = new JSONObject();
                    try {
                        jsonData.put("url", Constants.URL_DEVICE_INFO);
                        JSONObject jsonParams = new JSONObject(new HashMap());
                        jsonParams.put(Constants.USER_EMAIL, mSession.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(mContext, Tag.NETWORK_ERROR, null, jsonData.toString());
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> params = new HashMap<>();
                HashMap<String, String> user = mSession.getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                Log.i(LOG, "Posting phone info: " + params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(mContext);
            }
        };
        // Add request to the request queue
        SOSApplication.getInstance().addToRequestQueue(request, LOG);
    }


    @NonNull
    private static Map<String, String> setAuthHeader(Context context) {
        String jwt = new SessionManager(context).getToken();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + jwt);
        return headers;
    }

    public interface VolleySuccessCallback {
        void onResponse(String response);
    }

    public interface VolleyErrorCallback {
        void onResponse();
    }

}
