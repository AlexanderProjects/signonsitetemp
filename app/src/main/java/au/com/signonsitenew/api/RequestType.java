package au.com.signonsitenew.api;

/**
 * Created by Krishan Caldwell on 4/07/2016.
 */
public class RequestType {

    public static final int GET = 0;
    public static final int POST = 1;
    public static final int PUT = 2;
    public static final int DELETE = 3;
}
