package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.realm.services.BriefingService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class AcknowledgeBriefing {
    private static final String LOG = AcknowledgeBriefing.class.getSimpleName();
    private static final String url = Constants.URL_ACKNOWLEDGE_BRIEFING;

    /**
     * Sends a notification to the SoS server indicating that a user has acknowledged the daily briefing.
     * @param briefingId the briefing ID of the briefing the user is acknowledging
     */
    public static void post(final Context context,
                            final Integer briefingId,
                            final Integer siteId,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_BRIEFING_ID, briefingId.toString());
        params.put(Constants.JSON_SITE_ID, siteId.toString());

        API.post(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                            else {
                                // Acknowledged the briefing successfully
                                Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
                                BriefingService service = new BriefingService(realm);
                                service.setBriefingAcknowledged(briefingId);
                                realm.close();
                            }
                        }
                        catch (JSONException e) {
                            SLog.e(LOG, "A JSONException Occurred: " + e.getMessage());
                        }
                        responseCallback.onResponse(response);
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        SLog.e(LOG, "There was a volley error with " + Constants.URL_ACKNOWLEDGE_BRIEFING);
                        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
                        User user = realm.where(User.class).findFirst();
                        JSONObject jsonData = new JSONObject();
                        JSONObject jsonParams = new JSONObject();
                        try {
                            jsonData.put(Constants.USER_EMAIL, user.getEmail());
                            jsonData.put("url", Constants.URL_ACKNOWLEDGE_BRIEFING);
                            jsonParams.put(Constants.USER_EMAIL, user.getEmail());
                            jsonParams.put(Constants.JSON_SITE_ID, Integer.toString(siteId));
                            jsonParams.put(Constants.BRIEFING_ID, briefingId);
                            jsonData.put("params", jsonParams);
                            jsonData.put("error", "VolleyError");
                        }
                        catch (JSONException e) {
                            SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                        }
                        realm.close();
                        DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_ERROR, null, jsonData.toString());
                        errorCallback.onError();
                    }
                });
    }

}
