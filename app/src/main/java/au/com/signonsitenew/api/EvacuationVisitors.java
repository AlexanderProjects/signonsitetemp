package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.SiteSettings;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */
public class EvacuationVisitors {
    private static final String LOG = EvacuationVisitors.class.getSimpleName();
    private static final String url = Constants.URL_EVACUATION_VISITORS;

    public static void get(final Context context,
                           final API.ResponseCallback responseCallback,
                           final API.ErrorCallback errorCallback) {

        Realm realm = Realm.getDefaultInstance();
        SiteSettingsService settingsService = new SiteSettingsService(realm);
        long siteId = settingsService.getSiteId();
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(siteId));

        API.get(context, url, params, LOG,
            new API.ResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    JSONArray visitsArray;
                    try {
                        if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            // Visitors currently signed on retrieved
                            visitsArray = response.getJSONArray(Constants.JSON_VISITS);
                            Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
                            realm.beginTransaction();
                            realm.where(SiteAttendee.class).findAll().deleteAllFromRealm();

                            for (int i=0; i<visitsArray.length(); i++) {
                                JSONObject person = visitsArray.getJSONObject(i);

                                SiteAttendee visitor = new SiteAttendee();
                                visitor.setUserId(person.getLong(Constants.JSON_VISITOR_ID));
                                visitor.setFirstName(person.getString(Constants.JSON_VISITOR_FIRST_NAME));
                                visitor.setLastName(person.getString(Constants.JSON_VISITOR_LAST_NAME));
                                visitor.setPhoneNumber(person.getString(Constants.JSON_VISITOR_PHONE));
                                visitor.setCompany(person.getString(Constants.JSON_VISITOR_COMPANY));
                                visitor.setCheckInTime(person.getString(Constants.JSON_VISITOR_CHECKIN_TIME));
                                visitor.setMarkedSafe(false);
                                realm.copyToRealmOrUpdate(visitor);
                            }
                            realm.commitTransaction();
                            realm.close();
                        }
                        else {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseCallback.onResponse(response);
                }
            },
            new API.ErrorCallback() {
                @Override
                public void onError() {
                    errorCallback.onError();
                }
            });
    }

}
