package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class VisitorSignOn {
    private static final String LOG = VisitorSignOn.class.getSimpleName();
    private static final String url = Constants.URL_VISITOR_SIGN_ON;

    public static void post(final Context context,
                            final long userId,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(session.getSiteId()));
        params.put(Constants.JSON_VISITOR_ID, String.valueOf(userId));
        params.put(Constants.USER_EMAIL, session.getCurrentUser().get(Constants.USER_EMAIL));

        API.post(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString("status").equals("success")) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        responseCallback.onResponse(response);
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        errorCallback.onError();
                    }
                });
    }
}
