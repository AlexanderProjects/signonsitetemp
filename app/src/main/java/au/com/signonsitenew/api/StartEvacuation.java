package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class StartEvacuation {
    private static final String LOG = StartEvacuation.class.getSimpleName();
    private static final String url = Constants.URL_START_EVACUATION;

    public static void post(final Context context,
                            Boolean actualEmergency,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback,
                            final API.DataErrorCallback dataErrorCallback) {

        JSONObject params = new JSONObject();
        SessionManager session = new SessionManager(context);
        Integer siteId = session.getSiteId();
        String email = session.getCurrentUser().get(Constants.USER_EMAIL);

        try {
            params.put(Constants.JSON_SITE_ID, siteId);
            params.put(Constants.USER_EMAIL, email);
            params.put("actual", actualEmergency);
            params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
        }
        catch (JSONException e) {
            SLog.i(LOG, "JsonException occurred: " + e.getMessage());
        }

        API.post(context,
                url,
                params,
                LOG,
                response -> {
                    try {
                        if (!response.getString("status").equals("success") ||
                                !response.getString("status").equals("already_in_progress")) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseCallback.onResponse(response);
                },
                () -> errorCallback.onError(),
                errorMessage -> dataErrorCallback.onError(errorMessage));
    }

}
