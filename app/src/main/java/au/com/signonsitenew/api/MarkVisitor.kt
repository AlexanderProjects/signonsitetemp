package au.com.signonsitenew.api

import android.content.Context

import org.json.JSONException
import org.json.JSONObject

import java.util.HashMap

import au.com.signonsitenew.diagnostics.DiagnosticsManager
import au.com.signonsitenew.realm.DiagnosticLog
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SLog
import au.com.signonsitenew.utilities.SessionManager

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
object MarkVisitor {
    private val LOG = MarkVisitor::class.java.simpleName

    fun post(context: Context,
             userId: Long,
             responseCallback: API.ResponseCallback,
             errorCallback: API.ErrorCallback) {

        val session = SessionManager(context)
        val siteId = session.siteId.toLong()
        val params = HashMap<String, String>()
        params[Constants.JSON_SITE_ID] = session.siteId.toString()
        params[Constants.JSON_VISITOR_ID] = userId.toString()

        val url = Constants.URL_SITE_INDUCTION_BASE + siteId + "/inductions/users/" + userId + "/visitor"

        API.post(context, url, params, LOG,
                { response ->
                    try {
                        if (response.getString(Constants.JSON_STATUS) != Constants.JSON_SUCCESS) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString())
                        } else {
                            // Successful response
                            responseCallback.onResponse(response)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                { errorCallback.onError() })
    }

}
