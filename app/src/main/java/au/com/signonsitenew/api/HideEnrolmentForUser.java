package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class HideEnrolmentForUser {
    private static final String LOG = HideEnrolmentForUser.class.getSimpleName();

    public static void post(final Context context,
                            final long userId,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        long siteId = session.getSiteId();
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_VISITOR_ID, String.valueOf(userId));

        String url = Constants.BASE + "sites/" + siteId + "/enrolments/hide_for_user";

        API.post(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                            else {
                                // Successful response
                                responseCallback.onResponse(response);
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        errorCallback.onError();
                    }
                });
    }

}
