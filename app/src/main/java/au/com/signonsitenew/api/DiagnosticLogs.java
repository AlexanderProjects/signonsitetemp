package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class DiagnosticLogs {

    private static final String LOG = DiagnosticLogs.class.getSimpleName();
    private static final String url = Constants.URL_DIAGNOSTIC_LOGS;

    public static void post(final Context context, JSONArray logs,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        Map<String,String> params = new HashMap<>();
        final SessionManager session = new SessionManager(context);

        String email = session.getCurrentUser().get(Constants.USER_EMAIL);

        params.put(Constants.USER_EMAIL, email);
        params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        API.bzipJsonRequestWithBody(context, url, params, logs, LOG,
            new API.ResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseCallback.onResponse(response);
                }
            },
            new API.ErrorCallback() {
                @Override
                public void onError() {
                    JSONObject jsonData = new JSONObject();
                    JSONObject jsonParams = new JSONObject();
                    try {
                        jsonData.put(Constants.USER_EMAIL, session.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("url", Constants.URL_CURRENT_EMPLOYER);
                        jsonParams.put(Constants.USER_EMAIL, session.getCurrentUser().get(Constants.USER_EMAIL));
                        jsonData.put("params", jsonParams);
                        jsonData.put("error", "VolleyError");
                    }
                    catch (JSONException e) {
                        SLog.i(LOG, "JSON Exception occurred: " + e.getMessage());
                    }
                    DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_ERROR, null, jsonData.toString());
                    errorCallback.onError();
                }
            });
    }

}
