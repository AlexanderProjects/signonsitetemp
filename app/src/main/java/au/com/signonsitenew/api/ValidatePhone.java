package au.com.signonsitenew.api;

import android.content.Context;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */

public class ValidatePhone {
    private static final String LOG = ValidatePhone.class.getSimpleName();
    private static final String url = Constants.URL_VALIDATE_PHONE;

    public static void get(final Context context,
                           String phoneNumber,
                           String alpha2,
                           final API.ResponseCallback responseCallback,
                           @Nullable final API.ErrorCallback errorCallback) {

        HashMap<String, String> params = new HashMap<>();

        params.put(Constants.JSON_RAW_NUMBER, phoneNumber);
        params.put(Constants.USER_LOCALE, alpha2);

        API.get(context, url, params, LOG,
                response -> {
                    SLog.i(LOG, response.toString());
                    responseCallback.onResponse(response);
                },
                () -> {
                    if (errorCallback != null) {
                        errorCallback.onError();
                    }
                });
    }
}
