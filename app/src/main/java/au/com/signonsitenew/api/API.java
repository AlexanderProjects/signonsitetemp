package au.com.signonsitenew.api;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.domain.models.ApiResponse;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * This class provides the basis for making POST and GET requests to the SignOnSite API. All common
 * operations involved in these, such as setting credentials, are covered here. API calls should
 * extend the methods in here. In the future if additional REST operations are required, the base
 * calls should be added here.
 */
public class API {

    private static final String LOG = API.class.getSimpleName();

    public static void post(final Context context,
                            final String url,
                            JSONObject parameters,
                            final String tag,
                            final ResponseCallback responseListener,
                            final ErrorCallback errorListener,
                            final DataErrorCallback dataErrorListener) {
        Log.i(tag, "Parameters for " + url + ": " + parameters.toString());

        // Add Auth String to params
        try {
            parameters.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
        }
        catch (JSONException e) {
            SLog.e(LOG, "JSON Exception occurred: " + e.getMessage());
        }

        // Create request
        JsonObjectRequest request = new JsonObjectRequest(RequestType.POST, url, parameters,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SLog.i(tag, "Response received from " + url + ": " + response.toString());
                        //Set Default policy for evacuation to false
                        SOSApplication.getInstance().setEmergency(false);

                        responseListener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SLog.e(tag, "A volley error occurred with: " + url);
                        String message = "";
                        if (error != null) {
                            message += error.toString();
                            if (error.networkResponse != null) {
                                message += ". " + error.networkResponse.toString();
                            }
                        }
                        DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, message);
                        if(is500Error(error)){
                            if(!getApiError(error).isEmpty()){
                                dataErrorListener.onError(getApiError(error));
                            }else {
                                errorListener.onError();
                            }
                        }
                    }
             }) {
            @Override
            public Map<String, String> getHeaders() {
                return setAuthHeader(context);
            }
        };

        //Set emergency default policy
        SOSApplication.getInstance().setEmergency(true);
        // post the request to queue
        SLog.i(LOG, "Making request to: " + url);
        SLog.i(LOG, "Params are: " + parameters.toString());
        SOSApplication.getInstance().addToRequestQueue(request, tag);
    }

    private static boolean is500Error(VolleyError error) {
        return (error.networkResponse.statusCode == 500);
    }

    private static String getApiError(VolleyError error) {
        try {
            String json = new String(error.networkResponse.data);
            ApiResponse response = new Gson().fromJson(json, ApiResponse.class);
            if(response != null && !response.getStatus().isEmpty()){
                return response.getStatus();
            }else
                return "";
        }catch (Exception e){
            return "";
        }

    }

    public static void post(final Context context,
                            final String url,
                            final HashMap<String, String> parameters,
                            final String tag,
                            final ResponseCallback responseListener,
                            final ErrorCallback errorListener) {

        // Add Auth String to params
        parameters.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        StringRequest request = new StringRequest(RequestType.POST, url,
                response -> {
                    SLog.i(tag, "Response received from " + url + ": " + response);

                    try {
                        responseListener.onResponse(new JSONObject(response));
                    }
                    catch (JSONException e) {
                        SLog.e(LOG, "JSON Exception occurred. " + e.getMessage());
                    }
                },
                error -> {
                    SLog.e(tag, "A volley error occurred with: " + url);
                    String message = "";
                    if (error != null) {
                        message += error.toString();
                        if (error.networkResponse != null) {
                            message += ". " + error.networkResponse.toString();
                        }
                    }
                    DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, message);
                    errorListener.onError();
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Add Auth String
                parameters.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
                Log.i(LOG, "Parameters: " + parameters);
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(context);
            }
        };
        // post the request to queue
        SOSApplication.getInstance().addToRequestQueue(request, tag);
    }

    public static void get(final Context context,
                           final String url,
                           Map<String,String> urlParams,
                           final String tag,
                           final ResponseCallback responseListener,
                           final ErrorCallback errorListener) {

        JSONObject params = new JSONObject();
        SessionManager session = new SessionManager(context);

        // Add Auth String to params
        urlParams.put(Constants.AUTH_STRING, Constants.AUTH_KEY);
        urlParams.put(Constants.USER_EMAIL, session.getCurrentUser().get(Constants.USER_EMAIL));

        String uri = url + urlEncode(urlParams);
        // create request
        JsonObjectRequest request = new JsonObjectRequest(RequestType.GET, uri, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SLog.i(tag, "Response received from " + url + ": " + response.toString());
                        responseListener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SLog.e(tag, "A volley error occurred with: " + url);
                        String message = "";
                        if (error != null) {
                            message += error.toString();
                            if (error.networkResponse != null) {
                                message += ". " + error.networkResponse.toString();
                            }
                        }
                        DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, message);
                        errorListener.onError();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(context);
            }
        };
        // post the request to queue
        SLog.i(LOG, "Making request to: " + url);
        SLog.i(LOG, "Params are: " + urlParams.toString());
        SOSApplication.getInstance().addToRequestQueue(request, tag);
    }

    /*
        Does a post request with url params and a json body.
     */
    public static void jsonRequestWithBody(final Context context,
                                           final String url,
                                           final Map<String,String> urlParams,
                                           final String body,
                                           final String tag,
                                           final ResponseCallback responseListener,
                                           final ErrorCallback errorListener) {
        SLog.i(LOG, "Parameters for " + url + ": " + urlParams);
        SLog.i(LOG, "JSON Body for " + url + ": " + body);

        // Add Auth String to params
        urlParams.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        String uri = url + urlEncode(urlParams);
        final JsonObjectRequest request = new JsonObjectRequest(RequestType.POST, uri, new JSONObject(),
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    SLog.i(tag, "Response received from " + url + ": " + response.toString());
                    try {
                        if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                    }
                    catch (JSONException e) {
                        SLog.e(LOG, "A JSONException Occurred: " + e.getMessage());
                    }
                    //Set Default policy for evacuation to false
                    SOSApplication.getInstance().setEmergency(false);

                    responseListener.onResponse(response);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    SLog.e(tag, "A volley error occurred with: " + url);
                    String message = "";
                    if (error != null) {
                        message += error.toString();
                        if (error.networkResponse != null) {
                            message += ". " + error.networkResponse.toString();
                        }
                    }
                    DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, message);
                    errorListener.onError();
                }
            }) {
            @Override
            public String getBodyContentType() {
                return String.format("application/json; charset=utf-8");
            }

            // force the body to be JSON encoded by doing it ourselves...
            @Override
            public byte[] getBody() {
                try {
                    return body.getBytes("utf-8");
                }
                catch (UnsupportedEncodingException e) {
                    SLog.e(LOG, "Unsupported Encoding while trying to get the bytes of "
                            + body + " using utf-8");
                    return "".getBytes();
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(context);
            }
        };

        SOSApplication.getInstance().setEmergency(true);

        SLog.i(LOG, "Making request to: " + url);
        SOSApplication.getInstance().addToRequestQueue(request, tag);
    }

    public static void bzipJsonRequestWithBody(final Context context,
                                               final String url,
                                               final Map<String,String> urlParams,
                                               final JSONArray body,
                                               final String tag,
                                               final ResponseCallback responseListener,
                                               final ErrorCallback errorListener) {
        SLog.i(LOG, "Parameters for " + url + ": " + urlParams);
        Log.i(LOG, "JSON Body for " + url + ": " + body);

        // Add Auth String to params
        urlParams.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        String uri = url + urlEncode(urlParams);
        final JsonObjectRequest request = new JsonObjectRequest(RequestType.POST, uri, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        SLog.i(tag, "Response received from " + url + ": " + response.toString());
                        responseListener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SLog.e(tag, "A volley error occurred with: " + url);
                        String message = "";
                        if (error != null) {
                            message += error.toString();
                            if (error.networkResponse != null) {
                                message += ". " + error.networkResponse.toString();
                            }
                        }
                        DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, message);
                        errorListener.onError();
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "multipart/form-data";
            }
            private JSONObject getJSON() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();

                HashMap<String, String> user = new SessionManager(context).getCurrentUser();

                // Add relevant parameters to API call
                params.put(Constants.USER_EMAIL, user.get(Constants.USER_EMAIL));
                params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

                JSONObject json = new JSONObject();
                try {
                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        json.put(entry.getKey(), entry.getValue());
                    }
                    json.put(Constants.JSON_EVENT_LOG, body);
                }
                catch (Exception e) {
                    SLog.e(tag, "A JSON Exception occurred: " + e.getMessage());
                }

                return json;
            }

            // force the body to be JSON encoded by doing it ourselves...
            @Override
            public byte[] getBody() {
                byte[] rawBody;
                try {
                    String json = getJSON().toString();
                    rawBody = json.getBytes();
                }
                catch (Exception e) {
                    /* DO SOMETHIGN?? TODO */
                    SLog.e(LOG, "Couldn't get json bytes for sending logs");
                    return new byte[0];
                }

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                BZip2CompressorOutputStream bzOut;
                try {
                    bzOut = new BZip2CompressorOutputStream(output);
                }
                catch (Exception e) {
                    SLog.e(LOG, "Couldn't make bzip output stream: " + e.getLocalizedMessage());
                    return new byte[0];
                }

                try {
                    bzOut.write(rawBody);
                    bzOut.flush();
                    bzOut.close();
                }
                catch (Exception e) {
                    SLog.e(LOG, "Couldn't write to bzip output stream: " + e.getLocalizedMessage());
                    return new byte[0];
                }
                byte[] out = output.toByteArray();
                Log.i(LOG, "Get body returning for diag: " + out.length);
                return out;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return setAuthHeader(context);
            }
        };

        SOSApplication.getInstance().addToRequestQueue(request, tag);
    }

    /*
        Url encodes a String->String, something like:
            ?site_id=123&email=hello
     */
    private static String urlEncode(Map<String,String> params) {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if(first) builder.append("?");
            else builder.append("&");
            first = false;
            builder.append(Uri.encode(entry.getKey()) + "=" + Uri.encode(entry.getValue()));
        }
        return builder.toString();
    }

    @NonNull
    private static Map<String, String> setAuthHeader(Context context) {
        String jwt = new SessionManager(context).getToken();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + jwt);
        return headers;
    }

    public interface ResponseCallback {
        void onResponse(JSONObject response);
    }

    public interface ErrorCallback {
        void onError();
    }

    public interface DataErrorCallback{
        void onError(String errorMessage);
    }

}
