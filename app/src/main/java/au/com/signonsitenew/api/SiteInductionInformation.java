package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class SiteInductionInformation {
    private static final String LOG = SiteInductionInformation.class.getSimpleName();
    private static final String url = Constants.URL_SITE_INDUCTION_INFO;

    public static void get(final Context context,
                           final API.ResponseCallback responseCallback,
                           final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(session.getSiteId()));

        API.get(context, url, params, LOG,
                response -> {
                    try {
                        if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                        else {
                            // Success - get the Induction object and form check
                            boolean formAvailable = response.getBoolean(Constants.JSON_IND_FORM_AVAILABLE);

                            // Helpers
                            Realm realm = Realm.getDefaultInstance();
                            UserService userService = new UserService(realm);
                            SiteInductionService service = new SiteInductionService(realm);

                            long userId = userService.getCurrentUserId();
                            int siteId = new SessionManager(context).getSiteId();

                            if (formAvailable) {
                                // if inductions are set up on the site, then create an induction object
                                if (!response.isNull(Constants.JSON_INDUCTION)) {
                                    // User has an induction object
                                    JSONObject induction = response.getJSONObject(Constants.JSON_INDUCTION);
                                    int inductionId = induction.getInt(Constants.JSON_INDUCTION_ID);
                                    String inductionType = induction.getString(Constants.JSON_INDUCTION_TYPE);
                                    JSONObject state = induction.getJSONObject(Constants.JSON_INDUCTION_STATE);
                                    String inductionState = state.getString(Constants.JSON_IND_STATE_STRING);
                                    String updatedAt = state.getString(Constants.JSON_IND_STATE_AT);

                                    // Check if user still signed in
                                    if (siteId != 0) {
                                        // Store / Update induction
                                        service.createOrUpdateInduction(inductionId, userId, siteId, inductionType, inductionState, updatedAt);
                                    }
                                }
                                else {
                                    // Form is available, but user has not completed their induction yet, set up the blank induction
                                    Date date = new Date();
                                    android.text.format.DateFormat df = new android.text.format.DateFormat();
                                    String formattedDate = df.format("yyyy-MM-dd hh:mm:ss", date).toString();
                                    service.createOrUpdateInduction(null, userId, siteId, null, null, formattedDate);
                                }
                            }
                            else {
                                // In case the form has been activated and since disabled - remove any inductions for the site
                                service.deleteInductionsForSite(siteId);
                            }
                            realm.close();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseCallback.onResponse(response);
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        if (errorCallback != null) {
                            errorCallback.onError();
                        }
                    }
                });
    }

}
