package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.Company;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */
public class CompaniesForSite {
    private static final String LOG = CompaniesForSite.class.getSimpleName();
    private static final String url = Constants.URL_GET_COMPANIES_REGISTERED;

    public static void get(final Context context,
                           final API.ResponseCallback responseCallback,
                           final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(session.getSiteId()));

        API.get(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                            else {
                                // Successfully retrieved the companies registered for a site
                                Realm realm = Realm.getDefaultInstance();
                                JSONArray companiesArray = response.getJSONArray(Constants.JSON_COMPANIES);

                                // Store Companies
                                realm.beginTransaction();
                                realm.where(Company.class).findAll().deleteAllFromRealm();
                                realm.commitTransaction();

                                realm.beginTransaction();
                                // Store Companies registered information
                                for (int i=0; i<companiesArray.length(); i++) {
                                    JSONObject company = companiesArray.getJSONObject(i);

                                    Company realmCompany = new Company();
                                    realmCompany.setCompanyId(company.getLong(Constants.JSON_REGISTERED_COMPANY_ID));
                                    realmCompany.setName(company.getString(Constants.JSON_REGISTERED_COMPANY_NAME));
                                    realm.copyToRealmOrUpdate(realmCompany);
                                }
                                realm.commitTransaction();
                                realm.close();
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        responseCallback.onResponse(response);
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        errorCallback.onError();
                    }
                });
    }
}