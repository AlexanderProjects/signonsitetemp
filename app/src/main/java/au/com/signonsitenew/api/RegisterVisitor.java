package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class RegisterVisitor {
    private static final String LOG = RegisterVisitor.class.getSimpleName();
    private static final String url = Constants.URL_REGISTER_VISITOR;

    /**
     * Makes a post request to sign a user onto a given site. This call should only be accessible by
     * People with authority on site (ie Company Admins of various forms).
     * @param context
     * @param siteId
     * @param firstName
     * @param lastName
     * @param companyName
     * @param phoneNumber
     * @param responseCallback
     * @param errorCallback
     *
     * @returns JSONObject with a status response.
     */
    public static void post(final Context context,
                            final long siteId,
                            final String firstName,
                            final String lastName,
                            final String companyName,
                            final String phoneNumber,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        SessionManager session = new SessionManager(context);
        HashMap<String,String> params = new HashMap<>();
        params.put(Constants.JSON_SITE_ID, String.valueOf(siteId));
        params.put(Constants.JSON_VISITOR_FIRST_NAME, firstName);
        params.put(Constants.JSON_VISITOR_LAST_NAME, lastName);
        params.put(Constants.JSON_VISITOR_COMPANY_NAME, companyName);
        params.put(Constants.JSON_VISITOR_PHONE, phoneNumber);
        params.put(Constants.USER_EMAIL, session.getCurrentUser().get(Constants.USER_EMAIL));

        API.post(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        responseCallback.onResponse(response);
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        errorCallback.onError();
                    }
                });
    }
}
