package au.com.signonsitenew.api;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class StopEvacuation {
    public static class LogEntry {
        long user_id;
        boolean logged;

        public LogEntry(long user_id, boolean logged) {
            this.user_id = user_id;
            this.logged = logged;
        }

        public JSONObject toJSON() {
            JSONObject json = new JSONObject();
            try {
                json.put(Constants.JSON_VISITOR_ID, user_id);
                json.put(Constants.JSON_VISITOR_LOGGED, logged);
                return json;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private static final String LOG = StopEvacuation.class.getSimpleName();
    private static final String url = Constants.URL_STOP_EVACUATION;

    public static void post(final Context context, LogEntry[] rollcall,
                            final API.ResponseCallback responseCallback,
                            final API.ErrorCallback errorCallback) {

        Map<String,String> params = new HashMap<>();
        SessionManager session = new SessionManager(context);

        Integer siteId = session.getSiteId();
        String email = session.getCurrentUser().get(Constants.USER_EMAIL);

        params.put(Constants.JSON_SITE_ID, siteId.toString());
        params.put(Constants.USER_EMAIL, email);
        params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        JSONArray json = new JSONArray();
        for(LogEntry entry : rollcall) {
            JSONObject jsonEntry = entry.toJSON();
            if(jsonEntry != null) {
                json.put(jsonEntry);
            }
        }

        API.jsonRequestWithBody(context, url, params, json.toString(), LOG,
            new API.ResponseCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (!response.getString("status").equals("success")) {
                            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseCallback.onResponse(response);
                }
            },
            new API.ErrorCallback() {
                @Override
                public void onError() {
                    errorCallback.onError();
                }
            });
    }

}
