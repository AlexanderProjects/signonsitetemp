package au.com.signonsitenew.api;

import android.content.Context;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class IsEmergency {

    private static final String LOG = IsEmergency.class.getSimpleName();
    private static final String url = Constants.URL_IS_EMERGENCY;

    public static void post(final Context context, LocationManager locationManager,
                            final API.ResponseCallback responseCallback,
                            @Nullable final API.ErrorCallback errorCallback) {

        HashMap<String, String> params = new HashMap<>();
        final SessionManager session = new SessionManager(context);
        int siteId = session.getSiteId();

        params.put(Constants.JSON_SITE_ID, String.valueOf(siteId));
        params.put(Constants.AUTH_STRING, Constants.AUTH_KEY);

        API.post(context, url, params, LOG,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean(Constants.IS_EMERGENCY)) {
                                session.setEmergencyStatus(true);
                                locationManager.refresh();
                            }
                            else if (!response.getBoolean(Constants.IS_EMERGENCY)) {
                                session.setEmergencyStatus(false);
                                locationManager.refresh();
                            }
                            else if (!response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.NETWORK_FAIL_RESPONSE, null, response.toString());
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        responseCallback.onResponse(response);
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        if (errorCallback != null) {
                            errorCallback.onError();
                        }
                    }
                });
    }
}
