package au.com.signonsitenew.jobscheduler;

import android.app.Notification;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * This service sends diagnostics in the device to the SignOnSite servers.
 */
public class DiagnosticLogFlushJobService extends JobService {
    private static final String TAG = DiagnosticLogFlushJobService.class.getSimpleName();

    private static final int JOB_ID = 2;
    private static final int HALF_DAY = 1000*60*60*12;

    public static void schedule(Context context) {
        // Only schedule when the user has enabled Diagnostics
        SessionManager sessionManager = new SessionManager(context);
        if (!sessionManager.diagnosticsIsEnabled()) {
            // The user has not enabled diagnostics, stop here.
            Log.i(TAG, "User has not enabled diagnostics, not scheduling service");
            return;
        }

        // Check that job is not already scheduled.
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobIsScheduled(jobScheduler)) {
            return;
        }

        ComponentName component = new ComponentName(context, DiagnosticLogFlushJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, component)
                // schedule to run any time between 12 and 24 hours
                .setPeriodic(HALF_DAY) // Repeats the job every half day
                .setPersisted(true) // Job is set again when device restarts
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // Network type is required for jobs that access the network

        jobScheduler.schedule(builder.build());
    }

    public static void cancel(Context context) {
        Log.i(TAG, "Cancelling " + TAG);
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        // Check if user has enabled diagnostics
        SessionManager sessionManager = new SessionManager(this);
        if (!sessionManager.diagnosticsIsEnabled()) {
            // The user has not enabled diagnostics, stop here.
            Log.i(TAG, "User has not enabled diagnostics, stopping service and cancelling job");

            cancel(this);
            return false;
        }

        // Flush logs
        SLog.d(TAG, "Starting log flush check");

        DiagnosticsManager.flushLogs(DiagnosticLogFlushJobService.this, new DiagnosticsManager.SuccessCallback() {
            @Override
            public void onFlushComplete(JSONObject response) {
                SLog.i(TAG, "Log response was: " + response.toString());
                Boolean success = false;
                try {
                    success = response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS);
                }
                catch (JSONException e) {
                    Log.e(TAG, "A JSON Exception Occurred: " + e.getMessage());
                }
                NotificationUtil.createDebugNotification(DiagnosticLogFlushJobService.this,
                        "Logs Flushed",
                        "Logs were flushed successfully");
                jobFinished(params, !success);
                stopSelf();
            }
        });

        return true; // Return false when all work is finished. Return true if you are spawning background work, then call jobFinished from the other thread.
    }

    /**
     * Called if the job is cancelled before being finished. Return true if you want to RETRY in this case
     */
    @Override
    public boolean onStopJob(JobParameters params) {
        // whether or not you would like JobScheduler to automatically RETRY your failed job.
        Log.i(TAG, "Stopped " + TAG);

        return false;
    }

    /**
     * Checks whether this job has been scheduled to run already.
     * @return
     */
    public static boolean jobIsScheduled(JobScheduler scheduler) {
        List<JobInfo> jobs = scheduler.getAllPendingJobs();

        // This is a weird way to do it... but until we support API 24 it seems the only way.
        for ( JobInfo jobInfo : jobs ) {
            if (jobInfo.getId() == JOB_ID ) {

                // The job is running!
                return true;
            }
        }
        return false;
    }

}
