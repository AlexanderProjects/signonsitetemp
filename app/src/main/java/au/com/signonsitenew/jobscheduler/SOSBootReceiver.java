package au.com.signonsitenew.jobscheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */

public class SOSBootReceiver extends BroadcastReceiver {
    final private String LOG = SOSBootReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        SLog.i(LOG, "Device reboot intent received.");
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            RegionFetcherJobService.schedule(context);
            DiagnosticLogFlushJobService.schedule(context);
            DeviceInfoJobService.schedule(context);
        }
    }
}