package au.com.signonsitenew.jobscheduler;

import android.Manifest;
import android.app.Notification;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.concurrent.Semaphore;
import javax.inject.Inject;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl;
import au.com.signonsitenew.locationengine.GeofenceManager;
import au.com.signonsitenew.locationengine.GetLocation;
import au.com.signonsitenew.locationengine.LocationEngineBridge;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.locationengine.NetworkRegionFetcher;
import au.com.signonsitenew.models.Region;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.utilities.LogoutUtil;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import dagger.android.AndroidInjection;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Fetches the inducted region list for the user from the server.
 *
 * This service is critical to the operation of the Location Engine.
 */

public class RegionFetcherJobService extends JobService {

    @Inject
    SignOnStatusUseCaseImpl signOnStatusUseCaseImpl;

    @Inject
    LocationEngineBridge mBridge;

    @Inject
    GeofenceManager geofenceManager;

    @Inject
    LocationManager locationManager;

    private static final String LOG = "LocEng-" + RegionFetcherJobService.class.getSimpleName();
    private static final int JOB_ID = 1;
    private static final int HALF_DAY = 1000*60*60*12;
    private final Semaphore mMutex = new Semaphore(1);

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();

        // Start Foreground Notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            SLog.d(LOG, "Creating Foreground Notification");
            Notification notification = NotificationUtil.buildRegionFetcherNotification(this);
            startForeground(NotificationUtil.NOTIFICATION_LOCATION_SERVICE, notification);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        SLog.i(LOG, "Stopping service");
    }

    public static void schedule(Context context) {
        // This job unlike the others is allowed to run every time it is called. This allows users to
        // force update regions when the app is opened.
        ComponentName component = new ComponentName(context, RegionFetcherJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, component)
                .setPeriodic(HALF_DAY)
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // Network type is required for all jobs that use network access

        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());
    }

    public static void cancel(Context context) {
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        SLog.d(LOG, "onStartJob");
        SessionManager session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            // User is not logged in, cancel service
            cancel(getApplicationContext());
            LogoutUtil.logoutUser(this,locationManager);
            return false;
        }

        // Check if Location Permissions granted
        if (ActivityCompat.checkSelfPermission(RegionFetcherJobService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            cancel(getApplicationContext());
            return false;
        }

        if (!mMutex.tryAcquire()) {
            SLog.d(LOG, "mutex taken");
            return false;
        }
        SLog.d(LOG, "got mutex");

        //check if a user is currently signed on any site
        if(signOnStatusUseCaseImpl != null)
        signOnStatusUseCaseImpl.getUserStatusAsync(aBoolean ->{
            SLog.d(RegionFetcherJobService.class.getName(), "SignOn Status: " + aBoolean);
            return null;
        });

        Thread thread = new Thread() {
            @Override
            public void run() {
                Location location = new GetLocation(RegionFetcherJobService.this).getLocation();
                if(location == null) {
                    SLog.d(LOG, "couldn't get location");
                    mMutex.release();
                    jobFinished(params, true);
                    stopSelf();
                    return;
                }
                Region[] regions = new NetworkRegionFetcher(RegionFetcherJobService.this).getRegions(location);
                if (regions.length == 0) {
                    mMutex.release();
                    jobFinished(params, false);
                    stopSelf();
                    return;
                }

                // Log the user's location
                DiagnosticsManager.logEvent(RegionFetcherJobService.this, DiagnosticLog.Tag.LOC_REGION, location, null);

                // if we are signed on to a site and are outside that site
                // then we should trigger location monitoring for two minutes
                Integer siteId = mBridge.getCurrentSite();
                if(siteId != null) {
                    int site = siteId;
                    boolean siteInRegions = false;
                    // check if any region is our current site
                    for(Region region : regions) {
                        if(region.id == site) {
                            siteInRegions = true;
                            if(!region.sortOfIntersects(location,site)) {
                                // we are outside of the site, so trigger monitoring
                                locationManager.monitorLocationForTwoMinutes();

                                JSONObject jsonData = new JSONObject();
                                try {
                                    jsonData.put("reason", "exit");
                                    jsonData.put("info", "re-fetched regions outside of region user is signed onto");
                                    jsonData.put("siteId", siteId.toString());
                                }
                                catch (JSONException e) {
                                    SLog.e(LOG, "JsonException occurred: " + e.getMessage());
                                }
                                DiagnosticsManager.logEvent(getApplicationContext(), DiagnosticLog.Tag.REFETCH_TRIGGER, location, jsonData.toString());
                            }
                            break;
                        }
                    }
                    // our site isn't in our region list anymore, so sign off
                    if(!siteInRegions) {
                            mBridge.doSignOff(null, location);
                        }
                }


                SLog.d(LOG, "Instantiated Geofence manager!!!!");

                if(geofenceManager.setRegions(regions, true)) {
                    mBridge.setRegions(regions);
                    SLog.d(LOG, "Set Regions successfully");
                }
                else {
                    SLog.d(LOG, "Manager failed to set Regions");
                }
                mMutex.release();
                jobFinished(params, false);
                stopSelf();
            }
        };
        thread.start();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}