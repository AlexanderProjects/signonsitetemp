package au.com.signonsitenew.jobscheduler;

import android.app.Notification;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;

import java.util.List;

import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Sends information about the phone to SignOnSite's servers.
 */

public class DeviceInfoJobService extends JobService {
    private static final String LOG = DeviceInfoJobService.class.getSimpleName();

    private static final int JOB_ID = 0;
    private static final int HALF_DAY = 1000*60*60*12;

    public static void schedule(Context context) {
        // First check if job is already running
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobIsScheduled(jobScheduler)) {
            return;
        }

        ComponentName component = new ComponentName(context, DeviceInfoJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, component)
                // schedule to run any time between 12 and 24 hours
                .setPeriodic(HALF_DAY) // Repeats the job every half day
                .setPersisted(true) // Job is set again when device restarts
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // Network type is required for jobs that access the network

        jobScheduler.schedule(builder.build());
    }

    public static void cancel(Context context) {
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(JOB_ID);
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        SLog.d(LOG, "Starting version check");

        // Post the app version information
        SOSAPI sosAPI = new SOSAPI(this);
        sosAPI.postPhoneAndAppInfo(new SOSAPI.VolleySuccessCallback() {
            @Override
            public void onResponse(String result) {
                NotificationUtil.createDebugNotification(DeviceInfoJobService.this,
                        "Version Alarm",
                        "Version alarm was fired successfully");
                SLog.i(LOG, "Version check was successful");
                jobFinished(params, false);
                stopSelf();
            }
        });
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    /**
     * Checks whether this job has been scheduled to run already.
     * @return
     */
    public static boolean jobIsScheduled(JobScheduler scheduler) {
        List<JobInfo> jobs = scheduler.getAllPendingJobs();

        // This is a weird way to do it... but until we support API 24 it seems the only way.
        for ( JobInfo jobInfo : jobs ) {
            if (jobInfo.getId() == JOB_ID ) {

                // The job is running!
                return true;
            }
        }
        return false;
    }

}