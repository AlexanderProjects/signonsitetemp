package au.com.signonsitenew.data.factory.datasources.local

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.realm.SiteInduction


interface DataLocalSource {
    fun saveCompaniesInDb(companies:List<Company>)
    fun saveOffsetForBriefings(value: String)
    fun getOffsetValue():String
    fun clearOffsetValue()
    fun saveActiveBriefing(briefing: WorkerBriefing, siteId:Int)
    fun saveLinkedUserId(userId:Long)
    fun getSavedPreviousLikedUserId():Long
    fun deleteBriefings()
    fun getCurrentSiteName(siteId: Int):String
    fun saveInduction(inductionId:Int?,userId:Long,siteId:Int,inductionType:String?,inductionState:String?,updateAt:String?)
    fun deleteInduction(siteId: Int)
    fun saveSiteVisitorsForEvacuations(visitors:List<EvacuationVisitor>)
    fun getEvacuationVisitors():List<StopEvacuationRequest>
    fun removeSiteVisits()
    fun getSiteInductionForUser(userId: Long, siteId: Long): SiteInduction?
    fun canAccessUserInductions(userId: Long, siteId: Long): Boolean
    fun hasPermitManagementPermission(userId: Long, siteId: Long): Boolean
    fun getSiteTimeZone(siteId: Int):String?
    fun getLocalUserFullName():String
}