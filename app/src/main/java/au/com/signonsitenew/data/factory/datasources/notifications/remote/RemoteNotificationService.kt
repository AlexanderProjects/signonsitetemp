package au.com.signonsitenew.data.factory.datasources.notifications.remote

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import au.com.signonsitenew.data.factory.datasources.notifications.NotificationsChannels
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.ui.attendanceregister.AttendanceActivity
import au.com.signonsitenew.ui.prelogin.SplashActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.buildEmergencyNotification
import au.com.signonsitenew.utilities.buildNotification
import com.google.firebase.installations.FirebaseInstallations

class RemoteNotificationService(val context: Context?) : DataRemoteNotificationSource, NotificationsChannels() {

    override fun remoteBriefingNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupChannel(context, Constants.BRIEFING_CHANNEL_NAME, Constants.BRIEFING_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.BRIEF_CHANNEL_ID)
        val intent = Intent(context, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
        intent.putExtra(Constants.BRIEFING_NOTIFICATION, true)
        intent.putExtra(Constants.HAS_METADATA, extraProperties)
        intent.putExtra(Constants.NOTIFICATION_SENT_AT, utcNotificationSentAt)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.action = Constants.BRIEFING_REMOTE_ACTION
        notificationName?.let { intent.putExtra(Constants.BRIEFING_NOTIFICATION_TYPE, it) }

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            message?.let {
                buildNotification(context, message, contentIntent, Constants.NOTIFICATION_BRIEFING_ID, Constants.BRIEF_CHANNEL_ID)
            }
        }
        analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties!!, utcNotificationSentAt)
    }

    override fun remoteRejectionInductionNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupChannel(context, Constants.INDUCTION_CHANNEL_NAME, Constants.INDUCTION_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.INDUCTION_CHANNEL_ID)
        val intent = Intent(context, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.TAB_FUNCTION, Constants.SITE_INDUCTION)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
        intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_INDUCTION_REJECTED)

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            message?.let { it1 -> buildNotification(it, it1, contentIntent, Constants.NOTIFICATION_INDUCTION_ID, Constants.INDUCTION_CHANNEL_ID) }
        }
        analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties!!, utcNotificationSentAt)
    }

    override fun remoteDefaultNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupChannel(context, Constants.DEFAULT_CHANNEL_NAME, Constants.DEFAULT_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.DEFAULT_CHANNEL_ID)

        val intent = Intent(context, SplashActivity::class.java)
        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            message?.let {
                it1 -> buildNotification(
                    it, it1, contentIntent,
                    Constants.NOTIFICATION_DEFAULT_ID, Constants.DEFAULT_CHANNEL_ID)
            }
        }
        analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties!!, utcNotificationSentAt)
    }

    override fun unRegisterInstanceId() {
        try{
            FirebaseInstallations.getInstance().delete()
        }catch (exception: Exception){
            exception.message?.let { Log.d(RemoteNotificationService::class.java.name, it) }
        }
    }

    override fun remoteRiskyWorkerNotification(message: String?, userId: String, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupChannel(context, Constants.RISKY_WORKER_CHANNEL_NAME, Constants.RISKY_WORKER_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.RISKY_WORKER_CHANNEL_ID)
        val intent = Intent(context, AttendanceActivity::class.java)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
        intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_WORKER_NOTE_ALERT)
        intent.putExtra(Constants.USER_ID, userId)
        notificationName?.let { intent.putExtra(Constants.RISKY_WORKER_NOTIFICATION_TYPE, it) }

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            message?.let { it1 -> buildNotification(it, it1, contentIntent, Constants.NOTIFICATION_RISKY_WORKER_ID, Constants.RISKY_WORKER_CHANNEL_ID) }
        }
        extraProperties?.let { analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties, utcNotificationSentAt) }

    }

    override fun evacuationStarted(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupEmergencyChannel(context, Constants.EMERGENCY_CHANNEL_NAME, Constants.EMERGENCY_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.EMERGENCY_CHANNEL_ID)
        val intent = Intent(context, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
        intent.putExtra(Constants.EMERGENCY_NOTIFICATION, true)
        intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_EVACUATION_STARTED)
        notificationName?.let { intent.putExtra(Constants.EMERGENCY_STARTED_NOTIFICATION_TYPE, it) }
        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            if (message != null) {
                buildEmergencyNotification(it, message, contentIntent, Constants.NOTIFICATION_EVACUATION_ID, Constants.EMERGENCY_CHANNEL_ID)
            }
        }
        analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties!!, utcNotificationSentAt)
    }

    override fun evacuationEnded(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) {
        setupChannel(context, Constants.EMERGENCY_CHANNEL_NAME, Constants.EMERGENCY_CHANNEL_DESCRIPTION, NotificationManager.IMPORTANCE_HIGH, Constants.EMERGENCY_END_CHANNEL_ID)
        val intent = Intent(context, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
        intent.putExtra(Constants.EMERGENCY_NOTIFICATION, true)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_EVACUATION_ENDED)
        notificationName?.let { intent.putExtra(Constants.EMERGENCY_ENDED_NOTIFICATION_TYPE, it) }
        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            if (message != null) {
                buildNotification(it, message, contentIntent, Constants.NOTIFICATION_END_EVACUATION_ID, Constants.EMERGENCY_END_CHANNEL_ID)
            }
        }
        analyticsEventDelegateService!!.notificationReceived(notificationName!!, Constants.ANALYTICS_REMOTE_NOTIFICATION, extraProperties!!, utcNotificationSentAt)
    }
}