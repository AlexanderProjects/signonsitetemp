package au.com.signonsitenew.data.factory.datasources.analytics

import com.segment.analytics.Analytics
import com.segment.analytics.Properties

class AnalyticsService(private val segAnalytics: Analytics) : DataAnalyticsSource {

    override fun sendTrackEvent(event: String, properties: Properties?) {
        segAnalytics.track(event, properties)
    }
    override fun sendScreenEvent(name: String?, properties: Properties?) {
        segAnalytics.screen(name, properties)
    }
    override fun callIdentity(userId: String)  = segAnalytics.identify(userId)

}