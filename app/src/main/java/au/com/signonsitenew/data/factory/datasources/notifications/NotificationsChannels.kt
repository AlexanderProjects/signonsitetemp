package au.com.signonsitenew.data.factory.datasources.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.Constants

abstract class NotificationsChannels {
    fun setupChannel(context: Context?, channelName: String, channelDescription: String, importance: Int, channelId: String) {
        if (context != null) {
            deleteOldEvacChannel(context)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // The user-visible name of the channel.
            val name: CharSequence = channelName

            // The user-visible descriptions and importance of the channels.
            var description = channelDescription
            val channel = NotificationChannel(channelId, name, importance)

            // Configure the notification channel.
            channel.description = description
            channel.enableLights(true)
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            channel.lightColor = Color.YELLOW
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(250, 250, 250, 250)
            channel.canShowBadge()
            mNotificationManager.createNotificationChannel(channel)
        }
    }
    fun setupEmergencyChannel(context: Context?, channelName: String, channelDescription: String, importance: Int, channelId: String) {
        // Delete old channel if it exists
        if (context != null) {
            deleteOldEvacChannel(context)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // The user-visible name of the channel.
            val name: CharSequence = channelName

            // The user-visible descriptions and importance of the channels.
            var description = channelDescription
            val channel = NotificationChannel(channelId, name, importance)
            val audioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build()

            //Sound to be played
            val alarmSound = Uri.parse("android.resource://"
                    + context.packageName + "/" + R.raw.siren)

            // Configure the notification channel.
            channel.description = description
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 1000, 100, 1000, 100, 1000, 100, 1000, 100, 1000)
            channel.setSound(alarmSound, audioAttributes)
            channel.canBypassDnd()
            mNotificationManager.createNotificationChannel(channel)
        }
    }
    private fun deleteOldEvacChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            try {
                notificationManager.deleteNotificationChannel(Constants.OLD_EVAC_CHANNEL_ID)
            } catch (e: NullPointerException) {
                Log.e(NotificationsChannels::class.java.name, "Null pointer: " + e.message)
            }
        }
    }


}