package au.com.signonsitenew.data.factory.datasources.notifications.local

import au.com.signonsitenew.domain.models.state.AttendanceNotificationState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService

interface DataLocalNotificationSource {
    fun localSignOnPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int)
    fun localSignOffPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int)
    fun localAttendanceNotification(isSignedOn: Boolean, siteName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int, attendanceNotificationState: AttendanceNotificationState)
    fun cancelNotifications(notificationId:Int)
}