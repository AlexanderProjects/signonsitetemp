package au.com.signonsitenew.data.factory.datasources.net

import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.SiteContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.models.*
import au.com.signonsitenew.utilities.mapNetworkErrors
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class RestService(private val retrofit: Retrofit,
                  private val retrofitWithNulls: Retrofit,
                  private val retrofitWithMap:Retrofit) : DataNetSource {

    //GET service for Passport

    override fun get(userId: String, bearerToken: String, callback: Callback<UserInfoResponse>) {
        retrofit.create(SOSRestServiceApi::class.java)
                .getUserInfo(userId, bearerToken).enqueue(callback)
    }

    //GET service for Passport using Rx
    override operator fun get(userId: String, bearerToken: String): Single<UserInfoResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getUserInfoRx(userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //GET phone validation using Rx
    override fun get(alpha2: String, raw: String, email: String, auth: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getPhoneValidationRx(alpha2, raw, email, auth)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //GET personal info
    override fun getPersonalInfo(userId: String, bearerToken: String): Single<UserInfoResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getUserInfoRx(userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //PATCH service for Passport using Rx
    override fun updatePersonalInfo(userRequest: UserInfoUpdateRequest, userId: String, bearerToken: String): Single<UpdateInfoResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .updateUserInfoRx(userRequest, userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    //PATCH service for Passport
    override fun patch(userUpdateRequest: UserInfoUpdateRequest, userId: String, bearerToken: String, callback: Callback<UpdateInfoResponse>) {
        retrofit.create(SOSRestServiceApi::class.java)
                .updateUserInfo(userUpdateRequest, userId, bearerToken)
                .enqueue(callback)
    }

    //GET service for credentials using Rx
    override fun getCredentials(userId: String, bearerToken: String): Single<CredentialsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getCredentialsRx(userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    //POST service for update credentials using Rx
    override fun updateCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .updateCredentialRx(request, userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //POST service for create credentials using Rx
    override fun createCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .createCredentialRx(request, userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //Get service for credential types using Rx
    override fun getCredentialTypes(bearerToken: String): Single<CredentialTypesResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getCredentialTypesRx(bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //Delete credential service using Rx
    override fun deleteCredential(credentialId: String, bearerToken: String): Single<DeleteCredentialResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .deleteCredential(credentialId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getEnrolments(userId: String, bearerToken: String): Single<EnrolmentResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .getEnrolments(userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    override fun updateEnrolments(enrolmentId: String, isAutomaticAttendanceEnabled: String, bearerToken: String): Single<EnrolmentUpdateResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .updateEnrolments(enrolmentId, isAutomaticAttendanceEnabled, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getFeatureFlags(userId: String, bearerToken: String): Single<FeatureFlagsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getFeatureFlags(userId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getCompanyFeatureFlags(siteId: String, bearerToken: String): Single<CompanyFeatureFlagResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getCompanyFeatureFlags(siteId, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }


    override fun sharePassport(sharePassportRequest: SharePassportRequest, userId: String, token: String): Single<SharePassportResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .sharePassport(userId = userId, sharePassportRequest = sharePassportRequest, authHeader = token)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    //POST service for upload image
    override fun uploadImages(file: File, fileName: String): Single<UploadImageResponse> {
        val requestFile: RequestBody = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val part: MultipartBody.Part = MultipartBody.Part.createFormData("file", fileName, requestFile)
        val reqFileName: RequestBody = fileName.toRequestBody("text/plain".toMediaTypeOrNull())
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .uploadImageRx(part, reqFileName)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getPhotoUrl(accessKey: String): Single<RetrieveImageResponse> {
        return retrofit.create(SOSRestServiceApi::class.java).getImageUrl(accessKey)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getCompaniesForSite(siteId: String, userEmail: String, token: String, bearerToken: String): Single<CompaniesForSiteResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getCompaniesForSite(siteId, email = userEmail, auth = token, authHeader = bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getNearBySites(userId: String, bearerToken: String, latitude: Float, longitude: Float, uncertainty: Float): Single<NearSitesResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getNearBySites(userId, bearerToken, latitude, longitude, uncertainty)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun validateEmail(emailVerificationRequest: EmailVerificationRequest): Single<EmailVerificationResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .validateEmail(emailVerificationRequest)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun validatePhoneNumber(alpha2: String, raw: String, email: String, auth: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getPhoneValidationRx(alpha2, raw, email, auth)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getListOfEmployers(employersRequest: EmployersRequest): Single<EmployersResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getAllEmployers(employersRequest)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    override fun userRegistration(userRegistrationRequest: UserRegistrationRequest): Single<UserRegistrationResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .userRegistration(userRegistrationRequest)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    override fun getListOfBriefings(siteId: String, limit: String, offSet: String, bearerToken: String): Single<BriefingsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getListOfBriefingsRx(siteId, limit, offSet, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getActiveBriefings(siteId: String, authHeader: String): Single<BriefingWorkerResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getActiveBriefings(siteId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun acknowledgeBriefing(briefingId: String, authHeader: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .acknowledgeBriefing(briefingId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()

    }

    override fun updateBriefingContent(authHeader: String, siteId: String, briefingContent: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .updateBriefing(authHeader, siteId, briefingContent)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun seenBriefing(briefingId: String, authHeader: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .seenBriefing(briefingId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getTodaysVisits(siteId: String, userEmail: String, token: String, bearerToken: String): Single<TodaysVisitsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getTodaysVisits(siteId, userEmail, token, bearerToken)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getUserContextForAnalytics(platform: String, userId: String, bearerToken: String): Single<UserContextAnalyticsResponse> {
        return retrofitWithMap.create(SOSRestServiceApi::class.java)
                .getUserContext(userId, platform, bearerToken)
    }

    override fun getSiteContextForAnalytics(platform: String, siteId: String, bearerToken: String): Single<SiteContextAnalyticsResponse> {
        return retrofitWithMap.create(SOSRestServiceApi::class.java)
                .getSiteContext(siteId, platform, bearerToken)

    }

    override fun getCompanyContextAnalytics(platform: String, companyId: String, bearerToken: String): Single<CompanyContextAnalyticsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getCompanyContext(companyId, platform, bearerToken)

    }

    override fun retrieveSignedOnStatus(bearerToken: String, userEmail: String, token: String): Maybe<Map<String, Any>> {
        return retrofitWithMap.create(SOSRestServiceApi::class.java)
                .retrieveSignedOnStatus(bearerToken, userEmail, token)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getActiveInductions(bearerToken: String, email: String, token: String, siteId: String): Single<InductionResponse> {
        return retrofitWithNulls.create(SOSRestServiceApi::class.java)
                .getActiveInductions(bearerToken, email, token,siteId)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override suspend fun registerFcmToken(authHeader: String, email: String, auth: String, fcmToken: String, deviceType: String): Response<RegisterFcmTokenResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .registerFcmToken(authHeader, email, auth, fcmToken, deviceType)
    }

    override fun startEvacuation(bearerToken: String, email: String, token: String, siteId: String, actualEmergency: Boolean): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .startEvacuation(bearerToken, email, token, siteId, actualEmergency)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun stopEvacuation(bearerToken: String, email: String, token: String, siteId: String, stopEvacuationRequestList: List<StopEvacuationRequest>): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .stopEvacuation(bearerToken, email, token, siteId, stopEvacuationRequestList)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun visitorEvacuation(bearerToken: String, email: String, token: String, siteId: String): Single<EvacuationVisitorsResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .visitorEvacuation(bearerToken, email, token, siteId)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun attendees(siteId: String, bearerToken: String, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only: Boolean): Single<AttendeesResponse> {
        return retrofitWithMap.create(SOSRestServiceApi::class.java)
                .attendees(siteId,bearerToken,filterAttendanceStartUtc,filterAttendanceEndUtc,unarchived_worker_notes_only)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun attendees(siteId: String, bearerToken: String, filterUserId: Int, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only: Boolean): Single<AttendeesResponse> {
        return retrofit.create(SOSRestServiceApi::class.java).attendees(siteId,bearerToken,filterUserId,filterAttendanceStartUtc,filterAttendanceEndUtc,unarchived_worker_notes_only)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun attendeeSignOn(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java).attendeeSignOn(authHeader,email,auth,siteId,attendeeId)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun attendeeSignOff(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java).attendeeSignOff(authHeader,email,auth,siteId,attendeeId)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getCurrentPermits(siteId: String, authHeader: String): Single<PermitResponse> {
        return  retrofit.create(SOSRestServiceApi::class.java)
                .getCurrentPermits(siteId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getPermitTemplates(siteId: String, authHeader: String, filterEnable: Boolean): Single<SitePermitTemplateResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getPermitTemplates(siteId,authHeader,filterEnable)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getPermitInfo(permitId: String, authHeader: String): Single<PermitInfoResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getPermitInfo(permitId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun getEnrolledUsers(authHeader: String, getCompanies: Boolean, siteId: String, email: String): Single<EnrolledUsersResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .getEnrolledUsers(authHeader, getCompanies, siteId, email)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun setTeamMemberStatus(permitId: String, userId: String, status: String, authHeader: String): Single<ApiResponse> {
        return  retrofit.create(SOSRestServiceApi::class.java)
                .setTeamMemberStatus(permitId,userId,status,authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun updatePermit(updatePermitInfoRequest: UpdatePermitInfoRequest, permitId: String, authHeader: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .updatePermitInfo(updatePermitInfoRequest, permitId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun createPermit(createPermitRequest: CreatePermitRequest, siteId: String, authHeader: String): Single<SavePermitResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .createPermit(createPermitRequest, siteId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }

    override fun saveResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId: String, authHeader: String): Single<ApiResponse> {
        return retrofit.create(SOSRestServiceApi::class.java)
                .saveResponses(componentSaveResponsesRequest,permitId, authHeader)
                .subscribeOn(Schedulers.io())
                .mapNetworkErrors()
    }
}
