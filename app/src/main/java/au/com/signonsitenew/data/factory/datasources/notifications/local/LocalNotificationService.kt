package au.com.signonsitenew.data.factory.datasources.notifications.local

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import au.com.signonsitenew.data.factory.datasources.notifications.NotificationsChannels
import au.com.signonsitenew.domain.models.state.AttendanceNotificationState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.prelogin.SplashActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.buildNotification
import au.com.signonsitenew.utilities.buildNotificationWithGroupKey


class LocalNotificationService(val context: Context?):DataLocalNotificationSource, NotificationsChannels() {


    override fun localSignOnPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int) {
        setupChannel(context,Constants.PROMPTED_SIGN_ON_OFF_CHANNEL_NAME,Constants.PROMPTED_SIGN_ON_OFF_DESCRIPTION,NotificationManager.IMPORTANCE_DEFAULT,Constants.PROMPT_SIGNON_CHANNEL_ID)

        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("PROMPT_REQUEST", true)

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            buildNotification(context,Constants.SIGNON_PROMPT_NOTIFICATION_MESSAGE,contentIntent,
                    Constants.NOTIFICATION_PROMPT_SIGNON_ID,
                    Constants.PROMPT_SIGNON_CHANNEL_ID)
        }
        analyticsEventDelegateService.setSiteId(siteId).notificationReceived(notificationName!!, Constants.ANALYTICS_LOCAL_NOTIFICATION, extraProperties!!, null)

    }

    override fun localSignOffPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int) {
        setupChannel(context,Constants.PROMPTED_SIGN_ON_OFF_CHANNEL_NAME,Constants.PROMPTED_SIGN_ON_OFF_DESCRIPTION,NotificationManager.IMPORTANCE_DEFAULT,Constants.PROMPT_SIGNON_CHANNEL_ID)
        val intent = Intent(context, SplashActivity::class.java)

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        context?.let {
            buildNotification(context,
                    Constants.SIGNOFF_PROMPT_NOTIFICATION_MESSAGE,contentIntent,
                    Constants.NOTIFICATION_PROMPT_SIGNON_ID,
                    Constants.PROMPT_SIGNON_CHANNEL_ID)
        }
        analyticsEventDelegateService.setSiteId(siteId).notificationReceived(notificationName!!, Constants.ANALYTICS_LOCAL_NOTIFICATION, extraProperties!!, null)
    }

    override fun localAttendanceNotification(isSignedOn: Boolean, siteName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int, attendanceNotificationState: AttendanceNotificationState) {
        setupChannel(context,Constants.ATTENDANCE_CHANNEL_NAME,Constants.ATTENDANCE_CHANNEL_DESCRIPTION,NotificationManager.IMPORTANCE_HIGH,Constants.ATT_CHANNEL_ID)

        val intent = Intent(context, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
        intent.putExtra(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION, true)
        intent.putExtra(Constants.IS_LOCAL_NOTIFICATION, true)
        intent.action = Constants.SIGNONOFF_LOCAL_NOTIFICATION_INTENT_ACTION

        val message: String = when (attendanceNotificationState) {
            is AttendanceNotificationState.BriefingExistInductionNoExistBriefingUnAck -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName + Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_BRIEFING_END
            is AttendanceNotificationState.BriefingNoExistInductionExistInductionIncomplete -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName + Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_INDUCTION_END
            is AttendanceNotificationState.InductionExistBriefingExistInductionCompleteBriefingUnAck -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName + Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_BRIEFING_END
            is AttendanceNotificationState.InductionExistBriefingExistInductionInCompleteBriefingAck -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName + Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_INDUCTION_END
            is AttendanceNotificationState.InductionExistBriefingExistInductionInCompleteBriefingUnAck -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName + Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_INCOMPLETE_INDUCTION_INCOMPLETE_BRIEFING_MESSAGE_END
            else -> Constants.ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START + siteName

        }

        val contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        context?.let {
            val key = if(isSignedOn) Constants.SIGNON_ON_GROUP_NOTIFICATIONS else Constants.SIGNON_OFF_GROUP_NOTIFICATIONS
            val notificationMessage = if(isSignedOn) message else Constants.ATTENDANCE_SIGNED_OFF_NOTIFICATION_MESSAGE_COMPLETE
            buildNotificationWithGroupKey(it,notificationMessage,contentIntent,Constants.NOTIFICATION_ATTENDANCE_ID,Constants.ATT_CHANNEL_ID,key)
        }
        val notificationName = if(isSignedOn)Constants.AUTO_SIGN_ON else Constants.AUTO_SIGN_OFF

        analyticsEventDelegateService.setSiteId(siteId).notificationReceived(notificationName, Constants.ANALYTICS_LOCAL_NOTIFICATION, extraProperties!!, null)
    }

    override fun cancelNotifications(notificationId:Int) {
        context?.let {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancel(notificationId)
        }
    }

}