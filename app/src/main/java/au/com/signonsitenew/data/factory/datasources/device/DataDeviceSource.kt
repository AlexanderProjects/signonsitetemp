package au.com.signonsitenew.data.factory.datasources.device

interface DataDeviceSource {
    fun getCurrentBatteryLevel():Float
    fun getBackgroundAppRefresh():String
    fun getReachability():String
    fun isLocationEnable():Boolean?
    fun hasLocationPermission():String
    fun hasWifiEnable():Boolean?
    fun isIgnoringBatteryOptimization():Boolean?
    fun getPushNotificationPermissionToken():String?
    fun getPushNotificationPermission():String
    fun getAppName():String
    fun hasPowerSaveMode():Boolean?
}