package au.com.signonsitenew.data.factory.datasources.notifications.remote

import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.InstanceIdResult

interface DataRemoteNotificationSource {
    fun remoteBriefingNotification(message: String?,
                                    extraProperties: HashMap<String,Any>?,
                                    notificationName: String?,
                                    utcNotificationSentAt: String?,
                                    analyticsEventDelegateService: AnalyticsEventDelegateService?)

    fun remoteRejectionInductionNotification(message: String?,
                                             extraProperties: HashMap<String, Any>?,
                                             notificationName: String?,
                                             utcNotificationSentAt: String?,
                                             analyticsEventDelegateService: AnalyticsEventDelegateService?)

    fun remoteDefaultNotification(message: String?,
                                  extraProperties: HashMap<String, Any>?,
                                  notificationName: String?,
                                  utcNotificationSentAt: String?,
                                  analyticsEventDelegateService: AnalyticsEventDelegateService?)

    fun evacuationStarted(message: String?,
                          extraProperties: HashMap<String,Any>?,
                          notificationName: String?,
                          utcNotificationSentAt: String?,
                          analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun evacuationEnded(message: String?,
                        extraProperties: HashMap<String,Any>?,
                        notificationName: String?,
                        utcNotificationSentAt: String?,
                        analyticsEventDelegateService: AnalyticsEventDelegateService?)

    fun unRegisterInstanceId()

    fun remoteRiskyWorkerNotification(message: String?,
                                      userId:String,
                                      extraProperties: HashMap<String, Any>?,
                                      notificationName: String?,
                                      utcNotificationSentAt: String?,
                                      analyticsEventDelegateService: AnalyticsEventDelegateService?)
}