package au.com.signonsitenew.data.factory.datasources.notifications.remote.fcm

import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.fcm.RegisterFcmTokenUseCase
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.events.EmergencyEvent
import au.com.signonsitenew.events.InductionEvent
import au.com.signonsitenew.locationengine.LocationManager
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NotificationUtil
import au.com.signonsitenew.utilities.SLog
import au.com.signonsitenew.utilities.SessionManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjection
import org.greenrobot.eventbus.EventBus
import java.util.*
import javax.inject.Inject

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * This class processes messages received from the server. For some reason the application was not
 * picking this class up when it was in the services package. Moving it to the UI package fixed
 * this issue.
 */
class SosFcmListenerService : FirebaseMessagingService() {

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService

    @Inject
    lateinit var locationManager: LocationManager

    @Inject
    lateinit var notificationUseCase: NotificationUseCase

    @Inject
    lateinit var registrationUseCase: RegisterFcmTokenUseCase

    private val gson = Gson()

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onMessageReceived(gotMessage: RemoteMessage) {
        val from = gotMessage.from
        val data: Map<*, *> = gotMessage.data
        val session = SessionManager(this)
        SLog.i(LOG, "FCM received from: $from")
        SLog.i(LOG, "FCM contains: $data")
        val notification =
                if (data[Constants.FCM_NOTIFICATION] != null)
                    gson.fromJson<HashMap<String, Any>>(data[Constants.FCM_NOTIFICATION].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type)
                else null

        val notificationName = data[Constants.FCM_NOTIFICATION_NAME_KEY]
        when {
            data[Constants.FCM_START_EVAC] == "true" && BuildConfig.VERSION_CODE < Constants.SOS_VERSION -> {
                session.setEmergencyStatus(true)
                locationManager.refresh()
                NotificationUtil.createEvacuationNotification(applicationContext, data[Constants.GCM_MESSAGE].toString())
                val emergencyStarted = Intent(Constants.BROADCAST_EVAC_START)
                LocalBroadcastManager.getInstance(this@SosFcmListenerService).sendBroadcast(emergencyStarted)
                // Broadcast event
                EventBus.getDefault().post(EmergencyEvent(true, false))
            }
            data[Constants.FCM_FINISH_EVAC] == "true" && BuildConfig.VERSION_CODE < Constants.SOS_VERSION -> {
                session.setEmergencyStatus(false)
                locationManager.refresh()
                NotificationUtil.cancelNotifications(applicationContext,
                        NotificationUtil.NOTIFICATION_EVACUATION_ID)
                val emergencyStarted = Intent(Constants.BROADCAST_EVAC_STOP)
                LocalBroadcastManager.getInstance(this@SosFcmListenerService).sendBroadcast(emergencyStarted)
                // Broadcast event
                EventBus.getDefault().post(EmergencyEvent(false, false))
            }
            data[Constants.FCM_BRIEFING_UNACKNOWLEDGED] == "true" -> {
                notificationUseCase.showBriefingRemoteNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(),
                        data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)
            }
            notificationName == Constants.FCM_INDUCTION_REJECTED -> {
                NotificationUtil.createInductionRejectedNotification(applicationContext)
                notificationUseCase.showInductionRejectNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(), data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)
                EventBus.getDefault().post(InductionEvent(Constants.FCM_INDUCTION_REJECTED))
            }
            notificationName == Constants.FMC_NEW_ACTIVE_BRIEFING -> {
                notificationUseCase.showBriefingRemoteNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(),
                        data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)
            }
            notificationName == Constants.FCM_BRIEFING_REMINDER -> {
                notificationUseCase.showBriefingRemoteNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(),
                        data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)
            }
            notificationName == Constants.FCM_EVACUATION_STARTED && BuildConfig.VERSION_CODE >= Constants.SOS_VERSION -> {
                notificationUseCase.evacuationStartedNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(),
                        data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)

                val emergencyStarted = Intent(Constants.BROADCAST_EVAC_START)
                LocalBroadcastManager.getInstance(this@SosFcmListenerService).sendBroadcast(emergencyStarted)
                // Broadcast event
                EventBus.getDefault().post(EmergencyEvent(true, false))
            }
            notificationName == Constants.FCM_EVACUATION_ENDED && BuildConfig.VERSION_CODE >= Constants.SOS_VERSION -> {
                notificationUseCase.evacuationEndedNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(),
                        data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)

                session.setEmergencyStatus(false)
                locationManager.refresh()
                notificationUseCase.cancelNotifications(Constants.NOTIFICATION_EVACUATION_ID)
                val emergencyStarted = Intent(Constants.BROADCAST_EVAC_STOP)
                LocalBroadcastManager.getInstance(this@SosFcmListenerService).sendBroadcast(emergencyStarted)
                // Broadcast event
                EventBus.getDefault().post(EmergencyEvent(false, false))
            }
            notificationName == Constants.FCM_WORKER_NOTE_ALERT -> {
                notificationUseCase.remoteRiskyWorkerNotification(notification?.get(Constants.FCM_NOTIFICATION_BODY) as String, data[Constants.USER_ID] as String,
                        gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                        data[Constants.NOTIFICATION_NAME].toString(), data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(), analyticsEventDelegateService)
            }
            else -> {
                notificationUseCase.showDefaultNotification(
                    notification?.get(Constants.FCM_NOTIFICATION_BODY) as String,
                    gson.fromJson<HashMap<String, Any>>(data[Constants.ANALYTICS_METADATA].toString(), object : TypeToken<HashMap<String?, Any?>?>() {}.type),
                    data[Constants.NOTIFICATION_NAME].toString(),
                    data[Constants.ANALYTICS_NOTIFICATION_SENT_AT].toString(),
                    analyticsEventDelegateService
                )
            }
        }
    }

    override fun onNewToken(token: String) {
        registrationUseCase.setToken(token)
    }

    companion object {
        val LOG: String = SosFcmListenerService::class.java.simpleName
    }
}