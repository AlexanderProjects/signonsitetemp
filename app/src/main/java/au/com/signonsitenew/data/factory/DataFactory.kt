package au.com.signonsitenew.data.factory

import android.content.Context
import android.content.SharedPreferences
import au.com.signonsitenew.data.factory.datasources.analytics.AnalyticsService
import au.com.signonsitenew.data.factory.datasources.analytics.DataAnalyticsSource
import au.com.signonsitenew.data.factory.datasources.net.DataNetSource
import au.com.signonsitenew.data.factory.datasources.local.DataLocalSource
import au.com.signonsitenew.data.factory.datasources.local.LocalService
import au.com.signonsitenew.data.factory.datasources.net.RestService
import au.com.signonsitenew.data.factory.datasources.notifications.local.DataLocalNotificationSource
import au.com.signonsitenew.data.factory.datasources.notifications.local.LocalNotificationService
import au.com.signonsitenew.data.factory.datasources.notifications.remote.DataRemoteNotificationSource
import au.com.signonsitenew.data.factory.datasources.notifications.remote.RemoteNotificationService
import com.segment.analytics.Analytics
import com.squareup.moshi.Moshi
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Inject

/** Manifest:
 * Inside of the data layout cant be any context. However after a discussion with the mobile team, we decided to include the context only for notifications.
 * Mobile Team(Brent Tunnicliff and Alexander Parra)
 * **/

class DataFactory @Inject constructor(private val retrofit: Retrofit,
                                     private val retrofitWithNulls: Retrofit,
                                     private val retrofitForWithMap:Retrofit,
                                     private val context: Context, //This context is only for notifications and please dont use this context for other classes/services
                                     private val segAnalytics: Analytics,
                                     private val sharedPreferences: SharedPreferences){

    fun createNetSourceData(): DataNetSource = RestService(retrofit, retrofitWithNulls,retrofitForWithMap)
    fun createLocalSourceData(): DataLocalSource = LocalService(sharedPreferences)
    fun createAnalyticsSourceData() : DataAnalyticsSource = AnalyticsService(segAnalytics)
    fun createLocalNotification(): DataLocalNotificationSource = LocalNotificationService(context)
    fun createRemoteNotification(): DataRemoteNotificationSource = RemoteNotificationService(context)
}