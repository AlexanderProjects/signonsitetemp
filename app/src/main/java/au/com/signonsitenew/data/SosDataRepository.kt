package au.com.signonsitenew.data

import au.com.signonsitenew.data.factory.DataFactory
import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.SiteContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.domain.models.state.AttendanceNotificationState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.models.*
import au.com.signonsitenew.realm.SiteInduction
import com.segment.analytics.Properties
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.Response
import java.io.File
import javax.inject.Inject
import kotlin.collections.HashMap

class SosDataRepository @Inject constructor(private val dataFactory: DataFactory): DataRepository{
    override fun getEnrolments(userId: String, token: String): Single<EnrolmentResponse> = dataFactory.createNetSourceData().getEnrolments(userId = userId, bearerToken = token)
    override fun getCredentials(userId: String, token: String): Single<CredentialsResponse> = dataFactory.createNetSourceData().getCredentials(userId,token)
    override fun updateCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse> = dataFactory.createNetSourceData().updateCredentials(request,userId,bearerToken)
    override fun createCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse> = dataFactory.createNetSourceData().createCredentials(request,userId,bearerToken)
    override fun getCredentialTypes(bearerToken: String): Single<CredentialTypesResponse> = dataFactory.createNetSourceData().getCredentialTypes(bearerToken)
    override fun deleteCredential(credentialId: String, bearerToken: String): Single<DeleteCredentialResponse> = dataFactory.createNetSourceData().deleteCredential(credentialId,bearerToken)
    override fun updateEnrolments(enrolmentId: String, isAutomaticAttendanceEnabled:String, token: String): Single<EnrolmentUpdateResponse> = dataFactory.createNetSourceData().updateEnrolments(enrolmentId,isAutomaticAttendanceEnabled,token)
    override fun getFeatureFlags(userId: String, token: String): Single<FeatureFlagsResponse> = dataFactory.createNetSourceData().getFeatureFlags(userId = userId, bearerToken = token)
    override fun getCompanyFeatureFlags(siteId: String, token: String): Single<CompanyFeatureFlagResponse> = dataFactory.createNetSourceData().getCompanyFeatureFlags(siteId,token)
    override fun sharePassport(sharePassportRequest: SharePassportRequest,userId: String, token: String): Single<SharePassportResponse> = dataFactory.createNetSourceData().sharePassport(sharePassportRequest,userId,token)
    override fun getPersonalInfo(userId: String, bearerToken: String): Single<UserInfoResponse> = dataFactory.createNetSourceData().getPersonalInfo(userId = userId, bearerToken = bearerToken)
    override fun updatePersonalInfo(userRequest: UserInfoUpdateRequest, userId: String, bearerToken: String): Single<UpdateInfoResponse> = dataFactory.createNetSourceData().updatePersonalInfo(userRequest,userId, bearerToken)
    override fun uploadImages(file: File, fileName: String):  Single<UploadImageResponse> = dataFactory.createNetSourceData().uploadImages(file,fileName)
    override fun getPhotoUrl(accessKey: String): Single<RetrieveImageResponse> = dataFactory.createNetSourceData().getPhotoUrl(accessKey)
    override fun getCompaniesForSite(siteId: String, userEmail: String, token: String, bearerToken: String): Single<CompaniesForSiteResponse> = dataFactory.createNetSourceData().getCompaniesForSite(siteId = siteId, userEmail = userEmail, token = token, bearerToken = bearerToken)
    override fun saveCompaniesInDb(companies: List<Company>) = dataFactory.createLocalSourceData().saveCompaniesInDb(companies)
    override fun getNearBySites(userId: String, bearerToken: String, latitude:Float, longitude:Float,uncertainty:Float): Single<NearSitesResponse> = dataFactory.createNetSourceData().getNearBySites(userId,bearerToken, latitude, longitude,uncertainty)
    override fun validateEmail(emailVerificationRequest: EmailVerificationRequest): Single<EmailVerificationResponse> = dataFactory.createNetSourceData().validateEmail(emailVerificationRequest)
    override fun validatePhoneNumber(alpha2: String, raw: String, email: String, auth: String): Single<ApiResponse> = dataFactory.createNetSourceData().validatePhoneNumber(alpha2,raw,email,auth)
    override fun getListOfEmployers(employersRequest: EmployersRequest): Single<EmployersResponse> = dataFactory.createNetSourceData().getListOfEmployers(employersRequest)
    override fun userRegistration(userRegistrationRequest: UserRegistrationRequest): Single<UserRegistrationResponse> = dataFactory.createNetSourceData().userRegistration(userRegistrationRequest)
    override fun getListOfBriefings(siteId: String, limit: String, offSet: String, bearerToken: String): Single<BriefingsResponse> = dataFactory.createNetSourceData().getListOfBriefings(siteId,limit,offSet,bearerToken)
    override fun saveOffsetForBriefings(value: String) = dataFactory.createLocalSourceData().saveOffsetForBriefings(value)
    override fun saveActiveBriefingInDb(briefing: WorkerBriefing,siteId:Int) = dataFactory.createLocalSourceData().saveActiveBriefing(briefing,siteId)
    override fun getActiveBriefings(siteId: String, authHeader: String): Single<BriefingWorkerResponse> = dataFactory.createNetSourceData().getActiveBriefings(siteId,authHeader)
    override fun getOffsetValue(): String = dataFactory.createLocalSourceData().getOffsetValue()
    override fun clearOffsetValue() = dataFactory.createLocalSourceData().clearOffsetValue()
    override fun acknowledgeBriefings(briefingId: String, authHeader: String): Single<ApiResponse> = dataFactory.createNetSourceData().acknowledgeBriefing(briefingId,authHeader)
    override fun updateBriefingContent(authHeader: String, siteId: String, briefingContent:String): Single<ApiResponse> = dataFactory.createNetSourceData().updateBriefingContent(authHeader,siteId,briefingContent)
    override fun seenBriefing(briefingId: String, authHeader: String): Single<ApiResponse> = dataFactory.createNetSourceData().seenBriefing(briefingId,authHeader)
    override fun getTodaysVisits(siteId: String, userEmail: String, token: String, bearerToken: String): Single<TodaysVisitsResponse> = dataFactory.createNetSourceData().getTodaysVisits(siteId,userEmail,token,bearerToken)
    override fun sendTrackEvents(event:String,properties: Properties?): Unit = dataFactory.createAnalyticsSourceData().sendTrackEvent(event,properties)
    override fun sendScreenEvents(name:String?,properties: Properties?) :Unit = dataFactory.createAnalyticsSourceData().sendScreenEvent(name,properties)
    override fun callIdentity(userId: String) = dataFactory.createAnalyticsSourceData().callIdentity(userId)
    override fun saveLinkedUserId(userId: Long) = dataFactory.createLocalSourceData().saveLinkedUserId(userId)
    override fun getSavedPreviousLikedUserId(): Long = dataFactory.createLocalSourceData().getSavedPreviousLikedUserId()
    override fun getUserContextForAnalytics(platform: String, userId: String, bearerToken: String): Single<UserContextAnalyticsResponse> = dataFactory.createNetSourceData().getUserContextForAnalytics(platform,userId,bearerToken)
    override fun getSiteContextForAnalytics(platform: String, siteId: String, bearerToken: String): Single<SiteContextAnalyticsResponse> = dataFactory.createNetSourceData().getSiteContextForAnalytics(platform,siteId,bearerToken)
    override fun getCompanyContextAnalytics(platform: String, companyId: String, bearerToken: String): Single<CompanyContextAnalyticsResponse> = dataFactory.createNetSourceData().getCompanyContextAnalytics(platform,companyId,bearerToken)
    override fun remoteBriefingNotification(message: String?, extraProperties: HashMap<String,Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().remoteBriefingNotification(message,extraProperties,notificationName,utcNotificationSentAt,analyticsEventDelegateService)
    override fun remoteInductionNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().remoteRejectionInductionNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
    override fun defaultNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().remoteDefaultNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
    override fun localSignOnPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int) = dataFactory.createLocalNotification().localSignOnPromptNotification(notificationName,analyticsEventDelegateService,extraProperties,siteId)
    override fun localSignOffPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int) = dataFactory.createLocalNotification().localSignOffPromptNotification(notificationName,analyticsEventDelegateService,extraProperties,siteId)
    override fun localAttendanceNotification(isSignedOn: Boolean, siteName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String, Any>?, siteId: Int, attendanceNotificationState: AttendanceNotificationState) = dataFactory.createLocalNotification().localAttendanceNotification(isSignedOn,siteName,analyticsEventDelegateService,extraProperties,siteId,attendanceNotificationState)
    override fun getCurrentSiteName(siteId: Int): String = dataFactory.createLocalSourceData().getCurrentSiteName(siteId)
    override fun cancelNotifications(notificationId: Int) = dataFactory.createLocalNotification().cancelNotifications(notificationId)
    override fun deleteBriefings() = dataFactory.createLocalSourceData().deleteBriefings()
    override fun retrieveSignedOnStatus(bearerToken: String,userEmail: String, token: String): Maybe<Map<String,Any>> = dataFactory.createNetSourceData().retrieveSignedOnStatus(bearerToken,userEmail,token)
    override fun getActiveInductions(bearerToken: String, email: String, token: String, siteId: String): Single<InductionResponse> = dataFactory.createNetSourceData().getActiveInductions(bearerToken,email,token,siteId)
    override fun saveInduction(inductionId: Int?, userId: Long, siteId: Int, inductionType: String?, inductionState: String?, updateAt: String?) = dataFactory.createLocalSourceData().saveInduction(inductionId,userId,siteId,inductionType,inductionState,updateAt)
    override fun deleteInduction(siteId: Int) = dataFactory.createLocalSourceData().deleteInduction(siteId)
    override suspend fun registerFcmToken(authHeader: String, email: String, auth: String, fcmToken: String, deviceType: String):Response<RegisterFcmTokenResponse> = dataFactory.createNetSourceData().registerFcmToken(authHeader,email,auth,fcmToken,deviceType)
    override fun unRegisterInstanceId() = dataFactory.createRemoteNotification().unRegisterInstanceId()
    override fun startEvacuation(bearerToken: String, email: String, token: String, siteId: String, actualEmergency: Boolean): Single<ApiResponse> = dataFactory.createNetSourceData().startEvacuation(bearerToken,email,token,siteId,actualEmergency)
    override fun stopEvacuation(bearerToken: String, email: String, token: String, siteId: String, stopEvacuationRequestList: List<StopEvacuationRequest>): Single<ApiResponse> = dataFactory.createNetSourceData().stopEvacuation(bearerToken,email,token,siteId,stopEvacuationRequestList)
    override fun visitorEvacuation(bearerToken: String, email: String, token: String, siteId: String): Single<EvacuationVisitorsResponse> = dataFactory.createNetSourceData().visitorEvacuation(bearerToken,email,token,siteId)
    override fun evacuationStartedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().evacuationStarted(message,extraProperties,notificationName,utcNotificationSentAt,analyticsEventDelegateService)
    override fun evacuationEndedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().evacuationEnded(message,extraProperties,notificationName,utcNotificationSentAt,analyticsEventDelegateService)
    override fun remoteRiskyWorkerNotification(message: String?, userId: String, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?) = dataFactory.createRemoteNotification().remoteRiskyWorkerNotification(message,userId,extraProperties,notificationName,utcNotificationSentAt,analyticsEventDelegateService)
    override fun saveSiteVisitorsForEvacuations(visitors: List<EvacuationVisitor>) = dataFactory.createLocalSourceData().saveSiteVisitorsForEvacuations(visitors)
    override fun getEvacuationVisitors(): List<StopEvacuationRequest> = dataFactory.createLocalSourceData().getEvacuationVisitors()
    override fun removeSiteVisits() = dataFactory.createLocalSourceData().removeSiteVisits()
    override fun attendees(siteId: String, bearerToken: String, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only: Boolean): Single<AttendeesResponse> = dataFactory.createNetSourceData().attendees(siteId,bearerToken,filterAttendanceStartUtc,filterAttendanceEndUtc,unarchived_worker_notes_only)
    override fun attendees(siteId: String, bearerToken: String, filterUserId: Int, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only: Boolean): Single<AttendeesResponse> = dataFactory.createNetSourceData().attendees(siteId,bearerToken,filterUserId,filterAttendanceStartUtc,filterAttendanceEndUtc,unarchived_worker_notes_only)
    override fun attendeeSignOn(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String): Single<ApiResponse> = dataFactory.createNetSourceData().attendeeSignOn(authHeader,email,auth,siteId,attendeeId)
    override fun attendeeSignOff(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String): Single<ApiResponse> = dataFactory.createNetSourceData().attendeeSignOff(authHeader,email,auth,siteId,attendeeId)
    override fun getSiteInductionForUser(userId: Long, siteId: Long): SiteInduction? = dataFactory.createLocalSourceData().getSiteInductionForUser(userId,siteId)
    override fun canAccessUserInductions(userId: Long, siteId: Long): Boolean = dataFactory.createLocalSourceData().canAccessUserInductions(userId,siteId)
    override fun getSiteTimeZone(siteId: Int): String? = dataFactory.createLocalSourceData().getSiteTimeZone(siteId)
    override fun getCurrentPermits(siteId: String, authHeader: String): Single<PermitResponse> = dataFactory.createNetSourceData().getCurrentPermits(siteId,authHeader)
    override fun getPermitTemplates(siteId: String, authHeader: String, filterEnable: Boolean): Single<SitePermitTemplateResponse> = dataFactory.createNetSourceData().getPermitTemplates(siteId,authHeader,filterEnable)
    override fun getPermitInfo(permitId: String, authHeader: String): Single<PermitInfoResponse> = dataFactory.createNetSourceData().getPermitInfo(permitId,authHeader)
    override fun getEnrolledUsers(authHeader: String, getCompanies: Boolean, siteId: String, email: String): Single<EnrolledUsersResponse> = dataFactory.createNetSourceData().getEnrolledUsers(authHeader,getCompanies,siteId,email)
    override fun setTeamMemberStatus(permitId: String, userId: String, status: String, authHeader: String): Single<ApiResponse> = dataFactory.createNetSourceData().setTeamMemberStatus(permitId, userId, status, authHeader)
    override fun hasManagerPermission(userId: Long, siteId: Long): Boolean = dataFactory.createLocalSourceData().hasPermitManagementPermission(userId, siteId)
    override fun updatePermit(updatePermitInfoRequest: UpdatePermitInfoRequest, permitId: String, authHeader: String): Single<ApiResponse> = dataFactory.createNetSourceData().updatePermit(updatePermitInfoRequest, permitId, authHeader)
    override fun createPermit(createPermitRequest: CreatePermitRequest, siteId: String, authHeader: String): Single<SavePermitResponse> = dataFactory.createNetSourceData().createPermit(createPermitRequest, siteId, authHeader)
    override fun saveResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId: String, authHeader: String): Single<ApiResponse> = dataFactory.createNetSourceData().saveResponses(componentSaveResponsesRequest, permitId, authHeader)
    override fun getLocalUserFullName(): String = dataFactory.createLocalSourceData().getLocalUserFullName()
}