package au.com.signonsitenew.data.factory.datasources.net

import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.SiteContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.models.*
import au.com.signonsitenew.utilities.Constants
import io.reactivex.Maybe
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface SOSRestServiceApi {
    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_BRIEFING_LIST)
    fun getListOfBriefings(@Path("site_id") siteId: String, @Query("limit") limit: String, @Query("offset") offset: String, @Header("Authorization") authHeader: String): Call<BriefingsResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_BRIEFING_LIST)
    fun getListOfBriefingsRx(@Path("site_id") siteId: String, @Query("limit") limit: String, @Query("offset") offset: String, @Header("Authorization") authHeader: String): Single<BriefingsResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_USER_INFO)
    fun getUserInfo(@Path("user_id") userId: String, @Header("Authorization") authHeader: String): Call<UserInfoResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_USER_INFO)
    fun getUserInfoRx(@Path("user_id") userId: String, @Header("Authorization") authHeader: String): Single<UserInfoResponse>

    @Headers("Accept: application/json")
    @PATCH(Constants.URL_SITE_USER_INFO)
    fun updateUserInfo(@Body userRequest: UserInfoUpdateRequest, @Path("user_id") userId: String, @Header("Authorization") authHeader: String): Call<UpdateInfoResponse>

    @Headers("Accept: application/json")
    @PATCH(Constants.URL_SITE_USER_INFO)
    fun updateUserInfoRx(@Body userRequest: UserInfoUpdateRequest, @Path("user_id") userId: String, @Header("Authorization") authHeader: String): Single<UpdateInfoResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_PHONE_VALIDATION)
    fun getPhoneValidationRx(@Query("alpha2") alpha2: String, @Query("raw") raw: String, @Query("email") email: String, @Query("authentication") auth: String): Single<ApiResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_CREDENTIALS)
    fun getCredentialsRx(@Path("user_id") userId: String, @Header("Authorization") authHeader: String): Single<CredentialsResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_CREDENTIALS)
    fun updateCredentialRx(@Body credentialCreateUpdateRequest: CredentialCreateUpdateRequest, @Path("user_id") userId: String, @Header("Authorization") authHeader: String): Single<CredentialCreateUpdateResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_CREDENTIALS)
    fun createCredentialRx(@Body credentialCreateUpdateRequest: CredentialCreateUpdateRequest, @Path("user_id") userId: String, @Header("Authorization") authHeader: String): Single<CredentialCreateUpdateResponse>

    @Headers("Accept: application/json")
    @Multipart
    @POST(Constants.PHOTO_URL)
    fun uploadImageRx(@Part file: MultipartBody.Part, @Part("fs_name") fileName: RequestBody): Single<UploadImageResponse>

    @GET(Constants.PHOTO_URL)
    fun getImageUrl(@Query("access_key") accessKey: String): Single<RetrieveImageResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_CREDENTIALS_TYPES)
    fun getCredentialTypesRx(@Header("Authorization") authHeader: String): Single<CredentialTypesResponse>

    @Headers("Accept: application/json")
    @DELETE(Constants.URL_DELETE_CREDENTIALS)
    fun deleteCredential(@Path("credential_id") credentialId: String, @Header("Authorization") authHeader: String): Single<DeleteCredentialResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_CONNECTIONS_ENROLMENTS)
    fun getEnrolments(@Path("user_id") userId: String, @Header("Authorization") authHeader: String) : Single<EnrolmentResponse>

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @PATCH(Constants.URL_CONNECTIONS_ENROLMENTS_END_CONNECTION)
    fun updateEnrolments(@Path("enrolment_id") enrolmentId: String, @Field("is_automatic_attendance_enabled") isAutomaticAttendanceEnabled: String, @Header("Authorization") authHeader: String): Single<EnrolmentUpdateResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_USER_FEATURE_FLAG)
    fun getFeatureFlags(@Path("user_id") userId: String, @Header("Authorization") authHeader: String):Single<FeatureFlagsResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_FEATURE_FLAG)
    fun getCompanyFeatureFlags(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String):Single<CompanyFeatureFlagResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_GET_COMPANIES_REGISTERED)
    fun getCompaniesForSite(@Query("site_id") siteId: String, @Query("email") email: String, @Query("authentication") auth: String, @Header("Authorization") authHeader: String):Single<CompaniesForSiteResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_SHARE_PASSPORT)
    fun sharePassport(@Body sharePassportRequest: SharePassportRequest, @Path("user_id") userId: String, @Header("Authorization") authHeader: String):Single<SharePassportResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_NEAR_BY_SITES)
    fun getNearBySites(@Path("user_id") userId: String, @Header("Authorization") authHeader: String, @Query("filter_by_latitude") latitude: Float, @Query("filter_by_longitude") longitude: Float, @Query("filter_by_location_uncertainty_meters") uncertainty: Float):Single<NearSitesResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_EMAIL_IN_USE)
    fun validateEmail(@Body emailVerificationRequest: EmailVerificationRequest): Single<EmailVerificationResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_GET_ALL_EMPLOYERS)
    fun getAllEmployers(@Body employersRequest: EmployersRequest) : Single<EmployersResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_USER_REGISTRATION)
    fun userRegistration(@Body userRegistrationRequest: UserRegistrationRequest) : Single<UserRegistrationResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_ACTIVE_BRIEFINGS)
    fun getActiveBriefings(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String):Single<BriefingWorkerResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_ACKNOWLEDGE_BRIEFINGS)
    fun acknowledgeBriefing(@Path("briefing_id") briefingId: String, @Header("Authorization") authHeader: String):Single<ApiResponse>

    //Todo This is an old end point.
    @Headers("Accept: application/json")
    @POST(Constants.OLD_URL_UPDATE_BRIEFING)
    fun updateBriefing(@Header("Authorization") authHeader: String, @Query("site_id") siteId: String, @Query("talk_text") briefingContent: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_SEEN_BRIEFING)
    fun seenBriefing(@Path("briefing_id") briefingId: String, @Header("Authorization") authHeader: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_TODAYS_VISITS_WITH_BASE)
    fun getTodaysVisits(@Query("site_id") siteId: String, @Query("email") email: String, @Query("authentication") auth: String, @Header("Authorization") authHeader: String) : Single<TodaysVisitsResponse>

    @GET(Constants.URL_ANALYTICS_USER_CONTEXT)
    fun getUserContext(@Path("user_id") userId: String, @Query("platform") platform: String, @Header("Authorization") authHeader: String):Single<UserContextAnalyticsResponse>

    @GET(Constants.URL_ANALYTICS_SITE_CONTEXT)
    fun getSiteContext(@Path("site_id") siteId: String, @Query("platform") platform: String, @Header("Authorization") authHeader: String):Single<SiteContextAnalyticsResponse>

    @GET(Constants.URL_ANALYTICS_COMPANIES_CONTEXT)
    fun getCompanyContext(@Path("company_id") companyId: String, @Query("platform") platform: String, @Header("Authorization") authHeader: String):Single<CompanyContextAnalyticsResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_IS_USER_SIGNED_ON)
    fun retrieveSignedOnStatus(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String):Maybe<Map<String, Any>>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_INDUCTION_INFORMATION)
    fun getActiveInductions(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String):Single<InductionResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_SITE_REGISTER_FCM_TOKEN)
    suspend fun registerFcmToken(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("device_token") fcmToken: String, @Query("device_type") deviceType: String):Response<RegisterFcmTokenResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_START_EVACUATION)
    fun startEvacuation(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String, @Query("actual") actualEmergency: Boolean):Single<ApiResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_STOP_EVACUATION)
    fun stopEvacuation(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String, @Body stopEvacuationRequestList: List<StopEvacuationRequest>):Single<ApiResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_EVACUATION_VISITORS)
    fun visitorEvacuation(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String):Single<EvacuationVisitorsResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_ATTENDEES)
    fun attendees(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String, @Query("filter_attendance_start_utc") filterAttendanceStartUtc: String, @Query("filter_attendance_end_utc") filterAttendanceEndUtc: String, @Query("unarchived_worker_notes_only") unarchived_worker_notes_only: Boolean):Single<AttendeesResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_ATTENDEES)
    fun attendees(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String, @Query("filter_user_id") filterUserId: Int, @Query("filter_attendance_start_utc") filterAttendanceStartUtc: String, @Query("filter_attendance_end_utc") filterAttendanceEndUtc: String, @Query("unarchived_worker_notes_only") unarchived_worker_notes_only: Boolean):Single<AttendeesResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_VISITOR_SIGN_ON)
    fun attendeeSignOn(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String, @Query("user_id") attendeeId: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_VISITOR_SIGN_OFF)
    fun attendeeSignOff(@Header("Authorization") authHeader: String, @Query("email") email: String, @Query("authentication") auth: String, @Query("site_id") siteId: String, @Query("user_id") attendeeId: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_CURRENT_PERMITS)
    fun getCurrentPermits(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String):Single<PermitResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_SITE_PERMIT_TEMPLATE)
    fun getPermitTemplates(@Path("site_id") siteId: String, @Header("Authorization") authHeader: String, @Query("filter_enabled") filterEnable:Boolean):Single<SitePermitTemplateResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_GET_PERMIT_INFO)
    fun getPermitInfo(@Path("permit_id") permitId:String, @Header("Authorization") authHeader: String):Single<PermitInfoResponse>

    @Headers("Accept: application/json")
    @GET(Constants.URL_ENROLLED_USERS)
    fun getEnrolledUsers(@Header("Authorization") authHeader: String, @Query("get_companies")getCompanies:Boolean, @Query("site_id")siteId:String, @Query("email") email: String):Single<EnrolledUsersResponse>

    @Headers("Accept: application/json")
    @PATCH(Constants.URL_SET_MEMBER_STATUS_FOR_PERMIT)
    fun setTeamMemberStatus(@Path("permit_id") permitId:String, @Path("user_id") userId: String, @Query("status") status:String, @Header("Authorization")authHeader: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @PATCH(Constants.URL_UPDATE_MEMBERLIST_STATE_FOR_PERMIT)
    fun updatePermitInfo(@Body updatePermitInfoRequest: UpdatePermitInfoRequest, @Path("permit_id") permitId:String, @Header("Authorization")authHeader: String):Single<ApiResponse>

    @Headers("Accept: application/json")
    @POST(Constants.URL_SAVE_PERMIT)
    fun createPermit(@Body createPermitRequest: CreatePermitRequest, @Path("site_id") siteId:String, @Header("Authorization")authHeader: String):Single<SavePermitResponse>

    @POST(Constants.URL_SAVE_RESPONSES)
    @Headers("Accept: application/json")
    fun saveResponses(@Body componentSaveResponsesRequest: ComponentSaveResponsesRequest, @Path("permit_id") permitId:String, @Header("Authorization")authHeader: String):Single<ApiResponse>
}