package au.com.signonsitenew.data.factory.datasources.analytics

import com.segment.analytics.Properties

interface DataAnalyticsSource {
    fun sendTrackEvent(event:String,properties: Properties?)
    fun sendScreenEvent(name:String?,properties: Properties?)
    fun callIdentity(userId: String)
}