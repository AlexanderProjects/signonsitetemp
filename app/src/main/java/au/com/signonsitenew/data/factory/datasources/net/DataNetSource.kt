package au.com.signonsitenew.data.factory.datasources.net

import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.SiteContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.models.*
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.Callback
import retrofit2.Response
import java.io.File


interface DataNetSource {
    fun get(userId: String, bearerToken: String, callback: Callback<UserInfoResponse>)
    fun patch(userRequest: UserInfoUpdateRequest, userId: String, bearerToken: String, callback: Callback<UpdateInfoResponse>)
    fun get(userId: String, bearerToken: String): Single<UserInfoResponse>
    fun updatePersonalInfo(userRequest: UserInfoUpdateRequest, userId: String, bearerToken: String): Single<UpdateInfoResponse>
    fun get(alpha2: String, raw: String, email: String, auth: String):Single<ApiResponse>
    fun getCredentials(userId: String, bearerToken: String): Single<CredentialsResponse>
    fun updateCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse>
    fun createCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse>
    fun getCredentialTypes(bearerToken: String):Single<CredentialTypesResponse>
    fun deleteCredential(credentialId: String, bearerToken: String): Single<DeleteCredentialResponse>
    fun getEnrolments(userId: String, bearerToken: String):Single<EnrolmentResponse>
    fun updateEnrolments(enrolmentId:String, isAutomaticAttendanceEnabled:String, bearerToken: String):Single<EnrolmentUpdateResponse>
    fun getFeatureFlags(userId: String, bearerToken: String): Single<FeatureFlagsResponse>
    fun getCompanyFeatureFlags(siteId: String, bearerToken: String):Single<CompanyFeatureFlagResponse>
    fun sharePassport(sharePassportRequest: SharePassportRequest,userId:String, token:String):Single<SharePassportResponse>
    fun getPersonalInfo(userId: String, bearerToken: String): Single<UserInfoResponse>
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun getPhotoUrl(accessKey:String): Single<RetrieveImageResponse>
    fun getCompaniesForSite(siteId:String,userEmail:String, token:String, bearerToken: String): Single<CompaniesForSiteResponse>
    fun getNearBySites(userId: String, bearerToken: String, latitude:Float, longitude:Float,uncertainty:Float):Single<NearSitesResponse>
    fun validateEmail(emailVerificationRequest: EmailVerificationRequest):Single<EmailVerificationResponse>
    fun validatePhoneNumber(alpha2: String, raw: String, email: String, auth: String):Single<ApiResponse>
    fun getListOfEmployers(employersRequest: EmployersRequest):Single<EmployersResponse>
    fun userRegistration(userRegistrationRequest: UserRegistrationRequest):Single<UserRegistrationResponse>
    fun getListOfBriefings(siteId: String, limit: String, offSet: String, bearerToken: String):Single<BriefingsResponse>
    fun getActiveBriefings(siteId: String, authHeader: String):Single<BriefingWorkerResponse>
    fun acknowledgeBriefing(briefingId: String, authHeader: String):Single<ApiResponse>
    fun updateBriefingContent(authHeader: String, siteId: String, briefingContent:String):Single<ApiResponse>
    fun seenBriefing(briefingId: String, authHeader: String):Single<ApiResponse>
    fun getTodaysVisits(siteId:String,userEmail:String, token:String, bearerToken: String):Single<TodaysVisitsResponse>
    fun getUserContextForAnalytics(platform:String, userId: String, bearerToken: String):Single<UserContextAnalyticsResponse>
    fun getSiteContextForAnalytics(platform: String, siteId: String, bearerToken: String):Single<SiteContextAnalyticsResponse>
    fun getCompanyContextAnalytics(platform: String, companyId: String, bearerToken: String):Single<CompanyContextAnalyticsResponse>
    fun retrieveSignedOnStatus(bearerToken: String,userEmail: String, token: String): Maybe<Map<String,Any>>
    fun getActiveInductions(bearerToken: String, email: String, token: String, siteId: String):Single<InductionResponse>
    suspend fun registerFcmToken(authHeader: String, email: String, auth: String, fcmToken:String, deviceType:String):Response<RegisterFcmTokenResponse>
    fun startEvacuation(bearerToken: String, email: String, token: String, siteId: String, actualEmergency:Boolean):Single<ApiResponse>
    fun stopEvacuation(bearerToken: String, email: String, token: String, siteId: String, stopEvacuationRequestList: List<StopEvacuationRequest>):Single<ApiResponse>
    fun visitorEvacuation(bearerToken: String, email: String, token: String, siteId: String):Single<EvacuationVisitorsResponse>
    fun attendees(siteId: String, bearerToken: String, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only:Boolean):Single<AttendeesResponse>
    fun attendees(siteId: String, bearerToken: String, filterUserId: Int, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only:Boolean):Single<AttendeesResponse>
    fun attendeeSignOn(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String):Single<ApiResponse>
    fun attendeeSignOff(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String):Single<ApiResponse>
    fun getCurrentPermits(siteId: String, authHeader: String):Single<PermitResponse>
    fun getPermitTemplates(siteId: String, authHeader: String, filterEnable:Boolean):Single<SitePermitTemplateResponse>
    fun getPermitInfo(permitId:String, authHeader: String):Single<PermitInfoResponse>
    fun getEnrolledUsers(authHeader: String, getCompanies:Boolean, siteId:String, email: String):Single<EnrolledUsersResponse>
    fun setTeamMemberStatus(permitId: String,userId: String,status:String,authHeader: String):Single<ApiResponse>
    fun updatePermit(updatePermitInfoRequest: UpdatePermitInfoRequest, permitId:String, authHeader: String):Single<ApiResponse>
    fun createPermit(createPermitRequest: CreatePermitRequest, siteId:String, authHeader: String):Single<SavePermitResponse>
    fun saveResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId:String, authHeader: String):Single<ApiResponse>
}