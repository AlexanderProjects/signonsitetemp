package au.com.signonsitenew.data.factory.datasources.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.POWER_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.os.BatteryManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationManagerCompat
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty


class DeviceService(private val context: Context, private val pref:SharedPreferences) : DataDeviceSource {

    override fun getCurrentBatteryLevel(): Float {
        val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            context.registerReceiver(null, ifilter)
        }
        val batteryPct: Float? = batteryStatus?.let { intent ->
            val level: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale: Int = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            level  / scale.toFloat()
        }
        return batteryPct!!
    }

    override fun getBackgroundAppRefresh(): String {
        return if(ActivityCompat.checkSelfPermission(context, Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_GRANTED)
            Constants.AVAILABLE
        else
            Constants.DENIED
    }

    @SuppressLint("WrongConstant")
    override fun getReachability(): String {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val actNetwork = cm.getNetworkCapabilities(cm.activeNetwork)
            return if(actNetwork != null) {
                when {
                    actNetwork.hasCapability(NetworkCapabilities.TRANSPORT_WIFI) -> Constants.WIFI
                    actNetwork.hasCapability(NetworkCapabilities.TRANSPORT_CELLULAR) -> Constants.CELLULAR
                    actNetwork.hasCapability(NetworkCapabilities.TRANSPORT_ETHERNET) -> Constants.UNKNOWN_CONNECTION
                    actNetwork.hasCapability(NetworkCapabilities.TRANSPORT_VPN) -> Constants.UNKNOWN_CONNECTION
                    else -> Constants.NOT_CONNECTED
                }
            }else{
                Constants.NOT_CONNECTED
            }
        }else{
            val activeNetwork = cm.activeNetworkInfo
            return if(activeNetwork != null) {
                when (activeNetwork.type) {
                    ConnectivityManager.TYPE_MOBILE -> Constants.CELLULAR
                    ConnectivityManager.TYPE_WIFI -> Constants.WIFI
                    ConnectivityManager.TYPE_ETHERNET -> Constants.UNKNOWN_CONNECTION
                    ConnectivityManager.TYPE_VPN -> Constants.UNKNOWN_CONNECTION
                    else -> Constants.NOT_CONNECTED
                }
            }else{
                Constants.NOT_CONNECTED
            }
        }
    }


    override fun isLocationEnable(): Boolean? {
        return if(context.applicationContext != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val lm: LocationManager =
                    context.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                lm.isLocationEnabled
            } else {
                val locationMode =
                    Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
                locationMode != Settings.Secure.LOCATION_MODE_OFF
            }
        }else {
            null
        }
    }

    override fun hasLocationPermission(): String {
        return if(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            Constants.ALWAYS
        else
            Constants.DENIED
    }

    override fun hasWifiEnable(): Boolean? {
        return if (context.applicationContext != null) {
            val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            wifi.isWifiEnabled
        }else{
            null
        }

    }

    override fun isIgnoringBatteryOptimization(): Boolean? {
        return if (context.applicationContext != null) {
            val pm = context.applicationContext.getSystemService(POWER_SERVICE) as PowerManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                pm.isIgnoringBatteryOptimizations(context.packageName)
            } else {
                false
            }
        }else{
            null
        }
    }

    override fun getPushNotificationPermissionToken(): String?  = pref.getString(Constants.FIREBASE_PUSH_NOTIFICATION_TOKEN,null)

    override fun getAppName(): String {
        return if (context.applicationInfo != null){
            context.applicationInfo.loadLabel(context.packageManager).toString()
        }else{
            String().empty()
        }

    }

    override fun hasPowerSaveMode(): Boolean? {
        return if(context.applicationContext != null) {
            val pm = context.applicationContext.getSystemService(POWER_SERVICE) as PowerManager
            pm.isPowerSaveMode
        }else {
            null
        }
    }

    override fun getPushNotificationPermission(): String {
        return if(NotificationManagerCompat.from(context).areNotificationsEnabled())
            Constants.AVAILABLE
        else
            Constants.DENIED
    }
}