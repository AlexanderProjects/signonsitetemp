package au.com.signonsitenew.data.factory.datasources.local


import android.content.SharedPreferences
import androidx.core.content.edit
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.Company
import au.com.signonsitenew.realm.*
import au.com.signonsitenew.realm.SiteInduction
import au.com.signonsitenew.realm.services.BriefingService
import au.com.signonsitenew.realm.services.SiteInductionService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty
import au.com.signonsitenew.utilities.emptySpace
import io.realm.Realm
import io.realm.RealmResults
import java.util.*

class LocalService(private val sharedPreferences: SharedPreferences) : DataLocalSource {

    val realm: Realm = Realm.getInstance(RealmManager.getRealmConfiguration())

    override fun saveCompaniesInDb(companies: List<Company>) {
        // Store Companies
        realm.beginTransaction()
        realm.where(au.com.signonsitenew.realm.Company::class.java).findAll().deleteAllFromRealm()
        realm.commitTransaction()

        realm.beginTransaction()
        // Store Companies registered information
        for (company in companies) {
            val realmCompany = au.com.signonsitenew.realm.Company()
            realmCompany.companyId = company.id.toLong()
            realmCompany.name = company.name
            realm.copyToRealmOrUpdate(realmCompany)
        }
        realm.commitTransaction()
        realm.close()
    }

    override fun saveOffsetForBriefings(value: String) {
        sharedPreferences.edit { putString(Constants.PAGINATION_OFFSET, value)}
    }

    override fun getOffsetValue() : String {
        return sharedPreferences.getString(Constants.PAGINATION_OFFSET, "0").toString()
    }

    override fun clearOffsetValue() {
        sharedPreferences.edit {
            remove(Constants.PAGINATION_OFFSET)
            apply()
        }
    }

    override fun saveActiveBriefing(briefing: WorkerBriefing, siteId: Int) {
        val service = BriefingService(realm)
        service.storeBriefing(briefing.id, siteId, briefing.content, briefing.set_by_user.first_name, null, briefing.needs_acknowledgement)
        realm.close()
    }

    override fun saveLinkedUserId(userId: Long) {
        sharedPreferences.edit{ putLong(Constants.SAVE_USER_ID, userId) }
    }

    override fun getSavedPreviousLikedUserId() : Long {
        return sharedPreferences.getLong(Constants.SAVE_USER_ID, 0)
    }

    override fun deleteBriefings() {
        val service = BriefingService(realm)
        service.deleteAllBriefings()
        realm.close()
    }

    override fun getCurrentSiteName(siteId: Int):String {
        val site: EnrolledSite? = realm.where(EnrolledSite::class.java).equalTo(Constants.REALM_SITE_ID, siteId).findFirst()
        val siteName = if (site != null)  site.name  else String().empty()
        realm.close()
        return siteName
    }

    override fun saveInduction(inductionId: Int?, userId: Long, siteId: Int, inductionType: String?, inductionState: String?, updateAt: String?) {
        val service = SiteInductionService(realm)
        service.createOrUpdateInduction(inductionId, userId, siteId.toLong(), inductionType, inductionState, updateAt)
        realm.close()
    }

    override fun deleteInduction(siteId: Int) {
        val siteInductions: RealmResults<SiteInduction> = realm.where(SiteInduction::class.java).equalTo(Constants.SITE_ID_FOR_REALM, siteId).findAll()
        if (siteInductions.size < 1) {
            return
        }
        realm.beginTransaction()
        siteInductions.deleteAllFromRealm()
        realm.commitTransaction()
        realm.close()
    }

    override fun getSiteInductionForUser(userId: Long, siteId: Long): SiteInduction? {
        if (siteId == null || siteId == 0L) {
            return null
        }
        return if (userHasInductionOnSite(userId, siteId)) {
            realm.where(SiteInduction::class.java)
                    .equalTo(Constants.USER_ID_FOR_REALM, userId)
                    .equalTo(Constants.SITE_ID_FOR_REALM, siteId)
                    .findFirst()
        } else {
            null
        }
    }

    override fun saveSiteVisitorsForEvacuations(visitors: List<EvacuationVisitor>) {
        realm.beginTransaction()
        realm.where(SiteAttendee::class.java).findAll().deleteAllFromRealm()

        for (value in visitors) {
            val visitor = SiteAttendee()
            visitor.userId = value.user_id
            visitor.firstName = value.first_name
            visitor.lastName = value.last_name
            visitor.phoneNumber = value.phone_number
            visitor.company = value.company
            visitor.checkInTime = value.check_in_time
            visitor.isMarkedSafe = false
            realm.copyToRealmOrUpdate(visitor)
        }
        realm.commitTransaction()
        realm.close()
    }

    override fun getEvacuationVisitors(): List<StopEvacuationRequest> {
        val visitors = realm.where(SiteAttendee::class.java).findAll()
        val logEntries = ArrayList<StopEvacuationRequest>()
        for (attendee in visitors) {
            val request = StopEvacuationRequest(attendee.userId, attendee.isMarkedSafe)
            logEntries.add(request)
        }
        realm.close()
        return logEntries
    }

    override fun removeSiteVisits() {
        realm.beginTransaction()
        realm.where(SiteAttendee::class.java).findAll().deleteAllFromRealm()
        realm.commitTransaction()
        realm.close()
    }

    override fun canAccessUserInductions(userId: Long, siteId: Long): Boolean {
        val abilities: UserAbilities? = realm.where(UserAbilities::class.java)
                .equalTo(Constants.USER_ID_FOR_REALM, userId)
                .equalTo(Constants.SITE_ID_FOR_REALM, siteId)
                .findFirst()
        return abilities != null && abilities.canManageUserInductions()
    }

    override fun hasPermitManagementPermission(userId: Long, siteId: Long): Boolean {
        val abilities: UserAbilities? = realm.where(UserAbilities::class.java)
            .equalTo(Constants.USER_ID_FOR_REALM, userId)
            .equalTo(Constants.SITE_ID_FOR_REALM, siteId)
            .findFirst()
        return abilities != null && abilities.canManagerUserHasPermitPermission()
    }

    override fun getSiteTimeZone(siteId: Int): String? {
        return realm.where(SiteSettings::class.java).equalTo(Constants.SITE_ID_FOR_REALM,siteId).findFirst()?.siteTimeZone
    }

    override fun getLocalUserFullName(): String {
        return sharedPreferences.getString(Constants.USER_FIRST_NAME, String().empty()) + String().emptySpace() + sharedPreferences.getString(Constants.USER_LAST_NAME, String().empty())
    }

    private fun userHasInductionOnSite(userId: Long, siteId: Long): Boolean {
        val induction: SiteInduction? = realm.where(SiteInduction::class.java)
                .equalTo(Constants.USER_ID_FOR_REALM, userId)
                .equalTo(Constants.SITE_ID_FOR_REALM, siteId)
                .findFirst()
        return induction != null
    }

}