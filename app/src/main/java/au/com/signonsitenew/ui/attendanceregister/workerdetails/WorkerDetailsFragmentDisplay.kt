package au.com.signonsitenew.ui.attendanceregister.workerdetails

interface WorkerDetailsFragmentDisplay {
    fun signOnSuccessful()
    fun signOffSuccessful()
    fun signOnError()
    fun signOffError()
}