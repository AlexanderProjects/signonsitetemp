package au.com.signonsitenew.ui.passport

import android.graphics.Bitmap
import au.com.signonsitenew.events.RxBusMainActivity
import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.events.RxBusSigOnActivity
import au.com.signonsitenew.models.BadgeNotification
import au.com.signonsitenew.domain.models.CredentialsResponse
import au.com.signonsitenew.domain.usecases.passport.ShowUserPassportUseCaseImpl
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

interface PassportPresenter{
    fun retrieveData()
    fun uploadHeadShotPhoto(photo: Bitmap)
    fun inject(passportView: PassportView)
    fun registerNotificationListener()
}

class PassportPresenterImpl @Inject constructor(private val showUserPassportUseCaseImpl: ShowUserPassportUseCaseImpl,
                                                private val rxBusPassport: RxBusPassport,
                                                private val logger: Logger,
                                                private val rxBusSigOnActivity: RxBusSigOnActivity,
                                                private val rxBusMainActivity: RxBusMainActivity) : BasePresenter(), PassportPresenter {

    private lateinit var passportView: PassportView
    private var personalCount = 0
    private var emergencyCount = 0
    private var credentialCount = 0
    private var connectionsCount = 0
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun retrieveData() {
        showUserPassportUseCaseImpl.getUserInfoAndCredentialsAsync(object:DisposableSubscriber<Any>(){
            override fun onNext(response: Any?) {
                if (response is UserInfoResponse) {
                    if (NetworkErrorValidator.isValidResponse(response)) {
                        response.user.company_name?.let { showUserPassportUseCaseImpl.saveCompanyName(it) }
                        passportView.loadUserInfoData(response.user)
                    }
                    else {
                        passportView.dataError(NetworkErrorValidator.getErrorMessage(response))
                        logger.w(this@PassportPresenterImpl::class.java.name, attributes = mapOf(this@PassportPresenterImpl::retrieveData.name to toJson(response)))
                    }
                }
                if (response is CredentialsResponse) {
                    if (NetworkErrorValidator.isValidResponse(response))
                        passportView.loadCredentialsData(response)
                    else {
                        passportView.dataError(NetworkErrorValidator.getErrorMessage(response))
                        logger.w(this@PassportPresenterImpl::class.java.name, attributes = mapOf(this@PassportPresenterImpl::retrieveData.name to toJson(response)))
                    }
                }
            }

            override fun onError(error: Throwable?) {
                passportView.remoteFailure()
                logger.e(this@PassportPresenterImpl::class.java.name, attributes = mapOf(this@PassportPresenterImpl::retrieveData.name to error?.message))
            }

            override fun onComplete() {}

        })
    }

    override fun uploadHeadShotPhoto(photo: Bitmap) {
        showUserPassportUseCaseImpl.getPersonalInfoAsync(photo,object:DisposableSingleObserver<UserInfoResponse>(){
            override fun onSuccess(response: UserInfoResponse) {
                if (NetworkErrorValidator.isValidResponse(response))
                    response.user.headshot_photo_temporary_url?.let { passportView.showHeadShotPhoto(it) }
                else {
                    passportView.dataError(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@PassportPresenterImpl::class.java.name, attributes = mapOf(this@PassportPresenterImpl::uploadHeadShotPhoto.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                passportView.remoteFailure()
                logger.e(this@PassportPresenterImpl::class.java.name, attributes = mapOf(this@PassportPresenterImpl::uploadHeadShotPhoto.name to error.message))
            }

        })
    }

    override fun inject(passportView: PassportView) {
        this.passportView = passportView
    }

    private fun notifyToMainPassportBadge(counter: Int) {
        rxBusMainActivity.send(counter)
    }

    private fun notifyToSignedPassportBadge(badgeNotification: BadgeNotification) {
        rxBusSigOnActivity.send(badgeNotification)
    }

    /**
     * This method triggers an notification badge update in personal, emergency and credential fragments
     */
    override fun registerNotificationListener() {
        disposables.add(rxBusPassport.toObservable()
                .subscribe { notification: BadgeNotification ->
                    if (notification.notifier.equals(Constants.PERSONAL_TAB_NOTIFICATION, ignoreCase = true)) {
                        personalCount = 0
                        passportView.updateNotificationValueForPersonalInfo(notification.badgeNumber)
                        personalCount = notification.badgeNumber
                    }
                    if (notification.notifier.equals(Constants.EMERGENCY_TAB_NOTIFICATION, ignoreCase = true)) {
                        emergencyCount = 0
                        passportView.updateNotificationValueForEmergencyInfo(notification.badgeNumber)
                        emergencyCount = notification.badgeNumber
                    }
                    if (notification.notifier.equals(Constants.CREDENTIAL_TAB_NOTIFICATION, ignoreCase = true)) {
                        credentialCount = 0
                        passportView.updateNotificationValueForCredentials(notification.badgeNumber)
                        credentialCount = notification.badgeNumber
                    }
                    if (notification.notifier.equals(Constants.CONNECTION_TAB_NOTIFICATION, ignoreCase = true)) {
                        connectionsCount = 0
                        passportView.updateNotificationValueForConnections(notification.badgeNumber)
                        connectionsCount = notification.badgeNumber
                    }
                    notifyToMainPassportBadge(personalCount + emergencyCount + credentialCount + connectionsCount)
                    notifyToSignedPassportBadge(BadgeNotification(personalCount + emergencyCount + credentialCount + connectionsCount, Constants.PASSPORT_SIGNED_MAIN_TAB_NOTIFICATION))
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        showUserPassportUseCaseImpl.dispose()
    }

}