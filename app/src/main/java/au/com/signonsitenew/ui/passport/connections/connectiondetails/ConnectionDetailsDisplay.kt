package au.com.signonsitenew.ui.passport.connections.connectiondetails

interface ConnectionDetailsDisplay {
    fun reloadData()
    fun showNetworkErrors()
    fun showDataErrors(message:String)
}