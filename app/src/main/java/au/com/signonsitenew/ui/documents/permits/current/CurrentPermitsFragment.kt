 package au.com.signonsitenew.ui.documents.permits.current

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentCurrentPermitsBinding
import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.ui.documents.permits.current.adapter.CurrentPermitsRecyclerViewAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CurrentPermitsFragment : DaggerFragment(), CurrentPermitsDisplay {

    @Inject
    lateinit var router:Router
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var _binding: FragmentCurrentPermitsBinding? = null
    private val binding get() = _binding!!
    private val presenter: CurrentPermitsFragmentPresenterImpl by viewModels { viewModelFactory}


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentCurrentPermitsBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.permitToolbarTitleTextView.text = resources.getString(R.string.current_permit_title)
        binding.currentPermitsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && binding.newPermitFab.isExtended) {
                    binding.newPermitFab.shrink()
                } else if (dy < 0 && !binding.newPermitFab.isExtended) {
                    binding.newPermitFab.extend()
                }
            }
        })
        binding.swipeRefreshPermits.setOnRefreshListener{ presenter.getCurrentPermits() }
        binding.permitToolbar.title = String().empty()
        (requireActivity() as DaggerAppCompatActivity).setSupportActionBar(binding.permitToolbar)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.newPermitFab.setOnClickListener { presenter.navigateToTemplatePermit() }
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.getCurrentPermits()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.inject(this)
        presenter.observeStates()
        setHasOptionsMenu(true)
    }

    override fun showPermitList(permits: List<Permit>) {
        val sortedList = presenter.sortPermitListByDate(permits)
        val adapter = CurrentPermitsRecyclerViewAdapter(sortedList, onClick = {
            presenter.navigateToPermitDetails(it)
        })
        binding.currentPermitsList.adapter = adapter
        binding.swipeRefreshPermits.isRefreshing = false
    }

    override fun showPermitsError() {
        binding.swipeRefreshPermits.isRefreshing = false
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) { presenter.getCurrentPermits() }
    }

    override fun navigateToTemplatePermitFragment() {
        router.navigateToTemplatePermit(requireActivity())
    }

    override fun navigateToPermitDetails(permit: Permit) {
        router.navigateToPermitDetails(requireActivity(), permit)
    }

    override fun showProgressView() {
        router.showProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun removeProgressView() {
        router.removeProgressView(requireActivity(),R.id.doc_permits_container)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                router.navigateToDocumentList(requireActivity())
            }
        }
        return true
    }

}