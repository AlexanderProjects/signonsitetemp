package au.com.signonsitenew.ui.main

import au.com.signonsitenew.domain.models.NearSitesResponse
import au.com.signonsitenew.domain.usecases.location.LocationServiceUseCase
import au.com.signonsitenew.domain.usecases.siteinformation.GetListOfSitesNearByUseCaseImpl
import au.com.signonsitenew.events.RxBusLocationPermissionResult
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface SitesFragmentPresenter {
    fun inject(display: SitesFragmentDisplay)
    fun retrieveNearBySites()
    fun observeForPermissionLocationResult()
}

class SitesFragmentPresenterImpl @Inject constructor(private val getListOfSitesNearByUseCaseImpl: GetListOfSitesNearByUseCaseImpl,
                                                     private val logger:Logger,
                                                     private val rxBusLocationPermissionResult: RxBusLocationPermissionResult,
                                                     private val locationServiceUseCase: LocationServiceUseCase): BasePresenter(), SitesFragmentPresenter {
    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var sitesFragmentDisplay:SitesFragmentDisplay
    override fun inject(display: SitesFragmentDisplay){
        sitesFragmentDisplay = display
    }

    override fun retrieveNearBySites() {
        locationServiceUseCase.getCurrentLocation {
            disposables.add(getListOfSitesNearByUseCaseImpl.getNearBySites(it.latitude.toFloat(), it.longitude.toFloat(), it.accuracy).subscribe({ response ->
                if (response is NearSitesResponse) {
                    if (response.sites.isNotEmpty()) {
                        sitesFragmentDisplay.showListOfSites(response.sites)
                    } else {
                        sitesFragmentDisplay.showEmptyListOfSites()
                        logger.w(SitesFragmentPresenterImpl::class.java.name, attributes = mapOf(this::retrieveNearBySites.name to toJson(response)))
                    }
                }
                locationServiceUseCase.cancelRequestingLocation()
            }, { error ->
                sitesFragmentDisplay.showNetworkErrors()
                logger.e(SitesFragmentPresenterImpl::class.java.name,attributes = mapOf(this::retrieveNearBySites.name to error.message))
            }))
        }
    }

    override fun observeForPermissionLocationResult() {
        disposables.add(rxBusLocationPermissionResult.toLocationPermissionObservable()
                .subscribe {
                    sitesFragmentDisplay.onRequestPermissionsResultListener(it.requestCode,it.permissions,it.grantResults)
                })
    }


    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}