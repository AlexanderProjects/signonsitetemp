package au.com.signonsitenew.ui.prelogin

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService
import au.com.signonsitenew.locationengine.LocationManager
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.LogoutUtil
import au.com.signonsitenew.utilities.NetworkUtil
import au.com.signonsitenew.utilities.SessionManager
import dagger.android.support.DaggerAppCompatActivity
import io.realm.Realm
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Splash Screen for when the app first loads. This also dictates which screen the app should go to
 * next.
 */
class SplashActivity : DaggerAppCompatActivity() {

    private var mSession: SessionManager? = null

    @Inject
    lateinit var signOnStatusUseCaseImpl: SignOnStatusUseCaseImpl

    @Inject
    lateinit var locationManager: LocationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Doubling up on initialising Realm. There was a rare issue where the application context
        // did not appear to initialise correctly every time. As this is a base activity of sorts,
        // this should effectively solve the problem.
        Realm.init(this)
        routeToAppropriateActivity()
    }

    private fun routeToAppropriateActivity() {
        mSession = SessionManager(this)
        if (!mSession!!.isLoggedIn) {
            LogoutUtil.logoutUser(this, locationManager)
            navigateToStartActivity()
            return;
        }

        // Determine which activity to launch into
        val siteId = mSession!!.siteId
        when {
            NetworkUtil.networkIsConnected(this) -> {
                signOnStatusUseCaseImpl.getUserStatusAsync{
                    var tabIndex: Int? = null
                    val extras = intent.extras
                    if (extras != null) {
                        Log.i(LOG, extras.toString())
                        tabIndex = extras.getInt(Constants.TAB_INDEX)
                    }
                    if(it && siteId != 0){
                        navigateToSignedOnActivity(tabIndex)
                    }else{
                        navigateToMainActivity()
                    }
                }
            }
            siteId == 0 -> {
                navigateToMainActivity()
            }
            else -> {
                var tabIndex: Int? = null
                val extras = intent.extras
                if (extras != null) {
                    tabIndex = extras.getInt(Constants.TAB_INDEX)
                }
                navigateToSignedOnActivity(tabIndex)
            }
        }

        // First check that location permission has been granted (app is not fresh install)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // Ensure the job service is operating. If it is the first time, it will fire immediately, else it will fire when it is scheduled.
            RegionFetcherJobService.schedule(this)
        }
    }

    private fun navigateToStartActivity() {
        val intent = Intent(this, StartActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun navigateToMainActivity() {
        val extras = intent.extras
        val intent = Intent(this, MainActivity::class.java)
        extras?.let {
            if (it.getBoolean(Constants.HAS_USER_TAPPED_ON_NOTIFICATION)) {
                if (extras.getBoolean(Constants.IS_LOCAL_NOTIFICATION)) {
                    intent.putExtra(Constants.IS_LOCAL_NOTIFICATION, true)
                    intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
                    if (extras.getBoolean(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION))
                        intent.putExtra(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION, true)
                }
            }
        }

        startActivity(intent)
        finish()
    }

    private fun navigateToSignedOnActivity(tabIndex: Int?) {
        val extras = intent.extras
        val intent = Intent(this, SignedOnActivity::class.java)
        if (tabIndex != null && tabIndex != 0) {
            Log.i(LOG, "tabIndex: $tabIndex")
            intent.putExtra(Constants.TAB_INDEX, tabIndex)
            extras?.let { intent.putExtra(Constants.SHOW_PROGRESS_VIEW_KEY, it.getBoolean(Constants.SHOW_PROGRESS_VIEW_KEY)) }
            if (extras!!.getBoolean(Constants.HAS_USER_TAPPED_ON_NOTIFICATION)) {
                if (extras.getBoolean(Constants.IS_LOCAL_NOTIFICATION)) {
                    intent.putExtra(Constants.IS_LOCAL_NOTIFICATION, true)
                    intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
                    if (extras.getBoolean(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION))
                        intent.putExtra(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION, true)
                }
                if (extras.getBoolean(Constants.IS_REMOTE_NOTIFICATION)) {
                    intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
                    intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
                    if (extras.get(Constants.HAS_METADATA) != null) {
                        val metadata = extras.get(Constants.HAS_METADATA) as HashMap<*, *>
                        intent.putExtra(Constants.HAS_METADATA, metadata)
                    }
                    if (extras.getString(Constants.BRIEFING_NOTIFICATION_TYPE) != null) {
                        val briefingNotificationType = extras.getString(Constants.BRIEFING_NOTIFICATION_TYPE)
                        intent.putExtra(Constants.BRIEFING_NOTIFICATION_TYPE, briefingNotificationType)
                    }
                    if (extras.getString(Constants.EMERGENCY_ENDED_NOTIFICATION_TYPE) != null) {
                        val emergencyStartedNotificationType = extras.getString(Constants.EMERGENCY_ENDED_NOTIFICATION_TYPE)
                        intent.putExtra(Constants.EMERGENCY_ENDED_NOTIFICATION_TYPE, emergencyStartedNotificationType)
                    }
                    if (extras.getString(Constants.EMERGENCY_STARTED_NOTIFICATION_TYPE) != null) {
                        val emergencyStartedNotificationType = extras.getString(Constants.EMERGENCY_STARTED_NOTIFICATION_TYPE)
                        intent.putExtra(Constants.EMERGENCY_STARTED_NOTIFICATION_TYPE, emergencyStartedNotificationType)
                    }
                }
            }
            if (extras.getBoolean(Constants.BRIEFING_NOTIFICATION) || extras.get(Constants.FCM_NOTIFICATION_NAME_KEY) == Constants.FMC_NEW_ACTIVE_BRIEFING) {
                intent.putExtra(Constants.HAS_UNACKNOWLEDGED_BRIEFING_DOC, true)
                intent.putExtra(Constants.BRIEFING_NOTIFICATION, true)
            }
            if (extras.getBoolean(Constants.INDUCTION_NOTIFICATION)) {
                intent.putExtra(Constants.INDUCTION_NOTIFICATION, true)
                intent.putExtra(Constants.IS_REMOTE_NOTIFICATION, true)
                intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION, true)
                if (!extras.getBoolean(Constants.INDUCTION_SUBMITTED))
                    intent.putExtra(Constants.INDUCTION_SUBMITTED, false)
            } else
                intent.putExtra(Constants.INDUCTION_SUBMITTED, true)

            if (extras.getBoolean(Constants.EMERGENCY_NOTIFICATION) && extras.get(Constants.FCM_NOTIFICATION_NAME_KEY) == Constants.FCM_EVACUATION_ENDED) {
                intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_EVACUATION_ENDED)
            }
            if (extras.getBoolean(Constants.EMERGENCY_NOTIFICATION) && extras.get(Constants.FCM_NOTIFICATION_NAME_KEY) == Constants.FCM_EVACUATION_STARTED) {
                intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_EVACUATION_STARTED)
            }
            if (extras.get(Constants.FCM_NOTIFICATION_NAME_KEY) == Constants.FCM_INDUCTION_REJECTED) {
                intent.putExtra(Constants.FCM_NOTIFICATION_NAME_KEY, Constants.FCM_INDUCTION_REJECTED)
            }
        }
        startActivity(intent)
        finish()
    }

    companion object {
        private val LOG = SplashActivity::class.java.simpleName
    }
}