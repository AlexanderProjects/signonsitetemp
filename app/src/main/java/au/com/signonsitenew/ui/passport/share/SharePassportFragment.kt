package au.com.signonsitenew.ui.passport.share


import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.EditTextWatcher
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SharePassportFragment : DaggerFragment(), SharePassportDisplay {

    companion object {
        fun newInstance() = SharePassportFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenterImpl: SharePassportPresenterImpl by viewModels{ viewModelFactory }
    private lateinit var emailText:EditText


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =inflater.inflate(R.layout.share_passport_fragment, container, false)
        presenterImpl.inject(this)
        val passportShareToolbar = view.findViewById<Toolbar>(R.id.share_passport_toolbar)
        passportShareToolbar.title = Constants.SHARE_PASSPORT
        (activity as AppCompatActivity?)!!.setSupportActionBar(passportShareToolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)

        emailText = view.findViewById(R.id.share_email_edit_text_view)
        val invalidEmailText = view.findViewById<TextView>(R.id.invalid_email_textview)
        val shareButton = view.findViewById<Button>(R.id.share_action_button)
        emailText.addTextChangedListener(EditTextWatcher(emailText,object:EditTextWatcher.AddAction {
            override fun call(text: String?) {
                when {
                    text?.let { presenterImpl.validateEmail(it) }!! -> invalidEmailText.visibility = View.GONE
                    text.isEmpty() -> invalidEmailText.visibility = View.GONE
                    else -> {
                        invalidEmailText.visibility = View.VISIBLE
                    }
                }
            }
        }))
        shareButton.setOnClickListener {
            when {
                emailText.text.toString().isEmpty() -> AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.EMPTY_EMAIL_TEXT)
                !presenterImpl.validateEmail(emailText.text.toString()) -> AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.BAD_EMAIL_ALERT_DIALOG_ERROR)
                else -> presenterImpl.sharePassport(presenterImpl.buildRequest(emailText.text.toString()))
            }
        }
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.share_button)?.let { it.isVisible = false }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(requireActivity())
    }

    override fun showConfirmation() {
        AlertDialogMessageHelper.shareConfirmationMessage(requireActivity(),emailText.text.toString(),action = {requireActivity().supportFragmentManager.popBackStackImmediate()})
    }

    override fun showDataErrors(message: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),message)
    }

}
