package au.com.signonsitenew.ui.documents.permits.details.team

import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.PermitInfoResponse
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.domain.models.SaveButtonState
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitTeamTabFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitTeamTabUseCase
import au.com.signonsitenew.events.*
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface TeamFragmentPresenter{
    fun setPermitInfoState(permitInfo: PermitInfo)
    fun removeUserFromSelectedListTeam()
    fun getUserDetails()
    fun observeStates()
    fun observeForCtaState()
    fun inject(teamFragmentDisplay: TeamFragmentDisplay)
    fun navigateToAddTeamMembers()
    fun observeSelectedMembers()
    fun reOrganiseSelectedMemberList(originalList: List<RequesteeUser>,requesteeUser: RequesteeUser)
    fun retrieveSelectedMemberList():List<RequesteeUser>
    fun retrieveUnSelectedMemberList():List<RequesteeUser>
    fun clearSelectedListOfMembers()
    fun clearUnSelectedListOfMembers()
    fun sendUnSelectedUsers(memberList:List<RequesteeUser>)
    fun sendClearMembersList()
    fun saveUnSelectedMemberList(memberList:List<RequesteeUser>)
    fun saveSelectedMemberList(memberList:List<RequesteeUser>)
    fun removeSelectedUser(requesteeUser: RequesteeUser)
    fun removeListFromUnSelectedUsers(unSelectedList: List<RequesteeUser>)
    fun mapPermitUserState(permitUserState:PermitDoneState, showTick:()->Unit, hideTick:()->Unit)
    fun setUserStatusToPreRequest(userId: String)
    fun setUserClickMenu(requesteeUser: RequesteeUser,permitInfo: PermitInfo):Array<String>
    fun retrieveLocalPermitInfo():PermitInfo
    fun setLocalPermitInfo(permitInfo: PermitInfo)
    fun setFabButtonVisibilityState(isVisible:Boolean)
}


class TeamFragmentPresenterImpl @Inject constructor(private val permitTeamTabUseCase: PermitTeamTabUseCase,
                                                    private val rxBusSelectedTeamMember: RxBusSelectedTeamMember,
                                                    private val rxBusUpdateCtaState: RxBusUpdateCtaState,
                                                    private val rxBusUpdateTeamTab: RxBusUpdateTeamTab,
                                                    private val rxBusSaveButtonState: RxBusSaveButtonState,
                                                    private val rxBusAddUnSelectedTeamMember: RxBusAddUnSelectedTeamMember,
                                                    private val logger: Logger):BasePresenter(), TeamFragmentPresenter {

    private lateinit var display:TeamFragmentDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<PermitTeamTabFragmentState>()


    override fun inject(teamFragmentDisplay: TeamFragmentDisplay) {
        this.display = teamFragmentDisplay
    }
    override fun navigateToAddTeamMembers() {
        uiStateObservable.onNext(PermitTeamTabFragmentState.NavigateToAddTeamMember)
    }
    override fun observeSelectedMembers() {
       disposables.add(rxBusSelectedTeamMember.toSelectedMembersObservable().subscribe{
           val memberList = it as List<RequesteeUser>
           uiStateObservable.onNext(PermitTeamTabFragmentState.ShowSelectedMembers(memberList))
           rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
           rxBusUpdateCtaState.sendUpdateCtaState(memberList.isNotEmpty())
       })
    }

    override fun reOrganiseSelectedMemberList(originalList: List<RequesteeUser>, requesteeUser: RequesteeUser) {
        removeSelectedUser(requesteeUser)
        display.showSelectedMembersList(permitTeamTabUseCase.retrieveSelectedTeamMemberList())
    }
    override fun retrieveSelectedMemberList(): List<RequesteeUser> = permitTeamTabUseCase.retrieveSelectedTeamMemberList()
    override fun retrieveUnSelectedMemberList(): List<RequesteeUser> = permitTeamTabUseCase.retrieveUnSelectedTeamMemberList()
    override fun clearSelectedListOfMembers() { permitTeamTabUseCase.clearSelectedListOfMembers() }
    override fun clearUnSelectedListOfMembers() { permitTeamTabUseCase.clearUnSelectedListOfMembers() }
    override fun sendUnSelectedUsers(memberList: List<RequesteeUser>) { rxBusAddUnSelectedTeamMember.sendUnSelectedMembers(memberList) }
    override fun sendClearMembersList() { rxBusAddUnSelectedTeamMember.sendUnSelectedMembers(Any())}
    override fun saveUnSelectedMemberList(memberList: List<RequesteeUser>) { permitTeamTabUseCase.saveUnSelectedTeamMembers(memberList) }
    override fun saveSelectedMemberList(memberList: List<RequesteeUser>) { permitTeamTabUseCase.saveSelectedTeamMembers(memberList) }
    override fun removeSelectedUser(requesteeUser: RequesteeUser) { permitTeamTabUseCase.removeSelectedUser(requesteeUser) }
    override fun removeListFromUnSelectedUsers(unSelectedList: List<RequesteeUser>) { permitTeamTabUseCase.removeSelectedListOfMembers(unSelectedList) }
    override fun mapPermitUserState(permitUserState: PermitDoneState, showTick: () -> Unit, hideTick: () -> Unit) { permitTeamTabUseCase.mapPermitUserState(permitUserState,showTick,hideTick) }
    override fun setUserStatusToPreRequest(userId: String) {
        disposables.add(Single.concat(permitTeamTabUseCase.setTeamMemberStatus(userId,Constants.PERMIT_TEAM_STATUS_PREREQUEST), permitTeamTabUseCase.retrievePermitInfoAsync(permitTeamTabUseCase.retrievePermitInfo().id.toString()))
            .subscribe({
            if(it.status != Constants.JSON_SUCCESS)
                uiStateObservable.onNext(PermitTeamTabFragmentState.ShowPermitError)
            },{
                uiStateObservable.onNext(PermitTeamTabFragmentState.ShowError)
                logger.e(this@TeamFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TeamFragmentPresenterImpl::setUserStatusToPreRequest.name to it.message))
            }))
    }
    override fun setUserClickMenu(requesteeUser: RequesteeUser, permitInfo: PermitInfo): Array<String> = permitTeamTabUseCase.setUserClickMenu(requesteeUser,permitInfo)
    override fun retrieveLocalPermitInfo(): PermitInfo = permitTeamTabUseCase.retrievePermitInfo()
    override fun setLocalPermitInfo(permitInfo: PermitInfo) = permitTeamTabUseCase.setPermitInfo(permitInfo)
    override fun setFabButtonVisibilityState(isVisible: Boolean) { uiStateObservable.onNext(PermitTeamTabFragmentState.ShowFabButton(isVisible))}
    override fun setPermitInfoState(permitInfo: PermitInfo) {
        PermitTeamTabFragmentState.SaveUnSelectedMembers(permitInfo.requestee_users)
        when{
            permitTeamTabUseCase.isPermitReadOnly(permitInfo.state,permitInfo) -> uiStateObservable.onNext(PermitTeamTabFragmentState.ReadOnlyPermit(permitInfo))
            !permitTeamTabUseCase.isPermitReadOnly(permitInfo.state,permitInfo) -> uiStateObservable.onNext(PermitTeamTabFragmentState.FullAccessPermit(permitInfo))
        }
    }

    override fun removeUserFromSelectedListTeam() {
        disposables.add(Single.concat(permitTeamTabUseCase.updateListOfMembersForAPermitAsync(permitTeamTabUseCase.retrievePermitInfo()), permitTeamTabUseCase.updatePermitInfoAsync(permitTeamTabUseCase.retrievePermitInfo().id.toString()))
            .subscribe({
                if(it.status == Constants.JSON_SUCCESS){
                    if(it is PermitInfoResponse){
                        rxBusUpdateCtaState.sendUpdateCtaState(it.permit)
                    }
                }
            },{
                logger.e(this@TeamFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TeamFragmentPresenterImpl::removeUserFromSelectedListTeam.name to it.message))
            }))
    }

    override fun getUserDetails() { uiStateObservable.onNext(PermitTeamTabFragmentState.NewPermit) }
    override fun observeStates() {
        disposable.add(uiStateObservable.subscribe {
            when(it){
                is PermitTeamTabFragmentState.ReadOnlyPermit -> {
                    val filterList = permitTeamTabUseCase.filterRequesteeList(it.permitInfo.requestee_users,it.permitInfo)
                    display.setRequestor(it.permitInfo.requester_user.first_name + " " +it.permitInfo.requester_user.last_name,true)
                    display.setRequestorCompany(it.permitInfo.requester_user.site_company.name,true)
                    display.showSelectedMembersList(filterList)
                    display.setAddTeamMembersButtonVisibility(false)
                    permitTeamTabUseCase.isRequesteeListNotEmpty(it.permitInfo.requestee_users) { display.hideTeamMemberPermitRequestMessage() }
                    permitTeamTabUseCase.setPermitInfo(it.permitInfo)
                }
                is PermitTeamTabFragmentState.FullAccessPermit -> {
                    display.setRequestor(it.permitInfo.requester_user.first_name + " " +it.permitInfo.requester_user.last_name,false)
                    display.setRequestorCompany(it.permitInfo.requester_user.site_company.name,false)
                    display.showSelectedMembersList(it.permitInfo.requestee_users)
                    display.setAddTeamMembersButtonVisibility(true)
                    permitTeamTabUseCase.setPermitInfo(it.permitInfo)
                }
                is PermitTeamTabFragmentState.NewPermit -> {
                    display.setRequestor(permitTeamTabUseCase.getUserFirstAndLastName(),false)
                    display.setRequestorCompany(permitTeamTabUseCase.getUserCompany(),false)
                    display.setAddTeamMembersButtonVisibility(true)
                    display.showTeamMemberPermitRequestMessage()
                }
                is PermitTeamTabFragmentState.ShowSelectedMembers -> display.showSelectedMembersList(it.memberList)
                is PermitTeamTabFragmentState.ShowError -> display.showErrors()
                is PermitTeamTabFragmentState.NavigateToAddTeamMember -> display.navigationToAddTeamMembers()
                is PermitTeamTabFragmentState.ShowPermitError -> display.showPermitError()
                is PermitTeamTabFragmentState.SaveUnSelectedMembers -> display.saveUnSelectedMembers(it.memberList)
                is PermitTeamTabFragmentState.ShowFabButton -> display.setAddTeamMembersButtonVisibility(it.isFabVisible)
            }
        })
    }

    override fun observeForCtaState() {
        disposables.add(rxBusUpdateTeamTab.toSendPermitTeamTabBusObservable()
            .subscribe {
                clearSelectedListOfMembers()
                setPermitInfoState(it)
            })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposable.dispose()
    }

}