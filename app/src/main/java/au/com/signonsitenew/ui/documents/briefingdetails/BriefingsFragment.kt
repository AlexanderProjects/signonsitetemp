package au.com.signonsitenew.ui.documents.briefingdetails

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.webkit.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.realm.services.SiteSettingsService
import au.com.signonsitenew.realm.services.UserAbilitiesService
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.ui.documents.briefings.BriefingsActivity
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.*
import com.google.gson.Gson
import dagger.android.support.DaggerFragment
import io.realm.Realm
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class BriefingsFragment : DaggerFragment(),BriefingsDisplay {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var presenterImpl: BriefingsPresenterImpl

    @Inject

    lateinit var router: Router

    private lateinit var mSession: SessionManager
    private lateinit var workerBriefing: WorkerBriefing
    private lateinit var mRealm: Realm
    private var mSiteId: Int? = null
    private lateinit var mSiteBriefingView: TextView
    private lateinit var briefingSignature:TextView
    private lateinit var briefingSignatureTitle:TextView
    private lateinit var briefingCompanyName:TextView
    private lateinit var briefingUserPhoneNumber:TextView
    private lateinit var mAcknowledgementButton: Button
    private lateinit var mSignedOnLayout: ConstraintLayout
    private lateinit var mLoaderLayout: ConstraintLayout
    private lateinit var mSpinnerView: ImageView
    private lateinit var mAcknowledgedText: TextView
    private lateinit var quillWebView: WebView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mSession = SessionManager(requireActivity())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this,factory).get(BriefingsPresenterImpl::class.java)
        presenterImpl.inject(this)
        presenterImpl.observeStates()
        presenterImpl.setWorkerBriefing(workerBriefing)
        presenterImpl.hasRichText()
        presenterImpl.needsAcknowledge()
        presenterImpl.shouldTheAppShowSignature()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if(requireArguments().getParcelable<WorkerBriefing>(Constants.ACTIVE_BRIEFING_KEY) != null)
            workerBriefing = arguments?.getParcelable(Constants.ACTIVE_BRIEFING_KEY)!!
        mRealm = Realm.getDefaultInstance()

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_briefings,container, false)

        mSiteBriefingView = rootView.findViewById(R.id.site_briefing_text_view)
        briefingSignatureTitle = rootView.findViewById(R.id.briefing_signature_title)
        briefingCompanyName = rootView.findViewById(R.id.briefing_company_name)
        briefingUserPhoneNumber = rootView.findViewById(R.id.briefing_phone_number)
        mAcknowledgementButton = rootView.findViewById(R.id.briefing_acknowledged_button)
        mSignedOnLayout = rootView.findViewById(R.id.briefing_signed_on_linear_layout)
        mLoaderLayout = rootView.findViewById(R.id.acknowledgement_loading_layout)
        mSpinnerView = rootView.findViewById(R.id.acknowledgement_progress_spinner)
        mAcknowledgedText = rootView.findViewById(R.id.acknowledgement_success_text)
        quillWebView = rootView.findViewById(R.id.quill_webview)
        briefingSignature = rootView.findViewById(R.id.briefing_signature_name)
        mLoaderLayout.visibility = View.GONE
        mAcknowledgedText.visibility = View.GONE
        mSiteId = mSession.siteId
        mSiteBriefingView.movementMethod = ScrollingMovementMethod()
        mAcknowledgementButton.setOnClickListener { acknowledgeBriefing() }

        return rootView
    }


    override fun onDestroyView() {
        super.onDestroyView()
        mRealm.close()
    }


    private fun acknowledgeBriefing() {
        // Send a briefing read to the server
        transitionFadeViews(mAcknowledgementButton, mLoaderLayout, false)
        val spinnerColour = ContextCompat.getColor(requireActivity(), R.color.grey_primary)
        mSpinnerView.drawable.setColorFilter(spinnerColour, PorterDuff.Mode.SRC_IN)
        val animation = AnimationUtils.loadAnimation(activity, R.anim.rotate_around_center)
        mSpinnerView.startAnimation(animation)
        presenterImpl.acknowledgeBriefing()

    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        Log.i(LOG, "Preparing options")
        val user = UserService(mRealm)
        val siteSettings = SiteSettingsService(mRealm)
        val abilities = UserAbilitiesService(mRealm)

        when (abilities.hasManagerPermissions(user.currentUserId, siteSettings.siteId) && !presenterImpl.canEditTextRichText()) {
            true -> menu.findItem(R.id.new_briefing)?.isVisible = true
            else -> menu.findItem(R.id.new_briefing)?.isVisible = false
        }

        menu.findItem(R.id.save_briefing)?.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                // If at the main page, finish the activity
                router.navigateToDocumentList(requireActivity())
            }
            R.id.new_briefing -> (activity as BriefingsActivity).navigateToCreateBriefingFragment(workerBriefing.id.toLong())
        }
        return true
    }

    /**
     *
     * Animations
     *
     */

    private fun transitionFadeViews(startView: View, finalView: View, finalAct: Boolean) {
        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.interpolator = AccelerateInterpolator()
        fadeOut.duration = 500

        fadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                startView.visibility = View.GONE
                finalView.visibility = View.VISIBLE

                Handler(Looper.getMainLooper()).postDelayed({
                    if (finalAct && activity != null) {
                        router.navigateToDocumentList(requireActivity())
                    }
                }, 500)
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })

        startView.startAnimation(fadeOut)
    }

    companion object {

        private val LOG = BriefingsFragment::class.java.simpleName
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun showDataWithRichText(url:String,header:MutableMap<String, String>) {
        mSiteBriefingView.visibility = View.GONE
        quillWebView.settings?.run {
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            allowFileAccess = true
        }
        quillWebView.loadUrl(url,header)
        quillWebView.webChromeClient = CustomChromeWebClient(requireActivity())
        quillWebView.webViewClient = CustomWebClient(setContent = {
            webView ->  webView?.loadUrl("javascript:window.set_content(${Gson().toJson(workerBriefing.content)})")
            presenterImpl.seenBriefing()
        })
    }

    override fun showDataWithPlainText() {
        quillWebView.visibility = View.GONE
        mSiteBriefingView.text = workerBriefing.content
        presenterImpl.seenBriefing()
    }


    override fun showProgressLayout() {
        mLoaderLayout.visibility = View.VISIBLE
    }

    override fun hideProgressLayout() {
        if (activity != null) {
            NotificationUtil.cancelNotifications(requireActivity(), NotificationUtil.NOTIFICATION_BRIEFING_ID)
            // Show the success text
            Handler(Looper.getMainLooper()).postDelayed({ transitionFadeViews(mLoaderLayout, mAcknowledgedText, true) }, 1000)
        }
        mLoaderLayout.visibility = View.GONE
        mAcknowledgementButton.visibility = View.GONE
    }

    override fun showDataErrors(error: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),error)
    }

    override fun showNetworkError() {
        AlertDialogMessageHelper.networkErrorMessage(requireContext())
    }

    override fun hideAcknowledgeButton() {
        mAcknowledgementButton.visibility = View.GONE
        mAcknowledgedText.visibility = View.VISIBLE
    }

    override fun showAcknowledgeButton() {
        mAcknowledgementButton.visibility = View.VISIBLE
    }

    override fun showSignature() {
        briefingSignature.text = """${workerBriefing.set_by_user.first_name} ${workerBriefing.set_by_user.last_name}"""
        briefingCompanyName.text = workerBriefing.set_by_user.company.name
        briefingUserPhoneNumber.text = workerBriefing.set_by_user.phone_number
    }

    override fun hideSignature() {
        briefingSignatureTitle.visibility = View.GONE
        briefingSignature.visibility = View.GONE
        briefingCompanyName.visibility = View.GONE
        briefingUserPhoneNumber.visibility = View.GONE
    }
}
