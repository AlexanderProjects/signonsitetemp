package au.com.signonsitenew.ui.prelogin.registration.phone

import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.PhoneValidationUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface RegisterPhonePresenter {
    fun inject(registerPhoneDisplay: RegisterPhoneDisplay)
    fun validatePhoneNumber(alpha2:String,raw:String)
    fun inValidPhoneNumberFormat(number:String):Boolean
    fun inValidPhoneNumberLength(number: String):Boolean
}

class RegisterPhonePresenterImpl @Inject constructor (private val phoneValidationUseCaseImpl:PhoneValidationUseCaseImpl,
                                                      private val sessionManager: SessionManager,
                                                      private val analyticsEventDelegateService: AnalyticsEventDelegateService,
                                                      private val logger: Logger,
                                                      private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl) : BasePresenter(), RegisterPhonePresenter {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var registerPhoneDisplay: RegisterPhoneDisplay

    override fun inject(registerPhoneDisplay: RegisterPhoneDisplay){
        this.registerPhoneDisplay = registerPhoneDisplay
    }

    override fun validatePhoneNumber(alpha2:String, raw:String) {
        disposables.add(internetConnectionUseCaseImpl.isInternetConnected()
                .flatMap {
                    when (it) {
                        true -> phoneValidationUseCaseImpl.validatePhoneNumber(alpha2, raw)
                        else -> Single.just(registerPhoneDisplay.showNetworkErrors())
                    }
                }.subscribe({ response ->
                    if (response is ApiResponse) {
                        if (NetworkErrorValidator.isValidResponse(response)) {
                            sessionManager.updateUserDetail(Constants.USER_PHONE, raw)
                            sessionManager.updateUserDetail(Constants.REGISTER_LOCALE, alpha2)
                            registerPhoneDisplay.navigateToNextFragment()
                            analyticsEventDelegateService.registrationPhoneProvided()
                        } else {
                            registerPhoneDisplay.showNoValidPhoneAlertDialog(Constants.BAD_PHONE_NUMBER_VALIDATION_ERROR)
                        }
                    }
                }, {
                    registerPhoneDisplay.showNetworkErrors()
                    logger.e(this::class.java.name, attributes = mapOf(this::validatePhoneNumber.name to it.message))
                }))
    }

    override fun inValidPhoneNumberFormat(number:String):Boolean = !phoneValidationUseCaseImpl.validatePhoneFormat(number)
    override fun inValidPhoneNumberLength(number: String):Boolean = !phoneValidationUseCaseImpl.validatePhoneLength(number)

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}