package au.com.signonsitenew.ui.prelogin.registration.phone

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import com.hbb20.CountryCodePicker
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Step 2 of the registration flow.
 *
 * Fragment for the user to input and validate their phone number.
 */
class RegisterPhoneFragment : DaggerFragment(), RegisterPhoneDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenterImpl: RegisterPhonePresenterImpl by viewModels { viewModelFactory  }

    @Inject
    lateinit var logger: Logger

    private lateinit var onNextRegisterPhoneClickListener: OnNextRegisterPhoneClickListener
    private lateinit var mSession: SessionManager
    private lateinit var mDialog: ProgressDialog
    private lateinit var mCountryPicker: CountryCodePicker
    private lateinit var mPhone: EditText
    private lateinit var mSubmitPhoneButton: Button
    private lateinit var mErrorPhoneFormatTextView : TextView

    fun setOnClickListener(onNextRegisterListener: OnNextRegisterPhoneClickListener){
        this.onNextRegisterPhoneClickListener = onNextRegisterListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_register_phone, container, false)
        presenterImpl.inject(this)
        mDialog = ProgressDialog(activity)
        mDialog.setMessage("Validating...")
        mDialog.isIndeterminate = true
        mDialog.setCancelable(false)
        mSession = SessionManager(activity)
        mCountryPicker = rootView.findViewById(R.id.ccp)
        mPhone = rootView.findViewById(R.id.phoneEditText)
        mErrorPhoneFormatTextView = rootView.findViewById(R.id.wrong_phone_number_formatted_error)
        mPhone.addTextChangedListener(EditTextWatcher(mPhone,object :EditTextWatcher.AddAction{
            override fun call(text: String?) {
                when{
                    text?.let { presenterImpl.inValidPhoneNumberFormat(it) }!! -> mErrorPhoneFormatTextView.visibility = View.VISIBLE
                    presenterImpl.inValidPhoneNumberLength(text) -> mErrorPhoneFormatTextView.visibility = View.VISIBLE
                    else -> mErrorPhoneFormatTextView.visibility = View.INVISIBLE
                }
            }

        }))
        mSubmitPhoneButton = rootView.findViewById(R.id.submitDetailsButton)

        // Configure the country picker
        mCountryPicker.setFlagBorderColor(ContextCompat.getColor(requireActivity(), R.color.hint_text_colour))
        val countryCode = NetworkUtil.determineCountryCode(activity)
        mCountryPicker.setDefaultCountryUsingNameCode(countryCode)
        var countries = "AU,NZ"
        if (!countries.contains(countryCode)) {
            countries = "$countries,$countryCode"
        }
        mCountryPicker.setCountryPreference(countries)
        mSubmitPhoneButton.setOnClickListener {
            // Validate phone number
            val phone = mPhone.text.toString().trim { it <= ' ' }
            val alpha2 = mCountryPicker.selectedCountryNameCode
            when {
                presenterImpl.inValidPhoneNumberLength(phone) ->  AlertDialogMessageHelper.formatValidationErrorMessage(activity,Constants.PHONE_DATA_FORMAT_ERROR,Constants.DATA_FORMAT_ERROR_TITLE)
                presenterImpl.inValidPhoneNumberLength(phone) -> AlertDialogMessageHelper.formatValidationErrorMessage(activity, Constants.PHONE_DATA_LENGTH_ERROR, Constants.DATA_FORMAT_ERROR_TITLE)
                else ->  presenterImpl.validatePhoneNumber(alpha2,phone)
            }
        }
        return rootView
    }

    override fun onPause() {
        super.onPause()
        // Dismiss dialog if it is showing
        if (mDialog.isShowing) {
            mDialog.dismiss()
        }
    }

    override fun showNoValidPhoneAlertDialog(message: String) {
        AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),message)
    }

    override fun showNetworkErrors() {
        activity?.let { AlertDialogMessageHelper.networkErrorMessage(it) }
    }

    override fun navigateToNextFragment() {
        logger.d( this::class.java.name +" - "+ Constants.USER_PHONE_NUMBER_REGISTRATION_STEP_TWO,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail))
        onNextRegisterPhoneClickListener.navigateToRegisterPassword()
    }

    interface OnNextRegisterPhoneClickListener{
        fun navigateToRegisterPassword()
    }

}