package au.com.signonsitenew.ui.evacuation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Emergency fragment that is used to launch Emergencies from.
 *
 * If an emergency is already in progress, the app will display to the workers that they should
 * proceeed to the muster point.
 */
class EmergencyInitialFragment : DaggerFragment(), EmergencyInitialDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var mSession: SessionManager? = null
    private lateinit var presenter: EmergencyInitialFragmentPresenter
    private lateinit var mInfoTextView: TextView
    private lateinit var mDrillButton: Button
    private lateinit var mEmergencyButton: Button


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_emergency, container, false)
        val actionBar = (activity as EmergencyActivity?)!!.supportActionBar
        val initiator = requireArguments().getBoolean(Constants.BUNDLE_EMERGENCY_INITIATOR)
        presenter = ViewModelProvider(this, viewModelFactory).get(EmergencyInitialFragmentPresenterImpl::class.java)
        presenter.inject(this)
        presenter.observeState()
        mInfoTextView = rootView.findViewById(R.id.emergency_info_text_view)
        mDrillButton = rootView.findViewById(R.id.drill_evacuation_button)
        mEmergencyButton = rootView.findViewById(R.id.emergency_evacuation_button)
        mSession = SessionManager(activity)
        val isEmergency = mSession!!.isEmergency
        if (isEmergency && !initiator) {
            actionBar?.setDisplayHomeAsUpEnabled(false)
            mInfoTextView.text = requireContext().resources.getString(R.string.muster_point_message)
            mDrillButton.visibility = View.GONE
            mEmergencyButton.visibility = View.GONE
        } else if (isEmergency && initiator) {
            // Retrieve user list and navigate to check off view. The null parameter means that the
            // call will not initiate a new evacuation and will continue on the old one. This ensures
            // that the evacuation in progress will not accidentally have its "drill" or "actual" status
            // overwritten.
            actionBar?.setDisplayHomeAsUpEnabled(true)
            presenter.visitorEvacuation()
        } else {
            actionBar?.setDisplayHomeAsUpEnabled(true)
        }
        mDrillButton.setOnClickListener {
            // Check if network is available
            if (NetworkUtil.networkIsConnected(activity)) {
                initiateEvacuation(false)
            } else {
                NetworkUtil.displayNoNetworkToast(activity)
            }
        }
        mEmergencyButton.setOnClickListener {
            // Check if network is available
            if (NetworkUtil.networkIsConnected(activity)) {
                initiateEvacuation(true)
            } else {
                NetworkUtil.displayNoNetworkToast(activity)
            }
        }
        return rootView
    }

    private fun initiateEvacuation(actualEmergency: Boolean?) {
        // Start progress screen
        var actualEmergency = actualEmergency
        (activity as EmergencyActivity?)!!.showEmergencyProgressView()
        if (actualEmergency == null) {
            actualEmergency = false
        }
        startEvacuation(actualEmergency)
    }

    private fun startEvacuation(actualEmergency: Boolean) {
        presenter.startEvacuation(actualEmergency)
    }

    override fun navigateToEmergencyProgressActivity() {
        (activity as EmergencyActivity?)!!.showEmergencyContentView()
        val intent = Intent(activity, EmergencyProgressActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun showEmergencyError(error: Throwable) {
        SLog.i(LOG, "A JSON Exception occurred. " + error.message)
        (activity as EmergencyActivity?)!!.showEmergencyContentView()
        DarkToast.makeText(activity, "There was a problem starting the evacuation, " +
                "please try again.")
    }

    override fun showEmergencyTriggerError(apiResponse: ApiResponse) {
        SLog.i(LOG, "Emergency was NOT started. Response: $apiResponse")
        (activity as EmergencyActivity?)!!.showEmergencyContentView()
        DarkToast.makeText(activity, "There was a problem starting the evacuation, " +
                "please try again. " + apiResponse.status)
        DarkToast.makeText(activity, Constants.EVACUATION_IN_PROGRESS)
    }

    override fun getEvacuationVisitor() {
       (activity as EmergencyActivity?)!!.showEmergencyContentView()
        presenter.visitorEvacuation()
    }

    companion object {
        private val LOG = EmergencyInitialFragment::class.java.simpleName
    }
}