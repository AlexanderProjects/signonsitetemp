package au.com.signonsitenew.ui.prelogin.registration.confirmation

import au.com.signonsitenew.domain.models.UserRegistrationResponse

interface RegisterConfirmationDisplay {
    fun showNetworkErrors()
    fun showDataError(error:String)
    fun successResponseNavigateToCompleteRegistration()
}