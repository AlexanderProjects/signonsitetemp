package au.com.signonsitenew.ui.passport

interface PassportNavigationListener {
    fun navigateToTab(index: Int?)
}