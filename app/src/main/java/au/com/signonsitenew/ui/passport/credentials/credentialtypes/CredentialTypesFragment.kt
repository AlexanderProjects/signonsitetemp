package au.com.signonsitenew.ui.passport.credentials.credentialtypes

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.TypeOfCredentialsListAdapter
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.CredentialType
import au.com.signonsitenew.domain.utilities.Builder
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment.NavigateToPassport
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CredentialTypesFragment : DaggerFragment(), CredentialTypesView, NavigateToPassport {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager
    lateinit var presenterImpl: CredentialTypesPresenterImpl
    private var adapter: TypeOfCredentialsListAdapter? = null
    private var credentialTypesList: RecyclerView? = null
    private lateinit var credentialTypeToolbar: Toolbar
    private var credentialList: List<CredentialType>? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.credentials_types_fragment, container, false)
        credentialTypesList = view.findViewById(R.id.credential_types_list)
        credentialTypeToolbar = view.findViewById(R.id.credential_types_toolbar)
        credentialTypeToolbar.title = Constants.CREDENTIAL_TYPES_NAME

        (activity as AppCompatActivity?)!!.setSupportActionBar(credentialTypeToolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        CredentialCreationFormFragment.setNavigateToPassport(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this, viewModelFactory).get(CredentialTypesPresenterImpl::class.java)
        presenterImpl.inject(this)
        presenterImpl.retrieveListOfCredentialTypes()
    }

    override fun showData(credentials: List<CredentialType>) {
        credentialList = Builder.customLicense(credentials as MutableList<CredentialType>)
        adapter = TypeOfCredentialsListAdapter(credentialList as MutableList<CredentialType>, callAction = { credType -> navigateToCreateCredential(credType) })
        credentialTypesList!!.adapter = adapter
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenterImpl.retrieveListOfCredentialTypes() }
    }

    override fun showDataError(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),errorMessage)
    }

    private fun navigateToCreateCredential(credentialType: CredentialType) {
        NavigationHelper.navigateToCreateCredential(activity, credentialType)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search_action, menu)
        if(menu.findItem(R.id.share_button) != null)
            menu.findItem(R.id.share_button).isVisible = false
        val item = menu.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(QueryTextListener(addData = {text-> adapter!!.getFilter().filter(text)}))
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun navigate() {
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }

    companion object {
        @JvmStatic
        fun newInstance(): CredentialTypesFragment {
            return CredentialTypesFragment()
        }
    }


}