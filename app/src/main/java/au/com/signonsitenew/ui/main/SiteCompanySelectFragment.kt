package au.com.signonsitenew.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.CompanySearchRecyclerViewAdapter
import au.com.signonsitenew.realm.Company
import au.com.signonsitenew.utilities.DebouncedOnClickListener
import co.moonmonkeylabs.realmsearchview.RealmSearchView
import io.realm.Realm

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnCompanyInteractionListener]
 * interface.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class   SiteCompanySelectFragment : Fragment() {

    private var mListener: OnCompanyInteractionListener? = null

    private var mRealm: Realm? = null
    private var mAdapter: CompanySearchRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRealm = Realm.getDefaultInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_site_company_select, container, false)

        // Set the title for the parent activity
        (activity as FirstSignOnActivity).mTitleTextView.text = "Select Your Company"

        val realmSearchView = rootView.findViewById<RealmSearchView>(R.id.search_view)
        val noCompanyButton: Button = rootView.findViewById(R.id.company_not_found_button)

        // Set the adapter
        mAdapter = CompanySearchRecyclerViewAdapter(activity, mRealm, "name", mListener)
        realmSearchView.setAdapter(mAdapter)

        noCompanyButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                (activity as FirstSignOnActivity).navigateToAddNewCompanyFragment()
            }
        })

        return rootView
    }

    override fun onResume() {
        super.onResume()
        // If coming back from the screens ahead where users may have been signed on, refresh the adapter.
        if (mAdapter != null) {
            mAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnCompanyInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(requireContext().toString() + " must implement OnCompanyInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onDestroy() {
        super.onDestroy()

        if (mRealm != null) {
            mRealm!!.close()
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnCompanyInteractionListener {

        fun onCompanySelected(company: Company)
    }
}
