package au.com.signonsitenew.ui.documents.permits.template

import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentTemplatePermitBinding
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SitePermitTemplate
import au.com.signonsitenew.ui.documents.permits.template.adapter.TemplatePermitRecyclerViewAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class TemplatePermitFragment : DaggerFragment(), TemplatePermitsDisplay{

    private var _binding:FragmentTemplatePermitBinding? = null
    private var template: SitePermitTemplate? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router
    private val presenter:TemplatePermitFragmentPresenterImpl by viewModels { viewModelFactory  }
    private lateinit var templatePermitRecyclerViewAdapter : TemplatePermitRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTemplatePermitBinding.inflate(inflater, container, false)
        val view = binding.root
        arguments?.let { it ->
            template = it.getParcelable(Constants.PERMIT_TEMPLATE_OBJECT)
        }
        binding.permitTemplateToolbar.title = String().empty()
        (requireActivity() as DaggerAppCompatActivity).setSupportActionBar(binding.permitTemplateToolbar)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.permitTemplateToolbarTitleTextView.text = resources.getString(R.string.available_permits)
        presenter.inject(this)
        presenter.observeStates()
        presenter.getPermitTemplates()

        return view
    }

    override fun showPermitTemplateList(permitTemplateList: List<SitePermitTemplate>) {
        templatePermitRecyclerViewAdapter = TemplatePermitRecyclerViewAdapter(permitTemplateList) { presenter.createPermit(it) }
        binding.permitsTemplateList.adapter = templatePermitRecyclerViewAdapter
    }


    override fun showPermitTemplateError() {
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) { presenter.getPermitTemplates() }
    }

    override fun navigateToPermitDetails(permitInfo: PermitInfo) {
        router.navigateToPermitDetails(requireActivity(),permitInfo)
    }

    override fun showProgressView() {
        router.showProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun removeProgressView() {
        router.removeProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                // If at the main page, finish the activity
                router.navigateToPermitActivity(requireActivity())
            }
        }
        return true
    }

}