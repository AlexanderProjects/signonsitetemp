package au.com.signonsitenew.ui.prelogin.registration.details

interface RegisterDetailsDisplay {
    fun showNoValidEmailAlertDialog(message:String)
    fun showNetworkErrors()
    fun navigateToNextFragment()
}