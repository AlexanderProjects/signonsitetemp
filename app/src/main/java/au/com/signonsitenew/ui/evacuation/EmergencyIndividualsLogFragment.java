package au.com.signonsitenew.ui.evacuation;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.EmergencyChecklistAdapter;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.SiteAttendee;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class EmergencyIndividualsLogFragment extends Fragment {

    protected ExpandableListView mAttendanceListView;
    protected EmergencyChecklistAdapter mAdapter;
    protected List<String> mListTitles;
    protected HashMap<String, List<SiteAttendee>> mAttendanceList;
    protected Realm mRealm;

    public EmergencyIndividualsLogFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_emergency_site_log_individuals, container, false);
        mRealm = Realm.getInstance(RealmManager.getRealmConfiguration());

        mAttendanceListView = rootView.findViewById(R.id.expandable_list_view);
        mAttendanceList = new HashMap<>();
        mAttendanceListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            // Tick off the image view
            SiteAttendee visitor = (SiteAttendee) mAdapter.getChild(groupPosition, childPosition);
            boolean status = visitor.isMarkedSafe();

            mRealm.beginTransaction();
            SiteAttendee realmVisitor = mRealm.where(SiteAttendee.class).equalTo("userId", visitor.getUserId()).findFirst();
            assert realmVisitor != null;
            realmVisitor.setMarkedSafe(!status);
            mRealm.commitTransaction();

            // Set the adapter data again
            setAdapterData();

            return false;
        });

        // Set Visitor data
        setAdapterData();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @SuppressLint("SetTextI18n")
    protected void setAdapterData() {
        RealmResults<SiteAttendee> visitors = mRealm.where(SiteAttendee.class).findAll();
        int totalAttendance = visitors.size();

        List<SiteAttendee> checkedOff = new ArrayList<>();
        List<SiteAttendee> notCheckedOff = new ArrayList<>();
        for (SiteAttendee visitor : visitors) {
            if (visitor.isMarkedSafe()) {
                checkedOff.add(visitor);
            }
            else {
                notCheckedOff.add(visitor);
            }
        }

        // Update Log Count
        int checkedCount = checkedOff.size();
        Objects.requireNonNull(((EmergencyProgressActivity) requireActivity())
                .mCheckedCountTextView)
                .setText(checkedCount + " / " + totalAttendance);

        mAttendanceList.put("Not Marked Off", notCheckedOff);
        mAttendanceList.put("Marked Off", checkedOff);

        mListTitles = new ArrayList<>(mAttendanceList.keySet());

        if (mAdapter == null) {
            mAdapter = new EmergencyChecklistAdapter(getActivity(), mListTitles, mAttendanceList);
            mAttendanceListView.setAdapter(mAdapter);
        }
        else {
            ((EmergencyChecklistAdapter)mAttendanceListView.getExpandableListAdapter()).refill(mListTitles, mAttendanceList);
        }

        int groupCount = mAdapter.getGroupCount();
        for (int i = 0; i < groupCount; i++){
            mAttendanceListView.expandGroup(i);
        }
    }

}
