package au.com.signonsitenew.ui.documents.permits.details.task

import android.net.Uri
import au.com.signonsitenew.domain.models.ComponentTypeForm
import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SaveButtonState
import au.com.signonsitenew.domain.models.state.PermitContentType
import au.com.signonsitenew.domain.models.state.PermitTaskTabFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitTaskTabUseCase
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.events.RxBusTaskImageUri
import au.com.signonsitenew.events.RxBusTaskContentTypeComponent
import au.com.signonsitenew.models.UploadImageResponse
import au.com.signonsitenew.utilities.BasePresenter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxjava3.subjects.PublishSubject
import org.greenrobot.eventbus.Subscribe
import java.io.File
import javax.inject.Inject

interface TaskFragmentPresenter {
    fun inject(display: TaskDisplay)
    fun observeStates()
    fun onClickInStartDateTimeLayout(isLayoutVisible:Boolean)
    fun onClickInEndDateTimeLayout(isLayoutVisible: Boolean)
    fun formatStartDate(date:String?, isOnClick:Boolean)
    fun formatEndDate(date:String?, isOnClick:Boolean)
    fun checkForEditPermissions(permitInfo: PermitInfo)
    fun setPermitInfo(permitInfo: PermitInfo)
    fun getPermitInto():PermitInfo
    fun updatePermitState(permitInfo: PermitInfo)
    fun isStartDateAfterNow(dateTime: String):Boolean
    fun isEndDateIsAfterStartDate(startDateTime:String, endDateTime:String):Boolean
    fun mapStringTypeToPermitContentType(contentType:String): PermitContentType
    fun getLastContentTypeResponse(responses:List<PermitContentTypeResponses>): PermitContentTypeResponses
    fun sendContentTypeComponent(componentTypeForm: ComponentTypeForm)
    fun observeImageUri():Observable<Uri>
    fun isThereAnyPhoto(photoList:List<Uri>, callback:()->Unit)
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun isEnableToEdit(permitInfo: PermitInfo):Boolean
    fun getLocalUserFullName():String
}
class TaskFragmentPresenterImpl @Inject constructor (private val permitTaskTabUseCase: PermitTaskTabUseCase,
                                                     private val rxBusTaskImageUri: RxBusTaskImageUri,
                                                     private val rxBusTaskContentTypeComponent: RxBusTaskContentTypeComponent):BasePresenter(),TaskFragmentPresenter{

    lateinit var display: TaskDisplay
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<PermitTaskTabFragmentState>()


    override fun setPermitInfo(permitInfo: PermitInfo) {
        permitTaskTabUseCase.setPermitInfo(permitInfo)
    }

    override fun getPermitInto(): PermitInfo {
        return permitTaskTabUseCase.getPermitInfo()
    }

    override fun updatePermitState(permitInfo: PermitInfo) {
        permitTaskTabUseCase.sendPermitInfoUpdate(permitInfo)
    }

    override fun isStartDateAfterNow(dateTime: String): Boolean {
        return permitTaskTabUseCase.isStartDateAfterNow(dateTime)
    }

    override fun isEndDateIsAfterStartDate(startDateTime: String, endDateTime: String): Boolean {
        return permitTaskTabUseCase.isEndDateIsAfterStartDate(startDateTime, endDateTime)
    }

    override fun mapStringTypeToPermitContentType(contentType: String): PermitContentType {
        return permitTaskTabUseCase.mapStringTypeToPermitContentType(contentType)
    }

    override fun getLastContentTypeResponse(responses: List<PermitContentTypeResponses>): PermitContentTypeResponses {
        return permitTaskTabUseCase.getLastContentTypeResponse(responses)
    }

    override fun sendContentTypeComponent(componentTypeForm: ComponentTypeForm) {
        rxBusTaskContentTypeComponent.sendComponentTypeForm(componentTypeForm)
    }

    override fun observeImageUri(): Observable<Uri> {
        return rxBusTaskImageUri.toImageUriObservable()
    }

    override fun isThereAnyPhoto(photoList: List<Uri>, callback: () -> Unit) {
        if(photoList.isNotEmpty()){ callback() }
    }

    override fun uploadImages(file: File, fileName: String): Single<UploadImageResponse> {
        return permitTaskTabUseCase.uploadImages(file, fileName)
    }

    override fun isEnableToEdit(permitInfo: PermitInfo): Boolean {
        return permitTaskTabUseCase.isEnableToEdit(permitInfo)
    }

    override fun getLocalUserFullName(): String {
        return permitTaskTabUseCase.getLocalUserFullName()
    }

    override fun inject(display: TaskDisplay) {
        this.display = display
    }

    override fun observeStates() {
        disposable.add(uiStateObservable.subscribe {
            when(it){
                is PermitTaskTabFragmentState.ReadOnlyDates ->{
                    display.hideStarDateRow()
                    display.hideEndDateRow()
                    display.showComponents()
                    display.disableClickOnRows()
                    display.hideChevrons()
                }
                is PermitTaskTabFragmentState.LoadAllViewComponents ->{
                    display.showChevrons()
                    display.showComponents()
                    display.enableClickOnRows()
                }
                is PermitTaskTabFragmentState.ClickOnStartDate -> {
                    if(it.isLayoutVisible){
                        display.hideStarDateRow()
                        display.showStartDateArrowDown()
                    }else{
                        display.showStartDateRow()
                        display.showStartDateArrowUp()
                    }
                }
                is PermitTaskTabFragmentState.ClickOnEndDate ->{
                    if(it.isLayoutVisible){
                        display.hideEndDateRow()
                        display.showEndDateArrowDown()
                    }else{
                        display.showEndDateRow()
                        display.showEndDateArrowUp()
                    }
                }
                is PermitTaskTabFragmentState.ShowStartDate ->{
                    display.showStartDateTime(permitTaskTabUseCase.convertIsoFormatToReadableUserFormat(it.date))
                    display.setStartDateTimeInPermitInfo(permitTaskTabUseCase.convertUIDateTimeToISODateTimeFormat(it.date))
                    display.hideStarDateRow()

                }
                is PermitTaskTabFragmentState.ShowEndDate ->{
                    display.showEndDateTime(permitTaskTabUseCase.convertIsoFormatToReadableUserFormat(it.date))
                    display.setEndDateTimeInPermitInfo(permitTaskTabUseCase.convertUIDateTimeToISODateTimeFormat(it.date))
                    display.hideEndDateRow()

                }
                is PermitTaskTabFragmentState.UpdateStartDate ->{
                    display.showStartDateTime(permitTaskTabUseCase.convertCalendarFormatToUIFormat(it.date))
                    display.setStartDateTimeInPermitInfo(permitTaskTabUseCase.convertUIDateTimeToISODateTimeFormat(it.date))
                }
                is PermitTaskTabFragmentState.UpdateEndDate ->{
                    display.showEndDateTime(permitTaskTabUseCase.convertCalendarFormatToUIFormat(it.date))
                    display.setEndDateTimeInPermitInfo(permitTaskTabUseCase.convertUIDateTimeToISODateTimeFormat(it.date))
                }
                is PermitTaskTabFragmentState.ShowEmptyStartDate ->{
                    display.showEmptyStartDateMessage()
                }
                is PermitTaskTabFragmentState.ShowEmptyEndDate ->{
                    display.showEmptyEndDateMessage()
                }
            }
        })
    }

    override fun onClickInStartDateTimeLayout(isLayoutVisible: Boolean) {
        uiStateObservable.onNext(PermitTaskTabFragmentState.ClickOnStartDate(isLayoutVisible))
    }

    override fun onClickInEndDateTimeLayout(isLayoutVisible: Boolean) {
        uiStateObservable.onNext(PermitTaskTabFragmentState.ClickOnEndDate(isLayoutVisible))
    }

    override fun formatStartDate(date: String?, isOnClick:Boolean){
       if(date != null){
           when(isOnClick){
               true -> uiStateObservable.onNext(PermitTaskTabFragmentState.UpdateStartDate(permitTaskTabUseCase.parseCalendarDates(date)))
               false-> uiStateObservable.onNext(PermitTaskTabFragmentState.ShowStartDate(permitTaskTabUseCase.parseIsoFormatDates(date)))
           }
       }else{
           uiStateObservable.onNext(PermitTaskTabFragmentState.ShowEmptyStartDate)
       }
    }

    override fun formatEndDate(date: String?, isOnClick:Boolean) {
        if(date != null){
            when(isOnClick){
                true ->  uiStateObservable.onNext(PermitTaskTabFragmentState.UpdateEndDate(permitTaskTabUseCase.parseCalendarDates(date)))
                false -> uiStateObservable.onNext(PermitTaskTabFragmentState.ShowEndDate(permitTaskTabUseCase.parseIsoFormatDates(date)))
            }
        }else{
            uiStateObservable.onNext(PermitTaskTabFragmentState.ShowEmptyEndDate)
        }
    }

    override fun checkForEditPermissions(permitInfo: PermitInfo) {
        if(permitTaskTabUseCase.isEnableToEdit(permitInfo)){
            uiStateObservable.onNext(PermitTaskTabFragmentState.LoadAllViewComponents)
        }else{
            uiStateObservable.onNext(PermitTaskTabFragmentState.ReadOnlyDates)
        }
    }

    override fun onCleared() {
        disposable.dispose()
    }
}