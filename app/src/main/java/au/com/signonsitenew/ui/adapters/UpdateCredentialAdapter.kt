package au.com.signonsitenew.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.models.CredentialField
import au.com.signonsitenew.domain.utilities.Builder
import au.com.signonsitenew.utilities.*
import au.com.signonsitenew.domain.utilities.CredentialValidator.checkedForValidSpinnerOption
import au.com.signonsitenew.domain.utilities.CredentialValidator.setCredential
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialExpiryDate
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialIssueBy
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialIssueDate
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialReference
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialRegistrationNumber
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateCredentialRto
import au.com.signonsitenew.domain.utilities.CredentialValidator.validateIdentifier

class UpdateCredentialAdapter(val credentialFields: List<CredentialField?>?, private val credential: Credential) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        return when (position) {
            RowType.EDIT_TEXT_ROW -> {
                EditTexViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_create_credential_edit_text, parent, false))
            }
            RowType.SPINNER_ROW -> {
                SpinnerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_create_credential_spinner, parent, false))
            }
            else -> {
                PickerDateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_create_credential_date_picker, parent, false))
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        setCredential(credential)
        if (credentialFields != null && credentialFields.isNotEmpty()) {
            validateIdentifier()?.let {
                if (viewHolder is EditTexViewHolder && RowType.EDIT_TEXT_ROW == viewHolder.getItemViewType()) {
                    viewHolder.credentialFieldTitle.text = credentialFields[position]?.title ?: viewHolder.credentialFieldTitle.setText(credentialFields[position]?.value).toString()
                    viewHolder.credentialFieldEditText.setText(credentialFields[position]?.value)
                }
            }
            validateCredentialIssueDate()?.let {
                if (viewHolder is PickerDateViewHolder && RowType.PICKER_DATE_ROW == viewHolder.getItemViewType()) {
                    viewHolder.credentialPickerDateFieldTitle.text = credentialFields[position]?.title
                    viewHolder.credentialPickerDateFieldEditText.setText(DateFormatUtil.convertDateFormatResponse(credentialFields[position]?.value))
                    viewHolder.credentialPickerDateFieldEditText.setOnClickListener { DatePickerHelper.datePickerYYMMDD(viewHolder.itemView.context) { text: String? -> viewHolder.credentialPickerDateFieldEditText.setText(DateFormatUtil.convertDateFormatResponse(text)) } }
                }
            }
            validateCredentialExpiryDate()?.let {
                if (viewHolder is PickerDateViewHolder && RowType.PICKER_DATE_ROW == viewHolder.getItemViewType()) {
                    viewHolder.credentialPickerDateFieldTitle.text = credentialFields[position]?.title
                    viewHolder.credentialPickerDateFieldEditText.setText(DateFormatUtil.convertDateFormatResponse(credentialFields[position]?.value))
                    viewHolder.credentialPickerDateFieldEditText.setOnClickListener { DatePickerHelper.datePickerYYMMDD(viewHolder.itemView.context) { text: String? -> viewHolder.credentialPickerDateFieldEditText.setText(DateFormatUtil.convertDateFormatResponse(text)) } }
                }
            }
            validateCredentialIssueBy()?.let {
                if (viewHolder is SpinnerViewHolder) {
                    viewHolder.credentialFieldTitle.text = Constants.ISSUED_BY
                    val issuersList = Builder.addNotSelectedOption(credential.credential_type!!.issuers as MutableList<String>)
                    val issuedByAdapter = ArrayAdapter(viewHolder.view.context, R.layout.support_simple_spinner_dropdown_item, issuersList)
                    viewHolder.credentialSpinnerValue.adapter = issuedByAdapter
                    for (i in issuersList.indices) {
                        if (issuersList[i].equals(credentialFields[position]?.value, ignoreCase = true) && credentialFields[position]?.title.equals(Constants.ISSUED_BY, ignoreCase = true)) viewHolder.credentialSpinnerValue.setSelection(i)
                    }
                }
            }
            validateCredentialRto()?.let {
                if (viewHolder is EditTexViewHolder) {
                    viewHolder.credentialFieldTitle.text = credentialFields[position]?.title
                    viewHolder.credentialFieldEditText.setText(credentialFields[position]?.value)
                }
            }
            validateCredentialRegistrationNumber()?.let {
                if (viewHolder is EditTexViewHolder) {
                    viewHolder.credentialFieldTitle.text = credentialFields[position]?.title
                    viewHolder.credentialFieldEditText.setText(credentialFields[position]?.value)
                }
            }
            validateCredentialReference()?.let {
                if (viewHolder is EditTexViewHolder) {
                    viewHolder.credentialFieldTitle.text = credentialFields[position]?.title
                    viewHolder.credentialFieldEditText.setText(credentialFields[position]?.value)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return credentialFields!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (credentialFields!![position]?.title.equals(Constants.ISSUED_BY, ignoreCase = true) && checkedForValidSpinnerOption()) {
            RowType.SPINNER_ROW
        } else if (credentialFields[position]?.title.equals(Constants.ISSUE_DATE, ignoreCase = true) || credentialFields[position]?.title.equals(Constants.EXPIRY_DATE, ignoreCase = true)) {
            RowType.PICKER_DATE_ROW
        } else {
            RowType.EDIT_TEXT_ROW
        }
    }

    inner class SpinnerViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val credentialFieldTitle: TextView = view.findViewById(R.id.credential_field_spinner_title)
        val credentialSpinnerValue: Spinner = view.findViewById(R.id.credential_spinner_value)

        init {
            credentialSpinnerValue.onItemSelectedListener = AdapterViewOnItemSelected(credentialSpinnerValue,AdapterViewOnItemSelected.CallAction { credentialFields!![adapterPosition]?.value = credentialSpinnerValue.selectedItem.toString() })
        }
    }

    inner class EditTexViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val credentialFieldTitle: TextView = itemView.findViewById(R.id.credential_field_title)
        val credentialFieldEditText: EditText = itemView.findViewById(R.id.credential_field_value_edit_text)

        init {
            credentialFieldEditText.addTextChangedListener(EditTextWatcher(credentialFieldEditText,object :EditTextWatcher.CallAction{
                override fun call() {
                    credentialFields!![adapterPosition]?.value = credentialFieldEditText.text.toString()
                }
            }))
        }
    }

    inner class PickerDateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val credentialPickerDateFieldTitle: TextView = itemView.findViewById(R.id.credential_picker_date_field_title)
        val credentialPickerDateFieldEditText: EditText = itemView.findViewById(R.id.credential_picker_date_field_value_edit_text)

        init {
            credentialPickerDateFieldEditText.addTextChangedListener(EditTextWatcherNoFocusForPickers{ credentialFields!![adapterPosition]?.value = credentialPickerDateFieldEditText.text.toString() })

        }
    }

    private interface RowType {
        companion object {
            const val EDIT_TEXT_ROW = 0
            const val SPINNER_ROW = 1
            const val PICKER_DATE_ROW = 2
        }
    }

}