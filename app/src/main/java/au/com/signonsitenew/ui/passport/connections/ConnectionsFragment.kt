package au.com.signonsitenew.ui.passport.connections

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.domain.utilities.NotificationBadgeCounter
import au.com.signonsitenew.ui.adapters.ConnectionsRecyclerViewAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.ui.passport.connections.connectiondetails.ConnectionDetailsFragment
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ConnectionsFragment : DaggerFragment(), ConnectionsDisplay, ConnectionDetailsFragment.ConnectionsListLoader  {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var router: Router

    companion object {
        fun newInstance() = ConnectionsFragment()
    }
    private lateinit var connectionRecyclerView:RecyclerView
    private lateinit var connectionEmptyTextView: TextView
    private lateinit var presenterImpl: ConnectionsPresenterImpl
    private lateinit var connectionsAdapter:ConnectionsRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.connections_fragment, container, false)
        connectionRecyclerView = view.findViewById(R.id.connection_list)
        connectionEmptyTextView = view.findViewById(R.id.empty_connections_textview)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ConnectionDetailsFragment.setCredentialsListLoader(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this, viewModelFactory).get(ConnectionsPresenterImpl::class.java)
        presenterImpl.inject(this)
        presenterImpl.retrieveConnectionList()
    }

    override fun reloadData(enrolments: List<Enrolment>) {
        presenterImpl.filterEnrolmentsByAttendanceEnabledAndHidden(enrolments).let {
            if(it.isEmpty())connectionEmptyTextView.visibility = View.VISIBLE else connectionEmptyTextView.visibility = View.GONE
        }
        connectionsAdapter = ConnectionsRecyclerViewAdapter(presenterImpl.filterEnrolmentsByAttendanceEnabledAndHidden(enrolments), onClick ={ enrolment ->  router.navigateToConnectionDetails(requireActivity(),enrolment)}, setTimeZone = { enrolment -> presenterImpl.getDateTimeZone(enrolment)})
        connectionRecyclerView.adapter = connectionsAdapter
        presenterImpl.updateBadgeNotificationNumber(NotificationBadgeCounter.notifierNumberForConnections(presenterImpl.filterEnrolmentsByAttendanceEnabledAndHidden(enrolments)))
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenterImpl.retrieveConnectionList() }
    }

    override fun showDataErrors(messages: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),messages)
    }

    override fun refreshListOfConnections() {
        presenterImpl.retrieveConnectionList()
        connectionsAdapter.notifyDataSetChanged()
    }

}
