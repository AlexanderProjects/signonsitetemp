package au.com.signonsitenew.ui.main.menu

import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.menu.MenuUseCase
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class MenuFragmentPresenter @Inject constructor(private val menuUseCase: MenuUseCase,
                                                private val logger:Logger,
                                                private val analyticsEventDelegateService: AnalyticsEventDelegateService): BasePresenter() {

    private lateinit var menuFragmentDisplay: MenuFragmentDisplay

    fun inject(menuFragmentDisplay: MenuFragmentDisplay){
        this.menuFragmentDisplay = menuFragmentDisplay
    }

    fun getPersonalInfo() {
        menuUseCase.getPersonalInfoAsync(object:DisposableSingleObserver<UserInfoResponse>(){
            override fun onSuccess(response: UserInfoResponse) {
                if(NetworkErrorValidator.isValidResponse(response)) {
                    menuFragmentDisplay.retrieveUserData(response.user)
                }else{
                    logger.w(MenuUseCase::class.java.name,attributes = mapOf(this@MenuFragmentPresenter::getPersonalInfo.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                menuFragmentDisplay.showNetworkErrors()
                logger.e(MenuUseCase::class.java.name,attributes = mapOf(this@MenuFragmentPresenter::getPersonalInfo.name to error.message))
            }
        })
    }

    fun menuHelpOpenedAnalytics() = analyticsEventDelegateService.menuHelpOpened()

    override fun onCleared() {
        super.onCleared()
        menuUseCase.dispose()
    }
}