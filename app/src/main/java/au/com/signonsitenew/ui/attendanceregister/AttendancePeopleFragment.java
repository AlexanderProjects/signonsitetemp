package au.com.signonsitenew.ui.attendanceregister;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.AttendanceSearchRecyclerViewAdapter;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import co.moonmonkeylabs.realmsearchview.RealmSearchView;
import io.realm.Realm;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnVisitorInteractionListener}
 * interface.
 */
public class AttendancePeopleFragment extends Fragment {

    private OnVisitorInteractionListener mListener;
    private Realm mRealm;
    private AttendanceSearchRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AttendancePeopleFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_attendance_people, container, false);

        RealmSearchView realmSearchView = rootView.findViewById(R.id.search_view);

        // Set the adapter
        SiteSettingsService settingsService = new SiteSettingsService(mRealm);
        boolean inductionsEnabled = settingsService.siteInductionsEnabled();
        mAdapter = new AttendanceSearchRecyclerViewAdapter(getActivity(), mRealm, "firstName", inductionsEnabled, mListener);
        realmSearchView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // If coming back from the screens ahead where users may have been signed on, refresh the adapter.
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnVisitorInteractionListener) {
            mListener = (OnVisitorInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnVisitorInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mRealm != null) {
            mRealm.close();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnVisitorInteractionListener {
        void onVisitorSelected(SiteAttendee visitor);
    }
}
