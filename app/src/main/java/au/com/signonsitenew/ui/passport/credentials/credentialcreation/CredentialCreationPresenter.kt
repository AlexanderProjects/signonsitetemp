package au.com.signonsitenew.ui.passport.credentials.credentialcreation

import au.com.signonsitenew.domain.models.CredentialCreateUpdateRequest
import au.com.signonsitenew.domain.models.CredentialCreateUpdateResponse
import au.com.signonsitenew.domain.usecases.credentials.CredentialsUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.observers.DisposableSingleObserver
import java.io.File
import javax.inject.Inject

interface CredentialCreationPresenter {
    fun inject(credentialCreationView: CredentialCreationView)
    fun setFrontPhoto(frontPhotoFile: File?):CredentialCreationPresenterImpl
    fun setBackPhoto(backPhotoFile: File?):CredentialCreationPresenterImpl
    fun setRequest(request: CredentialCreateUpdateRequest):CredentialCreationPresenterImpl
    fun createCredential()
}

class CredentialCreationPresenterImpl @Inject constructor(private val credentialsUseCaseImpl:CredentialsUseCaseImpl, private val logger: Logger) : BasePresenter(), CredentialCreationPresenter{

    private lateinit var credentialCreationView: CredentialCreationView
    private var frontPhotoFile: File? = null
    private var backPhotoFile: File? = null
    private lateinit var request: CredentialCreateUpdateRequest

    override fun inject(credentialCreationView: CredentialCreationView) {
        this.credentialCreationView = credentialCreationView
    }
    override fun setFrontPhoto(frontPhotoFile: File?):CredentialCreationPresenterImpl{
        this.frontPhotoFile = frontPhotoFile
        return this
    }
    override fun setBackPhoto(backPhotoFile: File?):CredentialCreationPresenterImpl{
         this.backPhotoFile = backPhotoFile
         return this
    }
    override fun setRequest(request: CredentialCreateUpdateRequest):CredentialCreationPresenterImpl{
        this.request = request
        return this
    }

    override fun createCredential(){
        credentialsUseCaseImpl.setCredentialParameters(frontPhotoFile, backPhotoFile, request)
        credentialsUseCaseImpl.createUpdateCredentialAsync(object :DisposableSingleObserver<CredentialCreateUpdateResponse>(){
            override fun onSuccess(response: CredentialCreateUpdateResponse) {
                if (NetworkErrorValidator.isValidResponse(response))
                    credentialCreationView.goBackToCredentials()
                else {
                    credentialCreationView.showDataError(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@CredentialCreationPresenterImpl::class.java.name, attributes = mapOf(this@CredentialCreationPresenterImpl::createCredential.name to toJson(response)))
                }
            }
            override fun onError(error: Throwable) {
                credentialCreationView.showNetworkErrors()
                logger.e(this@CredentialCreationPresenterImpl::class.java.name, attributes = mapOf(this@CredentialCreationPresenterImpl::createCredential.name to error.message))
            }
        })
    }
}