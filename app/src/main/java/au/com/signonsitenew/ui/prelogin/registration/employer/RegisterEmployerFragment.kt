package au.com.signonsitenew.ui.prelogin.registration.employer

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */
class RegisterEmployerFragment : DaggerFragment(), RegisterEmployerDisplay{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenterImpl: RegisterEmployerPresenterImpl by viewModels { viewModelFactory  }

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService

    @Inject
    lateinit var logger: Logger

    private lateinit var mSubmitButton: Button
    private lateinit var mEmployerAutoComplete: AutoCompleteTextView
    private lateinit var onNextListener: ClickOnNextListener
    private lateinit var dialog: ProgressDialog
    private lateinit var mSession:SessionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_register_employer, container, false)
        presenterImpl.inject(this)
        mSession = SessionManager(activity)
        mEmployerAutoComplete = rootView.findViewById(R.id.employer_auto_complete_text_view)
        mSubmitButton = rootView.findViewById(R.id.next_button)
        mSubmitButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                val employer = mEmployerAutoComplete.text.toString().trim { it <= ' ' }
                if (employer.isEmpty()) {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),Constants.EMPTY_EMPLOYER)
                }
                else {
                    // Go to next fragment
                    val session = SessionManager(activity)
                    session.updateUserDetail(Constants.USER_COMPANY, employer)
                    analyticsEventDelegateService.registrationCompanyNameProvided()
                    onNextListener.navigateToRegisterConfirmation()
                    logger.d( this::class.java.name +" - "+ Constants.USER_COMPANY_NAME_STEP_FOUR,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail))
                }
            }
        })
        return rootView
    }

    override fun onResume() {
        super.onResume()
        presenterImpl.getEmployers(progressDialog = {showProgressBar()}, dismissProgressDialog = {dialog.dismiss()})
    }

    private fun showProgressBar(){
        dialog = ProgressDialog(activity)
        dialog.setMessage("Getting Employers...")
        dialog.isIndeterminate = true
        dialog.setCancelable(false)
        dialog.show()
    }

    fun setOnClickNextListener(onNextListener: ClickOnNextListener){
        this.onNextListener = onNextListener
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(requireActivity())
    }

    override fun showListOfCompanies(companies: List<String>) {
        val autoCompleteArrayAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_dropdown_item_1line, companies.sorted())
        mEmployerAutoComplete.setAdapter(autoCompleteArrayAdapter)
    }

    interface ClickOnNextListener{
        fun navigateToRegisterConfirmation()
    }

}
