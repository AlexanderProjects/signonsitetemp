package au.com.signonsitenew.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import au.com.signonsitenew.ui.documents.briefingslist.BriefingContentListFragment
import au.com.signonsitenew.ui.documents.briefingslist.BriefingDateListFragment

class BriefingListViewPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment = when (position) {
            0 ->  BriefingDateListFragment.newInstance()
            else -> BriefingContentListFragment.newInstance()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Show by Date"
            1 -> return "Show by Message"
        }
        return null
    }
}