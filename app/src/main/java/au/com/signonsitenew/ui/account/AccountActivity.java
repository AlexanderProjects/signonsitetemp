package au.com.signonsitenew.ui.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.RealmConfiguration;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Activity that handles actions relating to changing a user's account details
 */

public class AccountActivity extends AppCompatActivity implements
        AccountFragment.OnNameCellClickListener,
        AccountFragment.OnNumberCellClickListener,
        AccountFragment.OnPasswordCellClickListener,
        AccountFragment.OnEmployerCellClickListener {

    private static final String LOG = AccountActivity.class.getSimpleName();

    protected TextView mTitleTextView;
    protected FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mTitleTextView = (TextView)toolbar.findViewById(R.id.toolbar_title_text_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mFragmentManager = getSupportFragmentManager();

        loadAccountFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.account_container);
                if (fragment instanceof AccountFragment) {
                    // If at the main page, finish the activity
                    finish();
                }
                else if (fragment instanceof AccountNameFragment) {
                    if (((AccountNameFragment)fragment).nameDetailsChanged()) {
                        displayDetailsChangedDialog();
                    }
                    else {
                        getSupportFragmentManager().popBackStack();
                    }
                }
                else if (fragment instanceof AccountPasswordFragment) {
                    if (((AccountPasswordFragment)fragment).passwordDetailsEntered()) {
                        displayDetailsChangedDialog();
                    }
                    else {
                        getSupportFragmentManager().popBackStack();
                    }
                }
                else if (fragment instanceof AccountPhoneNumberFragment) {
                    if (((AccountPhoneNumberFragment)fragment).phoneNumberDetailsEntered()) {
                        displayDetailsChangedDialog();
                    }
                    else {
                        getSupportFragmentManager().popBackStack();
                    }
                }
                else if (fragment instanceof AccountEmployerFragment) {
                    if (((AccountEmployerFragment)fragment).employerDetailsChanged()) {
                        displayDetailsChangedDialog();
                    }
                    else {
                        getSupportFragmentManager().popBackStack();
                    }
                }
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.account_container);
        if (fragment instanceof AccountFragment) {
            // If at the main page, finish the activity
            finish();
        }
        else if (fragment instanceof AccountNameFragment) {
            if (((AccountNameFragment)fragment).nameDetailsChanged()) {
                displayDetailsChangedDialog();
            }
            else {
                super.onBackPressed();
            }
        }
        else if (fragment instanceof AccountPasswordFragment) {
            if (((AccountPasswordFragment)fragment).passwordDetailsEntered()) {
                displayDetailsChangedDialog();
            }
            else {
                super.onBackPressed();
            }
        }
        else if (fragment instanceof AccountPhoneNumberFragment) {
            if (((AccountPhoneNumberFragment)fragment).phoneNumberDetailsEntered()) {
                displayDetailsChangedDialog();
            }
            else {
                super.onBackPressed();
            }
        }
        else if (fragment instanceof AccountEmployerFragment) {
            if (((AccountEmployerFragment)fragment).employerDetailsChanged()) {
                displayDetailsChangedDialog();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    private void loadAccountFragment() {
        // Load initial Fragment
        Fragment fragment = new AccountFragment();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.account_container, fragment);
        transaction.commit();
    }

    private void navigateToFragment(Fragment fragment) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,
                                        R.anim.exit_to_left,
                                        R.anim.enter_from_left,
                                        R.anim.exit_to_right);
        transaction.replace(R.id.account_container, fragment);

        // Add the transaction to the back stack to preserve back navigation
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void createAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);
        builder.setTitle(getString(R.string.error_generic_title))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null);
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.orange_primary));
            }
        });

        dialog.show();
    }

    protected void displayDetailsChangedDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("You have not saved your changes! \n\nPlease click on the save " +
                        "button below to keep your changes.")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        // Continue with back
                        getSupportFragmentManager().popBackStack();
                    }
                })
                .setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     ***********************************************************************************************
     * Fragment Callback functions
     ***********************************************************************************************
     */

    @Override
    public void onNameCellClick() {
        // Load name Fragment
        navigateToFragment(new AccountNameFragment());
    }

    @Override
    public void onNumberCellClick() {
        navigateToFragment(new AccountPhoneNumberFragment());
    }

    @Override
    public void onPasswordCellClick() {
        navigateToFragment(new AccountPasswordFragment());
    }

    @Override
    public void onEmployerCellClick() {
        navigateToFragment(new AccountEmployerFragment());
    }
}
