package au.com.signonsitenew.ui.attendanceregister.workernotes

import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import au.com.signonsitenew.domain.usecases.workernotes.WorkerNotesUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import javax.inject.Inject

interface WorkerNotesFragmentPresenter {
    fun mapWorkerNotesListToListOfAny(workerNotesList: List<WorkerNotes>): List<Any>
    fun formatDateTimeForWorkerList(workerNotesList: List<WorkerNotes>):List<WorkerNotes>
}

class WorkerNotesFragmentPresenterImpl @Inject constructor(private val workerNotesUseCaseImpl: WorkerNotesUseCaseImpl):BasePresenter(), WorkerNotesFragmentPresenter {
    override fun mapWorkerNotesListToListOfAny(workerNotesList: List<WorkerNotes>): List<Any> = workerNotesUseCaseImpl.mapWorkerNotesListToListOfAny(workerNotesList)
    override fun formatDateTimeForWorkerList(workerNotesList: List<WorkerNotes>):List<WorkerNotes>  = workerNotesUseCaseImpl.formatDateTimeForWorkerList(workerNotesList)
}