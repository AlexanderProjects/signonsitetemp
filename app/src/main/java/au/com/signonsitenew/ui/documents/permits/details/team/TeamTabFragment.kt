package au.com.signonsitenew.ui.documents.permits.details.team

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentTeamBinding
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.ui.documents.permits.details.team.adapters.TeamMembersListAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class TeamTabFragment : DaggerFragment(), TeamFragmentDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router

    private var _binding: FragmentTeamBinding? = null
    private val binding get() = _binding!!
    private var permit: Permit? = null
    private var template: SitePermitTemplate? = null
    private var permitInfo: PermitInfo? = null
    private lateinit var adapter :TeamMembersListAdapter
    private val presenter:TeamFragmentPresenterImpl by viewModels { viewModelFactory  }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            permit = it.getParcelable(Constants.PERMIT_OBJECT)
            template = it.getParcelable(Constants.PERMIT_TEMPLATE_OBJECT)
            permitInfo = it.getParcelable(Constants.FULL_PERMIT_OBJECT)
        }
        presenter.inject(this)
        presenter.observeStates()
        presenter.observeSelectedMembers()
        presenter.sendClearMembersList()
        presenter.observeForCtaState()
        presenter.clearUnSelectedListOfMembers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        _binding = FragmentTeamBinding.inflate(inflater, container, false)
        permitInfo?.let {
            presenter.clearSelectedListOfMembers()
            presenter.setPermitInfoState(it)
            presenter.setLocalPermitInfo(it)
        }
        template?.let {
            presenter.clearSelectedListOfMembers()
            presenter.getUserDetails()
        }
        binding.addNewTeamMemberFab.setOnClickListener {
            if(presenter.retrieveUnSelectedMemberList().isNotEmpty()) { presenter.sendUnSelectedUsers(presenter.retrieveUnSelectedMemberList()) }
            presenter.navigateToAddTeamMembers()
        }

        binding.permitTeamRequestorValue.setOnClickListener {
            AlertDialogMessageHelper.permitRequesterAlertDialog(requireContext(), resources.getStringArray(R.array.call_dismiss_option),
                { router.navigateToCallUser(requireActivity(), presenter.retrieveLocalPermitInfo().requester_user.phone_number!!)},
                { it.dismiss() })
        }
        return binding.root
    }

    override fun setRequestorCompany(requesterUser: String, isReadOnly: Boolean) {
        when(isReadOnly){
            true -> {
                binding.permitTeamRequestorCompanyValue.text = requesterUser
                binding.permitTeamRequestorCompanyValue.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.darker_gray))
            }
            false -> binding.permitTeamRequestorCompanyValue.text = requesterUser
        }
    }

    override fun setRequestor(requesterUser: String, isReadOnly: Boolean) {
        when(isReadOnly){
            true -> {
                binding.permitTeamRequestorValue.text = requesterUser
                binding.permitTeamRequestorValue.setTextColor(ContextCompat.getColor(requireContext(), android.R.color.darker_gray))
            }
            false -> binding.permitTeamRequestorValue.text = requesterUser
        }
    }

    override fun setAddTeamMembersButtonVisibility(isReadOnly: Boolean) {
        if (isReadOnly)
            binding.addNewTeamMemberFab.show()
        else
            binding.addNewTeamMemberFab.hide()
    }

    override fun showSelectedMembersList(memberList: List<RequesteeUser>) {
        presenter.saveSelectedMemberList(memberList)
        presenter.removeListFromUnSelectedUsers(memberList)
        if(memberList.isEmpty()) presenter.clearSelectedListOfMembers()
        setAdapter()
    }

    private fun setAdapter(){
        if(!::adapter.isInitialized){
            adapter = TeamMembersListAdapter(presenter.retrieveSelectedMemberList(), presenter) { unSelectedListOfMembers, requesteeUser ->
                AlertDialogMessageHelper.permitAction(requireContext(), presenter.setUserClickMenu(requesteeUser, presenter.retrieveLocalPermitInfo()),
                    {
                        presenter.setUserStatusToPreRequest(requesteeUser.id.toString())
                    },
                    {
                        router.navigateToCallUser(requireActivity(),requesteeUser.phone_number.toString())
                    },
                    {
                        presenter.removeSelectedUser(requesteeUser)
                        presenter.saveUnSelectedMemberList(unSelectedListOfMembers)
                        presenter.reOrganiseSelectedMemberList(presenter.retrieveSelectedMemberList(), requesteeUser)
                        presenter.setFabButtonVisibilityState(true)
                        presenter.removeUserFromSelectedListTeam()
                    },
                    { it.dismiss() })
            }
            binding.addedMembersList.adapter = adapter
        }else {
            adapter.listOfMembers = presenter.retrieveSelectedMemberList()
            adapter.notifyDataSetChanged()
        }
    }

    override fun showErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) {
            template?.let { presenter.getUserDetails() }
        }
    }

    override fun showPermitError() {
        AlertDialogMessageHelper.permitErrorMessage(requireContext()) { presenter.setUserStatusToPreRequest(permit?.id.toString()) }
    }

    override fun navigationToAddTeamMembers() {
        router.navigateToAddTeamMembers(requireActivity())
    }

    override fun saveUnSelectedMembers(memberList: List<RequesteeUser>) {
        presenter.saveUnSelectedMemberList(memberList)
    }

    override fun hideTeamMemberPermitRequestMessage() {
        binding.permitTeamAddMembersMessage.visibility = View.INVISIBLE
    }

    override fun showTeamMemberPermitRequestMessage() {
        binding.permitTeamAddMembersMessage.visibility = View.VISIBLE
    }

}