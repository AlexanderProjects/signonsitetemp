package au.com.signonsitenew.ui.main;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.TagConstraint;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.adapters.SignedOnSectionsPagerAdapter;
import au.com.signonsitenew.api.IsEmergency;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.di.factory.ViewModelFactory;
import au.com.signonsitenew.domain.models.SignOnOffType;
import au.com.signonsitenew.domain.models.WorkerBriefing;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase;
import au.com.signonsitenew.events.EmergencyEvent;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.models.BadgeNotification;
import au.com.signonsitenew.models.Document;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.ui.documents.briefings.BriefingsActivity;
import au.com.signonsitenew.ui.documents.inductions.InductionActivity;
import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragment;
import au.com.signonsitenew.ui.evacuation.EmergencyActivity;
import au.com.signonsitenew.ui.main.documents.DocumentsFragment;
import au.com.signonsitenew.ui.navigation.Router;
import au.com.signonsitenew.ui.passport.PassportFragment;
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoFragment;
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuListener;
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuToEmergencyListener;
import au.com.signonsitenew.ui.passport.personal.PersonalFragment;
import au.com.signonsitenew.utilities.AlertDialogMessageHelper;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.PermissionCheckers;
import au.com.signonsitenew.utilities.SLog;
import dagger.android.support.DaggerAppCompatActivity;
import io.realm.Realm;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * This section of the app contains logic related to all tab-contained fragments to do with when
 * the user is signed onto a site. Tabs are controlled by the SignedOnSectionsPagerAdapter.
 *
 * The SignedOnSectionsPagerAdapter that will provide
 * fragments for each of the sections. We use a
 * {@link androidx.fragment.app.FragmentPagerAdapter} derivative, which will keep every
 * loaded fragment in memory. If this becomes too memory intensive, it
 * may be best to switch to a
 * {@link androidx.fragment.app.FragmentStatePagerAdapter}.
 */

public class SignedOnActivity extends DaggerAppCompatActivity implements
        SignedOnFragment.OnButtonInteractionListener,
        DocumentsFragment.OnListFragmentInteractionListener,
        SignedOnActivityNavigationListener, SignOnActivityView{

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    AnalyticsEventDelegateService analyticsEventDelegateService;

    @Inject
    LocationManager locationManager;

    @Inject
    NotificationUseCase notificationUseCase;

    @Inject
    Router router;


    private static final String LOG = SignedOnActivity.class.getSimpleName();

    private TextView mTitleTextView;
    private SignedOnSectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;
    protected ViewPager mViewPager;
    protected int mTabPosition;
    private SOSAPI mAPI;
    private JobManager mJobManager;
    private Realm mRealm;
    private BadgeDrawable passportBadge;
    private BadgeDrawable documentsBadge;
    private SignOnActivityPresenterImpl presenter;
    protected LinearLayout mSignedOnLayout;
    protected LinearLayout mProgressLayout;
    protected TextView mProgressDescriptionTextView;
    protected ImageView mProgressSpinner;
    private static MainTabMenuListener mainTabMenuListener;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Bundle extras;
    private static MainTabMenuToEmergencyListener mainTabMenuToEmergencyListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signed_on);

        PersonalFragment.setSignedOnActivityNavigationListener(this);
        EmergencyInfoFragment.setSignedOnActivityNavigationListener(this);
        presenter = new ViewModelProvider(this,viewModelFactory).get(SignOnActivityPresenterImpl.class);
        presenter.inject(this);
        presenter.registerNotificationListener();
        presenter.registerFirebaseIdToken();
        extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.getBoolean(Constants.HAS_USER_TAPPED_ON_NOTIFICATION)){
                if(extras.getBoolean(Constants.IS_LOCAL_NOTIFICATION)) {
                    if(extras.getBoolean(Constants.BRIEFING_NOTIFICATION))
                        analyticsEventDelegateService.notificationClicked(Constants.NEW_ACTIVE_BRIEFING, Constants.ANALYTICS_LOCAL_NOTIFICATION,new HashMap<>(),null);
                    if(extras.getBoolean(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION))
                        analyticsEventDelegateService.notificationClicked(Constants.AUTO_SIGN_ON,Constants.ANALYTICS_LOCAL_NOTIFICATION,new HashMap(),null);
                }
                if(extras.getBoolean(Constants.IS_REMOTE_NOTIFICATION)) {
                    if (extras.getBoolean(Constants.BRIEFING_NOTIFICATION)){
                        HashMap metadata = (HashMap) extras.get(Constants.HAS_METADATA);
                        analyticsEventDelegateService.notificationClicked(Constants.NEW_ACTIVE_BRIEFING,Objects.requireNonNull(extras.getString(Constants.BRIEFING_NOTIFICATION_TYPE)), metadata, extras.getString(Constants.NOTIFICATION_SENT_AT));
                    }
                    if (!extras.getBoolean(Constants.INDUCTION_SUBMITTED)) {
                        Intent intent = new Intent(this, InductionActivity.class);
                        startActivity(intent);
                        extras.putBoolean(Constants.INDUCTION_SUBMITTED, false);
                    }
                    if(Objects.equals(extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY), Constants.FCM_EVACUATION_ENDED)){
                        analyticsEventDelegateService.notificationClicked(Constants.EVACUATION_ENDED,Constants.ANALYTICS_REMOTE_NOTIFICATION,new HashMap(),null);
                        notificationUseCase.cancelNotifications(Constants.NOTIFICATION_END_EVACUATION_ID);
                    }
                    if(Objects.equals(extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY), Constants.FCM_EVACUATION_STARTED)) {
                        analyticsEventDelegateService.notificationClicked(Constants.EVACUATION_STARTED, Constants.ANALYTICS_REMOTE_NOTIFICATION, new HashMap(),null);
                    }
                    if(Objects.equals(extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY), Constants.FCM_INDUCTION_REJECTED)){
                        analyticsEventDelegateService.notificationClicked(Constants.FCM_INDUCTION_REJECTED, Constants.ANALYTICS_REMOTE_NOTIFICATION, new HashMap(),null);
                    }
                }
            }

        }

        mSignedOnLayout = findViewById(R.id.appbar);
        mProgressLayout = findViewById(R.id.progress_linear_layout);
        mProgressDescriptionTextView = findViewById(R.id.progress_action_text_view);
        mProgressSpinner = findViewById(R.id.progress_spinner_image_view);
        mProgressLayout.setVisibility(View.GONE);
        mViewPager = findViewById(R.id.container);
        mTabLayout = findViewById(R.id.tabs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        mTitleTextView = toolbar.findViewById(R.id.toolbar_title_text_view);
        mTitleTextView.setText("Site");
        setSupportActionBar(toolbar);
        showSigningOnProgressView();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        mAPI = new SOSAPI(this);
        mJobManager = SOSApplication.getInstance().getJobManager();
        mRealm = Realm.getDefaultInstance();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(Constants.GCM_SENT_TOKEN_TO_SERVER, false);

                SLog.i(LOG, "GCM sentToken status is: " + sentToken);
            }
        };

        PermissionCheckers.checkDoNotDisturbPermission(this);
        PermissionCheckers.checkBatteryOptimisationSettings(this);

        presenter.fragmentPassportSelector(fragment -> {
            configureTabs(fragment);
            return null;
        },()->{
            configureTabs(new PassportFragment());
            return null;
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.GCM_REGISTRATION_COMPLETE));
        presenter.checkForSignedStatus();

        // Remove signon/off notifications if they are displayed
        NotificationUtil.cancelNotifications(this, NotificationUtil.NOTIFICATION_ATTENDANCE_ID);

        // Check emergency status of site
        Log.i(LOG, "about to call is_emergency");
        checkEmergencyState();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

        // Cancel all pending background jobs and Volley requests
        mJobManager.cancelJobsInBackground(null, TagConstraint.ANY, LOG);
        SOSApplication.getInstance().cancelPendingRequests(LOG);
    }

    @Override
    protected void onDestroy() {
        if (mRealm != null) {
            mRealm.close();
        }
        super.onDestroy();
    }


    private void showSigningOnProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mSignedOnLayout.setVisibility(View.GONE);
        mProgressDescriptionTextView.setText("loading...");
        mProgressLayout.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.GONE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }


    protected void showSigningOffProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mSignedOnLayout.setVisibility(View.GONE);
        mProgressDescriptionTextView.setText("Signing you off...");
        mProgressLayout.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.GONE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    @SuppressLint("SetTextI18n")
    protected void showVisitorRegisterProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mSignedOnLayout.setVisibility(View.GONE);
        mProgressDescriptionTextView.setText("Retrieving site visitors...");
        mProgressLayout.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    @SuppressLint("SetTextI18n")
    public void showLoggingOutProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mSignedOnLayout.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.GONE);
        mProgressDescriptionTextView.setText("Logging you out...");
        mProgressLayout.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    public void showSignedOnView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
        mSignedOnLayout.setVisibility(View.VISIBLE);
        mProgressLayout.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.VISIBLE);
    }

    public static void setMainTabMenuListener(MainTabMenuListener listener){
        mainTabMenuListener = listener;
    }

    public static void setMenuToEmergencyListener(MainTabMenuToEmergencyListener listener){
        mainTabMenuToEmergencyListener = listener;
    }

    private void configureTabs(Fragment fragment) {
        if(mSectionsPagerAdapter == null) {
            mSectionsPagerAdapter = new SignedOnSectionsPagerAdapter(getSupportFragmentManager(), fragment);
        }else {
            mSectionsPagerAdapter.notifyDataSetChanged();
        }

        // Check if Site Manager
        final boolean siteManager = mSectionsPagerAdapter.getCount() == 5;

        // Set up the ViewPager with the sections adapter.
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
            @Override
            public void onPageSelected(int position) {
                Log.i(LOG, "onPageSelected, position: " + position);
                mTabPosition = position;
                if(mainTabMenuListener != null)
                    mainTabMenuListener.mainMenuTabSelected(position);
                if(mainTabMenuToEmergencyListener != null)
                    mainTabMenuToEmergencyListener.mainMenuTabSelected(position);

                // Set title based on fragment
                switch (position) {
                    // Site tab
                    case 0:
                        mTitleTextView.setText("Site");
                        break;
                    // Site Docs tab
                    case 1:
                        mTitleTextView.setText("Site Documents");
                        break;
                    // Licences
                    case 2:
                        mTitleTextView.setText("Passport");
                        break;
                    // Menu
                    case 3:
                        if (siteManager) {
                            mTitleTextView.setText("Manager");
                        }
                        else {
                            mTitleTextView.setText("Menu");
                        }
                        break;
                    case 4:
                        mTitleTextView.setText("Menu");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });

        mTabLayout.setupWithViewPager(mViewPager);
        passportBadge = mTabLayout.getTabAt(2).getOrCreateBadge();
        passportBadge.setVisible(false);
        documentsBadge = mTabLayout.getTabAt(1).getOrCreateBadge();
        //removes initial dot
        documentsBadge.setVisible(false);

        // Set up initial tabs
        int selectedTabColour = ContextCompat.getColor(SignedOnActivity.this, R.color.orange_primary);
        int unselectedTabColour = ContextCompat.getColor(SignedOnActivity.this, R.color.white);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            switch (i) {
                case 0:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_location);

                    // Set initial selected colour
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
                case 1:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_docs);
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
                case 2:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_licences);
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
                case 3:
                    if (siteManager) {
                        mTabLayout.getTabAt(i).setIcon(R.drawable.icon_managers_panel);
                        mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    }
                    else {
                        mTabLayout.getTabAt(i).setIcon(R.drawable.icon_menu);
                        mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    }
                    break;
                case 4:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_menu);
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
            }
        }

        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab != null) {
                    super.onTabSelected(tab);
                    int selectedTabColour = ContextCompat.getColor(SignedOnActivity.this, R.color.orange_primary);
                    if(tab.getIcon() != null)
                        tab.getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int selectedTabColour = ContextCompat.getColor(SignedOnActivity.this, R.color.white);
                if(tab.getIcon() != null)
                    tab.getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
        showSignedOnView();

    }

    @Subscribe
    public void onStatusEvent(StatusEvent event) {
        Log.i(LOG, "Status Event received. " + event.toString());
        if (event.eventType.equals(Constants.EVENT_SIGN_OFF) && !isFinishing()) {
            // Remove induction notification
            notificationUseCase.cancelNotifications(NotificationUtil.NOTIFICATION_INDUCTION_ID);
            notificationUseCase.cancelNotifications(NotificationUtil.NOTIFICATION_BRIEFING_ID);
            notificationUseCase.cancelNotifications(Constants.NOTIFICATION_END_EVACUATION_ID);
            navigateToMainActivity();
        }
    }

    @Subscribe
    public void onEmergencyEvent(EmergencyEvent event) {
        Log.i(LOG, "Emergency Event received. " + event.toString());
        if (event.isEmergency && !isFinishing()) {
            checkEmergencyState();
        }
    }

    /**
     ***********************************************************************************************
     * Fragment Callback functions
     ***********************************************************************************************
     */

    @Override
    public void onSignOffPressed(Integer siteId) {
        Log.i("where sign off", "onSignOffpressed activated");
        analyticsEventDelegateService.signedOff(SignOnOffType.Manual);
        showSigningOffProgressView();
        signOffSite(siteId);
    }

    @Override
    public void onDocumentSelected(Document document, WorkerBriefing briefing) {
        String type = document.getDocType();
        if (type.equals(Constants.DOC_BRIEFING)) {
            analyticsEventDelegateService.siteBriefingOpened();
            // Navigate to Briefing Activity
            Intent intent = new Intent(this, BriefingsActivity.class);
            intent.putExtra(Constants.ACTIVE_BRIEFING_KEY,briefing);
            startActivity(intent);
        }
        else if (type.equals(Constants.DOC_INDUCTION)) {
            if (document.getState().equals(Constants.DOC_INDUCTION_INCOMPLETE) || document.getState().equals(Constants.DOC_INDUCTION_REJECTED)) {
                // Navigate to InductionActivity
                Intent intent = new Intent(this, InductionActivity.class);
                startActivity(intent);
            }
            else if (document.getState().equals(Constants.DOC_INDUCTION_PENDING)) {
                DarkToast.makeText(this, "Your induction is currently being reviewed by a site staff member.");
            }
            else if (document.getState().equals(Constants.DOC_INDUCTION_COMPLETE)) {
                DarkToast.makeText(this, "Your induction has been completed");
            }
        }else if(type.equals(Constants.DOC_PERMITS)){
            router.navigateToPermitActivity(this);
        }
    }

    @Override
    public void onClickSeeAllButton(Document document) {
        String type = document.getDocType();
        if (type.equals(Constants.DOC_BRIEFING)) {
            // Navigate to Briefing Activity
            Intent intent = new Intent(this, BriefingsActivity.class);
            intent.putExtra(Constants.DOC_BRIEFING_LIST, true);
            startActivity(intent);
        }
    }

    @Override
    public void onClickNewButton(Document document) { }

    /**
     ***********************************************************************************************
     * API Calls
     ***********************************************************************************************
     */
    protected void signOffSite(@Nullable final Integer siteId) {
        SLog.i("this is where sgn off initiated", "its signonactivity.java");
        mAPI.signOffSite(false, siteId, null, response -> {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                if (jsonResponse.getBoolean(Constants.JSON_SIGNED_OFF)) {
                    // Clear any briefings if set
                    NotificationUtil.cancelNotifications(SignedOnActivity.this, NotificationUtil.NOTIFICATION_ATTENDANCE_ID);
                    NotificationUtil.cancelNotifications(SignedOnActivity.this, NotificationUtil.NOTIFICATION_BRIEFING_ID);

                    // Load signed on fragment
                    Intent intent = new Intent(SignedOnActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else {
                    showSignedOnView();
                    DarkToast.makeText(SignedOnActivity.this, "There was a problem signing you off, " +
                            "please make sure you have internet connectivity and try again.");
                }
            }
            catch (JSONException e) {
                SLog.d(LOG, "JSON Exception occurred: " + e.getMessage());
                showSignedOnView();
            }
        }, () -> {
            showSignedOnView();
            DarkToast.makeText(SignedOnActivity.this, "There was a problem signing you off, " +
                    "please make sure you have internet connectivity and try again.");
        });
    }

    private void checkEmergencyState() {
        IsEmergency.post(this,locationManager, response -> {
            Log.i(LOG, "response received from is_emergency");
            try {
                if (response.getBoolean("is_emergency")) {
                    // Site is currently in emergency - navigate to appropriate view
                    User user = mRealm.where(User.class).findFirst();
                    Long userId = 0L;
                    if (user != null) {
                        userId = user.getId();
                    }

                    if (response.getInt(Constants.BUNDLE_EMERGENCY_INITIATOR) == userId) {
                        // Current user is the initiator, take them to the log screen
                        Intent intent = new Intent(SignedOnActivity.this, EmergencyActivity.class);
                        intent.putExtra(Constants.BUNDLE_EMERGENCY_INITIATOR, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(SignedOnActivity.this, EmergencyActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
            catch (JSONException e) {
                SLog.e(LOG, "JSON Exception occurred. " + e.getMessage());
            }
        }, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (Fragment f : fragments) {
                if (f instanceof PassportFragment) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    @Override
    public void navigateToTab(Integer index) {
        mTabLayout.getTabAt(index).select();
    }


    @Override
    public void updateNotificationValueForPassport(BadgeNotification badgeNotification) {
        if(badgeNotification.getNotifier().equalsIgnoreCase(Constants.PASSPORT_SIGNED_MAIN_TAB_NOTIFICATION)){
            if(passportBadge != null) {
                if (badgeNotification.getBadgeNumber() > 0) {
                    passportBadge.setVisible(true);
                    passportBadge.setNumber(badgeNotification.getBadgeNumber());
                } else {
                    passportBadge.clearNumber();
                    passportBadge.setVisible(false);
                }
            }
        }
        if(badgeNotification.getNotifier().equalsIgnoreCase(Constants.DOCUMENTS_SIGNED_MAIN_TAB_NOTIFICATION)){
            if(documentsBadge != null){
                if(badgeNotification.getBadgeNumber() > 0){
                    documentsBadge.setVisible(true);
                    documentsBadge.setNumber(badgeNotification.getBadgeNumber());
                } else {
                    documentsBadge.clearNumber();
                    documentsBadge.setVisible(false);
                }
            }
        }
    }


    @Override
    public void showProgressView(String text) {
        mSignedOnLayout.setVisibility(View.GONE);
        mProgressDescriptionTextView.setText(text);
        mProgressLayout.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    @Override
    public void showInternetError() {
        AlertDialogMessageHelper.networkErrorMessage(this,() -> {
            presenter.checkForSignedStatus();
            return null;
        });
    }

    @Override
    public void showDataError(@NotNull String message) {
        AlertDialogMessageHelper.dataErrorMessage(this,message);
    }

    @Override
    public void isThereADocActive(boolean isThereAActiveDoc) {
        if(isThereAActiveDoc)
            mViewPager.postDelayed(() -> mViewPager.setCurrentItem(1),500);
    }

    @Override
    public void onBriefingNotificationSend(@NotNull WorkerBriefing briefing) {
        if (extras != null)
            if(extras.getBoolean(Constants.HAS_UNACKNOWLEDGED_BRIEFING_DOC) && extras.getBoolean(Constants.BRIEFING_NOTIFICATION)) {
                Intent intent = new Intent(this, BriefingsActivity.class);
                intent.putExtra(Constants.ACTIVE_BRIEFING_KEY, briefing);
                startActivity(intent);
                extras.putBoolean(Constants.HAS_UNACKNOWLEDGED_BRIEFING_DOC,false);
            }

    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.doc_permits_container);
        if(fragment instanceof CurrentPermitsFragment ){
            router.navigateToDocumentFragment(this);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void navigateToMainActivity() {
       Intent intent = new Intent(this, MainActivity.class);
       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
       startActivity(intent);
    }
}
