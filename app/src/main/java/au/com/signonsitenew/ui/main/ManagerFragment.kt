package au.com.signonsitenew.ui.main


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import au.com.signonsitenew.R
import au.com.signonsitenew.api.TodaysVisits
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.realm.EnrolledSite
import au.com.signonsitenew.realm.services.*
import au.com.signonsitenew.ui.attendanceregister.AttendanceActivity
import au.com.signonsitenew.ui.evacuation.EmergencyActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SLog
import dagger.android.support.DaggerFragment
import io.realm.Realm
import org.json.JSONArray
import org.json.JSONException
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * A Fragment class that displays management functionality. Displays the currently signed on site,
 * and allows managers to perform evacuations, view the site attendance list, and the site induction list.
 *
 * Will add remote management of sites in an upcoming release.
 */
class ManagerFragment : DaggerFragment() {

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService

    private var mActivityContainerView: LinearLayout? = null
    private var mProgressView: LinearLayout? = null
    private lateinit var mSiteNameTextView: TextView
    private lateinit var mAddressTextView: TextView
    private lateinit var mSiteEmergencyButton: Button
    private lateinit var mManageVisitorsButton: Button
    private lateinit var mRealm: Realm
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_manager, container, false)

        mActivityContainerView = requireActivity().findViewById(R.id.appbar)
        mProgressView = requireActivity().findViewById(R.id.progress_linear_layout)
        mSiteNameTextView = rootView.findViewById(R.id.site_name_text_view)
        mAddressTextView = rootView.findViewById(R.id.site_owner_text_view)
        mSiteEmergencyButton = rootView.findViewById(R.id.managers_site_emergency_button)
        mManageVisitorsButton = rootView.findViewById(R.id.manage_visitors_button)

        mRealm = Realm.getDefaultInstance()
        val abilitiesService = UserAbilitiesService(mRealm)
        val userService = UserService(mRealm)
        val siteSettingsService = SiteSettingsService(mRealm)

        val userId: Long = userService.currentUserId
        val siteId: Long = siteSettingsService.siteId

        val site = mRealm.where(EnrolledSite::class.java).equalTo("id", siteId).findFirst()
        if (site != null) {
            mSiteNameTextView.text = site.name
            mAddressTextView.text = site.managerName
        }

        // Set appropriate function buttons
        if (abilitiesService.canAccessEvacuations(userId, siteId)) {
            mSiteEmergencyButton.visibility = View.VISIBLE
            mSiteEmergencyButton.setOnClickListener {
                val intent = Intent(activity, EmergencyActivity::class.java)
                startActivity(intent)
            }
        }

        if (abilitiesService.canAccessVisitorRegister(userId, siteId)) {
            mManageVisitorsButton.visibility = View.VISIBLE
            mManageVisitorsButton.setOnClickListener {
                fetchAndStoreTodaysVisits()
                analyticsEventDelegateService.managerAttendanceRegisterViewed()
            }
        }

        return rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mRealm.close()
    }


    private fun fetchAndStoreTodaysVisits() {
        // Display loading view
        (activity as SignedOnActivity).showVisitorRegisterProgressView()

        // Fetch Visitor data
        TodaysVisits.get(activity,
                { response ->
                    val attendancesArray: JSONArray
                    val attendeeArray: JSONArray
                    try {
                        if (response.getString(Constants.JSON_STATUS) == Constants.JSON_SUCCESS) {
                            // Today's Attendances successfully retrieved
                            attendancesArray = response.getJSONArray(Constants.JSON_VISITS)
                            attendeeArray = response.getJSONArray(Constants.JSON_VISITORS)

                            storeTodaysAttendances(attendeeArray, attendancesArray)

                            // Start Activity to display data
                            if (activity != null) {
                                val intent = Intent(activity, AttendanceActivity::class.java)
                                startActivity(intent)
                            }

                            // Cancel loading screen
                            fadeOutProgressView()
                        } else {
                            // Cancel loading screen
                            fadeOutProgressView()
                        }
                    } catch (e: JSONException) {
                        SLog.e(TAG, "A JSON Exception occurred: " + e.message)

                        // Cancel loading screen
                        fadeOutProgressView()
                    }
                }
        ) {
            // Cancel loading screen
            fadeOutProgressView()
        }
    }

    private fun storeTodaysAttendances(attendeeArray: JSONArray, attendancesArray: JSONArray) {
        val siteSettingsService = SiteSettingsService(mRealm)
        val inductionService = SiteInductionService(mRealm)
        val siteAttendeeService = SiteAttendeeService(mRealm)
        val attendanceRecordService = AttendanceRecordService(mRealm)

        val siteId = siteSettingsService.siteId!!

        // Store Visitors
        siteAttendeeService.deleteAllSiteAttendees()
        attendanceRecordService.deleteAllAttendanceRecords()
        inductionService.deleteInductionsForSite(siteId)

        for (i in 0 until attendeeArray.length()) {
            val person = attendeeArray.getJSONObject(i)
            siteAttendeeService.createOrUpdateSiteAttendee(person)
        }

        // TODO: Check if repeatedly opening transactions is bad
        // Store AttendanceRecord Information
        for (i in 0 until attendancesArray.length()) {
            val attendance = attendancesArray.getJSONObject(i)
            attendanceRecordService.createOrUpdateAttendanceRecord(attendance)
        }
    }

    private fun fadeOutProgressView() {
        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.interpolator = AccelerateInterpolator()
        fadeOut.duration = 1000

        fadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                if (mProgressView != null) {
                    mProgressView!!.visibility = View.GONE
                }

                val actionBar = (activity as SignedOnActivity).supportActionBar
                actionBar?.show()

                // Display the views
                if (mActivityContainerView != null) {
                    mActivityContainerView!!.visibility = View.VISIBLE
                }
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })

        if (mProgressView != null) {
            mProgressView!!.startAnimation(fadeOut)
        }
    }

    companion object {
        const val TAG = "ManagerFragment"
    }

}
