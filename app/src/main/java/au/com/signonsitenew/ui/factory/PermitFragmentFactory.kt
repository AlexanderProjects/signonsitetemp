package au.com.signonsitenew.ui.factory

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import au.com.signonsitenew.ui.documents.permits.cta.CtaContextualButtonFragment

import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragment
import au.com.signonsitenew.ui.documents.permits.details.PermitDetailsFragment
import au.com.signonsitenew.ui.documents.permits.details.team.TeamTabFragment
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMembersFragment
import au.com.signonsitenew.utilities.ProgressViewFragment

class PermitFragmentFactory : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when(className){
            ProgressViewFragment::class.java.name -> ProgressViewFragment()
            CurrentPermitsFragment::class.java.name -> CurrentPermitsFragment()
            PermitDetailsFragment::class.java.name -> PermitDetailsFragment()
            TeamTabFragment::class.java.name -> TeamTabFragment()
            AddTeamMembersFragment::class.java.name -> AddTeamMembersFragment()
            CtaContextualButtonFragment::class.java.name -> CtaContextualButtonFragment()

            else -> super.instantiate(classLoader, className)
    }
}
