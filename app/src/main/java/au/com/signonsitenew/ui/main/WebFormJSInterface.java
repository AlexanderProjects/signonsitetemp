package au.com.signonsitenew.ui.main;

import android.content.Context;
import android.webkit.JavascriptInterface;

/**
 * Created by kcaldwell on 16/03/18.
 *
 * Interface to map our Forms JS to Android functions
 */

public class WebFormJSInterface {
    private Context mContext;

    /** Instantiate the interface and set the context */
    WebFormJSInterface(Context context) {
        mContext = context;
    }

    /**
     * Called when a manager-level user accepts/rejected an induction through the phone
     */
    @JavascriptInterface
    public void siteInductionSubmitted() {
        // Pop stack once to go back to the user view
        ((InductionViewerActivity)mContext).finish();
    }
}