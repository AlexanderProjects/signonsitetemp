package au.com.signonsitenew.ui.documents.permits.details.checks.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.ItemPermitContentTypeCheckboxBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypePlainTextBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypeTakePhotoBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypeTextInputBinding
import au.com.signonsitenew.domain.models.ComponentTypeForm
import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.domain.models.SaveButtonState
import au.com.signonsitenew.domain.models.state.PermitChecksState
import au.com.signonsitenew.domain.models.state.PermitContentType
import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.ui.documents.permits.details.adapters.PermitPhotoListAdapter
import au.com.signonsitenew.ui.documents.permits.details.checks.ChecksFragmentPresenter
import au.com.signonsitenew.utilities.ImageUtil
import au.com.signonsitenew.utilities.empty
import au.com.signonsitenew.utilities.notEmptyList
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable

class PermitChecksTabComponentsListAdapter(private val contentTypeItems:List<ContentTypeItem>,
                                           private val presenter: ChecksFragmentPresenter,
                                           private val logger:Logger,
                                           private val permitChecksState: PermitChecksState,
                                           private val rxBusSaveButtonState: RxBusSaveButtonState,
                                           val takePhotoOnClickListener:()->Unit):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var position:Int = 0
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(presenter.mapStringTypeToContentType(contentTypeItems[this.position])){
            is PermitContentType.CheckBox -> {
                val binding = ItemPermitContentTypeCheckboxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                PermitChecksCheckboxHolder(binding)
            }
            is PermitContentType.TextInput ->{
                val binding = ItemPermitContentTypeTextInputBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                PermitChecksTextInput(binding)
            }
            PermitContentType.Photo -> {
                val binding = ItemPermitContentTypeTakePhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                PermitChecksPhotoViewHolder(binding)
            }
            else -> {
                val binding = ItemPermitContentTypePlainTextBinding.inflate(LayoutInflater.from(parent.context),parent,false)
                PermitChecksPlainTextViewHolder(binding)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is PermitChecksPlainTextViewHolder ->{
                when(presenter.mapComponentState(permitChecksState)){
                    is PermitContentTypeState.ReadableAndEditable ->{
                        holder.binding.plainText.text = contentTypeItems[position].name
                        holder.binding.plainText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                    }
                    is PermitContentTypeState.Readable ->{
                        holder.binding.plainText.text = contentTypeItems[position].name
                        holder.binding.plainText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                    }
                    is PermitContentTypeState.GreyedOut ->{
                        holder.binding.plainText.text = contentTypeItems[position].name
                        holder.binding.plainText.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.text_grey_secondary))
                    }
                }
            }
            is PermitChecksCheckboxHolder ->{
                holder.binding.checkboxText.text = contentTypeItems[position].name
                holder.binding.permitContentTypeCheckbox.isChecked = if(contentTypeItems[position].responses.isNotEmpty()){
                    presenter.getLastContentTypeResponse(contentTypeItems[position].responses).value as Boolean
                } else
                    false
                when (contentTypeItems[position].is_mandatory) {
                    true -> holder.binding.requiredCheckboxText.visibility = View.VISIBLE
                    else -> holder.binding.requiredCheckboxText.visibility = View.GONE
                }

                when(presenter.mapComponentState(permitChecksState)){
                    is PermitContentTypeState.ReadableAndEditable ->{
                        holder.binding.checkboxText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.permitContentTypeCheckbox.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.permitContentTypeCheckbox.setOnClickListener {
                            if(holder.binding.permitContentTypeCheckbox.isChecked)
                                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                            presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, holder.binding.permitContentTypeCheckbox.isChecked))
                        }
                        holder.itemView.setOnClickListener {
                            presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, holder.binding.permitContentTypeCheckbox.isChecked))
                            holder.binding.permitContentTypeCheckbox.isChecked = !holder.binding.permitContentTypeCheckbox.isChecked
                            if(holder.binding.permitContentTypeCheckbox.isChecked)
                                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                        }
                    }
                    is PermitContentTypeState.Readable ->{
                        holder.binding.checkboxText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.permitContentTypeCheckbox.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.requiredCheckboxText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.permitContentTypeCheckbox.isEnabled = false

                    }
                    is PermitContentTypeState.GreyedOut ->{
                        holder.binding.checkboxText.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                        holder.binding.requiredCheckboxText.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                        holder.binding.permitContentTypeCheckbox.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                        holder.binding.permitContentTypeCheckbox.isEnabled = false

                    }
                }
            }
            is PermitChecksTextInput -> {
                holder.binding.inputText.text = contentTypeItems[position].name
                if(contentTypeItems[position].is_mandatory) holder.binding.requiredTextInput.visibility = View.VISIBLE else holder.binding.requiredTextInput.visibility = View.GONE
                val inputText = if(contentTypeItems[position].responses.isNotEmpty()) {
                    presenter.getLastContentTypeResponse(contentTypeItems[position].responses).value as String
                } else {
                    String().empty()
                }
                holder.binding.permitContentTypeTextInput.setText(inputText)
                when(presenter.mapComponentState(permitChecksState)){
                    is PermitContentTypeState.ReadableAndEditable ->{
                        holder.binding.inputText.setTextColor(ContextCompat.getColor(holder.itemView.context, android.R.color.black))
                        holder.binding.permitContentTypeTextInput.addTextChangedListener(onTextChanged = {text, _, _, _ ->
                            if(holder.binding.permitContentTypeTextInput.hasFocus()) {
                                presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, text.toString()))
                                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                            }
                        })
                    }
                    is PermitContentTypeState.Readable ->{
                        holder.binding.inputText.setTextColor(ContextCompat.getColor(holder.itemView.context, android.R.color.black))
                        holder.binding.permitContentTypeTextInput.isEnabled = false
                        holder.binding.requiredTextInput.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                    }
                    is PermitContentTypeState.GreyedOut ->{
                        holder.binding.inputText.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.text_grey_secondary))
                        holder.binding.permitContentTypeTextInput.isEnabled = false
                        holder.binding.requiredTextInput.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                    }
                }
            }
            is PermitChecksPhotoViewHolder ->{
                val photoList = mutableListOf<PermitContentTypeResponses>()
                val adapter = PermitPhotoListAdapter(photoList, presenter.getLocalUserFullName(),)
                holder.binding.photoOptionalText.text = contentTypeItems[position].name
                holder.binding.photoComponentList.adapter = adapter
                when(contentTypeItems[position].is_mandatory){
                    true -> holder.binding.requiredPhoto.visibility = View.VISIBLE
                    else -> holder.binding.requiredPhoto.visibility = View.GONE
                }
                contentTypeItems[position].responses.notEmptyList {
                    holder.binding.fileDescriptionConstraintLayout.visibility = View.VISIBLE
                    adapter.setNewPayLoad(contentTypeItems[position].responses)
                    adapter.isSavedImage = true
                    adapter.notifyDataSetChanged()
                }
                holder.binding.permitContentTypeTakePhotoButton.setOnClickListener{
                    takePhotoOnClickListener()
                    disposables.add(presenter.observeImageUri().firstElement()
                            .flatMap {
                                photoList.add(PermitContentTypeResponses(id = null, user = null, value = it,utc_date_time = null))
                                val file = ImageUtil.convertExistingUriToFile(holder.itemView.context,it)
                                val fileName = presenter.getPermitInfo().id.toString() + " " + it.lastPathSegment
                                return@flatMap presenter.uploadImages(file, fileName).toMaybe()
                            }
                            .subscribe ({
                                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                                presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, it.access_key))
                                holder.binding.fileDescriptionConstraintLayout.visibility = View.VISIBLE
                                holder.binding.requiredPhoto.visibility = View.GONE
                                adapter.setNewPayLoad(photoList)
                                adapter.isSavedImage = false
                                adapter.notifyDataSetChanged()
                            },{ logger.e(this@PermitChecksTabComponentsListAdapter::class.java.name, attributes = mapOf(this@PermitChecksTabComponentsListAdapter::class.java.name to it.message)) }))
                }
                when (presenter.mapComponentState(permitChecksState)) {
                    is PermitContentTypeState.ReadableAndEditable -> {
                        holder.binding.permitContentTypeTakePhotoButton.isEnabled = true
                    }
                    is PermitContentTypeState.Readable ->{
                        holder.binding.photoOptionalText.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.requiredPhoto.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.black))
                        holder.binding.permitContentTypeTakePhotoButton.isEnabled = false
                        holder.binding.fileDescriptionConstraintLayout.visibility = View.VISIBLE
                        adapter.setNewPayLoad(contentTypeItems[position].responses)
                        adapter.notifyDataSetChanged()
                    }
                    is PermitContentTypeState.GreyedOut ->{
                        holder.binding.photoOptionalText.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                        holder.binding.requiredPhoto.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                        holder.binding.permitContentTypeTakePhotoButton.isEnabled = false
                        holder.binding.permitContentTypeTakePhotoButton.setBackgroundColor(ContextCompat.getColor(holder.itemView.context,R.color.text_grey_secondary))
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        this.position = position
        return contentTypeItems.size
    }

    override fun getItemCount(): Int {
        return contentTypeItems.size
    }

    inner class PermitChecksPlainTextViewHolder(var binding: ItemPermitContentTypePlainTextBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitChecksCheckboxHolder(var binding: ItemPermitContentTypeCheckboxBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitChecksTextInput(var binding: ItemPermitContentTypeTextInputBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitChecksPhotoViewHolder(var binding: ItemPermitContentTypeTakePhotoBinding):RecyclerView.ViewHolder(binding.root)

    fun dispose(){
        disposables.dispose()
    }

}