package au.com.signonsitenew.ui.evacuation

import au.com.signonsitenew.domain.usecases.evacuation.EvacuationUseCase
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface EmergencyProgressActivityPresenter{
    fun stopEvacuation()
    fun inject(emergencyProgressActivityDisplay: EmergencyProgressActivityDisplay)
}

class EmergencyProgressActivityPresenterImpl @Inject constructor(private val evacuationUseCase: EvacuationUseCase,
                                                                 private val logger:Logger) : EmergencyProgressActivityPresenter, BasePresenter() {

    private lateinit var emergencyProgressActivityDisplay: EmergencyProgressActivityDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun stopEvacuation() {
        disposables.add(evacuationUseCase.stopEvacuation(evacuationUseCase.retrieveEvacuationVisitors())
                .subscribe({
                    if(it.status == Constants.JSON_SUCCESS) {
                        evacuationUseCase.removeSiteVisitors()
                        emergencyProgressActivityDisplay.navigationSignedActivity()
                    }else{
                        emergencyProgressActivityDisplay.showContentView()
                        logger.i(EmergencyProgressActivityPresenterImpl::class.java.name,attributes = mapOf(this::stopEvacuation.name to toJson(it)))
                    }
                },{
                    emergencyProgressActivityDisplay.showContentView()
                    logger.e(EmergencyProgressActivityPresenterImpl::class.java.name,it,attributes = mapOf(this::stopEvacuation.name to it.message))
                }))
    }

    override fun inject(emergencyProgressActivityDisplay: EmergencyProgressActivityDisplay) {
        this.emergencyProgressActivityDisplay = emergencyProgressActivityDisplay
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}