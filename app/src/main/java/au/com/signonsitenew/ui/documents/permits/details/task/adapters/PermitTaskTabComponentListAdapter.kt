package au.com.signonsitenew.ui.documents.permits.details.task.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.databinding.ItemPermitContentTypeCheckboxBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypePlainTextBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypeTakePhotoBinding
import au.com.signonsitenew.databinding.ItemPermitContentTypeTextInputBinding
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitContentType
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.ui.documents.permits.details.adapters.PermitPhotoListAdapter
import au.com.signonsitenew.ui.documents.permits.details.task.TaskFragmentPresenter
import au.com.signonsitenew.utilities.ImageUtil
import au.com.signonsitenew.utilities.emptyList
import au.com.signonsitenew.utilities.emptySpace
import au.com.signonsitenew.utilities.notEmptyList
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable

class PermitTaskTabComponentListAdapter(private val contentTypeItems:List<ContentTypeItem>,
                                        private val presenter:TaskFragmentPresenter,
                                        private val logger:Logger,
                                        private val rxBusSaveButtonState: RxBusSaveButtonState,
                                        val takePhotoOnClickListener:() -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var position:Int = 0
    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var recyclerView:RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(presenter.mapStringTypeToPermitContentType(contentTypeItems[position].type)){
            PermitContentType.CheckBox -> {
                val binding = ItemPermitContentTypeCheckboxBinding.inflate(LayoutInflater.from(parent.context),parent,false)
                PermitTabsContentTypeCheckBoxViewHolder(binding)
            }
            PermitContentType.TextInput -> {
                val binding = ItemPermitContentTypeTextInputBinding.inflate(LayoutInflater.from(parent.context),parent,false)
                PermitTabsContentTypeInputTextViewHolder(binding)
            }
            PermitContentType.Photo -> {
                val binding = ItemPermitContentTypeTakePhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                PermitTabsContentTypePhotoViewHolder(binding)
            }
            else -> {
                val binding = ItemPermitContentTypePlainTextBinding.inflate(LayoutInflater.from(parent.context),parent,false)
                PermitTabsContentTypePlainTextViewHolder(binding)
            }
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is PermitTabsContentTypePlainTextViewHolder ->  holder.binding.plainText.text = contentTypeItems[position].name
            is PermitTabsContentTypeCheckBoxViewHolder ->{
                holder.binding.checkboxText.text = contentTypeItems[position].name
                holder.binding.permitContentTypeCheckbox.setOnClickListener {
                    presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, holder.binding.permitContentTypeCheckbox.isChecked))
                    if(holder.binding.permitContentTypeCheckbox.isChecked)
                        rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                }
                holder.itemView.setOnClickListener {
                    holder.binding.permitContentTypeCheckbox.isChecked = !holder.binding.permitContentTypeCheckbox.isChecked
                    presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, holder.binding.permitContentTypeCheckbox.isChecked))
                    if(holder.binding.permitContentTypeCheckbox.isChecked) rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                }
                holder.binding.permitContentTypeCheckbox.isEnabled = presenter.isEnableToEdit(presenter.getPermitInto())
                holder.itemView.isEnabled = presenter.isEnableToEdit(presenter.getPermitInto())

                if(contentTypeItems[position].is_mandatory) holder.binding.requiredCheckboxText.visibility = View.VISIBLE else holder.binding.requiredCheckboxText.visibility = View.GONE
                if(!presenter.isEnableToEdit(presenter.getPermitInto())) holder.binding.requiredCheckboxText.setTextColor(ContextCompat.getColor(holder.itemView.context, android.R.color.black)) else holder.binding.permitContentTypeCheckbox.isEnabled = true

                contentTypeItems[position].responses.notEmptyList {
                    holder.binding.permitContentTypeCheckbox.isChecked = presenter.getLastContentTypeResponse(contentTypeItems[position].responses).value as Boolean
                }
                if(contentTypeItems[position].responses.isEmpty())
                    holder.binding.permitContentTypeCheckbox.isChecked = false
            }
            is PermitTabsContentTypeInputTextViewHolder ->{
                holder.binding.inputText.text = contentTypeItems[position].name
                holder.binding.permitContentTypeTextInput.isEnabled = presenter.isEnableToEdit(presenter.getPermitInto())
                holder.binding.permitContentTypeTextInput.addTextChangedListener(onTextChanged = {text, _, _, _ ->
                    if(holder.binding.permitContentTypeTextInput.hasFocus()) {
                        presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, text.toString()))
                        rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                    }
                })
                contentTypeItems[position].responses.notEmptyList {
                    holder.binding.permitContentTypeTextInput.setText(presenter.getLastContentTypeResponse(contentTypeItems[position].responses).value as String)
                }
                if(contentTypeItems[position].is_mandatory)
                    holder.binding.requiredTextInput.visibility = View.VISIBLE
                else
                    holder.binding.requiredTextInput.visibility = View.GONE
                if(!presenter.isEnableToEdit(presenter.getPermitInto())) holder.binding.requiredTextInput.setTextColor(ContextCompat.getColor(holder.itemView.context, android.R.color.black))

            }
            is PermitTabsContentTypePhotoViewHolder ->{
                val imageUriList = mutableListOf<PermitContentTypeResponses>()
                holder.binding.photoOptionalText.text = contentTypeItems[position].name

                val adapter = PermitPhotoListAdapter(imageUriList)
                holder.binding.photoComponentList.adapter = adapter

                if(contentTypeItems[position].is_mandatory)
                    holder.binding.requiredPhoto.visibility = View.VISIBLE
                else
                    holder.binding.requiredPhoto.visibility = View.GONE

                if(!presenter.isEnableToEdit(presenter.getPermitInto()))
                    holder.binding.requiredPhoto.setTextColor(ContextCompat.getColor(holder.itemView.context, android.R.color.black))

                contentTypeItems[position].responses.notEmptyList {
                    val responses = contentTypeItems[position].responses
                    val imageLabel = presenter.getPermitInto().id.toString() + String().emptySpace() +"-" + String().emptySpace() + presenter.getLocalUserFullName()
                    adapter.setNewPayLoad(responses)
                    adapter.isSavedImage = true
                    adapter.userFullName = imageLabel
                    adapter.notifyDataSetChanged()
                }
                contentTypeItems[position].responses.emptyList {  holder.binding.fileDescriptionConstraintLayout.visibility = View.GONE }

                holder.binding.permitContentTypeTakePhotoButton.setOnClickListener {
                    takePhotoOnClickListener()
                    disposables.add(presenter.observeImageUri().firstElement()
                            .flatMap {
                                imageUriList.add(PermitContentTypeResponses(value = it, id = null, user = null, utc_date_time = null))
                                val file = ImageUtil.convertExistingUriToFile(holder.itemView.context,it)
                                val fileName = presenter.getPermitInto().id.toString() + " " + it.lastPathSegment
                                return@flatMap presenter.uploadImages(file, fileName).toMaybe()
                            }
                            .subscribe ({
                                presenter.sendContentTypeComponent(ComponentTypeForm(contentTypeItems[position].id, it.access_key))
                                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
                                holder.binding.fileDescriptionConstraintLayout.visibility = View.VISIBLE
                                holder.binding.requiredPhoto.visibility = View.GONE
                                adapter.setNewPayLoad(imageUriList)
                                adapter.isSavedImage = false
                                adapter.userFullName = presenter.getLocalUserFullName()
                                adapter.notifyDataSetChanged()
                            },{
                                logger.e(this@PermitTaskTabComponentListAdapter::class.java.name, attributes = mapOf(this@PermitTaskTabComponentListAdapter::class.java.name to it.message))}))
                }
                holder.binding.permitContentTypeTakePhotoButton.isEnabled = presenter.isEnableToEdit(presenter.getPermitInto())
            }
        }

    }
    override fun getItemCount(): Int = contentTypeItems.size
    override fun getItemViewType(position: Int): Int {
        this.position = position
        return contentTypeItems.size
    }

    inner class PermitTabsContentTypePlainTextViewHolder(var binding: ItemPermitContentTypePlainTextBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitTabsContentTypeCheckBoxViewHolder(var binding: ItemPermitContentTypeCheckboxBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitTabsContentTypeInputTextViewHolder(var binding:ItemPermitContentTypeTextInputBinding):RecyclerView.ViewHolder(binding.root)
    inner class PermitTabsContentTypePhotoViewHolder(var binding: ItemPermitContentTypeTakePhotoBinding):RecyclerView.ViewHolder(binding.root)

    fun dispose(){
        disposables.dispose()
    }

}