package au.com.signonsitenew.ui.documents.permits.details.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SitePermitTemplate
import au.com.signonsitenew.ui.documents.permits.details.checks.ChecksFragment
import au.com.signonsitenew.ui.documents.permits.details.task.TaskFragment
import au.com.signonsitenew.ui.documents.permits.details.team.TeamTabFragment
import au.com.signonsitenew.utilities.Constants

class PermitDetailsViewPagerAdapter(val activity: FragmentActivity, val permit: Permit?, private val permitInfo: PermitInfo?): FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> {
            val args = Bundle()
            val fragment = TeamTabFragment()
            permit?.let { args.putParcelable(Constants.PERMIT_OBJECT, it) }
            permitInfo?.let { args.putParcelable(Constants.FULL_PERMIT_OBJECT, it) }
            fragment.arguments = args
            fragment
        }
        1 -> {
            val args = Bundle()
            val fragment = TaskFragment()
            permitInfo?.let { args.putParcelable(Constants.FULL_PERMIT_OBJECT, it) }
            fragment.arguments = args
            fragment
        }
        2 -> {
            val args = Bundle()
            val fragment = ChecksFragment()
            permitInfo?.let { args.putParcelable(Constants.FULL_PERMIT_OBJECT, it) }
            fragment.arguments = args
            fragment
        }
        else -> Fragment()

    }

}