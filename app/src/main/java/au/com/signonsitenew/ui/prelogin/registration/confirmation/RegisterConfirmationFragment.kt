package au.com.signonsitenew.ui.prelogin.registration.confirmation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.DebouncedOnClickListener
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Fragment that displays all entered details and an agreement for the Privacy Policy.
 * Agreement leads to registration.
 */
class RegisterConfirmationFragment : DaggerFragment(), RegisterConfirmationDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var logger: Logger

    private val presenterImpl: RegisterConfirmationPresenterImpl by viewModels { viewModelFactory  }
    private lateinit var onNextRegisterConfirmation:OnNextRegisterConfirmation
    private lateinit var mSession: SessionManager

    fun setOnClickListener(onNextRegisterConfirmation: OnNextRegisterConfirmation){
        this.onNextRegisterConfirmation = onNextRegisterConfirmation
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_register_confirmation, container, false)
        presenterImpl.inject(this)
        mSession = SessionManager(activity)
        val privacyPolicyTextView: TextView = rootView.findViewById(R.id.privacy_policy_text_view)
        val termsAndConditionsTextView: TextView = rootView.findViewById(R.id.terms_and_conditions_text_view)
        val termsAndConditionsCheck: CheckBox = rootView.findViewById(R.id.terms_and_conditions_checkbox)
        val privacyPolicyCheck: CheckBox = rootView.findViewById(R.id.privacy_policy_checkbox)
        val ageConsentCheck: CheckBox = rootView.findViewById(R.id.age_consent_checkbox)
        val registerButton: Button = rootView.findViewById(R.id.register_button)

        termsAndConditionsTextView.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                val url = Constants.URL_TERMS_CONDITIONS
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })

        privacyPolicyTextView.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                val url = Constants.URL_PRIVACY_POLICY
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })

        // Set registration button function
        registerButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                // Check that the boxes have been ticked
                if (!termsAndConditionsCheck.isChecked) {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),resources.getString(R.string.registration_error_terms_conditions_text))
                }
                else if (!privacyPolicyCheck.isChecked) {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), resources.getString(R.string.registration_error_privacy_policy_text))
                }
                else if (!ageConsentCheck.isChecked) {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),resources.getString(R.string.registration_error_acknowledge_age_text))
                }
                else {
                    // We're good
                    presenterImpl.registerUser()
                    logger.d( this::class.java.name +" - "+ Constants.USER_CLICK_ON_REGISTER_BUTTON_STEP_FIVE,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail))

                }
            }
        })

        return rootView
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(requireActivity())
    }

    override fun showDataError(error: String) {
        AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),error)
    }

    override fun successResponseNavigateToCompleteRegistration() {
        onNextRegisterConfirmation.navigateToCompleteRegistration()
        logger.d( this::class.java.name +" - "+ Constants.USER_COMPLETE_REGISTRATION_STEP_SIX,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail, Constants.USER_REGISTRATION_ID to mSession.userId))
    }

    interface OnNextRegisterConfirmation {
        fun navigateToCompleteRegistration()
    }
}
