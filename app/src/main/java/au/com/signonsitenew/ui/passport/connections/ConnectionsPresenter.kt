package au.com.signonsitenew.ui.passport.connections

import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.domain.models.EnrolmentResponse
import au.com.signonsitenew.domain.usecases.connections.ConnectionListUseCaseImpl
import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.models.BadgeNotification
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

interface ConnectionsPresenter{
    fun inject(connectionsDisplay: ConnectionsDisplay)
    fun retrieveConnectionList()
    fun filterEnrolmentsByAttendanceEnabledAndHidden(enrolments: List<Enrolment>): List<Enrolment>
    fun getDateTimeZone(enrolment: Enrolment):String
    fun updateBadgeNotificationNumber(counter: Int?)
}

class ConnectionsPresenterImpl @Inject constructor(private val connectionsUseCaseConnectionImpl: ConnectionListUseCaseImpl,
                                                   private val logger:Logger,
                                                   private val rxBusPassport: RxBusPassport) : BasePresenter(), ConnectionsPresenter{

    private lateinit var connectionsDisplay:ConnectionsDisplay

    override fun inject(connectionsDisplay: ConnectionsDisplay){
        this.connectionsDisplay = connectionsDisplay
    }
    override fun retrieveConnectionList() {
        connectionsUseCaseConnectionImpl.getEnrollmentsAsync(object :DisposableSingleObserver<EnrolmentResponse>(){
            override fun onSuccess(response: EnrolmentResponse) {
                if (NetworkErrorValidator.isValidResponse(response))
                    connectionsDisplay.reloadData(response.enrolments)
                else {
                    connectionsDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@ConnectionsPresenterImpl::class.java.name,attributes = mapOf(this@ConnectionsPresenterImpl::retrieveConnectionList.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                connectionsDisplay.showNetworkErrors()
                logger.e(this@ConnectionsPresenterImpl::class.java.name,attributes = mapOf(this@ConnectionsPresenterImpl::retrieveConnectionList.name to error.message))
            }

        })
    }

    override fun filterEnrolmentsByAttendanceEnabledAndHidden(enrolments: List<Enrolment>): List<Enrolment> = connectionsUseCaseConnectionImpl.filterEnrolments(enrolments)

    override fun getDateTimeZone(enrolment: Enrolment):String = connectionsUseCaseConnectionImpl.convertTimeZoneDate(enrolment)

    override fun updateBadgeNotificationNumber(counter: Int?){
        rxBusPassport.send(BadgeNotification(counter, Constants.CONNECTION_TAB_NOTIFICATION))
    }

    override fun onCleared() {
        super.onCleared()
        connectionsUseCaseConnectionImpl.dispose()
    }
}
