package au.com.signonsitenew.ui.prelogin.registration.complete

import au.com.signonsitenew.domain.models.ViewSelector
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.CompleteRegistrationUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface CompleteRegistrationPresenter{
    fun inject(completeRegistrationDisplay: CompleteRegistrationDisplay)
    fun checkIfUserHasPassport()
}
class CompleteRegistrationPresenterImpl @Inject constructor(private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl,
                                                            private val logger:Logger,
                                                            private val completeRegistrationUseCaseImpl: CompleteRegistrationUseCaseImpl) : BasePresenter(),CompleteRegistrationPresenter {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var completeRegistrationDisplay: CompleteRegistrationDisplay

    override fun inject(completeRegistrationDisplay: CompleteRegistrationDisplay){
        this.completeRegistrationDisplay = completeRegistrationDisplay
    }

    override fun checkIfUserHasPassport() {
        disposables.add(
                internetConnectionUseCaseImpl.isInternetConnected().flatMap {
                    when (it) {
                        true -> completeRegistrationUseCaseImpl.validateFirstTimeUser()
                        else -> Single.just(completeRegistrationDisplay.showNetworkErrors())
                    }
                }.subscribe({ response ->
                    if (response is ViewSelector) {
                        if (completeRegistrationUseCaseImpl.validateIfShowPassportButton(response)) {
                            completeRegistrationDisplay.showPassportButton(true)
                        } else {
                            completeRegistrationDisplay.showPassportButton(false)
                            logger.w(this::class.java.name,attributes = mapOf(this::checkIfUserHasPassport.name to toJson(response)))
                        }
                    }
                }, {
                    completeRegistrationDisplay.showNetworkErrors()
                    logger.e(this::class.java.name,attributes = mapOf(this::checkIfUserHasPassport.name to it.message))
                }))
    }

}
