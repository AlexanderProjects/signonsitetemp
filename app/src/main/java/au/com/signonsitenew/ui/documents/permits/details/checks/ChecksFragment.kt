package au.com.signonsitenew.ui.documents.permits.details.checks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentChecksBinding
import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.state.PermitChecksState
import au.com.signonsitenew.domain.models.state.UsingCameraX
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.ui.documents.permits.details.checks.adapters.PermitChecksTabComponentsListAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.Constants
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class ChecksFragment :  DaggerFragment(), ChecksDisplay {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var logger: Logger
    @Inject
    lateinit var rxBusSaveButtonState: RxBusSaveButtonState

    private val presenter: ChecksFragmentPresenterImpl by viewModels { viewModelFactory }
    private var _binding:FragmentChecksBinding? = null
    private val binding get() = _binding!!
    private var permitInfo: PermitInfo? = null
    private lateinit var requestChecksAdapterChecksTab:PermitChecksTabComponentsListAdapter
    private lateinit var inProgressChecksAdapterChecksTab:PermitChecksTabComponentsListAdapter
    private lateinit var closingChecksAdapterChecksTab:PermitChecksTabComponentsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            permitInfo = it.getParcelable(Constants.FULL_PERMIT_OBJECT)
            permitInfo?.let {permitInfo -> presenter.setPermitInfo(permitInfo) }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = FragmentChecksBinding.inflate(inflater, container, false)
        presenter.inject(this)
        presenter.observeStates()
        presenter.setContentTypeComponents()
        return binding.root
    }

    override fun showRequestStateComponents(components:List<ContentTypeItem>,state: PermitChecksState) {
        requestChecksAdapterChecksTab = PermitChecksTabComponentsListAdapter(components, presenter, logger, PermitChecksState.RequestChecks, rxBusSaveButtonState){
            router.navigateToCamerax(requireActivity(), UsingCameraX.ChecksFragment)
        }
        binding.requestChecksList.adapter = requestChecksAdapterChecksTab
    }

    override fun showInProgressStateComponents(components:List<ContentTypeItem>,state: PermitChecksState) {
        inProgressChecksAdapterChecksTab = PermitChecksTabComponentsListAdapter(components, presenter, logger, PermitChecksState.InProgressChecks, rxBusSaveButtonState){
            router.navigateToCamerax(requireActivity(), UsingCameraX.ChecksFragment)
        }
        binding.inProgressChecksList.adapter = inProgressChecksAdapterChecksTab
    }

    override fun showClosingStateComponents(components:List<ContentTypeItem>,state: PermitChecksState) {
        closingChecksAdapterChecksTab = PermitChecksTabComponentsListAdapter(components, presenter, logger, PermitChecksState.ClosingChecks, rxBusSaveButtonState){
            router.navigateToCamerax(requireActivity(), UsingCameraX.ChecksFragment)
        }
        binding.closingChecksList.adapter = closingChecksAdapterChecksTab
    }

    override fun hideRequestChecksHeader() {
        binding.requestChecksHeader.visibility = View.GONE
        binding.requestChecksHeaderText.visibility = View.GONE
    }

    override fun hideInProgressChecksHeader() {
        binding.inProgressChecksHeader.visibility = View.GONE
        binding.inProgressChecksHeaderText.visibility = View.GONE
    }

    override fun hideClosingRequestHeader() {
        binding.closingChecksHeader.visibility = View.GONE
        binding.closingChecksHeaderText.visibility = View.GONE
    }

    override fun showTextForEmptyState() {
        binding.endOfRequestChecks.visibility = View.GONE
        binding.endOfInProgressChecks.visibility = View.GONE
        binding.endOfClosingChecks.visibility = View.GONE
        binding.requestChecksHeaderText.visibility = View.VISIBLE
        binding.requestChecksHeaderText.updatePadding(top = 16)
        binding.requestChecksHeaderText.setTextColor(ContextCompat.getColor(requireContext(),R.color.text_grey_secondary))
        binding.requestChecksHeaderText.text = getString(R.string.permits_checks_empty_state)
    }

    override fun onDetach() {
        super.onDetach()
        requestChecksAdapterChecksTab.dispose()
        inProgressChecksAdapterChecksTab.dispose()
        closingChecksAdapterChecksTab.dispose()
    }
}