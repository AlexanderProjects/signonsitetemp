package au.com.signonsitenew.ui.main

import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import au.com.signonsitenew.domain.usecases.fcm.RegisterFcmTokenUseCase
import au.com.signonsitenew.domain.usecases.location.LocationServiceUseCase
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCase
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl
import au.com.signonsitenew.events.RxBusSigOnActivity
import au.com.signonsitenew.models.BadgeNotification
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface SignOnActivityPresenter {
    fun inject(signOnActivityView: SignOnActivityView)
    fun registerNotificationListener()
    fun isLocationPermissionGranted():Boolean
    fun registerFirebaseIdToken()
    fun checkForSignedStatus()
    fun fragmentPassportSelector(callback: (Fragment) -> Unit, userHasPassportCallback:() -> Unit)
}

class SignOnActivityPresenterImpl @Inject constructor(private val rxBusSigOnActivity: RxBusSigOnActivity,
                                                      private val pref:SharedPreferences,
                                                      private val introPassportUseCase: IntroPassportUseCase,
                                                      private val signOnStatusUseCaseImpl: SignOnStatusUseCaseImpl,
                                                      private val logger: Logger,
                                                      private val sessionManager: SessionManager,
                                                      private val locationServiceUseCase: LocationServiceUseCase,
                                                      private var registrationUseCase: RegisterFcmTokenUseCase) : BasePresenter(), SignOnActivityPresenter {

    private lateinit var signOnActivityView: SignOnActivityView
    private val disposables = CompositeDisposable()

    override fun inject(signOnActivityView: SignOnActivityView) {
        this.signOnActivityView = signOnActivityView
    }

    override fun registerNotificationListener() {
        disposables.add(rxBusSigOnActivity.toObservable()
                .subscribe { notification: BadgeNotification? -> signOnActivityView.updateNotificationValueForPassport(notification) })
    }

    override fun isLocationPermissionGranted():Boolean = locationServiceUseCase.isLocationPermissionGranted()


    override fun registerFirebaseIdToken() {
        pref.getString(Constants.FIREBASE_PUSH_NOTIFICATION_TOKEN,null)?.let { registrationUseCase.setToken(it) }
    }

    override fun checkForSignedStatus() {
        disposables.add(signOnStatusUseCaseImpl.checkForUserStatus()
                .subscribe({
                    if (!(it["signed_on"] as Boolean))
                        signOnActivityView.navigateToMainActivity()
                }, { error: Throwable ->
                    logger.e(this::class.java.name,error,attributes = mapOf(this::checkForSignedStatus.name to error.message))
                }))
    }

    override fun fragmentPassportSelector(callback: (Fragment) -> Unit, userHasPassportCallback:() -> Unit) {
        if(!sessionManager.hasUserPassport) {
            disposables.add(introPassportUseCase.validateFirstTimeUser()
                    .subscribe({
                        if (it.status == Constants.JSON_SUCCESS) {
                            val fragment = introPassportUseCase.validateFragment(it.user.has_passport)
                            sessionManager.hasUserPassport = it.user.has_passport
                            callback(fragment)
                        }
                    }, { error ->
                        logger.e(this::class.java.name, error, attributes = mapOf(this::fragmentPassportSelector.name to error.message))
                    }))
        }else{
            userHasPassportCallback()
        }
    }



    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}