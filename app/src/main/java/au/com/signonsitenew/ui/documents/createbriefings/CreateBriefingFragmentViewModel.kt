package au.com.signonsitenew.ui.documents.createbriefings

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.domain.models.state.CreateBriefingFragmentState
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCase
import au.com.signonsitenew.utilities.addOnPropertyChanged
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CreateBriefingFragmentViewModel @Inject constructor(private val briefingsUseCase: BriefingsUseCase): ViewModel() {

    private lateinit var createBriefingDisplay: CreateBriefingDisplay
    val uiState = ObservableField<CreateBriefingFragmentState>()
    private val disposables: CompositeDisposable = CompositeDisposable()

    fun inject(createBriefingDisplay: CreateBriefingDisplay){
        this.createBriefingDisplay = createBriefingDisplay
    }

    fun checkForAdvanceBriefing(workerBriefing: WorkerBriefing) =
        if(briefingsUseCase.isRichText(workerBriefing))
            uiState.set(CreateBriefingFragmentState.IsAdvancedBriefingLayout(true))
        else
            uiState.set(CreateBriefingFragmentState.IsAdvancedBriefingLayout(false))

    fun observeStates() = disposables.add(uiState.addOnPropertyChanged {
        when(it.get()){
            is CreateBriefingFragmentState.IsAdvancedBriefingLayout -> if((it.get() as CreateBriefingFragmentState.IsAdvancedBriefingLayout).isAdvancedLayout) createBriefingDisplay.showAdvanceBriefingLayout() else createBriefingDisplay.showDefaultBriefingLayout()

    }})

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}