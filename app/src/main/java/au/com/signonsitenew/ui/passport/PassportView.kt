package au.com.signonsitenew.ui.passport

import android.graphics.Bitmap
import au.com.signonsitenew.domain.models.CredentialsResponse
import au.com.signonsitenew.domain.models.User

interface PassportView {
    fun loadUserInfoData(user: User?)
    fun loadCredentialsData(credentialsResponse: CredentialsResponse?)
    fun remoteFailure()
    fun dataError(errorMessage:String)
    fun showHeadShotPhoto(url:String)
    fun updateNotificationValueForPersonalInfo(count: Int?)
    fun updateNotificationValueForEmergencyInfo(count: Int?)
    fun updateNotificationValueForCredentials(count: Int?)
    fun updateNotificationValueForConnections(count: Int?)
}