package au.com.signonsitenew.ui.passport.emergencyinfo

import au.com.signonsitenew.domain.models.User

interface EmergencyInfoDataListener {
    fun showData(user: User?)
}