package au.com.signonsitenew.ui.prelogin.registration.employer

import au.com.signonsitenew.domain.models.EmployersResponse
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.EmployersUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface RegisterEmployerPresenter {
    fun inject(registerEmployerDisplay: RegisterEmployerDisplay)
    fun getEmployers(progressDialog:()->Unit, dismissProgressDialog:()->Unit)
}


class RegisterEmployerPresenterImpl @Inject constructor(private val employersUseCaseImpl: EmployersUseCaseImpl,
                                                        private val logger: Logger,
                                                        private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl) : BasePresenter(), RegisterEmployerPresenter{

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var registerEmployerDisplay: RegisterEmployerDisplay
    private var listOfCompanies = mutableListOf<String>()

    override fun inject(registerEmployerDisplay: RegisterEmployerDisplay){
        this.registerEmployerDisplay = registerEmployerDisplay
    }

    override fun getEmployers(progressDialog:()->Unit, dismissProgressDialog:()->Unit){
        progressDialog()
        disposables.add(internetConnectionUseCaseImpl.isInternetConnected()
                .flatMap {
                    when(it){
                        true -> employersUseCaseImpl.getListOfEmployers()
                        else -> Single.just(registerEmployerDisplay.showNetworkErrors())
                    }
                }.subscribe({response ->
                    if(response is EmployersResponse) {
                        response.companies.forEach { (_, v) -> listOfCompanies.add(v) }
                        registerEmployerDisplay.showListOfCompanies(listOfCompanies)
                    }
                    dismissProgressDialog()
                },{
                    registerEmployerDisplay.showNetworkErrors()
                    dismissProgressDialog()
                    logger.e(this::class.java.name,attributes = mapOf(this::getEmployers.name to it.message))
                }))
    }
    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}