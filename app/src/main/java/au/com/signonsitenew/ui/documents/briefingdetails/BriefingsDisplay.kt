package au.com.signonsitenew.ui.documents.briefingdetails

interface BriefingsDisplay {
    fun showDataWithRichText(url:String,header:MutableMap<String, String>)
    fun showDataWithPlainText()
    fun showProgressLayout()
    fun hideProgressLayout()
    fun showDataErrors(error:String)
    fun showNetworkError()
    fun hideAcknowledgeButton()
    fun showAcknowledgeButton()
    fun showSignature()
    fun hideSignature()
}