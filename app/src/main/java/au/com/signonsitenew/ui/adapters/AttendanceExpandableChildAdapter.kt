package au.com.signonsitenew.ui.adapters

import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.state.AttendanceAlertState

class AttendanceExpandableChildAdapter (private var listOfAttendanceChild: MutableList<AttendanceChildAdapterModel>, val onClickChildAction:(attendanceChildAdapterModel: AttendanceChildAdapterModel)->Unit): RecyclerView.Adapter<AttendanceExpandableChildAdapter.ChildViewHolder>() {
    inner class ChildViewHolder(view: View):RecyclerView.ViewHolder(view){
        val workerName: TextView = view.findViewById(R.id.new_attendance_worker_name)
        val timeIn: TextView = view.findViewById(R.id.time_in)
        val alertIcon : ImageView = view.findViewById(R.id.alert_status_icon)
        val rowLayout: ConstraintLayout = view.findViewById(R.id.child_row_layout)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_child_attendance,parent,false)
        return ChildViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfAttendanceChild.size
    }

    override fun onBindViewHolder(holder: ChildViewHolder, position: Int) {
        holder.workerName.text = listOfAttendanceChild[position].workerName
        holder.timeIn.text = listOfAttendanceChild[position].timeIn
        when(listOfAttendanceChild[position].attendanceAlertState){
            is AttendanceAlertState.Alert -> holder.alertIcon.setImageResource(R.drawable.ic_danger)
            is AttendanceAlertState.Warning -> holder.alertIcon.setImageResource(R.drawable.ic_warn)
            is AttendanceAlertState.Information -> holder.alertIcon.setImageResource(R.drawable.ic_info)
            is AttendanceAlertState.None -> holder.alertIcon.visibility = View.INVISIBLE
        }

        holder.rowLayout.setOnClickListener { onClickChildAction(listOfAttendanceChild[position]) }

        if(listOfAttendanceChild[position].timeOut.equals("null")) {
            holder.workerName.setTextColor(ContextCompat.getColor(holder.workerName.context, R.color.green_acknowledgement))
            holder.timeIn.setTextColor(ContextCompat.getColor(holder.timeIn.context, R.color.green_acknowledgement))
        }else{
            holder.workerName.setTextColor(ContextCompat.getColor(holder.workerName.context, R.color.text_grey_secondary))
            holder.timeIn.setTextColor(ContextCompat.getColor(holder.timeIn.context, R.color.text_grey_secondary))
        }
    }
}