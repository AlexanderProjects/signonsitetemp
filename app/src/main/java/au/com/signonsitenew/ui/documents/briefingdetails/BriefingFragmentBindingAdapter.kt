package au.com.signonsitenew.ui.documents.briefingdetails

import android.view.View
import android.webkit.WebView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import au.com.signonsitenew.domain.models.state.BriefingsFragmentState


@BindingAdapter("showPlainText")
fun TextView.showPlainText(uiState: BriefingsFragmentState){
    visibility = if (uiState is BriefingsFragmentState.ShowPlainTypeText)
        View.VISIBLE
    else
        View.GONE

}

@BindingAdapter("showRichText")
fun WebView.showRichText(uiState: BriefingsFragmentState){
    visibility = if (uiState is BriefingsFragmentState.ShowRichTypeText)
        View.VISIBLE
    else
        View.GONE
}