 package au.com.signonsitenew.ui.documents.permits.current.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty


class CurrentPermitsRecyclerViewAdapter(val permits: List<Permit>, val onClick: (permit: Permit) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    private val permitConstant = " permit "

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder  = CurrentPermitViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pemit, parent, false))


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as CurrentPermitViewHolder
        when (permits[position].state){
            PermitState.Request -> holder.permitStatusTitle.text = Constants.PERMIT_REQUEST_TITLE
            PermitState.PendingApproval -> holder.permitStatusTitle.text = Constants.PERMIT_PENDING_APPROVAL_TITLE
            PermitState.InProgress -> holder.permitStatusTitle.text = Constants.PERMIT_APPROVED_TITLE
            PermitState.PendingClosure -> holder.permitStatusTitle.text = Constants.PERMIT_CLOSING_TITLE
            else -> String().empty()
        }
        holder.permitRefNumber.text = permits[position].human_id
        when(permits[position].state){
            PermitState.Request -> holder.permitSmallDescription.text = permits[position].permit_template.name + " " + permits[position].human_id + Constants.PERMIT_STATE_REQUEST
            PermitState.PendingApproval -> holder.permitSmallDescription.text = permits[position].permit_template.name + permitConstant + permits[position].human_id + Constants.PERMIT_STATE_PENDING_APPROVAL
            PermitState.InProgress -> holder.permitSmallDescription.text = permits[position].permit_template.name + permitConstant + permits[position].human_id + Constants.PERMIT_STATE_APPROVED
            PermitState.PendingClosure -> holder.permitSmallDescription.text = permits[position].permit_template.name + permitConstant + permits[position].human_id + Constants.PERMIT_STATE_CLOSING
            else -> String().empty()
        }
        holder.itemView.setOnClickListener { onClick(permits[position]) }

    }

    override fun getItemCount(): Int = permits.size


    inner class CurrentPermitViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val permitStatusTitle: TextView = itemView.findViewById(R.id.permit_status_title)
        val permitRefNumber : TextView = itemView.findViewById(R.id.permit_ref_number)
        val permitSmallDescription : TextView = itemView.findViewById(R.id.permit_small_description)
    }

}