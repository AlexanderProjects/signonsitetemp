package au.com.signonsitenew.ui.passport.credentials.credentialtypes

import au.com.signonsitenew.domain.models.CredentialType

interface CredentialTypesView {
    fun showData(credentials: List<CredentialType>)
    fun showNetworkErrors()
    fun showDataError(errorMessage:String)
}