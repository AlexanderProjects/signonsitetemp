package au.com.signonsitenew.ui.attendanceregister.workernotes

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentWorkerNotesBinding
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.ui.adapters.WorkerNotesListAdapter
import au.com.signonsitenew.ui.attendanceregister.workerdetails.WorkerDetailsFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject



class WorkerNotesFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var presenterImpl: WorkerNotesFragmentPresenterImpl

    private var _binding: FragmentWorkerNotesBinding? = null
    private val binding get() = _binding!!
    private lateinit var attendanceChildAdapterModel: AttendanceChildAdapterModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            attendanceChildAdapterModel = it.getParcelable(WorkerDetailsFragment.workerAttendanceChildModel)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentWorkerNotesBinding.inflate(inflater, container, false)
        presenterImpl= ViewModelProvider(this, viewModelFactory).get(WorkerNotesFragmentPresenterImpl::class.java)
        val view = binding.root
        binding.workerNotesToolbar.title = "Notes"
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.workerNotesToolbar)
        (requireActivity() as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        val workerList = attendanceChildAdapterModel.workerNotes?.let { presenterImpl.formatDateTimeForWorkerList(it) }
        val adapter = workerList?.let { presenterImpl.mapWorkerNotesListToListOfAny(it) }?.let { WorkerNotesListAdapter(it) }
        val recyclerView = binding.workerNotesList
        recyclerView.adapter = adapter
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            requireActivity().supportFragmentManager.popBackStack()
        }
        return true
    }
    override fun onPrepareOptionsMenu(menu: Menu) {
        val viewAllUserItem = menu.findItem(R.id.view_all_users)
        val addAttendeeItem = menu.findItem(R.id.add_attendee)
        viewAllUserItem.isVisible = false
        addAttendeeItem.isVisible = false
    }

}