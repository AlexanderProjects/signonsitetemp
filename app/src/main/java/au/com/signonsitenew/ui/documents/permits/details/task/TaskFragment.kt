package au.com.signonsitenew.ui.documents.permits.details.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentTaskBinding
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SaveButtonState
import au.com.signonsitenew.domain.models.state.UsingCameraX
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.ui.documents.permits.details.task.adapters.PermitTaskTabComponentListAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.setAllEnabled
import au.com.signonsitenew.utilities.setAllOnClickListener
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import java.util.*
import javax.inject.Inject


class TaskFragment : DaggerFragment(), TaskDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var logger:Logger
    @Inject
    lateinit var rxBusSaveButtonState: RxBusSaveButtonState

    private val presenter:TaskFragmentPresenterImpl by viewModels { viewModelFactory }
    private var _binding:FragmentTaskBinding? = null
    private val binding get() = _binding!!
    private var permitInfo: PermitInfo? = null
    private val startDateTimeCalendar = Calendar.getInstance()
    private val endDateTimeCalendar = Calendar.getInstance()
    private lateinit var adapter:PermitTaskTabComponentListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.inject(this)
        presenter.observeStates()
        arguments?.let {
            permitInfo = it.getParcelable(Constants.FULL_PERMIT_OBJECT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = FragmentTaskBinding.inflate(inflater, container, false)
        permitInfo?.let {
            presenter.setPermitInfo(it)
            presenter.formatStartDate(it.start_date,false)
            presenter.formatEndDate(it.end_date, false)
            presenter.checkForEditPermissions(it)
        }
        binding.startDateLayoutGroup.setAllOnClickListener{
            presenter.onClickInStartDateTimeLayout(binding.startDateTimeValueLayout.isVisible)
        }
        binding.endDateLayoutGroup.setAllOnClickListener {
            presenter.onClickInEndDateTimeLayout(binding.endDateTimeValueLayout.isVisible)
        }
        binding.startDatePicker.setOnDateChangeListener { _, year, month, dayOfMonth ->
            startDateTimeCalendar.set(year,month,dayOfMonth,startDateTimeCalendar.get(Calendar.HOUR_OF_DAY), startDateTimeCalendar.get(Calendar.MINUTE))
            if(presenter.isStartDateAfterNow(startDateTimeCalendar.time.toString())) {
                presenter.formatStartDate(startDateTimeCalendar.time.toString(), true)
                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
            }
        }
        binding.startTimePicker.setOnTimeChangedListener { _, hourOfDay, minute ->
            startDateTimeCalendar.set(startDateTimeCalendar.get(Calendar.YEAR), startDateTimeCalendar.get(Calendar.MONTH), startDateTimeCalendar.get(Calendar.DAY_OF_MONTH) ,hourOfDay,minute)
            if(presenter.isStartDateAfterNow(startDateTimeCalendar.time.toString())) {
                presenter.formatStartDate(startDateTimeCalendar.time.toString(), true)
                rxBusSaveButtonState.sendSaveButtonState(SaveButtonState.ShowButton)
            }
        }
        binding.startDatePicker.minDate = startDateTimeCalendar.timeInMillis
        binding.endDatePicker.setOnDateChangeListener { _, year, month, dayOfMonth ->
            endDateTimeCalendar.set(year,month,dayOfMonth,endDateTimeCalendar.get(Calendar.HOUR_OF_DAY), endDateTimeCalendar.get(Calendar.MINUTE))
            if(presenter.isEndDateIsAfterStartDate(startDateTimeCalendar.time.toString(), endDateTimeCalendar.time.toString()))
                presenter.formatEndDate(endDateTimeCalendar.time.toString(),true)
        }
        binding.endTimePicker.setOnTimeChangedListener { _, hourOfDay, minute ->
            endDateTimeCalendar.set(endDateTimeCalendar.get(Calendar.YEAR), endDateTimeCalendar.get(Calendar.MONTH), endDateTimeCalendar.get(Calendar.DAY_OF_MONTH),hourOfDay,minute)
            if(presenter.isEndDateIsAfterStartDate(startDateTimeCalendar.time.toString(), endDateTimeCalendar.time.toString()))
                presenter.formatEndDate(endDateTimeCalendar.time.toString(),true)
        }
        binding.endDatePicker.minDate = binding.startDatePicker.minDate

        return binding.root
    }

    override fun showStartDateRow() {
        binding.startDateTimeValueLayout.visibility = View.VISIBLE
    }

    override fun hideStarDateRow() {
        binding.startDateTimeValueLayout.visibility = View.GONE
        binding.viewDivider.visibility = View.GONE
    }

    override fun showEndDateRow() {
        binding.endDateTimeValueLayout.visibility = View.VISIBLE
    }

    override fun hideEndDateRow() {
        binding.endDateTimeValueLayout.visibility = View.GONE
        binding.endComponentDivider.visibility = View.GONE
        binding.viewEndDivider.visibility = View.GONE
    }

    override fun showStartDateArrowUp(){
        binding.startDateTimeArrowUp.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_black_arrow_up_24,requireContext().theme))
    }

    override fun showStartDateArrowDown(){
        binding.startDateTimeArrowUp.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_black_arrow_down_24,requireContext().theme))
    }

    override fun showEndDateArrowUp() {
        binding.endDateTimeArrowUp.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_black_arrow_up_24,requireContext().theme))
    }

    override fun showEndDateArrowDown() {
        binding.endDateTimeArrowUp.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_black_arrow_down_24,requireContext().theme))
    }

    override fun showEmptyStartDateMessage() {
        binding.startDateTimeValue.text  = resources.getString(R.string.please_select_start_date)
    }

    override fun showEmptyEndDateMessage() {
        binding.endDateTimeValue.text  = resources.getString(R.string.please_select_end_date)
    }

    override fun showStartDateTime(dateTime: String) {
        binding.startDateTimeValue.text = dateTime
    }

    override fun showEndDateTime(dateTime: String) {
        binding.endDateTimeValue.text = dateTime
    }

    override fun showComponents() {
        adapter = PermitTaskTabComponentListAdapter(presenter.getPermitInto().tasks, presenter, logger, rxBusSaveButtonState) {
            router.navigateToCamerax(requireActivity(), UsingCameraX.TaskFragment)
        }
        binding.componentList.adapter = adapter
    }

    override fun setStartDateTimeInPermitInfo(dateTime: String) {
        val permitInfo = presenter.getPermitInto()
        permitInfo.start_date = dateTime
        presenter.setPermitInfo(permitInfo)
        presenter.updatePermitState(permitInfo)
    }

    override fun setEndDateTimeInPermitInfo(dateTime: String) {
        val permitInfo = presenter.getPermitInto()
        permitInfo.end_date= dateTime
        presenter.setPermitInfo(permitInfo)
        presenter.updatePermitState(permitInfo)
    }

    override fun hideChevrons() {
        binding.startDateTimeArrowUp.visibility = View.GONE
        binding.endDateTimeArrowUp.visibility = View.GONE
    }

    override fun showChevrons() {
        binding.startDateTimeArrowUp.visibility = View.VISIBLE
        binding.endDateTimeArrowUp.visibility = View.VISIBLE
    }

    override fun disableClickOnRows() {
        binding.startDateTimeLayout.setAllEnabled(false)
        binding.endDateTimeLayout.setAllEnabled(false)
    }

    override fun enableClickOnRows() {
        binding.startDateTimeLayout.setAllEnabled(true)
        binding.endDateTimeLayout.setAllEnabled(true)
    }

    override fun onDetach() {
        super.onDetach()
        adapter.dispose()
    }
}