package au.com.signonsitenew.ui.evacuation;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.events.EmergencyEvent;
import au.com.signonsitenew.ui.main.SignedOnActivity;
import au.com.signonsitenew.utilities.Constants;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * EmergencyActivity houses all controls for conducting site evacuations
 */

public class EmergencyActivity extends DaggerAppCompatActivity {
    private static final String LOG = EmergencyActivity.class.getSimpleName();

    @Inject
    AnalyticsEventDelegateService analyticsEventDelegateService;

    protected LinearLayout mContentView;
    protected LinearLayout mProgressView;
    protected ImageView mProgressSpinner;
    protected TextView mTitleTextView;
    protected FragmentManager mFragmentManager;
    protected Boolean mInitiator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mContentView = findViewById(R.id.emergency_content_view);
        mProgressView = findViewById(R.id.progress_linear_layout);
        mProgressSpinner = findViewById(R.id.progress_spinner_image_view);
        mTitleTextView = toolbar.findViewById(R.id.toolbar_title_text_view);
        mTitleTextView.setText(getResources().getString(R.string.site_emergency));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mInitiator = extras.getBoolean(Constants.BUNDLE_EMERGENCY_INITIATOR);
        }
        else {
            mInitiator = false;
        }
        mFragmentManager = getSupportFragmentManager();
        loadEmergencyFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.emergency_container);
                if (fragment instanceof EmergencyInitialFragment) {
                    // If at the main page, finish the activity
                    finish();
                }
                else {
                    // Else navigate back one screen
                    getSupportFragmentManager().popBackStack();
                }
                break;
        }
        return true;
    }

    private void loadEmergencyFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BUNDLE_EMERGENCY_INITIATOR, mInitiator);

        // Load initial Fragment
        Fragment fragment = new EmergencyInitialFragment();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.emergency_container, fragment);
        transaction.commit();
    }

    protected void showEmergencyProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mContentView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    /**
     * Displays a view stating that a network failure occurred while the device was attempting to
     * start an emergency. Show this screen letting them know a restart attempt is in the works with
     * a countdown timer until the job fails.
     */

    protected void showEmergencyContentView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }

        mContentView.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.GONE);
        mProgressSpinner.clearAnimation();
    }

    @Subscribe
    public void onEmergencyEvent(EmergencyEvent event) {
        Log.i(LOG, "Emergency Event received. " + event.toString());
        if (!event.isEmergency && !isFinishing()) {
            // Emergency has finished, take user back to SignedOnActivity
            analyticsEventDelegateService.managerEvacuationCompleted();
            Intent intent = new Intent(EmergencyActivity.this, SignedOnActivity.class);
            intent.putExtra(Constants.BUNDLE_EMERGENCY_INITIATOR, event.isInitiator);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

}
