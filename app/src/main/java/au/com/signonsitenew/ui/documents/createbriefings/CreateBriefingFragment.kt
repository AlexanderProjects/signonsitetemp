package au.com.signonsitenew.ui.documents.createbriefings

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.realm.services.BriefingService
import au.com.signonsitenew.ui.documents.briefings.BriefingsActivity
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import io.realm.Realm
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */


class CreateBriefingFragment : DaggerFragment(), CreateBriefingDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: CreateBriefingFragmentViewModel by viewModels { viewModelFactory  }

    var mListener: OnBriefingSavedListener? = null
    private lateinit var mContentEditText: EditText
    private lateinit var workerBriefing: WorkerBriefing
    private lateinit var mSession: SessionManager
    private lateinit var briefingStartDate:EditText
    private lateinit var briefingEndDate:EditText
    private lateinit var advanceBriefingLayout:ConstraintLayout
    private lateinit var isBriefingForeverCheckBox:CheckBox
    private var mBriefingId: Long = 0L


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            mBriefingId = it.getLong(ARG_BRIEFING_ID)
        }
        mSession = SessionManager(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        workerBriefing = arguments?.getParcelable(Constants.ACTIVE_BRIEFING_KEY)!!
        // Inflate the layout for this fragment
        viewModel.inject(this)
        viewModel.observeStates()
        val rootView = inflater.inflate(R.layout.fragment_create_briefing, container, false)

        (activity as BriefingsActivity).mTitleTextView.text = "New Briefing"

        val clearContentTextView: TextView = rootView.findViewById(R.id.clear_content_text_view)
        mContentEditText = rootView.findViewById(R.id.briefing_content_edit_text)
        briefingStartDate = rootView.findViewById(R.id.briefing_start_date)
        briefingEndDate = rootView.findViewById(R.id.briefing_end_date)
        isBriefingForeverCheckBox = rootView.findViewById(R.id.is_briefing_forever)
        advanceBriefingLayout = rootView.findViewById(R.id.advance_briefing_layout)
        mContentEditText.requestFocus()

        clearContentTextView.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                mContentEditText.setText("")
            }
        })

        // If site already has a briefing, pre-populate the edit text so that it can be edited/updated
        when (mBriefingId) {
            0L -> Log.i(TAG, "No previous briefing")
            else -> {
                mContentEditText.setText(workerBriefing.content)
            }
        }
        viewModel.checkForAdvanceBriefing(workerBriefing)


        return rootView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = context as? OnBriefingSavedListener
        if (mListener == null) {
            throw RuntimeException("$context must implement OnBriefingSavedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.new_briefing)?.isVisible = false
        menu.findItem(R.id.save_briefing)?.isVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save_briefing -> mListener?.onBriefingSaved(mContentEditText.text.toString())
            android.R.id.home -> (activity as BriefingsActivity).mFragmentManager.popBackStack()
        }
        return true
    }

    override fun showAdvanceBriefingLayout() {
        advanceBriefingLayout.visibility = View.VISIBLE
    }

    override fun showDefaultBriefingLayout() {
        advanceBriefingLayout.visibility = View.GONE
    }

    interface OnBriefingSavedListener {
        fun onBriefingSaved(content: String)
    }

    companion object {
        private const val ARG_BRIEFING_ID = "briefingId"
        private val TAG = CreateBriefingFragment::class.java.simpleName
    }
}
