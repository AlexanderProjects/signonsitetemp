package au.com.signonsitenew.ui.main

import au.com.signonsitenew.models.BadgeNotification

interface SignOnActivityView {
    fun updateNotificationValueForPassport(badgeNotification: BadgeNotification?)
    fun showProgressView(text: String?)
    fun showInternetError()
    fun showDataError(message:String)
    fun navigateToMainActivity()
}