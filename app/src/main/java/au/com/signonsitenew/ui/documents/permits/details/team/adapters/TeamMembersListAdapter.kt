package au.com.signonsitenew.ui.documents.permits.details.team.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.ui.documents.permits.details.team.TeamFragmentPresenterImpl

class TeamMembersListAdapter(var listOfMembers:List<RequesteeUser>,
                             private val presenter:TeamFragmentPresenterImpl,
                             private val popUpCallback:(listOfMembers: List<RequesteeUser>, requesteeUser:RequesteeUser)->Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val newMemberList = mutableListOf<RequesteeUser>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddTeamMembersViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_team_member, parent, false))
    }

    override fun getItemCount(): Int = listOfMembers.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as AddTeamMembersViewHolder
        holder.memberName.text = listOfMembers[position].first_name + " " + listOfMembers[position].last_name
        holder.companyName.text = listOfMembers[position].company_name
        presenter.mapPermitUserState(listOfMembers[position].status,{ holder.memberTeamCheckBox.isChecked = true },{ holder.memberTeamCheckBox.visibility = View.GONE })
        holder.itemView.setOnClickListener {
            newMemberList.add(listOfMembers[position])
            popUpCallback(newMemberList, listOfMembers[position])
        }
    }

    inner class AddTeamMembersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val memberName: TextView = itemView.findViewById(R.id.team_member_name)
        val companyName:TextView = itemView.findViewById(R.id.team_member_company_name)
        val memberTeamCheckBox: CheckBox = itemView.findViewById(R.id.team_member_checkbox)
    }
}