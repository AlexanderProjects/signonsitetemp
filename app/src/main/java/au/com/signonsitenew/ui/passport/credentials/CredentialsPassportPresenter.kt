package au.com.signonsitenew.ui.passport.credentials

import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.models.BadgeNotification
import au.com.signonsitenew.domain.models.CredentialsResponse
import au.com.signonsitenew.domain.usecases.credentials.CredentialsUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

interface CredentialsPassportPresenter{
    fun retrieveListCredential()
    fun inject(credentialsView: CredentialsView?)
    fun updateBadgeNotificationNumber(counter: Int?)
}

class CredentialsPassportPresenterImpl @Inject constructor(private val credentialsUseCaseImpl: CredentialsUseCaseImpl,
                                                           private val logger:Logger,
                                                           private val rxBusPassport: RxBusPassport) : BasePresenter(),CredentialsPassportPresenter {

    private var credentialsView: CredentialsView? = null
    private val disposables = CompositeDisposable()
    override fun retrieveListCredential() {
        credentialsUseCaseImpl.getCredentials(object:DisposableSingleObserver<CredentialsResponse>(){
            override fun onSuccess(response: CredentialsResponse) {
                if (NetworkErrorValidator.isValidResponse(response)) {
                    response.credentials?.let { credentialsView!!.reloadData(it) }
                }
                else {
                    credentialsView?.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@CredentialsPassportPresenterImpl::class.java.name,attributes = mapOf(this@CredentialsPassportPresenterImpl::retrieveListCredential.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                credentialsView!!.showNetworkErrors()
                logger.e(this@CredentialsPassportPresenterImpl::class.java.name,attributes = mapOf(this@CredentialsPassportPresenterImpl::retrieveListCredential.name to error.message))
            }
        })
    }

    override fun inject(credentialsView: CredentialsView?) {
        this.credentialsView = credentialsView
    }

    override fun updateBadgeNotificationNumber(counter: Int?) {
        rxBusPassport.send(BadgeNotification(counter, Constants.CREDENTIAL_TAB_NOTIFICATION))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        credentialsUseCaseImpl.dispose()
    }

}