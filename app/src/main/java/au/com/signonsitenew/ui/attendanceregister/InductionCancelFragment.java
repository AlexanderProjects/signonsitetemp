package au.com.signonsitenew.ui.attendanceregister;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.API;
import au.com.signonsitenew.api.HideEnrolmentForUser;
import au.com.signonsitenew.api.MarkVisitor;
import au.com.signonsitenew.realm.services.EnrolledUserService;
import au.com.signonsitenew.realm.services.SiteAttendeeService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class InductionCancelFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_ATTENDEE_ID = "attendeeId";
    private VisitorStatusListener visitorStatusListener;
    private long mAttendeeId;

    private Realm mRealm;

    protected Button mMarkVisitorButton;
    protected Button mRemoveEnrolmentButton;
    protected TextView mCancelTextView;

    public InductionCancelFragment() {
        // Required empty public constructor
    }

    public void setVisitorStatusListener(VisitorStatusListener visitorStatusListener){
        this.visitorStatusListener = visitorStatusListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remove the option icons for the activity
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mAttendeeId = getArguments().getLong(ARG_ATTENDEE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_induction_cancel, container, false);

        mMarkVisitorButton = rootView.findViewById(R.id.mark_visitor_button);
        mRemoveEnrolmentButton = rootView.findViewById(R.id.remove_enrolment_button);
        mCancelTextView = rootView.findViewById(R.id.cancel_text_view);

        mRealm = Realm.getDefaultInstance();

        Toolbar toolbar = rootView.findViewById(R.id.induction_cancel_toolbar);
        toolbar.setTitle(Constants.INDUCTION_STATUS);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mMarkVisitorButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Mark the user as a visitor and hence not requiring an induction
                MarkVisitor.INSTANCE.post(getActivity(), mAttendeeId, new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display a toast and navigate back
                        if (getActivity() != null) {
                            DarkToast.makeText(getActivity(), "This person has been marked as a visitor.");
                            visitorStatusListener.updateStatusToVisitor();
                            // Update user data model

                            // Navigate back to detail view
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    }
                }, new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        if (getActivity() != null) {
                            DarkToast.makeText(getActivity(), "There was a problem marking this user as a visitor, please try again.");
                        }
                    }
                });
            }
        });

        mRemoveEnrolmentButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                HideEnrolmentForUser.post(getActivity(), mAttendeeId, new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (getActivity() != null) {
                            DarkToast.makeText(getActivity(), "This person has been removed from site.");

                            // Remove this user from the enrolments, but not attendees - they may still be on site.
                            EnrolledUserService userService = new EnrolledUserService(mRealm);
                            userService.deleteEnrolledUser(mAttendeeId);

                            // If the user has been on site today, update the SiteAttendee model.
                            SiteAttendeeService attendeeService = new SiteAttendeeService(mRealm);
                            boolean onSite = attendeeService.userOnSite(mAttendeeId);
                            if (onSite) {
                                attendeeService.deactivateEnrolment(mAttendeeId);
                            }

                            // Navigate out of menus
                            getActivity().getSupportFragmentManager().popBackStack();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    }
                }, new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        if (getActivity() != null) {
                            DarkToast.makeText(getActivity(), "There was a problem removing this user from site, please try again.");
                        }
                    }
                });
            }
        });

        mCancelTextView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Navigate back
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(true);
    }

    public interface VisitorStatusListener {
        void updateStatusToVisitor();
    }
}
