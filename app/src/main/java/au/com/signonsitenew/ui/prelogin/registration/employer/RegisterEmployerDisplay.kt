package au.com.signonsitenew.ui.prelogin.registration.employer

interface RegisterEmployerDisplay {
    fun showNetworkErrors()
    fun showListOfCompanies(companies:List<String>)
}