package au.com.signonsitenew.ui.passport.credentials.credentialcreation

interface CredentialCreationView {
    fun showNetworkErrors()
    fun goBackToCredentials()
    fun showDataError(errorMessage:String)
}