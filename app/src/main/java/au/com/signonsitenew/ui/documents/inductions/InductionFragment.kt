package au.com.signonsitenew.ui.documents.inductions

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.*
import android.widget.Button
import android.widget.FrameLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.commit
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


/**
 * A simple Fragment containing a WebView that we use to display SignOnSite forms.
 *
 * The class utilises WebFormInterface to map Javascript functionality to native Android functions.
 *
 * For example, the WebFormInterface is initialised with the name "Android", so in our JS we can put
 *
 * <script type="text/javascript">
function doAndroidFunction(someArgument) {
Android.webInterfaceDefinedFunction(someArgument);
}
</script>
 */
class InductionFragment : DaggerFragment() {

    @Inject
    lateinit var analyticsEventService: AnalyticsEventDelegateService

    private var mCamMessage: String? = null
    private var mFileMessage: ValueCallback<Uri?>? = null
    private var mFilePath: ValueCallback<Array<Uri>>? = null
    private lateinit var mWebView: WebView
    private var mSubmitButton: Button? = null
    private var mCustomView: View? = null
    private var mCustomViewCallback: WebChromeClient.CustomViewCallback? = null
    private var mOriginalOrientation = 0
    private var mOriginalSystemUiVisibility = 0
    private lateinit var progressViewFragment:ProgressViewFragment

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Hide the parent Toolbar for more screen estate
        (activity as InductionActivity?)!!.mToolbar.visibility = View.GONE
        (activity as InductionActivity?)!!.mNextButton.visibility = View.GONE

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_induction, container, false)
        mWebView = rootView.findViewById(R.id.webview)
        mWebView.settings.run {
            javaScriptEnabled = true
            javaScriptCanOpenWindowsAutomatically = true
            allowFileAccess = true
        }
        mSubmitButton = requireActivity().findViewById(R.id.next_button)
        showProgressBar()

        // Prepare URL and Headers for the WebView
        val siteId = SessionManager(activity).siteId
        val url = Constants.URL_SITE_INDUCTION_BASE + siteId + Constants.URL_SITE_INDUCTION_FORM
        val jwt = SessionManager(activity).token
        val headers: MutableMap<String, String> = HashMap()
        headers["Authorization"] = "Bearer $jwt"
        mWebView.loadUrl(url, headers)
        mWebView.addJavascriptInterface(WebFormInterface(activity), "Android")
        mWebView.addJavascriptInterface(WebFormInterfaceForAnalytics(analyticsEventService), Constants.WEBVIEW_JAVASCRIPT_INTERFACE)
        mWebView.webViewClient = object : WebViewClient() {
            /**
             * This method sets whether the page should load in the webview or in the external
             * browser. To avoid navigating away from the induction form (and losing progress),
             * open all external links (such as uploaded images) in the device's default browser.
             * In practical use - they should never leave the page, so handle as relevant.
             */
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                // Launch another Activity that handles URLs
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intent)
                return false
            }
        }
        mWebView.webChromeClient = object : WebChromeClient() {
            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
                Log.d("SignOnSite", consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId())
                return super.onConsoleMessage(consoleMessage)
            }

            override fun onProgressChanged(view: WebView, newProgress: Int) {
                if(newProgress == 100)
                    removeProgressBar()
            }

            override fun onHideCustomView() {
                (activity?.window?.decorView as FrameLayout).removeView(mCustomView)
                mCustomView = null
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                    requireActivity().window.setDecorFitsSystemWindows(false)
                else
                    requireActivity().window.decorView.systemUiVisibility =  mOriginalSystemUiVisibility
                requireActivity().requestedOrientation = mOriginalOrientation
                mCustomViewCallback!!.onCustomViewHidden()
                mCustomViewCallback = null
            }

            override fun onShowCustomView(paramView: View?, paramCustomViewCallback: CustomViewCallback?) {
                if (mCustomView != null) {
                    onHideCustomView()
                    return
                }
                mCustomView = paramView
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                    requireActivity().window.setDecorFitsSystemWindows(true)
                else
                    mOriginalSystemUiVisibility = requireActivity().window.decorView.systemUiVisibility
                mOriginalOrientation = requireActivity().requestedOrientation
                mCustomViewCallback = paramCustomViewCallback
                (requireActivity().window.decorView  as FrameLayout).addView(mCustomView, FrameLayout.LayoutParams(-1, -1))
                requireActivity().window.decorView.systemUiVisibility = 3846
            }

            //Handling input[type="file"] requests for android API 16+
            fun openFileChooser(uploadMsg: ValueCallback<Uri?>?, acceptType: String?, capture: String?) {
                if (FILE_UPLOAD_ENABLED) {
                    mFileMessage = uploadMsg
                    val intent = Intent(Intent.ACTION_GET_CONTENT)
                    intent.addCategory(Intent.CATEGORY_OPENABLE)
                    intent.type = FILE_TYPE
                    startActivityForResult(Intent.createChooser(intent, "File Chooser"), mFileReq)
                }
            }

            //Handling input[type="file"] requests for android API 21+
            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: FileChooserParams): Boolean {
                SLog.d(InductionFragment::class.java.name, "Click upload file ")
                if (!checkPermission(0) || !checkPermission(1)) {
                    // SignOnSite needs a permission to proceed
                    showPermissionDialog()
                    return false
                }
                if (FILE_UPLOAD_ENABLED) {
                    if (mFilePath != null) {
                        mFilePath!!.onReceiveValue(null)
                    }
                    mFilePath = filePathCallback
                    val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    contentSelectionIntent.type = FILE_TYPE
                    val intentArray: Array<Intent?>
                    if (CAM_UPLOAD_ENABLED) {
                        var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (takePictureIntent!!.resolveActivity(activity!!.packageManager) != null) {
                            var photoFile: File? = null
                            try {
                                photoFile = createImage()
                                takePictureIntent.putExtra("PhotoPath", mCamMessage)
                            } catch (e: IOException) {
                                Log.e(TAG, "Image file creation failed", e)
                            }
                            if (photoFile != null) {
                                mCamMessage = "file:" + photoFile.absolutePath
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                            } else {
                                takePictureIntent = null
                            }
                        }
                        intentArray = arrayOf(takePictureIntent)
                    } else {
                        intentArray = arrayOfNulls(0)
                    }
                    val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "File Chooser")
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                    startActivityForResult(chooserIntent, mFileReq)
                }
                return true

            }
        }
        return rootView
    }

    private fun showProgressBar() {
        analyticsEventService.inductionFormSubmissionStarted()
        progressViewFragment = ProgressViewFragment()
        val args = Bundle()
        args.putString(Constants.LOADING_TITLE_KEY, requireContext().resources.getString(R.string.loading_webview_induction_spinner_title))
        args.putString(Constants.LOADING_MESSAGE_KEY, requireContext().resources.getString(R.string.loading_webview_induction_spinner_message))
        progressViewFragment.arguments = args
        requireActivity().supportFragmentManager.commit{
            addToBackStack(Constants.PROGRESS_BAR_FRAGMENT_BACKSTACK)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            add(R.id.fragment_container, progressViewFragment)
        }
    }

    fun removeProgressBar() {
        if(isAdded) {
            val fragment =
                requireActivity().supportFragmentManager.findFragmentById(R.id.fragment_container)
            fragment?.let {
                if (fragment is ProgressViewFragment) {
                    requireActivity().supportFragmentManager.commit {
                        addToBackStack(Constants.PROGRESS_BAR_FRAGMENT_BACKSTACK)
                        setCustomAnimations(
                            R.anim.enter_from_right,
                            R.anim.exit_to_left,
                            R.anim.enter_from_left,
                            R.anim.exit_to_right
                        )
                        remove(progressViewFragment)
                    }
                }
            }
        }
    }

    fun showPermissionDialog() {
        SLog.i(TAG, "Displaying camera + read/write info dialog")
        val builder = AlertDialog.Builder(activity)
                .setTitle(Constants.ALERT_DIALOG_PERMISSION_INDUCTION_TITLE)
                .setMessage(Constants.ALERT_DIALOG_PERMISSION_INDUCTION_MESSAGES)
                .setPositiveButton(Constants.ALERT_DIALOG_PERMISSION_POSITIVE_BUTTON_TITLE) { _, _ -> // User accepted at initial screen - open proper permission dialog
                    requestRequiredPermissions()
                }
                .setNegativeButton(Constants.ALERT_DIALOG_PERMISSION_NEGATIVE_BUTTON_TITLE) { _, _ ->
                    val jsonData = JSONObject()
                    try {
                        jsonData.put("state", "soft_deny")
                    } catch (e: JSONException) {
                        SLog.e(TAG, "JSON Exception occurred: " + e.message)
                    }
                }
        val dialog = builder.create()
        dialog.show()
    }

    //Checking permission for storage and camera for writing and uploading images
    private fun requestRequiredPermissions() {
        val perms = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)

        //Checking for storage permission to write images for upload
        if (FILE_UPLOAD_ENABLED && CAM_UPLOAD_ENABLED && !checkPermission(0) && !checkPermission(1)) {
            ActivityCompat.requestPermissions(requireActivity(), perms, mFilePerm)

            //Checking for WRITE_EXTERNAL_STORAGE permission
        } else if (FILE_UPLOAD_ENABLED && !checkPermission(0)) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), mFilePerm)

            //Checking for CAMERA permissions
        } else if (CAM_UPLOAD_ENABLED && !checkPermission(1)) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA), mFilePerm)
        }
    }

    //Checking if particular permission has been granted
    fun checkPermission(permission: Int): Boolean {
        when (permission) {
            0 -> return ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            1 -> return ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    //Creating image file for upload
    @Throws(IOException::class)
    private fun createImage(): File {
        val file_name = SimpleDateFormat("yyyy_mm_ss").format(Date())
        val new_name = "file_" + file_name + "_"
        val sd_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(new_name, ".jpg", sd_directory)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (Build.VERSION.SDK_INT >= 21) {
            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            requireActivity().window.statusBarColor = resources.getColor(R.color.orange_primary)
            var results: Array<Uri>? = null
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == mFileReq) {
                    if (null == mFilePath) {
                        return
                    }
                    if (intent == null) {
                        if (mCamMessage != null) {
                            results = arrayOf(Uri.parse(mCamMessage))
                        }
                    } else {
                        if (intent.dataString != null) {
                            results = arrayOf(Uri.parse(intent.dataString))
                        }
                    }
                }
            }
            if (results != null) mFilePath!!.onReceiveValue(results) else mFilePath!!.onReceiveValue(arrayOf())
            mFilePath = null
        } else {
            if (requestCode == mFileReq) {
                if (null == mFileMessage) return
                val result = if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
                mFileMessage!!.onReceiveValue(result)
                mFileMessage = null
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val builder = AlertDialog.Builder(activity)
                            .setTitle(Constants.ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_TITLE)
                            .setMessage(Constants.ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_MESSAGE)
                            .setPositiveButton(Constants.ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_POSITIVE_BUTTON_TITLE) { dialogInterface, _ -> // User accepted at initial screen - open proper permission dialog
                                analyticsEventService.siteInductionFormFileUploaded()
                                dialogInterface.dismiss()
                            }
                    val dialog = builder.create()
                    dialog.show()
                } else {
                    DarkToast.makeText(activity, "You will need to grant permission to upload photos")
                }
            }
        }
    }



    companion object {
        private val TAG = InductionFragment::class.java.simpleName
        private const val FILE_TYPE = "image/*"
        private const val FILE_UPLOAD_ENABLED = true
        private const val CAM_UPLOAD_ENABLED = true
        private const val mFileReq = 1
        private const val mFilePerm = 2
    }
}