package au.com.signonsitenew.ui.account;

import android.content.Context;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.API;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.api.ValidatePhone;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.ui.main.SignedOnFragment;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NetworkUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import au.com.signonsitenew.utilities.ToastUtil;
import dagger.android.support.DaggerAppCompatActivity;
import io.realm.Realm;

/**
 *
 */
public class AccountPhoneNumberFragment extends Fragment {

    private static final String LOG = AccountPhoneNumberFragment.class.getSimpleName();

    private SessionManager mSession;
    private SOSAPI mAPI;

    protected TextView mPhoneNumberView;
    protected CountryCodePicker mCountryPicker;
    protected EditText mNewNumberEditText;
    protected Button mSubmitNumberButton;

    public AccountPhoneNumberFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSession = new SessionManager(getActivity());
        mAPI = new SOSAPI(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(getActivity() instanceof  AccountActivity)
            ((AccountActivity)getActivity()).mTitleTextView.setText("Change Mobile Number");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account_phone_number, container, false);

        mPhoneNumberView = (TextView)rootView.findViewById(R.id.phone_number_text_view);
        mCountryPicker = (CountryCodePicker)rootView.findViewById(R.id.ccp);
        mNewNumberEditText = (EditText)rootView.findViewById(R.id.new_number_edit_text);
        mSubmitNumberButton = (Button)rootView.findViewById(R.id.submit_number_button);

        String currentPhoneNumber = mSession.getCurrentUser().get(Constants.USER_PHONE);
        mPhoneNumberView.setText(currentPhoneNumber);

        // Configure the country picker
        mCountryPicker.setFlagBorderColor(ContextCompat.getColor(getActivity(), R.color.hint_text_colour));
        String countryCode = NetworkUtil.determineCountryCode(getActivity());
        mCountryPicker.setDefaultCountryUsingNameCode(countryCode);

        String countries = "AU,NZ,GB";
        if (!countries.contains(countryCode)) {
            countries = countries.concat("," + countryCode);
        }
        mCountryPicker.setCountryPreference(countries);

        mSubmitNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = mNewNumberEditText.getText().toString().trim();
                number = number.replace(" ", "");
                Log.i(LOG, number);

                onNewNumberSubmitted(number);
            }
        });

        return rootView;
    }

    protected boolean phoneNumberDetailsEntered() {
        String oldNumber = mPhoneNumberView.getText().toString().trim();
        String newNumber = mNewNumberEditText.getText().toString().trim();

        return (!newNumber.isEmpty() || oldNumber.isEmpty());
    }

    private void onNewNumberSubmitted(final String number) {
        // Submit details to server
        final String countryCode = mCountryPicker.getSelectedCountryNameCode();

        // First validate the phone number
        ValidatePhone.get(getActivity(), number, countryCode, new API.ResponseCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString(Constants.JSON_STATUS);
                    if (status.equals(Constants.JSON_SUCCESS)) {
                        // Phone number validated successfully
                        updatePhoneNumber(number, countryCode);
                    }
                    else {
                        // Phone number was not valid
                        ((AccountActivity)getActivity()).createAlertDialog("We were not able to validate this phone number. Please check that" +
                                " it is a real phone number or contact Support.");
                    }
                }
                catch (JSONException e) {
                    SLog.e(LOG, "A JSON Exception occurred:" + e.getMessage());
                }
            }
        }, new API.ErrorCallback() {
            @Override
            public void onError() {
                SLog.e(LOG, "Volley error when validating phone number");
                ((AccountActivity)getActivity()).createAlertDialog("We were not able to validate this phone number. Please check that" +
                        " it is a real phone number or contact Support.");
            }
        });
    }

    private void updatePhoneNumber(final String number, final String countryCode) {
        HashMap<String, String> user = mSession.getCurrentUser();
        String email = user.get(Constants.USER_EMAIL);
        String firstName = user.get(Constants.USER_FIRST_NAME);
        String lastName = user.get(Constants.USER_LAST_NAME);

        mAPI.editPersonalDetails(email, firstName, lastName, number, countryCode,
                new SOSAPI.VolleySuccessCallback() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonResponse;
                        try {
                            jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("update_ok")) {
                                // Name updated successfully, update details locally and navigate
                                mSession.updateUserDetail(Constants.USER_LOCALE, countryCode);
                                mSession.updateUserDetail(Constants.USER_PHONE, number);

                                Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
                                User user = realm.where(User.class).findFirst();
                                if (user != null) {
                                    realm.beginTransaction();
                                    user.setPhoneNumber(number);
                                    realm.copyToRealmOrUpdate(user);
                                    realm.commitTransaction();
                                }
                                realm.close();
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                            else {
                                ToastUtil.displayGenericErrorToast(getActivity());
                            }
                        }
                        catch (JSONException e) {
                            SLog.e(LOG, "A JSON Exception occurred");
                            ToastUtil.displayGenericErrorToast(getActivity());
                        }
                    }
                },
                new SOSAPI.VolleyErrorCallback() {
                    @Override
                    public void onResponse() {
                        ToastUtil.displayGenericErrorToast(getActivity());
                    }
                });
    }


}
