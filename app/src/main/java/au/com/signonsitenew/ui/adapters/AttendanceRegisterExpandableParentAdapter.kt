package au.com.signonsitenew.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.adapters.AttendanceAdapterChildModel
import au.com.signonsitenew.domain.models.adapters.AttendanceRegisterParentAdapterModel
import java.util.ArrayList

class AttendanceRegisterExpandableParentAdapter(private var listOfAttendanceRegisterParent: MutableList<AttendanceRegisterParentAdapterModel>, val onClickChildAction:(userId:Int)->Unit): RecyclerView.Adapter<AttendanceRegisterExpandableParentAdapter.ParentViewHolder>(), Filterable {

    internal var filter : AttendanceRegisterAdapterFilter

    init {
        filter = AttendanceRegisterAdapterFilter(this@AttendanceRegisterExpandableParentAdapter, listOfAttendanceRegisterParent as ArrayList<AttendanceRegisterParentAdapterModel>)
    }

    fun refillData(listOfAttendanceRegisterParent: MutableList<AttendanceRegisterParentAdapterModel>){
        this.listOfAttendanceRegisterParent = listOfAttendanceRegisterParent
        notifyDataSetChanged()
    }
    inner class ParentViewHolder(view: View):RecyclerView.ViewHolder(view){
        val companyName: TextView = view.findViewById(R.id.new_attendance_company_name)
        val totalWorkers: TextView = view.findViewById(R.id.new_attendance_number_of_workers)
        val parentConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintLayout2)
        val expandableArrow: ImageView = view.findViewById(R.id.expandable_arrow)
        val childRecyclerView: RecyclerView = view.findViewById(R.id.child_list)
        val nameTitle:TextView = view.findViewById(R.id.textView33)
        val timeTitle:TextView = view.findViewById(R.id.textView34)
        val briefingTitle:TextView = view.findViewById(R.id.textView35)
        val inductionTitle:TextView = view.findViewById(R.id.textView36)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_parent_attendance_register, parent, false)
        return ParentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfAttendanceRegisterParent.size
    }

    override fun onBindViewHolder(holder: ParentViewHolder, position: Int) {
        holder.companyName.text = listOfAttendanceRegisterParent[position].companyName
        holder.totalWorkers.text = listOfAttendanceRegisterParent[position].total
        holder.childRecyclerView.addItemDecoration(DividerItemDecoration(holder.childRecyclerView.context, LinearLayoutManager.VERTICAL))
        holder.childRecyclerView.adapter = AttendanceRegisterExpandableChildAdapter(listOfAttendanceRegisterParent[position].childList as MutableList<AttendanceAdapterChildModel>, onClickChildAction)

        val isExpanded = listOfAttendanceRegisterParent[position].isExpanded
        holder.childRecyclerView.visibility = if (isExpanded) View.VISIBLE else View.GONE
        holder.parentConstraintLayout.setOnClickListener {
            val parentAdapterModel = listOfAttendanceRegisterParent[position]
            parentAdapterModel.isExpanded = !parentAdapterModel.isExpanded
            notifyItemChanged(position)
        }
        if(isExpanded) {
            holder.parentConstraintLayout.setBackgroundColor(Color.parseColor("#FDA447"))
            holder.expandableArrow.setImageResource(R.drawable.ic_arrow_drop_down_white_24dp)
            holder.nameTitle.visibility = View.VISIBLE
            holder.timeTitle.visibility = View.VISIBLE
            holder.briefingTitle.visibility = View.VISIBLE
            holder.inductionTitle.visibility = View.VISIBLE
        }else{
            holder.parentConstraintLayout.setBackgroundColor(Color.parseColor("#CCCCCC"))
            holder.expandableArrow.setImageResource(R.drawable.ic_arrow_drop_up_white_24dp)
            holder.nameTitle.visibility = View.INVISIBLE
            holder.timeTitle.visibility = View.INVISIBLE
            holder.briefingTitle.visibility = View.INVISIBLE
            holder.inductionTitle.visibility = View.INVISIBLE
        }
    }

    override fun getFilter(): Filter {
        return filter
    }

    class AttendanceRegisterAdapterFilter(var adapter:AttendanceRegisterExpandableParentAdapter, private var listOfAttendance: ArrayList<AttendanceRegisterParentAdapterModel>): Filter(){
        private var listOfAttendanceParent :MutableList<AttendanceRegisterParentAdapterModel> = arrayListOf()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            listOfAttendanceParent.clear()
            val results = FilterResults()
            if(constraint!!.isEmpty()){
                listOfAttendanceParent.addAll(listOfAttendance)
            }else{
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for(company in listOfAttendance){
                    if(company.companyName.toLowerCase().startsWith(filterPattern))
                        listOfAttendanceParent.add(company)
                }
            }
            results.values = listOfAttendanceParent
            results.count = listOfAttendanceParent.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapter.refillData(results?.values as ArrayList<AttendanceRegisterParentAdapterModel>)
        }

    }

}