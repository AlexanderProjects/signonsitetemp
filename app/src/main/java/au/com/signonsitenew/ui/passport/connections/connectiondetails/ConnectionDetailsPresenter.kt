package au.com.signonsitenew.ui.passport.connections.connectiondetails

import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.domain.usecases.connections.ConnectionListUseCaseImpl
import au.com.signonsitenew.domain.usecases.connections.UpdateEnrolmentUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface ConnectionDetailsPresenter{
    fun inject(connectionDetailsDisplay: ConnectionDetailsDisplay)
    fun getDateTimeZone(enrolment: Enrolment):String
    fun updateEnrolments(enrolment: Enrolment)
}

class ConnectionDetailsPresenterImpl @Inject constructor(private val connectionsUseCaseConnectionImpl: ConnectionListUseCaseImpl, private val updateEnrolmentUseCaseImpl: UpdateEnrolmentUseCaseImpl) : BasePresenter(), ConnectionDetailsPresenter {

    private lateinit var connectionDetailsDisplay:ConnectionDetailsDisplay
    private val disposables = CompositeDisposable()

    override fun inject(connectionDetailsDisplay: ConnectionDetailsDisplay){
        this.connectionDetailsDisplay = connectionDetailsDisplay
    }

    override fun getDateTimeZone(enrolment: Enrolment):String = connectionsUseCaseConnectionImpl.convertTimeZoneDate(enrolment)

    override fun updateEnrolments(enrolment: Enrolment) {
        disposables.add(updateEnrolmentUseCaseImpl.updateEnrolment(enrolment.id.toString())
            .subscribe ({  response ->
                if(NetworkErrorValidator.isValidResponse(response))
                    connectionDetailsDisplay.reloadData()
                else
                    connectionDetailsDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
            }, {
                connectionDetailsDisplay.showNetworkErrors()}))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}
