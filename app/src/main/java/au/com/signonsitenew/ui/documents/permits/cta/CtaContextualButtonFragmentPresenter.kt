package au.com.signonsitenew.ui.documents.permits.cta

import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.domain.models.state.CtaContextualButtonFragmentState
import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import au.com.signonsitenew.domain.usecases.permits.PermitCtaContextualUseCase
import au.com.signonsitenew.events.*
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface CtaContextualButtonFragmentPresenter {
    fun observeState()
    fun observeForRemoteCtaState()
    fun observeTeamMembersTab()
    fun observeChecksTab()
    fun observeTaskTab()
    fun observeTasksResponses()
    fun observeChecksResponses()
    fun onClickCtaPermitListener()
    fun setCtaButtonTitle(permitInfo: PermitInfo)
    fun inject(ctaContextualButtonDisplay: CtaContextualButtonDisplay)
    fun updateAnExistingPermit(permitInfo: PermitInfo, isRejectedPermit:Boolean)
    fun setPermitInfo(permitInfo: PermitInfo)
    fun setOriginalPermitInfo(permitInfo: PermitInfo)
    fun retrievePermitInfo():PermitInfo
    fun markAsDonePermit(doneState:String,callback:(PermitInfo)->Unit)
    fun clearListOfTeamMembers()
    fun clearListOfComponents()
}

@Suppress("UNCHECKED_CAST")
class CtaContextualButtonFragmentPresenterImpl @Inject constructor(private val permitCtaContextualUseCase: PermitCtaContextualUseCase,
                                                                   private val rxBusUpdateCtaState: RxBusUpdateCtaState,
                                                                   private val rxBusUpdatePermitDetails: RxBusUpdatePermitDetails,
                                                                   private val rxBusUpdateTeamTab: RxBusUpdateTeamTab,
                                                                   private val rxBusTaskTab: RxBusTaskTab,
                                                                   private val rxBusChecksTab: RxBusChecksTab,
                                                                   private val rxBusUserPermitResponses: RxBusUserPermitResponses,
                                                                   private val rxBusTaskContentTypeComponent: RxBusTaskContentTypeComponent,
                                                                   private val rxBusChecksContentTypeComponent: RxBusChecksContentTypeComponent,
                                                                   private val logger:Logger,
                                                                   private val rxBusSelectedTeamMember: RxBusSelectedTeamMember) : CtaContextualButtonFragmentPresenter,BasePresenter() {

    lateinit var display: CtaContextualButtonDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStatePermitObservable = PublishSubject.create<Any>()
    private lateinit var currentState:CtaContextualButtonFragmentState


    override fun observeState() {
        disposable.add(uiStatePermitObservable.subscribe { value ->
            when(value){
                is PermitInfo ->{
                    permitCtaContextualUseCase.setPermitInfo(value)
                    permitCtaContextualUseCase.setListOfMembers(value.requestee_users)
                    currentState = permitCtaContextualUseCase.mapPermitStateForCta(value)
                    when (currentState) {
                        is CtaContextualButtonFragmentState.ShowObtainApprovalButton -> display.showObtainApprovalButton()
                        is CtaContextualButtonFragmentState.ShowObtainApprovalDisableButton -> display.showObtainApprovalDisableButton()
                        is CtaContextualButtonFragmentState.ShowSendToTeamButton -> display.showSendToTeamButton()
                        is CtaContextualButtonFragmentState.ShowApproveRejectButton -> display.showApproveRejectClosureButton()
                        is CtaContextualButtonFragmentState.ShowApproveRejectDisableButton -> display.showApproveRejectDisableButton()
                        is CtaContextualButtonFragmentState.ShowDoneInRequestButton -> display.showDoneInRequestButton()
                        is CtaContextualButtonFragmentState.ShowDoneInProgressButton -> display.showDoneInProgressButton()
                        is CtaContextualButtonFragmentState.ShowSendForClosureButton -> display.showSendForClosureButton()
                        is CtaContextualButtonFragmentState.ShowSendForClosureDisableButton -> display.showSendForClosureDisableButton()
                        is CtaContextualButtonFragmentState.ShowRejectClosureButton -> display.showRejectClosureButton()
                        is CtaContextualButtonFragmentState.ShowApproveRejectClosureButton -> display.showApproveRejectClosureButton()
                        is CtaContextualButtonFragmentState.HideCtaButton -> display.hideButton()
                    }
                }
                is Boolean ->{
                    currentState = permitCtaContextualUseCase.mapForNewPermitStateForCta(value, permitCtaContextualUseCase.retrievePermitInfo())
                    when (currentState){
                        is CtaContextualButtonFragmentState.ShowSendToTeamButton -> display.showSendToTeamButton()
                        is CtaContextualButtonFragmentState.ShowSendToTeamButtonWithWarningMessage -> display.showSendToTeamButton()
                        is CtaContextualButtonFragmentState.ShowSendToTeamDisableButton -> display.showSendToTeamDisableButton()
                    }
                }
                is PermitContentTypeState->{

                }
            }
        })
    }


    override fun observeForRemoteCtaState() {
        disposables.add(rxBusUpdateCtaState.toSendUpdateCtaUpdateObservable()
            .subscribe {
                uiStatePermitObservable.onNext(it)
            })
    }

    override fun observeTeamMembersTab() {
        disposables.add(rxBusSelectedTeamMember.toSelectedMembersObservable()
            .subscribe{
                permitCtaContextualUseCase.setListOfMembers(it as List<RequesteeUser>)
        })
    }

    override fun observeChecksTab() {
        disposables.add(rxBusChecksTab.toTaskTabObservable()
             .subscribe {
                 permitCtaContextualUseCase.setPermitChecksContentTypeState(it)
             })
    }

    override fun observeTaskTab() {
        disposables.add(rxBusTaskTab.toTaskTabObservable()
                .subscribe {
                    if (it.start_date != null && it.end_date != null) uiStatePermitObservable.onNext(it)
                })
    }

    override fun observeTasksResponses() {
        disposables.add(rxBusTaskContentTypeComponent.toTaskCheckBoxResponsesObservable()
                .subscribe {
                    permitCtaContextualUseCase.setComponentTypeForm(it)
                    permitCtaContextualUseCase.checkForEmptyMandatoryFields()
                    rxBusUserPermitResponses.sendUserPermitResponse(it)
                    uiStatePermitObservable.onNext(permitCtaContextualUseCase.retrievePermitInfo())
                })
    }

    override fun observeChecksResponses() {
        disposables.add(rxBusChecksContentTypeComponent.toChecksCheckBoxResponsesObservable()
                .subscribe {
                    permitCtaContextualUseCase.setComponentTypeForm(it)
                    permitCtaContextualUseCase.checkForEmptyMandatoryFields()
                    rxBusUserPermitResponses.sendUserPermitResponse(it)
                    uiStatePermitObservable.onNext(permitCtaContextualUseCase.retrievePermitInfo())
                })
    }


    override fun onClickCtaPermitListener() {
        when (currentState) {
            is CtaContextualButtonFragmentState.ShowSendToTeamButton -> display.showSendToTeamAction()
            is CtaContextualButtonFragmentState.ShowSendToTeamButtonWithWarningMessage -> display.showSendToTeamActionWithWarningMessage()
            is CtaContextualButtonFragmentState.ShowObtainApprovalButton -> display.showObtainApprovalInRequestMessage()
            is CtaContextualButtonFragmentState.ShowApproveRejectButton -> display.showApproveRejectInPendingApprovalMessage()
            is CtaContextualButtonFragmentState.ShowDoneInProgressButton ->  display.showDoneInProgressMessage()
            is CtaContextualButtonFragmentState.ShowDoneInRequestButton -> display.showDoneInRequestMessage()
            is CtaContextualButtonFragmentState.ShowSendForClosureButton -> display.showSendForClosureInProgressMessage()
            is CtaContextualButtonFragmentState.ShowRejectClosureButton -> display.showRejectClosureInClosingMessage()
            is CtaContextualButtonFragmentState.ShowApproveRejectClosureButton -> display.showApproveRejectClosureInClosingMessage()
            is CtaContextualButtonFragmentState.ShowPermitStartDateBeforeNowMessage -> display.showPermitDateBeforeNowError()
            is CtaContextualButtonFragmentState.ShowProgressView -> display.showProgressView()
            is CtaContextualButtonFragmentState.RemoveProgressView -> display.removeProgressView()
        }
    }

    override fun setCtaButtonTitle(permitInfo: PermitInfo) {
        uiStatePermitObservable.onNext(permitInfo)
    }

    override fun inject(ctaContextualButtonDisplay: CtaContextualButtonDisplay) {
        this.display = ctaContextualButtonDisplay
    }

    override fun updateAnExistingPermit(permitInfo: PermitInfo, isRejectedPermit:Boolean) {
        permitInfo.requestee_users = permitCtaContextualUseCase.retrieveListOfMembers()
        disposables.add(Single.just(isRejectedPermit)
                .flatMap { when(it){
                    true -> return@flatMap Single.just(it)
                    false -> return@flatMap Single.just(permitCtaContextualUseCase.hasPermitsAndContentTypeSameResponses())
                } }
                .flatMap { when (it){
                    true -> return@flatMap  Single.just(it)
                    false -> return@flatMap  permitCtaContextualUseCase.savePermitContentComponentsAsync(permitCtaContextualUseCase.buildComponentTypeRespondRequest(),permitInfo.id.toString())
                } }
                .flatMap { permitCtaContextualUseCase.updateCurrentPermitAsync(permitCtaContextualUseCase.buildUpdatePermitRequest(permitInfo,isRejectedPermit),permitInfo.id.toString()) }
                .flatMap { permitCtaContextualUseCase.retrievePermitInfoAsync(permitInfo.id.toString()) }
                .compose(applySchedulers())
                .doOnSubscribe { uiStatePermitObservable.onNext(CtaContextualButtonFragmentState.ShowProgressView) }
                .doAfterTerminate { uiStatePermitObservable.onNext(CtaContextualButtonFragmentState.RemoveProgressView) }
                .subscribe({
                    if(it.status == Constants.JSON_SUCCESS){
                        uiStatePermitObservable.onNext(it.permit)
                        rxBusUpdatePermitDetails.sendPermitDetailsBus(it.permit)
                        rxBusUpdateTeamTab.sendPermitTeamTabBus(it.permit)
                        permitCtaContextualUseCase.setPermitInfo(it.permit)
                    }else{
                        logger.w(this@CtaContextualButtonFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CtaContextualButtonFragmentPresenterImpl::updateAnExistingPermit.name to toJson(it)))
                    }
                },{
                    logger.e(this@CtaContextualButtonFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CtaContextualButtonFragmentPresenterImpl::updateAnExistingPermit.name to it.message))
                }))
    }

    override fun setPermitInfo(permitInfo: PermitInfo) {
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
    }

    override fun setOriginalPermitInfo(permitInfo: PermitInfo) {
        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
    }

    override fun retrievePermitInfo(): PermitInfo {
        return permitCtaContextualUseCase.retrievePermitInfo()
    }

    override fun markAsDonePermit(doneState: String, callback: (PermitInfo) -> Unit) {
        disposables.add(permitCtaContextualUseCase.markCurrentPermitAsDoneAsync(doneState)
            .flatMap { permitCtaContextualUseCase.retrievePermitInfoAsync(permitCtaContextualUseCase.retrievePermitInfo().id.toString()) }
            .compose(applySchedulers())
            .doOnSubscribe { uiStatePermitObservable.onNext(CtaContextualButtonFragmentState.ShowProgressView) }
            .doAfterTerminate { uiStatePermitObservable.onNext(CtaContextualButtonFragmentState.RemoveProgressView) }
            .subscribe({
                if(it.status == Constants.JSON_SUCCESS){
                    callback(it.permit)
                    uiStatePermitObservable.onNext(it.permit)
                }else{
                    logger.w(this@CtaContextualButtonFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CtaContextualButtonFragmentPresenterImpl::markAsDonePermit.name to toJson(it)))
                }
        },{
            logger.e(this@CtaContextualButtonFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CtaContextualButtonFragmentPresenterImpl::markAsDonePermit.name to it.message))
        }))
    }

    override fun clearListOfTeamMembers() {
        permitCtaContextualUseCase.clearListOfMembersInCta()
    }

    override fun clearListOfComponents() {
        permitCtaContextualUseCase.clearListOfComponents()
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        disposables.dispose()
    }
}