package au.com.signonsitenew.ui.prelogin.registration.confirmation

import au.com.signonsitenew.domain.models.UserRegistrationResponse
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.UserRegisterUseCaseImpl
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface RegisterConfirmationPresenter {
    fun inject(registerConfirmationDisplay: RegisterConfirmationDisplay)
    fun registerUser()
}

class RegisterConfirmationPresenterImpl @Inject constructor(private val userRegisterUseCaseImpl: UserRegisterUseCaseImpl,
                                                            private val sessionManager: SessionManager,
                                                            private val logger:Logger,
                                                            private val analyticsEventDelegateService: AnalyticsEventDelegateService,
                                                            private val userService: UserService): BasePresenter(), RegisterConfirmationPresenter {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var registerConfirmationDisplay:RegisterConfirmationDisplay

    override fun inject(registerConfirmationDisplay: RegisterConfirmationDisplay){
        this.registerConfirmationDisplay = registerConfirmationDisplay
    }

    override fun registerUser() {
        disposables.add(userRegisterUseCaseImpl.registration().subscribe({ response ->
                    if (response is UserRegistrationResponse)
                        if (NetworkErrorValidator.isValidResponse(response)) {
                            userService.createUser(response.user.id.toLong(),
                                    sessionManager.currentUser[Constants.USER_FIRST_NAME],
                                    sessionManager.currentUser[Constants.USER_LAST_NAME],
                                    sessionManager.currentUser[Constants.USER_EMAIL], null,
                                    sessionManager.currentUser[Constants.USER_COMPANY],
                                    sessionManager.currentUser[Constants.USER_PHONE],
                                    response.user.auth_token)
                            sessionManager.createSession(sessionManager.currentUser[Constants.USER_EMAIL],
                                    sessionManager.currentUser[Constants.USER_FIRST_NAME],
                                    sessionManager.currentUser[Constants.USER_LAST_NAME],
                                    sessionManager.currentUser[Constants.USER_PHONE],
                                    response.user.auth_token, response.user.id.toString())
                            sessionManager.deleteRegisterPass()
                            analyticsEventDelegateService.registrationCompleteTrackEvent()
                            registerConfirmationDisplay.successResponseNavigateToCompleteRegistration()
                        } else {
                            registerConfirmationDisplay.showDataError(NetworkErrorValidator.getErrorMessage(response))
                            logger.w(this::class.java.name,attributes = mapOf(this::registerUser.name to toJson(response)))
                        }
                }, {
                    registerConfirmationDisplay.showNetworkErrors()
                    logger.e(this::class.java.name, attributes = mapOf(this::registerUser.name to it.message))
                }))
    }


}