package au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers

import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.domain.models.state.AddTeamMembersFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitTeamTabUseCase
import au.com.signonsitenew.events.RxBusAddUnSelectedTeamMember
import au.com.signonsitenew.events.RxBusSelectedTeamMember
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface AddTeamMemberFragmentPresenter{
    fun sendSelectedUsers(memberList:List<RequesteeUser>)
    fun observeUnSelectedMembers()
    fun inject(addTeamMembersFragmentDisplay: AddTeamMembersFragmentDisplay)
    fun retrieveUnSelectedMemberList():List<RequesteeUser>
    fun retrieveSelectedMemberList():List<RequesteeUser>
    fun saveUnSelectedTeamMembers(teamMembers:List<RequesteeUser>)
    fun saveSelectedTeamMembers(teamMembers:List<RequesteeUser>)
    fun removeSelectedUser(requesteeUser: RequesteeUser)
    fun removeUnSelectedListOfMembers(teamMembers: List<RequesteeUser>)
    fun getEnrolledUsers()
    fun observeEnrolledUsers()
}

class AddTeamMemberFragmentPresenterImpl  @Inject constructor(private val permitTeamTabUseCase: PermitTeamTabUseCase,
                                                              private val rxBusAddUnSelectedTeamMember: RxBusAddUnSelectedTeamMember,
                                                              private val rxBusSelectedTeamMember: RxBusSelectedTeamMember): BasePresenter(), AddTeamMemberFragmentPresenter {
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private lateinit var display: AddTeamMembersFragmentDisplay
    private val uiStateObservable = PublishSubject.create<AddTeamMembersFragmentState>()

    override fun sendSelectedUsers(memberList: List<RequesteeUser>) { rxBusSelectedTeamMember.sendSelectedMembers(memberList) }
    override fun observeUnSelectedMembers() {
        disposables.add(rxBusAddUnSelectedTeamMember.toUnSelectedMembersObservable().subscribe{
            when(it){
                is List<*> -> {
                    uiStateObservable.onNext(AddTeamMembersFragmentState.UpdateTeamMemberList(it as List<RequesteeUser>))
                }
                is Any -> {
                    permitTeamTabUseCase.clearUnSelectedListOfMembers()
                }
            }
        })
    }
    override fun inject(addTeamMembersFragmentDisplay: AddTeamMembersFragmentDisplay) { this.display = addTeamMembersFragmentDisplay }
    override fun retrieveUnSelectedMemberList(): List<RequesteeUser> = permitTeamTabUseCase.retrieveUnSelectedTeamMemberList()
    override fun retrieveSelectedMemberList(): List<RequesteeUser> = permitTeamTabUseCase.retrieveSelectedTeamMemberList()
    override fun saveUnSelectedTeamMembers(teamMembers: List<RequesteeUser>) = permitTeamTabUseCase.saveUnSelectedTeamMembers(teamMembers)
    override fun saveSelectedTeamMembers(teamMembers: List<RequesteeUser>) = permitTeamTabUseCase.saveSelectedTeamMembers(teamMembers)
    override fun removeSelectedUser(requesteeUser: RequesteeUser) { permitTeamTabUseCase.removeSelectedUser(requesteeUser) }
    override fun removeUnSelectedListOfMembers(teamMembers: List<RequesteeUser>) { permitTeamTabUseCase.removeUnSelectedListOfMembers(teamMembers) }
    override fun getEnrolledUsers() {
        disposables.add(permitTeamTabUseCase.retrieveEnrolledUsersAsync().subscribe({
            if(it.status == Constants.JSON_SUCCESS){
                uiStateObservable.onNext(AddTeamMembersFragmentState.ShowEnrolledUsers(permitTeamTabUseCase.mapEnrolledUserToRequesteeUser(it.users)))
            }
        },{
            uiStateObservable.onNext(AddTeamMembersFragmentState.ShowError)
        }))
    }
    override fun observeEnrolledUsers() {
        disposable.add(uiStateObservable.subscribe {
            when(it){
                is AddTeamMembersFragmentState.ShowEnrolledUsers -> { display.showUnSelectedMembersList(it.userList) }
                is AddTeamMembersFragmentState.ShowError -> { display.showError() }
                is AddTeamMembersFragmentState.UpdateTeamMemberList -> { display.showUnSelectedMembersList(it.userList)}
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposable.clear()
    }

}