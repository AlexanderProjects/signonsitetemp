package au.com.signonsitenew.ui.main.menu

import au.com.signonsitenew.domain.models.User

interface MenuFragmentDisplay{
    fun retrieveUserData(user:User)
    fun showNetworkErrors()
}