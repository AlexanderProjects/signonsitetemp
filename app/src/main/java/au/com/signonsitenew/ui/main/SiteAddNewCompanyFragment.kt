package au.com.signonsitenew.ui.main


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.DebouncedOnClickListener
import io.realm.Realm


/**
 * A simple [Fragment] subclass.
 *
 */
class SiteAddNewCompanyFragment : Fragment() {
    lateinit var mRealm: Realm
    private var mListener:OnConfirmationSelected? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRealm = Realm.getDefaultInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_site_add_new_company, container, false)

        // Set the title for the parent activity
        (activity as FirstSignOnActivity).mTitleTextView.text = "Select Your Company"

        val newCompanyTextView: TextView = rootView.findViewById(R.id.company_name_text_view)
        val addCompanyButton: Button = rootView.findViewById(R.id.add_company_button)

        addCompanyButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                // Do API Call to add new company to site and associate with user
                val companyName = newCompanyTextView.text.toString().trim()
                mListener?.onCompanyConfirmed(companyName)
            }
        })

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        mRealm.close()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnConfirmationSelected) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnCompanyInteractionListener")
        }
    }

    interface OnConfirmationSelected{
        fun onCompanyConfirmed(companyName: String)
    }

}
