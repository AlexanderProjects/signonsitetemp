package au.com.signonsitenew.ui.attendanceregister.attendance

import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceRegisterParentAdapterModel

interface AttendanceRegisterDisplay{
    fun showData(listAttendance: ArrayList<AttendanceRegisterParentAdapterModel>)
    fun showAttendance(listAttendance: ArrayList<AttendanceParentAdapterModel>)
    fun showWorkerDetails(childAdapterModel: AttendanceChildAdapterModel)
    fun showDataError(errorMessage:String)
    fun showNetworkError()
    fun showProgressView()
    fun hideProgressView()
}