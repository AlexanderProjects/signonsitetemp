package au.com.signonsitenew.ui.navigation
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.*
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.state.UsingCameraX
import au.com.signonsitenew.ui.attendanceregister.AttendanceActivity
import au.com.signonsitenew.ui.attendanceregister.UserAttendanceHistoryFragment
import au.com.signonsitenew.ui.attendanceregister.UserDetailFragment
import au.com.signonsitenew.ui.attendanceregister.workerdetails.WorkerDetailsFragment
import au.com.signonsitenew.ui.attendanceregister.workernotes.WorkerNotesFragment
import au.com.signonsitenew.ui.documents.permits.PermitsActivity
import au.com.signonsitenew.ui.documents.permits.cta.CtaContextualButtonFragment
import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragment
import au.com.signonsitenew.ui.documents.permits.template.TemplatePermitFragment
import au.com.signonsitenew.ui.documents.permits.details.PermitDetailsFragment
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMembersFragment
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.ui.main.documents.DocumentsFragment
import au.com.signonsitenew.ui.passport.connections.connectiondetails.ConnectionDetailsFragment
import au.com.signonsitenew.ui.passport.share.SharePassportFragment
import au.com.signonsitenew.ui.prelogin.SplashActivity
import au.com.signonsitenew.ui.prelogin.registration.complete.CompleteRegistrationFragment
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.ProgressViewFragment
import au.com.signonsitenew.utilities.camera.CameraXFragment
import au.com.signonsitenew.utilities.camera.ImagePreviewFragment
import dagger.android.support.DaggerAppCompatActivity

class Router{

    fun navigateToConnectionDetails(activity: FragmentActivity, enrolment: Enrolment){
        val fragmentTransaction = activity.supportFragmentManager.beginTransaction()
        val connectionDetailsFragment = ConnectionDetailsFragment.newInstance(enrolment)
        if (activity is MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container, connectionDetailsFragment, Constants.FRAGMENT_CONNECTION_LIST_BACK_STACK)
        else
            fragmentTransaction.replace(R.id.signon_container, connectionDetailsFragment, Constants.FRAGMENT_CONNECTION_LIST_BACK_STACK)
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_CREDENTIAL_TYPES_BACK_STACK)
        fragmentTransaction.commit()
    }

    fun navigateToShareFragment(activity: FragmentActivity){
        val fragmentTransaction = activity.supportFragmentManager.beginTransaction()
        val sharePassportFragment = SharePassportFragment.newInstance()
        if (activity is MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container, sharePassportFragment, Constants.FRAGMENT_SHARE_BACK_STACK)
        else
            fragmentTransaction.replace(R.id.signon_container, sharePassportFragment, Constants.FRAGMENT_SHARE_BACK_STACK)
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_SHARE_BACK_STACK)
        fragmentTransaction.commit()
    }

    fun navigateToSignedOnActivity(activity: FragmentActivity) {
        val intent = Intent(activity, SignedOnActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun navigateToMainActivity(activity: FragmentActivity) {
        val intent = Intent(activity, MainActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun navigationToUserAttendanceHistoryFragment(activity: FragmentActivity,attendeeId:Long){
        val historyFragment: Fragment = UserAttendanceHistoryFragment()
        val args = Bundle()
        args.putLong(UserDetailFragment.ATTENDEE_ID, attendeeId)
        historyFragment.arguments = args
        animateTransaction(activity,historyFragment)
    }

    fun navigateUserDetailFragment(activity: FragmentActivity, userId: Int){

        // The user selected a visitor in one of the VisitorsFragments.
        // Navigate to details fragment and display the appropriate details
        val detailFragment = UserDetailFragment()
        val args = Bundle()
        args.putLong(UserDetailFragment.ATTENDEE_ID, userId.toLong())
        detailFragment.arguments = args
        animateTransaction(activity, detailFragment)
    }

    fun navigateToWorkerDetails(activity: FragmentActivity, attendanceChildAdapterModel: AttendanceChildAdapterModel){
        val workerDetailsFragment = WorkerDetailsFragment()
        val args = Bundle()
        args.putParcelable(WorkerDetailsFragment.workerAttendanceChildModel, attendanceChildAdapterModel)
        workerDetailsFragment.arguments = args
        animateTransaction(activity, workerDetailsFragment)
    }

    fun navigateToWorkerNotes(activity: FragmentActivity, attendanceChildAdapterModel: AttendanceChildAdapterModel){
        val workerNotesFragment = WorkerNotesFragment()
        val args = Bundle()
        args.putParcelable(WorkerDetailsFragment.workerAttendanceChildModel, attendanceChildAdapterModel)
        workerNotesFragment.arguments = args
        animateTransaction(activity, workerNotesFragment)
    }

    fun navigateToAttendanceRegisterFragment(activity: FragmentActivity){
        val intent = Intent(activity, AttendanceActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun navigateToInductionForms(activity: FragmentActivity, fragment: Fragment){
        animateTransaction(activity,fragment)
    }


    fun navigateToCompleteRegistration(activity: FragmentActivity){
        val fm = activity.supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        val completeRegFragment = CompleteRegistrationFragment()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        fragmentTransaction.replace(R.id.register_container, completeRegFragment)
        fragmentTransaction.addToBackStack(null).commit()
    }

    fun navigateToSiteList(activity: DaggerAppCompatActivity) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("GET_REG_LOC", true)
        activity.startActivity(intent)
    }

    fun navigateToPassport(activity: DaggerAppCompatActivity) {
        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("NAV_PASSPORT", true)
        activity.startActivity(intent)
    }

    fun navigateToDocumentList(activity: FragmentActivity){
        val intent = Intent(activity, SignedOnActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.HAS_UNACKNOWLEDGED_BRIEFING_DOC, false)
        activity.startActivity(intent)
    }

    fun navigateToDocumentListFromInduction(activity: DaggerAppCompatActivity){
        val intent = Intent(activity, SignedOnActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS)
        intent.putExtra(Constants.HAS_UNACKNOWLEDGED_BRIEFING_DOC, false)
        intent.putExtra(Constants.INDUCTION_SUBMITTED, true)
        activity.startActivity(intent)
    }

    fun navigateToAppSettings(activity: DaggerAppCompatActivity){
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivity(intent)
    }

    fun navigationToMainActivityOrSignedActivity(activity: DaggerAppCompatActivity){
        val intent = Intent(activity, SplashActivity::class.java)
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_MENU)
        intent.putExtra(Constants.SHOW_PROGRESS_VIEW_KEY, true)
        activity.startActivity(intent)
        activity.finish()
    }

    fun navigationToSignedActivity(activity: DaggerAppCompatActivity){
        val intent = Intent(activity, SignedOnActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    fun navigateToDocumentFragment(activity: FragmentActivity){
        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.doc_permits_container, DocumentsFragment())
        transaction.addToBackStack(DocumentsFragment::class.java.name)
        transaction.commit()
    }

    fun navigateToPermitActivity(activity: FragmentActivity){
        val intent = Intent(activity, PermitsActivity::class.java)
        activity.startActivity(intent)
    }

    fun navigateToCurrentPermits(activity: FragmentActivity){
        val currentPermitsFragment = activity.supportFragmentManager.findFragmentByTag(Constants.CURRENT_PERMITS_TAG)?: CurrentPermitsFragment()
        activity.supportFragmentManager.commit {
            addToBackStack(Constants.CURRENT_PERMITS_TAG)
            setReorderingAllowed(true)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            replace(R.id.doc_permits_container, currentPermitsFragment, Constants.CURRENT_PERMITS_TAG)
        }
    }

    fun navigateToPermitDetails(activity: FragmentActivity, permit: Permit){
        val permitDetailsFragment = PermitDetailsFragment()
        val args = Bundle()
        args.putParcelable(Constants.PERMIT_OBJECT, permit)
        permitDetailsFragment.arguments = args
        activity.supportFragmentManager.commit {
            addToBackStack(Constants.PERMIT_DETAILS_TAG)
            setReorderingAllowed(true)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            replace(R.id.doc_permits_container, permitDetailsFragment,Constants.PERMIT_DETAILS_TAG)
        }

    }

    fun navigateToPermitDetails(activity: FragmentActivity, permitInfo: PermitInfo){
        val permitDetailsFragment = PermitDetailsFragment()
        val args = Bundle()
        args.putParcelable(Constants.PERMIT_INFO_OBJECT, permitInfo)
        permitDetailsFragment.arguments = args
        activity.supportFragmentManager.commit {
            addToBackStack(Constants.PERMIT_DETAILS_TAG)
            setReorderingAllowed(true)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            replace(R.id.doc_permits_container, permitDetailsFragment,Constants.PERMIT_DETAILS_TAG)
        }
    }

    fun navigateToPermitDetails(activity: FragmentActivity){
        val permitDetailsFragment = activity.supportFragmentManager.findFragmentByTag(Constants.PERMIT_DETAILS_TAG)!!
        activity.supportFragmentManager.commit {
            addToBackStack(Constants.PERMIT_DETAILS_TAG)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            replace(R.id.doc_permits_container, permitDetailsFragment,Constants.PERMIT_DETAILS_TAG)
        }
    }

    fun navigateToAddTeamMembers(activity: FragmentActivity){
        val fragment = activity.supportFragmentManager.findFragmentByTag(Constants.ADD_TEAM_MEMBERS_TAG) ?: AddTeamMembersFragment.newInstance()
        activity.supportFragmentManager.commit {
            addToBackStack(Constants.ADD_TEAM_MEMBERS_TAG)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            add(R.id.doc_permits_container, fragment,Constants.ADD_TEAM_MEMBERS_TAG)
        }
    }

    fun navigateToTemplatePermit(activity: FragmentActivity){
        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        transaction.replace(R.id.doc_permits_container, TemplatePermitFragment())
        transaction.commit()
    }

    fun showProgressView(activity: FragmentActivity, container:Int){
        val fragment = ProgressViewFragment()
        val args = Bundle()
        args.putBoolean(Constants.PROGRESS_VIEW_HIDE_ACTION_BAR,false)
        fragment.arguments = args
        activity.supportFragmentManager.commit{
            addToBackStack(null)
            setReorderingAllowed(true)
            setCustomAnimations(R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
            add(container, fragment)
        }
    }

    fun removeProgressView(activity: FragmentActivity,container: Int){
        val fragment = activity.supportFragmentManager.findFragmentById(container)!!
        if(fragment is ProgressViewFragment)
            activity.supportFragmentManager.commit{
                addToBackStack(null)
                setReorderingAllowed(true)
                setCustomAnimations(R.anim.enter_from_right,
                        R.anim.exit_to_left,
                        R.anim.enter_from_left,
                        R.anim.exit_to_right)
                remove(fragment)
            }
    }

    fun callCtaContextualButtonFragment(activity: FragmentActivity, permitInfo: PermitInfo?){
        val fragment = CtaContextualButtonFragment()
        permitInfo?.let {
            val args = Bundle()
            args.putParcelable(Constants.PERMIT_INFO_OBJECT, it)
            fragment.arguments = args
        }
        activity.supportFragmentManager.commit{
            addToBackStack(Constants.CTA_CONTEXTUAL_FRAGMENT_TAG)
            replace(R.id.selector_fragment_container, fragment, Constants.CTA_CONTEXTUAL_FRAGMENT_TAG)
        }
    }

    fun navigateToCallUser(activity: FragmentActivity, number:String){
        val number: String = number.trim { num -> num <= ' ' }
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        activity.startActivity(intent)
    }

    fun navigateToCamerax(activity: FragmentActivity, usingCameraX: UsingCameraX){
        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        val fragment = CameraXFragment()
        val args = Bundle()
        args.putParcelable(Constants.CAMERAX_PASSING_OBJECT, usingCameraX)
        fragment.arguments = args
        transaction.add(R.id.doc_permits_container, fragment)
        transaction.addToBackStack(CameraXFragment::class.java.name)
        transaction.commit()
    }

    fun navigateToImagePreview(activity: FragmentActivity, uri: Uri, usingCameraX: UsingCameraX){
        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        val fragment = ImagePreviewFragment()
        val args = Bundle()
        args.putParcelable(Constants.PERMIT_IMAGE_URL, uri)
        args.putParcelable(Constants.CAMERAX_PASSING_OBJECT, usingCameraX)
        fragment.arguments = args
        transaction.add(R.id.doc_permits_container, fragment)
        transaction.addToBackStack(ImagePreviewFragment::class.java.name)
        transaction.commit()
    }

    fun navigateToPreviousView(activity: FragmentActivity){
        activity.supportFragmentManager.popBackStack()
        activity.supportFragmentManager.popBackStack()
    }

    private fun animateTransaction(activity: FragmentActivity, fragment: Fragment){
        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        transaction.replace(R.id.container, fragment)
        transaction.setReorderingAllowed(true)
        transaction.addToBackStack(fragment::class.java.name)
        transaction.commit()
    }

}