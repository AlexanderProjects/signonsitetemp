package au.com.signonsitenew.ui.main.documents

import androidx.databinding.ObservableField
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.domain.models.state.DocumentsFragmentState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.FeatureFlagsBaseUseCase
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCase
import au.com.signonsitenew.domain.usecases.documents.DocumentsUseCase
import au.com.signonsitenew.domain.usecases.featureflag.FeatureFlagsUseCase
import au.com.signonsitenew.domain.usecases.inductions.InductionsUseCase
import au.com.signonsitenew.domain.usecases.permits.PermitsUseCase
import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.events.RxBusSigOnActivity
import au.com.signonsitenew.models.BadgeNotification
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

class DocumentsFragmentPresenter @Inject constructor(private val rxBusPassport: RxBusPassport,
                                                     private val rxBusSigOnActivity: RxBusSigOnActivity,
                                                     private val inductionsUseCase: InductionsUseCase,
                                                     private val documentsUseCase:DocumentsUseCase,
                                                     private val permitsUseCase: PermitsUseCase,
                                                     private val repository: DataRepository,
                                                     private val logger:Logger,
                                                     private val featureFlagsUseCase: FeatureFlagsUseCase,
                                                     private val sessionManager: SessionManager,
                                                     private val userService: UserService,
                                                     private val briefingsUseCase: BriefingsUseCase): BasePresenter() {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private var notificationBadgeCounter : Int = 0
    private lateinit var documentsDisplay:DocumentsDisplay
    private val uiState = ObservableField<DocumentsFragmentState>()

    fun inject(documentsDisplay: DocumentsDisplay){
        this.documentsDisplay = documentsDisplay
    }

    fun addNotificationNumber() = ++notificationBadgeCounter

    fun updateBadgeNotificationNumber() {
        rxBusPassport.send(BadgeNotification(notificationBadgeCounter, Constants.DOCUMENTS_SIGNED_MAIN_TAB_NOTIFICATION))
        notificationBadgeCounter = 0
    }

    fun registerNotificationListener() {
        disposables.add(rxBusPassport
                .toObservable()
                .subscribe { notification: BadgeNotification ->
                    if (notification.notifier.equals(Constants.DOCUMENTS_SIGNED_MAIN_TAB_NOTIFICATION, ignoreCase = true)) {
                        notificationBadgeCounter = notification.badgeNumber
                        uiState.set(DocumentsFragmentState.UpdateNotificationBadge(notificationBadgeCounter, notification.notifier))
                    }
                })
    }

    fun getActiveDocuments(){
        var inductionResponse = InductionResponse()
        documentsUseCase.getActiveDocumentsAsync(object :DisposableSubscriber<Any>(){
            override fun onNext(it: Any?) {
                when (it) {
                    is Boolean -> {
                        if (it == false) uiState.set(DocumentsFragmentState.Error)
                    }
                    is InductionResponse -> {
                        if (NetworkErrorValidator.isValidResponse(it)) {
                            inductionsUseCase.hasFormAvailable(it,hasForm = {
                                if (it.induction != null) {
                                    it.induction?.let { induction ->
                                        repository.saveInduction(induction.id, userService.currentUserId, sessionManager.siteId, induction.type, induction.state?.as_string, induction.state?.set_at) }
                                    inductionResponse = it
                                } else {
                                    repository.saveInduction(null, userService.currentUserId, sessionManager.siteId, null, null, null)
                                    inductionResponse = it
                                    inductionResponse.induction = buildInduction()
                                }
                            },hasNotForm = {
                                repository.deleteInduction(sessionManager.siteId)
                            })
                        }else{
                            logger.w(this::class.java.name,attributes = mapOf(this@DocumentsFragmentPresenter::getActiveDocuments.name to toJson(it)))
                        }
                    }
                    is BriefingWorkerResponse -> {
                        if (NetworkErrorValidator.isValidResponse(it))
                            uiState.set(DocumentsFragmentState.Success(it, inductionResponse))
                        else {
                            uiState.set(DocumentsFragmentState.NoData(it, inductionResponse))
                            logger.w(this::class.java.name,attributes = mapOf(this@DocumentsFragmentPresenter::getActiveDocuments.name to toJson(it)))
                        }
                    }
                }
            }

            override fun onError(error: Throwable?) {
                uiState.set(DocumentsFragmentState.Error)
                logger.e(this::class.java.name,error,attributes = mapOf(this@DocumentsFragmentPresenter::getActiveDocuments.name to error?.message))
            }

            override fun onComplete() {}

        })
    }
    fun observeStates(){
        disposables.add(uiState.addOnPropertyChanged {
            when (it.get()) {
                is DocumentsFragmentState.Success -> documentsDisplay.updateDocumentsState((it.get() as DocumentsFragmentState.Success).workerResponse.briefing,(it.get() as DocumentsFragmentState.Success).inductionResponse.induction)
                is DocumentsFragmentState.NoData -> documentsDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage((it.get() as DocumentsFragmentState.NoData).workerResponse))
                is DocumentsFragmentState.Error -> documentsDisplay.showNetworkError()
                is DocumentsFragmentState.UpdateNotificationBadge -> notifyToMainPassportBadge(BadgeNotification((it.get() as DocumentsFragmentState.UpdateNotificationBadge).counter, (it.get() as DocumentsFragmentState.UpdateNotificationBadge).notifier))
            }
        })
    }

    fun getErrorDocument():Document = document { docType = Constants.DOC_ERROR }

    fun getEmptyInductionDocument():Document = inductionsUseCase.buildEmptyInductionDocument()

    fun getEmptyBriefingDocument():Document = briefingsUseCase.buildEmptyBriefingDocument()

    fun checkForEmptyDocs(docs:MutableList<Document>):MutableList<Document>{
        val filterList = docs.filter{ it.docType == Constants.DOC_EMPTY_INDUCTION }
        val secondFilterList = docs.filter { it.docType == Constants.DOC_EMPTY_BRIEFING }
        val permitFilterList = docs.filter { it.docType == Constants.DOC_PERMITS }
        if(filterList.size == 1 && secondFilterList.size == 1 && permitFilterList.isEmpty()){
            val newDocsList = mutableListOf<Document>()
            newDocsList.add(document { docType = Constants.EMPTY_DOCS })
            return newDocsList
        }
        if(filterList.size == 1 && secondFilterList.size == 1 && permitFilterList.size == 1){
            val newDocsList = mutableListOf<Document>()
            newDocsList.add(document { docType = Constants.DOC_PERMITS })
            return newDocsList
        }
        return docs
    }

    fun buildInductionDocument(induction: Induction, notifierCallback:()->Unit):Document = inductionsUseCase.buildInductionDocument(induction, notifierCallback)

    fun buildBriefingDocument(workerBriefing: WorkerBriefing, notifierCallback: () -> Unit, listener:(workerBriefing: WorkerBriefing)->Unit): Document =
            briefingsUseCase.buildBriefingDocument(workerBriefing,notifierCallback,listener)

    fun buildPermitDocument():Document = permitsUseCase.buildPermitDocument()

    fun isEnablePermitsFeatureFlags(callBack: ()-> Unit){
        featureFlagsUseCase.getUserFeatureFlagsAsync(object : DisposableSingleObserver<FeatureFlagsResponse>(){
            override fun onSuccess(response: FeatureFlagsResponse) {
                if(response.status == Constants.JSON_SUCCESS) {
                    val permitsFeatureFlag = response.user_feature_flags.find { it.feature_flag_name == Constants.PERMITS_KEY }
                    if (permitsFeatureFlag?.value == true) {
                        callBack()
                    }
                }
            }

            override fun onError(error: Throwable) {
                logger.e(this::class.java.name,error,attributes = mapOf(this@DocumentsFragmentPresenter::isEnablePermitsFeatureFlags.name to error?.message))
            }
        })
    }

    private fun buildInduction():Induction{
        val state = State(Constants.DOC_INDUCTION_INCOMPLETE,null,null)
        return Induction(null,null,null,state)
    }
    private fun notifyToMainPassportBadge(badgeNotification: BadgeNotification) { rxBusSigOnActivity.send(badgeNotification) }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        documentsUseCase.dispose()
    }
}