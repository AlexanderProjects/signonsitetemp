package au.com.signonsitenew.ui.passport.credentials.credentialdetails

import au.com.signonsitenew.domain.models.CredentialCreateUpdateRequest
import au.com.signonsitenew.domain.models.CredentialCreateUpdateResponse
import au.com.signonsitenew.domain.models.DeleteCredentialResponse
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.credentials.CredentialsUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import java.io.File
import javax.inject.Inject

interface CredentialUpdatePresenter{
    fun inject(credentialUpdateView: CredentialUpdateView?)
    fun updateCredentialDetails(frontPhotoFile: File?, backPhotoFile: File?, request: CredentialCreateUpdateRequest)
    fun deleteCredential(credentialId:String)
}

class CredentialUpdatePresenterImpl @Inject constructor(private val credentialsUseCaseImpl: CredentialsUseCaseImpl,
                                                        private val logger:Logger,
                                                        private val analyticsEventDelegateService: AnalyticsEventDelegateService) : BasePresenter(), CredentialUpdatePresenter {
    private var credentialUpdateView: CredentialUpdateView? = null
    private val disposables = CompositeDisposable()

    override fun inject(credentialUpdateView: CredentialUpdateView?) {
        this.credentialUpdateView = credentialUpdateView
    }

    override fun updateCredentialDetails(frontPhotoFile: File?, backPhotoFile: File?, request: CredentialCreateUpdateRequest) {
        logger.i(this@CredentialUpdatePresenterImpl::class.java.name,attributes = mapOf(this@CredentialUpdatePresenterImpl::updateCredentialDetails.name to toJson(request)))
        credentialsUseCaseImpl.setCredentialParameters(frontPhotoFile, backPhotoFile, request)
        credentialsUseCaseImpl.createUpdateCredentialAsync(object :DisposableSingleObserver<CredentialCreateUpdateResponse>(){
            override fun onSuccess(response: CredentialCreateUpdateResponse) {
                if (NetworkErrorValidator.isValidResponse(response)) {
                    analyticsEventDelegateService.passportCredentialsUpdated()
                    credentialUpdateView!!.goBackToCredentials()
                } else {
                    credentialUpdateView!!.showDataError(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@CredentialUpdatePresenterImpl::class.java.name,attributes = mapOf(this@CredentialUpdatePresenterImpl::updateCredentialDetails.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                credentialUpdateView!!.showNetworkErrors()
                logger.e(this@CredentialUpdatePresenterImpl::class.java.name,attributes = mapOf(Pair(this@CredentialUpdatePresenterImpl::updateCredentialDetails.name,error)))
            }
        })
    }

    override fun deleteCredential(credentialId:String){
        credentialsUseCaseImpl.setDeleteCredential(credentialId)
        credentialsUseCaseImpl.deleteCredentialAsync(object:DisposableSingleObserver<DeleteCredentialResponse>(){
            override fun onSuccess(response: DeleteCredentialResponse) {
                if (NetworkErrorValidator.isValidResponse(response)) {
                    analyticsEventDelegateService.passportCredentialsRemoved()
                    credentialUpdateView!!.goBackToCredentials()
                }
                else {
                    credentialUpdateView!!.showDataError(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@CredentialUpdatePresenterImpl::class.java.name,attributes = mapOf(this@CredentialUpdatePresenterImpl::deleteCredential.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                credentialUpdateView!!.showNetworkErrors()
                logger.e(this@CredentialUpdatePresenterImpl::class.java.name,attributes = mapOf(this@CredentialUpdatePresenterImpl::deleteCredential.name to error.message))
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        credentialsUseCaseImpl.dispose()
    }

}