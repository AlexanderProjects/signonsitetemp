package au.com.signonsitenew.ui.passport.intro

interface IntroPassportDisplay {
    fun showNetworkError()
    fun showDateError(message:String)
    fun navigateToMainActivityOrSignedActivity()
    fun showProgressView()
    fun removeProgressView()
}