package au.com.signonsitenew.ui.documents.permits.details.checks

import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.state.PermitChecksState

interface ChecksDisplay {
    fun showRequestStateComponents(components:List<ContentTypeItem>,state: PermitChecksState)
    fun showInProgressStateComponents(components:List<ContentTypeItem>, state:PermitChecksState)
    fun showClosingStateComponents(components:List<ContentTypeItem>, state:PermitChecksState)
    fun hideRequestChecksHeader()
    fun hideInProgressChecksHeader()
    fun hideClosingRequestHeader()
    fun showTextForEmptyState()
}