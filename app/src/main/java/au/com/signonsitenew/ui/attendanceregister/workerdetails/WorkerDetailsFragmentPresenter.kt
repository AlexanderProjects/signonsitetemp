package au.com.signonsitenew.ui.attendanceregister.workerdetails

import androidx.fragment.app.Fragment
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import au.com.signonsitenew.domain.usecases.workerdetails.WorkerDetailsUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

interface WorkerDetailsFragmentPresenter {
    fun inject(workerDetailsFragmentDisplay: WorkerDetailsFragmentDisplay)
    fun getInductionIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getBriefingIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getWorkerNotesIconState(workerNotesList: List<WorkerNotes>?):Int
    fun getFragmentAccordingToInductionState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Fragment?
    fun signOnAttendee(attendeeId:String)
    fun signOffAttendee(attendeeId:String)
    fun analyticsForWorkerNotes()
    fun showCellIfUserHasPermissions(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int
    fun canAccessToWorkingNotes(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int
    fun getSignButtonState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int
    fun getWorkerNotesCellState(workerNotesList: List<WorkerNotes>?): Int
    fun getInductionCellState(attendanceChildAdapterModel: AttendanceChildAdapterModel) : Int
}

class WorkerDetailsFragmentPresenterImpl @Inject constructor(private val workerDetailsUseCase: WorkerDetailsUseCaseImpl, private val logger: Logger) : BasePresenter(), WorkerDetailsFragmentPresenter {

    lateinit var workerDetailsFragmentDisplay: WorkerDetailsFragmentDisplay

    override fun inject(workerDetailsFragmentDisplay: WorkerDetailsFragmentDisplay){
        this.workerDetailsFragmentDisplay = workerDetailsFragmentDisplay
    }

    override fun getInductionIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int =  workerDetailsUseCase.getInductionIconState(attendanceChildAdapterModel)

    override fun getBriefingIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int =  workerDetailsUseCase.getBriefingIconState(attendanceChildAdapterModel)

    override fun getWorkerNotesIconState(workerNotesList: List<WorkerNotes>?):Int = workerDetailsUseCase.getWorkerNotesIconState(workerNotesList)

    override fun getFragmentAccordingToInductionState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Fragment? = workerDetailsUseCase.getFragmentAccordingToInductionState(attendanceChildAdapterModel)

    override fun signOnAttendee(attendeeId:String) {
        workerDetailsUseCase.signOnAttendeeAsync(attendeeId,object :DisposableSingleObserver<ApiResponse>(){
            override fun onSuccess(response: ApiResponse) {
                if(response.status == Constants.JSON_SUCCESS) {
                    workerDetailsUseCase.attendanceRegisterWorkerSignOnPressedAnalytics(attendeeId.toInt())
                    workerDetailsFragmentDisplay.signOnSuccessful()
                }
                else {
                    workerDetailsFragmentDisplay.signOnError()
                    logger.w(WorkerDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@WorkerDetailsFragmentPresenterImpl::signOnAttendee.name to toJson(response)))
                }

            }

            override fun onError(error: Throwable) {
                workerDetailsFragmentDisplay.signOffError()
                logger.e(WorkerDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@WorkerDetailsFragmentPresenterImpl::signOnAttendee.name to error.message))
            }

        })
    }

    override fun signOffAttendee(attendeeId:String) {
        workerDetailsUseCase.signOffAttendeeAsync(attendeeId, object : DisposableSingleObserver<ApiResponse>() {
            override fun onSuccess(response: ApiResponse) {
                if (response.status == Constants.JSON_SUCCESS) {
                    workerDetailsUseCase.attendanceRegisterWorkerSignOffPressedAnalytics(attendeeId.toInt())
                    workerDetailsFragmentDisplay.signOffSuccessful()
                }else{
                    workerDetailsFragmentDisplay.signOffError()
                    logger.w(WorkerDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@WorkerDetailsFragmentPresenterImpl::signOffAttendee.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                workerDetailsFragmentDisplay.signOffError()
                logger.e(WorkerDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@WorkerDetailsFragmentPresenterImpl::signOffAttendee.name to error.message))
            }

        })
    }

    override fun analyticsForWorkerNotes() = workerDetailsUseCase.analyticsForWorkerNotes()

    override fun showCellIfUserHasPermissions(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int = workerDetailsUseCase.canAccessUserInductions(attendanceChildAdapterModel)

    override fun canAccessToWorkingNotes(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int = workerDetailsUseCase.canAccessToWorkerNotes(attendanceChildAdapterModel)

    override fun getSignButtonState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int = workerDetailsUseCase.getSignButtonState(attendanceChildAdapterModel)

    override fun getWorkerNotesCellState(workerNotesList: List<WorkerNotes>?): Int = workerDetailsUseCase.getWorkerNotesCellState(workerNotesList)

    override fun getInductionCellState(attendanceChildAdapterModel: AttendanceChildAdapterModel) : Int = workerDetailsUseCase.getInductionCellState(attendanceChildAdapterModel)

    override fun onCleared() {
        super.onCleared()
        workerDetailsUseCase.dispose()
    }
}