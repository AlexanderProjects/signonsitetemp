package au.com.signonsitenew.ui.passport.emergencyinfo

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.viewModels
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.utilities.NotificationBadgeCounter
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.MainActivityNavigationListener
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.ui.main.SignedOnActivityNavigationListener
import au.com.signonsitenew.ui.passport.PassportFragment.Companion.setEmergencyInfoDataListener
import au.com.signonsitenew.ui.passport.PassportFragment.Companion.setPassportTabMenuToEmergencyListener
import au.com.signonsitenew.ui.passport.PassportNavigationListener
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuToEmergencyListener
import au.com.signonsitenew.ui.passport.listeners.PassportTabMenuToEmergencyListener
import au.com.signonsitenew.utilities.*
import au.com.signonsitenew.utilities.phonepicker.PhoneContactSelectedListener
import au.com.signonsitenew.utilities.phonepicker.PhonePickerFragment
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import dagger.android.support.DaggerFragment
import javax.inject.Inject




class EmergencyInfoFragment : DaggerFragment(), EmergencyInfoDataListener, MainTabMenuToEmergencyListener, PassportTabMenuToEmergencyListener, EmergencyInfoDisplay, PhoneContactSelectedListener {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var sessionManager: SessionManager
    @Inject
    lateinit var mapper: Mapper
    private val presenter: EmergencyInfoPresenterImpl by viewModels { viewModelFactory}
    private lateinit var contactNameEditText: EditText
    private lateinit var contactPhoneEditText: EditText
    private lateinit var secondContactNameEditText: EditText
    private lateinit var secondContactPhoneEditText: EditText
    private lateinit var medicalInformationEditText: EditText
    private lateinit var emergencyWarning: ImageView
    private lateinit var contactNameWarning: ImageView
    private lateinit var contactPhoneWarning: ImageView
    private lateinit var secondContactNameWarning: ImageView
    private lateinit var secondContactPhoneWarning: ImageView
    private var isFocusInMedicalInfo:Boolean = false
    private var medicalInformationWarning: ImageView? = null
    private lateinit var saveButton: ExtendedFloatingActionButton
    private var user: User? = null
    private var phoneNumber = PhoneNumber()
    private lateinit var rootView: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.emergency_info_fragment, container, false)
        rootView = view.rootView
        presenter.inject(this)
        presenter.observeStates()
        contactNameWarning = view.findViewById(R.id.contact_name_warning_img)
        contactNameEditText = view.findViewById(R.id.contact_name_emergency_editText)
        saveButton = view.findViewById(R.id.save_emergency_info_fab)
        saveButton.setOnClickListener {
            hideKeyboard()
            updateData()
        }
        contactNameEditText.addTextChangedListener(CustomEditTextWatcher(contactNameEditText,
                updateFlags = {
                    sessionManager.editingInEmergencyPassport = true
                    saveButton.show()
                },
                showNotProvided = {
                    contactNameEditText.hint = Constants.NOT_PROVIDED_HINT
                    contactNameWarning.visibility = View.VISIBLE
                },
                clearActions = { contactNameWarning.visibility = View.INVISIBLE }))
        contactPhoneWarning = view.findViewById(R.id.contact_phone_warning_img)
        contactPhoneEditText = view.findViewById(R.id.contact_phone_emergency_editText)
        contactPhoneEditText.addTextChangedListener(PhoneNumberTextWatcher(contactPhoneEditText,callActionOnPhoneEditText = {
            sessionManager.editingInEmergencyPassport = true
            saveButton.show()
            contactPhoneWarning.visibility = View.INVISIBLE
            isFocusInMedicalInfo = false
        }))
        contactPhoneEditText.setOnClickListener {
            presenter.isPrimaryContact = true
            NavigationHelper.navigateToPhonePicker(activity, user, true, false)
        }
        secondContactNameEditText = view.findViewById(R.id.second_contact_name_emergency_editText)
        secondContactNameEditText.addTextChangedListener(EditTextWatcher(secondContactNameEditText,object :EditTextWatcher.CallAction{
            override fun call() {
                sessionManager.editingInEmergencyPassport = true
                saveButton.show()
                isFocusInMedicalInfo = false
            }
        }))
        secondContactPhoneEditText = view.findViewById(R.id.second_phone_number_emergency_editText)
        secondContactPhoneEditText.setOnClickListener {
            presenter.isSecondaryContact = true
            NavigationHelper.navigateToPhonePicker(activity, user, false, true)
        }
        secondContactPhoneEditText.addTextChangedListener(PhoneNumberTextWatcher(secondContactPhoneEditText, callActionOnPhoneEditText = {
            sessionManager.editingInEmergencyPassport = true
            saveButton.show()
        }))
        medicalInformationEditText = view.findViewById(R.id.medical_info_editText)
        medicalInformationEditText.addTextChangedListener(EditTextWatcher(medicalInformationEditText, object :EditTextWatcher.CallAction{
            override fun call() {
                sessionManager.editingInEmergencyPassport = true
                saveButton.show()
                isFocusInMedicalInfo = true
            }
        }))
        secondContactNameWarning = view.findViewById(R.id.second_contact_name_warning_img)
        secondContactPhoneWarning = view.findViewById(R.id.second_contact_phone_number_warning_img)
        medicalInformationWarning = view.findViewById(R.id.medical_info_warning_img)
        medicalInformationWarning?.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.MEDICAL_INFORMATION_MESSAGE, Constants.MEDICAL_INFORMATION_TITLE) }
        emergencyWarning = view.findViewById(R.id.emergency_warning_img)
        emergencyWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.EMERGENCY_MESSAGE, Constants.EMERGENCY_TITLE) }

        view.findViewById<ImageView>(R.id.second_contact_name_clear_icon).setOnClickListener  {
            presenter.clearButtonClicked(secondContactNameEditText.text.toString())
            this.user?.secondary_contact?.name = null
            secondContactNameEditText.setText(String().empty())
        }
        view.findViewById<ImageView>(R.id.second_contact_phone_clear_icon).setOnClickListener  {
            presenter.clearButtonClicked(secondContactPhoneEditText.text.toString())
            this.user?.secondary_contact?.phone_number = null
            secondContactPhoneEditText.setText(String().empty())
        }
        view.findViewById<ImageView>(R.id.clear_medical_info_clear_img).setOnClickListener {
            presenter.clearButtonClicked(medicalInformationEditText.text.toString())
            this.user?.medical_information = null
            medicalInformationEditText.setText(String().empty())
        }

        view.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            view.getWindowVisibleDisplayFrame(r)
            val height = r.height()
            val heightDiff: Int = view.rootView.bottom - r.bottom
            if (view.rootView.height - (r.bottom - r.top) > 500) {
                // Keyboard open
                    if (isFocusInMedicalInfo) {
                        //view.setMargins(saveButton, 0, 0, 50, 400)
                        saveButton.translationY = - 400f
                    }
                    else {
                        //view.setMargins(saveButton, 0, 0, 50, 820)
                        saveButton.translationY = - 820f
                    }
            } else {
                // Keyboard close
                //view.setMargins(saveButton,0,0,50,50)
                saveButton.translationY = -50f
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setEmergencyInfoDataListener(this)
        PhonePickerFragment.setPhoneContactSelectedListener(this)
        setPassportTabMenuToEmergencyListener(this)
        SignedOnActivity.setMenuToEmergencyListener(this)
        MainActivity.setMenuToEmergencyListener(this)
    }

    override fun showData(user: User?) {
        this.user = user
        this.user?.primary_contact?.let {
            if (it.name?.isNotEmpty()!!) {
                contactNameEditText.setText(it.name)
                contactNameWarning.visibility = View.INVISIBLE
            }

            it.phone_number?.let { contactPhoneNumber ->
                if (contactPhoneNumber.number?.isNotEmpty()!!) {
                    contactPhoneWarning.visibility = View.INVISIBLE
                    contactPhoneEditText.setText(contactPhoneNumber.number)
                }
            }
        }

        this.user?.secondary_contact?.let { it ->
            if (it.name?.isNotEmpty()!!) {
                secondContactNameWarning.visibility = View.INVISIBLE
                secondContactNameEditText.setText(it.name)
            }
            it.phone_number?.let { secondContactPhoneNumber ->
                if (secondContactPhoneNumber.number?.isNotEmpty()!!) {
                    secondContactPhoneWarning.visibility = View.INVISIBLE
                    secondContactPhoneEditText.setText(secondContactPhoneNumber.number)
                }
            }
        }
        this.user?.medical_information?.let {
            if (it.isNotEmpty()) {
                medicalInformationEditText.setText(it)
            }
        }
        this.user?.phone_number?.let {
            phoneNumber.alpha2 = user?.phone_number?.alpha2
        }
        presenter.updateBadgeNotificationNumber(this.user?.let { NotificationBadgeCounter.notifierNumberForEmergencyInfo(it) })
        saveButton.hide()
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenter.showUpdatedEmergencyInfo() }
    }

    override fun showDataErrors(error: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireActivity(),error)
    }

    override fun showUpdatedData(user: User?) {
        showData(user!!)
    }

    override fun hideSaveButton() {
        saveButton.hide()
    }

    override fun showSaveButton() {
        saveButton.show()
    }

    private fun updateData() {
        val userInfoUpdateRequest = UserInfoUpdateRequest()
        userInfoUpdateRequest.primary_contact = presenter.validateContact(contactNameEditText.text.toString(), contactPhoneEditText.text.toString(), phoneNumber.alpha2!!)
        userInfoUpdateRequest.secondary_contact = presenter.validateContact(secondContactNameEditText.text.toString(), secondContactPhoneEditText.text.toString(), phoneNumber.alpha2!!)
        presenter.isMedicalInfoEmpty(medicalInformationEditText.text.toString(),
                isEmpty = { userInfoUpdateRequest.medical_information = String().empty() },
                isNotEmpty = { userInfoUpdateRequest.medical_information = medicalInformationEditText.text.toString() })
        presenter.updateEmergencyInfoData(userInfoUpdateRequest)
    }


    override fun phoneNumberValidated(phoneNumber: PhoneNumber?) {
        this.phoneNumber = phoneNumber!!
        if (presenter.isPrimaryContact) {
            contactPhoneEditText.setText(phoneNumber.number)
            presenter.isPrimaryContact = false
        }
        if (presenter.isSecondaryContact) {
            secondContactPhoneEditText.setText(phoneNumber.number)
            presenter.isSecondaryContact = false
        }
    }

    override fun mainMenuTabSelected(position: Int) {
        presenter.checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position, callAction = {
            AlertDialogMessageHelper.saveBeforeChangeScreenAlert(activity,
                    Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE,
                    Constants.ALERT_DIALOG_TITLE_FOR_SAVE,
                    { sessionManager.editingInEmergencyPassport = false }) { navigateToPassport() }
        })
    }

    private fun navigateToPassport() {
        if (activity is SignedOnActivity)
            signedOnActivityNavigationListener.navigateToTab(2)
        if (activity is MainActivity)
            mainActivityNavigationListener.navigateToTab(1)
    }

    override fun passportMenuTabSelected(position: Int) {
        presenter.checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position, callAction = {
            AlertDialogMessageHelper.saveBeforeChangeScreenAlert(activity,
                    Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE,
                    Constants.ALERT_DIALOG_TITLE_FOR_SAVE,
                    { sessionManager.editingInEmergencyPassport = false }) { navigateToEmergencyInfo() }
        })

    }

    private fun navigateToEmergencyInfo() {
        passportNavigationListener.navigateToTab(1)
    }

    private fun hideKeyboard(){
        if (isKeyboardVisible(rootView)) {
            val inputMethodManager: InputMethodManager = requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
        }
    }

    companion object {
        private lateinit var passportNavigationListener: PassportNavigationListener
        private lateinit var signedOnActivityNavigationListener: SignedOnActivityNavigationListener
        private lateinit var mainActivityNavigationListener: MainActivityNavigationListener
        fun newInstance(): EmergencyInfoFragment {
            return EmergencyInfoFragment()
        }

        fun setPassportNavigationListener(listener: PassportNavigationListener) {
            passportNavigationListener = listener
        }

        @JvmStatic
        fun setSignedOnActivityNavigationListener(listener: SignedOnActivityNavigationListener) {
            signedOnActivityNavigationListener = listener
        }

        @JvmStatic
        fun setMainActivityNavigationListener(listener: MainActivityNavigationListener) {
            mainActivityNavigationListener = listener
        }
    }
}