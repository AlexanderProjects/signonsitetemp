package au.com.signonsitenew.ui.attendanceregister;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class InductionOptionsFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_ATTENDEE_ID = "attendeeId";

    private long mAttendeeId;

    protected Button mInductButton;
    protected TextView mOtherActionsText;

    public InductionOptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remove the option icons for the activity
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mAttendeeId = getArguments().getLong(ARG_ATTENDEE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_induction_options, container, false);

        // Set title
        Toolbar toolbar = rootView.findViewById(R.id.induction_options_toolbar);
        toolbar.setTitle(Constants.REVIEW_OPTIONS);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mInductButton = rootView.findViewById(R.id.manually_induct_user_button);
        mOtherActionsText = rootView.findViewById(R.id.other_actions_text_view);

        mInductButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Start webview
                Fragment manualInductionFragment = new ManualInductionFragment();
                Bundle args = new Bundle();
                args.putLong(ARG_ATTENDEE_ID, mAttendeeId);
                manualInductionFragment.setArguments(args);
                ((AttendanceActivity)getActivity()).navigateToFragment(manualInductionFragment, true);
            }
        });

        mOtherActionsText.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Navigate to Status screen - replace the fragment here so back goes straight back to the detail activity
                Fragment inductionCancelFragment = new InductionCancelFragment();
                Bundle args = new Bundle();
                args.putLong(ARG_ATTENDEE_ID, mAttendeeId);
                inductionCancelFragment.setArguments(args);
                // Don't add this fragment to he back stack
                ((AttendanceActivity)getActivity()).navigateToFragment(inductionCancelFragment, true);
            }
        });

        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(false);
    }
}
