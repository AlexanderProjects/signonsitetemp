package au.com.signonsitenew.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.state.AttendanceAlertState

import java.util.ArrayList

class AttendanceExpandableParentAdapter (private var listOfAttendanceParent: MutableList<AttendanceParentAdapterModel>, val onClickChildAction:(attendanceChildAdapterModel: AttendanceChildAdapterModel)->Unit): RecyclerView.Adapter<AttendanceExpandableParentAdapter.ParentViewHolder>(), Filterable {

    internal var filter: AttendanceAdapterFilter = AttendanceAdapterFilter(this@AttendanceExpandableParentAdapter, listOfAttendanceParent as ArrayList<AttendanceParentAdapterModel>)

    fun refillData(listOfAttendanceParent: MutableList<AttendanceParentAdapterModel>) {
        this.listOfAttendanceParent = listOfAttendanceParent
        notifyDataSetChanged()
    }

    inner class ParentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val companyName: TextView = view.findViewById(R.id.new_attendance_company_name)
        val totalWorkers: TextView = view.findViewById(R.id.new_attendance_number_of_workers)
        val parentConstraintLayout: ConstraintLayout = view.findViewById(R.id.constraintLayout2)
        val expandableArrow: ImageView = view.findViewById(R.id.expandable_arrow)
        val childRecyclerView: RecyclerView = view.findViewById(R.id.child_list)
        val nameTitle: TextView = view.findViewById(R.id.textView33)
        val timeTitle: TextView = view.findViewById(R.id.textView34)
        val alertIcon: ImageView = view.findViewById(R.id.alert_icon)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_parent_attendance, parent, false)
        return ParentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfAttendanceParent.size
    }

    override fun onBindViewHolder(holder: ParentViewHolder, position: Int) {
        holder.companyName.text = listOfAttendanceParent[position].companyName
        holder.totalWorkers.text = listOfAttendanceParent[position].total
        holder.childRecyclerView.addItemDecoration(DividerItemDecoration(holder.childRecyclerView.context, LinearLayoutManager.VERTICAL))
        holder.childRecyclerView.adapter = AttendanceExpandableChildAdapter(listOfAttendanceParent[position].childList as MutableList<AttendanceChildAdapterModel>, onClickChildAction)
        holder.parentConstraintLayout.setBackgroundColor(Color.parseColor("#F7F7F7"))
        when(listOfAttendanceParent[position].attendanceAlertState){
            is AttendanceAlertState.Alert -> holder.alertIcon.setImageResource(R.drawable.ic_danger)
            is AttendanceAlertState.Warning -> holder.alertIcon.setImageResource(R.drawable.ic_warn)
            is AttendanceAlertState.Information -> holder.alertIcon.setImageResource(R.drawable.ic_info)
            is AttendanceAlertState.None -> holder.alertIcon.visibility = View.INVISIBLE
        }

        val isExpanded = listOfAttendanceParent[position].isExpanded
        holder.childRecyclerView.visibility = if (isExpanded) View.VISIBLE else View.GONE
        holder.parentConstraintLayout.setOnClickListener {
            val parentAdapterModel = listOfAttendanceParent[position]
            parentAdapterModel.isExpanded = !parentAdapterModel.isExpanded
            notifyItemChanged(position)
        }

        if (isExpanded) {

            holder.expandableArrow.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp)
            holder.nameTitle.visibility = View.VISIBLE
            holder.timeTitle.visibility = View.VISIBLE
        } else {
            holder.expandableArrow.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp)
            holder.nameTitle.visibility = View.INVISIBLE
            holder.timeTitle.visibility = View.INVISIBLE
        }


    }

    override fun getFilter(): Filter {
        return filter
    }

    class AttendanceAdapterFilter(var adapter: AttendanceExpandableParentAdapter, private var listOfAttendance: ArrayList<AttendanceParentAdapterModel>) : Filter() {
        private var listOfAttendanceParent: MutableList<AttendanceParentAdapterModel> = arrayListOf()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            listOfAttendanceParent.clear()
            val results = FilterResults()
            if (constraint!!.isEmpty()) {
                listOfAttendanceParent.addAll(listOfAttendance)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for (company in listOfAttendance) {
                    if (company.companyName.toLowerCase().startsWith(filterPattern))
                        listOfAttendanceParent.add(company)
                }
            }
            results.values = listOfAttendanceParent
            results.count = listOfAttendanceParent.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapter.refillData(results?.values as ArrayList<AttendanceParentAdapterModel>)
        }
    }
}