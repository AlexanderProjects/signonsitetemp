package au.com.signonsitenew.ui.passport.credentials

import au.com.signonsitenew.domain.models.Credential


interface CredentialsView {
    fun reloadData(credentials: List<Credential?>)
    fun showNetworkErrors()
    fun showDataErrors(errorMessage:String)
}