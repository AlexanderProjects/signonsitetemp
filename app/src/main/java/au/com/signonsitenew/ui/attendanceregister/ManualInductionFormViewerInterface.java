package au.com.signonsitenew.ui.attendanceregister;

import android.app.AlertDialog;
import android.content.Context;
import android.webkit.JavascriptInterface;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.TodaysVisits;

/**
 * Created by kcaldwell on 10/10/17.
 *
 * Interface to map our Forms JS to Android functions.
 *
 * Creating a second one so that it pops the back stack twice after completing a manual induction
 * to ensure that a user is taken out of the induction options menu after manually inducting
 * a user. This is a bit hacky, but a quick work around given that we are using a web form
 * interface with limited callbacks.
 */

public class ManualInductionFormViewerInterface {
    private Context mContext;

    /** Instantiate the interface and set the context */
    ManualInductionFormViewerInterface(Context context) {
        mContext = context;
    }

    /**
     * Called when a manager-level user accepts/rejected an induction through the phone
     */
    @JavascriptInterface
    public void siteInductionSubmitted() {
        // Pop stack twice to get back to user detail view
        TodaysVisits.get(mContext, null, null);
        ((AttendanceActivity)mContext).getSupportFragmentManager().popBackStack();
        ((AttendanceActivity)mContext).getSupportFragmentManager().popBackStack();
    }

    /** Show a pop up with info! */
    @JavascriptInterface
    public void submissionFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.error_generic_title))
                .setMessage(mContext.getString(R.string.induction_form_submit_error_message))
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
