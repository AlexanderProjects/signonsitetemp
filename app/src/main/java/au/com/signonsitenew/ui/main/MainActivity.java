package au.com.signonsitenew.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.TagConstraint;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationListener;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.adapters.MainSectionsPagerAdapter;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.domain.models.CompanyPropertyType;
import au.com.signonsitenew.domain.models.CompanySegmentProperties;
import au.com.signonsitenew.domain.models.NearSite;
import au.com.signonsitenew.domain.models.RequestPermission;
import au.com.signonsitenew.domain.models.SignOnOffType;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.events.StatusEvent;
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.locationengine.signonoffnotifications.SignOnOffDelegate;
import au.com.signonsitenew.locationengine.signonoffnotifications.SignOnOffNotifier;
import au.com.signonsitenew.realm.EnrolledSite;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.ui.navigation.Router;
import au.com.signonsitenew.ui.passport.PassportFragment;
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoFragment;
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuListener;
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuToEmergencyListener;
import au.com.signonsitenew.ui.passport.personal.PersonalFragment;
import au.com.signonsitenew.utilities.AlertDialogMessageHelper;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.LogoutUtil;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import dagger.android.support.DaggerAppCompatActivity;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * MainActivity is the central place that the app will go for any activity that does not occur on
 * site. It houses all of the tabs in this section of the app. Tabs are controlled in
 * MainSectionsPagerAdapter.
 *
 * Refer to SignedOnActivity for screens relating to being signed on. Additional Activities branch
 * off from the Fragments that are contained in the ViewPager.
 */


public class MainActivity extends DaggerAppCompatActivity implements LocationListener, ResultCallback<Status>, SitesFragment.OnListFragmentInteractionListener, MainActivityNavigationListener, MainActivityView {

    protected static final String LOG = MainActivity.class.getSimpleName();
    protected static final int REQUEST_CHECK_SETTINGS = 0x1; // Constant used in location settings dialog
    protected SOSAPI mSoSApi;
    private JobManager mJobManager;
    private SessionManager mSession;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    protected LinearLayout mContentView;
    protected LinearLayout mProgressView;
    protected ImageView mProgressSpinner;
    protected TextView mProgressStatusTextView;
    protected TextView mSignOnInformationTextView;
    protected Location mCurrentLocation;
    protected ArrayList<Geofence> mGeofenceList;
    protected int mTabPosition;
    protected Boolean mNewRegistration = false;
    private Fragment introOrPassportFragment;
    private RealmConfiguration mRealmConfig;
    private SignOnOffNotifier mNotifier;
    private SignOnOffDelegate mNotifierDelegate;
    private MainSectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;
    private BadgeDrawable passportBadge;
    private ViewPager mViewPager;
    private ActionBar actionBar;
    private TextView titleTextView;
    private MainActivityViewPresenterImpl presenter;
    private static MainTabMenuListener mainTabMenuListener;
    private static MainTabMenuToEmergencyListener mainTabMenuToEmergencyListener;
    protected static final int REQUEST_PHONE_STATE = 0;
    protected static final int REQUEST_LOCATION = 1;
    protected static final int REQUEST_IGNORE_BATTERY_OPTIMISATIONS = 2;

    //MANUAL/PROMPTED_SIGNON
    private Boolean PromptRequest;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    AnalyticsEventDelegateService analyticsEventDelegateService;

    @Inject
    Router router;

    @Inject
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new ViewModelProvider(this, viewModelFactory).get(MainActivityViewPresenterImpl.class);
        presenter.inject(this);
        presenter.registerNotificationListener();

        Toolbar toolbar = findViewById(R.id.toolbar_main);
        toolbar.setTitle("");
        titleTextView = toolbar.findViewById(R.id.toolbar_title_text_view);
        titleTextView.setText("Site");
        setSupportActionBar(toolbar);

        PersonalFragment.setMainActivityNavigationListener(this);
        EmergencyInfoFragment.setMainActivityNavigationListener(this);
        mProgressView = findViewById(R.id.progress_linear_layout);
        mProgressSpinner = findViewById(R.id.progress_spinner_image_view);
        mProgressStatusTextView = findViewById(R.id.progress_action_text_view);
        mSignOnInformationTextView = findViewById(R.id.progress_information_text_view);
        mContentView = findViewById(R.id.appbar);
        mTabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.pager);
        showLoadingProgressView();
        actionBar = getSupportActionBar();
        mSession = new SessionManager(getApplicationContext());
        mJobManager = SOSApplication.getInstance().getJobManager();
        mSoSApi = new SOSAPI(this);

        Bundle extras = getIntent().getExtras();
        //MANUAL/PROMPTED_SIGNON: sending extra to invoke a sites fragment method
        if (extras != null) {
            if (extras.getBoolean("PROMPT_REQUEST")) {
                PromptRequest = extras.getBoolean("PROMPT_REQUEST");
            }
            else {
                mNewRegistration = extras.getBoolean("GET_REG_LOC");
            }
            if(extras.getBoolean("NAV_PASSPORT")){
                presenter.setUserJustCreatePassport(extras.getBoolean("NAV_PASSPORT"));
            }

            if(extras.getBoolean(Constants.AUTO_SIGN_ON_OFF_NOTIFICATION))
                analyticsEventDelegateService.notificationClicked(Constants.AUTO_SIGN_OFF,Constants.ANALYTICS_LOCAL_NOTIFICATION, new HashMap(),null);

        }
        // Set default app preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        mRealmConfig = RealmManager.getRealmConfiguration();
        mNotifier = new SignOnOffNotifier(this);
        mNotifierDelegate = new SignOnOffDelegate() {
            @Override
            public void signedOn() {
                SLog.d(LOG, "Got signed on notification!!!");
            }

            @Override
            public void signedOff() {
                SLog.d(LOG, "Got signed off notification!!!");
            }
        };

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(Constants.GCM_SENT_TOKEN_TO_SERVER, false);

                SLog.i(LOG, "GCM sentToken status is: " + sentToken);
            }
        };

        // Check if user is logged in
        if (!mSession.isLoggedIn()) {
            LogoutUtil.logoutUser(this,locationManager);
            return;
        }
        // This method selectes the corrent fragment when the user does not have passpot
        presenter.fragmentPassportSelector(fragment -> {
            configureTabs(fragment);
            introOrPassportFragment = fragment;
            return null;
        },()->{
            configureTabs(new PassportFragment());
            return null;
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.unRegisterFirebaseIdToken();
        presenter.checkForSignedStatus();

        // listen for changes from location engine
        mNotifier.setDelegate(mNotifierDelegate);

        // Check device local status and if device is running the latest app version
        mSoSApi.postPhoneAndAppInfo(null);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.GCM_REGISTRATION_COMPLETE));

        // Remove signon/off notifications if they are displayed
        NotificationUtil.cancelNotifications(this, NotificationUtil.NOTIFICATION_ATTENDANCE_ID);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mNotifier.unsetDelegate();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();

        // Cancel all pending background Jobs and Volley requests
        mJobManager.cancelJobsInBackground(null, TagConstraint.ANY, LOG);
        SOSApplication.getInstance().cancelPendingRequests(LOG);
    }


    private void configureTabs(Fragment fragment) {
        if(mSectionsPagerAdapter == null) {
            mSectionsPagerAdapter = new MainSectionsPagerAdapter(this, getSupportFragmentManager(), PromptRequest, fragment);
        }else {
            mSectionsPagerAdapter.notifyDataSetChanged();
        }
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Don't care
            }

            @Override
            public void onPageSelected(int position) {
                Log.i(LOG, "onPageSelected, position: " + position);
                mTabPosition = position;
                if(mainTabMenuListener != null)
                    mainTabMenuListener.mainMenuTabSelected(position);
                if(mainTabMenuToEmergencyListener != null)
                    mainTabMenuToEmergencyListener.mainMenuTabSelected(position);

                // Set title based on fragment
                switch (position) {
                    // Site tab
                    case 0:
                        titleTextView.setText("Site");
                        break;
                    // Site Docs tab
                    case 1:
                        titleTextView.setText("Passport");
                        break;
                    // Menu
                    case 2:
                        titleTextView.setText("Menu");
                        break;
                }

                // If sites fragment, start GPS
                if (position == 0
                        && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isPrompt", String.valueOf(PromptRequest));
                    Fragment fragobj = new SitesFragment();
                    fragobj.setArguments(bundle);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Don't care
            }
        });
        mViewPager.setOffscreenPageLimit(3);
        mTabLayout.setupWithViewPager(mViewPager);
        passportBadge = mTabLayout.getTabAt(1).getOrCreateBadge();
        passportBadge.setVisible(false);
        // Set up initial tabs
        int selectedTabColour = ContextCompat.getColor(MainActivity.this, R.color.orange_primary);
        int unselectedTabColour = ContextCompat.getColor(MainActivity.this, R.color.white);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            switch (i) {
                case 0:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_location);

                    // Set initial selected colour
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
                case 1:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_licences);
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
                case 2:
                    mTabLayout.getTabAt(i).setIcon(R.drawable.icon_menu);
                    mTabLayout.getTabAt(i).getIcon().setColorFilter(unselectedTabColour, PorterDuff.Mode.SRC_IN);
                    break;
            }
        }

        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int selectedTabColour = ContextCompat.getColor(MainActivity.this, R.color.orange_primary);
                if(tab.getIcon() != null)
                    tab.getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int selectedTabColour = ContextCompat.getColor(MainActivity.this, R.color.white);
                if(tab.getIcon() != null)
                    tab.getIcon().setColorFilter(selectedTabColour, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
        if(presenter.getUserJustCreatePassport())
            mTabLayout.getTabAt(1).select();
        showActivityContentView();
    }

    /**
     * Runs when the result of calling addGeofences() and removeGeofences() becomes available.
     * Either method can complete successfully or with an error.
     *
     * Since this activity implements the {@link ResultCallback} interface, we are required to
     * define this method.
     *
     * @param status The Status returned through a PendingIntent when addGeofences() or
     *               removeGeofences() get called.
     */
    @Override
    public void onResult(Status status) {
        if (status.isSuccess()) {
            SLog.i(LOG, "Monitoring Geofences");
            // Get all the requestIds for the current geofences
            ArrayList<String> geofenceIds = new ArrayList<>();
            for (Geofence geofence : mGeofenceList) {
                geofenceIds.add(geofence.getRequestId());
            }

            // Store the requests
            mSession.setGeofenceIds(geofenceIds);
            SLog.i(LOG, "Geofence IDs stored locally");
        }
        else {
            SLog.e(LOG, status.getStatusMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        SLog.i(LOG, "Location is: " + location.toString());
        mCurrentLocation = location;
        // If the user has just registered an account, log their location
        presenter.checkForSignedStatus();
        if (mNewRegistration) {
            DiagnosticsManager.logEvent(
                    this,
                    DiagnosticLog.Tag.LOC_REGISTER,
                    mCurrentLocation,
                    null);
            mNewRegistration = false;
        }
    }

    /**
     * Called when the user's location settings are incorrect and they are promted to fix them.
     * Posts the phone's settings to our server at completion so we can see what the user chose.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        SLog.i(LOG, "User agreed to make required location settings changes.");
                        break;
                    case Activity.RESULT_CANCELED:
                        SLog.i(LOG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f instanceof PassportFragment) {
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
        mSoSApi.postPhoneAndAppInfo(null);
    }


    /**
     * This method is used to determine the appropriate actiivty that the app should navigate to
     * depending upon the site settings.
     */
    protected void navigateToSignedOnState(Boolean hasEnrolmentLocked) {
        if(hasEnrolmentLocked){
            signOnSite(mCurrentLocation,mSession.getSiteId());
        } else {
            Intent intent = new Intent(this, FirstSignOnActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(Constants.NEAR_SITE, mSession.getNearSite());
            intent.putExtra("location",mCurrentLocation);
            intent.putExtra("siteId", mSession.getSiteId());
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            // Received permission result for Location Request
            SLog.i(LOG, "Response received for Location permission request");
            presenter.sendLocationPermissionResult(new RequestPermission(requestCode,permissions,grantResults));
            configureTabs(introOrPassportFragment);
        }
        else if (requestCode == REQUEST_PHONE_STATE) {
            // Received permission result for Phone State Request
            SLog.i(LOG, "Response received for Read Phone State permission request");

            // Check if the required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SLog.i(LOG, "Read Phone State has been GRANTED");
            }
            else {
                SLog.i(LOG, "Read Phone State permission was DENIED");
            }
        }
        else if (requestCode == REQUEST_IGNORE_BATTERY_OPTIMISATIONS) {
            // Received permission result from Battery Optimisation Request
            SLog.i(LOG, "Response received from Ignore Battery Optimisations request");

            // Check if the required permision has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SLog.i(LOG, "Ignore Battery Optimisations permission has been GRANTED");

                JSONObject jsonData = new JSONObject();
                try {
                    jsonData.put("state", "allow");
                }
                catch (JSONException e) {
                    SLog.e(LOG, "JsonException occurred: " + e.getMessage());
                }
                DiagnosticsManager.logEvent(this, DiagnosticLog.Tag.BATT_OPT_IGNORE_REQUEST, null, jsonData.toString());
            }
            else {
                SLog.i(LOG, "Ignore Battery Optimisations permission was DENIED");

                JSONObject jsonData = new JSONObject();
                try {
                    jsonData.put("state", "hard_deny");
                }
                catch (JSONException e) {
                    SLog.e(LOG, "JsonException occurred: " + e.getMessage());
                }
                DiagnosticsManager.logEvent(this, DiagnosticLog.Tag.BATT_OPT_IGNORE_REQUEST, null, jsonData.toString());
            }
        }
        else if(grantResults[0] == -1 && grantResults[1] == -1){
            presenter.sendLocationPermissionResult(new RequestPermission(requestCode,permissions,grantResults));
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showActivityContentView() {
        if(actionBar != null)
            actionBar.show();
        mProgressView.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.VISIBLE);

    }

    protected void showSiteSearchProgressView() {
        if(actionBar != null)
            actionBar.hide();
        mContentView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
        mSignOnInformationTextView.setVisibility(View.VISIBLE);
        mProgressStatusTextView.setText("Searching for sites...");
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);

    }

    private void showLoadingProgressView() {
        if(actionBar != null)
            actionBar.hide();
        mContentView.setVisibility(View.INVISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
        mSignOnInformationTextView.setVisibility(View.INVISIBLE);
        mProgressStatusTextView.setText(getResources().getString(R.string.loading_plain_title));
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    protected void showSigningOnProgressView() {
        if(actionBar != null)
            actionBar.hide();
        mContentView.setVisibility(View.INVISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
        mSignOnInformationTextView.setVisibility(View.INVISIBLE);
        mProgressStatusTextView.setText("Signing you on...");
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    public void showLoggingOutProgressView() {
        if(actionBar != null)
            actionBar.hide();
        mContentView.setVisibility(View.INVISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
        mSignOnInformationTextView.setVisibility(View.INVISIBLE);
        mProgressStatusTextView.setText("Logging you out...");
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    protected void onCompleteProgressView(){
        if(actionBar != null)
            actionBar.show();
        mTabLayout.setVisibility(View.VISIBLE);
        mContentView.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.GONE);
    }

    @Subscribe
    public void onStatusEvent(StatusEvent event) {
        Log.i(LOG, "Status Event received. " + event.toString());
        if (event.eventType.equals(Constants.EVENT_SIGN_ON) && !isFinishing()) {
            presenter.getCompaniesForSite(String.valueOf(mSession.getSiteId()));
            presenter.checkForSignedStatus();
        }
    }

    /**
     ***********************************************************************************************
     * Fragment Callback functions
     ***********************************************************************************************
     */

    @Override
    public void onSiteSelected(NearSite site) {
        // Sign on site
        Log.i(LOG, "Site: " + site.toString());
        if(!site.is_connected()) {
            AlertDialogMessageHelper.showAlertDialogBeforeSingOn(this, site, () -> {
                mSession.setSiteId(site.getId());
                mSession.setNearSite(site);
                showSigningOnProgressView();
                presenter.getCompaniesForSite(String.valueOf(site.getId()));
                return null;
            });
        }else {
            mSession.setSiteId(site.getId());
            mSession.setNearSite(site);
            showSigningOnProgressView();
            presenter.getCompaniesForSite(String.valueOf(site.getId()));
        }
        analyticsEventDelegateService.signedOn(SignOnOffType.Manual, new CompanySegmentProperties(CompanyPropertyType.Auto,null),!site.is_connected());
    }

    /**
     ***********************************************************************************************
     * API Calls
     ***********************************************************************************************
     */

    private void signOnSite(final Location location, final Integer siteId) {
        mSoSApi.signOnSite(false, siteId, location, result -> {
            SLog.i(LOG, "Result from signon.php received: " + result);

            try {
                JSONObject jsonResult = new JSONObject(result);

                if (jsonResult.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                    SLog.i(LOG, "Manual sign on successful");

                    mSession.setSiteId(siteId);
                    mSession.setSignOnTime();

                    // Add/update induction status to Realm
                    Realm realm = Realm.getInstance(mRealmConfig);
                    EnrolledSite site = realm.where(EnrolledSite.class).equalTo("id", siteId).findFirst();
                    realm.beginTransaction();
                    if (site != null) {
                        site.setInducted(jsonResult.getBoolean("inducted"));
                    } else {
                        // Create a new site with all the fields to maintain the inducted status
                        EnrolledSite newSite = new EnrolledSite();
                        newSite.setId(jsonResult.getLong("site_id"));
                        newSite.setName(jsonResult.getString("site_name"));
                        newSite.setPoints(jsonResult.getString("points"));
                        newSite.setManagerName(jsonResult.getString("address"));
                        newSite.setSiteAddress(jsonResult.getString("company_name"));
                        newSite.setRegionLat(jsonResult.getDouble("lat"));
                        newSite.setRegionLong(jsonResult.getDouble("long"));
                        newSite.setRegionRadius(jsonResult.getDouble("radius"));
                        newSite.setInducted(jsonResult.getBoolean("inducted"));
                        realm.copyToRealmOrUpdate(newSite);
                    }
                    realm.commitTransaction();
                    realm.close();

                    // Re-fetch the users region list as they now have a new site entry potentially
                    RegionFetcherJobService.schedule(MainActivity.this);

                    // Load SignedOnActivity
                    router.navigateToSignedOnActivity(this);
                }

            } catch (JSONException e) {
                SLog.i(LOG, "There was a JSON Exception: " + e.getMessage());
            }
        }, () -> {
            SLog.e(LOG, "There was a volley error");
            showActivityContentView();
        });
    }


    @Override
    public void showProgressView(String text) {
        if(actionBar != null)
            actionBar.hide();
        mContentView.setVisibility(View.INVISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
        mSignOnInformationTextView.setVisibility(View.INVISIBLE);
        mProgressStatusTextView.setText(text);
        mTabLayout.setVisibility(View.GONE);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    @Override
    public void showInternetError() {
        AlertDialogMessageHelper.networkErrorMessage(this);
    }

    public static void setMainTabMenuListener(MainTabMenuListener listener){
        mainTabMenuListener = listener;
    }

    public static void setMenuToEmergencyListener(MainTabMenuToEmergencyListener listener){
        mainTabMenuToEmergencyListener = listener;
    }

    @Override
    public void navigateToTab(int index) { mTabLayout.getTabAt(index).select();}


    @Override
    public void checkIfUserHasCompanyEnrolmentLocked(boolean hasEnrolmentLocked, @NotNull Location currentLocation) {
        mCurrentLocation = currentLocation;
        navigateToSignedOnState(hasEnrolmentLocked);
    }


    @Override
    public void updateNotificationValueForPassport(@org.jetbrains.annotations.Nullable Integer count) {
        if(passportBadge != null) {
            if (count > 0) {
                passportBadge.setVisible(true);
                passportBadge.setNumber(count);
            } else {
                passportBadge.setVisible(false);
                passportBadge.clearNumber();
            }
        }
    }

    @Override
    public void showDataError(@NotNull String message) {
        AlertDialogMessageHelper.dataErrorMessage(this,message);
    }

    @Override
    public void navigateToSignedActivity() {
        router.navigationToSignedActivity(this);
    }

}
