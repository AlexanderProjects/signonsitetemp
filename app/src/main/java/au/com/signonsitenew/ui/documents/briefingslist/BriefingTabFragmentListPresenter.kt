package au.com.signonsitenew.ui.documents.briefingslist

import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.lifecycle.*
import au.com.signonsitenew.domain.models.state.BriefingListState
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCaseImpl
import au.com.signonsitenew.models.BriefingsResponse
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.SLog
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class BriefingTabFragmentListPresenter @Inject constructor(private val briefingsUseCase: BriefingsUseCaseImpl) : BasePresenter() {

    private lateinit var briefingListTabbedView: BriefingListTabbedView
    private val _uiState = MutableLiveData<BriefingListState>()

    private lateinit var observer : Observer<BriefingListState>
    val uiState : LiveData<BriefingListState> = _uiState

    fun inject(briefingListTabbedView: BriefingListTabbedView) {
        this.briefingListTabbedView = briefingListTabbedView
    }

    fun retrieveListOfBriefings() {
        briefingsUseCase.getListOfBriefingsAsync(object :DisposableSingleObserver<BriefingsResponse>(){
            override fun onSuccess(response: BriefingsResponse) {
                if(NetworkErrorValidator.isValidResponse(response)) {
                    briefingsUseCase.saveNewOffSetForBriefings(response)
                    _uiState.postValue(BriefingListState.Success)
                }else
                    _uiState.postValue(BriefingListState.NoData(response))
            }

            override fun onError(e: Throwable) {
                _uiState.postValue(BriefingListState.Error)
                SLog.e(BriefingTabFragmentListPresenter::class.java.name,e.message)
            }

        })
    }

    @BindingAdapter("showListOfBriefings")
    fun ViewGroup.showListOfBriefings(uiState:BriefingListState){
        visibility = if (uiState is BriefingListState.Success)
            View.VISIBLE
        else
            View.GONE
    }

    fun observeStates(){
        observer = Observer<BriefingListState>(function = { uiState ->
            when(uiState){
                is BriefingListState.NoData -> briefingListTabbedView.showError(NetworkErrorValidator.getErrorMessage(uiState.response))
                is BriefingListState.Error -> briefingListTabbedView.showError(Constants.NETWORK_MESSAGE_ERROR)
            }
        })
        uiState.observeForever(observer)
    }

    fun clearOffset() {
        briefingsUseCase.clearOffSetValue()
    }

    override fun onCleared() {
        super.onCleared()
        uiState.removeObserver(observer)
        briefingsUseCase.dispose()
    }

}