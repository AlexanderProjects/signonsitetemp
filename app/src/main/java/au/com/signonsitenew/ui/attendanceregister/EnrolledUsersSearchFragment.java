package au.com.signonsitenew.ui.attendanceregister;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.EnrolledWorkerSearchRecyclerViewAdapter;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.adapters.EnrolledWorkerSearchRecyclerViewAdapter.OnUserSelectedListener;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import co.moonmonkeylabs.realmsearchview.RealmSearchView;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */

public class EnrolledUsersSearchFragment extends Fragment {


    private OnUserSelectedListener mListener;
    private Realm mRealm;
    private EnrolledWorkerSearchRecyclerViewAdapter mAdapter;
    private LinearLayout mAddWorkerLayout;
    private Button mButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getInstance(RealmManager.getRealmConfiguration());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enrolled_users_search, container, false);
        setHasOptionsMenu(true);

        Toolbar toolbar = rootView.findViewById(R.id.enrolled_user_search_toolbar);
        toolbar.setTitle(Constants.SEARCH_FOR_WORKER);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RealmSearchView realmSearchView = rootView.findViewById(R.id.search_view);
        mAddWorkerLayout = rootView.findViewById(R.id.add_worker_layout);
        mButton = rootView.findViewById(R.id.add_worker_button);

        mButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                ((AttendanceActivity)getActivity()).navigateToFragment(new RegisterUserFragment(), true);

            }
        });

        // Set Adapter
        SiteSettingsService settingsService = new SiteSettingsService(mRealm);
        boolean inductionsEnabled = settingsService.siteInductionsEnabled();
        mAdapter = new EnrolledWorkerSearchRecyclerViewAdapter(getActivity(), mRealm, "firstName", inductionsEnabled, mListener, mAddWorkerLayout);
        realmSearchView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUserSelectedListener) {
            mListener = (OnUserSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnUserSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mRealm != null) {
            mRealm.close();
        }
    }
}
