package au.com.signonsitenew.ui.documents.permits.details

import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.PermitInfo

interface PermitDetailsDisplay {
    fun setPermitStateTitle(stateTitle:String)
    fun setPermitHumanId(humanId:String)
    fun hidePermitHumanId()
    fun showPermitHumanId()
    fun setSmallPermitDescription(description:String)
    fun showErrors()
    fun showPermitError()
    fun setPermitDetailsViewPagerAdapter(permit: Permit?, permitInfo: PermitInfo?)
    fun setCtaStateForNewPermit(permitInfo: PermitInfo?)
    fun setCtaStateForExitingPermit(permitInfo: PermitInfo?)
    fun showPermitToolbar()
    fun hidePermitToolbar()
    fun navigateToCurrentPermits()
    fun showCancelAlertDialogForRequestState()
    fun showCancelAlertDialogForInProgressState()
    fun showSaveButton()
    fun hideSaveButton()
    fun showProgressView()
    fun hideProgressView()
}