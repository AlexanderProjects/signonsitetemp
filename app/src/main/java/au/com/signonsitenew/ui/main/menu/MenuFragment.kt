package au.com.signonsitenew.ui.main.menu

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.api.SOSAPI
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.locationengine.LocationManager
import au.com.signonsitenew.ui.account.AccountActivity
import au.com.signonsitenew.ui.account.SettingsActivity
import au.com.signonsitenew.ui.account.TroubleshootActivity
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject

/**
 * The Menu fragment in the SignedOn and SignedOff Activities
 */
class MenuFragment : DaggerFragment(), MenuFragmentDisplay {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var locationManager: LocationManager

    @Inject
    lateinit var notificationUseCase: NotificationUseCase

    private lateinit var presenter: MenuFragmentPresenter
    private lateinit var mSession: SessionManager
    private lateinit var mAPI: SOSAPI
    private lateinit var mInitialsTextView: TextView
    private lateinit var mFirstNameTextView: TextView
    private lateinit var mLastNameTextView: TextView
    private lateinit var mCompanyTextView: TextView
    private lateinit var mAccountCell: LinearLayout
    private lateinit var mSettingsCell: LinearLayout
    private lateinit var mTroubleshootingCell: LinearLayout
    private lateinit var mPrivacyPolicyCell: LinearLayout
    private lateinit var mTermsAndConditionsCell: LinearLayout
    private lateinit var helpCell:LinearLayout
    private lateinit var mLogOutButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSession = SessionManager(activity)
        mAPI = SOSAPI(activity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_menu, container, false)
        presenter = ViewModelProvider(this, viewModelFactory).get(MenuFragmentPresenter::class.java)
        presenter.inject(this)
        presenter.getPersonalInfo()

        mInitialsTextView = rootView.findViewById(R.id.initials_text_view)
        mFirstNameTextView = rootView.findViewById(R.id.first_name_text_view)
        mLastNameTextView = rootView.findViewById(R.id.last_name_text_view)
        mCompanyTextView = rootView.findViewById(R.id.company_text_view)
        mAccountCell = rootView.findViewById(R.id.menu_account_cell)
        mSettingsCell = rootView.findViewById(R.id.menu_app_settings_cell)
        mTroubleshootingCell = rootView.findViewById(R.id.menu_troubleshooting_cell)
        mTermsAndConditionsCell = rootView.findViewById(R.id.menu_terms_and_conditions_cell)
        mPrivacyPolicyCell = rootView.findViewById(R.id.menu_privacy_policy_cell)
        mLogOutButton = rootView.findViewById(R.id.log_out_button)
        helpCell = rootView.findViewById(R.id.menu_help_cell)

        mAccountCell.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View) {
                val intent = Intent(activity, AccountActivity::class.java)
                startActivity(intent)
            }
        })
        mSettingsCell.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View) {
                val intent = Intent(activity, SettingsActivity::class.java)
                startActivity(intent)
            }
        })
        mTroubleshootingCell.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View) {
                val intent = Intent(activity, TroubleshootActivity::class.java)
                startActivity(intent)
            }
        })
        helpCell.setOnClickListener(object :DebouncedOnClickListener(){
            override fun onDebouncedClick(v: View?) {
                presenter.menuHelpOpenedAnalytics()
                val url = Constants.URL_HELP_LINK
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })
        mTermsAndConditionsCell.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View) {
                val url = Constants.URL_TERMS_CONDITIONS
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })
        mPrivacyPolicyCell.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View) {
                val url = Constants.URL_PRIVACY_POLICY
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })
        mLogOutButton.setOnClickListener { createLogOutAlertDialog() }
        return rootView
    }



    private fun signOffSiteThenLogout() {
        val activity: Activity? = activity
        if (activity is MainActivity) {
            activity.showLoggingOutProgressView()
        } else if (activity is SignedOnActivity) {
            activity.showLoggingOutProgressView()
        }
        if (mSession.siteId != 0) { // Sign the user off first
            SLog.i("this is the menu fragment", ": here is signoff call initiated")
            mAPI.signOffSite(false, mSession.siteId, null,
                    { response: String ->
                        try {
                            val jsonResponse = JSONObject(response)
                            if (jsonResponse.getBoolean(Constants.JSON_SIGNED_OFF)) {
                                LogoutUtil.logoutUser(getActivity(),locationManager)
                            } else {
                                showContentView()
                                DarkToast.makeText(getActivity(), "Please sign off site before logging off")
                                SLog.i(LOG, "User attempted to log off but failed to sign off. $response")
                            }
                        } catch (e: JSONException) {
                            showContentView()
                            SLog.e(LOG, "JSON Exception occurred " + e.message)
                        }
                    }) {
                showContentView()
                DarkToast.makeText(getActivity(), "Please sign off site before logging off")
                SLog.i(LOG, "User attempted to log off but failed to sign off.")
            }
        } else {
            LogoutUtil.logoutUser(getActivity(),locationManager)
        }
    }

    private fun showContentView() {
        val activity: Activity? = activity
        if (activity is MainActivity) {
            activity.showActivityContentView()
        } else if (activity is SignedOnActivity) {
            activity.showSignedOnView()
        }
    }

    private fun createLogOutAlertDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        val message = "Are you sure you want to log out?"
        builder.setMessage(message)
                .setTitle("Logout Confirmation")
                .setPositiveButton("Logout"
                ) { _: DialogInterface?, id: Int ->
                    SLog.i(LOG, "User pressed logout")
                    signOffSiteThenLogout()
                }
                .setNegativeButton("Cancel"
                ) { d: DialogInterface, id: Int ->
                    SLog.i(LOG, "User pressed cancel when asked to confirm if they " +
                            "wanted to logout")
                    d.cancel()
                }
        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireActivity(), R.color.orange_primary))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(requireActivity(), R.color.orange_primary))
        }
        dialog.show()
    }

    companion object {
        private val LOG = MenuFragment::class.java.simpleName
    }

    override fun retrieveUserData(user: User) {
        val firstName = user.first_name
        val lastName = user.last_name
        val initials = firstName?.substring(0, 1)?.toUpperCase() + lastName?.substring(0, 1)?.toUpperCase()

        mInitialsTextView.text = initials
        mFirstNameTextView.text = firstName
        mLastNameTextView.text = lastName
        mCompanyTextView.text = user.company_name
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenter.getPersonalInfo() }
    }
}