package au.com.signonsitenew.ui.documents.briefingslist

import au.com.signonsitenew.models.Briefing

interface BriefingDataListener {
    fun updateData(briefingList: List<Briefing>)
}