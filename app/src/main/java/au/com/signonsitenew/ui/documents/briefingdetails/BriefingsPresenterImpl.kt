package au.com.signonsitenew.ui.documents.briefingdetails

import androidx.databinding.ObservableField
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.domain.models.state.BriefingsFragmentState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

interface BriefingsPresenter {
    fun setWorkerBriefing(workerBriefing: WorkerBriefing)
    fun inject(briefingsDisplay: BriefingsDisplay)
    fun hasRichText()
    fun canEditTextRichText() :Boolean
    fun shouldTheAppShowSignature()
    fun needsAcknowledge()
    fun observeStates()
    fun seenBriefing()
    fun acknowledgeBriefing()
}

class BriefingsPresenterImpl @Inject constructor(private val briefingsUseCase: BriefingsUseCaseImpl,
                                                 private val logger: Logger,
                                                 private val analyticsEventService: AnalyticsEventDelegateService) : BasePresenter(), BriefingsPresenter {

    private lateinit var briefingsDisplay: BriefingsDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    val uiState = ObservableField<BriefingsFragmentState>()
    private lateinit var workerBriefing:WorkerBriefing

    override fun setWorkerBriefing(workerBriefing: WorkerBriefing){
        this.workerBriefing = workerBriefing
    }

    override fun inject(briefingsDisplay: BriefingsDisplay){
        this.briefingsDisplay = briefingsDisplay
    }


    override fun hasRichText() = if (briefingsUseCase.isRichText(workerBriefing))
        uiState.set(BriefingsFragmentState.ShowRichTypeText)
    else
        uiState.set(BriefingsFragmentState.ShowPlainTypeText)

    override fun canEditTextRichText() :Boolean {
        return briefingsUseCase.isRichText(workerBriefing)
    }

    override fun shouldTheAppShowSignature() =
            if(workerBriefing.should_show_set_by_user)
                uiState.set(BriefingsFragmentState.ShouldShowSignature(true))
            else
                uiState.set(BriefingsFragmentState.ShouldShowSignature(false))

    override fun needsAcknowledge() =
            if(briefingsUseCase.briefingNeedsAcknowledgement(workerBriefing))
                uiState.set(BriefingsFragmentState.NeedsAcknowledge(true))
            else
                uiState.set(BriefingsFragmentState.NeedsAcknowledge(false))

    override fun observeStates() {
        disposables.add(uiState.addOnPropertyChanged {
            when (it.get()) {
                is BriefingsFragmentState.ShowRichTypeText -> briefingsDisplay.showDataWithRichText(briefingsUseCase.getQuillUrl(), briefingsUseCase.getHeader())
                is BriefingsFragmentState.ShowPlainTypeText -> briefingsDisplay.showDataWithPlainText()
                is BriefingsFragmentState.Error -> briefingsDisplay.showNetworkError()
                is BriefingsFragmentState.ShowProgressLayout -> briefingsDisplay.showProgressLayout()
                is BriefingsFragmentState.HideProgressLayout -> briefingsDisplay.hideProgressLayout()
                is BriefingsFragmentState.DataError -> briefingsDisplay.showDataErrors((it.get() as BriefingsFragmentState.DataError).error)
                is BriefingsFragmentState.NeedsAcknowledge -> if ((it.get() as BriefingsFragmentState.NeedsAcknowledge).isAcknowledge) briefingsDisplay.showAcknowledgeButton() else briefingsDisplay.hideAcknowledgeButton()
                is BriefingsFragmentState.ShouldShowSignature -> if ((it.get() as BriefingsFragmentState.ShouldShowSignature).hasSignature) briefingsDisplay.showSignature() else briefingsDisplay.hideSignature()

            }
        })
    }

    override fun seenBriefing(){
        briefingsUseCase.setSeenBriefing(workerBriefing)
        briefingsUseCase.seenBriefingAsync(object :DisposableSingleObserver<ApiResponse>(){
            override fun onSuccess(response: ApiResponse) {
                logger.i(BriefingsPresenterImpl::class.java.name,attributes = mapOf(this@BriefingsPresenterImpl::seenBriefing.name to toJson(response)))
            }

            override fun onError(error: Throwable) {
                logger.e(BriefingsPresenterImpl::class.java.name,attributes = mapOf(this@BriefingsPresenterImpl::seenBriefing.name to error.message))
            }

        })
    }


    override fun acknowledgeBriefing() {
        uiState.set(BriefingsFragmentState.ShowProgressLayout)
        briefingsUseCase.setAcknowledgeBriefing(workerBriefing)
        briefingsUseCase.acknowledgeBriefingAsync(object :DisposableSingleObserver<ApiResponse>(){
            override fun onSuccess(response: ApiResponse) {
                if (NetworkErrorValidator.isValidResponse(response)) {
                    analyticsEventService.siteBriefingAcknowledged()
                    uiState.set(BriefingsFragmentState.HideProgressLayout)
                }
                else {
                    uiState.set(BriefingsFragmentState.DataError(NetworkErrorValidator.getErrorMessage(response)))
                    logger.w(BriefingsPresenterImpl::class.java.name,attributes = mapOf(this@BriefingsPresenterImpl::acknowledgeBriefing.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                uiState.set(BriefingsFragmentState.Error)
                logger.e(BriefingsPresenterImpl::class.java.name,error, attributes = mapOf(this@BriefingsPresenterImpl::acknowledgeBriefing.name to error.message))
            }

        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        briefingsUseCase.dispose()
    }


}