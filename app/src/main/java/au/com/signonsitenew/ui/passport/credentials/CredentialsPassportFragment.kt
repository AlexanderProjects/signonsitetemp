package au.com.signonsitenew.ui.passport.credentials

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.CredentialsRecyclerViewAdapter
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.models.CredentialsResponse
import au.com.signonsitenew.ui.passport.PassportFragment.Companion.setCredentialsDataListener
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment.CredentialsListLoader
import au.com.signonsitenew.ui.passport.credentials.credentialdetails.CredentialUpdateFragment
import au.com.signonsitenew.ui.passport.credentials.credentialdetails.CredentialsLoader
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NavigationHelper
import au.com.signonsitenew.domain.utilities.NotificationBadgeCounter
import au.com.signonsitenew.utilities.FeatureFlagsManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CredentialsPassportFragment : DaggerFragment(), CredentialsView, CredentialsDataListener, CredentialsLoader, CredentialsListLoader {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager
    private lateinit var recyclerView: RecyclerView
    private var presenterImpl: CredentialsPassportPresenterImpl? = null
    private lateinit var adapter: CredentialsRecyclerViewAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.credentials_passport_fragment, container, false)
        val button: FloatingActionButton = view.findViewById(R.id.add_new_credential)
        button.setOnClickListener { v: View? -> NavigationHelper.navigateToListOfCredentials(activity) }
        recyclerView = view.findViewById(R.id.credential_list)
        return view
    }

    override fun onResume() {
        super.onResume()
        CredentialUpdateFragment.setCredentialsLoader(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setCredentialsDataListener(this)
        CredentialCreationFormFragment.setCredentialsListLoader(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this, viewModelFactory).get(CredentialsPassportPresenterImpl::class.java)
        presenterImpl!!.inject(this)
    }

    override fun reloadData(credentials: List<Credential?>) {
       setupAdapter(credentials)
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenterImpl!!.retrieveListCredential() }
    }

    override fun showDataErrors(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(), errorMessage)
    }

    override fun showData(credentialsResponse: CredentialsResponse?) {
        credentialsResponse?.credentials?.let { setupAdapter(it) }
    }

    private fun navigateToCredentialDetails(credential: Credential?) {
        NavigationHelper.navigateToCredentialUpdate(activity, credential!!)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(featureFlagsManager.hasUserPassportShareEnable)
            menu.findItem(R.id.share_button).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun refreshListOfCredentials() {
        presenterImpl!!.retrieveListCredential()
        adapter!!.notifyDataSetChanged()
    }

    private fun setupAdapter(credentials: List<Credential?>){
        adapter = CredentialsRecyclerViewAdapter(credentials, callAction = { credential -> navigateToCredentialDetails(credential) })
        recyclerView.adapter = adapter
        presenterImpl!!.updateBadgeNotificationNumber(NotificationBadgeCounter.notifierNumberForCredential(credentials as List<Credential>))
    }

    companion object {
        fun newInstance(): CredentialsPassportFragment {
            return CredentialsPassportFragment()
        }
    }
}