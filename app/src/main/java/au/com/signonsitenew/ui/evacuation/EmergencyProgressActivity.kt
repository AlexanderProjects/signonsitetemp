package au.com.signonsitenew.ui.evacuation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.EmergencySectionsPagerAdapter
import au.com.signonsitenew.realm.RealmManager
import au.com.signonsitenew.ui.main.SignedOnActivity
import com.google.android.material.tabs.TabLayout
import dagger.android.support.DaggerAppCompatActivity
import io.realm.RealmConfiguration
import javax.inject.Inject

class EmergencyProgressActivity : DaggerAppCompatActivity(), EmergencyProgressActivityDisplay {
    /**
     * Copyright © 2016 SignOnSite. All rights reserved.
     *
     * The [androidx.viewpager.widget.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [androidx.fragment.app.FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [androidx.fragment.app.FragmentStatePagerAdapter].
     *
     */

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var mSectionsPagerAdapter: EmergencySectionsPagerAdapter? = null
    private var mRealmConfig: RealmConfiguration? = null
    private lateinit var presenter: EmergencyProgressActivityPresenter
    protected var mEmergencyContentView: LinearLayout? = null
    protected var mProgressView: LinearLayout? = null
    protected var mProgressSpinner: ImageView? = null
    protected var mTitleTextView: TextView? = null
    protected var mFinishButton: TextView? = null
    @JvmField
    var mCheckedCountTextView: TextView? = null

    /**
     * The [ViewPager] that will host the section contents.
     */
    private lateinit var mViewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emergency_progress)
        presenter = ViewModelProvider(this, viewModelFactory).get(EmergencyProgressActivityPresenterImpl::class.java)
        presenter.inject(this)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        mEmergencyContentView = findViewById(R.id.emergency_in_progress_content_view)
        mProgressView = findViewById(R.id.emergency_progress_view)
        mProgressSpinner = findViewById(R.id.progress_spinner_image_view)
        mTitleTextView = toolbar.findViewById(R.id.toolbar_title_text_view)
        mFinishButton = toolbar.findViewById<View>(R.id.toolbar_finish_button) as Button
        mCheckedCountTextView = findViewById(R.id.site_log_counter)
        mRealmConfig = RealmManager.getRealmConfiguration()
        mFinishButton!!.setOnClickListener {
            // Set Views
            showEmergencyProgressView()
            presenter.stopEvacuation()
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = EmergencySectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container)
        mViewPager.adapter = mSectionsPagerAdapter
        mViewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                // Reset list data on fragment change
                when (position) {
                    0 -> {
                        val individualsFragment = mViewPager
                                .adapter?.instantiateItem(mViewPager, mViewPager.currentItem) as EmergencyIndividualsLogFragment
                        if (individualsFragment.mAdapter != null) {
                            individualsFragment.setAdapterData()
                        }
                    }
                    1 -> {
                        val companiesFragment = mViewPager
                                .adapter?.instantiateItem(mViewPager, mViewPager.currentItem) as EmergencyCompaniesLogFragment
                        if (companiesFragment.mAdapter != null) {
                            companiesFragment.setAdapterData()
                        }
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout.setupWithViewPager(mViewPager)
    }

    protected fun showEmergencyProgressView() {
        val actionBar = supportActionBar
        actionBar?.hide()
        mEmergencyContentView!!.visibility = View.GONE
        mProgressView!!.visibility = View.VISIBLE
        val animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center)
        mProgressSpinner!!.startAnimation(animation)
    }

    protected fun showEmergencyContentView() {
        val actionBar = supportActionBar
        actionBar?.show()
        mEmergencyContentView!!.visibility = View.VISIBLE
        mProgressView!!.visibility = View.GONE
        mProgressSpinner!!.clearAnimation()
    }

    override fun navigationSignedActivity() {
        // Navigate
        val intent = Intent(this@EmergencyProgressActivity, SignedOnActivity::class.java)
        startActivity(intent)
    }

    override fun showContentView() {
        showEmergencyContentView()
    }
}