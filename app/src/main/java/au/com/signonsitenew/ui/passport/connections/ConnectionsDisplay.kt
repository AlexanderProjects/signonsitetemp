package au.com.signonsitenew.ui.passport.connections

import au.com.signonsitenew.domain.models.Enrolment

interface ConnectionsDisplay {
    fun reloadData(enrolments: List<Enrolment>)
    fun showNetworkErrors()
    fun showDataErrors(messages:String)
}