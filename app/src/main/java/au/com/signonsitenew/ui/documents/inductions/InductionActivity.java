package au.com.signonsitenew.ui.documents.inductions;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import javax.inject.Inject;

import au.com.signonsitenew.R;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.ui.navigation.Router;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import dagger.android.support.DaggerAppCompatActivity;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * InductionActivity is used as a Wizard for collecting additional user information that can
 * be used to pre-fill elements of their induction forms. As the information may be sensitive,
 * filling out these fields is optional.
 *
 * This class handles data flow between fragments.
 */

public class InductionActivity extends DaggerAppCompatActivity {

    private static final String LOG = InductionActivity.class.getSimpleName();
    @Inject
    Router router;

    @Inject
    AnalyticsEventDelegateService analyticsEventDelegateService;

    private Realm mRealm;
    protected Toolbar mToolbar;
    protected TextView mTitleTextView;
    protected Button mNextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_induction);
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        mRealm = Realm.getDefaultInstance();

        mTitleTextView = mToolbar.findViewById(R.id.toolbar_title_text_view);
        mTitleTextView.setText("Site Induction");

        mNextButton = findViewById(R.id.next_button);

        // Add click listeners
        mNextButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                Fragment currentFragment = getCurrentFragment();
                if (currentFragment instanceof InductionSubmittedFragment) {
                    router.navigateToDocumentListFromInduction(InductionActivity.this);
                    finish();
                }
                else {
                    analyticsEventDelegateService.siteInductionFormStarted();
                    loadNextFragment();
                }
            }
        });

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        Fragment fragment = null;

        if (savedInstanceState == null) {
            // If we're being restored from a previous state, then we don't need to do anything and
            // should return or else we could end up with overlapping fragments.
            fragment = new InductionIntroFragment();
        }
        else {
            fragment = new InductionFragment();
        }

        // Don't add to backStack, or else we will get to a blank screen on back input
        fragmentTransaction.add(R.id.fragment_container, fragment).commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof InductionFragment) {
            // Display a dialog first so they don't lose their shit
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Are you sure?")
                    .setMessage("You have not Finished your induction yet! \n\nIf you go back, you " +
                            "will lose any information you have entered.")
                    .setPositiveButton("Go Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which) {
                            // Continue with back
                            loadNextFragment();
                        }
                    })
                    .setNegativeButton("Stay", null);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else if (fragment instanceof InductionSubmittedFragment) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("You can\'t go back to your induction!")
                    .setMessage("A site manager must review your induction before you can review " +
                            "it again.")
                    .setPositiveButton("Ok", null);

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else {
            super.onBackPressed();
        }
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    protected void loadNextFragment() {
        Fragment currentFragment = getCurrentFragment();
        Fragment nextFragment = null;

        if (currentFragment instanceof InductionIntroFragment) {
            nextFragment = new InductionFragment();
        }
        else if (currentFragment instanceof InductionFragment) {
            nextFragment = new InductionSubmittedFragment();
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right);
        fragmentTransaction.replace(R.id.fragment_container, nextFragment);
        fragmentTransaction.addToBackStack("Induction").commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(LOG, "Request Code: " + requestCode);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setTitle("Congratulations! You're ready to go")
                            .setMessage("Click on upload again to get started!")
                            .setPositiveButton("Let\'s do this", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    // User accepted at initial screen - open proper permission dialog
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    DarkToast.makeText(this, "You will need to grant permission to upload photos");
                }
            }
            case 2: {
                Log.i(LOG, "results length: " + grantResults.length);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setTitle("Congratulations! You're ready to go")
                            .setMessage("Click on upload again to get started!")
                            .setPositiveButton("Let\'s do this", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    // User accepted at initial screen - open proper permission dialog
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    DarkToast.makeText(this, "You will need to grant permission to upload photos");
                }
            }
        }
    }
}
