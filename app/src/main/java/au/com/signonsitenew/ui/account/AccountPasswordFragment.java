package au.com.signonsitenew.ui.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.SLog;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountPasswordFragment extends Fragment {

    private static final String LOG = AccountPasswordFragment.class.getSimpleName();

    protected TextView mForgotPasswordTextView;
    protected EditText mOldPasswordEditText;
    protected EditText mNewPasswordEditText;
    protected EditText mConfirmPasswordEditText;
    protected Button mSubmitButton;

    public AccountPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account_password, container, false);

        // Set title
        ((AccountActivity)getActivity()).mTitleTextView.setText("Change Password");

        mForgotPasswordTextView = (TextView)rootView.findViewById(R.id.forgot_password_text_view);
        mOldPasswordEditText = (EditText)rootView.findViewById(R.id.old_password_edit_text);
        mNewPasswordEditText = (EditText)rootView.findViewById(R.id.new_password_edit_text);
        mConfirmPasswordEditText = (EditText)rootView.findViewById(R.id.confirm_password_edit_text);
        mSubmitButton = (Button)rootView.findViewById(R.id.submit_password_button);

        mForgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.URL_FORGOT_PASSWORD;
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validate password
                String oldPassword = mOldPasswordEditText.getText().toString().trim();
                String newPassword = mNewPasswordEditText.getText().toString().trim();
                String confirmPassword = mConfirmPasswordEditText.getText().toString().trim();

                if (oldPassword.isEmpty()) {
                    ((AccountActivity)getActivity()).createAlertDialog("Please enter your current password.");
                }
                else if (newPassword.isEmpty() || confirmPassword.isEmpty()) {
                    ((AccountActivity)getActivity()).createAlertDialog("Please enter your new password and confirm it in the field below.");
                }
                else if (newPassword.length() < 6) {
                    ((AccountActivity)getActivity()).createAlertDialog("Please make sure your password is at least 6 characters or more.");
                }
                else if (!newPassword.equals(confirmPassword)) {
                    ((AccountActivity)getActivity()).createAlertDialog("The new passwords you entered do not match.");
                }
                else {
                    // We good
                    submitPasswordToServer(oldPassword, newPassword, confirmPassword);
                }
            }
        });

        return rootView;
    }

    protected boolean passwordDetailsEntered() {
        String oldPassword = mOldPasswordEditText.getText().toString().trim();
        String newPassword = mNewPasswordEditText.getText().toString().trim();
        String confirmedPassword = mConfirmPasswordEditText.getText().toString().trim();

        return (!oldPassword.isEmpty() && !newPassword.isEmpty() && !confirmedPassword.isEmpty());
    }

    private void submitPasswordToServer(String oldPassword, String newPassword, String confirmPassword) {
        new SOSAPI(getActivity()).changeUserPassword(oldPassword, newPassword, confirmPassword,
                new SOSAPI.VolleySuccessCallback() {
                    @Override
                    public void onResponse(String response) {
                        // Check the response result
                        JSONObject JSONResponse;
                        try {
                            JSONResponse = new JSONObject(response);
                            if (JSONResponse.getBoolean(Constants.JSON_UPDATE_OK) == true) {
                                DarkToast.makeText(getActivity(), "Successfully changed password");
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                            else {
                                // The changing password failed
                                SLog.e(LOG, JSONResponse.getString(Constants.JSON_MESSAGE));
                                ((AccountActivity) getActivity())
                                        .createAlertDialog("There was an issue updating your password. " +
                                                "Please ensure that your old password is correct");
                            }
                        } catch (JSONException e) {
                            SLog.e(LOG, e.getMessage());
                            ((AccountActivity) getActivity()).createAlertDialog(getString(R.string.error_generic_response));
                        }
                    }
                }, new SOSAPI.VolleyErrorCallback() {
                    @Override
                    public void onResponse() {

                        DarkToast.makeText(getActivity(),
                                getString(R.string.error_generic_response));
                    }
                });
    }
}
