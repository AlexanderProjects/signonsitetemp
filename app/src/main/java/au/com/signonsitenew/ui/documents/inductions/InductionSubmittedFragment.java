package au.com.signonsitenew.ui.documents.inductions;

import android.graphics.PorterDuff;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.utilities.NotificationUtil;

/**
 * First fragment of the Site Induction flow.
 */
public class InductionSubmittedFragment extends Fragment {

    private static final String LOG = InductionSubmittedFragment.class.getSimpleName();

    private ImageView mImageView;

    public InductionSubmittedFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Show the toolbar and next button
        ((InductionActivity)getActivity()).mToolbar.setVisibility(View.VISIBLE);
        ((InductionActivity)getActivity()).mNextButton.setVisibility(View.VISIBLE);
        ((InductionActivity)getActivity()).mNextButton.setText("Finish");

        View rootView = inflater.inflate(R.layout.fragment_induction_submitted, container, false);

        mImageView = rootView.findViewById(R.id.image_view);
        mImageView.setColorFilter(ContextCompat.getColor(getActivity(), R.color.grey_icon_highlight), PorterDuff.Mode.SRC_IN);

        // Remove any induction notifications if they remain
        NotificationUtil.cancelNotifications(getActivity(), NotificationUtil.NOTIFICATION_INDUCTION_ID);

        return rootView;
    }

}
