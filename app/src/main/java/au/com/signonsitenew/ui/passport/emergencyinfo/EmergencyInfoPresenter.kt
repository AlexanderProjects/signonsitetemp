package au.com.signonsitenew.ui.passport.emergencyinfo

import au.com.signonsitenew.domain.models.Contact
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.domain.models.state.EmergencyInfoFragmentState
import au.com.signonsitenew.domain.models.state.PersonalInfoFragmentState
import au.com.signonsitenew.domain.usecases.emergency.PassportEmergencyInfoUseCaseImpl
import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.models.*
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface EmergencyInfoPresenter{
    fun showUpdatedEmergencyInfo()
    fun inject(emergencyInfoDisplay: EmergencyInfoDisplay)
    fun validateContact(nameContact: String, phoneContact: String, alpha2: String): Contact?
    fun isMedicalInfoEmpty(information: String?, isEmpty: () -> Unit, isNotEmpty: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position: Int, callAction: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit)
    fun updateBadgeNotificationNumber(counter: Int?)
    fun showSaveButton()
    fun hideSaveButton()
    fun updateEmergencyInfoData(userInfoUpdateRequest: UserInfoUpdateRequest)
    fun clearButtonClicked(value: String)
    fun observeStates()
}

class EmergencyInfoPresenterImpl @Inject constructor(private val sessionManager: SessionManager,
                                                     private val logger: Logger,
                                                     private val emergencyInfoUseCaseImpl: PassportEmergencyInfoUseCaseImpl,
                                                     private val rxBusPassport: RxBusPassport) : BasePresenter(), EmergencyInfoPresenter {

    private lateinit var emergencyInfoDisplay: EmergencyInfoDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<EmergencyInfoFragmentState>()
    var isPrimaryContact = false
    var isSecondaryContact = false

    private fun updateEmergencyInfo(request: UserInfoUpdateRequest) {
        logger.i(this::class.java.name,attributes = mapOf(this::updateEmergencyInfo.name to toJson(request)))
        emergencyInfoUseCaseImpl.setRequest(request)
        disposables.add(emergencyInfoUseCaseImpl.updateAndGetPersonalInfoAsync().subscribe({ response ->
            when (response) {
                is UserInfoResponse -> if (NetworkErrorValidator.isValidResponse(response)) {
                    sessionManager.editingInEmergencyPassport = false
                    emergencyInfoDisplay.showUpdatedData(response.user)
                }
                else -> if (!NetworkErrorValidator.isValidResponse(response)) {
                    emergencyInfoDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@EmergencyInfoPresenterImpl::class.java.name, attributes = mapOf(this@EmergencyInfoPresenterImpl::updateEmergencyInfo.name to toJson(response)))
                }
            }
        },{ error ->
            emergencyInfoDisplay.showNetworkErrors()
            logger.e(this@EmergencyInfoPresenterImpl::class.java.name, attributes = mapOf(this@EmergencyInfoPresenterImpl::updateEmergencyInfo.name to error?.message))
        }))

    }

    override fun showUpdatedEmergencyInfo() {
        disposables.add(emergencyInfoUseCaseImpl.getPersonalInfoAsync().subscribe({ response ->
            if (NetworkErrorValidator.isValidResponse(response))
                emergencyInfoDisplay.showUpdatedData(response.user)
            else {
                emergencyInfoDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                logger.w(this@EmergencyInfoPresenterImpl::class.java.name, attributes = mapOf(this@EmergencyInfoPresenterImpl::showUpdatedEmergencyInfo.name to toJson(response)))
            }
        },{ error ->
            emergencyInfoDisplay.showNetworkErrors()
            logger.e(this@EmergencyInfoPresenterImpl::class.java.name, attributes = mapOf(this@EmergencyInfoPresenterImpl::showUpdatedEmergencyInfo.name to error.message))

        }))
    }

    override fun inject(emergencyInfoDisplay: EmergencyInfoDisplay) {
        this.emergencyInfoDisplay = emergencyInfoDisplay
    }

    override fun validateContact(nameContact: String, phoneContact: String, alpha2: String): Contact? = emergencyInfoUseCaseImpl.validateContact(nameContact, phoneContact, alpha2)
    override fun isMedicalInfoEmpty(information: String?, isEmpty: () -> Unit, isNotEmpty: () -> Unit) = emergencyInfoUseCaseImpl.isEmptyMedicalInformation(information, isEmpty, isNotEmpty)
    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position: Int, callAction: () -> Unit) = emergencyInfoUseCaseImpl.checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position, callAction)
    override fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit) = emergencyInfoUseCaseImpl.checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position, callAction)
    override fun updateBadgeNotificationNumber(counter: Int?) {
        rxBusPassport.send(BadgeNotification(counter, Constants.EMERGENCY_TAB_NOTIFICATION))
    }

    override fun showSaveButton() {
        uiStateObservable.onNext(EmergencyInfoFragmentState.ShowSaveButton)
    }

    override fun hideSaveButton() {
        uiStateObservable.onNext(EmergencyInfoFragmentState.HideSaveButton)
    }

    override fun updateEmergencyInfoData(userInfoUpdateRequest: UserInfoUpdateRequest) {
        uiStateObservable.onNext(EmergencyInfoFragmentState.SaveButtonClicked(userInfoUpdateRequest))
    }

    override fun clearButtonClicked(value: String) {
        uiStateObservable.onNext(EmergencyInfoFragmentState.ClearButtonClicked(value))
    }

    override fun observeStates() {
        var lastButtonState: Any = EmergencyInfoFragmentState.HideSaveButton
        disposable.add(uiStateObservable.subscribe {
            when (it) {
                is EmergencyInfoFragmentState.ClearButtonClicked -> {
                    when (it.value) {
                        String().empty() -> {
                            sessionManager.editingInEmergencyPassport = false
                            emergencyInfoDisplay.hideSaveButton()
                        }
                        Constants.NOT_PROVIDED -> {
                            sessionManager.editingInEmergencyPassport = false
                            emergencyInfoDisplay.hideSaveButton()
                        }
                        else -> {
                            emergencyInfoDisplay.showSaveButton()
                            lastButtonState = PersonalInfoFragmentState.ShowSaveButton
                            sessionManager.editingInEmergencyPassport = true
                        }
                    }
                    when (lastButtonState == PersonalInfoFragmentState.ShowSaveButton) {
                        true -> emergencyInfoDisplay.showSaveButton()
                        false -> emergencyInfoDisplay.hideSaveButton()
                    }
                }
                is EmergencyInfoFragmentState.ShowSaveButton -> {
                    emergencyInfoDisplay.showSaveButton()
                    lastButtonState = it
                }
                is EmergencyInfoFragmentState.HideSaveButton -> {
                    emergencyInfoDisplay.hideSaveButton()
                    lastButtonState = it
                }
                is EmergencyInfoFragmentState.SaveButtonClicked -> updateEmergencyInfo(it.userInfoUpdateRequest)
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposable.dispose()
    }

}