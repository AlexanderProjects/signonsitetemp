package au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers

import au.com.signonsitenew.domain.models.RequesteeUser

interface AddTeamMembersFragmentDisplay {
    fun showUnSelectedMembersList(userList: List<RequesteeUser>)
    fun showError()
}