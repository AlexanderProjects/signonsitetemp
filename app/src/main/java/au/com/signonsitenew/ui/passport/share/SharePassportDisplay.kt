package au.com.signonsitenew.ui.passport.share

interface SharePassportDisplay {
    fun showNetworkErrors()
    fun showConfirmation()
    fun showDataErrors(message:String)
}