package au.com.signonsitenew.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.CredentialType

class TypeOfCredentialsListAdapter(var typeOfCredentials: MutableList<CredentialType>, private val callAction: (CredentialType)->Unit) : RecyclerView.Adapter<TypeOfCredentialsListAdapter.ViewHolder>(), Filterable {

    internal var filter: NewFilter = NewFilter(this@TypeOfCredentialsListAdapter, typeOfCredentials)

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_credential_type, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        typeOfCredentials[position].name.let { viewHolder.credentialTypeName.text = typeOfCredentials[position].name }
        viewHolder.view.setOnClickListener { callAction(typeOfCredentials[position]) }
    }

    override fun getFilter(): Filter { return filter }


    override fun getItemCount(): Int {
        return typeOfCredentials.size
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        internal var credentialTypeName: TextView = view.findViewById(R.id.credential_type_name)

    }

    class NewFilter(var adapter: TypeOfCredentialsListAdapter, private var credentialTypeList: MutableList<CredentialType>): Filter(){

        private var credTypeList :MutableList<CredentialType> = arrayListOf()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            credTypeList.clear()
            val results = FilterResults()
            if (constraint!!.isEmpty()){
                credTypeList.addAll(credentialTypeList)
            }else{
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for (typeCredential in credentialTypeList){
                    if(typeCredential.name!!.toLowerCase().startsWith(filterPattern))
                        credTypeList.add(typeCredential)
                }
            }
            results.values = credTypeList
            results.count = credTypeList.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapter.typeOfCredentials = results!!.values as MutableList<CredentialType>
            adapter.notifyDataSetChanged()
        }

    }

}

