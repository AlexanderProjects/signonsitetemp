package au.com.signonsitenew.ui.passport.personal

import au.com.signonsitenew.events.RxBusPassport
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.domain.models.state.PersonalInfoFragmentState
import au.com.signonsitenew.domain.usecases.personal.PassportPersonalInfoUseCaseImpl
import au.com.signonsitenew.models.*
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface PersonalPresenter {
    fun showUpdatedPersonalInfo()
    fun inject(personalDisplay: PersonalDisplay)
    fun convertDateFormat(selectedDate: String?): String?
    fun updateBadgeNotificationNumber(counter: Int?)
    fun convertDateFormatResponse(selectedDate: String?): String?
    fun validateIndigenousStatus(option: String): Boolean
    fun validateApprenticeStatus(option: String): Boolean
    fun validateEmptyAndNotProvidedValues(value: String, valid: () -> Unit, empty: () -> Unit)
    fun validateEmptyAndNotProvidedValuesForMandatoryFields(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForGender(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForApprentice(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForIndigenous(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForConstructionExperience(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForNativeEnglishSpeaker(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForInterpreterRequired(value: String, valid: () -> Unit)
    fun convertIndigenousStatusToString(status: Boolean): String
    fun convertApprenticeToString(status: Boolean): String
    fun convertHasExperienceToString(status: Boolean): String
    fun convertNativeEnglishToString(status: Boolean): String
    fun convertConfirmationToBoolean(value: String): Boolean
    fun convertNeedsInterpreterToString(status: Boolean): String
    fun createPhoneNumberToBeUpdated(phoneText: String?, alpha2: String?): PhoneNumber
    fun validateGender(gender: String, valid: () -> Unit, invalid: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position: Int, callAction: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position: Int, callAction: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit)
    fun validSpinnerSelection(userValue: Boolean?, isValid: () -> Unit, isInValid: () -> Unit)
    fun showSaveButton()
    fun hideSaveButton()
    fun updatePersonalInfoData(userInfoUpdateRequest: UserInfoUpdateRequest)
    fun clearButtonClicked(value: String)
    fun observeStates()
}

class PersonalPresenterImpl @Inject constructor(private val sessionManager: SessionManager,
                                                private val personalInfoUseCaseImpl: PassportPersonalInfoUseCaseImpl,
                                                private val logger: Logger,
                                                private val rxBusPassport: RxBusPassport) : BasePresenter(), PersonalPresenter {

    private lateinit var personalDisplay: PersonalDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<PersonalInfoFragmentState>()


    private fun updatePersonalInfo(userInfoUpdateRequest: UserInfoUpdateRequest) {
        personalInfoUseCaseImpl.setRequest(userInfoUpdateRequest)
        disposables.add(personalInfoUseCaseImpl.updateAndGetPersonalInfoAsync().subscribe({ response ->
            when (response) {
                is UserInfoResponse -> if (NetworkErrorValidator.isValidResponse(response)) {
                    personalDisplay.showUpdatedData(response.user)
                    sessionManager.editingInPersonalInfoPassport = false
                }
                else -> if (!NetworkErrorValidator.isValidResponse(response)) {
                    personalDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                    logger.w(this@PersonalPresenterImpl::class.java.name, attributes = mapOf(this@PersonalPresenterImpl::updatePersonalInfo.name to toJson(response)))
                }
            }

        }, { error ->
            personalDisplay.showNetworkErrors()
            logger.e(this@PersonalPresenterImpl::class.java.name, attributes = mapOf(this@PersonalPresenterImpl::updatePersonalInfo.name to error.message))
        }))

    }

    override fun showUpdatedPersonalInfo() {
        disposables.add(personalInfoUseCaseImpl.getPersonalInfoAsync().subscribe({ response ->
            if (NetworkErrorValidator.isValidResponse(response))
                personalDisplay.showUpdatedData(response.user)
            else {
                personalDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                logger.w(this@PersonalPresenterImpl::class.java.name, attributes = mapOf(this@PersonalPresenterImpl::showUpdatedPersonalInfo.name to toJson(response)))
            }
        }, { error ->
            personalDisplay.showNetworkErrors()
            logger.e(this@PersonalPresenterImpl::class.java.name, attributes = mapOf(this@PersonalPresenterImpl::showUpdatedPersonalInfo.name to error.message))
        }))
    }

    override fun inject(personalDisplay: PersonalDisplay) {
        this.personalDisplay = personalDisplay
    }

    override fun convertDateFormat(selectedDate: String?): String? = selectedDate?.let { personalInfoUseCaseImpl.convertDateFormat(it) }

    override fun updateBadgeNotificationNumber(counter: Int?) = rxBusPassport.send(BadgeNotification(counter, Constants.PERSONAL_TAB_NOTIFICATION))

    override fun convertDateFormatResponse(selectedDate: String?): String? = personalInfoUseCaseImpl.convertDateFormatResponse(selectedDate)

    override fun validateIndigenousStatus(option: String): Boolean = personalInfoUseCaseImpl.validateIndigenousStatus(option)

    override fun validateApprenticeStatus(option: String): Boolean = personalInfoUseCaseImpl.validateApprenticeStatus(option)

    override fun validateEmptyAndNotProvidedValues(value: String, valid: () -> Unit, empty: () -> Unit) = personalInfoUseCaseImpl.validateEmptyAndNotProvidedValues(value, valid, empty)

    override fun validateEmptyAndNotProvidedValuesForMandatoryFields(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateEmptyAndNotProvidedValuesForMandatoryFields(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForGender(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForGender(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForApprentice(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForApprentice(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForIndigenous(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForIndigenous(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForConstructionExperience(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForConstructionExperience(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForNativeEnglishSpeaker(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForNativeSpeaker(value, valid)

    override fun validateNotProvidedValuesOnSpinnerForInterpreterRequired(value: String, valid: () -> Unit) = personalInfoUseCaseImpl.validateNotProvidedValuesOnSpinnerForInterpreterRequired(value, valid)

    override fun convertIndigenousStatusToString(status: Boolean): String = personalInfoUseCaseImpl.convertIndigenousStatusToString(status)

    override fun convertApprenticeToString(status: Boolean): String = personalInfoUseCaseImpl.convertApprenticeToString(status)

    override fun convertHasExperienceToString(status: Boolean): String = personalInfoUseCaseImpl.convertHasExperienceToString(status)

    override fun convertNativeEnglishToString(status: Boolean) = personalInfoUseCaseImpl.convertNativeEnglishToString(status)

    override fun convertConfirmationToBoolean(value: String): Boolean = personalInfoUseCaseImpl.convertConfirmationToBoolean(value)

    override fun convertNeedsInterpreterToString(status: Boolean): String = personalInfoUseCaseImpl.convertNeedsInterpreterToString(status)

    override fun createPhoneNumberToBeUpdated(phoneText: String?, alpha2: String?): PhoneNumber = personalInfoUseCaseImpl.createPhoneNumberToBeUpdated(phoneText, alpha2)

    override fun validateGender(gender: String, valid: () -> Unit, invalid: () -> Unit) = personalInfoUseCaseImpl.validateGender(gender, valid, invalid)

    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position: Int, callAction: () -> Unit) = personalInfoUseCaseImpl.checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position, callAction)

    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position: Int, callAction: () -> Unit) = personalInfoUseCaseImpl.checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position, callAction)

    override fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit) = personalInfoUseCaseImpl.checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position, callAction)

    override fun validSpinnerSelection(userValue: Boolean?, isValid: () -> Unit, isInValid: () -> Unit) = personalInfoUseCaseImpl.validSpinnerSelection(userValue, isValid, isInValid)

    override fun showSaveButton() {
        uiStateObservable.onNext(PersonalInfoFragmentState.ShowSaveButton)
    }

    override fun hideSaveButton() {
        uiStateObservable.onNext(PersonalInfoFragmentState.HideSaveButton)
    }

    override fun updatePersonalInfoData(userInfoUpdateRequest: UserInfoUpdateRequest) {
        uiStateObservable.onNext(PersonalInfoFragmentState.SaveButtonClicked(userInfoUpdateRequest))
    }

    override fun clearButtonClicked(value: String) {
        uiStateObservable.onNext(PersonalInfoFragmentState.ClearButtonClicked(value))
    }

    override fun observeStates() {
        var lastButtonState: Any = PersonalInfoFragmentState.HideSaveButton
        disposable.add(uiStateObservable.subscribe {
            when (it) {
                is PersonalInfoFragmentState.ClearButtonClicked -> {
                    when (it.value) {
                        String().empty() -> {
                            sessionManager.editingInPersonalInfoPassport = false
                            personalDisplay.hideSaveButton()
                        }
                        Constants.NOT_PROVIDED -> {
                            sessionManager.editingInPersonalInfoPassport = false
                            personalDisplay.hideSaveButton()
                        }
                        else -> {
                            personalDisplay.showSaveButton()
                            lastButtonState = PersonalInfoFragmentState.ShowSaveButton
                            sessionManager.editingInPersonalInfoPassport = true
                        }
                    }
                    when (lastButtonState == PersonalInfoFragmentState.ShowSaveButton) {
                        true -> personalDisplay.showSaveButton()
                        false -> personalDisplay.hideSaveButton()
                    }
                }
                is PersonalInfoFragmentState.ShowSaveButton -> {
                    personalDisplay.showSaveButton()
                    lastButtonState = it
                }
                is PersonalInfoFragmentState.HideSaveButton -> {
                    personalDisplay.hideSaveButton()
                    lastButtonState = it
                }
                is PersonalInfoFragmentState.SaveButtonClicked -> updatePersonalInfo(it.userInfoUpdateRequest)
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposable.dispose()
    }


}

