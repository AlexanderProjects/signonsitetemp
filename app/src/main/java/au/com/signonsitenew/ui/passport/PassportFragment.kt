package au.com.signonsitenew.ui.passport

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.PassportPagerAdapter
import au.com.signonsitenew.databinding.PassportFragmentBinding
import au.com.signonsitenew.domain.models.CredentialsResponse
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.ui.passport.credentials.CredentialsDataListener
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoDataListener
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoFragment
import au.com.signonsitenew.ui.passport.listeners.PassportTabMenuListener
import au.com.signonsitenew.ui.passport.listeners.PassportTabMenuToEmergencyListener
import au.com.signonsitenew.ui.passport.personal.PersonalDataListener
import au.com.signonsitenew.ui.passport.personal.PersonalFragment
import au.com.signonsitenew.utilities.*
import au.com.signonsitenew.utilities.imagepicker.SoSImagePicker
import com.bumptech.glide.Glide
import com.google.android.material.badge.BadgeDrawable
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class PassportFragment : DaggerFragment(), PassportView, PassportNavigationListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var sessionManager: SessionManager
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager

    private lateinit var passportFragmentBinding: PassportFragmentBinding
    private lateinit var checkLocationPermission: ActivityResultLauncher<Array<String>>
    private val binding get() = passportFragmentBinding
    private lateinit var presenter: PassportPresenter
    private lateinit var badgePersonal: BadgeDrawable
    private lateinit var badgeEmergency: BadgeDrawable
    private lateinit var badgeCredentials: BadgeDrawable
    private lateinit var badgeConnections: BadgeDrawable
    private lateinit var imagePicker:SoSImagePicker
    private lateinit var photoUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkLocationPermission = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ permissions ->
            if (permissions[Manifest.permission.CAMERA] == true ||
                permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true) {
                cameraHandler()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        passportFragmentBinding = PassportFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        presenter= ViewModelProvider(this, viewModelFactory).get(PassportPresenterImpl::class.java)
        presenter.inject(this)
        presenter.retrieveData()
        presenter.registerNotificationListener()
        binding.profilePhotoPassportImageView.setOnClickListener {
            PermissionManager.checkForCameraPermissionsInPassportProfilePhoto(activity, checkLocationPermission){ cameraHandler() }
        }
        binding.containerPassport.offscreenPageLimit = 3
        binding.containerPassport.addOnPageChangeListener(
            ViewPagerOnPageListener(
                { position: Int? ->
                    passportTabMenuListener.passportMenuTabSelected(
                        position!!
                    )
                },
                { position: Int? ->
                    passportTabMenuToEmergencyListener.passportMenuTabSelected(
                        position!!
                    )
                })
        )
        val passportPagerAdapter = PassportPagerAdapter(childFragmentManager)
        binding.containerPassport.adapter = passportPagerAdapter
        binding.passportTabsLayout.setupWithViewPager(binding.containerPassport)
        badgePersonal = binding.passportTabsLayout.getTabAt(0)!!.orCreateBadge
        badgeEmergency = binding.passportTabsLayout.getTabAt(1)!!.orCreateBadge
        badgeCredentials = binding.passportTabsLayout.getTabAt(2)!!.orCreateBadge
        badgeConnections = binding.passportTabsLayout.getTabAt(3)!!.orCreateBadge
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbarPassport)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        setHasOptionsMenu(true)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        imagePicker = activity?.let {
            SoSImagePicker(
                it,
                this,
                object : au.com.signonsitenew.utilities.imagepicker.OnImagePickedListener {
                    override fun onImagePicked(imageUri: Uri?) {
                        presenter.uploadHeadShotPhoto(
                            ImageUtil.getScaledBitmap(
                                requireContext(),
                                imageUri,
                                Constants.IMAGE_MEDIUM,
                                false
                            )
                        )
                    }
                }).setWithImageCrop(1, 1)
        }!!
        PersonalFragment.setPassportNavigationListener(this)
        EmergencyInfoFragment.setPassportNavigationListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }


    @SuppressLint("SetTextI18n")
    override fun loadUserInfoData(user: User?) {
        if (user!!.last_name != null && user.first_name != null) { binding.name.text = user.first_name + " " + user.last_name }
        user.company_name?.let { binding.companyName.text = user.company_name }
        Glide.with(requireActivity()).load(user.headshot_photo_temporary_url).centerCrop().placeholder(
            R.drawable.image_blur_placeholder
        ).into(binding.profilePhotoPassportImageView)
        personalDataListener.showData(user)
        emergencyInfoDataListener.showData(user)
    }

    override fun loadCredentialsData(credentialsResponse: CredentialsResponse?) {
        credentialsDataListener.showData(credentialsResponse)
    }

    override fun remoteFailure() {
        AlertDialogMessageHelper.networkErrorMessage(
            activity,
            Constants.NETWORK_MESSAGE_ERROR,
            Constants.NETWORK_MESSAGE_TITLE
        ) { presenter.retrieveData() }
    }

    override fun dataError(errorMessage: String) {
        context?.let { AlertDialogMessageHelper.dataErrorMessage(it, errorMessage) }
    }

    override fun showHeadShotPhoto(url: String) {
        Glide.with(requireActivity()).load(url).centerCrop().placeholder(R.drawable.image_blur_placeholder).into(
            binding.profilePhotoPassportImageView
        )
    }

    override fun updateNotificationValueForPersonalInfo(count: Int?) {
        if (count!! > 0) {
            badgePersonal.isVisible = true
            badgePersonal.number = count
        } else badgePersonal.isVisible = false
    }

    override fun updateNotificationValueForEmergencyInfo(count: Int?) {
        if (count!! > 0) {
            badgeEmergency.isVisible = true
            badgeEmergency.number = count
        } else badgeEmergency.isVisible = false
    }

    override fun updateNotificationValueForCredentials(count: Int?) {
        if (count!! > 0) {
            badgeCredentials.isVisible = true
            badgeCredentials.number = count
        } else badgeCredentials.isVisible = false
    }

    override fun updateNotificationValueForConnections(count: Int?) {
        if (count!! > 0){
            badgeConnections.isVisible = true
            badgeConnections.number = count
        } else badgeConnections.isVisible = false
    }

    override fun navigateToTab(index: Int?) {
        binding.passportTabsLayout.getTabAt(index!!)!!.select()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
            inflater.inflate(R.menu.menu_share, menu)
            super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            when(item.itemId){
                R.id.share_button -> {
                    item.isVisible = true
                    router.navigateToShareFragment(requireActivity())
                }
            }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        imagePicker.handlePermission(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            imagePicker.handleActivityResult(resultCode, requestCode, data)
        }else {
            if(::photoUri.isInitialized){
                presenter.uploadHeadShotPhoto(
                    ImageUtil.getScaledBitmap(
                        requireContext(),
                        photoUri,
                        Constants.IMAGE_MEDIUM,
                        true
                    )
                )
            }

        }
    }

    private fun cameraHandler(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            startImagePicker()
        }else {
            startCamera(Constants.IMAGE_CAPTURE_FRONT_FLAG_CODE)
        }
    }

    private fun startImagePicker() {
        imagePicker.choosePicture(true)
    }
    private fun startCamera(captureFlag: Int) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        photoUri = ImageUtil.getOutputPhotoFileUri(
            requireContext(),
            Constants.PROFILE_FILE_PHOTO,
            true
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
        startActivityForResult(intent, captureFlag)
    }

    companion object {
        private lateinit var personalDataListener: PersonalDataListener
        private lateinit var emergencyInfoDataListener: EmergencyInfoDataListener
        private lateinit var credentialsDataListener: CredentialsDataListener
        private lateinit var passportTabMenuListener: PassportTabMenuListener
        private lateinit var passportTabMenuToEmergencyListener: PassportTabMenuToEmergencyListener

        fun newInstance(): PassportFragment {
            return PassportFragment()
        }

        @JvmStatic
        fun setPersonalDataListener(dataListener: PersonalDataListener) {
            personalDataListener = dataListener
        }

        @JvmStatic
        fun setEmergencyInfoDataListener(dataListener: EmergencyInfoDataListener) {
            emergencyInfoDataListener = dataListener
        }

        @JvmStatic
        fun setPassportTabMenuListener(tabMenuListener: PassportTabMenuListener) {
            passportTabMenuListener = tabMenuListener
        }

        @JvmStatic
        fun setPassportTabMenuToEmergencyListener(listener: PassportTabMenuToEmergencyListener) {
                passportTabMenuToEmergencyListener = listener
        }

        @JvmStatic
        fun setCredentialsDataListener(listener: CredentialsDataListener) {
            credentialsDataListener = listener
        }
    }
}

