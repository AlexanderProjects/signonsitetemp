package au.com.signonsitenew.ui.main

import androidx.fragment.app.Fragment
import au.com.signonsitenew.domain.models.CompaniesForSiteResponse
import au.com.signonsitenew.domain.models.RequestPermission
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.location.LocationServiceUseCase
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCase
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl
import au.com.signonsitenew.domain.usecases.siteinformation.GetCompaniesForSiteUseCaseImpl
import au.com.signonsitenew.events.RxBusLocationPermissionResult
import au.com.signonsitenew.events.RxBusMainActivity
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface MainActivityViewPresenter {
    fun inject(mainActivityView: MainActivityView)
    fun registerNotificationListener()
    fun getCompaniesForSite(siteId: String)
    fun checkForSignedStatus()
    fun unRegisterFirebaseIdToken()
    fun sendLocationPermissionResult(requestPermission: RequestPermission)
    fun fragmentPassportSelector(callback: (Fragment) -> Unit, userHasPassportCallback:() -> Unit)
}

class MainActivityViewPresenterImpl @Inject constructor(private val rxBusMainActivity: RxBusMainActivity,
                                                        private val rxBusLocationPermissionResult: RxBusLocationPermissionResult,
                                                        private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl,
                                                        private val introPassportUseCase: IntroPassportUseCase,
                                                        private val repository: DataRepository,
                                                        private val sessionManager: SessionManager,
                                                        private val locationServiceUseCase: LocationServiceUseCase,
                                                        private val signOnStatusUseCaseImpl: SignOnStatusUseCaseImpl,
                                                        private val logger:Logger,
                                                        private val companiesForSiteUseCaseImpl: GetCompaniesForSiteUseCaseImpl) : BasePresenter(), MainActivityViewPresenter {

    private lateinit var mainActivityView: MainActivityView
    private val disposables = CompositeDisposable()
    var userJustCreatePassport = false

    override fun inject(mainActivityView: MainActivityView) {
        this.mainActivityView = mainActivityView
    }

    override fun registerNotificationListener() {
        disposables.add(rxBusMainActivity.toObservable()
                .subscribe { notification: Int? -> mainActivityView.updateNotificationValueForPassport(notification) })
    }

    override fun getCompaniesForSite(siteId: String) {
        disposables.add(internetConnectionUseCaseImpl.isInternetConnected()
                .flatMap {
                    if (it) return@flatMap companiesForSiteUseCaseImpl.retrieveCompaniesForSite(siteId)
                    else return@flatMap io.reactivex.Single.just(mainActivityView.showInternetError())
                }
                .subscribe({ response: Any? ->
                    if (response is CompaniesForSiteResponse) {
                        if (NetworkErrorValidator.isValidResponse(response)) {
                            locationServiceUseCase.getCurrentLocation {
                                if (!response.has_locked_enrolment_for_site && response.companies.isNotEmpty()) {
                                    companiesForSiteUseCaseImpl.saveCompaniesInSiteInDB(response.companies)
                                    mainActivityView.checkIfUserHasCompanyEnrolmentLocked(response.has_locked_enrolment_for_site,it)
                                } else {
                                    mainActivityView.checkIfUserHasCompanyEnrolmentLocked(true,it)
                                }
                                locationServiceUseCase.cancelRequestingLocation()
                            }
                        }else{
                            logger.w(this::class.java.name,attributes = mapOf(this::getCompaniesForSite.name to toJson(response)))
                        }
                    }
                })
                { error ->
                    logger.e(this::class.java.name,attributes = mapOf(this::getCompaniesForSite.name to error.message))
                })
    }

    override fun checkForSignedStatus() {
        disposables.add(signOnStatusUseCaseImpl.checkForUserStatus()
                .subscribe({
                    if (it["signed_on"] as Boolean)
                        mainActivityView.navigateToSignedActivity()
                }, { error: Throwable ->
                    logger.e(this::class.java.name,error,attributes = mapOf(this::checkForSignedStatus.name to error.message))
                }))
    }

    override fun fragmentPassportSelector(callback: (Fragment) -> Unit, userHasPassportCallback:() -> Unit) {
        if(!sessionManager.hasUserPassport) {
            disposables.add(introPassportUseCase.validateFirstTimeUser()
                    .subscribe({
                        if (it.status == Constants.JSON_SUCCESS) {
                            val fragment = introPassportUseCase.validateFragment(it.user.has_passport)
                            sessionManager.hasUserPassport = it.user.has_passport
                            callback(fragment)
                        }
                    }, { error ->
                        logger.e(this::class.java.name, error, attributes = mapOf(this::fragmentPassportSelector.name to error.message))
                    }))
        }else{
            userHasPassportCallback()
        }
    }

    override fun unRegisterFirebaseIdToken() = repository.unRegisterInstanceId()
    override fun sendLocationPermissionResult(requestPermission: RequestPermission) = rxBusLocationPermissionResult.sendLocationPermissionResult(requestPermission)
    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}