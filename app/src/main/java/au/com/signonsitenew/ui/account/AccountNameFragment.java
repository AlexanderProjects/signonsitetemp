package au.com.signonsitenew.ui.account;

import android.content.Context;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.NetworkUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import au.com.signonsitenew.utilities.ToastUtil;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountNameFragment extends Fragment {

    private static final String LOG = AccountNameFragment.class.getSimpleName();

    private SessionManager mSession;
    private SOSAPI mAPI;
    private RealmConfiguration mRealmConfig;

    private String mCurrentFirstName;
    private String mCurrentLastName;

    protected EditText mFirstName;
    protected EditText mLastName;
    protected Button mSubmitButton;

    public AccountNameFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account_name, container, false);

        mSession = new SessionManager(getActivity());
        mAPI = new SOSAPI(getActivity());
        mRealmConfig = RealmManager.getRealmConfiguration();

        // Set title
        ((AccountActivity)getActivity()).mTitleTextView.setText("Change Name");

        // Map the views
        mFirstName = (EditText)rootView.findViewById(R.id.first_name_edit_text);
        mLastName = (EditText)rootView.findViewById(R.id.last_name_edit_text);
        mSubmitButton = (Button)rootView.findViewById(R.id.submit_name_button);

        // Pre-populate name fields
        Realm realm = Realm.getInstance(mRealmConfig);
        User user = realm.where(User.class).findFirst();
        if (user == null) {
            // Realm user not stored yet, get details from mSession
            HashMap<String, String> crapUser = mSession.getCurrentUser();
            mFirstName.setText(crapUser.get(Constants.USER_FIRST_NAME));
            mLastName.setText(crapUser.get(Constants.USER_LAST_NAME));
        }
        else {
            mFirstName.setText(user.getFirstName());
            mLastName.setText(user.getLastName());
        }
        realm.close();
        mCurrentFirstName = mFirstName.getText().toString();
        mCurrentLastName = mLastName.getText().toString();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstName = mFirstName.getText().toString().trim();
                String lastName = mLastName.getText().toString().trim();

                if (firstName.isEmpty()) {
                    ((AccountActivity)getActivity()).createAlertDialog("You must enter a first name.");
                }
                else if (lastName.isEmpty()) {
                    ((AccountActivity)getActivity()).createAlertDialog("You must enter a last name.");
                }
                else if (!firstName.matches("[a-zA-Z' -]+") || !lastName.matches("[a-zA-Z' -]+")) {
                    ((AccountActivity)getActivity()).createAlertDialog("Please make sure your name contains only letters.");
                }
                else {
                    // We're good! Just make sure the names are properly formatted
                    WordUtils.capitalize(firstName);
                    WordUtils.capitalize(lastName);

                    submitNameToServer(firstName, lastName);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected boolean nameDetailsChanged() {
        // Check if they have attempted to change details - they may not realised they haven't saved them.
        return (!mCurrentFirstName.equals(mFirstName.getText().toString().trim())
                || !mCurrentLastName.equals(mLastName.getText().toString().trim()));
    }

    private void submitNameToServer(final String firstName, final String lastName) {
        // Submit details to server
        HashMap<String, String> user = mSession.getCurrentUser();
        final String userId = user.get(Constants.USER_ID);
        String email = user.get(Constants.USER_EMAIL);
        String phoneNumber = user.get(Constants.USER_PHONE);
        final String countryCode = NetworkUtil.determineCountryCode(getActivity());

        mAPI.editPersonalDetails(email, firstName, lastName, phoneNumber, countryCode,
                new SOSAPI.VolleySuccessCallback() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonResponse;
                        try {
                            jsonResponse = new JSONObject(response);
                            if (jsonResponse.getBoolean("update_ok")) {
                                // Name updated successfully, update details locally and navigate
                                mSession.updateUserDetail(Constants.USER_FIRST_NAME, firstName);
                                mSession.updateUserDetail(Constants.USER_LAST_NAME, lastName);

                                // Realm user update
                                Realm realm = Realm.getInstance(mRealmConfig);
                                User user = realm.where(User.class).findFirst();
                                if (user != null) {
                                    realm.beginTransaction();
                                    user.setFirstName(firstName);
                                    user.setLastName(lastName);
                                    realm.copyToRealmOrUpdate(user);
                                    realm.commitTransaction();
                                }
                                realm.close();

                                // Navigate
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                            else {
                                ToastUtil.displayGenericErrorToast(getActivity());
                            }
                        }
                        catch (JSONException e) {
                            SLog.e(LOG, "A JSON Exception occurred");
                        }
                    }
                },
                new SOSAPI.VolleyErrorCallback() {
                    @Override
                    public void onResponse() {
                        ToastUtil.displayGenericErrorToast(getActivity());
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i(LOG, "Change name back");
                // Check if they have attempted to change details - they may not realised they haven't saved them.
                if (!mCurrentFirstName.equals(mFirstName.getText().toString().trim())
                        || !mCurrentLastName.equals(mLastName.getText().toString().trim())) {
                    ((AccountActivity)getActivity()).displayDetailsChangedDialog();
                }
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
        return true;
    }
}
