package au.com.signonsitenew.ui.passport.personal

import au.com.signonsitenew.domain.models.User

interface PersonalDisplay {
    fun showNetworkErrors()
    fun showDataErrors(error:String)
    fun showUpdatedData(user: User?)
    fun hideSaveButton()
    fun showSaveButton()
}