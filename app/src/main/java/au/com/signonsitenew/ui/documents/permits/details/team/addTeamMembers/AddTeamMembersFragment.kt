package au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentAddTeamMembersBinding
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.ui.documents.permits.details.team.adapters.AddTeamMembersListAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.QueryTextListener
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class AddTeamMembersFragment : DaggerFragment(), AddTeamMembersFragmentDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var router: Router

    private var _binding:FragmentAddTeamMembersBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter : AddTeamMembersListAdapter
    private lateinit var presenter: AddTeamMemberFragmentPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ViewModelProvider(this,viewModelFactory).get(AddTeamMemberFragmentPresenterImpl::class.java)
        presenter.inject(this)
        presenter.observeUnSelectedMembers()
        presenter.observeEnrolledUsers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentAddTeamMembersBinding.inflate(inflater, container, false)
        val rootView = if(view != null) view  else binding.root
        binding.addTeamMembersToolbar.title = resources.getString(R.string.add_team_members)
        presenter.getEnrolledUsers()
        (requireActivity() as DaggerAppCompatActivity).setSupportActionBar(binding.addTeamMembersToolbar)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        return rootView!!
    }

    override fun showUnSelectedMembersList(userList: List<RequesteeUser>) {
        presenter.saveUnSelectedTeamMembers(userList)
        adapter = AddTeamMembersListAdapter(presenter.retrieveUnSelectedMemberList().toMutableList(),{ selectedListOfMembers ->
            presenter.saveSelectedTeamMembers(selectedListOfMembers)
            presenter.removeUnSelectedListOfMembers(selectedListOfMembers)
            presenter.sendSelectedUsers(presenter.retrieveSelectedMemberList())
        },{
            presenter.removeSelectedUser(it)
        })
        binding.addTeamMemberRecyclerview.adapter = adapter
        adapter.listOfMembers = presenter.retrieveUnSelectedMemberList().toMutableList()
        adapter.notifyDataSetChanged()
    }

    override fun showError() {
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) { presenter.getEnrolledUsers() }
    }

    companion object{
        @JvmStatic
        fun newInstance() = AddTeamMembersFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search_attendance_register, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(QueryTextListener(addData = { text->
            adapter.getFilter().filter(text)
        }))
    }
}