package au.com.signonsitenew.ui.passport.listeners

interface MainTabMenuToEmergencyListener {
    fun mainMenuTabSelected(position: Int)
}