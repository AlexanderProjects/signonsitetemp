package au.com.signonsitenew.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.SiteRecyclerViewAdapter
import au.com.signonsitenew.api.SOSAPI
import au.com.signonsitenew.domain.models.NearSite
import au.com.signonsitenew.models.Site
import au.com.signonsitenew.ui.main.SitesFragment.OnListFragmentInteractionListener
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Fragment for searching and displaying a list of sites close to the user.
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
class SitesFragment : DaggerFragment(), android.location.LocationListener, SitesFragmentDisplay{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var  presenter: SitesFragmentPresenterImpl
    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager
    @Inject
    lateinit var router: Router
    private val requestLocation = 1
    private val permissionGranted = 0
    private var mNewUser: Boolean = false
    private var mSearchForSites: Boolean? = false
    private var fromPrompt: String = "false"
    private var mAPI: SOSAPI? = null
    private val mSites = ArrayList<Site>()
    private var mListener: OnListFragmentInteractionListener? = null
    private var mAdapter: SiteRecyclerViewAdapter? = null
    private var mCurrentLocation: Location? = null
    private var mAccuracy: Float = 0f
    private var mActivityContainerView: LinearLayout? = null
    private var mProgressView: LinearLayout? = null
    private var mProgressTextView: TextView? = null
    private lateinit var mRecycleView: RecyclerView
    private lateinit var mSiteListView: LinearLayout
    private lateinit var mPreSearchView: LinearLayout
    private lateinit var mChooseSiteTextView: TextView
    private lateinit var mInformationImageView: ImageView
    private lateinit var mInformationTextView: TextView
    private lateinit var mSearchButton: Button
    private lateinit var mRefreshButton: Button
    private lateinit var mProgressSpinner: ImageView
    private var locationPermissions = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    )
    private val backgroundLocationPermission = arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
    private var locationPermissionsAndroidTen = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Check if new user
        mNewUser = (activity as MainActivity).mNewRegistration
        mAPI = SOSAPI(activity)
        presenter= ViewModelProvider(this, viewModelFactory).get(SitesFragmentPresenterImpl::class.java)
        presenter.inject(this)
        presenter.observeForPermissionLocationResult()
        val locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == permissionGranted && checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == permissionGranted) {
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 5000, 100f, this)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_site_list, container, false)
        mActivityContainerView = requireActivity().findViewById(R.id.appbar)
        mProgressView = requireActivity().findViewById(R.id.progress_linear_layout)
        mProgressSpinner = requireActivity().findViewById(R.id.progress_spinner_image_view)
        mProgressTextView = requireActivity().findViewById(R.id.progress_action_text_view)
        mRecycleView = rootView.findViewById(R.id.site_list)
        mSiteListView = rootView.findViewById(R.id.site_linear_layout)
        mPreSearchView = rootView.findViewById(R.id.pre_search_linear_layout)
        mChooseSiteTextView = rootView.findViewById(R.id.choose_your_site_text_view)
        mInformationImageView = rootView.findViewById(R.id.sites_information_image_view)
        mInformationTextView = rootView.findViewById(R.id.sites_information_text_view)
        mSearchButton = rootView.findViewById(R.id.sites_button)
        mRefreshButton = rootView.findViewById(R.id.refetch_sites_button)

        //MANUAL/PROMPTED_SIGNON
        val bundle = this.arguments
        if (bundle !=null) fromPrompt = bundle.getString("prompt").toString()

        mSearchButton.setOnClickListener(object : DebouncedOnClickListener() {
            @SuppressLint("WrongConstant")
            override fun onDebouncedClick(v: View) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Request Location permissions
                        displayLocationPermissionInfoDialog()

                    }
                } else {
                    if (checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Request Location permissions
                        displayLocationPermissionInfoDialog()
                    }
                }
                (activity as MainActivity).showSiteSearchProgressView()
                searchForSites()
            }
        })

        mRefreshButton.setOnClickListener(object : DebouncedOnClickListener() {
            @SuppressLint("WrongConstant")
            override fun onDebouncedClick(v: View) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(requireContext(), Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Request Location permissions
                        displayLocationPermissionInfoDialog()
                    } else {
                        (activity as MainActivity).showSiteSearchProgressView()
                        // Start searching for sites
                        mPreSearchView.visibility = View.GONE
                    }
                } else {
                    if (checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Request Location permissions
                        displayLocationPermissionInfoDialog()
                    } else {
                        (activity as MainActivity).showSiteSearchProgressView()
                        // Start searching for sites
                        mPreSearchView.visibility = View.GONE
                    }
                }
                searchForSites()
            }
        })

        // Set the icon's colour
        mInformationImageView.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.grey_icon_highlight), PorterDuff.Mode.SRC_IN)

        if (mNewUser) {
            val mInfoString = "Welcome to SignOnSite! \r\n\r\n Search for sites to sign on to by clicking the button below."
            mInformationTextView.visibility = View.VISIBLE
            mInformationTextView.text = mInfoString
        } else {
            // App is just starting up, show the pre-search view
            mSiteListView.visibility = View.GONE
            mPreSearchView.visibility = View.VISIBLE
        }

        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(requireContext().toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        EventBus.getDefault().unregister(this)
    }

    fun searchForSites(){
        if (NetworkUtil.networkIsConnected(requireActivity())) {
            mPreSearchView.visibility = View.GONE
            presenter.retrieveNearBySites()
        } else {
            AlertDialogMessageHelper.networkErrorMessage(requireActivity())
        }
    }

    override fun onLocationChanged(location: Location) {
        // Check that the app hasn't navigated or begun navigating to SignedOnActivity
        if (activity == null) {
            return
        }

        if (fromPrompt =="true") {
            mSearchButton.performClick()
        }

        SLog.i(LOG, location.toString())
        // The passive provider does not appear to receive updates from the Fused location service in
        // MainActivity. If possible, manually copy the location data across.
        if ((activity as MainActivity).mCurrentLocation != null) {
            mAccuracy = location.accuracy
            mCurrentLocation = (activity as MainActivity).mCurrentLocation
            Log.i(LOG, "mCurrentLocation: " + mCurrentLocation!!.toString())
        }
        if (mSearchForSites!! && mCurrentLocation != null) {
            mSearchForSites = false
        }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

    override fun onProviderEnabled(provider: String) {}

    override fun onProviderDisabled(provider: String) {}

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(featureFlagsManager.hasUserPassportShareEnable)
            menu.findItem(R.id.share_button).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun showListOfSites(nearSites: List<NearSite>) {
        SLog.i(LOG, "sites to display: 1 or more")
        (activity as MainActivity).onCompleteProgressView()
        mPreSearchView.visibility = View.GONE
        mSiteListView.visibility = View.VISIBLE
        mChooseSiteTextView.visibility = View.VISIBLE
        mRefreshButton.visibility = View.VISIBLE
        mPreSearchView.visibility = View.VISIBLE
        mSearchButton.visibility = View.GONE
        mInformationImageView.visibility = View.GONE
        mInformationTextView.visibility = View.GONE
        setAdapter(nearSites)
    }

    private fun setAdapter(nearSites: List<NearSite>){
        mRecycleView.layoutManager = LinearLayoutManager(activity)
        if (mAdapter == null) {
            mAdapter = SiteRecyclerViewAdapter(nearSites, mListener)
            mRecycleView.adapter = mAdapter
        }else{
            mAdapter?.refill(nearSites)
            mAdapter?.notifyDataSetChanged()
        }
    }


    override fun showNetworkErrors() {
        (activity as MainActivity).onCompleteProgressView()
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) {
            presenter.retrieveNearBySites() }
    }

    override fun showEmptyListOfSites() {
        SLog.i(LOG, "sites to display: less than 1")
        (activity as MainActivity).onCompleteProgressView()
        mSiteListView.visibility = View.GONE
        mPreSearchView.visibility = View.VISIBLE
        mChooseSiteTextView.visibility = View.INVISIBLE
        mInformationTextView.visibility = View.VISIBLE
        mRefreshButton.visibility = View.VISIBLE
        if (activity != null && (!mNewUser) && mSites.size == 0) {
            mInformationTextView.text = getString(R.string.sites_not_found_message)
        }
    }

    override fun onRequestPermissionsResultListener(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when{
            PermissionManager.checkForAndroidQAbovePermission(requireActivity())->{
                executeLocationServices()
            }

           !PermissionManager.checkForAndroidQAbovePermission(requireActivity()) ->{
                PermissionManager.requestLocationPermissionInAndroidQAbove(requireActivity(), grantResults)
            }
            ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED &&
                    ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED->{
                showEmptyListOfSites()
            }
            ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED->{
                executeLocationServices()
            }
            else ->{
                (activity as MainActivity).showActivityContentView()
                SLog.i(LOG, "Location permissions were NOT granted")
            }

        }
    }

    fun executeLocationServices(){
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 100f, this)
            presenter.retrieveNearBySites()
            mSearchForSites = true
        }
    }

    fun displayLocationPermissionInfoDialog() {
        SLog.i(MainActivity.LOG, "Displaying location permission info dialog")
        val builder = AlertDialog.Builder(requireActivity())
                .setTitle(requireContext().resources.getString(R.string.location_permission_title))
                .setMessage(requireContext().resources.getString(R.string.location_permission_message))
                .setPositiveButton(requireContext().resources.getString(R.string.location_permission_positive_button_title)) { _: DialogInterface?, _: Int ->
                    // User accepted at initial screen - open proper permission dialog
                    requestLocationPermission()
                }
                .setNegativeButton(requireContext().resources.getString(R.string.location_permission_negative_button_title)) { _: DialogInterface?, _: Int ->
                    router.navigateToMainActivity(requireActivity())
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun requestLocationPermission() {
        when (Build.VERSION.SDK_INT) {
            Build.VERSION_CODES.Q -> {
                ActivityCompat.requestPermissions(requireActivity(),locationPermissionsAndroidTen, requestLocation)
            }
            else -> {
                ActivityCompat.requestPermissions(requireActivity(),locationPermissions, requestLocation)
            }
        }
    }

    interface OnListFragmentInteractionListener {
        fun onSiteSelected(site: NearSite)
    }

    companion object {
        private val LOG = SitesFragment::class.java.simpleName
    }
}
