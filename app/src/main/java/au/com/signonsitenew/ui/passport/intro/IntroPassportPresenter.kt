package au.com.signonsitenew.ui.passport.intro

import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCaseImpl
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface IntroPassportPresenter {
    fun inject(introPassportDisplay: IntroPassportDisplay)
    fun updateUserInfo()
}

class IntroPassportPresenterImpl @Inject constructor(private val introPassportUseCaseImpl: IntroPassportUseCaseImpl,
                                                     private val logger: Logger,
                                                     private val analyticsEventDelegateService: AnalyticsEventDelegateService) : BasePresenter(), IntroPassportPresenter {
    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var introPassportDisplay: IntroPassportDisplay

    override fun inject(introPassportDisplay: IntroPassportDisplay) {
        this.introPassportDisplay = introPassportDisplay
    }

    override fun updateUserInfo() {
        disposables.add(
                introPassportUseCaseImpl.createPassport()
                        .compose(applySchedulers())
                        .doOnSubscribe { introPassportDisplay.showProgressView() }
                        .doAfterTerminate { introPassportDisplay.removeProgressView() }
                        .subscribe({ response ->
                            if (response.status == Constants.RESPONSE_SUCCESS) {
                                analyticsEventDelegateService.passportCreated()
                                introPassportDisplay.navigateToMainActivityOrSignedActivity()
                            } else {
                                introPassportDisplay.showDateError(introPassportUseCaseImpl.getErrorMessage(response))
                                logger.w(this::class.java.name,attributes = mapOf(this::updateUserInfo.name to toJson(response)))
                            }
                        }, {
                            introPassportDisplay.showNetworkError()
                            logger.e(this::class.java.name,attributes = mapOf(this::updateUserInfo.name to it.message))
                        }))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}
