package au.com.signonsitenew.ui.main

import android.location.Location

interface MainActivityView {
    fun updateNotificationValueForPassport(count: Int?)
    fun checkIfUserHasCompanyEnrolmentLocked(hasEnrolmentLocked: Boolean, currentLocation:Location)
    fun showProgressView(text: String?)
    fun showInternetError()
    fun showDataError(message:String)
    fun navigateToSignedActivity()
}