package au.com.signonsitenew.ui.documents.permits.details.team.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.ui.adapters.AttendanceExpandableParentAdapter
import java.util.ArrayList

class AddTeamMembersListAdapter(var listOfMembers:MutableList<RequesteeUser>, val callback:(listOfMembers: List<RequesteeUser>)->Unit, val removeCallback:(requestee:RequesteeUser)->Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private val newMemberList = mutableListOf<RequesteeUser>()
    internal var filter: AddTeamMembersAdapterFilter = AddTeamMembersAdapterFilter(this@AddTeamMembersListAdapter, listOfMembers as ArrayList<RequesteeUser>)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       return  AddTeamMembersViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_team_member, parent, false))
    }

    override fun getItemCount(): Int = listOfMembers.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as AddTeamMembersViewHolder
        holder.memberName.text = listOfMembers[position].first_name + " " + listOfMembers[position].last_name
        holder.memberCompanyName.text = listOfMembers[position].company_name
        holder.memberTeamCheckBox.isChecked = false
        holder.memberTeamCheckBox.setOnCheckedChangeListener{ _, isChecked ->
            if(isChecked) {
                newMemberList.add(listOfMembers[position])
                callback(newMemberList)
            }else{
                removeCallback(listOfMembers[position])
            }
        }
        holder.itemView.setOnClickListener {
            holder.memberTeamCheckBox.isChecked = !holder.memberTeamCheckBox.isChecked
        }
    }

    inner class AddTeamMembersViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val memberName:TextView = itemView.findViewById(R.id.team_member_name)
        val memberTeamCheckBox:CheckBox = itemView.findViewById(R.id.team_member_checkbox)
        val memberCompanyName:TextView = itemView.findViewById(R.id.team_member_company_name)
    }

    override fun getFilter(): Filter {
        return filter
    }

    fun reloadData(listOfMembers: MutableList<RequesteeUser>){
        this.listOfMembers = listOfMembers
        notifyDataSetChanged()
    }


    class AddTeamMembersAdapterFilter(var adapter: AddTeamMembersListAdapter, private var requesteeUsersList: ArrayList<RequesteeUser>) : Filter() {
        private var listOfTeamMembers: MutableList<RequesteeUser> = arrayListOf()

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            listOfTeamMembers.clear()
            val results = FilterResults()
            if (constraint!!.isEmpty()) {
                listOfTeamMembers.addAll(requesteeUsersList)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for (requestee in requesteeUsersList) {
                    if(!requestee.first_name.isNullOrBlank() && !requestee.last_name.isNullOrBlank())
                    if (requestee.first_name.toLowerCase().startsWith(filterPattern) || requestee.first_name.toLowerCase().contains(filterPattern)||
                            requestee.last_name.toLowerCase().startsWith(filterPattern) || requestee.last_name.toLowerCase().contains(filterPattern)) {
                        listOfTeamMembers.add(requestee)
                    }
                }
            }
            results.values = listOfTeamMembers
            results.count = listOfTeamMembers.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            adapter.reloadData(results?.values as ArrayList<RequesteeUser>)
        }
    }

}