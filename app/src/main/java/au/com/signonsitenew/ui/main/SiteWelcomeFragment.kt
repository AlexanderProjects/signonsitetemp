package au.com.signonsitenew.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.api.CompaniesForSite
import au.com.signonsitenew.domain.models.NearSite
import au.com.signonsitenew.realm.EnrolledSite
import au.com.signonsitenew.realm.services.SiteSettingsService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.DarkToast
import au.com.signonsitenew.utilities.DebouncedOnClickListener
import au.com.signonsitenew.utilities.SLog
import io.realm.Realm
import org.json.JSONException


/**
 * Created by Krishan Caldwell on 23/10/2018.
 *
 * A simple fragment that users will see on sites who are using pre-enrolled companies.
 * Currently this will only prompt the user to select which company they are working for on this
 * site. However, in the future it may include more functionality. It may also not...
 *
 */
class SiteWelcomeFragment : Fragment() {
    private lateinit var mRealm: Realm
    private lateinit var nearSite: NearSite

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRealm = Realm.getDefaultInstance()
        arguments?.let {
            nearSite = it.getParcelable(Constants.NEAR_SITE)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView: View = inflater.inflate(R.layout.fragment_site_welcome, container, false)

        // Set the title for the parent activity
        (activity as FirstSignOnActivity).mTitleTextView.text = "Welcome to Site"

        val siteNameTextView: TextView = rootView.findViewById(R.id.site_name_text_view)
        val searchCompaniesButton: Button = rootView.findViewById(R.id.search_companies_button)
        siteNameTextView.text = nearSite.name

        searchCompaniesButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                // Navigate the user to the add new company screen
                (activity as FirstSignOnActivity).navigateToSiteCompanySelectFragment()
            }
        })

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        mRealm.close()
    }

    companion object {
        val TAG: String = SiteWelcomeFragment::class.java.simpleName
    }
}
