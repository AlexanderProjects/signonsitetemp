package au.com.signonsitenew.ui.attendanceregister.attendance


import android.os.Bundle
import androidx.databinding.ObservableField
import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.domain.models.CompanyFeatureFlagResponse
import au.com.signonsitenew.domain.models.TodaysVisitsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.state.NewAttendanceRegisterFragmentState
import au.com.signonsitenew.domain.usecases.attendance.AttendanceRegisterUseCaseImpl
import au.com.signonsitenew.domain.usecases.featureflag.FeatureFlagsUseCase
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

interface AttendanceRegisterPresenter{
    fun inject(display: AttendanceRegisterDisplay)
    fun getWorkersVisits(extras: Bundle?)
    fun attendanceRegisterWorkerViewedAnalytics(targetted_user_id:Int)
    fun observeStates()
}

class AttendanceRegisterPresenterImpl @Inject constructor(private val attendanceRegisterUseCase: AttendanceRegisterUseCaseImpl,
                                                          private val logger:Logger,
                                                          private val featureFlagsUseCase: FeatureFlagsUseCase) : BasePresenter(), AttendanceRegisterPresenter{

    private val disposables: CompositeDisposable = CompositeDisposable()
    private val uiState = ObservableField<NewAttendanceRegisterFragmentState>()
    private lateinit var display: AttendanceRegisterDisplay
    var isWorkerNotesFeatureFlagActivated :Boolean = false

    override fun inject(display: AttendanceRegisterDisplay){
        this.display = display
    }

    override fun getWorkersVisits(extras: Bundle?) {
        uiState.set(NewAttendanceRegisterFragmentState.ShowProgressView)
        featureFlagsUseCase.getCompanyFeatureFlagsAsync(object :DisposableSingleObserver<CompanyFeatureFlagResponse>(){
            override fun onSuccess(response: CompanyFeatureFlagResponse) {
                if(response.status == Constants.JSON_SUCCESS){
                    val workerNotesFeatureFlag = response.owner_company_feature_flags.find { it.feature_flag_name == Constants.WORKER_NOTES_KEY }
                    if(workerNotesFeatureFlag?.value == true && BuildConfig.WORKER_NOTES_FLAG){
                        isWorkerNotesFeatureFlagActivated = true
                        if (extras != null && extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY) == Constants.FCM_WORKER_NOTE_ALERT){
                            attendanceRegisterUseCase.getAttendeeWorkerAsync(extras.getString(Constants.USER_ID)!!.toInt(),object :DisposableSingleObserver<AttendeesResponse>(){
                                override fun onSuccess(response: AttendeesResponse) {
                                    if (NetworkErrorValidator.isValidResponse(response)) {
                                        uiState.set(attendanceRegisterUseCase.getRickyWorker(response, extras.getString(Constants.USER_ID)!!.toInt())?.let { NewAttendanceRegisterFragmentState.ShowWorkerDetails(it) })
                                    }else{
                                        logger.w(AttendanceRegisterPresenterImpl::class.java.name,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to Gson().toJson(response)))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    logger.e(AttendanceRegisterPresenterImpl::class.java.name,error,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to error.message))
                                }
                            })
                        }else {
                            attendanceRegisterUseCase.getAttendeesAsync(object : DisposableSingleObserver<AttendeesResponse>() {
                                override fun onSuccess(response: AttendeesResponse) {
                                    if (NetworkErrorValidator.isValidResponse(response)) {
                                        uiState.set(NewAttendanceRegisterFragmentState.SuccessResponse(ArrayList(attendanceRegisterUseCase.mapAttendeeToAttendanceAdapterParentModel(response))))
                                        uiState.set(NewAttendanceRegisterFragmentState.HideProgressView)
                                    } else {
                                        uiState.set(NewAttendanceRegisterFragmentState.HideProgressView)
                                        uiState.set(NewAttendanceRegisterFragmentState.DataError(NetworkErrorValidator.getErrorMessage(response)))
                                        logger.w(AttendanceRegisterPresenterImpl::class.java.name,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to toJson(response)))
                                    }
                                }

                                override fun onError(error: Throwable) {
                                    logger.e(AttendanceRegisterPresenterImpl::class.java.name,error,attributes =mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to error.message))
                                }
                            })
                        }
                    }else{
                        attendanceRegisterUseCase.getTodayVisitsAsync(object : DisposableSingleObserver<TodaysVisitsResponse>() {
                            override fun onSuccess(response: TodaysVisitsResponse) {
                                if (NetworkErrorValidator.isValidResponse(response)) {
                                    uiState.set(NewAttendanceRegisterFragmentState.Success(ArrayList(attendanceRegisterUseCase.mapTodaysAttendanceToNewAttendanceAdapterParentModel(response))))
                                    uiState.set(NewAttendanceRegisterFragmentState.HideProgressView)
                                } else {
                                    uiState.set(NewAttendanceRegisterFragmentState.HideProgressView)
                                    uiState.set(NewAttendanceRegisterFragmentState.DataError(NetworkErrorValidator.getErrorMessage(response)))
                                    logger.w(AttendanceRegisterPresenterImpl::class.java.name,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to toJson(response)))
                                }
                            }

                            override fun onError(error: Throwable) {
                                logger.e(AttendanceRegisterPresenterImpl::class.java.name,error,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to error.message))
                            }
                        })

                    }
                }
            }

            override fun onError(error: Throwable) {
                logger.e(AttendanceRegisterPresenterImpl::class.java.name,error,attributes = mapOf(this@AttendanceRegisterPresenterImpl::getWorkersVisits.name to error.message))
            }

        })

    }

    override fun attendanceRegisterWorkerViewedAnalytics(targetted_user_id:Int) = attendanceRegisterUseCase.attendanceRegisterWorkerViewedAnalytics(targetted_user_id)

    override fun observeStates() {
        disposables.add(uiState.addOnPropertyChanged {
            when (it.get()) {
                is NewAttendanceRegisterFragmentState.Success -> display.showData((it.get() as NewAttendanceRegisterFragmentState.Success).listAttendance)
                is NewAttendanceRegisterFragmentState.SuccessResponse -> display.showAttendance((it.get() as NewAttendanceRegisterFragmentState.SuccessResponse).listAttendance)
                is NewAttendanceRegisterFragmentState.ShowWorkerDetails -> display.showWorkerDetails((it.get() as NewAttendanceRegisterFragmentState.ShowWorkerDetails).childAdapterModel)
                is NewAttendanceRegisterFragmentState.DataError -> display.showDataError((it.get() as NewAttendanceRegisterFragmentState.DataError).errorMessage)
                is NewAttendanceRegisterFragmentState.Error -> display.showNetworkError()
                is NewAttendanceRegisterFragmentState.ShowProgressView -> display.showProgressView()
                is NewAttendanceRegisterFragmentState.HideProgressView -> display.hideProgressView()
            }
        })
    }


    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        attendanceRegisterUseCase.dispose()
    }
}
