package au.com.signonsitenew.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.attendees.WorkerNotes


class WorkerNotesListAdapter(val workerNotesList:List<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_ONE = 1
        const val VIEW_TYPE_TWO = 2
    }

    inner class WorkerNotesTitleViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title : TextView = view.findViewById(R.id.worker_notes_desc_title)
        val icon :ImageView = view.findViewById(R.id.worker_notes_desc_icon)
        fun bind(position: Int){
            title.text = workerNotesList[position] as String
            when(workerNotesList[position] as String){
                "alert" -> title.text = "Alert"
                "warning" -> title.text = "Warning"
                "info" -> title.text = "Information"
            }
            when(workerNotesList[position] as String){
                "alert" -> icon.setImageResource(R.drawable.ic_danger)
                "warning" -> icon.setImageResource(R.drawable.ic_warn)
                "info" -> icon.setImageResource(R.drawable.ic_info)
            }
        }

    }
    inner class WorkerNotesDescriptionHolder(view: View): RecyclerView.ViewHolder(view){
        private val descriptionContent:TextView = view.findViewById(R.id.worker_notes_desc_details_content)
        private val descriptionCreatedBy:TextView = view.findViewById(R.id.worker_notes_desc_details_created_by)
        fun bind(position: Int){
            descriptionContent.text = (workerNotesList[position] as WorkerNotes).content
            descriptionCreatedBy.text = "Modified " + (workerNotesList[position] as WorkerNotes).created_at +" "+"by"+" "+(workerNotesList[position] as WorkerNotes).created_by_user.first_name + " " + (workerNotesList[position] as WorkerNotes).created_by_user.last_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == VIEW_TYPE_ONE)
            WorkerNotesTitleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_worker_notes_title,parent,false))
        else
            WorkerNotesDescriptionHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_worker_notes,parent,false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(workerNotesList[position] is String)
            (holder as WorkerNotesTitleViewHolder).bind(position)
        else
            (holder as WorkerNotesDescriptionHolder).bind(position)

    }

    override fun getItemCount(): Int = workerNotesList.size

    override fun getItemViewType(position: Int): Int {
        return if(workerNotesList[position] is String)
            VIEW_TYPE_ONE
        else
            VIEW_TYPE_TWO
    }
}