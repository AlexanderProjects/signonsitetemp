package au.com.signonsitenew.ui.passport.intro

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.IntroPassportFragmentBinding
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.ui.adapters.IntroFragmentsAdapter
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.ui.passport.intro.sequence.*
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import com.google.android.material.tabs.TabLayoutMediator
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class IntroPassportFragment : DaggerFragment(), IntroPassportDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private var _binding:IntroPassportFragmentBinding? = null
    private val binding get() = _binding

    @Inject
    lateinit var router: Router

    companion object {
        fun newInstance() = IntroPassportFragment()
    }

    private lateinit var presenterImpl: IntroPassportPresenterImpl

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = IntroPassportFragmentBinding.inflate(inflater, container, false)
        val viewPager = binding?.viewPager
        val listOfIntroFragments = mutableListOf<Fragment>()
        listOfIntroFragments.add(FirstIntroFragment())
        listOfIntroFragments.add(SecondIntroFragment())
        listOfIntroFragments.add(ThirdIntroFragment())
        listOfIntroFragments.add(FourthIntroFragment())
        listOfIntroFragments.add(FifthIntroFragment())
        val adapter = IntroFragmentsAdapter(requireActivity(),listOfIntroFragments)
        viewPager?.adapter = adapter
        val tabLayout = binding?.introTablayout
        TabLayoutMediator(tabLayout!!, viewPager!!){ tab, _ -> viewPager.setCurrentItem(tab.position,true)}.attach()
        binding?.createPassportButton?.setOnClickListener { presenterImpl.updateUserInfo() }
        return binding?.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this,viewModelFactory).get(IntroPassportPresenterImpl::class.java)
        presenterImpl.inject(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showNetworkError() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenterImpl.updateUserInfo()}
    }

    override fun showDateError(message: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),message)
    }

    override fun navigateToMainActivityOrSignedActivity() {
        router.navigationToMainActivityOrSignedActivity(requireActivity() as DaggerAppCompatActivity)
    }

    override fun showProgressView() {
        if(activity is MainActivity){
            router.showProgressView(requireActivity(), R.id.main_activity_container)
        }else{
            router.showProgressView(requireActivity(), R.id.signon_container)
        }
    }

    override fun removeProgressView() {
        if(activity is MainActivity){
            router.removeProgressView(requireActivity(), R.id.main_activity_container)
        }else{
            router.removeProgressView(requireActivity(), R.id.signon_container)
        }
    }

}
