package au.com.signonsitenew.ui.attendanceregister;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Objects;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.api.API;
import au.com.signonsitenew.api.TodaysVisits;
import au.com.signonsitenew.api.VisitorSignOff;
import au.com.signonsitenew.api.VisitorSignOn;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.events.AttendanceUpdatedEvent;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.EnrolledUser;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.SiteInduction;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.realm.services.UserAbilitiesService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import au.com.signonsitenew.utilities.SLog;
import dagger.android.support.DaggerFragment;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Fragment class to display the details of a user after they are selected in the Management panel.
 * Provides details of the user such as name, company, and phone number.
 * Provides additional functionality to view their historical visits, call them, or sign them on/off.
 */
public class UserDetailFragment extends DaggerFragment implements AttendanceActivity.UpdateVisitorStatusInUserDetailsListener {

    private static final String TAG = UserDetailFragment.class.getSimpleName();

    @Inject
    AnalyticsEventDelegateService analyticsEventService;

    private static final String ARG_ACTION = "action";
    public static final String ATTENDEE_ID = "attendeeId";
    protected static final String INDUCTION_ID = "inductionId";
    private Realm mRealm;
    private boolean mInductionsEnabled;
    private long attendeeId;
    private boolean mSignedOn;
    private ImageView mIconImageView;
    boolean visitor = false;
    protected TextView mNameView;
    protected TextView mCompanyView;
    protected TextView mPhoneNumberView;
    protected TextView mInductionStatusView;
    protected Button mSignOnOffButton;
    protected LinearLayout mInductionCell;
    protected LinearLayout mHistoryCell;
    protected LinearLayout mCallUserCell;
    protected TextView mHistoryText;
    protected TextView mCallUserText;
    protected TextView mInductionStatusText;
    protected ImageView mInductionAlert;
    protected ImageView mInductionIcon;
    protected ImageView mCallIcon;

    public UserDetailFragment() { }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRealm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            attendeeId = getArguments().getLong(ATTENDEE_ID);
        }

        // User here is either a supervisor or site manager - supervisors cannot see inductions
        SiteSettingsService siteSettingsService = new SiteSettingsService(mRealm);
        mInductionsEnabled = siteSettingsService.siteInductionsEnabled();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_detail, container, false);

        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).hide();
        Toolbar toolbar = rootView.findViewById(R.id.details_user_toolbar);
        toolbar.setTitle("Details");
        ((AppCompatActivity) requireActivity()).setSupportActionBar(toolbar);
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        mIconImageView = rootView.findViewById(R.id.attendee_icon);
        mNameView = rootView.findViewById(R.id.visitor_name_text_view);
        mCompanyView = rootView.findViewById(R.id.visitor_company_text_view);
        mPhoneNumberView = rootView.findViewById(R.id.visitor_phone_number_text_view);
        mInductionStatusView = rootView.findViewById(R.id.visitor_induction_state_text_view);
        mSignOnOffButton = rootView.findViewById(R.id.visitor_sign_on_button);
        mInductionCell = rootView.findViewById(R.id.attendee_induction_cell);
        mHistoryCell = rootView.findViewById(R.id.visitor_history_cell);
        mCallUserCell = rootView.findViewById(R.id.visitor_call_cell);
        mHistoryText = rootView.findViewById(R.id.visitor_history_text);
        mCallUserText = rootView.findViewById(R.id.visitor_call_text);
        mInductionStatusText = rootView.findViewById(R.id.attendee_induction_text);
        mInductionAlert = rootView.findViewById(R.id.induction_alert_image_view);
        mInductionIcon = rootView.findViewById(R.id.attendee_induction_icon);
        mCallIcon = rootView.findViewById(R.id.phone_icon);

        mIconImageView.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.grey_primary), PorterDuff.Mode.SRC_IN));
        mInductionIcon.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.grey_primary), PorterDuff.Mode.SRC_IN));
        mCallIcon.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.grey_primary), PorterDuff.Mode.SRC_IN));

        // Fetch Visitor details to display
        setUserInfo();

        mSignOnOffButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                TogglerUserAttendanceStatus();
            }
        });

        mHistoryCell.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Navigate to visit history fragment
                Fragment historyFragment = new UserAttendanceHistoryFragment();
                Bundle args = new Bundle();
                args.putLong(ATTENDEE_ID,attendeeId);
                historyFragment.setArguments(args);

                ((AttendanceActivity)getActivity()).navigateToFragment(historyFragment, true);
            }
        });

        mCallUserCell.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Take the user to the phone dialer with the number input
                callUser();
            }
        });

        return rootView;
    }



    private void setUserInfo() {
        String name;
        final String firstName;
        String company;
        String phoneNumber;
        String inductionStatus;
        String inductionText = "";
        String inductionCellText;
        boolean enrolmentActive = true;


        UserAbilitiesService abilitiesService = new UserAbilitiesService(mRealm);
        SiteInductionService inductionService = new SiteInductionService(mRealm);
        SiteSettingsService settingsService = new SiteSettingsService(mRealm);
        UserService userService = new UserService(mRealm);

        SiteAttendee attendee = mRealm.where(SiteAttendee.class).equalTo("userId", attendeeId).findFirst();
        long userId = userService.getCurrentUserId();
        long siteId = settingsService.getSiteId();

        if (attendee != null) {
            name = attendee.getName();
            firstName = attendee.getFirstName();
            company = attendee.getCompany();
            phoneNumber = attendee.getPhoneNumber();
            if (attendee.getInductionStatus() == null) {
                inductionStatus = "Unknown";
            }
            else {
                inductionStatus = attendee.getInductionStatus();
            }
            enrolmentActive = attendee.getEnrolmentState();
            if (enrolmentActive && attendee.getInductionStatus().equals(Constants.DOC_INDUCTION_COMPLETE)) {
                // Check the users enrolment type
                visitor = inductionService.getSiteInductionForUser(attendee.getUserId()).getType().equals(Constants.INDUCTED_VISITOR);
            }
        }
        else {
            EnrolledUser enrolledUser = mRealm.where(EnrolledUser.class)
                    .equalTo("id", attendeeId)
                    .findFirst();

            name = enrolledUser.getName();
            firstName = enrolledUser.getFirstName();
            company = enrolledUser.getCompanyName();
            phoneNumber = enrolledUser.getPhoneNumber();
            if (enrolledUser.getInductionStatus() == null) {
                inductionStatus = "Unknown";
            }
            else {
                inductionStatus = enrolledUser.getInductionStatus();
            }
            if (enrolledUser.getInductionStatus().equals(Constants.DOC_INDUCTION_COMPLETE)) {
                // Check the users enrolment type
                visitor = inductionService.getSiteInductionForUser(enrolledUser.getId()).getType().equals(Constants.INDUCTED_VISITOR);
            }
        }

        Log.i(TAG, "Induction status: " + inductionStatus);
        // Set the alert as appropriate depending on induction status
        if (!enrolmentActive) {
            inductionText = "Marked on wrong site";
            mInductionCell.setVisibility(View.GONE);
            mInductionStatusView.setTextColor(getResources().getColor(R.color.orange_secondary));
        }
        else if (visitor) {
            inductionText = "Visitor";
            mInductionCell.setVisibility(View.GONE);
            mInductionStatusView.setTextColor(getResources().getColor(R.color.text_grey_primary));
        }
        else if (inductionStatus.equals("Unknown")) {
            inductionText = "Unknown";
        }
        else {
            if (inductionStatus.equals(Constants.DOC_INDUCTION_INCOMPLETE)) {
                inductionText = "Induction Required";
                if (mInductionsEnabled) {
                    inductionCellText = "View induction options";
                }
                else {
                    inductionCellText = "Change " + firstName + "\'s status";
                }
                mInductionStatusText.setText(inductionCellText);
                mInductionStatusView.setTextColor(getResources().getColor(R.color.emergency_primary));
                if (mInductionsEnabled) {
                    mInductionAlert.setVisibility(View.VISIBLE);
                }
                mInductionAlert.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.emergency_primary), PorterDuff.Mode.SRC_IN));
            }
            else if (inductionStatus.equals(Constants.DOC_INDUCTION_PENDING)) {
                inductionText = "Induction Pending";
                inductionCellText = "Review " + firstName + "\'s Induction";
                mInductionStatusText.setText(inductionCellText);
                mInductionStatusView.setTextColor(getResources().getColor(R.color.orange_secondary));
                if (mInductionsEnabled) {
                    mInductionAlert.setVisibility(View.VISIBLE);
                }
                mInductionAlert.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.orange_secondary), PorterDuff.Mode.SRC_IN));
            }
            else if (inductionStatus.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                inductionText = "Inducted";
                inductionCellText = "View " + firstName + "\'s Induction";
                mInductionStatusText.setText(inductionCellText);
                mInductionStatusView.setTextColor(getResources().getColor(R.color.green_highlight));
                mInductionAlert.setVisibility(View.GONE);
                if (!mInductionsEnabled) {
                    mInductionCell.setVisibility(View.GONE);
                }
            }

        }

        // Hide Cell for supervisors
        if (!abilitiesService.canAccessUserInductions(userId, siteId)) {
            mInductionCell.setVisibility(View.GONE);
        }

        // Set title
        ((AttendanceActivity)getActivity()).mTitleTextView.setText(name);

        // Set details
        mNameView.setText(name);
        mCompanyView.setText(company);
        mPhoneNumberView.setText(phoneNumber);
        mInductionStatusView.setText(inductionText);

        String historyText = "View " + firstName +"'s visits for today";
        String callText = "Call " + firstName;
        mHistoryText.setText(historyText);
        mCallUserText.setText(callText);

        // Determine if visitor is currently signed on or off
        RealmResults<AttendanceRecord> visits = mRealm.where(AttendanceRecord.class)
                .equalTo("userId", attendeeId)
                .sort("id", Sort.DESCENDING)
                .findAll();

        AttendanceRecord currentVisit = null;
        if (visits.size() > 0) {
            currentVisit = visits.first();
        }

        User currentUser = mRealm.where(User.class).findFirst();

        if (currentUser.getId() == attendeeId) {
            // We don't want the Site Manager to sign themselves off from this screen
            mSignOnOffButton.setVisibility(View.INVISIBLE);
        }
        else if (currentVisit == null) {
            mSignedOn = false;
            mSignOnOffButton.setText("Sign On");
        }
        else if (currentVisit.getCheckOutTime() != null && !currentVisit.getCheckOutTime().equals("null")) {
            // User is currently signed off
            mSignedOn = false;
            mSignOnOffButton.setText("Sign On");
        }
        else {
            // User is currently signed on
            mSignedOn = true;
            mSignOnOffButton.setText("Sign Off");
        }

        final String inductionState = inductionStatus;
        mInductionCell.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Work out status to decide where to navigate to
                Fragment fragment = null;
                Bundle args = new Bundle();
                if (mInductionsEnabled) {
                    switch (inductionState) {
                        case Constants.DOC_INDUCTION_PENDING:
                            analyticsEventService.managerAttendanceRegisterSiteInductionReviewed((int) attendeeId);
                            fragment = new InductionReviewFragment();
                            break;
                        case Constants.DOC_INDUCTION_INCOMPLETE:
                            fragment = new InductionOptionsFragment();
                            break;
                        case Constants.DOC_INDUCTION_COMPLETE:
                            fragment = new FormViewFragment();
                            SiteInductionService inductionService = new SiteInductionService(mRealm);
                            SiteInduction induction = inductionService.getSiteInductionForUser(attendeeId);
                            long formId = induction.getId();
                            String type = induction.getType();
                            switch (type) {
                                case "user_form":
                                    args.putString(ARG_ACTION, "view");
                                    break;
                                case "forced_with_documents":
                                    args.putString(ARG_ACTION, "view_docs");
                                    break;
                                default:
                                    break;
                            }
                            args.putLong(INDUCTION_ID, formId);
                            break;
                        default:
                            break;
                    }
                }
                else {
                    switch (inductionState) {
                        case Constants.DOC_INDUCTION_PENDING:
                            fragment = new InductionReviewFragment();
                            break;
                        case Constants.DOC_INDUCTION_INCOMPLETE:
                            fragment = new InductionCancelFragment();
                            break;
                        case Constants.DOC_INDUCTION_COMPLETE:
                            fragment = new FormViewFragment();
                            SiteInductionService inductionService = new SiteInductionService(mRealm);
                            long formId = inductionService.getSiteInductionForUser(attendeeId).getId();
                            args.putString(ARG_ACTION, "view");
                            args.putLong(INDUCTION_ID, formId);
                            break;
                        default:
                            break;
                    }
                }
                // First make sure there are docs or a form to see...
                if (mInductionsEnabled) {
                    SiteInductionService inductionService = new SiteInductionService(mRealm);
                    SiteInduction induction = inductionService.getSiteInductionForUser(attendeeId);
                    if (induction != null && induction.getType().equals("forced")) {
                        DarkToast.makeText(getActivity(), firstName + " was inducted without documents. There is nothing to view");
                        return;
                    }
                }

                // Navigate to Induction Options Fragment
                if (fragment != null) {
                    args.putLong(ATTENDEE_ID, attendeeId);
                    fragment.setArguments(args);
                    ((AttendanceActivity)getActivity()).navigateToFragment(fragment, true);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // Show the parent Toolbar in case it was switched off by form viewer
        ((AttendanceActivity)getActivity()).mToolbar.setVisibility(View.GONE);

    }

    private void TogglerUserAttendanceStatus() {
        // Show progress view
        ((AttendanceActivity)getActivity()).showDetailProgressView();

        // Sign user on/off as appropriate
        if (mSignedOn) {
            // Sign user off
            VisitorSignOff.post(getActivity(),
                    attendeeId,
                    new API.ResponseCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                    // Successfully signed the user off
                                    analyticsEventService.attendanceRegisterWorkerSignOffPressed((int) attendeeId);
                                    mSignedOn = false;
                                    mSignOnOffButton.setText("Sign On");
                                    mSignOnOffButton.setEnabled(false);
                                    DarkToast.makeText(getActivity(),
                                            "Successfully signed " + mNameView.getText().toString() + " Off");

                                    retrieveTodaysSiteVisits();
                                }
                                else {
                                    ((AttendanceActivity)getActivity()).showContentView();
                                    displayErrorToast(false);
                                }
                            }
                            catch (JSONException e) {
                                ((AttendanceActivity)getActivity()).showContentView();
                                SLog.e(TAG, "JSON Exception Occurred: " + e.getMessage());
                                displayErrorToast(false);
                            }
                        }
                    },
                    new API.ErrorCallback() {
                        @Override
                        public void onError() {
                            ((AttendanceActivity)getActivity()).showContentView();
                            displayErrorToast(false);
                        }
                    });
        }
        else {
            // Sign user on
            VisitorSignOn.post(getActivity(),
                    attendeeId
                    ,
                    new API.ResponseCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                    // Successfully signed the user on
                                    analyticsEventService.attendanceRegisterWorkerSignOnPressed((int) attendeeId);
                                    mSignedOn = true;
                                    mSignOnOffButton.setText("Sign Off");
                                    mSignOnOffButton.setEnabled(true);
                                    DarkToast.makeText(getActivity(),
                                            "Successfully signed " + mNameView.getText().toString() + " On");

                                    // Update the visitor list
                                    retrieveTodaysSiteVisits();
                                }
                                else {
                                    displayErrorToast(true);
                                    ((AttendanceActivity)getActivity()).showContentView();
                                }
                            }
                            catch (JSONException e) {
                                SLog.e(TAG, "JSON Exception Occurred: " + e.getMessage());
                                displayErrorToast(true);
                                ((AttendanceActivity)getActivity()).showContentView();
                            }
                        }
                    },
                    new API.ErrorCallback() {
                        @Override
                        public void onError() {
                            displayErrorToast(true);
                            ((AttendanceActivity)getActivity()).showContentView();
                        }
                    });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AttendanceActivity activity = (AttendanceActivity) requireActivity();
        activity.setUpdateVisitorListener(this);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    // Hide all options
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return true;
    }

    private void displayErrorToast(boolean signingOn) {
        String message;
        if (signingOn) {
            message = "An error occurred when attempting to sign user on, please make sure you " +
                    "have a stable internet connection.";
        }
        else {
            message = "An error occurred when attempting to sign user off, please make sure you " +
                    "have a stable internet connection.";
        }
        DarkToast.makeText(getActivity(), message);
    }

    private void callUser() {
        String number = mPhoneNumberView.getText().toString().trim();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    private void retrieveTodaysSiteVisits() {
        TodaysVisits.get(getActivity(),
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {

                                ((AttendanceActivity)getActivity()).showContentView();
                            }
                            else {
                                // Cancel loading screen
                                ((AttendanceActivity)getActivity()).showContentView();
                            }
                        }
                        catch (JSONException e) {
                            ((AttendanceActivity)getActivity()).showContentView();
                            SLog.e(TAG, "A JSON Exception occurred: " + e.getMessage());
                        }
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        // Cancel the loading screen
                        ((AttendanceActivity)getActivity()).showContentView();
                    }
                });
    }

    @Subscribe
    public void onAttendanceUpdatedEvent(AttendanceUpdatedEvent event) {
        Log.i(TAG, "AttendanceUpdatedEvent received. " + event.toString());
        if (getActivity() != null) {
            setUserInfo();
        }
    }

    @Override
    public void updateVisitorStatus() {
        retrieveTodaysSiteVisits();
    }
}
