package au.com.signonsitenew.ui.documents.briefings

import android.app.ProgressDialog
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider

import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.ui.documents.briefingslist.BriefingListTabbedFragment
import au.com.signonsitenew.ui.documents.createbriefings.CreateBriefingFragment.OnBriefingSavedListener
import au.com.signonsitenew.ui.documents.briefingdetails.BriefingsFragment
import au.com.signonsitenew.ui.documents.createbriefings.CreateBriefingFragment
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.DarkToast
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * BriefingsActivity is where all logic related to displaying and setting briefings is located.
 */

class BriefingsActivity : DaggerAppCompatActivity(), OnBriefingSavedListener, BriefingsActivityDisplay  {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenter: BriefingsActivityPresenter by viewModels { viewModelFactory  }

    @Inject
    lateinit var router: Router

    lateinit var mTitleTextView: TextView
    private var briefing: WorkerBriefing? = null
    private lateinit var dialog:ProgressDialog
    lateinit var mFragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_briefings)
        presenter.inject(this)
        presenter.observeStates()
        dialog = ProgressDialog(this)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        briefing = intent.getParcelableExtra(Constants.ACTIVE_BRIEFING_KEY)

        mTitleTextView = findViewById(R.id.toolbar_title_text_view)
        mTitleTextView.text = Constants.BRIEFINGS_TOOLBAR_TITLE
        mFragmentManager = supportFragmentManager

        // Show the view
        if (intent.getBooleanExtra(Constants.DOC_BRIEFING_LIST, false))
        {
            mTitleTextView.visibility = View.INVISIBLE
            loadBriefingListFragment()
        } else {
            loadBriefingFragment()
        }

    }

    private fun loadBriefingListFragment(){
        val fragment = BriefingListTabbedFragment()
        val args = Bundle()
        args.putParcelable(Constants.ACTIVE_BRIEFING_KEY,briefing)
        fragment.arguments = args
        val transaction = mFragmentManager.beginTransaction()
        transaction.replace(R.id.briefing_container, fragment, "BriefingListTabbedFragment")
        transaction.commit()
    }

    private fun loadBriefingFragment() {
        val fragment = BriefingsFragment()
        val args = Bundle()
        args.putParcelable(Constants.ACTIVE_BRIEFING_KEY,briefing)
        fragment.arguments = args
        val transaction = mFragmentManager.beginTransaction()
        transaction.add(R.id.briefing_container, fragment)
        transaction.commit()
    }

    fun navigateToCreateBriefingFragment(briefingId: Long) {
        val fragment = CreateBriefingFragment()
        val args = Bundle()
        args.putLong("briefingId", briefingId)
        args.putParcelable(Constants.ACTIVE_BRIEFING_KEY,briefing)
        fragment.arguments = args
        val transaction = mFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        transaction.replace(R.id.briefing_container, fragment, "createBriefingFragment")
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_briefings, menu)
        return true
    }

    override fun onBriefingSaved(content: String) {
        presenter.createBriefing(content)
    }

    override fun showCreateBriefingConfirmation() {
        dialog.dismiss()
        DarkToast.makeText(this, "New briefing set")
        finish()
    }

    override fun showDataErrors(error: String) {
        AlertDialogMessageHelper.dataErrorMessage(this,error)
    }

    override fun showNetworkError() {
        AlertDialogMessageHelper.networkErrorMessage(this)
    }

    override fun showProgressDialog() {
        dialog.setMessage("Saving Briefing...")
        dialog.isIndeterminate = true
        dialog.show()

    }

    override fun onBackPressed() {
        router.navigateToDocumentList(this)
        super.onBackPressed()
    }
}
