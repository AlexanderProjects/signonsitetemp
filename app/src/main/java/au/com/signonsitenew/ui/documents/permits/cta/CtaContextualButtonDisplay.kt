package au.com.signonsitenew.ui.documents.permits.cta

import au.com.signonsitenew.domain.models.state.CtaContextualButtonFragmentState


interface CtaContextualButtonDisplay {
    fun showSendToTeamButton()
    fun showSendToTeamDisableButton()
    fun showSendToTeamAction()
    fun showSendToTeamActionWithWarningMessage()
    fun showObtainApprovalButton()
    fun showObtainApprovalDisableButton()
    fun showObtainApprovalInRequestMessage()
    fun showDoneInRequestButton()
    fun showDoneInProgressButton()
    fun showApproveRejectButton()
    fun showApproveRejectInPendingApprovalMessage()
    fun showDoneInProgressMessage()
    fun showDoneInRequestMessage()
    fun hideButton()
    fun showSendForClosureButton()
    fun showSendForClosureDisableButton()
    fun showSendForClosureInProgressMessage()
    fun showRejectClosureButton()
    fun showRejectClosureInClosingMessage()
    fun showApproveRejectClosureButton()
    fun showApproveRejectDisableButton()
    fun showApproveRejectClosureInClosingMessage()
    fun showPermitDateBeforeNowError()
    fun showProgressView()
    fun removeProgressView()
}