package au.com.signonsitenew.ui.prelogin.registration.details

import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Fragment for registering user details.
 */
class RegisterDetailsFragment : DaggerFragment(), RegisterDetailsDisplay, View.OnClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenterImpl: RegisterDetailsPresenterImpl by viewModels { viewModelFactory  }

    @Inject
    lateinit var logger: Logger
    private lateinit var onNextClickListener: OnNextRegisterDetailsClickListener
    private lateinit var mSession: SessionManager
    private lateinit var mDialog: ProgressDialog
    private lateinit var mDescription: TextView
    private lateinit var mFirstName: EditText
    private lateinit var firstNameError : TextView
    private lateinit var mLastName: EditText
    private lateinit var lastNameError: TextView
    private lateinit var mEmail: EditText
    private lateinit var emailFormatError:TextView
    private lateinit var mSubmitDetailsButton: Button

    fun setOnClickNextListener(onNextListener: OnNextRegisterDetailsClickListener){
        this.onNextClickListener = onNextListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_register_details, container, false)
        presenterImpl.inject(this)
        mDialog = ProgressDialog(activity)
        mDialog.setMessage("Validating...")
        mDialog.isIndeterminate = true
        mDialog.setCancelable(false)
        mSession = SessionManager(activity)
        mEmail = rootView.findViewById(R.id.emailRegEditText)
        mSubmitDetailsButton = rootView.findViewById(R.id.submitDetailsButton)
        mDescription = rootView.findViewById(R.id.registerDescriptionText)
        mFirstName = rootView.findViewById(R.id.firstNameEditText)
        firstNameError = rootView.findViewById(R.id.wrong_first_name_format)
        mFirstName.addTextChangedListener(EditTextWatcher(mFirstName,object : EditTextWatcher.AddAction{
            override fun call(text: String?) {
                when {
                    text?.let { presenterImpl.validateFirstNameFormat(it) }!! -> firstNameError.visibility = View.GONE
                    else -> firstNameError.visibility = View.VISIBLE
                }
            }
        }))
        lastNameError = rootView.findViewById(R.id.wrong_last_name_format)
        mLastName = rootView.findViewById(R.id.lastNameEditText)
        mLastName.addTextChangedListener(EditTextWatcher(mLastName,object : EditTextWatcher.AddAction{
            override fun call(text: String?) {
                when{
                    text?.let { presenterImpl.validateLastNameFormat(it) }!! -> lastNameError.visibility = View.GONE
                    else -> lastNameError.visibility = View.VISIBLE
                }
            }

        }))
        emailFormatError = rootView.findViewById(R.id.wrong_email_name_format)
        mEmail.addTextChangedListener(EditTextWatcher(mEmail,object:EditTextWatcher.AddAction{
            override fun call(text: String?) {
                when{
                    text?.let { presenterImpl.validateEmailFormat(it) }!! -> emailFormatError.visibility = View.GONE
                    else -> emailFormatError.visibility = View.VISIBLE
                }
            }

        }))
        mSubmitDetailsButton.setOnClickListener(this)

        return rootView
    }

    override fun onPause() {
        super.onPause()
        // Dismiss dialog if it is showing
        if (mDialog.isShowing) {
            mDialog.dismiss()
        }
    }

    override fun showNoValidEmailAlertDialog(message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(getString(R.string.error_generic_title))
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
        val dialog = builder.create()
        dialog.show()
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(requireActivity())
    }

    override fun navigateToNextFragment() {
        onNextClickListener.navigateToRegisterPhone()
        logger.d( this::class.java.name +" - "+ Constants.USER_DETAILS_REGISTRATION_STEP_ONE,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail))
    }

    override fun onClick(v: View?) {
        val email = mEmail.text.toString().trim { it <= ' ' }
        val firstName = mFirstName.text.toString().trim { it <= ' ' }
        val lastName = mLastName.text.toString().trim { it <= ' ' }

        if(presenterImpl.validateEmailFormat(email) && presenterImpl.validateFirstNameFormat(firstName) && presenterImpl.validateLastNameFormat(lastName))
            presenterImpl.validateEmail(email,firstName,lastName)
        else
            if(email.isEmpty())
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.EMPTY_EMAIL_TEXT)
            if(!presenterImpl.validateEmailFormat(email))
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.BAD_EMAIL_ALERT_DIALOG_ERROR)
            if(firstName.isEmpty())
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.EMPTY_FIRST_NAME)
            if(!presenterImpl.validateFirstNameFormat(firstName))
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.BAD_FIRST_NAME_ALERT_DIALOG_ERROR)
            if(lastName.isEmpty())
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.EMPTY_LAST_NAME)
            if(!presenterImpl.validateLastNameFormat(lastName))
                AlertDialogMessageHelper.registrationErrorMessage(requireActivity(), Constants.BAD_LAST_NAME_ALERT_DIALOG_ERROR)
    }

    interface OnNextRegisterDetailsClickListener{
        fun navigateToRegisterPhone()
    }
}