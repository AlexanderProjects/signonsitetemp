package au.com.signonsitenew.ui.prelogin.registration.phone

interface RegisterPhoneDisplay {
    fun showNoValidPhoneAlertDialog(message:String)
    fun showNetworkErrors()
    fun navigateToNextFragment()
}