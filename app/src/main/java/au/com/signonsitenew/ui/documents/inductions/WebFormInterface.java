package au.com.signonsitenew.ui.documents.inductions;

import android.app.AlertDialog;
import android.content.Context;
import android.webkit.JavascriptInterface;

import au.com.signonsitenew.R;
import au.com.signonsitenew.ui.documents.inductions.InductionActivity;
import au.com.signonsitenew.utilities.DarkToast;

/**
 * Created by kcaldwell on 10/10/17.
 *
 * Interface to map our Forms JS to Android functions
 */

public class WebFormInterface {
    private Context mContext;

    /** Instantiate the interface and set the context */
    WebFormInterface(Context context) {
        mContext = context;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void siteInductionSubmitted() {
        DarkToast.makeText(mContext, mContext.getResources().getString(R.string.induction_submit_success_message));
        // Navigate somewhere
        if (mContext instanceof InductionActivity) {
            ((InductionActivity)mContext).loadNextFragment();
        }
    }

    /** Show a god damn pop up with info! */
    @JavascriptInterface
    public void submissionFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.error_generic_title))
                .setMessage(mContext.getString(R.string.induction_form_submit_error_message))
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
