package au.com.signonsitenew.ui.documents.permits.details

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitDetailsFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitsUseCase
import au.com.signonsitenew.events.RxBusSaveButtonState
import au.com.signonsitenew.events.RxBusSelectedTeamMember
import au.com.signonsitenew.events.RxBusUserPermitResponses
import au.com.signonsitenew.events.RxBusUpdatePermitDetails
import au.com.signonsitenew.utilities.*
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface PermitDetailsFragmentPresenter {
    fun inject(display: PermitDetailsDisplay)
    fun setPermitDetailsState(permitInfo: PermitInfo)
    fun updateAnExistingPermit(permitState: String)
    fun observeStates()
    fun observeSaveButtonStates()
    fun observeTeamMembersTab()
    fun getFullPermitInfoAsync()
    fun getPermitInfo():PermitInfo?
    fun setPermitInfo(permitInfo: PermitInfo)
    fun observeCtaState()
    fun setPermit(permit: Permit)
    fun launchCancelAlertDialog()
    fun saveUserResponses()
    fun observeForUnSavedResponses()
    fun clearPermitResponses()
    fun hideSaveButton()
    fun getUserPermitResponses():List<ComponentTypeForm>
    fun hasUserUnsavedResponses():Boolean

}

class PermitDetailsFragmentPresenterImpl  @Inject constructor(private val permitUseCase: PermitsUseCase,
                                                              private val rxBusUpdatePermitDetails:RxBusUpdatePermitDetails,
                                                              private val rxBusSaveButtonState: RxBusSaveButtonState,
                                                              private val rxBusUserPermitResponses: RxBusUserPermitResponses,
                                                              private val rxBusSelectedTeamMember: RxBusSelectedTeamMember,
                                                              private val logger: Logger):BasePresenter(), PermitDetailsFragmentPresenter {
    lateinit var display:PermitDetailsDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<PermitDetailsFragmentState>()
    private val hasPermitUnsavedValues: () -> Boolean
        get() = {
        when{
            permitUseCase.getListOfMembers().isNotEmpty() -> true
            permitUseCase.getPermitInfo()?.start_date.isNullOrEmpty() -> false
            permitUseCase.getPermitInfo()?.end_date.isNullOrEmpty() -> false
            else -> true
        }
    }

    override fun inject(display: PermitDetailsDisplay) {
        this.display = display
    }

    override fun setPermitDetailsState(permitInfo: PermitInfo) {
        uiStateObservable.onNext(PermitDetailsFragmentState.SetPermitDetails(null,permitInfo))
    }

    override fun observeStates() {
        disposable.add(uiStateObservable.subscribe {
            when(it){
                is PermitDetailsFragmentState.SetPermitDetails -> {
                    display.setPermitStateTitle(permitUseCase.mapPermitStateTitle(it.permitInfo.state))
                    display.showPermitHumanId()
                    display.setPermitHumanId(it.permitInfo.human_id)
                    display.setSmallPermitDescription(permitUseCase.buildSmallPermitInfoDescription(it.permitInfo))
                    display.setPermitDetailsViewPagerAdapter(it.permit,it.permitInfo)
                    display.setCtaStateForExitingPermit(it.permitInfo)
                    showPermitToolbar(it.permitInfo)
                }
                is PermitDetailsFragmentState.ShowError ->{
                    display.showErrors()
                }
                is PermitDetailsFragmentState.ShowPermitError ->{
                    display.showPermitError()
                }
                is PermitDetailsFragmentState.ShowCancelAlertDialogInRequest ->{
                    display.showCancelAlertDialogForRequestState()
                }
                is PermitDetailsFragmentState.ShowCancelAlertDialogInProgress ->{
                    display.showCancelAlertDialogForInProgressState()
                }
                is PermitDetailsFragmentState.NavigateToCurrentPermits ->{
                    display.navigateToCurrentPermits()
                }
                is PermitDetailsFragmentState.ShowSaveButton ->{
                    display.showSaveButton()
                }
                is PermitDetailsFragmentState.HideSaveButton ->{
                    display.hideSaveButton()
                }
                is PermitDetailsFragmentState.ShowProgressView ->{
                    display.showProgressView()
                }
                is PermitDetailsFragmentState.RemoveProgressView ->{
                    display.hideProgressView()
                }
            }
        })
    }

    override fun observeSaveButtonStates() {
        disposables.add(rxBusSaveButtonState
                .toSendSaveButtonObservable()
                .subscribe ({
                    when(it){
                        SaveButtonState.ShowButton -> uiStateObservable.onNext(PermitDetailsFragmentState.ShowSaveButton)
                        SaveButtonState.HideButton -> uiStateObservable.onNext(PermitDetailsFragmentState.HideSaveButton)
                    }
                }, {
                    logger.e(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::observeSaveButtonStates.name to it.message))
                }))
    }


    override fun observeTeamMembersTab() {
        disposables.add(rxBusSelectedTeamMember.toSelectedMembersObservable()
                .subscribe{
                    permitUseCase.setListOfMembers(it as List<RequesteeUser>)
                })
    }

    override fun getFullPermitInfoAsync() {
        disposables.add(permitUseCase.retrievePermitInfoAsync()
            .subscribe({
                if(it.status == Constants.JSON_SUCCESS) {
                    permitUseCase.setPermitInfo(it.permit)
                    uiStateObservable.onNext(PermitDetailsFragmentState.SetPermitDetails(permitUseCase.retrievePermit(),it.permit))
                } else {
                    uiStateObservable.onNext(PermitDetailsFragmentState.ShowPermitError)
                    logger.w(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::getFullPermitInfoAsync.name to it.status))
                }
            },{
                uiStateObservable.onNext(PermitDetailsFragmentState.ShowError)
                logger.e(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::getFullPermitInfoAsync.name to it.message))
            }))
    }

    override fun updateAnExistingPermit(permitState: String) {
        val request = updatePermitInfoRequest {
            state = permitState
            requestee_users = null
            start_date = if(permitUseCase.getPermitInfo()?.start_date != null) permitUseCase.getPermitInfo()?.start_date else null
            end_date = if(permitUseCase.getPermitInfo()?.end_date != null) permitUseCase.getPermitInfo()?.end_date else null
        }
        disposables.add(permitUseCase.updateCurrentPermitAsync(request)
            .subscribe({
                if(it.status == Constants.JSON_SUCCESS){
                    uiStateObservable.onNext(PermitDetailsFragmentState.NavigateToCurrentPermits)
                }else{
                    logger.w(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::updateAnExistingPermit.name to toJson(it)))
                }
            },{
                logger.e(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::updateAnExistingPermit.name to it.message))
            }))
    }

    override fun getPermitInfo(): PermitInfo? = permitUseCase.getPermitInfo()
    override fun setPermitInfo(permitInfo: PermitInfo) = permitUseCase.setPermitInfo(permitInfo)
    override fun observeCtaState() {
        disposables.add(rxBusUpdatePermitDetails.toSendPermitDetailsBusObservable()
            .subscribe {
                permitUseCase.setPermitInfo(it)
                uiStateObservable.onNext(PermitDetailsFragmentState.NavigateToCurrentPermits)
                showPermitToolbar(it)
            })
    }

    override fun setPermit(permit: Permit) = permitUseCase.setPermit(permit)
    override fun launchCancelAlertDialog() {
        when (permitUseCase.mapPermitInfoToPermitDetailsState()){
            PermitDetailsFragmentState.ShowCancelAlertDialogInRequest -> display.showCancelAlertDialogForRequestState()
            else -> display.showCancelAlertDialogForInProgressState()
        }
    }

    override fun saveUserResponses() {
        disposables.add(Single.just(hasPermitUnsavedValues())
                .flatMap {
                    when (it){
                        true -> return@flatMap permitUseCase.updateCurrentPermitAsync(permitUseCase.buildPermitInfoRequest())
                        false-> return@flatMap Single.just(it)
                    }
                }.flatMap {
                    when (permitUseCase.getUserPermitResponses().isNotEmpty()){
                        true -> return@flatMap permitUseCase.saveUserResponses(ComponentSaveResponsesRequest(permitUseCase.getUserPermitResponses()))
                        else -> return@flatMap Single.just(it)
                    }
                }
                .compose(applySchedulers())
                .doOnSubscribe { uiStateObservable.onNext(PermitDetailsFragmentState.ShowProgressView) }
                .doAfterTerminate { uiStateObservable.onNext(PermitDetailsFragmentState.RemoveProgressView) }
                .subscribe({ when (it) {
                       is ApiResponse -> {
                           if (it.status == Constants.JSON_SUCCESS) {
                               if (hasPermitUnsavedValues()) {
                                   uiStateObservable.onNext(PermitDetailsFragmentState.HideSaveButton)
                                   permitUseCase.clearPermitResponses()
                               }
                           } else {
                               logger.w(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::saveUserResponses.name to toJson(it)))
                           }
                       }
                }
                },{
                    logger.e(this@PermitDetailsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@PermitDetailsFragmentPresenterImpl::saveUserResponses.name to it.message))
                })
        )
    }

    override fun observeForUnSavedResponses() {
        disposables.add(rxBusUserPermitResponses.toSendUserPermitResponseObservable()
                .subscribe {
                    permitUseCase.setUserPermitResponses(it)
                })
    }

    override fun clearPermitResponses() {
        permitUseCase.clearPermitResponses()
    }

    override fun hideSaveButton() {
        uiStateObservable.onNext(PermitDetailsFragmentState.HideSaveButton)
    }

    override fun getUserPermitResponses(): List<ComponentTypeForm> {
        return permitUseCase.getUserPermitResponses()
    }

    override fun hasUserUnsavedResponses(): Boolean {
        return permitUseCase.hasUserUnsavedResponses()
    }

    private fun showPermitToolbar(permitInfo: PermitInfo) {
        when(permitUseCase.mapPermitInfoToToolbarState(permitInfo)){
            PermitDetailsFragmentState.ShowPermitToolbar -> display.showPermitToolbar()
            else -> display.hidePermitToolbar()
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        disposable.dispose()
    }
}