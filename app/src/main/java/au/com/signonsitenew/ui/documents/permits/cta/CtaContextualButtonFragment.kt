package au.com.signonsitenew.ui.documents.permits.cta

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentCtaContextualButtonBinding
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class CtaContextualButtonFragment : DaggerFragment(), CtaContextualButtonDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router

    private val presenter : CtaContextualButtonFragmentPresenter by viewModels { viewModelFactory}
    private var _binding: FragmentCtaContextualButtonBinding? = null
    private val binding get() = _binding!!
    private var permitInfo: PermitInfo? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCtaContextualButtonBinding.inflate(inflater,container, false)
        arguments?.let {
            permitInfo = it.getParcelable(Constants.PERMIT_INFO_OBJECT)
        }
        presenter.inject(this)
        presenter.observeState()
        presenter.observeForRemoteCtaState()
        presenter.observeTeamMembersTab()
        presenter.observeTaskTab()
        presenter.observeTasksResponses()
        presenter.observeChecksResponses()
        presenter.clearListOfTeamMembers()
        presenter.clearListOfComponents()
        permitInfo?.let { permitInfo ->
            presenter.setPermitInfo(permitInfo)
            presenter.setOriginalPermitInfo(permitInfo)
            presenter.setCtaButtonTitle(permitInfo)
            binding.ctaContextualButton.setOnClickListener { presenter.onClickCtaPermitListener() }
        }

        return binding.root
    }

    override fun showSendToTeamButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.send_to_team_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_button_primary,null)
        binding.ctaContextualButton.isEnabled = true
    }

    override fun showSendToTeamDisableButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.send_to_team_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.grey))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_list_header_border_grey,null)
        binding.ctaContextualButton.isEnabled = false
    }

    override fun showSendToTeamAction() {
        permitInfo?.let { presenter.updateAnExistingPermit(presenter.retrievePermitInfo(),false) }

    }

    override fun showObtainApprovalButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.obtain_approval_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_button_primary,null)
        binding.ctaContextualButton.isEnabled = true
    }

    override fun showObtainApprovalDisableButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.obtain_approval_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.grey))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_list_header_border_grey,null)
        binding.ctaContextualButton.isEnabled = false
    }

    override fun showObtainApprovalInRequestMessage() {
        AlertDialogMessageHelper.permitObtainApprovalInRequestMessage(requireContext()){
            permitInfo?.let {
                val permitInfo = presenter.retrievePermitInfo()
                permitInfo.state = PermitState.PendingApproval
                presenter.updateAnExistingPermit(permitInfo,false)
            }
        }
    }

    override fun showSendToTeamActionWithWarningMessage() {
        AlertDialogMessageHelper.showSendToTeamWarningMessage(requireContext()) {
            showSendToTeamAction()
        }
    }

    override fun showDoneInRequestButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.done_button_text)
    }

    override fun showDoneInProgressButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.done_button_text)
    }

    override fun showApproveRejectButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.approve_reject_button_text)
    }

    override fun showApproveRejectInPendingApprovalMessage() {
        AlertDialogMessageHelper.permitApproveRejectInPendingApproval(requireContext(),{
            val permitInfo = presenter.retrievePermitInfo()
            permitInfo.state = PermitState.InProgress
            presenter.updateAnExistingPermit(permitInfo, false)
        },{
            val permitInfo = presenter.retrievePermitInfo()
            permitInfo.state = PermitState.Request
            presenter.updateAnExistingPermit(permitInfo, true)
        })
    }

    override fun showDoneInProgressMessage() {
        AlertDialogMessageHelper.permitDoneInProgressMessage(requireContext()){
            presenter.markAsDonePermit(Constants.PERMIT_FINISHED_WORK){
                val permitInfo = it
                permitInfo.state = PermitState.Closed
                presenter.updateAnExistingPermit(permitInfo, false)
            }
        }
    }

    override fun showDoneInRequestMessage() {
        AlertDialogMessageHelper.permitDoneInRequestMessage(requireContext()){
            presenter.markAsDonePermit(Constants.PERMIT_FINISHED_REQUEST){
                presenter.updateAnExistingPermit(it, false)
            }
        }
    }

    override fun hideButton() {
        binding.ctaContextualButton.visibility = View.INVISIBLE
    }

    override fun showSendForClosureButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.send_for_closure_button_text)
    }

    override fun showSendForClosureDisableButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.send_for_closure_button_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.grey))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_list_header_border_grey,null)
    }

    override fun showSendForClosureInProgressMessage() {
        AlertDialogMessageHelper.permitSendForClosureInProgressMessage(requireContext()){
            val permitInfo = presenter.retrievePermitInfo()
            permitInfo.state = PermitState.PendingClosure
            presenter.updateAnExistingPermit(permitInfo, false)
        }
    }

    override fun showRejectClosureButton() {
        this.permitInfo = permitInfo
        binding.ctaContextualButton.text = resources.getString(R.string.reject_closure_button_text)
    }

    override fun showRejectClosureInClosingMessage() {

    }

    override fun showApproveRejectClosureButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.approve_reject_button_text)
    }

    override fun showApproveRejectDisableButton() {
        binding.ctaContextualButton.text = resources.getString(R.string.approve_reject_button_text)
        binding.ctaContextualButton.setTextColor(ContextCompat.getColor(requireContext(),R.color.grey))
        binding.ctaContextualButton.background = resources.getDrawable(R.drawable.custom_list_header_border_grey,null)
        binding.ctaContextualButton.isEnabled = false
    }

    override fun showApproveRejectClosureInClosingMessage() {
        //TODO("Not yet implemented")
    }

    override fun showPermitDateBeforeNowError() {
        AlertDialogMessageHelper.permitDateBeforeNowError(requireContext())
    }

    override fun showProgressView() {
        router.showProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun removeProgressView() {
        router.removeProgressView(requireActivity(),R.id.doc_permits_container)
    }

}