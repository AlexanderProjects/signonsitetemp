package au.com.signonsitenew.ui.attendanceregister;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.R;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

import static android.app.Activity.RESULT_OK;

/**
 * A simple Fragment containing a WebView that we use to display SignOnSite forms.
 *
 * The class utilises WebFormInterface to map Javascript functionality to native Android functions.
 *
 * For example, the WebFormInterface is initialised with the name "Android", so in our JS we can put
 *
 * <script type="text/javascript">
 *   function doAndroidFunction(someArgument) {
 *     Android.webInterfaceDefinedFunction(someArgument);
 *   }
 * </script>
 */
public class ManualInductionFragment extends Fragment {
    private static final String TAG = ManualInductionFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String ARG_ATTENDEE_ID = "attendeeId";

    private static String FILE_TYPE = "image/*";
    private static boolean FILE_UPLOAD_ENABLED = true;
    private static boolean CAM_UPLOAD_ENABLED  = true;

    private String mCamMessage;
    private ValueCallback<Uri> mFileMessage;
    private ValueCallback<Uri[]> mFilePath;
    private final static int mFileReq = 1;
    private static final int mFilePerm = 2;

    protected WebView mWebView;
    protected Button mSubmitButton;

    private ProgressDialog mDialog;

    private long mAttendeeId;

    public ManualInductionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Hide the parent Toolbar for more screen estate
        ((AttendanceActivity)getActivity()).mToolbar.setVisibility(View.GONE);

        // get the ID
        if (getArguments() != null) {
            mAttendeeId = getArguments().getLong(ARG_ATTENDEE_ID);
        }


        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_induction, container, false);

        mWebView = rootView.findViewById(R.id.webview);
        mSubmitButton = getActivity().findViewById(R.id.next_button);

        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Preparing form...");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(true);
        mDialog.show();

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Prepare URL and Headers for the WebView
        int siteId = new SessionManager(getActivity()).getSiteId();

        String url = Constants.URL_SITE_INDUCTION_BASE + siteId + "/inductions/users/" + mAttendeeId + "/manual";
        String jwt = new SessionManager(getActivity()).getToken();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + jwt);
        mWebView.loadUrl(url, headers);

        mWebView.addJavascriptInterface(new ManualInductionFormViewerInterface(getActivity()), "Android");

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                mDialog.dismiss();
                super.onPageFinished(view, url);
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("SignOnSite", consoleMessage.message() + " -- From line "
                    + consoleMessage.lineNumber() + " of "
                    + consoleMessage.sourceId());
                return super.onConsoleMessage(consoleMessage);
            }

            //Handling input[type="file"] requests for android API 16+
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
                if (FILE_UPLOAD_ENABLED) {
                    mFileMessage = uploadMsg;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType(FILE_TYPE);
                    startActivityForResult(Intent.createChooser(intent, "File Chooser"), mFileReq);
                }
            }

            //Handling input[type="file"] requests for android API 21+
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (!checkPermission(0) || !checkPermission(1)) {
                    // SignOnSite needs a permission to proceed
                    showPermissionDialog();
                    return false;
                }
                if(FILE_UPLOAD_ENABLED) {
                    if (mFilePath != null) {
                        mFilePath.onReceiveValue(null);
                    }
                    mFilePath = filePathCallback;
                    Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    contentSelectionIntent.setType(FILE_TYPE);
                    Intent[] intentArray;

                    if (CAM_UPLOAD_ENABLED) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                photoFile = createImage();
                                takePictureIntent.putExtra("PhotoPath", mCamMessage);
                            }
                            catch (IOException e) {
                                Log.e(TAG, "Image file creation failed", e);
                            }

                            if (photoFile != null) {
                                mCamMessage = "file:" + photoFile.getAbsolutePath();
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            }
                            else {
                                takePictureIntent = null;
                            }
                        }

                        if (takePictureIntent != null) {
                            intentArray = new Intent[]{takePictureIntent};
                        }
                        else {
                            intentArray = new Intent[0];
                        }
                    }
                    else {
                        intentArray = new Intent[0];
                    }
                    Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "File Chooser");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                    startActivityForResult(chooserIntent, mFileReq);
                }
                return true;
            }
        });

        return rootView;
    }

    public void showPermissionDialog() {
        SLog.i(TAG, "Displaying camera + read/write info dialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Let SignOnSite Access Camera and take photos")
                .setMessage("SignOnSite needs to access the camera and phone storage to allow you " +
                        "to take photos and upload them with your induction form.")
                .setPositiveButton("Give Access", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        // User accepted at initial screen - open proper permission dialog
                        requestRequiredPermissions();
                    }
                })
                .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject jsonData = new JSONObject();
                        try {
                            jsonData.put("state", "soft_deny");
                        }
                        catch (JSONException e) {
                            SLog.e(TAG, "JSON Exception occurred: " + e.getMessage());
                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //Checking permission for storage and camera for writing and uploading images
    public void requestRequiredPermissions() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        //Checking for storage permission to write images for upload
        if (FILE_UPLOAD_ENABLED && CAM_UPLOAD_ENABLED && !checkPermission(0) && !checkPermission(1)) {
            ActivityCompat.requestPermissions(getActivity(), perms, mFilePerm);

            //Checking for WRITE_EXTERNAL_STORAGE permission
        }
        else if (FILE_UPLOAD_ENABLED && !checkPermission(0)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, mFilePerm);

            //Checking for CAMERA permissions
        }
        else if (CAM_UPLOAD_ENABLED && !checkPermission(1)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, mFilePerm);
        }
    }

    //Checking if particular permission has been granted
    public boolean checkPermission(int permission) {
        switch(permission){
            case 0:
                return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

            case 1:
                return ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

        }
        return false;
    }

    //Creating image file for upload
    private File createImage() throws IOException {
        String file_name  = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
        String new_name   = "file_"+file_name+"_";
        File sd_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(new_name, ".jpg", sd_directory);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.orange_primary));
            Uri[] results = null;
            if (resultCode == RESULT_OK) {
                if (requestCode == mFileReq) {
                    if (null == mFilePath) {
                        return;
                    }

                    if (intent == null) {
                        if (mCamMessage != null) {
                            results = new Uri[]{Uri.parse(mCamMessage)};
                        }
                    }
                    else {
                        String dataString = intent.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{ Uri.parse(dataString) };
                        }
                    }
                }
            }
            mFilePath.onReceiveValue(results);
            mFilePath = null;
        }
        else {
            if (requestCode == mFileReq) {
                if (null == mFileMessage) return;
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mFileMessage.onReceiveValue(result);
                mFileMessage = null;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "Request Code: " + requestCode);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                            .setTitle("Congratulations! You're ready to go")
                            .setMessage("Click on the photo buttons to get started!")
                            .setPositiveButton("Let\'s do this", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    // User accepted at initial screen - open proper permission dialog
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    DarkToast.makeText(getActivity(), "You will need to grant permission to upload photos");
                }
            }
        }
    }
}
