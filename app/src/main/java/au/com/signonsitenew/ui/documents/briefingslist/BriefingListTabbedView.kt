package au.com.signonsitenew.ui.documents.briefingslist

import au.com.signonsitenew.models.BriefingsResponse

interface BriefingListTabbedView {
    fun updateData(response: BriefingsResponse)
    fun showError(error: String)
}