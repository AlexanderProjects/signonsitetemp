package au.com.signonsitenew.ui.main.documents

import au.com.signonsitenew.domain.models.Induction
import au.com.signonsitenew.domain.models.WorkerBriefing

interface DocumentsDisplay {
    fun updateDocumentsState(workerBriefing: WorkerBriefing?, induction: Induction?)
    fun showDataErrors(error:String)
    fun showNetworkError()
}