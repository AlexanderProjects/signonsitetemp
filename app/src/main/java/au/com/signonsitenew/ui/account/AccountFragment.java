package au.com.signonsitenew.ui.account;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * A placeholder fragment containing a simple view.
 */
public class AccountFragment extends Fragment {

    private static final String LOG = AccountFragment.class.getSimpleName();

    protected TextView mEmailTextView;
    protected LinearLayout mNameCell;
    protected LinearLayout mPhoneNumberCell;
    protected LinearLayout mPasswordCell;
    protected LinearLayout mEmployerCell;

    private OnNameCellClickListener mNameCellListener;
    private OnNumberCellClickListener mNumberCellListener;
    private OnPasswordCellClickListener mPasswordCellListener;
    private OnEmployerCellClickListener mEmployerCellListener;

    public AccountFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_account, container, false);

        // Set title
        ((AccountActivity)getActivity()).mTitleTextView.setText("Account");

        mEmailTextView = (TextView)rootView.findViewById(R.id.account_email_text_view);
        mNameCell = (LinearLayout)rootView.findViewById(R.id.account_name_cell);
        mPhoneNumberCell = (LinearLayout)rootView.findViewById(R.id.account_mobile_cell);
        mPasswordCell = (LinearLayout)rootView.findViewById(R.id.account_password_cell);
        mEmployerCell = (LinearLayout)rootView.findViewById(R.id.account_employer_cell);

        // Set the user's email
        String email;
        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
        User user = realm.where(User.class).findFirst();
        if (user != null) {
            email = user.getEmail();
        }
        else {
            email = new SessionManager(getActivity()).getCurrentUser().get(Constants.USER_EMAIL);
        }
        realm.close();
        mEmailTextView.setText(email);

        mNameCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load Name Fragment
                if (mNameCellListener != null) {
                    mNameCellListener.onNameCellClick();
                }
            }
        });

        mPhoneNumberCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load Phone Number Fragment
                if (mNumberCellListener != null) {
                    mNumberCellListener.onNumberCellClick();
                }
            }
        });

        mPasswordCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load Password Fragment
                if (mPasswordCellListener != null) {
                    mPasswordCellListener.onPasswordCellClick();
                }
            }
        });

        mEmployerCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load Employer Fragment
                if (mEmployerCellListener != null) {
                    mEmployerCellListener.onEmployerCellClick();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNameCellClickListener) {
            mNameCellListener = (OnNameCellClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNameCellClickListener");
        }

        if (context instanceof OnNumberCellClickListener) {
            mNumberCellListener = (OnNumberCellClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnNumberCellClickListener");
        }

        if (context instanceof OnPasswordCellClickListener) {
            mPasswordCellListener = (OnPasswordCellClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPasswordCellClickListener");
        }

        if (context instanceof OnEmployerCellClickListener) {
            mEmployerCellListener = (OnEmployerCellClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEmployerCellClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mNameCellListener = null;
        mNumberCellListener = null;
        mPasswordCellListener = null;
        mEmployerCellListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnNameCellClickListener {
        void onNameCellClick();
    }

    public interface OnNumberCellClickListener {
        void onNumberCellClick();
    }

    public interface OnPasswordCellClickListener {
        void onPasswordCellClick();
    }

    public interface OnEmployerCellClickListener {
        void onEmployerCellClick();
    }
}
