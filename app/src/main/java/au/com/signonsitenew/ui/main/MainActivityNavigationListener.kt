package au.com.signonsitenew.ui.main

interface MainActivityNavigationListener {
    fun navigateToTab(index: Int)
}