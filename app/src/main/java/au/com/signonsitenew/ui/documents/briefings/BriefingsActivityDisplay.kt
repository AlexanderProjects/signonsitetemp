package au.com.signonsitenew.ui.documents.briefings

interface BriefingsActivityDisplay {
    fun showCreateBriefingConfirmation()
    fun showDataErrors(error:String)
    fun showNetworkError()
    fun showProgressDialog()
}