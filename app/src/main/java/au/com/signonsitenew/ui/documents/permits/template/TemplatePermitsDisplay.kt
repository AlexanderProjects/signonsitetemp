package au.com.signonsitenew.ui.documents.permits.template

import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SitePermitTemplate

interface TemplatePermitsDisplay {
    fun showPermitTemplateList(permitTemplateList:List<SitePermitTemplate>)
    fun showPermitTemplateError()
    fun navigateToPermitDetails(permitInfo: PermitInfo)
    fun showProgressView()
    fun removeProgressView()
}