package au.com.signonsitenew.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.Enrolment

class ConnectionsRecyclerViewAdapter(private var enrolments:List<Enrolment>, private var onClick:(enrolment:Enrolment)->Unit, private var setTimeZone:(enrolment:Enrolment)->String) : RecyclerView.Adapter<ConnectionsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_site_connection,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = enrolments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.siteName.text = enrolments[position].site.name
        holder.companyName.text = enrolments[position].site.principal_company.name
        holder.siteAddress.text = enrolments[position].site.address
        holder.signOnAs.text = enrolments[position].company.name
        holder.lastSignOn.text = when(enrolments[position].utc_last_signon_at){
            null -> "None"
            else -> setTimeZone(enrolments[position])

        }
        enrolments[position].site_induction?.state.let {
            holder.inductionStatus.text = when(it){
                "acknowledged" -> "Inducted"
                "pending" -> "Pending"
                "rejected" -> "Not Inducted"
                 else -> "Not Inducted"
            }
            holder.inductionStatus.visibility = when (it){
                "pending" -> View.VISIBLE
                "acknowledged" -> View.VISIBLE
                null -> View.VISIBLE
                else -> View.GONE
            }
            holder.inductionStatusTitle.visibility = when(it){
                "pending" -> View.VISIBLE
                "acknowledged" -> View.VISIBLE
                null -> View.VISIBLE
                else -> View.GONE
            }
            holder.inductionStatus.setTextColor(when(it){
                "pending" -> Color.GRAY
                "acknowledged" -> Color.GRAY
                null -> Color.RED
                else -> Color.GRAY
            })
            holder.inductionStatusTitle.setTextColor(when(it){
                "pending" -> Color.GRAY
                "acknowledged" -> Color.GRAY
                null -> Color.RED
                else -> Color.GRAY
            })
            holder.inductionTick.setImageResource(when(it){
                "pending" -> R.drawable.ic_check_circle_green_24dp
                "acknowledged" -> R.drawable.ic_check_circle_green_24dp
                null -> R.drawable.ic_warning_red_24dp
                else -> R.drawable.ic_warning_red_24dp
            })
        }
        holder.view.setOnClickListener { onClick(enrolments[position]) }

    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val siteName: TextView = view.findViewById(R.id.site_name_in_connection)
        val companyName: TextView = view.findViewById(R.id.company_name_in_connection)
        val siteAddress: TextView = view.findViewById(R.id.site_address_in_connection)
        val signOnAs: TextView = view.findViewById(R.id.sign_on_as_texview)
        val lastSignOn: TextView = view.findViewById(R.id.last_sign_on_textview)
        val inductionStatus: TextView = view.findViewById(R.id.induction_status_textview)
        val inductionStatusTitle:TextView = view.findViewById(R.id.induction_status_title)
        val inductionTick: ImageView = view.findViewById(R.id.inducted_tick)
    }



}
