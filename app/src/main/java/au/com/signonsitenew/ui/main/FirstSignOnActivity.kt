package au.com.signonsitenew.ui.main

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import au.com.signonsitenew.R
import au.com.signonsitenew.api.SOSAPI
import au.com.signonsitenew.domain.models.CompanyPropertyType
import au.com.signonsitenew.domain.models.CompanySegmentProperties
import au.com.signonsitenew.domain.models.NearSite
import au.com.signonsitenew.domain.models.SignOnOffType
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService
import au.com.signonsitenew.realm.Company
import au.com.signonsitenew.realm.EnrolledSite
import au.com.signonsitenew.realm.RealmManager
import au.com.signonsitenew.ui.main.SiteCompanySelectFragment.OnCompanyInteractionListener
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SLog
import au.com.signonsitenew.utilities.SessionManager
import dagger.android.support.DaggerAppCompatActivity
import io.realm.Realm
import org.json.JSONException
import org.json.JSONObject
import javax.inject.Inject

/**
 * Created by Krishan Caldwell on 23/10/2018.
 *
 * Activity to dictate the sign on flow for users when they first sign onto a site.
 * This will only work on sites who have enabled this feature.
 *
 * This aspect of the app may evolve into a more involved process for the user in the future. For
 * the time being, it will only prompt the user to select the company they are representing on site.
 * It will also provide confirmation of this to user to prevent any fat thumbs.
 *
 * This activity hosts a container that controls the flow of fragments relevant to this flow.
 */

class FirstSignOnActivity : DaggerAppCompatActivity(), OnCompanyInteractionListener, SiteConfirmationFragment.OnConfirmationSelected, SiteAddNewCompanyFragment.OnConfirmationSelected {

    val LOG = FirstSignOnActivity::class.java.simpleName

    @Inject
    lateinit var notificationUseCase: NotificationUseCase

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService

    lateinit var mTitleTextView: TextView
    private lateinit var currentLocation :Location
    private var siteId :Int = 0
    private lateinit var nearSite:NearSite
    private lateinit var mFragmentManager: FragmentManager

    companion object {
        private const val ARG_COMPANY = "selected_company"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_sign_on)
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = ""
        setSupportActionBar(toolbar)


        if (intent.getParcelableExtra<Location>("location") == null){ navigateToMainActivity()}
        currentLocation = intent.getParcelableExtra("location")!!
        nearSite = intent.getParcelableExtra(Constants.NEAR_SITE)!!
        siteId = intent.getIntExtra("siteId", 0)

        mTitleTextView = toolbar.findViewById<View>(R.id.toolbar_title_text_view) as TextView

        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        mFragmentManager = supportFragmentManager

        loadWelcomeFragment()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                when (mFragmentManager.findFragmentById(R.id.container)) {
                    is SiteConfirmationFragment -> supportFragmentManager.popBackStack()
                    is SiteCompanySelectFragment -> supportFragmentManager.popBackStack()
                    is SiteAddNewCompanyFragment -> supportFragmentManager.popBackStack()
                    is SiteWelcomeFragment -> navigateToMainActivity()
                }
            }
        }
        return true
    }

    /**
     * Loads the initial fragment
     */
    private fun loadWelcomeFragment() {
        val fragment = SiteWelcomeFragment()
        val args = Bundle()
        args.putParcelable(Constants.NEAR_SITE, nearSite)
        fragment.arguments = args
        val transaction = mFragmentManager.beginTransaction()
        transaction.add(R.id.container, fragment)
        transaction.commit()
    }

    private fun navigateToConfirmationFragment(company: String) {
        val fragment = SiteConfirmationFragment()
        val args = Bundle()
        args.putString(ARG_COMPANY, company)
        fragment.arguments = args
        val transaction = mFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        transaction.replace(R.id.container, fragment)

        // Add the transaction to the back stack to preserve back navigation
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun navigateToSiteCompanySelectFragment() {
        val fragment = SiteCompanySelectFragment()
        val transaction = mFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun navigateToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    fun navigateToAddNewCompanyFragment() {
        val fragment = SiteAddNewCompanyFragment()
        val transaction = mFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCompanySelected(company: Company) {

        // Move to confirmation fragment if company, otherwise move to add new company
        if (company.companyId != 0L) {
            navigateToConfirmationFragment(company.name)
            analyticsEventDelegateService.signedOn(SignOnOffType.Manual, CompanySegmentProperties(CompanyPropertyType.FromList,company.name),true)
        } else {
            navigateToAddNewCompanyFragment()
        }
    }

    override fun onCompanyConfirmed(companyName: String) {
        SOSAPI(this).signOnSite(false,siteId,currentLocation,companyName, { result ->
            try {
                val jsonResult = JSONObject(result)
                if (jsonResult.getString(Constants.JSON_STATUS) == Constants.JSON_SUCCESS) {
                    SLog.i(MainActivity.LOG, "Manual sign on successful")
                    val session = SessionManager(this)
                    session.siteId = siteId
                    session.setSignOnTime()
                    // Add/update induction status to Realm
                    val realm = Realm.getInstance(RealmManager.getRealmConfiguration())
                    val site = realm.where(EnrolledSite::class.java).equalTo("id", siteId).findFirst()
                    realm.beginTransaction()
                    if (site != null) {
                        site.setInducted(jsonResult.getBoolean("inducted"))
                    } else { // Create a new site with all the fields to maintain the inducted status
                        val newSite = EnrolledSite()
                        newSite.id = jsonResult.getLong("site_id")
                        newSite.name = jsonResult.getString("site_name")
                        newSite.points = jsonResult.getString("points")
                        newSite.managerName = jsonResult.getString("address")
                        newSite.siteAddress = jsonResult.getString("company_name")
                        newSite.regionLat = jsonResult.getDouble("lat")
                        newSite.regionLong = jsonResult.getDouble("long")
                        newSite.regionRadius = jsonResult.getDouble("radius")
                        newSite.setInducted(jsonResult.getBoolean("inducted"))
                        realm.copyToRealmOrUpdate(newSite)
                    }
                    realm.commitTransaction()
                    realm.close()
                    // Send relevant notifications
                    notificationUseCase.isUserSignedOn(true).setSiteId(siteId).hasPrompt(false).buildSiteNotifications()
                    // Re-fetch the users region list as they now have a new site entry potentially
                    RegionFetcherJobService.schedule(this)
                    // Load SignedOnActivity
                    navigateToSignedOnState()
                    // Send sign on segment event
                    analyticsEventDelegateService.signedOn(SignOnOffType.Manual, CompanySegmentProperties(CompanyPropertyType.Custom,companyName),true)
                }
            } catch (e: JSONException) {
                SLog.i(LOG, "There was a JSON Exception: " + e.message)
            }
        }, {
            SLog.e(LOG, "There was a volley error")
        })

    }

    private fun navigateToSignedOnState(){
       val intent = Intent(this, SignedOnActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun onBackPressed() {}
}
