package au.com.signonsitenew.ui.prelogin.registration.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
class RegisterPasswordFragment : DaggerFragment() {

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService
    @Inject
    lateinit var logger: Logger

    private lateinit var onNextRegisterPasswordClickListener: OnNextRegisterPasswordClickListener
    private lateinit var mSetPasswordText: TextView
    private lateinit var mOldPassword: EditText
    private lateinit var mPassword: EditText
    private lateinit var mConfirmPassword: EditText
    private lateinit var mOldPasswordGroup: LinearLayout
    private lateinit var mSubmitPasswordButton: Button
    private lateinit var mSession: SessionManager

    fun setOnClickListener(onNextRegisterPasswordClickListener: OnNextRegisterPasswordClickListener){
        this.onNextRegisterPasswordClickListener = onNextRegisterPasswordClickListener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_register_password, container, false)
        mSession = SessionManager(requireActivity())
        mSetPasswordText = rootView.findViewById(R.id.setPasswordText)
        mOldPassword = rootView.findViewById(R.id.oldPasswordEditText)
        mPassword = rootView.findViewById(R.id.newPasswordEditText1)
        mConfirmPassword = rootView.findViewById(R.id.newPasswordEditText2)
        mOldPasswordGroup = rootView.findViewById(R.id.oldPasswordGroup)
        mSubmitPasswordButton = rootView.findViewById(R.id.submitPasswordButton)

        // Set appropriate description and button text
        mSetPasswordText.text = "Enter a strong password. \nInclude at least 6 characters."
        mSubmitPasswordButton.text = "Next"

        // Turn on/off appropriate views
        mOldPasswordGroup.visibility = View.GONE
        mSubmitPasswordButton.setOnClickListener(View.OnClickListener {
            // Grab variables, then set appropriate actions. Do this earlier for some variables
            // in this fragment as the fields available will change depending on if user is
            // editing or not
            val password = mPassword.text.toString().trim { it <= ' ' }
            val passwordConf = mConfirmPassword.text.toString().trim { it <= ' ' }

            // Validate
            when {
                password.isEmpty() -> {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),Constants.PASSWORD_EMPTY_ERROR)
                }
                password.length > 72 -> {
                    // Reject overly long passwords to avoid attacks on server
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),Constants.PASSWORD_MAX_LENGTH_ERROR)
                }
                password.length < 6 -> {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),Constants.PASSWORD_MIN_LENGTH_ERROR)
                }
                password != passwordConf -> {
                    AlertDialogMessageHelper.registrationErrorMessage(requireActivity(),Constants.PASSWORDS_DO_NOT_MATCH)
                }
                else -> {
                    mSession.updateRegisterPass(password)
                    analyticsEventDelegateService.registrationPassportProvided()
                    onNextRegisterPasswordClickListener.navigateToRegisterEmployers()
                    logger.d( this::class.java.name +" - "+ Constants.USER_PASSWORD_STEP_THREE,attributes = mapOf(Constants.USER_REGISTRATION_EMAIL to mSession.tempUserEmail))
                }
            }
        })
        return rootView
    }

    interface OnNextRegisterPasswordClickListener{
        fun navigateToRegisterEmployers()
    }
}