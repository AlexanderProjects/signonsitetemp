package au.com.signonsitenew.ui.documents.permits.current

import au.com.signonsitenew.domain.models.Permit

interface CurrentPermitsDisplay {
    fun showPermitList(permits:List<Permit>)
    fun showPermitsError()
    fun navigateToTemplatePermitFragment()
    fun navigateToPermitDetails(permit:Permit)
    fun showProgressView()
    fun removeProgressView()
}