package au.com.signonsitenew.ui.passport.credentials

import au.com.signonsitenew.domain.models.CredentialsResponse

interface CredentialsDataListener {
    fun showData(credentialsResponse: CredentialsResponse?)
}