package au.com.signonsitenew.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.utilities.DateFormatUtil
import au.com.signonsitenew.domain.utilities.Validator
import com.bumptech.glide.Glide

class CredentialsRecyclerViewAdapter(private var credentials: List<Credential?>, private val callAction: (Credential?) ->Unit) : RecyclerView.Adapter<CredentialsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_credential, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.credentialTypeName.text = credentials[position]?.name ?: credentials[position]?.credential_type?.name
        credentials[position]?.identifier?.let { viewHolder.credentialIdNumber.text = credentials[position]?.identifier.toString() }
        credentials[position]?.issued_by?.let { }
         if (credentials[position]?.issued_by != null){
             viewHolder.credentialIssuedLocation.text = credentials[position]?.issued_by
         }else{
             viewHolder.credentialIssuedLocation.visibility = View.GONE
             viewHolder.credentialIssuedByTitle.visibility = View.GONE
         }

        if(credentials[position]?.issue_date != null){
            viewHolder.credentialIssuedDate.visibility = View.VISIBLE
            viewHolder.credentialIssueDateTitle.visibility = View.VISIBLE
            viewHolder.credentialIssuedDate.text = DateFormatUtil.convertDateFormatResponse(credentials[position]?.issue_date)
        }else {
            viewHolder.credentialIssuedDate.visibility = View.GONE
            viewHolder.credentialIssueDateTitle.visibility = View.GONE
        }

        if (credentials[position]?.expiry_date != null) {
            viewHolder.credentialExpiryDate.visibility = View.VISIBLE
            viewHolder.credentialExpiryDateTitle.visibility = View.VISIBLE
            viewHolder.credentialExpiryDate.text = DateFormatUtil.convertDateFormatResponse(credentials[position]?.expiry_date)
            if (Validator.validateExpiryCredential(credentials[position])) {
                viewHolder.credentialExpiryDateTick.setImageResource(R.drawable.ic_warning_red_24dp)
            } else {
                viewHolder.credentialExpiryDateTick.setImageResource(R.drawable.ic_check_circle_green_24dp)
            }
        } else {
            viewHolder.credentialExpiryDate.visibility = View.GONE
            viewHolder.credentialExpiryDateTitle.visibility = View.GONE
            viewHolder.credentialExpiryDateTick.setImageResource(R.drawable.ic_check_circle_green_24dp)
        }

        credentials[position]?.front_photo_temporary_url.let {
            if(credentials[position]?.front_photo_temporary_url != null)
                Glide.with(viewHolder.view)
                    .load(credentials[position]?.front_photo_temporary_url)
                    .centerCrop()
                    .placeholder(R.drawable.whiteplaceholder)
                    .into(viewHolder.credentialPhoto)
            else
                viewHolder.credentialPhoto.setImageResource(R.drawable.whiteplaceholder)
        }

        viewHolder.view.setOnClickListener { callAction(credentials[position])}
    }

    override fun getItemCount(): Int {
        return credentials.size
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val credentialPhoto: ImageView = view.findViewById(R.id.credential_photo)
        val credentialExpiryDateTick: ImageView = view.findViewById(R.id.credential_expiry_tick)
        val credentialTypeName: TextView = view.findViewById(R.id.credential_type_name)
        val credentialIdNumber: TextView = view.findViewById(R.id.credential_id_number)
        val credentialIssuedByTitle:TextView = view.findViewById(R.id.credential_issue_by_title)
        val credentialIssuedLocation: TextView = view.findViewById(R.id.credential_issued_location)
        val credentialIssueDateTitle:TextView = view.findViewById(R.id.credential_issued_date_title)
        val credentialIssuedDate: TextView = view.findViewById(R.id.credential_issued_date)
        val credentialExpiryDate: TextView = view.findViewById(R.id.credential_expiry_date)
        val credentialExpiryDateTitle: TextView = view.findViewById(R.id.credential_expiry_date_title)
    }


}