package au.com.signonsitenew.ui.documents.permits.details.task

interface TaskDisplay {
    fun showStartDateRow()
    fun hideStarDateRow()
    fun showEndDateRow()
    fun hideEndDateRow()
    fun showStartDateArrowUp()
    fun showStartDateArrowDown()
    fun showEndDateArrowUp()
    fun showEndDateArrowDown()
    fun showEmptyStartDateMessage()
    fun showEmptyEndDateMessage()
    fun showStartDateTime(dateTime:String)
    fun showEndDateTime(dateTime: String)
    fun showComponents()
    fun setStartDateTimeInPermitInfo(dateTime: String)
    fun setEndDateTimeInPermitInfo(dateTime: String)
    fun hideChevrons()
    fun showChevrons()
    fun disableClickOnRows()
    fun enableClickOnRows()
}