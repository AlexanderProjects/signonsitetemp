package au.com.signonsitenew.ui.passport.share

import au.com.signonsitenew.domain.models.SharePassportRequest
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.sharepassport.SharePassportUseCase
import au.com.signonsitenew.domain.usecases.sharepassport.SharePassportUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface SharePassportPresenter{
    fun inject(sharePassportDisplay: SharePassportDisplay)
    fun validateEmail(email:String):Boolean
    fun sharePassport(sharePassportRequest: SharePassportRequest)
    fun buildRequest(email: String):SharePassportRequest
}
class SharePassportPresenterImpl @Inject constructor(private val sharePassportUseCase: SharePassportUseCaseImpl,
                                                     private val logger:Logger,
                                                     private val analyticsEventDelegateService: AnalyticsEventDelegateService ) : BasePresenter(), SharePassportPresenter {

    private lateinit var sharePassportDisplay: SharePassportDisplay
    private val disposables = CompositeDisposable()

    override fun inject(sharePassportDisplay: SharePassportDisplay) {this.sharePassportDisplay = sharePassportDisplay}

    override fun validateEmail(email:String):Boolean = sharePassportUseCase.isValidEmailAddress(email)

    override fun sharePassport(sharePassportRequest: SharePassportRequest) {
        disposables.add(sharePassportUseCase.sharePassport(sharePassportRequest)
                .subscribe({ response ->
                    if(NetworkErrorValidator.isValidResponse(response)) {
                        analyticsEventDelegateService.sharedPassport(sharePassportRequest.email_addresses[0])
                        sharePassportDisplay.showConfirmation()
                    }else{
                        sharePassportDisplay.showDataErrors(NetworkErrorValidator.getErrorMessage(response))
                        logger.w(this::class.java.name, attributes = mapOf(this::sharePassport.name to toJson(response)))
                    }
                },{
                    sharePassportDisplay.showNetworkErrors()
                    logger.e(this::class.java.name, attributes = mapOf(this::sharePassport.name to it.message))
                }))
    }

    override fun buildRequest(email: String):SharePassportRequest = sharePassportUseCase.buildShareRequest(email)

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}
