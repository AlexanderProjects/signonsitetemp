package au.com.signonsitenew.ui.documents.inductions;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.EnrolledSite;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

/**
 * First fragment of the Site Induction flow.
 */
public class InductionIntroFragment extends Fragment {

    private static final String LOG = InductionIntroFragment.class.getSimpleName();

    private ImageView mImageView;
    private TextView mInductionDescriptionTextView;

    private Realm mRealm;

    public InductionIntroFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Show the toolbar and next button
        ((InductionActivity)getActivity()).mToolbar.setVisibility(View.VISIBLE);
        ((InductionActivity)getActivity()).mNextButton.setVisibility(View.VISIBLE);

        // Initial instantiations
        mRealm = Realm.getDefaultInstance();

        View rootView = inflater.inflate(R.layout.fragment_induction_intro, container, false);

        mImageView = rootView.findViewById(R.id.logo_image_view);
        mInductionDescriptionTextView = rootView.findViewById(R.id.second_explainer_text_view);

        mImageView.setColorFilter(ContextCompat.getColor(getActivity(), R.color.grey_icon_highlight), PorterDuff.Mode.SRC_IN);

        // Set the appropriate text.
        int siteId = new SessionManager(getActivity()).getSiteId();
        EnrolledSite site = mRealm.where(EnrolledSite.class).equalTo("id", siteId).findFirst();
        String siteName = site.getName();
        if(siteName != null) {
            // The stored string files trim white space off the ends so we have to use a space like below...
            String textToShow = getActivity().getString(R.string.induction_site_text) + " " + siteName + getActivity().getString(R.string.induction_prefill_text);
            mInductionDescriptionTextView.setText(textToShow);
        }else{
            mInductionDescriptionTextView.setText("");
        }

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mRealm != null) {
            mRealm.close();
        }
    }

}
