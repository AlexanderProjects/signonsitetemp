package au.com.signonsitenew.ui.passport.credentials.credentialtypes

import au.com.signonsitenew.domain.models.CredentialType
import au.com.signonsitenew.domain.models.CredentialTypesResponse
import au.com.signonsitenew.domain.usecases.credentials.CredentialsUseCaseImpl
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface CredentialTypesPresenter{
    fun retrieveListOfCredentialTypes()
    fun inject(credentialTypesView: CredentialTypesView?)
    fun select(credentialType: CredentialType, text: String): Boolean
}

class CredentialTypesPresenterImpl @Inject constructor(private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl,
                                                       private val logger: Logger,
                                                       private val credentialsUseCaseImpl: CredentialsUseCaseImpl) : BasePresenter(), CredentialTypesPresenter {
    private var credentialTypesView: CredentialTypesView? = null
    private val disposables = CompositeDisposable()

    override fun retrieveListOfCredentialTypes() {
        disposables.add(internetConnectionUseCaseImpl.isInternetConnected()
                .flatMap {
                    if(it) return@flatMap credentialsUseCaseImpl.retrieveTypeOfCredentials()
                    else return@flatMap io.reactivex.Single.just(credentialTypesView!!.showNetworkErrors())
                }.subscribe({ response ->
                    if(response is CredentialTypesResponse) {
                        if (NetworkErrorValidator.isValidResponse(response))
                            response.credential_types?.let { credentialTypesView!!.showData(it) }
                        else {
                            credentialTypesView!!.showDataError(NetworkErrorValidator.getErrorMessage(response))
                            logger.w(this::class.java.name,attributes = mapOf(this::retrieveListOfCredentialTypes.name to toJson(response)))
                        }
                    }
                }, {
                    credentialTypesView!!.showNetworkErrors()
                    logger.e(this::class.java.name, attributes = mapOf(this::retrieveListOfCredentialTypes.name to it.message))
                }))
    }

    override fun inject(credentialTypesView: CredentialTypesView?) {
        this.credentialTypesView = credentialTypesView
    }

    override fun select(credentialType: CredentialType, text: String): Boolean {
        return credentialType.name!!.toLowerCase().contains(text.toLowerCase())
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}