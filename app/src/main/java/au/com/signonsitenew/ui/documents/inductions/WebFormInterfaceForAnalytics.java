package au.com.signonsitenew.ui.documents.inductions;

import android.webkit.JavascriptInterface;
import com.google.gson.Gson;
import au.com.signonsitenew.domain.models.analytics.Event;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.utilities.Constants;

public class WebFormInterfaceForAnalytics {

    private AnalyticsEventDelegateService analyticsEventService;
    private Gson gson = new Gson();

    /** Instantiate the interface and set the context */
    public WebFormInterfaceForAnalytics(AnalyticsEventDelegateService analyticsEventDelegateService) {
        analyticsEventService = analyticsEventDelegateService;
    }

    @JavascriptInterface
    public void on_event(String event) {
        Event eventObj  = gson.fromJson(event,Event.class);
        switch (eventObj.getName()) {
            case Constants.SITE_INDUCTION_FORM_FILE_UPLOADED:
                analyticsEventService.siteInductionFormFileUploaded();
                break;
            case Constants.SITE_INDUCTION_FORM_SIGNATURE_UPLOADED:
                analyticsEventService.siteInductionFormSignatureUploaded();
                break;
            case Constants.SITE_INDUCTION_FORM_SUBMITTED:
                analyticsEventService.siteInductionFormSubmitted();
                break;
            case Constants.MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED:
                analyticsEventService.managerAttendanceRegisterSiteInductionAccepted(eventObj.getTargetted_user_id());
                break;
            case Constants.MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED:
                analyticsEventService.managerAttendanceRegisterSiteInductionRejected(eventObj.getTargetted_user_id());
                break;
            case Constants.SITE_INDUCTION_FORM_LOADED_FOR_SUBMISSION:
                analyticsEventService.inductionFormSubmissionLoaded(eventObj.is_new_form());
        }
    }
}
