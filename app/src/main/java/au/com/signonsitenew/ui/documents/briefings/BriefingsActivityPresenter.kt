package au.com.signonsitenew.ui.documents.briefings

import androidx.databinding.ObservableField
import au.com.signonsitenew.domain.models.state.BriefingsActivityState
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.NetworkErrorValidator
import au.com.signonsitenew.utilities.addOnPropertyChanged
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import retrofit2.HttpException
import javax.inject.Inject


class BriefingsActivityPresenter @Inject constructor(private val briefingsUseCase: BriefingsUseCaseImpl,
                                                     private val logger:Logger) :BasePresenter() {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var briefingsActivityDisplay:BriefingsActivityDisplay
    private val uiState = ObservableField<BriefingsActivityState>()

    fun inject(briefingsActivityDisplay: BriefingsActivityDisplay){
        this.briefingsActivityDisplay = briefingsActivityDisplay
    }

    fun createBriefing(content:String) {
        briefingsUseCase.setContent(content)
        briefingsUseCase.updateBriefingContentAsync(object:DisposableSingleObserver<ApiResponse>(){
            override fun onSuccess(response: ApiResponse) {
                if (NetworkErrorValidator.isValidResponse(response)) {
                    uiState.set(BriefingsActivityState.Success)
                }
                else {
                    uiState.set(BriefingsActivityState.DataError(NetworkErrorValidator.getErrorMessage(response)))
                    logger.w(BriefingsActivityPresenter::class.java.name,attributes = mapOf(this@BriefingsActivityPresenter::createBriefing.name to toJson(response)))
                }
            }

            override fun onError(error: Throwable) {
                if (error is HttpException) {
                    val body = error.response()!!.errorBody()
                    val type = object : TypeToken<ApiResponse>() {}.type
                    var errorResponse: ApiResponse? = Gson().fromJson(body?.charStream(), type)
                    uiState.set(errorResponse?.let { it1 -> NetworkErrorValidator.getErrorMessage(it1) }?.let { it2 -> BriefingsActivityState.DataError(it2) })
                }
                uiState.set(BriefingsActivityState.Error)
                logger.e(BriefingsActivityPresenter::class.java.name,attributes = mapOf(this@BriefingsActivityPresenter::createBriefing.name to error.message))
            }

        })
    }
    fun observeStates() = disposables.add(uiState.addOnPropertyChanged {
        when(it.get()){
            is BriefingsActivityState.StartProgressDialog -> briefingsActivityDisplay.showProgressDialog()
            is BriefingsActivityState.Success -> briefingsActivityDisplay.showCreateBriefingConfirmation()
            is BriefingsActivityState.Error -> briefingsActivityDisplay.showNetworkError()
            is BriefingsActivityState.DataError -> briefingsActivityDisplay.showDataErrors((it.get() as BriefingsActivityState.DataError).error)
        }
    })

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
        briefingsUseCase.dispose()
    }
}