package au.com.signonsitenew.ui.documents.permits.details.checks

import android.net.Uri
import au.com.signonsitenew.domain.models.ComponentTypeForm
import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.state.PermitChecksState
import au.com.signonsitenew.domain.models.state.PermitContentType
import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import au.com.signonsitenew.domain.models.state.PermitsChecksTabFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitsChecksTabUseCase
import au.com.signonsitenew.events.RxBusChecksContentTypeComponent
import au.com.signonsitenew.events.RxBusChecksImageUri
import au.com.signonsitenew.models.UploadImageResponse
import au.com.signonsitenew.utilities.BasePresenter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxjava3.subjects.PublishSubject
import java.io.File
import javax.inject.Inject

interface ChecksFragmentPresenter {
    fun inject(display: ChecksDisplay)
    fun observeStates()
    fun observeImageUri():Observable<Uri>
    fun mapComponentState(checksState: PermitChecksState): PermitContentTypeState
    fun mapStringTypeToContentType(contentTypeItem: ContentTypeItem):PermitContentType
    fun setContentTypeComponents()
    fun setPermitInfo(permitInfo: PermitInfo)
    fun getPermitInfo():PermitInfo
    fun getLastContentTypeResponse(responses:List<PermitContentTypeResponses>): PermitContentTypeResponses
    fun sendContentTypeComponent(componentTypeForm: ComponentTypeForm)
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun getLocalUserFullName():String
}

class ChecksFragmentPresenterImpl@Inject constructor (private val permitsChecksTabUseCase: PermitsChecksTabUseCase,
                                                      private val rxBusChecksImageUri: RxBusChecksImageUri,
                                                      private val rxBusChecksContentTypeComponent: RxBusChecksContentTypeComponent): BasePresenter(), ChecksFragmentPresenter {

    private lateinit var display: ChecksDisplay
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<PermitsChecksTabFragmentState>()

    override fun inject(display: ChecksDisplay) {
        this.display = display
    }

    override fun observeStates() {
        disposable.add(uiStateObservable
                .subscribe {
                    when(it){
                        is PermitsChecksTabFragmentState.ShowRequestState ->{
                            permitsChecksTabUseCase.checkForRequestChecksComponents({
                                display.showRequestStateComponents(it.components, PermitChecksState.RequestChecks)
                            },{
                                display.hideRequestChecksHeader()
                            })
                        }
                        is PermitsChecksTabFragmentState.ShowInProgressState ->{
                            permitsChecksTabUseCase.checkForInProgressChecksComponents({
                                display.showInProgressStateComponents(it.components, PermitChecksState.InProgressChecks)

                            },{
                                display.hideInProgressChecksHeader()
                            })
                        }
                        is PermitsChecksTabFragmentState.ShowClosingState ->{
                            permitsChecksTabUseCase.checkForClosingChecksComponents({
                                display.showClosingStateComponents(it.components, PermitChecksState.ClosingChecks)
                            },{
                                display.hideClosingRequestHeader()
                            })
                        }
                        is PermitsChecksTabFragmentState.ShowTextForEmptyState ->{
                            display.showTextForEmptyState()
                        }
                    }
                })
    }

    override fun observeImageUri():Observable<Uri> {
        return rxBusChecksImageUri.toImageUriInChecksObservable()
    }

    override fun mapComponentState(checksState: PermitChecksState): PermitContentTypeState {
        return permitsChecksTabUseCase.mapComponentState(checksState)
    }

    override fun mapStringTypeToContentType(contentTypeItem: ContentTypeItem): PermitContentType {
        return permitsChecksTabUseCase.mapStringTypeToContentType(contentTypeItem)
    }

    override fun setContentTypeComponents() {
        uiStateObservable.onNext(PermitsChecksTabFragmentState.ShowRequestState(getPermitInfo().request_checks, PermitChecksState.RequestChecks))
        uiStateObservable.onNext(PermitsChecksTabFragmentState.ShowInProgressState(getPermitInfo().in_progress_checks, PermitChecksState.InProgressChecks))
        uiStateObservable.onNext(PermitsChecksTabFragmentState.ShowClosingState(getPermitInfo().pending_closure_checks, PermitChecksState.ClosingChecks))
        if(permitsChecksTabUseCase.areComponentsEmpty()) uiStateObservable.onNext(PermitsChecksTabFragmentState.ShowTextForEmptyState)
    }

    override fun setPermitInfo(permitInfo: PermitInfo) {
       permitsChecksTabUseCase.setPermitInfo(permitInfo)
    }

    override fun getPermitInfo(): PermitInfo {
        return permitsChecksTabUseCase.getPermitInfo()
    }

    override fun getLastContentTypeResponse(responses: List<PermitContentTypeResponses>): PermitContentTypeResponses {
        return permitsChecksTabUseCase.getLastContentTypeResponse(responses)
    }

    override fun sendContentTypeComponent(componentTypeForm: ComponentTypeForm) {
        rxBusChecksContentTypeComponent.sendComponentTypeForm(componentTypeForm)
    }

    override fun uploadImages(file: File, fileName: String): Single<UploadImageResponse> {
        return permitsChecksTabUseCase.uploadImages(file, fileName)
    }

    override fun getLocalUserFullName(): String {
        return permitsChecksTabUseCase.getLocalFullDateName()
    }

    override fun onCleared() {
        disposable.dispose()
    }
}