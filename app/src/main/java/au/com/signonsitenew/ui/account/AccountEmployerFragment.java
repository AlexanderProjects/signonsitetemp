package au.com.signonsitenew.ui.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;

public class AccountEmployerFragment extends Fragment {

    private static final String LOG = AccountEmployerFragment.class.getSimpleName();

    private SessionManager mSession;
    private SOSAPI mAPI;

    protected ArrayList<String> mEmployerNames;

    protected TextView mCurrentEmployerTextView;
    protected AutoCompleteTextView mNewEmployerAutoComplete;
    protected Button mSubmitButton;

    public AccountEmployerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSession = new SessionManager(getActivity());
        mAPI = new SOSAPI(getActivity());

        mEmployerNames = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account_employer, container, false);

        // Set title
        ((AccountActivity)getActivity()).mTitleTextView.setText("Change Employer");

        mCurrentEmployerTextView = (TextView)rootView.findViewById(R.id.current_employer_text_view);
        mNewEmployerAutoComplete = (AutoCompleteTextView)rootView.findViewById(R.id.new_employer_autocomplete);
        mSubmitButton = (Button)rootView.findViewById(R.id.update_employer_button);

        mCurrentEmployerTextView.setText(mSession.getCurrentUser().get(Constants.USER_COMPANY_NAME));

        retrieveEmployersList();

        mNewEmployerAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validate
                String newEmployer = mNewEmployerAutoComplete.getText().toString().trim();

                if (newEmployer.toLowerCase().equals(mCurrentEmployerTextView.getText().toString().trim().toLowerCase())) {
                    // Employers are the same
                    ((AccountActivity)getActivity()).createAlertDialog("You are already registered with this company!");
                }
                else {
                    // We good - Make the (hopefully moreso) grammatically correct
                    newEmployer = WordUtils.capitalize(newEmployer);
                    Log.i(LOG, newEmployer);

                    updateEmployer(newEmployer);
                }
            }
        });

        return rootView;
    }

    protected boolean employerDetailsChanged() {
        String newEmployer = mNewEmployerAutoComplete.getText().toString().trim();

        return (!newEmployer.isEmpty());
    }

    private void retrieveEmployersList() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Retrieving Employers...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();

        SLog.i(LOG, "About to call get_all_employers.php");

        mAPI.getAllCompanies(new SOSAPI.VolleySuccessCallback() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                // Check the response result
                JSONObject JSONResponse;
                try {
                    JSONResponse = new JSONObject(response);
                    JSONObject companies = JSONResponse.getJSONObject(Constants.JSON_COMPANIES);
                    Iterator<?> iterator = companies.keys();

                    while(iterator.hasNext()){
                        String key = (String)iterator.next();
                        mEmployerNames.add(companies.getString(key));
                    }
                    Collections.sort(mEmployerNames);
                }
                catch (JSONException e) {
                    SLog.e(LOG, e.getMessage());
                    DarkToast.makeText(getActivity(),
                            getString(R.string.error_generic_response));
                }

                ArrayAdapter<String> autoCompleteArrayAdapter
                        = new ArrayAdapter<>(getActivity(),
                                             android.R.layout.simple_dropdown_item_1line,
                                             mEmployerNames);
                mNewEmployerAutoComplete.setAdapter(autoCompleteArrayAdapter);
            }
        }, new SOSAPI.VolleyErrorCallback() {
            @Override
            public void onResponse() {
                dialog.dismiss();

                DarkToast.makeText(getActivity(),
                        getString(R.string.error_generic_response));
            }
        });
    }

    private void updateEmployer(final String employer) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Updating...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();

        SLog.i(LOG, "About to call edit_user_companies.php");

        mAPI.updateEmployer(employer, new SOSAPI.VolleySuccessCallback() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                // Check the response result
                JSONObject JSONResponse;
                try {
                    JSONResponse = new JSONObject(response);
                    if (JSONResponse.getBoolean(Constants.JSON_UPDATE_OK)) {
                        DarkToast.makeText(getActivity(), "Successfully updated employer");
                        mAPI.retrieveCurrentEmployer(null);
                        if (getActivity() != null) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    }
                }
                catch (JSONException e) {
                    SLog.e(LOG, e.getMessage());
                    DarkToast.makeText(getActivity(),
                            getString(R.string.error_generic_response));
                }
            }
        }, new SOSAPI.VolleyErrorCallback() {
            @Override
            public void onResponse() {
                // Stop spinner
                dialog.dismiss();

                DarkToast.makeText(getActivity(),
                        getString(R.string.error_generic_response));
            }
        });

    }

}
