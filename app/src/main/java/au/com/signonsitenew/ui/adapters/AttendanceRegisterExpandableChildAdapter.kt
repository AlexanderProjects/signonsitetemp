package au.com.signonsitenew.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.adapters.AttendanceAdapterChildModel
import au.com.signonsitenew.utilities.Constants

class AttendanceRegisterExpandableChildAdapter (private var listOfAttendanceRegisterChild: MutableList<AttendanceAdapterChildModel>, val onClickChildAction:(userId:Int)->Unit): RecyclerView.Adapter<AttendanceRegisterExpandableChildAdapter.ChildViewHolder>() {

    inner class ChildViewHolder(view: View):RecyclerView.ViewHolder(view){
        val workerName: TextView = view.findViewById(R.id.new_attendance_worker_name)
        val timeIn: TextView = view.findViewById(R.id.time_in)
        val briefingStatus: ImageView = view.findViewById(R.id.alert_status_icon)
        val inductionStatus: ImageView = view.findViewById(R.id.induction_status)
        val rowLayout:ConstraintLayout = view.findViewById(R.id.child_row_layout)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_child_attendance_register,parent,false)
        return ChildViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfAttendanceRegisterChild.size
    }

    override fun onBindViewHolder(holder: ChildViewHolder, position: Int) {
        holder.workerName.text = listOfAttendanceRegisterChild[position].workerName
        holder.timeIn.text = listOfAttendanceRegisterChild[position].timeIn
        if(listOfAttendanceRegisterChild[position].hasActiveBriefing){
            if(listOfAttendanceRegisterChild[position].briefing)
                holder.briefingStatus.setImageResource(R.drawable.ic_check_green_24)
            else
                holder.briefingStatus.setImageResource(R.drawable.ic_red_cross_24dp)
        }else{
            holder.briefingStatus.visibility = View.INVISIBLE
        }
        if(listOfAttendanceRegisterChild[position].hasActiveSiteInductionForm) {
            if (listOfAttendanceRegisterChild[position].induction != null) {
                if (listOfAttendanceRegisterChild[position].induction.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                    if(listOfAttendanceRegisterChild[position].type.equals(Constants.DOC_INDUCTION_VISITOR))
                        holder.inductionStatus.setImageResource(R.drawable.ic_visitor_green_24dp)
                    else
                        holder.inductionStatus.setImageResource(R.drawable.ic_check_green_24)
                }
                else
                    holder.inductionStatus.setImageResource(R.drawable.ic_review_red_24dp)
            } else {
                holder.inductionStatus.setImageResource(R.drawable.ic_red_cross_24dp)
            }
        }else{
            holder.inductionStatus.visibility = View.INVISIBLE
        }
        holder.rowLayout.setOnClickListener { onClickChildAction(listOfAttendanceRegisterChild[position].userId) }

        if(listOfAttendanceRegisterChild[position].timeOut == null) {
            holder.workerName.setTextColor(ContextCompat.getColor(holder.workerName.context,R.color.green_acknowledgement))
            holder.timeIn.setTextColor(ContextCompat.getColor(holder.timeIn.context,R.color.green_acknowledgement))
        }else{
            holder.workerName.setTextColor(ContextCompat.getColor(holder.workerName.context,R.color.text_grey_secondary))
            holder.timeIn.setTextColor(ContextCompat.getColor(holder.timeIn.context,R.color.text_grey_secondary))
        }
    }
}