package au.com.signonsitenew.ui.attendanceregister;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.ManagedCompany;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import au.com.signonsitenew.utilities.ToastUtil;
import au.com.signonsitenew.utilities.Validation;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Activities containing this fragment MUST implement the {@link OnRegisterWorkerInteractionListener}
 * interface.
 */

public class RegisterUserFragment extends Fragment {

    private static final String LOG = RegisterUserFragment.class.getSimpleName();

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mPhoneNumberEditText;
    private TextView mInfoTextView;
    private AutoCompleteTextView mCompanyAutocompleteTextView;
    private Spinner mCompanySpinner;
    private Button mSignOnButton;
    private OnRegisterWorkerInteractionListener mListener;

    protected ArrayAdapter<String> mSpinnerArrayAdapter;

    private boolean mSuperviseMultiple;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_worker, container, false);

        // Set the title
        Toolbar toolbar = rootView.findViewById(R.id.add_worker_toolbar);
        toolbar.setTitle(Constants.ADD_A_PERSON);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setHasOptionsMenu(true);

        mFirstNameEditText = rootView.findViewById(R.id.first_name_edit_text);
        mLastNameEditText = rootView.findViewById(R.id.last_name_edit_text);
        mPhoneNumberEditText = rootView.findViewById(R.id.phone_number_edit_text);
        mInfoTextView = rootView.findViewById(R.id.company_info_text_view);
        mCompanyAutocompleteTextView = rootView.findViewById(R.id.company_autocomplete_text_view);
        mCompanySpinner = rootView.findViewById(R.id.company_spinner);
        mSignOnButton = rootView.findViewById(R.id.sign_on_button);

        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
        final RealmResults<ManagedCompany> companies = realm.where(ManagedCompany.class).findAll();
        if (companies.size() == 1) {
            // User is a supervisor, pre-fill company
            mSuperviseMultiple = false;
            mCompanyAutocompleteTextView.setVisibility(View.VISIBLE);
            mCompanySpinner.setVisibility(View.GONE);

            String companyName = companies.first().getCompanyName();
            mInfoTextView.setText("You can only manage workers for " + companyName);
            mCompanyAutocompleteTextView.setText(companyName);
            mCompanyAutocompleteTextView.setEnabled(false);

            // Since the user can't change the company, set the keyboard to dimiss after entering number
            mPhoneNumberEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
        else if (companies.size() > 1) {
            // User is a supervisor, restrict companies
            mSuperviseMultiple = true;
            mCompanyAutocompleteTextView.setVisibility(View.GONE);
            mCompanySpinner.setVisibility(View.VISIBLE);

            mInfoTextView.setText("Select company from the options provided");

            // Fill the state spinner with list of states
            ArrayList<String> companiesArray = new ArrayList<>();
            for (ManagedCompany company : companies) {
                companiesArray.add(company.getCompanyName());
            }
            mCompanySpinner.setPrompt("Select company");
            mSpinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, companiesArray);
            mSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCompanySpinner.setAdapter(mSpinnerArrayAdapter);
            mCompanySpinner.setSelection(companiesArray.size() - 1);
        }
        else {
            mSuperviseMultiple = false;
            mCompanyAutocompleteTextView.setVisibility(View.VISIBLE);
            mCompanySpinner.setVisibility(View.GONE);

            mInfoTextView.setText("Please enter the worker\'s company below.");
        }
        realm.close();

        mCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((AttendanceActivity)getActivity()).hideKeyboard(getActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ((AttendanceActivity)getActivity()).hideKeyboard(getActivity());
            }
        });

        mSignOnButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Validate user input
                String firstName = WordUtils.capitalize(mFirstNameEditText.getText().toString().trim());
                String lastName = WordUtils.capitalize(mLastNameEditText.getText().toString().trim());
                String phoneNumber = mPhoneNumberEditText.getText().toString().trim();
                phoneNumber = phoneNumber.replace(" ", "");
                String companyName;
                if (!mSuperviseMultiple) {
                    companyName = mCompanyAutocompleteTextView.getText().toString().trim();
                }
                else {
                    companyName = mCompanySpinner.getSelectedItem().toString();
                }

                boolean validFirst = Validation.validateName(getActivity(), firstName);
                boolean validLast = Validation.validateName(getActivity(), lastName);
                boolean validPhone = Validation.validPhoneNumber(getActivity(), phoneNumber);

                if (companyName.isEmpty()) {
                    ToastUtil.displayToast(getActivity(), "Please enter a company");
                    return;
                }

                if (validFirst && validLast && validPhone) {
                    mListener.onRegisterPressed(firstName, lastName, phoneNumber, companyName);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterWorkerInteractionListener) {
            mListener = (OnRegisterWorkerInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterWorkerInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(false);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRegisterWorkerInteractionListener {
        void onRegisterPressed(String firstName, String lastName, String phoneNumber, String companyName);
    }
}
