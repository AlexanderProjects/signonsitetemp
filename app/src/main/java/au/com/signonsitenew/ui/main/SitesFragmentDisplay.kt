package au.com.signonsitenew.ui.main

import au.com.signonsitenew.domain.models.NearSite

interface SitesFragmentDisplay {
    fun showListOfSites(nearSites:List<NearSite>)
    fun showNetworkErrors()
    fun showEmptyListOfSites()
    fun onRequestPermissionsResultListener(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
}