package au.com.signonsitenew.ui.attendanceregister;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class InductionReviewFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_ATTENDEE_ID = "attendeeId";
    private static final String ARG_INDUCTION_ID = "inductionId";
    private static final String ARG_ACTION_CONST = "action";

    private Realm mRealm;
    private long mAttendeeId;

    protected Button mReviewButton;
    protected TextView mWrongInductionTextView;

    public InductionReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRealm = Realm.getDefaultInstance();

        // Remove the option icons for the activity
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mAttendeeId = getArguments().getLong(ARG_ATTENDEE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_induction_review, container, false);

        // Set title
        Toolbar toolbar = rootView.findViewById(R.id.induction_review_toolbar);
        toolbar.setTitle(Constants.REVIEW_INDUCTION);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mReviewButton = rootView.findViewById(R.id.review_induction_button);
        mWrongInductionTextView = rootView.findViewById(R.id.wrong_induction_text_view);

        mReviewButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Open a webview to review the induction form
                Fragment reviewFragment = new FormViewFragment();

                SiteInductionService siteInductionService = new SiteInductionService(mRealm);
                long formId = siteInductionService.getSiteInductionForUser(mAttendeeId).getId();
                Bundle args = new Bundle();
                args.putLong(ARG_INDUCTION_ID, formId);
                args.putString(ARG_ACTION_CONST, "review");
                reviewFragment.setArguments(args);
                ((AttendanceActivity)getActivity()).navigateToFragment(reviewFragment, true);
            }
        });

        mWrongInductionTextView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Navigate to the cancel induction screen
                Fragment inductionCancelFragment = new InductionCancelFragment();
                Bundle args = new Bundle();
                args.putLong(ARG_ATTENDEE_ID, mAttendeeId);
                inductionCancelFragment.setArguments(args);
                // Don't add this fragment to he back stack
                ((AttendanceActivity)getActivity()).navigateToFragment(inductionCancelFragment, false);
            }
        });

        return rootView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }
}
