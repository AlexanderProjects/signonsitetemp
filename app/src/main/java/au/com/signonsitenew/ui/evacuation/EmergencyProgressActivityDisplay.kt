package au.com.signonsitenew.ui.evacuation

interface EmergencyProgressActivityDisplay {
    fun navigationSignedActivity()
    fun showContentView()
}