package au.com.signonsitenew.ui.passport.listeners

interface MainTabMenuListener {
    fun mainMenuTabSelected(position: Int)
}