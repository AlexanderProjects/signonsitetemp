package au.com.signonsitenew.ui.passport.personal


import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.Gender
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.utilities.NotificationBadgeCounter
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.MainActivityNavigationListener
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.ui.main.SignedOnActivityNavigationListener
import au.com.signonsitenew.ui.passport.PassportFragment.Companion.setPassportTabMenuListener
import au.com.signonsitenew.ui.passport.PassportFragment.Companion.setPersonalDataListener
import au.com.signonsitenew.ui.passport.PassportNavigationListener
import au.com.signonsitenew.ui.passport.listeners.MainTabMenuListener
import au.com.signonsitenew.ui.passport.listeners.PassportTabMenuListener
import au.com.signonsitenew.utilities.*
import au.com.signonsitenew.utilities.EditTextWatcher.CallAction
import au.com.signonsitenew.utilities.phonepicker.PhonePickerFragment
import au.com.signonsitenew.utilities.phonepicker.PhoneSelectedListener
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import android.view.inputmethod.InputMethodManager
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton


class PersonalFragment : DaggerFragment(), PersonalDataListener, PersonalDisplay, MainTabMenuListener, PassportTabMenuListener, PhoneSelectedListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var sessionManager: SessionManager
    @Inject
    lateinit var mapper: Mapper
    private val presenter: PersonalPresenterImpl by viewModels { viewModelFactory}
    private var datePicker: String? = null
    private var user: User? = null
    private var phoneNumber = PhoneNumber()
    private lateinit var genderSpinner: Spinner
    private lateinit var apprenticeSpinner: Spinner
    private lateinit var indigenousStatusSpinner: Spinner
    private lateinit var hasExperienceSpinner: Spinner
    private lateinit var nativeEnglishSpeakerSpinner: Spinner
    private lateinit var interpreterRequiredSpinner: Spinner
    private lateinit var firstNameEditText: EditText
    private lateinit var emailTextView: TextView
    private lateinit var lastNameEditText: EditText
    private lateinit var companyNameEditText: EditText
    private lateinit var tradeEditText: EditText
    private lateinit var phoneEditText: EditText
    private lateinit var postCodeEditText: EditText
    private lateinit var customGender: EditText
    private lateinit var dateOfBirthEditText: EditText
    private lateinit var firstNameWarning: ImageView
    private lateinit var lastNameWarning: ImageView
    private lateinit var companyWarning: ImageView
    private lateinit var tradeWarning: ImageView
    private lateinit var emailWarning: ImageView
    private lateinit var phoneWarning: ImageView
    private lateinit var indigenousWarning: ImageView
    private lateinit var postCodeWarning: ImageView
    private lateinit var genderWarning: ImageView
    private lateinit var apprenticeWarning: ImageView
    private lateinit var dateOfBirthWarning: ImageView
    private lateinit var savePersonalInfoButton: ExtendedFloatingActionButton
    private lateinit var rootView: View
    private var isFocusInPostCode:Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.personal_fragment, container, false)
        rootView = view.rootView
        presenter.inject(this)
        presenter.observeStates()
        firstNameEditText = view.findViewById(R.id.first_name_personal_info_edit_text)
        firstNameWarning = view.findViewById(R.id.first_name_warning_img)
        firstNameEditText.filters = Util.emojiFilter()
        savePersonalInfoButton = view.findViewById(R.id.save_personal_info_fab)
        firstNameEditText.addTextChangedListener(CustomEditTextWatcher(firstNameEditText,
                updateFlags = {
                    sessionManager.editingInPersonalInfoPassport = true
                    presenter.showSaveButton()
                    isFocusInPostCode = false
                },
                showNotProvided = {
                    firstNameEditText.hint = Constants.NOT_PROVIDED_HINT
                    firstNameWarning.visibility = View.VISIBLE
                },
                clearActions = { firstNameWarning.visibility = View.INVISIBLE }))

        lastNameWarning = view.findViewById(R.id.last_name_warning_img)
        lastNameEditText = view.findViewById(R.id.last_name_personal_info_edit_text)
        lastNameEditText.filters = Util.emojiFilter()
        lastNameEditText.addTextChangedListener(CustomEditTextWatcher(lastNameEditText,
                updateFlags = {
                    sessionManager.editingInPersonalInfoPassport = true
                    presenter.showSaveButton()
                    isFocusInPostCode = false
                },
                showNotProvided = {
                    lastNameEditText.hint = Constants.NOT_PROVIDED_HINT
                    lastNameWarning.visibility = View.VISIBLE
                },
                clearActions = { lastNameWarning.visibility = View.INVISIBLE }))
        companyWarning = view.findViewById(R.id.company_warning_img)
        companyNameEditText = view.findViewById(R.id.company_personal_info_edit_text)
        companyNameEditText.addTextChangedListener(CustomEditTextWatcher(companyNameEditText,
                updateFlags = {
                    sessionManager.editingInPersonalInfoPassport = true
                    presenter.showSaveButton()
                    isFocusInPostCode = false
                },
                showNotProvided = {
                    companyNameEditText.hint = Constants.NOT_PROVIDED_HINT
                    companyWarning.visibility = View.VISIBLE
                },
                clearActions = { companyWarning.visibility = View.INVISIBLE }))
        tradeWarning = view.findViewById(R.id.trade_warning_img)
        tradeEditText = view.findViewById(R.id.trade_personal_info_edit_text)
        tradeEditText.addTextChangedListener(CustomEditTextWatcher(tradeEditText,
                updateFlags = {
                    sessionManager.editingInPersonalInfoPassport = true
                    presenter.showSaveButton()
                    isFocusInPostCode = false
                },
                showNotProvided = {
                    tradeEditText.hint = Constants.NOT_PROVIDED_HINT
                    tradeWarning.visibility = View.VISIBLE
                },
                clearActions = {
                    tradeWarning.visibility = View.INVISIBLE
                }))

        emailTextView = view.findViewById(R.id.email_passport_textview)
        emailWarning = view.findViewById(R.id.email_personal_warning_img)

        phoneEditText = view.findViewById(R.id.phone_personal_editText)
        phoneEditText.setOnClickListener { NavigationHelper.navigateToPhonePicker(activity, user, false, false) }
        phoneWarning = view.findViewById(R.id.phone_warning_img)
        phoneEditText.addTextChangedListener(PhoneNumberTextWatcher(phoneEditText,callActionOnPhoneEditText = {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }))
        indigenousStatusSpinner = view.findViewById(R.id.indigenous_status_spinner)
        val indigenousArrayAdapter = ArrayAdapter.createFromResource(requireActivity(), R.array.indigenous, R.layout.spinner_item)
        indigenousStatusSpinner.adapter = indigenousArrayAdapter

        indigenousWarning = view.findViewById(R.id.indigenous_status_warning_img)
        indigenousWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(requireActivity(), Constants.INDIGENOUS_MESSAGE, Constants.INDIGENOUS_TITLE) }
        indigenousStatusSpinner.onItemSelectedListener = AdapterViewOnItemSelected(indigenousStatusSpinner) {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }

        postCodeWarning = view.findViewById(R.id.postcode_warning_img)
        postCodeEditText = view.findViewById(R.id.postcode_editText)
        postCodeEditText.addTextChangedListener(EditTextWatcher(postCodeEditText, object : CallAction {
            override fun call() {
                sessionManager.editingInPersonalInfoPassport = true
                presenter.showSaveButton()
                isFocusInPostCode = true
            }
        }))
        postCodeWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(requireActivity(), Constants.POSTCODE_MESSAGE, Constants.POSTCODE_TITLE) }

        genderSpinner = view.findViewById(R.id.gender_spinner)
        genderWarning = view.findViewById(R.id.gender_warning_img)
        genderWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(requireActivity(), Constants.GENDER_MESSAGE, Constants.GENDER_TITLE) }
        val genderArrayAdapter = ArrayAdapter.createFromResource(requireActivity().applicationContext, R.array.gender, R.layout.spinner_item)
        genderArrayAdapter.setDropDownViewResource(R.layout.spinner_item)
        genderSpinner.adapter = genderArrayAdapter
        customGender = view.findViewById(R.id.custom_gender)
        genderSpinner.isFocusableInTouchMode = true
        genderSpinner.onItemSelectedListener = AdapterViewOnItemSelected(genderSpinner,
                {
                    customGender.visibility = View.VISIBLE
                    customGender.addTextChangedListener(EditTextWatcher(customGender, object :CallAction{
                        override fun call() {
                            sessionManager.editingInPersonalInfoPassport = true
                            presenter.showSaveButton()
                        }
                    }))
                },
                { customGender.visibility = View.GONE },
                {
                    sessionManager.editingInPersonalInfoPassport = true
                    presenter.showSaveButton()
                })

        apprenticeSpinner = view.findViewById(R.id.apprentice_spinner)
        val apprenticeArrayAdapter = ArrayAdapter.createFromResource(requireActivity(), R.array.apprentice, R.layout.spinner_item)
        apprenticeSpinner.adapter = apprenticeArrayAdapter
        apprenticeWarning = view.findViewById(R.id.apprentice_warning_img)
        apprenticeWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.APPRENTICE_STATUS_MESSAGE, Constants.APPRENTICE_TITLE) }
        apprenticeSpinner.isClickable = false
        apprenticeSpinner.onItemSelectedListener = AdapterViewOnItemSelected(apprenticeSpinner) {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }

        dateOfBirthEditText = view.findViewById(R.id.dateOfBirth_editText)
        dateOfBirthWarning = view.findViewById(R.id.dateofbirth_warning_img)
        dateOfBirthWarning.setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.DATE_OF_BIRTH_MESSAGE, Constants.DATE_OF_BIRTH_TITLE) }
        dateOfBirthEditText.inputType = InputType.TYPE_NULL
        dateOfBirthEditText.setOnClickListener {
            DatePickerHelper.datePicker(requireActivity(),
                    datePicker =
                    { text ->
                        datePicker = text
                    },
                    updateFields = { text1 ->
                        dateOfBirthEditText.setText(presenter.convertDateFormat(text1))
                    })
        }
        dateOfBirthEditText.addTextChangedListener(EditTextWatcher(dateOfBirthEditText, object : CallAction {
            override fun call() {
                sessionManager.editingInPersonalInfoPassport = true
                presenter.showSaveButton()
            }
        }))

        view.findViewById<ImageView>(R.id.experience_warning_img).setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.CONSTRUCTION_EXPERIENCE_QUESTION, Constants.CONSTRUCTION_EXPERIENCE_QUESTION_TITLE) }
        hasExperienceSpinner = view.findViewById(R.id.experience_spinner)
        val hasExperienceAdapter = ArrayAdapter.createFromResource(requireActivity(), R.array.user_has_experience, R.layout.spinner_item)
        hasExperienceSpinner.adapter = hasExperienceAdapter
        hasExperienceSpinner.onItemSelectedListener = AdapterViewOnItemSelected(hasExperienceSpinner) {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }

        view.findViewById<ImageView>(R.id.native_english_speaker_warning_img).setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.ENGLISH_NATIVE_QUESTION, Constants.ENGLISH_NATIVE_QUESTION_TITLE) }
        nativeEnglishSpeakerSpinner = view.findViewById(R.id.native_speaker_spinner)
        val nativeSpeakerAdapter = ArrayAdapter.createFromResource(requireActivity(), R.array.native_english_speaker, R.layout.spinner_item)
        nativeEnglishSpeakerSpinner.adapter = nativeSpeakerAdapter
        nativeEnglishSpeakerSpinner.onItemSelectedListener = AdapterViewOnItemSelected(nativeEnglishSpeakerSpinner) {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }

        view.findViewById<ImageView>(R.id.interpreter_required_warning_img).setOnClickListener { AlertDialogMessageHelper.messageForInfoIcon(activity, Constants.INTERPRETER_REQUIRED_QUESTION, Constants.INTERPRETER_REQUIRED_QUESTION_TITLE) }
        interpreterRequiredSpinner = view.findViewById(R.id.interpreter_spinner)
        val interpreterAdapter = ArrayAdapter.createFromResource(requireActivity(), R.array.require_interpreter, R.layout.spinner_item)
        interpreterRequiredSpinner.adapter = interpreterAdapter
        interpreterRequiredSpinner.onItemSelectedListener = AdapterViewOnItemSelected(interpreterRequiredSpinner) {
            sessionManager.editingInPersonalInfoPassport = true
            presenter.showSaveButton()
        }

        view.findViewById<ImageView>(R.id.gender_clear_img).setOnClickListener {
            presenter.clearButtonClicked(genderSpinner.selectedItem.toString())
            this.user?.gender = null
            genderSpinner.setSelection(0)
        }
        view.findViewById<ImageView>(R.id.indigenous_status_clear_img).setOnClickListener {
            presenter.clearButtonClicked(indigenousStatusSpinner.selectedItem.toString())
            this.user?.is_indigenous = null
            indigenousStatusSpinner.setSelection(0)
        }
        view.findViewById<ImageView>(R.id.apprentice_clear_img).setOnClickListener {
            presenter.clearButtonClicked(apprenticeSpinner.selectedItem.toString())
            this.user?.is_apprentice = null
            apprenticeSpinner.setSelection(0)
        }
        view.findViewById<ImageView>(R.id.experience_clear_img).setOnClickListener {
            presenter.clearButtonClicked(hasExperienceSpinner.selectedItem.toString())
            this.user?.is_experienced = null
            hasExperienceSpinner.setSelection(0)
        }
        view.findViewById<ImageView>(R.id.interpreter_required_clear_img).setOnClickListener {
            presenter.clearButtonClicked(interpreterRequiredSpinner.selectedItem.toString())
            this.user?.is_interpreter_required = null
            interpreterRequiredSpinner.setSelection(0)
        }
        view.findViewById<ImageView>(R.id.native_english_speaker_clear_img).setOnClickListener {
            presenter.clearButtonClicked(nativeEnglishSpeakerSpinner.selectedItem.toString())
            this.user?.is_english_native = null
            nativeEnglishSpeakerSpinner.setSelection(0)

        }

       view.findViewById<ImageView>(R.id.date_of_Birth_clear_button).setOnClickListener {
           presenter.clearButtonClicked(dateOfBirthEditText.text.toString())
           this.user?.date_of_birth = null
           dateOfBirthEditText.setText(String().empty())
        }
        view.findViewById<ImageButton>(R.id.postcode_clear_button).setOnClickListener{
            presenter.clearButtonClicked(postCodeEditText.text.toString())
            this.user?.post_code = null
            postCodeEditText.setText(String().empty())
        }

        savePersonalInfoButton.setOnClickListener{
            hideKeyboard()
            updatePersonalInfo()
        }
        view.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            view.getWindowVisibleDisplayFrame(r)
            if (view.rootView.height - (r.bottom - r.top) > 500) {
                // Keyboard open
                savePersonalInfoButton.translationY = -820f
            } else {
                // Keyboard close
                savePersonalInfoButton.translationY = -50f
            }
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setPersonalDataListener(this)
        setPassportTabMenuListener(this)
        SignedOnActivity.setMainTabMenuListener(this)
        MainActivity.setMainTabMenuListener(this)
        PhonePickerFragment.setPhoneSelectedListener(this)
    }

    override fun showUpdatedData(user: User?) {
        showData(user!!)
    }

    override fun hideSaveButton() {
        savePersonalInfoButton.hide()
    }

    override fun showSaveButton() {
        savePersonalInfoButton.show()
    }


    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenter.showUpdatedPersonalInfo() }
    }

    override fun showDataErrors(error: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireActivity(),error)
    }

    override fun onStart() {
        super.onStart()
        if (user != null) {
            showData(user)
        }
    }

    override fun showData(user: User?) {
        this.user = user
        this.user?.first_name?.let {
            firstNameEditText.setText(it)
            firstNameWarning.visibility = View.INVISIBLE
        }
        this.user?.last_name?.let {
            lastNameEditText.setText(it)

            lastNameWarning.visibility = View.INVISIBLE
        }
        this.user?.company_name?.let {
            companyNameEditText.setText(it)

            companyWarning.visibility = View.INVISIBLE
        }
        this.user?.trade?.let {
            tradeEditText.setText(it)
            if(it.isEmpty())tradeWarning.visibility = View.VISIBLE
            else tradeWarning.visibility = View.INVISIBLE
        }
        this.user?.email?.let {
            emailTextView.text = it
            emailWarning.visibility = View.INVISIBLE

        }
        this.user?.post_code?.let {
            postCodeEditText.setText(this.user?.post_code)

        }
        this.user?.date_of_birth?.let {
                dateOfBirthEditText.setText(presenter.convertDateFormatResponse(this.user?.date_of_birth.toString()))

        }
        this.user?.phone_number?.let {
            phoneEditText.setText(it.number)

            phoneWarning.visibility = View.INVISIBLE
        }
        this.user?.gender.let {
            val genderArray = resources.getStringArray(R.array.gender)
            for (i in genderArray.indices) {
                if (it?.tag == genderArray[i]) {
                    genderSpinner.setSelection(i)
                    if (genderSpinner.selectedItem.toString() != Constants.SPINNER_CUSTOM_OPTION) {
                        customGender.visibility = View.GONE
                    }
                } else {
                    customGender.setText(it?.value)
                }
            }
        }
        this.user?.is_indigenous?.let {
            presenter.validSpinnerSelection(
                    userValue =  this.user?.is_indigenous,
                    isValid = {
                        val indigenousArray = resources.getStringArray(R.array.indigenous)
                        for (i in indigenousArray.indices) {
                            if (indigenousArray[i] == it.let { it1 -> presenter.convertIndigenousStatusToString(it1) }) indigenousStatusSpinner.setSelection(i)
                        }
                    },
                    isInValid= {
                        indigenousStatusSpinner.setSelection(0)
                        this.user?.is_indigenous = null
                    })

        }
        this.user?.is_apprentice?.let {
            presenter.validSpinnerSelection(
                    userValue =  this.user?.is_apprentice,
                    isValid = {
                        val apprenticeArray = resources.getStringArray(R.array.apprentice)
                        for (i in apprenticeArray.indices) {
                            if (apprenticeArray[i] == it.let { it1 -> presenter.convertApprenticeToString(it1) }) apprenticeSpinner.setSelection(i)
                        }
                    },
                    isInValid = {
                        apprenticeSpinner.setSelection(0)
                        this.user?.is_apprentice = null
                    })
        }

        this.user?.is_experienced?.let {
            presenter.validSpinnerSelection(
                    userValue = this.user?.is_experienced,
                    isValid = {
                        val hasExperienceArray = resources.getStringArray(R.array.user_has_experience)
                        for (i in hasExperienceArray.indices) {
                            if (hasExperienceArray[i] == it.let { it1 -> presenter.convertHasExperienceToString(it1) }) hasExperienceSpinner.setSelection(i)
                        }
                    },
                    isInValid = {
                        hasExperienceSpinner.setSelection(0)
                        this.user?.is_experienced = null
                    })
        }

        this.user?.is_english_native?.let {
            presenter.validSpinnerSelection(
                    userValue = user?.is_english_native,
                    isValid = {
                        val nativeEnglishArray = resources.getStringArray(R.array.native_english_speaker)
                        for (i in nativeEnglishArray.indices) {
                            if (nativeEnglishArray[i] == it.let { it1 -> presenter.convertNativeEnglishToString(it1) }) nativeEnglishSpeakerSpinner.setSelection(i)
                        }
                    },
                    isInValid = {
                        nativeEnglishSpeakerSpinner.setSelection(0)
                        user?.is_english_native = null
                    })
        }

        this.user?.is_interpreter_required?.let {
            presenter.validSpinnerSelection(
                    userValue = user?.is_interpreter_required,
                    isValid = {
                        val needsInterpreterArray = resources.getStringArray(R.array.require_interpreter)
                        for (i in needsInterpreterArray.indices) {
                            if (needsInterpreterArray[i] == it.let { it1 -> presenter.convertNeedsInterpreterToString(it1) }) interpreterRequiredSpinner.setSelection(i)
                        }
                    },
                    isInValid = {
                        interpreterRequiredSpinner.setSelection(0)
                        user?.is_interpreter_required = null
                    })
        }

        //update badge number
        presenter.updateBadgeNotificationNumber(user?.let { NotificationBadgeCounter.notifierNumberForPersonalInfo(it) })
        presenter.hideSaveButton()
    }

    private fun updatePersonalInfo() {
        val user = UserInfoUpdateRequest()
        user.has_passport = true
        presenter.validateEmptyAndNotProvidedValuesForMandatoryFields(firstNameEditText.text.toString(),
                valid = { user.first_name = firstNameEditText.text.toString()})
        presenter.validateEmptyAndNotProvidedValuesForMandatoryFields(lastNameEditText.text.toString(),
                valid = { user.last_name = lastNameEditText.text.toString() })
        presenter.validateEmptyAndNotProvidedValuesForMandatoryFields(companyNameEditText.text.toString(),
                valid = { user.company_name = companyNameEditText.text.toString()})
        presenter.validateEmptyAndNotProvidedValuesForMandatoryFields(tradeEditText.text.toString(),
                valid = { user.trade = tradeEditText.text.toString()})
        presenter.validateEmptyAndNotProvidedValues(postCodeEditText.text.toString(),
                valid = { user.post_code = postCodeEditText.text.toString() },
                empty = { user.post_code = null})
        presenter.validateEmptyAndNotProvidedValues(dateOfBirthEditText.text.toString(),
                valid = { user.date_of_birth = if(datePicker != null) datePicker else DateFormatUtil.convertDateFormatResponseForUser(this.user?.date_of_birth) },
                empty = { user.date_of_birth = null})
        presenter.validateEmptyAndNotProvidedValues(phoneEditText.text.toString(),
                valid = { user.phone_number = presenter.createPhoneNumberToBeUpdated(phoneEditText.text.toString(), phoneNumber.alpha2) },
                empty = { user.phone_number = null})
        presenter.validateNotProvidedValuesOnSpinnerForIndigenous(indigenousStatusSpinner.selectedItem.toString(),
                valid = { user.is_indigenous = presenter.validateIndigenousStatus(indigenousStatusSpinner.selectedItem.toString())})
        presenter.validateNotProvidedValuesOnSpinnerForApprentice(apprenticeSpinner.selectedItem.toString(),
                valid = { user.is_apprentice = presenter.validateApprenticeStatus(apprenticeSpinner.selectedItem.toString())})
        presenter.validateNotProvidedValuesOnSpinnerForGender(genderSpinner.selectedItem.toString(), valid = {
            val gender = Gender()
            presenter.validateGender(genderSpinner.selectedItem.toString(), valid = {
                gender.tag = genderSpinner.selectedItem.toString()
                gender.value = customGender.text.toString()
                user.gender = gender
            }, invalid = {
                gender.tag = genderSpinner.selectedItem.toString()
                user.gender = null
            })})
        presenter.validateNotProvidedValuesOnSpinnerForConstructionExperience(hasExperienceSpinner.selectedItem.toString(), valid = { user.is_experienced = presenter.convertConfirmationToBoolean(hasExperienceSpinner.selectedItem.toString())})
        presenter.validateNotProvidedValuesOnSpinnerForNativeEnglishSpeaker(nativeEnglishSpeakerSpinner.selectedItem.toString(), valid = { user.is_english_native = presenter.convertConfirmationToBoolean(nativeEnglishSpeakerSpinner.selectedItem.toString())})
        presenter.validateNotProvidedValuesOnSpinnerForInterpreterRequired(interpreterRequiredSpinner.selectedItem.toString(), valid = { user.is_interpreter_required = presenter.convertConfirmationToBoolean(interpreterRequiredSpinner.selectedItem.toString())})
        presenter.updatePersonalInfoData(user)
    }

    override fun mainMenuTabSelected(position: Int) {
        if (activity is SignedOnActivity) {
            presenter.checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position, callAction = {
                AlertDialogMessageHelper.saveBeforeChangeScreenAlert(activity,
                        Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE,
                        Constants.ALERT_DIALOG_TITLE_FOR_SAVE,
                        { sessionManager.editingInPersonalInfoPassport = false }) { navigateToPassport() }
            })
        } else {
            presenter.checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position, callAction = {
                AlertDialogMessageHelper.saveBeforeChangeScreenAlert(activity,
                        Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE,
                        Constants.ALERT_DIALOG_TITLE_FOR_SAVE,
                        { sessionManager.editingInPersonalInfoPassport = false }) { navigateToPassport() }
            })
        }
    }

    override fun passportMenuTabSelected(position: Int) {
        presenter.checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position, callAction = {
            AlertDialogMessageHelper.saveBeforeChangeScreenAlert(activity,
                    Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE,
                    Constants.ALERT_DIALOG_TITLE_FOR_SAVE,
                    { sessionManager.editingInPersonalInfoPassport = false }) { navigateToPersonalInfo() }
        })
    }

    private fun navigateToPersonalInfo() {
        passportNavigationListener.navigateToTab(0)
    }

    private fun navigateToPassport() {
        if (activity is SignedOnActivity)
            signedOnActivityNavigationListener.navigateToTab(2)
        if (activity is MainActivity)
            mainActivityNavigationListener.navigateToTab(1)
    }

    override fun phoneNumberValidated(phoneNumber: PhoneNumber?) {
        if (phoneNumber != null) {
            this.phoneNumber = phoneNumber
            phoneEditText.setText(phoneNumber.number)
        }
    }

    private fun hideKeyboard(){
        if (isKeyboardVisible(rootView)) {
            val inputMethodManager: InputMethodManager = requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
        }
    }

    companion object {
        private lateinit var passportNavigationListener: PassportNavigationListener
        private lateinit var signedOnActivityNavigationListener: SignedOnActivityNavigationListener
        private lateinit var mainActivityNavigationListener: MainActivityNavigationListener
        fun newInstance(): PersonalFragment {
            return PersonalFragment()
        }

        fun setPassportNavigationListener(listener: PassportNavigationListener) {
            passportNavigationListener = listener
        }

        @JvmStatic
        fun setSignedOnActivityNavigationListener(listener: SignedOnActivityNavigationListener) {
            signedOnActivityNavigationListener = listener
        }

        @JvmStatic
        fun setMainActivityNavigationListener(listener: MainActivityNavigationListener) {
            mainActivityNavigationListener = listener
        }

    }
}