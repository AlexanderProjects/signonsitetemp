package au.com.signonsitenew.ui.passport.emergencyinfo

import au.com.signonsitenew.domain.models.User

interface EmergencyInfoDisplay {
    fun showNetworkErrors()
    fun showDataErrors(error:String)
    fun showUpdatedData(user: User?)
    fun hideSaveButton()
    fun showSaveButton()
}