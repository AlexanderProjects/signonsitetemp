package au.com.signonsitenew.ui.documents.permits.details.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.ItemPermitContentTypeTakePhotoChildBinding
import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.utilities.empty
import au.com.signonsitenew.utilities.emptySpace
import com.bumptech.glide.Glide

class PermitPhotoListAdapter(private var responsesList:List<PermitContentTypeResponses>, var userFullName:String = String().empty(), var isSavedImage:Boolean = false) : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

    inner class PermitContentTypePhotoViewHolder(var binding: ItemPermitContentTypeTakePhotoChildBinding):RecyclerView.ViewHolder(binding.root)

    fun setNewPayLoad(newResponsesList:List<PermitContentTypeResponses>){
        this.responsesList = newResponsesList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemPermitContentTypeTakePhotoChildBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PermitContentTypePhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as PermitContentTypePhotoViewHolder
        Glide.with(holder.itemView.context).load(responsesList[position].value).into(holder.binding.thumbnailImage)
        holder.binding.photoName.text = if(isSavedImage && responsesList[position].user != null)
            responsesList[position].id.toString() + " - " + responsesList[position].user!!.first_name + String().emptySpace() + responsesList[position].user!!.last_name
        else
            holder.itemView.context.getText(R.string.unsaved_take_photo_prefix).toString() + String().emptySpace() + userFullName
    }

    override fun getItemCount(): Int = responsesList.size
}