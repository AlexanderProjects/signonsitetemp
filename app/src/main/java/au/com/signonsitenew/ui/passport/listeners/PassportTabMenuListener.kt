package au.com.signonsitenew.ui.passport.listeners

interface PassportTabMenuListener {
    fun passportMenuTabSelected(position: Int)
}