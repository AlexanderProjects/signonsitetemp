package au.com.signonsitenew.ui.main.documents

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.DocumentsRecyclerViewAdapter
import au.com.signonsitenew.api.API.ResponseCallback
import au.com.signonsitenew.api.SOSAPI
import au.com.signonsitenew.api.SiteInductionInformation
import au.com.signonsitenew.domain.models.Induction
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.events.BriefingEvent
import au.com.signonsitenew.events.InductionEvent
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.realm.services.*
import au.com.signonsitenew.utilities.*
import dagger.android.support.DaggerFragment
import io.realm.Realm
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import javax.inject.Inject

/**
 * Fragment to display list of documents available to the user on site.
 */
class DocumentsFragment : DaggerFragment(), DocumentsDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenter: DocumentsFragmentPresenter by viewModels { viewModelFactory  }

    private lateinit var mSession: SessionManager
    private lateinit var mRealm: Realm
    private lateinit var mDocuments: MutableList<Document>
    private lateinit var mRecyclerView: RecyclerView
    private var mListener: OnListFragmentInteractionListener? = null
    private lateinit var mAdapter: DocumentsRecyclerViewAdapter
    private lateinit var userService: UserService
    private lateinit var siteSettingsService: SiteSettingsService
    private lateinit var userAbilitiesService: UserAbilitiesService
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var isManager = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mRealm = Realm.getDefaultInstance()
        EventBus.getDefault().register(this)
        presenter.registerNotificationListener()
        presenter.inject(this)
        presenter.observeStates()
        mSession = SessionManager(activity)
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_documents, container, false)

        mRecyclerView = rootView.findViewById(R.id.documents_list)

        // Set the adapter
        val context = rootView.context
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        refreshLayout = rootView.findViewById(R.id.documents_swipe_refresh)
        refreshLayout.setOnRefreshListener {
            retrieveUserPermissions()
            presenter.getActiveDocuments()
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        presenter.getActiveDocuments()
        retrieveUserPermissions()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is OnListFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (::mRealm.isInitialized) {
            mRealm.close()
        }
        EventBus.getDefault().unregister(this)
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }
    // Get the documents for the correct site. At this point documents are only appropriate when signed onto a site.
    private fun setDocuments(briefing: WorkerBriefing?, induction: Induction?) {
        // Get the documents for the correct site. At this point documents are only appropriate when signed onto a site.
        refreshLayout.isRefreshing = false
        mListener?.isThereADocActive(false)
        val settingsService = SiteSettingsService(mRealm)
        val siteId = settingsService.siteId
        if (siteId == 0L) {
            // Need a settings object to enforce inductions, signout
            SOSAPI(activity).signOffSite(true, null, null, null, null)
        }

        // Clear existing documents
        if (!::mDocuments.isInitialized) {
            mDocuments = ArrayList()
        } else {
            mDocuments.clear()
        }

        briefing?.let {
            val briefingDoc = presenter.buildBriefingDocument(briefing,notifierCallback = { presenter.addNotificationNumber() },listener = { mListener?.onBriefingNotificationSend(it) })
            mDocuments.add(briefingDoc)
            if(briefing.needs_acknowledgement)
                mListener?.isThereADocActive(true)
        }?: run {
            mDocuments.add(presenter.getEmptyBriefingDocument())
        }

        induction?.let {
            val inductionDoc = presenter.buildInductionDocument(induction,notifierCallback = { presenter.addNotificationNumber() })
            mDocuments.add(inductionDoc)
            if (induction.state?.as_string == Constants.DOC_INDUCTION_INCOMPLETE || induction.state?.as_string == Constants.DOC_INDUCTION_REJECTED)
                mListener?.isThereADocActive(true)
        }?: run {
            mDocuments.add(presenter.getEmptyInductionDocument())
        }

        presenter.isEnablePermitsFeatureFlags {
            mDocuments.add(presenter.buildPermitDocument())
            mAdapter.notifyDataSetChanged()
        }
        mDocuments = presenter.checkForEmptyDocs(mDocuments)
        presenter.updateBadgeNotificationNumber()
    }

    private fun retrieveUserPermissions() {
        //Check for permissions
        userService = UserService(mRealm)
        siteSettingsService = SiteSettingsService(mRealm)
        userAbilitiesService = UserAbilitiesService(mRealm)
        isManager = userAbilitiesService.hasManagerPermissions(userService.currentUserId, siteSettingsService.siteId)
    }

    @Subscribe
    fun onBriefingEvent(event: BriefingEvent) {
        Log.i(LOG, "Event received. $event")
        if (event.eventType == Constants.EVENT_NEW_BRIEFING && activity != null) {
            presenter.getActiveDocuments()
        }
    }

    @Subscribe
    fun onInductionEvent(event: InductionEvent) {
        Log.i(LOG, "Event received. $event")
        if (event.eventType == Constants.FCM_INDUCTION_REJECTED && activity != null) {
            SiteInductionInformation.get(activity, ResponseCallback {
                presenter.getActiveDocuments()
            }, null)
        }
    }

    override fun updateDocumentsState(workerBriefing: WorkerBriefing?, induction: Induction?) {
        activity?.let {
            setDocuments(workerBriefing, induction)
            setAdapter(mDocuments, workerBriefing)
        }
   }

    override fun showDataErrors(error: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireActivity(), error)
        showNetworkErrorRow()
    }

    override fun showNetworkError() {
        AlertDialogMessageHelper.networkDocumentsErrorMessage(requireActivity()) { presenter.getActiveDocuments() }
        showNetworkErrorRow()
    }

    private fun showNetworkErrorRow(){
        refreshLayout.isRefreshing = false
        mDocuments = mutableListOf()
        mDocuments.add(presenter.getErrorDocument())
        setAdapter(mDocuments, null)
    }

    private fun setAdapter(documents: MutableList<Document>, workerBriefing: WorkerBriefing?){
        refreshLayout.isRefreshing = false
        if (::mAdapter.isInitialized) {
            mAdapter.refill(documents, workerBriefing)
            mAdapter.notifyDataSetChanged()
        } else {
            mAdapter = DocumentsRecyclerViewAdapter(documents, workerBriefing, mListener,isManager)
            mRecyclerView.adapter = mAdapter
        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnListFragmentInteractionListener {
        fun onDocumentSelected(document: Document?, briefing: WorkerBriefing)
        fun onBriefingNotificationSend(briefing: WorkerBriefing)
        fun onClickSeeAllButton(document: Document?)
        fun onClickNewButton(document: Document?)
        fun isThereADocActive(boolean: Boolean)
    }

    companion object {
        private val LOG = DocumentsFragment::class.java.simpleName
    }

}