package au.com.signonsitenew.ui.evacuation

import au.com.signonsitenew.domain.models.ApiResponse

interface EmergencyInitialDisplay {
    fun showEmergencyError(error:Throwable)
    fun showEmergencyTriggerError(apiResponse: ApiResponse)
    fun getEvacuationVisitor()
    fun navigateToEmergencyProgressActivity()
}