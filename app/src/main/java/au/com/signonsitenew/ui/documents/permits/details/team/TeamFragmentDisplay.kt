package au.com.signonsitenew.ui.documents.permits.details.team

import au.com.signonsitenew.domain.models.RequesteeUser

interface TeamFragmentDisplay {
    fun setRequestorCompany(requesterUser: String, isReadOnly:Boolean)
    fun setRequestor(requesterUser: String,isReadOnly:Boolean)
    fun setAddTeamMembersButtonVisibility(isReadOnly: Boolean)
    fun showSelectedMembersList(memberList:List<RequesteeUser>)
    fun showErrors()
    fun showPermitError()
    fun navigationToAddTeamMembers()
    fun saveUnSelectedMembers(memberList:List<RequesteeUser>)
    fun hideTeamMemberPermitRequestMessage()
    fun showTeamMemberPermitRequestMessage()
}