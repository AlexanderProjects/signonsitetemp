package au.com.signonsitenew.ui.passport.connections.connectiondetails

import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.FeatureFlagsManager
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ConnectionDetailsFragment : DaggerFragment(), ConnectionDetailsDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager

    lateinit var siteName:TextView
    lateinit var siteOwner:TextView
    lateinit var siteAddress:TextView
    lateinit var siteSignOnCompanyName:TextView
    lateinit var siteInductionStatus:TextView
    lateinit var siteLastSignOn:TextView
    private lateinit var endConnectionButton: Button
    lateinit var connectionToolbar:Toolbar
    lateinit var enrolment: Enrolment

    companion object {
        private lateinit var connectionListLoader: ConnectionsListLoader

        fun newInstance(enrolment: Enrolment) : ConnectionDetailsFragment {
            val connectionDetails = ConnectionDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constants.CONNECTION_FLAG, enrolment)
            connectionDetails.arguments = bundle
            return connectionDetails
        }

        fun setCredentialsListLoader(connectionListUpdate: ConnectionsListLoader) {
            connectionListLoader = connectionListUpdate
        }
    }

    private lateinit var presenterImpl: ConnectionDetailsPresenterImpl

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.connection_details_fragment, container, false)
        enrolment = requireArguments().getParcelable(Constants.CONNECTION_FLAG)!!
        siteName = view.findViewById(R.id.site_connection_details_value)
        siteOwner = view.findViewById(R.id.site_connection_details_owner_value)
        siteAddress = view.findViewById(R.id.site_connection_details_address_value)
        siteSignOnCompanyName = view.findViewById(R.id.site_connection_details_company_value)
        siteInductionStatus = view.findViewById(R.id.site_connection_details_induction_status_value)
        siteLastSignOn = view.findViewById(R.id.site_connection_details_last_sign_on_value)
        endConnectionButton = view.findViewById(R.id.end_connection_button)
        connectionToolbar = view.findViewById(R.id.toolbar_connection_details)
        connectionToolbar.title = enrolment.site.name
        (activity as AppCompatActivity?)!!.setSupportActionBar(connectionToolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        endConnectionButton.setOnClickListener { AlertDialogMessageHelper.removeConnectionsAlertDialog(it.context,action = { presenterImpl.updateEnrolments(enrolment)}) }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData(){
        siteName.text = enrolment.site.name
        siteOwner.text = enrolment.site.principal_company.name
        siteAddress.text = enrolment.site.address
        siteSignOnCompanyName.text = enrolment.company.name
        enrolment.site_induction.let {
            siteInductionStatus.text = when(it?.state){
                "pending" -> "Not Inducted"
                else -> "Inducted"
            }
            siteInductionStatus.visibility = when(it?.state){
                null -> View.INVISIBLE
                else -> View.VISIBLE
            }
            siteInductionStatus.setTextColor(when(it?.state){
                "pending" -> Color.RED
                null -> Color.RED
                else -> Color.DKGRAY
            })
        }
        enrolment.utc_last_signon_at.let {
            siteLastSignOn.text = when(it){
                null -> "None"
                else -> presenterImpl.getDateTimeZone(enrolment)
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this,viewModelFactory).get(ConnectionDetailsPresenterImpl::class.java)
        presenterImpl.inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.share_button)?.let { it.isVisible = false }
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun reloadData() {
        connectionListLoader.refreshListOfConnections()
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE
        ) { presenterImpl.updateEnrolments(enrolment) }
    }

    override fun showDataErrors(message: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),message)
    }

    interface ConnectionsListLoader {
        fun refreshListOfConnections()
    }

}
