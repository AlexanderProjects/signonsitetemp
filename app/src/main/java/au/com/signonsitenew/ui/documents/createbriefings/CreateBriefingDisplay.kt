package au.com.signonsitenew.ui.documents.createbriefings

interface CreateBriefingDisplay {
    fun showAdvanceBriefingLayout()
    fun showDefaultBriefingLayout()
}