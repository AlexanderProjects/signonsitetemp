package au.com.signonsitenew.ui.passport.credentials.credentialdetails

interface CredentialUpdateView {
    fun showNetworkErrors()
    fun goBackToCredentials()
    fun showDataError(errorMessage:String)
}