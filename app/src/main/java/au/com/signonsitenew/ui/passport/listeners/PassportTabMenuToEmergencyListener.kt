package au.com.signonsitenew.ui.passport.listeners

interface PassportTabMenuToEmergencyListener {
    fun passportMenuTabSelected(position: Int)
}