package au.com.signonsitenew.ui.prelogin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * This is the activity that displays if the user is not logged in. It currently just gives the
 * options to log in or register, but there could be a bit of an explainer put here in the future.
 */
class StartActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService

    private lateinit var mNewUserButton: Button
    private lateinit var mExistingUserButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        // Link the buttons
        mNewUserButton = findViewById(R.id.newUserButton)
        mExistingUserButton = findViewById(R.id.existingUserButton)
        mNewUserButton.setOnClickListener {
            // Navigate to Login
            val intent = Intent(this@StartActivity, LoginActivity::class.java)
            startActivity(intent)
        }
        mExistingUserButton.setOnClickListener {
            // Navigate to Register
            analyticsEventDelegateService.registrationStarted()
            val intent = Intent(this@StartActivity, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        private val TAG = StartActivity::class.java.simpleName
    }
}