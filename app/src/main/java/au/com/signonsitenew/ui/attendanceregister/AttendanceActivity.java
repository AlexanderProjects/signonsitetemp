package au.com.signonsitenew.ui.attendanceregister;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

import javax.inject.Inject;

import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.EnrolledWorkerSearchRecyclerViewAdapter.OnUserSelectedListener;
import au.com.signonsitenew.api.API;
import au.com.signonsitenew.api.EnrolledUsers;
import au.com.signonsitenew.api.RegisterVisitor;
import au.com.signonsitenew.api.TodaysVisits;
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.EnrolledUser;
import au.com.signonsitenew.realm.ManagedCompany;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.services.EnrolledUserService;
import au.com.signonsitenew.ui.attendanceregister.AttendancePeopleFragment.OnVisitorInteractionListener;
import au.com.signonsitenew.ui.attendanceregister.RegisterUserFragment.OnRegisterWorkerInteractionListener;
import au.com.signonsitenew.ui.attendanceregister.attendance.AttendanceRegisterFragment;
import au.com.signonsitenew.ui.attendanceregister.workerdetails.WorkerDetailsFragment;
import au.com.signonsitenew.ui.navigation.Router;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DarkToast;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import dagger.android.support.DaggerAppCompatActivity;
import io.realm.Realm;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Activity to handle all aspects of the Visitor Register screens in the Management Panel.
 * The main operational areas of the fragments are:
 *  AttendanceListPeopleFragment - hosts two Fragments with lists of users and users by company
 *  displaying all users who have signed onto the site that day or are currently on site.
 *      Depending on the site configuration, it will display information about users who are not
 *      inducted onto site. Pressing on users allows further details to be displayed.
 *  EnrolledUsersSearchFragment - Shows all users who have been are enrolled on the site (have been
 *      to site at some point). Also also allows the user to press on the person for more actions.
 *  UserDetailFragment - activated after a user is selected. This allows further information on
 *      the user to ve viewed, and actions to be taken such as changing the user to a visitor or
 *      removing them from site altogether. Induction-related actions can also be performed on the user.
 *  RegisterUserFragment - allows Supervisors/Managers to register new users on site.
 *  Induction*Fragment - allows users to perform actions related to inductions. These include viewing,
 *      reviewing, and changing user status.
 */

public class AttendanceActivity extends DaggerAppCompatActivity implements OnVisitorInteractionListener,
                                                                     OnUserSelectedListener, AttendanceRegisterFragment.ShowProgressViewForNewAttendance,
                                                                     OnRegisterWorkerInteractionListener, InductionCancelFragment.VisitorStatusListener {

    private static final String TAG = AttendanceActivity.class.getSimpleName();

    @Inject
    Router router;

    @Inject
    AnalyticsEventDelegateService analyticsEventDelegateService;

    private Realm mRealm;
    protected Toolbar mToolbar;
    protected TextView mTitleTextView;
    protected FragmentManager mFragmentManager;
    protected LinearLayout mProgressView;
    protected ImageView mProgressSpinner;
    protected TextView mProgressStatusTextView;
    private Bundle extras;
    private  UpdateVisitorStatusInUserDetailsListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor);

        mRealm = Realm.getDefaultInstance();

        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mTitleTextView = mToolbar.findViewById(R.id.toolbar_title_text_view);
        mTitleTextView.setText("Attendance Register");
        mProgressView = findViewById(R.id.progress_linear_layout);
        mProgressSpinner = findViewById(R.id.progress_spinner_image_view);
        mProgressStatusTextView = findViewById(R.id.progress_action_text_view);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mFragmentManager = getSupportFragmentManager();

        extras = getIntent().getExtras();
        if(extras != null){
            if(Objects.equals(extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY),Constants.FCM_WORKER_NOTE_ALERT)){
                loadVisitorTabsWithExtras(extras);
            }
            if(Objects.equals(extras.getString(Constants.FCM_NOTIFICATION_NAME_KEY),Constants.FCM_WORKER_NOTE_ALERT)){
                analyticsEventDelegateService.notificationClicked(Constants.FCM_WORKER_NOTE_ALERT,Constants.ANALYTICS_REMOTE_NOTIFICATION, new HashMap(),null);
                analyticsEventDelegateService.managerAttendanceRegisterViewed();
            }
        }

        loadVisitorTabs();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showContentView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    public void setUpdateVisitorListener(UpdateVisitorStatusInUserDetailsListener listener){
        this.listener = listener;
    }

    private void loadVisitorTabs() {
        // Load initial Fragment
        Fragment fragment = new AttendanceRegisterFragment();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

    private void loadVisitorTabsWithExtras(Bundle extras){
        Fragment fragment = new AttendanceRegisterFragment();
        fragment.setArguments(extras);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

    protected void navigateToFragment(Fragment fragment, boolean shouldAddToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right);
        transaction.replace(R.id.container, fragment);

        // Add the transaction to the back stack to preserve back navigation
        if (shouldAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_visitor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        hideKeyboard(this);
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
                if(fragment instanceof AttendanceRegisterFragment){
                    router.navigateToSignedOnActivity(this);
                }else {
                    getSupportFragmentManager().popBackStack();
                }
                if(fragment instanceof WorkerDetailsFragment){
                    router.navigateToAttendanceRegisterFragment(this);
                }
                break;
            case R.id.view_all_users:
                fetchEnrolments();
                break;
            case R.id.add_attendee:
                analyticsEventDelegateService.attendanceRegisterWorkerSearchForWorkerNewPressed();
                navigateToFragment(new RegisterUserFragment(), true);
                break;
        }
        return true;
    }

    protected static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void fetchEnrolments() {
        // Display the loading view
        showVisitorProgressView();

        // Retrieve Enrolled Users
        EnrolledUsers.get(this,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray enrolledUsersArray;
                        try {
                            if (response.getString(Constants.JSON_STATUS).equals(Constants.JSON_SUCCESS)) {
                                // Visitors currently signed on retrieved
                                enrolledUsersArray = response.getJSONArray(Constants.JSON_ENROLLED_USERS);

                                // Store Visitors
                                EnrolledUserService enrolledUserService = new EnrolledUserService(mRealm);

                                // Remove old records first
                                enrolledUserService.deleteAllEnrolledUsers();

                                // Add new users
                                for (int i = 0; i < enrolledUsersArray.length(); i++) {
                                    JSONObject person = enrolledUsersArray.getJSONObject(i);
                                    enrolledUserService.createOrUpdateEnrolledUser(person);
                                }

                                // Remove all Managed Companies from realm
                                mRealm.beginTransaction();
                                mRealm.where(ManagedCompany.class).findAll().deleteAllFromRealm();

                                // Check to see if user has restricted access to companies (ie Subcontractor Supervisor)
                                if (!response.getBoolean(Constants.JSON_MANAGE_ALL_COMPANIES)) {
                                    // User can manage limited companies - store the companies from response in Realm
                                    JSONArray managedCompaniesArray = response.getJSONArray(Constants.JSON_MANAGED_COMPANIES);

                                    for (int i = 0; i < managedCompaniesArray.length(); i++) {
                                        ManagedCompany company = mRealm.createObject(ManagedCompany.class);
                                        company.setCompanyName(managedCompaniesArray.get(i).toString());
                                    }
                                    SLog.i(TAG, "Managed Companies: " + managedCompaniesArray.toString());
                                }
                                mRealm.commitTransaction();

                                // Segment Event
                                analyticsEventDelegateService.attendanceRegisterWorkerSearchForWorkerViewed();
                                // Navigate to Fragment
                                navigateToFragment(new EnrolledUsersSearchFragment(), true);

                                showContentView();
                            }
                            else {
                                // Cancel loading screen
                                showContentView();
                            }
                        }
                        catch (JSONException e) {
                            SLog.e(TAG, "A JSON Exception occurred: " + e.getMessage());
                            showContentView();
                        }
                    }
                },
                new API.ErrorCallback() {
                    @Override
                    public void onError() {
                        // Remove the progress view
                        showContentView();
                    }
                });
    }

    /**
     * This function causes the content view to be replaced with a search view with progress spinner.
     *
     * This function should be called when the user selects the "Add User" function from the menu,
     * while the enrolled users are fetched from the server. It should be dismissed once the users
     * are successfully fetched or the API call fails.
     */
    protected void showVisitorProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mProgressView.setVisibility(View.VISIBLE);
        mProgressStatusTextView.setText("Fetching users...");

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    protected void showDetailProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mProgressView.setVisibility(View.VISIBLE);
        mProgressStatusTextView.setText("Updating user...");

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    private void showVisitorRegisterProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mProgressView.setVisibility(View.VISIBLE);
        mProgressStatusTextView.setText("Retrieving site visitors...");

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }


    protected void showRegisteringProgressView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mProgressView.setVisibility(View.VISIBLE);
        mProgressStatusTextView.setText("Registering Worker...");

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotate_around_center);
        mProgressSpinner.startAnimation(animation);
    }

    protected void showContentView() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "Request Code: " + requestCode);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setTitle("Congratulations! You're ready to go")
                            .setMessage("Click on upload again to get started!")
                            .setPositiveButton("Let\'s do this", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    // User accepted at initial screen - open proper permission dialog
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    DarkToast.makeText(this, "You will need to grant permission to upload photos");
                }
            }
            case 2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setTitle("Congratulations! You're ready to go")
                            .setMessage("Click on upload again to get started!")
                            .setPositiveButton("Let\'s do this", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    // User accepted at initial screen - open proper permission dialog
                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    DarkToast.makeText(this, "You will need to grant permission to upload photos");
                }
            }
        }
    }

    @Override
    public void onVisitorSelected(SiteAttendee visitor) {
        // The user selected a visitor in one of the VisitorsFragments.
        // Navigate to details fragment and display the appropriate details
        UserDetailFragment detailFragment = new UserDetailFragment();
        Bundle args = new Bundle();
        args.putLong(UserDetailFragment.ATTENDEE_ID, visitor.getUserId());
        detailFragment.setArguments(args);

        navigateToFragment(detailFragment, true);

        hideKeyboard(this);
    }

    @Override
    public void onUserSelected(long userId, String companyName) {
        //segment event
        analyticsEventDelegateService.attendanceRegisterWorkerSearchForWorkerSelected((int)userId);
        // Pass the user's ID and company back to the previous activity in the Result
        UserDetailFragment detailFragment = new UserDetailFragment();
        Bundle args = new Bundle();
        args.putLong(UserDetailFragment.ATTENDEE_ID, userId);
        detailFragment.setArguments(args);
        navigateToFragment(detailFragment, true);
        hideKeyboard(this);
    }

    @Override
    public void onAttachFragment(@NonNull Fragment fragment) {
        super.onAttachFragment(fragment);
        if(fragment instanceof AttendanceRegisterFragment)
            ((AttendanceRegisterFragment) fragment).setShowProgressView(this);
        if(fragment instanceof InductionCancelFragment){
            ((InductionCancelFragment) fragment).setVisitorStatusListener(this);
        }
    }

    @Override
    public void onRegisterPressed(final String firstName, final String lastName, final String phoneNumber, final String companyName) {
        showRegisteringProgressView();
        final int siteId = new SessionManager(this).getSiteId();

        RegisterVisitor.post(this, siteId, firstName, lastName, companyName, phoneNumber,
                new API.ResponseCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showContentView();
                        String status = "";
                        try {
                            status = response.getString(Constants.JSON_STATUS);
                        }
                        catch (JSONException e) {
                            SLog.e(TAG, "A JSON Exception occurred: " + e.getMessage());
                        }

                        if (status.equals(Constants.JSON_SUCCESS)) {
                            // We're good
                            DarkToast.makeText(AttendanceActivity.this, "Successfully signed " + firstName + " on!");
                            Long userId = null;
                            Long visitId = null;
                            try {
                                userId = response.getLong("user_id");
                                visitId = response.getLong("visit_id");
                            }
                            catch (JSONException e) {
                                SLog.e(TAG, e.getMessage());
                            }
                            //segment event
                            analyticsEventDelegateService.attendanceRegisterNewWorkerSignOnPressed();
                            // Set check in time to now "2017-04-06 06:50:15"
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String formattedDate = sdf.format(calendar.getTime());

                            // Add the user to our Realm visits and enrolments so that the data does not have to be re-fetched
                            mRealm.beginTransaction();
                            EnrolledUser enrolment = new EnrolledUser();
                            enrolment.setId(userId);
                            enrolment.setFirstName(firstName);
                            enrolment.setLastName(lastName);
                            enrolment.setPhoneNumber(phoneNumber);
                            enrolment.setCompanyName(companyName);
                            enrolment.setInductionStatus(Constants.DOC_INDUCTION_INCOMPLETE);
                            mRealm.copyToRealmOrUpdate(enrolment);

                            SiteAttendee visitor = new SiteAttendee();
                            visitor.setUserId(userId);
                            visitor.setFirstName(firstName);
                            visitor.setLastName(lastName);
                            visitor.setPhoneNumber(phoneNumber);
                            visitor.setCompany(companyName);
                            visitor.setInductionStatus(Constants.DOC_INDUCTION_INCOMPLETE);
                            visitor.setCheckInTime(null);
                            visitor.setMarkedSafe(false);
                            mRealm.copyToRealmOrUpdate(visitor);

                            AttendanceRecord attendanceRecord = new AttendanceRecord();
                            attendanceRecord.setId(visitId);
                            attendanceRecord.setUserId(userId);
                            attendanceRecord.setSiteId((long) siteId);
                            attendanceRecord.setCheckInTime(formattedDate);
                            attendanceRecord.setCheckOutTime("null");
                            mRealm.copyToRealmOrUpdate(attendanceRecord);
                            mRealm.commitTransaction();

                            // Update visits
                            TodaysVisits.get(AttendanceActivity.this, null, null);

                            // Navigate back to previous Attendance list
                            getSupportFragmentManager().popBackStack();
                            getSupportFragmentManager().popBackStack();
                        }
                        else if (status.equals(Constants.JSON_BAD_PHONE)) {
                            // Display an error
                            DarkToast.makeText(AttendanceActivity.this, "Please enter a valid phone number");
                        }
                        else {
                            DarkToast.displayGenericErrorToast(AttendanceActivity.this);
                        }
                    }
                }, () -> {
                    showContentView();
                    DarkToast.displayGenericErrorToast(AttendanceActivity.this);
                });
    }

    @Override
    public void showProgressViewForAttendance() {
        showVisitorRegisterProgressView();
    }

    @Override
    public void hideShowProgressViewForAttendance() {
        showContentView();
    }

    @Override
    public void updateStatusToVisitor() {
        listener.updateVisitorStatus();
    }

    public interface UpdateVisitorStatusInUserDetailsListener{
        void updateVisitorStatus();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment =  getSupportFragmentManager().findFragmentById(R.id.container);
        if(fragment instanceof AttendanceRegisterFragment){
            router.navigateToSignedOnActivity(this);
        }
        if(fragment instanceof WorkerDetailsFragment)
            router.navigateToAttendanceRegisterFragment(this);

        super.onBackPressed();
    }
}
