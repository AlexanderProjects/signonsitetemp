package au.com.signonsitenew.ui.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.SiteInduction;
import au.com.signonsitenew.realm.services.SiteInductionService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;

public class InductionViewerActivity extends AppCompatActivity {
    private static final String TAG = InductionViewerActivity.class.getSimpleName();

    private ProgressDialog mDialog;
    private WebView mWebView;
    private Button mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_induction_viewer);

        mWebView = findViewById(R.id.webview);
        mBackButton = findViewById(R.id.back_button);

        mBackButton.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Finish the activity
                finish();
            }
        });

        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Loading induction...");
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(true);
        mDialog.show();

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Prepare URL and Headers for the WebView
        Realm realm = Realm.getDefaultInstance();
        UserService userService = new UserService(realm);
        SiteInductionService inductionService = new SiteInductionService(realm);

        long userId = userService.getCurrentUserId();
        SiteInduction induction = inductionService.getSiteInductionForUser(userId);
        int inductionId = induction.getId();
        String inductionType = induction.getType();
        Log.i(TAG, "induction type: " + inductionType);
        realm.close();

        SessionManager session = new SessionManager(this);
        String jwt = session.getToken();

        String url;
        switch (inductionType) {
            case "view":
                url = Constants.URL_INDUCTION_FORM_BASE + inductionId + Constants.URL_INDUCTION_VIEW;
                mWebView.addJavascriptInterface(new WebFormJSInterface(this), "Android");
                break;
            case "forced_with_documents":
                url = Constants.URL_INDUCTION_FORM_BASE + inductionId + Constants.URL_INDUCTION_VIEW_DOCS;
                break;
            default:
                Log.e(TAG, "Unknown action");
                url = Constants.URL_INDUCTION_FORM_BASE + inductionId + Constants.URL_INDUCTION_VIEW;
                mWebView.addJavascriptInterface(new WebFormJSInterface(this), "Android");
        }

        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + jwt);
        mWebView.loadUrl(url, headers);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                mDialog.dismiss();
                super.onPageFinished(view, url);
            }

            /**
             * This method sets whether the page should load in the webview or in the external
             * browser. To avoid navigating away from the induction form (and losing progress),
             * open all external links (such as uploaded images) in the device's default browser.
             * In practical use - they should never leave the page, so handle as relevant.
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Launch another Activity that handles URLs
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
        });

    }
}
