package au.com.signonsitenew.ui.documents.permits.template

import au.com.signonsitenew.domain.models.SitePermitTemplate
import au.com.signonsitenew.domain.models.state.TemplatePermitFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitTemplateUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.applySchedulers
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface TemplatePermitFragmentPresenter{
    fun inject(display: TemplatePermitsDisplay)
    fun getPermitTemplates()
    fun observeStates()
    fun createPermit(template:SitePermitTemplate)
}

class TemplatePermitFragmentPresenterImpl  @Inject constructor (private val permitTemplateUseCase: PermitTemplateUseCaseImpl,
                                                              private val logger: Logger):BasePresenter(), TemplatePermitFragmentPresenter {
    lateinit var display: TemplatePermitsDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<TemplatePermitFragmentState>()

    override fun inject(display: TemplatePermitsDisplay) {
        this.display = display
    }

    override fun getPermitTemplates() {
        disposables.add(permitTemplateUseCase.retrieveSitePermitTemplateAsync()
            .compose(applySchedulers())
            .doOnSubscribe { uiStateObservable.onNext(TemplatePermitFragmentState.ShowProgressView) }
            .doAfterTerminate { uiStateObservable.onNext(TemplatePermitFragmentState.RemoveProgressView) }
            .subscribe ({
                if(it.status == Constants.JSON_SUCCESS) {
                    uiStateObservable.onNext(TemplatePermitFragmentState.ShowTemplateList(it.site_permit_templates))
                }else{
                    uiStateObservable.onNext(TemplatePermitFragmentState.ShowError)
                    logger.w(this@TemplatePermitFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TemplatePermitFragmentPresenterImpl::getPermitTemplates.name to toJson(it)))
                }
        },{
                uiStateObservable.onNext(TemplatePermitFragmentState.ShowError)
                logger.e(this@TemplatePermitFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TemplatePermitFragmentPresenterImpl::getPermitTemplates.name to it.message))
        }))
    }

    override fun observeStates() {
        disposable.add(uiStateObservable.subscribe {
            when(it){
                is TemplatePermitFragmentState.ShowTemplateList -> display.showPermitTemplateList(it.templateList)
                is TemplatePermitFragmentState.ShowError -> display.showPermitTemplateError()
                is TemplatePermitFragmentState.NavigateToPermitDetails -> display.navigateToPermitDetails(it.permitInfo)
                is TemplatePermitFragmentState.ShowProgressView -> display.showProgressView()
                is TemplatePermitFragmentState.RemoveProgressView -> display.removeProgressView()
            }
        })
    }

    override fun createPermit(template: SitePermitTemplate) {
        disposables.add(permitTemplateUseCase.createPermitAsync(template.permit_template, mutableListOf(),null,null)
            .flatMap { permitTemplateUseCase.retrievePermitInfoAsync(it.permit_id) }
            .compose(applySchedulers())
            .doOnSubscribe { uiStateObservable.onNext(TemplatePermitFragmentState.ShowProgressView) }
            .doAfterTerminate { uiStateObservable.onNext(TemplatePermitFragmentState.RemoveProgressView) }
            .subscribe({
                if(it.status == Constants.JSON_SUCCESS) {
                    uiStateObservable.onNext(TemplatePermitFragmentState.NavigateToPermitDetails(it.permit))
                }else{
                    uiStateObservable.onNext(TemplatePermitFragmentState.ShowError)
                    logger.w(this@TemplatePermitFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TemplatePermitFragmentPresenterImpl::createPermit.name to toJson(it)))
                }
            },{
                uiStateObservable.onNext(TemplatePermitFragmentState.ShowError)
                logger.e(this@TemplatePermitFragmentPresenterImpl::class.java.name, attributes = mapOf(this@TemplatePermitFragmentPresenterImpl::createPermit.name to it.message))
            }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        disposables.dispose()
    }

}