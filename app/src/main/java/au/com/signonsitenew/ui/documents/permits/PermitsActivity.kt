package au.com.signonsitenew.ui.documents.permits

import android.os.Bundle
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragment
import au.com.signonsitenew.ui.documents.permits.details.PermitDetailsFragment
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMembersFragment
import au.com.signonsitenew.ui.documents.permits.template.TemplatePermitFragment
import au.com.signonsitenew.ui.factory.PermitFragmentFactory
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.camera.CameraXFragment
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class PermitsActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var router:Router


    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = PermitFragmentFactory()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permits)
        router.navigateToCurrentPermits(this)
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.doc_permits_container)){
            is CurrentPermitsFragment -> router.navigateToDocumentList(this)
            is TemplatePermitFragment -> router.navigateToPermitActivity(this)
            is PermitDetailsFragment -> router.navigateToCurrentPermits(this)
            is AddTeamMembersFragment -> router.navigateToPermitDetails(this)
            is CameraXFragment -> router.navigateToPreviousView(this)
            else -> super.onBackPressed()
        }
    }

}