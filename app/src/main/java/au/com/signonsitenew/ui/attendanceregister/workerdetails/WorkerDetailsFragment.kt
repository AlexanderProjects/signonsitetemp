package au.com.signonsitenew.ui.attendanceregister.workerdetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentWorkerDetailsBinding
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.DarkToast
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class WorkerDetailsFragment : DaggerFragment(), WorkerDetailsFragmentDisplay {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var router: Router

    lateinit var presenterImpl: WorkerDetailsFragmentPresenterImpl

    private var _binding: FragmentWorkerDetailsBinding? = null
    private val binding get() = _binding!!
    private lateinit var attendanceChildAdapterModel: AttendanceChildAdapterModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            attendanceChildAdapterModel = it.getParcelable(workerAttendanceChildModel)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        (requireActivity() as AppCompatActivity).supportActionBar!!.hide()
        presenterImpl= ViewModelProvider(this, viewModelFactory).get(WorkerDetailsFragmentPresenterImpl::class.java)
        presenterImpl.inject(this)
        _binding = FragmentWorkerDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.detailsWorkerToolbar.title = "Details"
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.detailsWorkerToolbar)
        (requireActivity() as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        binding.attendeeNameTextView.text = attendanceChildAdapterModel.workerName
        binding.attendeeCompanyTextView.text = attendanceChildAdapterModel.companyName
        binding.attendeePhoneNumberTextView.text = attendanceChildAdapterModel.phoneNumber
        binding.siteInductionIcon.setImageResource(presenterImpl.getInductionIconState(attendanceChildAdapterModel))
        binding.dailyBriefingImage.setImageResource(presenterImpl.getBriefingIconState(attendanceChildAdapterModel))
        binding.workerNotesBoxCell.visibility = presenterImpl.getWorkerNotesCellState(attendanceChildAdapterModel.workerNotes)
        if(binding.workerNotesBoxCell.isVisible) binding.workerNotesBoxImage.setImageResource(presenterImpl.getWorkerNotesIconState(attendanceChildAdapterModel.workerNotes))
        binding.workerNotesTotalText.text = attendanceChildAdapterModel.workerNotes?.size.toString()
        binding.attendeeHistoryTotalText.text = attendanceChildAdapterModel.attendances.size.toString()
        binding.workerNotesCell.setOnClickListener {
            presenterImpl.analyticsForWorkerNotes()
            router.navigateToWorkerNotes(requireActivity(),attendanceChildAdapterModel)
        }
        binding.attendeeInductionCell.setOnClickListener {
            presenterImpl.getFragmentAccordingToInductionState(attendanceChildAdapterModel)?.let {
                router.navigateToInductionForms(requireActivity(),it)
            }
        }
        binding.attendeeHistoryCell.setOnClickListener { router.navigationToUserAttendanceHistoryFragment(requireActivity(), attendanceChildAdapterModel.userId.toLong()) }
        binding.attendeeCallCell.setOnClickListener {
            if(attendanceChildAdapterModel.phoneNumber != null) {
                val number: String =
                    attendanceChildAdapterModel.phoneNumber.toString().trim { it <= ' ' }
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:$number")
                startActivity(intent)
            }else{
                AlertDialogMessageHelper.showUserDoesNotHavePhoneNumberError(requireContext())
            }
        }
        binding.attendeeSignButton.text = getString(presenterImpl.getSignButtonState(attendanceChildAdapterModel))
        binding.attendeeSignButton.setOnClickListener {
            if(attendanceChildAdapterModel.timeOut.equals("null"))
                presenterImpl.signOffAttendee(attendanceChildAdapterModel.userId.toString())
            else
                presenterImpl.signOnAttendee(attendanceChildAdapterModel.userId.toString())
        }
        binding.attendeeInductionCell.visibility = presenterImpl.getInductionCellState(attendanceChildAdapterModel)
        binding.attendeeHistoryText.text = attendanceChildAdapterModel.workerName.substring(0,attendanceChildAdapterModel.workerName.indexOf(" ")) +" "+ "visits today"
        binding.attendeeCallText.text = "Call"+" "+ attendanceChildAdapterModel.workerName.substring(0,attendanceChildAdapterModel.workerName.indexOf(" "))
        binding.attendeeInductionCell.visibility = presenterImpl.showCellIfUserHasPermissions(attendanceChildAdapterModel)
        binding.workerNotesCell.visibility = presenterImpl.canAccessToWorkingNotes(attendanceChildAdapterModel)
        binding.workerNotesBoxCell.visibility = presenterImpl.showCellIfUserHasPermissions(attendanceChildAdapterModel)

        return view
    }

    companion object {
        const val workerAttendanceChildModel = "workerAttendanceChildModel"
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun signOnSuccessful() {
        DarkToast.makeText(activity,
                "Successfully signed " + attendanceChildAdapterModel.workerName + " On")
    }

    override fun signOffSuccessful() {
        DarkToast.makeText(activity,
                "Successfully signed " + attendanceChildAdapterModel.workerName + " Off")
    }

    override fun signOnError() {
        DarkToast.makeText(activity,
                "Error signed " + attendanceChildAdapterModel.workerName + " On")
    }

    override fun signOffError() {
        DarkToast.makeText(activity,
                "Error signed " + attendanceChildAdapterModel.workerName + " Off")
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val viewAllUserItem = menu.findItem(R.id.view_all_users)
        val addAttendeeItem = menu.findItem(R.id.add_attendee)
        viewAllUserItem.isVisible = false
        addAttendeeItem.isVisible = false
    }
}