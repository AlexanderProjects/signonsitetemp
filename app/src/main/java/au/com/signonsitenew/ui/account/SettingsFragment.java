package au.com.signonsitenew.ui.account;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import dagger.android.AndroidInjection;

/**
 *  Copyright © 2018 SignOnSite. All rights reserved.
 */
public class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = SettingsFragment.class.getSimpleName();

    @Inject
    LocationManager mLocationManager;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            ((SettingsActivity)getActivity()).mTitleTextView.setText("App Settings");
        }

        // Load the preferences
        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case Constants.SETTINGS_AUTO_SIGNON:
                SLog.i(TAG, "Auto sign on preference toggled");
                mLocationManager.refresh();
                break;
        }

        // Send post updated settings when the user toggles it
        new SOSAPI(getActivity()).postPhoneAndAppInfo(null);
    }
}
