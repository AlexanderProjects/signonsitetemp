package au.com.signonsitenew.ui.prelogin.registration.complete

interface CompleteRegistrationDisplay {
    fun showNetworkErrors()
    fun showPassportButton(value: Boolean)
}