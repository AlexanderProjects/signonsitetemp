package au.com.signonsitenew.ui.documents.permits.template.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.ItemPermitEmptyTemplateBinding
import au.com.signonsitenew.databinding.ItemPermitTemplateBinding
import au.com.signonsitenew.domain.models.SitePermitTemplate

class TemplatePermitRecyclerViewAdapter(private val permitTemplates:List<SitePermitTemplate>, val onClick: (sitePermitTemplate: SitePermitTemplate) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private val populatedState:Int = 2
    private val emptyState:Int = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       return  if(viewType == populatedState) {
           PermitTemplateViewHolder(ItemPermitTemplateBinding.inflate(LayoutInflater.from(parent.context), parent, false))
       } else {
           PermitEmptyTemplateViewHolder(ItemPermitEmptyTemplateBinding.inflate(LayoutInflater.from(parent.context), parent, false))
       }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder.itemViewType == populatedState) {
            holder as PermitTemplateViewHolder
            holder.binding.permitTemplateName.text = permitTemplates[position].permit_template.name
            holder.binding.root.setOnClickListener { onClick(permitTemplates[position]) }
        }else {
            holder as PermitEmptyTemplateViewHolder
            holder.binding.permitEmptyTemplateName.text = holder.itemView.context.getString(R.string.permit_template_empty_message)
        }
    }

    override fun getItemCount(): Int {
        return if(permitTemplates.isEmpty())
            emptyState
        else
            permitTemplates.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(permitTemplates.isNotEmpty())
            populatedState
        else
            emptyState
    }
    inner class PermitTemplateViewHolder(val binding:ItemPermitTemplateBinding) : RecyclerView.ViewHolder(binding.root)
    inner class PermitEmptyTemplateViewHolder(val binding: ItemPermitEmptyTemplateBinding) : RecyclerView.ViewHolder(binding.root)
}