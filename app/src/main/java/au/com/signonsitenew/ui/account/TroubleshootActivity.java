package au.com.signonsitenew.ui.account;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import org.json.JSONException;
import org.json.JSONObject;
import javax.inject.Inject;
import au.com.signonsitenew.R;
import au.com.signonsitenew.api.SOSAPI;
import au.com.signonsitenew.ui.navigation.Router;
import au.com.signonsitenew.utilities.NotificationUtil;
import au.com.signonsitenew.utilities.PermissionCheckers;
import au.com.signonsitenew.utilities.PermissionUtil;
import au.com.signonsitenew.utilities.SLog;
import au.com.signonsitenew.utilities.SessionManager;
import au.com.signonsitenew.utilities.StatusCheckers;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Activity to display details about the app such as the version number and to inform the user if
 * an update is available.
 */
public class TroubleshootActivity extends DaggerAppCompatActivity {

    public static final String LOG = TroubleshootActivity.class.getSimpleName();

    @Inject
    Router router;


    private SOSAPI mAPI;
    private int mAppBuildNumber;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    protected static final int REQUEST_LOCATION = 1;
    protected TextView mAppVersionTextView;
    protected TextView mOSVersionTextView;
    protected TextView mDeviceTypeTextView;
    protected TextView mLocationEnabledTextView;
    protected TextView mAutoSignOnEnabledTextView;
    protected TextView mLocationPermissionTextView;
    protected TextView mWifiEnabledTextView;
    protected TextView mIgnoringBatteryOptsTextView;
    protected TextView mNotificationsAllowedTextView;
    private LinearLayout locationPermissionRow;
    protected LinearLayout mUpdateAppCell;
    private static String[] PERMISSIONS_LOCATION =
            {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            };
    private static String[] PERMISSIONS_LOCATION_ANDROID_TEN =
            {
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troubleshoot);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);

        mAppVersionTextView = findViewById(R.id.app_version_text_view);
        mOSVersionTextView = findViewById(R.id.operating_system_version_text_view);
        mDeviceTypeTextView = findViewById(R.id.device_type_text_view);
        mLocationEnabledTextView = findViewById(R.id.location_services_enabled_text_view);
        mAutoSignOnEnabledTextView = findViewById(R.id.auto_sign_on_enabled_text_view);
        mLocationPermissionTextView = findViewById(R.id.location_permissions_granted_text_view);
        mWifiEnabledTextView = findViewById(R.id.wifi_enabled_text_view);
        mIgnoringBatteryOptsTextView = findViewById(R.id.battery_optimisations_ignored_text_view);
        mNotificationsAllowedTextView = findViewById(R.id.notifications_allowed_text_view);
        locationPermissionRow = findViewById(R.id.location_permission_row);
        mUpdateAppCell = findViewById(R.id.menu_update_app_item);
        mUpdateAppCell.setOnClickListener(v -> startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()))));

        mAPI = new SOSAPI(this);

        setInfoStrings();

        // Determine if app is up-to-date
        PackageInfo pInfo = null;
        mAppBuildNumber = 99999;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e) {
            SLog.e(LOG, e.getMessage());
        }
        if (pInfo != null) {
            mAppBuildNumber = pInfo.versionCode;
        }

        mAPI.postPhoneAndAppInfo(response -> {
            JSONObject result;
            try {
                result = new JSONObject(response);
                int productionBuild = result.getInt("current_version_code_android");
                if (mAppBuildNumber < productionBuild) {
                    mAppVersionTextView.setTextColor(getResources().getColor(R.color.red_highlight));

                    // Create an AlertDialog to the Play Store
                    //Todo change this function. There is ticket in jira to fix this issue SOS-920
                    //createAppUpdateAlertDialog();

                    mUpdateAppCell.setVisibility(View.VISIBLE);
                }
                else {
                    mAppVersionTextView.setTextColor(getResources().getColor(R.color.green_highlight));
                }
            }
            catch (JSONException e) {
                SLog.e(LOG, "A JSON Exception Occurred");
            }
        });
        locationPermissionRow.setOnClickListener(view -> router.navigateToAppSettings(this));
        if(PermissionUtil.hasLocationPermissions(this))
            requestLocationPermission();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               finish();
                break;
        }
        return true;
    }

    /**
     * Sets and displays the Strings in the list view
     */
    private void setInfoStrings() {
        String appVersion;
        String androidOSVersion;
        String device;
        String locationStatus;
        String wifiStatus;
        String autoSignOn;
        String locPermissionsGranted;
        String batteryOptimisationsIgnored;
        String notificationsStatus;

        // App Version
        PackageInfo pInfo = null;
        String versionName = "";
        try {
            pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e) {
            SLog.e(LOG, e.getMessage());
        }
        if (pInfo != null) {
            versionName = pInfo.versionName;
        }
        appVersion = versionName;
        mAppVersionTextView.setText(appVersion);

        // Android Info
        androidOSVersion = Build.VERSION.RELEASE;
        mOSVersionTextView.setText(androidOSVersion);

        // Device Info
        device = Build.BRAND.substring(0, 1).toUpperCase() + Build.BRAND.substring(1) + " " + Build.MODEL;
        mDeviceTypeTextView.setText(device);

        // Location Enabled
        boolean locationEnabled = StatusCheckers.gpsEnabled(this);
        if (locationEnabled) {
            locationStatus = "On";
            mLocationEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
        }
        else {
            locationStatus = "Off";
            mLocationEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
        }
        mLocationEnabledTextView.setText(locationStatus);

        boolean wifiEnabled = StatusCheckers.wifiEnabled(this);
        if (wifiEnabled) {
            wifiStatus = "On";
            mWifiEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
        }
        else {
            wifiStatus = "Off";
            mWifiEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
        }
        mWifiEnabledTextView.setText(wifiStatus);

        // Auto Sign On
        boolean autoSignOnEnabled = new SessionManager(this).getAutoSignOnEnabled();
        if (autoSignOnEnabled) {
            autoSignOn = "On";
            mAutoSignOnEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
        }
        else {
            autoSignOn = "Off";
            mAutoSignOnEnabledTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
        }
        mAutoSignOnEnabledTextView.setText(autoSignOn);
        updatePermissionLayouts();

        // Ignoring Battery Optimisations
        boolean ignoringBatteryOpts = PermissionCheckers.ignoreBattOptsGranted(this);
        if (ignoringBatteryOpts) {
            batteryOptimisationsIgnored = "Granted";
            mIgnoringBatteryOptsTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
        }
        else {
            batteryOptimisationsIgnored = "Denied";
            mIgnoringBatteryOptsTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
        }
        mIgnoringBatteryOptsTextView.setText(batteryOptimisationsIgnored);

        // Notifications Allowed
        boolean notificationsEnabled = NotificationUtil.appNotificationsEnabled(this);
        if (notificationsEnabled) {
            notificationsStatus = "On";
            mNotificationsAllowedTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
        }
        else {
            notificationsStatus = "Off";
            mNotificationsAllowedTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
        }
        mNotificationsAllowedTextView.setText(notificationsStatus);
    }

    private void createAppUpdateAlertDialog() {

        final String message = "It appears your app is out of date. We highly recommend " +
                "updating to the newest version.";

        builder = new AlertDialog.Builder(TroubleshootActivity.this);
        dialog = builder.create();

        builder.setMessage(message)
                .setTitle("Update your app")
                .setPositiveButton("Update",
                        (d, id) -> {
                            SLog.i(LOG, "User pressed update when prompted to update");
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                            }
                            catch (ActivityNotFoundException e) {
                                SLog.e(LOG, "Activity not found exception: " + e.getMessage());
                                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                            }
                            d.dismiss();
                        })
                .setNegativeButton("Cancel",
                        (d, id) -> {
                            SLog.i(LOG, "User pressed cancel when prompted to update");
                            d.cancel();
                        });
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(dialogInterface -> {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.orange_primary));
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.orange_primary));
        });

        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(dialog != null) dialog.dismiss();
    }

    private void requestLocationPermission() {
        SLog.i(LOG, "User accepted initial dialog. Requesting LOCATION SERVICES permission.");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION_ANDROID_TEN, REQUEST_LOCATION);
        }else {
            // Location permissions not granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            updatePermissionLayouts();
        }
    }

    private void updatePermissionLayouts(){
        String locPermissionsGranted;
        boolean locationPermissionGranted = PermissionCheckers.locationPermissionGranted(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (locationPermissionGranted && PermissionCheckers.isLocationPermissionGrantedForAndroidTen(this)) {
                locPermissionsGranted = "Always";
                mLocationPermissionTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
            }else if(!PermissionCheckers.isLocationPermissionGrantedForAndroidTen(this) && locationPermissionGranted){
                locPermissionsGranted = "When using";
                mLocationPermissionTextView.setTextColor(ContextCompat.getColor(this, R.color.orange_primary));
            }
            else {
                locPermissionsGranted = "Never";
                mLocationPermissionTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
            }
        }else{
            if (locationPermissionGranted) {
                locPermissionsGranted = "On";
                mLocationPermissionTextView.setTextColor(ContextCompat.getColor(this, R.color.green_highlight));
            }else{
                locPermissionsGranted = "Off";
                mLocationPermissionTextView.setTextColor(ContextCompat.getColor(this, R.color.red_highlight));
            }
        }
        mLocationPermissionTextView.setText(locPermissionsGranted);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
