package au.com.signonsitenew.ui.documents.briefingslist

import android.app.ActionBar
import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.models.BriefingsResponse
import au.com.signonsitenew.ui.adapters.BriefingListViewPagerAdapter
import au.com.signonsitenew.utilities.DarkToast
import au.com.signonsitenew.utilities.SessionManager
import com.google.android.material.tabs.TabLayout
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class BriefingListTabbedFragment : DaggerFragment(), BriefingListTabbedView {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var searchMenuItem: MenuItem
    private lateinit var viewPager: ViewPager
    private lateinit var searchView: SearchView
    private lateinit var tabLayout: TabLayout
    private lateinit var session: SessionManager
    private lateinit var briefingListViewPagerAdapter: BriefingListViewPagerAdapter
    private lateinit var presenter: BriefingTabFragmentListPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        session = SessionManager(activity)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = ViewModelProvider(this, viewModelFactory).get(BriefingTabFragmentListPresenter::class.java)
        presenter.inject(this)
        presenter.observeStates()
        presenter.retrieveListOfBriefings()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_briefinglist_tab, container, false)
        briefingListViewPagerAdapter = BriefingListViewPagerAdapter(childFragmentManager)
        viewPager = view.findViewById(R.id.briefings_viewpager)
        viewPager.adapter = briefingListViewPagerAdapter
        tabLayout = view.findViewById(R.id.briefings_tabLayout)
        tabLayout.setupWithViewPager(viewPager)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menusearch, menu)
        searchMenuItem = menu.findItem(R.id.action_search)
        searchView = searchMenuItem.actionView as SearchView
        searchView.layoutParams = ActionBar.LayoutParams(Gravity.RIGHT)
        val searchManager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    searchMenuItem.collapseActionView()
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    //TODO:Fixme - Add search implementation
                    return true
                }
            })
        }
        searchMenuItem.expandActionView()
        searchMenuItem.actionView = searchView
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clearOffset()
    }

    override fun updateData(response: BriefingsResponse) {
        briefingDateDataListener.updateData(response.data)
        briefingContentDataListener.updateData(response.data)
    }

    override fun showError(error: String) {
        DarkToast.makeText(activity, error)
    }

    companion object {
        private lateinit var briefingDateDataListener: BriefingDataListener
        private lateinit var briefingContentDataListener: BriefingDataListener
        fun setDateDataListener(dataListener: BriefingDataListener) {
            briefingDateDataListener = dataListener
        }

        fun setContentDataListener(dataListener: BriefingDataListener) {
            briefingContentDataListener = dataListener
        }
    }
}