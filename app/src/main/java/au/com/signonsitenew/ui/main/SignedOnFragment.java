package au.com.signonsitenew.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import java.util.HashMap;
import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.EnrolledSite;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Controls or logic to display information when a user is signed onto site.
 *
 * If the site cannot be found in the region list when this screen is opened the user will be
 * forcefully signed off. The history behind such a crude implementation is that one of SignOnSite's
 * first customers was the Asbestos Response Taskforce (ART), which had 1,137 sites in Canberra
 * alone. Workers were working across many of these, so there were instances of individuals being
 * enrolled with over 100 sites. iOS supported a maximum of 20 regions being monitored at any one
 * time, and we initially kept Android in line with these requirements (even though Android can
 * support monitoring up to 100 regions). Since the workers for these projects were susceptible to
 * not signing off the sites since they had cheaper phones, that logic was put in place as an
 * additional mechanism to sign users off if they were sufficiently far away from their work site.
 */
public class SignedOnFragment extends Fragment implements OnMapReadyCallback {

    private SessionManager mSession;
    private RealmConfiguration mRealmConfig;
    private GoogleMap mMap;

    private SupportMapFragment mSiteMap;
    private ImageView mMapView;
    private TextView mNameView;
    private TextView mSiteNameView;
    private TextView mAddressView;
    private TextView mOwnerView;
    private TextView mTimeView;
    private TextView mDateView;

    private OnButtonInteractionListener mListener;

    public SignedOnFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSession = new SessionManager(requireActivity());
        mRealmConfig = RealmManager.getRealmConfiguration();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_signed_on, container, false);

        mSiteMap = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.site_map_fragment);

        mMapView = rootView.findViewById(R.id.map_snapshot_image_view);
        mNameView = rootView.findViewById(R.id.signed_on_name_text_view);
        mSiteNameView = rootView.findViewById(R.id.signed_on_site_name_text_view);
        mAddressView = rootView.findViewById(R.id.signed_on_address_text_view);
        mOwnerView = rootView.findViewById(R.id.signed_on_owner_name_text_view);
        mTimeView = rootView.findViewById(R.id.signed_on_time_text_view);
        mDateView = rootView.findViewById(R.id.signed_on_date_text_view);
        Button mSignOffButton = rootView.findViewById(R.id.sign_off_button);

        mSiteMap.getMapAsync(this);

        mSignOffButton.setOnClickListener(v -> {
            Log.i("sign off button clicked", String.valueOf(v));
            onSignOffButtonPressed();
        });
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        if (mSession.getSiteId() == 0) {
            // We are not signed on, navigate to MainActivity
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        else {
            // Get user and site and sign on details
            HashMap<String, String> user = mSession.getCurrentUser();
            String name = user.get(Constants.USER_FIRST_NAME) + " " + user.get(Constants.USER_LAST_NAME);
            int siteId = mSession.getSiteId();

            Realm realm = Realm.getInstance(mRealmConfig);
            EnrolledSite site = realm.where(EnrolledSite.class).equalTo("id", siteId).findFirst();
            if (site == null) {
                ((SignedOnActivity) requireActivity()).signOffSite(null);
                return;
            }
            String siteName = site.getName();
            String siteAddress = site.getSiteAddress();
            String siteOwner = site.getManagerName();
            realm.close();

            HashMap<String, String> timeDetails = mSession.getSignOnTime();
            String timeString = timeDetails.get(Constants.SIGN_ON_TIME);
            String date = timeDetails.get(Constants.SIGN_ON_DATE);

            // Set details
            mNameView.setText(name + ", you are currently signed on to");
            mSiteNameView.setText(siteName);
            mAddressView.setText(siteOwner);
            mOwnerView.setText("Managed by " + siteAddress);
            mTimeView.setText(timeString);
            mDateView.setText(date);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(mSiteMap);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Find Site Centre
        int siteId = mSession.getSiteId();
        Realm realm = Realm.getInstance(mRealmConfig);
        EnrolledSite site = realm.where(EnrolledSite.class).equalTo("id", siteId).findFirst();
        LatLng siteCentroid;
        if (site != null) {
            siteCentroid = new LatLng(site.getRegionLat(), site.getRegionLong());
        }
        else {
            siteCentroid = new LatLng(0,0);
        }
        realm.close();

        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(siteCentroid, 18));
        mMap.setOnMapLoadedCallback(() -> mMap.snapshot(bitmap -> {
            mMapView.setImageBitmap(bitmap);
            applyBlurToMapImage();
        }));
    }

    private void applyBlurToMapImage() {
        mMapView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mMapView.getViewTreeObserver().removeOnPreDrawListener(this);
                mMapView.buildDrawingCache();

                Bitmap bitmap = mMapView.getDrawingCache();
                blurBitmap(bitmap, mMapView);
                return true;
            }
        });
    }


    private void blurBitmap(Bitmap image, ImageView view) {
        float radius = 2;
        float scaleFactor = 4;

        Bitmap overlay = Bitmap.createBitmap((int) (view.getMeasuredWidth()/scaleFactor),
                (int) (view.getMeasuredHeight()/scaleFactor), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(overlay);
        canvas.scale(1 / scaleFactor, 1 / scaleFactor);
        Paint paint = new Paint();
        paint.setFlags(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(image, 0, 0, paint);

        RenderScript renderScript = RenderScript.create(getActivity());

        Allocation overlayAllocation = Allocation.createFromBitmap(renderScript, overlay);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript, overlayAllocation.getElement());
        blur.setInput(overlayAllocation);
        blur.setRadius(radius);
        blur.forEach(overlayAllocation);

        overlayAllocation.copyTo(overlay);

        view.setImageBitmap(overlay);

        renderScript.destroy();
    }

    private void onSignOffButtonPressed() {
        if (mListener != null) {
            int siteId = mSession.getSiteId();
            mListener.onSignOffPressed(siteId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnButtonInteractionListener) {
            mListener = (OnButtonInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInductionOptionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnButtonInteractionListener {
        void onSignOffPressed(Integer siteId);
    }
}
