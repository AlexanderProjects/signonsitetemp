package au.com.signonsitenew.ui.documents.permits.details

import android.app.Activity
import android.graphics.Rect
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import au.com.signonsitenew.R
import au.com.signonsitenew.databinding.FragmentPermitDetailsBinding
import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.ui.documents.permits.details.adapters.PermitDetailsViewPagerAdapter
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMembersFragment
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty
import com.google.android.material.tabs.TabLayoutMediator
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class PermitDetailsFragment : DaggerFragment(), PermitDetailsDisplay {

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var permit: Permit? = null
    private var permitInfo: PermitInfo? = null
    private var _binding: FragmentPermitDetailsBinding? = null
    private val binding get() = _binding!!
    private val presenter: PermitDetailsFragmentPresenterImpl by viewModels { viewModelFactory }
    lateinit var adapter: FragmentStateAdapter
    private val isKeyboardVisible: () -> Boolean
        get() = {
            val visibleBounds = Rect()
            binding.root.getWindowVisibleDisplayFrame(visibleBounds)
            val heightDiff = binding.root.height - visibleBounds.height()
            heightDiff > 0
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.inject(this)
        presenter.observeStates()
        presenter.observeCtaState()
        presenter.observeSaveButtonStates()
        presenter.clearPermitResponses()
        presenter.observeForUnSavedResponses()
        presenter.observeTeamMembersTab()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentPermitDetailsBinding.inflate(inflater, container, false)
        binding.permitDetailsToolbar.title = String().empty()
        arguments?.let { it ->
            permit = it.getParcelable(Constants.PERMIT_OBJECT)
            permit?.let { permit ->
                presenter.setPermit(permit)
                presenter.getFullPermitInfoAsync()
            }
            permitInfo = it.getParcelable(Constants.PERMIT_INFO_OBJECT)
            permitInfo?.let { permitInfo ->
                presenter.setPermitDetailsState(permitInfo)
            }
        }
        (requireActivity() as DaggerAppCompatActivity).setSupportActionBar(binding.permitDetailsToolbar)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        (requireActivity() as DaggerAppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.bottomCustomBar.permitActionBarCancelButton.setOnClickListener { presenter.launchCancelAlertDialog() }
        binding.bottomCustomBar.permitActionBarSaveButton.setOnClickListener { presenter.saveUserResponses() }

        return binding.root
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                when (requireActivity().supportFragmentManager.findFragmentById(R.id.doc_permits_container)) {
                    is AddTeamMembersFragment -> {
                        hideKeyboard()
                        router.navigateToPermitDetails(requireActivity())
                    }
                    is PermitDetailsFragment -> {
                        if (presenter.hasUserUnsavedResponses()) {
                            AlertDialogMessageHelper.showUnSavedPermitInfoInPermits(requireContext()){
                                router.navigateToCurrentPermits(requireActivity())
                            }
                        }else{
                            router.navigateToCurrentPermits(requireActivity())
                        }
                    }
                }
            }
        }
        return true
    }

    override fun showPermitError() {
        AlertDialogMessageHelper.permitErrorMessage(requireContext()) { permit?.let { presenter.getFullPermitInfoAsync() } }
    }

    override fun setPermitDetailsViewPagerAdapter(permit: Permit?, permitInfo: PermitInfo?) {
        if (!::adapter.isInitialized) {
            adapter = PermitDetailsViewPagerAdapter(requireActivity(), permit, permitInfo)
            binding.permitDetailsViewPager.adapter = adapter
            TabLayoutMediator(
                    binding.permitTabLayout,
                    binding.permitDetailsViewPager
            ) { tab, position ->
                when (position) {
                    0 -> tab.text = resources.getString(R.string.permit_team_tab)
                    1 -> tab.text = resources.getString(R.string.permit_task_tab)
                    2 -> tab.text = resources.getString(R.string.permit_checks_tab)
                }
            }.attach()
        }
    }

    override fun setCtaStateForNewPermit(permitInfo: PermitInfo?) {
        router.callCtaContextualButtonFragment(requireActivity(), permitInfo)
    }

    override fun setCtaStateForExitingPermit(permitInfo: PermitInfo?) {
        router.callCtaContextualButtonFragment(requireActivity(), permitInfo)
    }

    override fun showPermitToolbar() {
        binding.contextualPermitToolbar.visibility = View.VISIBLE
    }

    override fun hidePermitToolbar() {
        binding.contextualPermitToolbar.visibility = View.GONE
    }

    override fun navigateToCurrentPermits() {
        router.navigateToCurrentPermits(requireActivity())
    }

    override fun showCancelAlertDialogForRequestState() {
        AlertDialogMessageHelper.cancelPermitInRequestMessage(requireContext()) {
            presenter.updateAnExistingPermit(Constants.PERMIT_REQUESTER_CANCELLED)
        }
    }

    override fun showCancelAlertDialogForInProgressState() {
        AlertDialogMessageHelper.cancelPermitInProgressMessage(requireContext()) {
            presenter.updateAnExistingPermit(Constants.PERMIT_APPROVER_CANCELLED)
        }
    }

    override fun showSaveButton() {
        if (!binding.bottomCustomBar.permitActionBarSaveButton.isVisible)
        binding.bottomCustomBar.permitActionBarSaveButton.visibility = View.VISIBLE
    }

    override fun hideSaveButton() {
        if (binding.bottomCustomBar.permitActionBarSaveButton.isVisible)
        binding.bottomCustomBar.permitActionBarSaveButton.visibility = View.GONE
    }

    override fun showProgressView() {
        router.showProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun hideProgressView() {
        router.removeProgressView(requireActivity(),R.id.doc_permits_container)
    }

    override fun setPermitStateTitle(stateTitle: String) {
        binding.permitTypeTitle.text = stateTitle
    }

    override fun setPermitHumanId(humanId: String) {
        binding.permitHumanId.text = humanId
    }

    override fun hidePermitHumanId() {
        binding.permitHumanId.visibility = View.INVISIBLE
    }

    override fun showPermitHumanId() {
        binding.permitHumanId.visibility = View.VISIBLE
    }

    override fun setSmallPermitDescription(description: String) {
        binding.permitDescription.text = description
    }

    override fun showErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE) {
            permit?.let { presenter.getFullPermitInfoAsync() }
        }
    }

    private fun hideKeyboard() {
        if (isKeyboardVisible()) {
            val inputMethodManager: InputMethodManager = requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
        }
    }
}
