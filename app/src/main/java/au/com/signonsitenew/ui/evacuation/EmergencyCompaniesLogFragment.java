package au.com.signonsitenew.ui.evacuation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.EmergencyChecklistAdapter;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.SiteAttendee;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class EmergencyCompaniesLogFragment extends Fragment {

    protected ExpandableListView mAttendanceListView;
    protected EmergencyChecklistAdapter mAdapter;
    protected List<String> mListTitles;
    protected HashMap<String, List<SiteAttendee>> mAttendanceList;
    protected Realm mRealm;

    public EmergencyCompaniesLogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_emergency_site_log_companies, container, false);
        mRealm = Realm.getInstance(RealmManager.getRealmConfiguration());

        mAttendanceListView = rootView.findViewById(R.id.expandable_list_view);
        mAttendanceList = new HashMap<>();
        mAttendanceListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            // Tick off the image view
            SiteAttendee visitor = (SiteAttendee) mAdapter.getChild(groupPosition, childPosition);
            boolean status = visitor.isMarkedSafe();
            mRealm.beginTransaction();
            SiteAttendee realmVisitor = mRealm.where(SiteAttendee.class).equalTo("userId", visitor.getUserId()).findFirst();
            realmVisitor.setMarkedSafe(!status);
            mRealm.commitTransaction();

            // Set the adapter data again
            setAdapterData();

            return false;
        });

        // Set Visitor data
        setAdapterData();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRealm != null) {
            mRealm.close();
        }
    }

    protected void setAdapterData() {
        RealmResults<SiteAttendee> visitors = mRealm.where(SiteAttendee.class).findAll();
        int totalAttendance = visitors.size();
        int totalSafe = mRealm.where(SiteAttendee.class).equalTo("markedSafe", true).findAll().size();

        ((EmergencyProgressActivity)getActivity())
                .mCheckedCountTextView
                .setText(totalSafe + " / " + totalAttendance);

        // Create Company Groups
        List<String> companyNames = new ArrayList<>();
        for (SiteAttendee visitor : visitors) {
            if (!companyNames.contains(visitor.getCompany())) {
                companyNames.add(visitor.getCompany());
            }
        }

        for (String name : companyNames) {
            RealmResults<SiteAttendee> usersInCompany = mRealm.where(SiteAttendee.class)
                                                            .equalTo("company", name)
                                                            .findAll();
            // Put users that are not marked safe at the top
            usersInCompany.sort("markedSafe", Sort.ASCENDING);
            mAttendanceList.put(name, usersInCompany);
        }

        mListTitles = new ArrayList<>(mAttendanceList.keySet());

        if (mAdapter == null) {
            mAdapter = new EmergencyChecklistAdapter(getActivity(), mListTitles, mAttendanceList);
            mAttendanceListView.setAdapter(mAdapter);
        }
        else {
            ((EmergencyChecklistAdapter)mAttendanceListView.getExpandableListAdapter()).refill(mListTitles, mAttendanceList);
        }

        int groupCount = mAdapter.getGroupCount();
        for (int i = 0; i < groupCount; i++){
            mAttendanceListView.expandGroup(i);
        }
    }

}
