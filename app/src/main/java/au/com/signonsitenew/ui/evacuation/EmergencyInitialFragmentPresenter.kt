package au.com.signonsitenew.ui.evacuation

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import au.com.signonsitenew.domain.models.state.EmergencyInitialFragmentState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.evacuation.EvacuationUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.addOnPropertyChanged
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface EmergencyInitialFragmentPresenter {
    fun startEvacuation(actualEmergency:Boolean)
    fun observeState()
    fun inject(emergencyInitialDisplay: EmergencyInitialDisplay)
    fun visitorEvacuation()
}

class EmergencyInitialFragmentPresenterImpl @Inject constructor(private val evacuationUseCase: EvacuationUseCase,
                                                                private val logger:Logger,
                                                                private val analyticsEventDelegateService: AnalyticsEventDelegateService) : ViewModel(), EmergencyInitialFragmentPresenter {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private val uiState = ObservableField<EmergencyInitialFragmentState>()
    private lateinit var emergencyInitialDisplay: EmergencyInitialDisplay

    override fun startEvacuation(actualEmergency: Boolean) {
        disposables.add(evacuationUseCase.starEvacuation(actualEmergency)
                .subscribe({
                    if(it.status == Constants.JSON_SUCCESS || it.status == Constants.JSON_IN_PROGRESS)
                        uiState.set(EmergencyInitialFragmentState.GetEvacuationVisitor)
                    else {
                        uiState.set(EmergencyInitialFragmentState.TriggerError(it))
                        logger.w(EmergencyInitialFragmentPresenterImpl::class.java.name,attributes = mapOf(this::startEvacuation.name to toJson(it)))
                    }
                },{
                    uiState.set(EmergencyInitialFragmentState.Error(it))
                    logger.e(EmergencyInitialFragmentPresenterImpl::class.java.name ,attributes = mapOf(this::startEvacuation.name to it.message))
                }))
        if(actualEmergency)
            analyticsEventDelegateService.managerEvacuationEmergencyTriggered()
        else
            analyticsEventDelegateService.managerEvacuationDrillTriggered()

    }

    override fun observeState() {
        disposables.add(uiState.addOnPropertyChanged {
            when(it.get()){
                is EmergencyInitialFragmentState.GetEvacuationVisitor -> emergencyInitialDisplay.getEvacuationVisitor()
                is EmergencyInitialFragmentState.NavigateToEmergencyProgress -> emergencyInitialDisplay.navigateToEmergencyProgressActivity()
                is EmergencyInitialFragmentState.Error -> emergencyInitialDisplay.showEmergencyError((it.get() as EmergencyInitialFragmentState.Error).error)
                is EmergencyInitialFragmentState.TriggerError -> emergencyInitialDisplay.showEmergencyTriggerError((it.get() as EmergencyInitialFragmentState.TriggerError).apiResponse)
            }
        })
    }

    override fun inject(emergencyInitialDisplay: EmergencyInitialDisplay) {
        this.emergencyInitialDisplay = emergencyInitialDisplay
    }

    override fun visitorEvacuation() {
        disposables.add(evacuationUseCase.visitorEvacuation()
                .subscribe({
                    if(it.status == Constants.JSON_SUCCESS) {
                        uiState.set(EmergencyInitialFragmentState.NavigateToEmergencyProgress)
                        evacuationUseCase.saveEvacuationVisitors(it.visits)
                    }else{
                        logger.i(EmergencyInitialFragmentPresenterImpl::class.java.name,attributes = mapOf(this::visitorEvacuation.name to toJson(it)))
                    }
                },{
                    uiState.set(EmergencyInitialFragmentState.Error(it))
                    logger.e(EmergencyInitialFragmentPresenterImpl::class.java.name, attributes = mapOf(this::visitorEvacuation.name to it.message))
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}