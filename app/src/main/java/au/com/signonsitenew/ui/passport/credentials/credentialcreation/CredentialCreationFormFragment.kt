package au.com.signonsitenew.ui.passport.credentials.credentialcreation

import android.app.Activity
import android.content.Intent
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.CredentialType
import au.com.signonsitenew.domain.utilities.Builder
import au.com.signonsitenew.domain.utilities.CredentialTypeValidator
import au.com.signonsitenew.domain.utilities.Mapper
import au.com.signonsitenew.ui.adapters.CreateCredentialAdapter
import au.com.signonsitenew.utilities.*
import com.bumptech.glide.Glide
import dagger.android.support.DaggerFragment
import java.io.File
import java.io.IOException
import javax.inject.Inject

class CredentialCreationFormFragment : DaggerFragment(), CredentialCreationView {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var sessionManager: SessionManager

    private lateinit var presenterImpl: CredentialCreationPresenterImpl
    private lateinit var credentialType: CredentialType
    private lateinit var credentialCreationToolbar: Toolbar
    private lateinit var createCredentialFieldsList: RecyclerView
    private lateinit var adapter: CreateCredentialAdapter
    private lateinit var frontPhotoImageView: ImageView
    private lateinit var backPhotoImageView: ImageView
    private lateinit var frontPhotoTextView: TextView
    private lateinit var backPhotoTextView: TextView
    private lateinit var createCredentialSaveButton: Button
    private lateinit var savingInfoProgressBar:ProgressBar
    private var frontPhotoFile: File? = null
    private var backPhotoFile: File? = null
    private lateinit var frontPhotoStatus: TextView
    private lateinit var backPhotoStatus: TextView
    private var isFrontPhoto = false
    private lateinit var photoUri:Uri

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.credential_creation_form, container, false)
        credentialType = requireArguments().getParcelable(Constants.CREDENTIAL_CREATION_FLAG)!!
        CredentialTypeValidator.setCredentialType(credentialType)

        createCredentialFieldsList = view.findViewById(R.id.create_credential_fields_list)
        frontPhotoImageView = view.findViewById(R.id.credential_creation_form_front_photo_imageView)
        backPhotoImageView = view.findViewById(R.id.credential_creation_form_back_photo_imageView)
        frontPhotoTextView = view.findViewById(R.id.front_photo_text_view)
        backPhotoTextView = view.findViewById(R.id.back_photo_text_view)
        frontPhotoStatus = view.findViewById(R.id.status_front_photo)
        backPhotoStatus = view.findViewById(R.id.status_back_photo)
        savingInfoProgressBar = view.findViewById(R.id.savingInfoProgressBar)

        createCredentialSaveButton = view.findViewById(R.id.creation_credential_from_save_button)
        credentialCreationToolbar = view.findViewById(R.id.credential_creation_form_toolbar)
        credentialCreationToolbar.title = credentialType.name
        (activity as AppCompatActivity?)!!.setSupportActionBar(credentialCreationToolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        createCredentialFieldsList.addItemDecoration(DividerItemDecoration(createCredentialFieldsList.context, DividerItemDecoration.VERTICAL))

        adapter = CreateCredentialAdapter(Builder.buildFieldsFromCredentialType(credentialType), credentialType)
        createCredentialFieldsList.adapter = adapter
        credentialType.front_photo.let {
            if(it.equals(Constants.NONE_FIELD, true) || it == null) {
                frontPhotoImageView.visibility = View.GONE
                frontPhotoTextView.visibility = View.GONE
                frontPhotoStatus.visibility = View.GONE
            }
        }
        credentialType.back_photo.let {
            if(it.equals(Constants.NONE_FIELD, true) || it == null) {
                backPhotoImageView.visibility = View.GONE
                backPhotoTextView.visibility = View.GONE
                backPhotoStatus.visibility = View.GONE
            }
        }
        frontPhotoImageView.setOnClickListener {
            PermissionManager.checkForCameraPermissions(activity) {
                startCamera(Constants.IMAGE_CAPTURE_FRONT_FLAG_CODE)
            }
            PermissionManager.checkForStoragePermissions(activity)
            isFrontPhoto = true
        }
        backPhotoImageView.setOnClickListener {
            PermissionManager.checkForCameraPermissions(activity) { startCamera(Constants.IMAGE_CAPTURE_BACK_FLAG_CODE) }
            PermissionManager.checkForStoragePermissions(activity)
            isFrontPhoto = false
        }
        if (credentialType.front_photo != null) frontPhotoStatus.text = "(" + " " + credentialType.front_photo + " " + ")"
        if (credentialType.back_photo != null) backPhotoStatus.text = "(" + " " + credentialType.back_photo + " " + ")"
        createCredentialSaveButton.setOnClickListener {
            savingInfoProgressBar.visibility = View.VISIBLE
            createCredentials()
        }
        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this, viewModelFactory).get(CredentialCreationPresenterImpl::class.java)
        presenterImpl.inject(this)
    }

    private fun startCamera(captureFlag: Int) {

        photoUri = ImageUtil.getOutputPhotoFileUri(requireContext(), credentialType.name?.deleteSpacesAndSlash(),isFrontPhoto)
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri)

        val chooserIntent = Intent.createChooser(galleryIntent, Constants.IMAGE_SOURCE_PICKER_LABEL)
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(cameraIntent, galleryIntent))
        startActivityForResult(chooserIntent, captureFlag)
    }

    private fun createCredentials() {
        val credentialCreateUpdateRequest = adapter.credentialFields?.let { Mapper.mapCredentialFieldToRequest(it, credentialType.identifier_name) }
        credentialCreateUpdateRequest?.credential_type_id = credentialType.id
        if (adapter.credentialFields?.let { CredentialTypeValidator.isValidForm(it, frontPhotoFile, backPhotoFile) }!!) {
            presenterImpl.setFrontPhoto(frontPhotoFile).setBackPhoto(backPhotoFile).setRequest(credentialCreateUpdateRequest!!)
            presenterImpl.createCredential()
        }
        else AlertDialogMessageHelper.messageValidationForm(activity)
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkValidationErrorMessage(activity, Constants.NETWORK_MESSAGE_ERROR, Constants.NETWORK_MESSAGE_TITLE)
    }

    override fun goBackToCredentials() {
        savingInfoProgressBar.visibility = View.GONE
        credentialsListLoader.refreshListOfCredentials()
        requireActivity().supportFragmentManager.popBackStackImmediate()
        navigateToPassport.navigate()
        sessionManager.editingBackPhoto = false
        sessionManager.editingFrontPhoto = false
    }

    override fun showDataError(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),errorMessage)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(menu.findItem(R.id.share_button) != null)
            menu.findItem(R.id.share_button).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // intent data will have photoUri in it if picked from gallery.
        photoUri = if (data?.data == null) photoUri else data.data!!

        when (requestCode) {
            1 -> if (resultCode == Activity.RESULT_OK) {
                frontPhotoTextView.visibility = View.GONE
                frontPhotoStatus.visibility = View.GONE
                if (CredentialTypeValidator.validateFrontPhoto() != null) {
                    frontPhotoFile = ImageUtil.convertUriToFile(requireContext(), photoUri)
                    Glide.with(requireActivity()).load(photoUri).into(frontPhotoImageView)
                    sessionManager.editingFrontPhoto = true
                }
            }
            2 -> if (resultCode == Activity.RESULT_OK) {
                backPhotoTextView.visibility = View.GONE
                backPhotoStatus.visibility = View.GONE
                if (CredentialTypeValidator.validateBackPhoto() != null) {
                    backPhotoFile = ImageUtil.convertUriToFile(requireContext(), photoUri)
                    Glide.with(requireActivity()).load(photoUri).into(backPhotoImageView)
                    sessionManager.editingBackPhoto = true
                }
            }
            else -> DarkToast.makeText(activity, Constants.CAPTURE_CAMERA_CANCELED)
        }
    }

    //This interface updates the list of credentials
    interface CredentialsListLoader {
        fun refreshListOfCredentials()
    }

    interface NavigateToPassport {
        fun navigate()
    }

    companion object {
        private lateinit var credentialsListLoader: CredentialsListLoader
        private lateinit var navigateToPassport: NavigateToPassport

        @JvmStatic
        fun newInstance(credentialType: CredentialType?): CredentialCreationFormFragment {
            val credentialCreationFromFragment = CredentialCreationFormFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constants.CREDENTIAL_CREATION_FLAG, credentialType)
            credentialCreationFromFragment.arguments = bundle
            return credentialCreationFromFragment
        }

        fun setCredentialsListLoader(credentialsListUpdate: CredentialsListLoader) {
            credentialsListLoader = credentialsListUpdate
        }

        fun setNavigateToPassport(navigateToPassport: NavigateToPassport) {
            Companion.navigateToPassport = navigateToPassport
        }
    }
}