package au.com.signonsitenew.ui.prelogin

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Layout
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.app.ActivityCompat

import org.json.JSONException
import org.json.JSONObject

import au.com.signonsitenew.R
import au.com.signonsitenew.SOSApplication
import au.com.signonsitenew.api.SOSAPI
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.utilities.*
import io.realm.Realm

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Activity to log into SignOnSite.
 */
class LoginActivity : Activity() {

    private lateinit var mSession: SessionManager
    private lateinit var mEmail: EditText
    private lateinit var mPassword: EditText
    private lateinit var mLoginButton: Button
    private lateinit var mForgotPasswordButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mSession = SessionManager(applicationContext)

        // Assign views
        mEmail = findViewById(R.id.email_edit_text)
        mPassword = findViewById(R.id.password_edit_text)
        mLoginButton = findViewById(R.id.login_button)
        mForgotPasswordButton = findViewById(R.id.forgot_password_button)
        val privacyPolicyTextView: TextView = findViewById(R.id.privacy_policy_text_view)

        mLoginButton.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                // Start progress spinner
                val dialog = ProgressDialog(this@LoginActivity)
                dialog.setMessage(getString(R.string.login_progress_dialog_text))
                dialog.isIndeterminate = true
                dialog.setCancelable(false)
                dialog.show()

                // Prepare variables
                val email = mEmail.text.toString().trim { it <= ' ' }
                val password = mPassword.text.toString().trim { it <= ' ' }

                if (email.isEmpty()) {
                    dialog.dismiss()
                    createAlertDialog(this@LoginActivity,
                            getString(R.string.error_generic_title), getString(R.string.error_blank_email))
                    return
                }
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    dialog.dismiss()
                    createAlertDialog(this@LoginActivity,
                            getString(R.string.error_generic_title), getString(R.string.error_invalid_email))
                    return
                }
                if (password.isEmpty()) {
                    dialog.dismiss()
                    createAlertDialog(this@LoginActivity,
                            getString(R.string.error_generic_title), getString(R.string.error_blank_password))
                    return
                }

                // Attempt login
                attemptLogin(dialog, email, password)
            }
        })

        mForgotPasswordButton.setOnClickListener {
            // TODO: Add ForgotPasswordActivity + Endpoint
            val url = Constants.URL_FORGOT_PASSWORD
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            startActivity(intent)
        }

        privacyPolicyTextView.setOnClickListener(object: DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                val url = Constants.URL_PRIVACY_POLICY
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                startActivity(intent)
            }
        })

    }

    override fun onStop() {
        super.onStop()

        // Cancel all pending Volley requests
        SOSApplication.getInstance().cancelPendingRequests(LOG)
    }

    private fun createAlertDialog(context: Context, title: String, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
        val dialog = builder.create()
        dialog.show()
    }

    private fun attemptLogin(dialog: ProgressDialog, userEmail: String, password: String) {
        SOSAPI(this@LoginActivity).loginUser(userEmail, password,
                { response ->
                    // Stop progress spinner
                    dialog.dismiss()
                    // Check the response result
                    var jsonResponse: JSONObject
                    try {
                        jsonResponse = JSONObject(response)
                        when {
                            jsonResponse.getBoolean(Constants.JSON_LOGGED_IN) -> {
                                // Retrieve details
                                val userId = jsonResponse.getLong(Constants.USER_ID)
                                val email = jsonResponse.getString(Constants.USER_EMAIL)
                                val firstName = jsonResponse.getString(Constants.USER_FIRST_NAME)
                                val lastName = jsonResponse.getString(Constants.USER_LAST_NAME)
                                val phoneNumber = jsonResponse.getString(Constants.USER_PHONE)
                                val token = jsonResponse.getString(Constants.AUTH_TOKEN)

                                // Create Realm User Object
                                val realm = Realm.getDefaultInstance()
                                val service = UserService(realm)
                                service.createUser(userId, firstName, lastName, email, null, null, phoneNumber, token)
                                realm.close()

                                mSession.createSession(email, firstName, lastName, phoneNumber, token, userId.toString())

                                // Re-fetch regions after logging in. Ensure that permissions have been granted in case
                                // the user has re-installed the app, got a new phone, or something similar
                                if (ActivityCompat.checkSelfPermission(this@LoginActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this@LoginActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                    // Re-fetch the regions
                                    RegionFetcherJobService.schedule(this@LoginActivity)
                                }

                                // Navigate to list of sites.
                                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                this@LoginActivity.startActivity(intent)
                            }
                            jsonResponse.getString(Constants.JSON_MESSAGE) == "bad_auth" -> {
                                createAlertDialog(this@LoginActivity,
                                        getString(R.string.error_generic_title),
                                        "Please ensure that you have typed in your email address and password correctly.")

                            }
                            else -> {
                                // The user was not logged in for some reason.
                                SLog.e(LOG, jsonResponse.getString(Constants.JSON_MESSAGE))
                                createAlertDialog(this@LoginActivity,
                                        getString(R.string.error_generic_title),
                                        "Login failed. " + jsonResponse.getString(Constants.JSON_MESSAGE))
                            }

                        }
                    } catch (e: JSONException) {
                        SLog.e(LOG, e.message)
                        jsonResponse = JSONObject(response)
                        if(jsonResponse.getString(Constants.JSON_RESPONSE_STATUS) == Constants.RATE_LIMITED){
                            AlertDialogMessageHelper
                                    .rateLimitErrorMessage(this@LoginActivity,getString(R.string.rate_limit_error_message),getString(R.string.error_rate_limit_title))

                        }
                    }
                }) {
            dialog.dismiss()
            createAlertDialog(this@LoginActivity,
                    getString(R.string.error_generic_title),
                    "Please make sure you are connected to the internet.")
        }
    }

    companion object {
        val LOG: String = LoginActivity::class.java.simpleName
    }

}

