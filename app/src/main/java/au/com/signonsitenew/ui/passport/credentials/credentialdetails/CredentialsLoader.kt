package au.com.signonsitenew.ui.passport.credentials.credentialdetails

interface CredentialsLoader {
    fun refreshListOfCredentials()
}