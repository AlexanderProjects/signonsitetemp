package au.com.signonsitenew.ui.main

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit

import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.*
import io.realm.Realm


/**
 * A simple [Fragment] subclass.
 *
 */
class SiteConfirmationFragment : Fragment() {
    private var company: String? = null
    private var mRealm: Realm? = null
    private var dialog: Dialog? = null
    private var mListener: OnConfirmationSelected? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            company = it.getString(ARG_COMPANY)
        }

        mRealm = Realm.getDefaultInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_site_confirmation, container, false)

        val companyImageView: ImageView = rootView.findViewById(R.id.company_information_image_view)
        val companyTextView: TextView = rootView.findViewById(R.id.company_confirmation_text_view)
        val incorrectButton: Button = rootView.findViewById(R.id.details_incorrect_button)
        val correctButton: Button = rootView.findViewById(R.id.details_correct_button)

        // Set the title for the parent activity
        (activity as FirstSignOnActivity).mTitleTextView.text = "Confirmation"

        companyImageView.setColorFilter(ContextCompat.getColor(requireActivity(), R.color.grey_primary), PorterDuff.Mode.SRC_IN)
        companyTextView.text = company

        incorrectButton.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                activity?.supportFragmentManager?.popBackStack()
            }
        })

        correctButton.setOnClickListener(object : DebouncedOnClickListener() {
            override fun onDebouncedClick(v: View?) {
                mListener?.onCompanyConfirmed(company!!)
                // Update the site settings to show user has now registered
                Log.i(TAG, "User selected this company: $company")
                val fragment = ProgressViewFragment()
                activity!!.supportFragmentManager.commit{
                    addToBackStack(null)
                    setCustomAnimations(R.anim.enter_from_right,
                            R.anim.exit_to_left,
                            R.anim.enter_from_left,
                            R.anim.exit_to_right)
                    replace(R.id.container, fragment)
                }
            }
        })

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mRealm != null)
            mRealm?.close()
        if(dialog != null)
            dialog!!.dismiss()
    }

    companion object {
        val TAG: String = SiteConfirmationFragment::class.java.simpleName
        private const val ARG_COMPANY = "selected_company"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnConfirmationSelected) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnCompanyInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnConfirmationSelected{
        fun onCompanyConfirmed(companyName: String)
    }

}
