package au.com.signonsitenew.ui.attendanceregister.attendance

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceRegisterParentAdapterModel
import au.com.signonsitenew.ui.adapters.AttendanceExpandableParentAdapter
import au.com.signonsitenew.ui.adapters.AttendanceRegisterExpandableParentAdapter
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.QueryTextListener
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class AttendanceRegisterFragment : DaggerFragment(), AttendanceRegisterDisplay {

    companion object {
        fun newInstance() = AttendanceRegisterFragment()
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    @Inject
    lateinit var router: Router
    private lateinit var listener:ShowProgressViewForNewAttendance
    private lateinit var toolbar: Toolbar
    private lateinit var presenterImpl: AttendanceRegisterPresenterImpl
    private lateinit var parentAdapter:AttendanceRegisterExpandableParentAdapter
    private lateinit var parentExpandableAdapter:AttendanceExpandableParentAdapter
    private lateinit var expandableRecyclerViewList:RecyclerView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.new_attendance_register_fragment, container, false)
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        toolbar = view.findViewById(R.id.attendance_register_toolbar)
        toolbar.title = Constants.ATTENDANCE_REGISTER_TITLE
        setHasOptionsMenu(true)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        expandableRecyclerViewList = view.findViewById(R.id.expandable_list_view)
        expandableRecyclerViewList.isNestedScrollingEnabled = false
        expandableRecyclerViewList.addItemDecoration(DividerItemDecoration(expandableRecyclerViewList.context, LinearLayoutManager.VERTICAL))

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this,factory).get(AttendanceRegisterPresenterImpl::class.java)
        presenterImpl.inject(this)
        presenterImpl.observeStates()
        presenterImpl.getWorkersVisits(arguments)
    }

    override fun onResume() {
        super.onResume()
        setHasOptionsMenu(true)
    }

    override fun showData(listAttendance: ArrayList<AttendanceRegisterParentAdapterModel>) {
        parentAdapter = AttendanceRegisterExpandableParentAdapter(listAttendance.toMutableList(), onClickChildAction = {
            presenterImpl.attendanceRegisterWorkerViewedAnalytics(it)
            router.navigateUserDetailFragment(requireActivity(),it)
        })
        expandableRecyclerViewList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = parentAdapter
        }
    }

    override fun showAttendance(listAttendance: ArrayList<AttendanceParentAdapterModel>) {
        parentExpandableAdapter = AttendanceExpandableParentAdapter(listAttendance.toMutableList(),onClickChildAction = {
            presenterImpl.attendanceRegisterWorkerViewedAnalytics(it.userId)
            router.navigateToWorkerDetails(requireActivity(),it)
        })
        expandableRecyclerViewList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = parentExpandableAdapter
        }
    }

    override fun showWorkerDetails(childAdapterModel: AttendanceChildAdapterModel) {
        router.navigateToWorkerDetails(requireActivity(),childAdapterModel)
    }

    override fun showDataError(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),errorMessage)
    }

    override fun showNetworkError() {
        AlertDialogMessageHelper.networkErrorMessage(requireContext())
    }

    override fun showProgressView() {
        listener.showProgressViewForAttendance()
    }

    override fun hideProgressView() {
        listener.hideShowProgressViewForAttendance()
    }

    fun setShowProgressView(listener:ShowProgressViewForNewAttendance){
        this.listener = listener
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search_attendance_register, menu)
        val viewAllUserItem = menu.findItem(R.id.view_all_users)
        val addAttendeeItem = menu.findItem(R.id.add_attendee)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        viewAllUserItem.isVisible = true
        addAttendeeItem.isVisible = false
        searchView.setOnQueryTextListener(QueryTextListener(addData = { text->
            if(presenterImpl.isWorkerNotesFeatureFlagActivated)
                parentExpandableAdapter.getFilter().filter(text)
            else
                parentAdapter.getFilter().filter(text)
        }))
    }

    interface ShowProgressViewForNewAttendance{
        fun showProgressViewForAttendance()
        fun hideShowProgressViewForAttendance()
    }

}
