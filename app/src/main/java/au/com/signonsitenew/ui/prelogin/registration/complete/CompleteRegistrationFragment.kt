package au.com.signonsitenew.ui.prelogin.registration.complete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.databinding.CompleteRegistrationFragmentBinding
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class CompleteRegistrationFragment : DaggerFragment(), CompleteRegistrationDisplay{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val presenterImpl: CompleteRegistrationPresenterImpl by viewModels { viewModelFactory  }

    private var completeRegistrationFragmentBinding: CompleteRegistrationFragmentBinding? = null
    private lateinit var onNextCompleteRegistration: OnNextCompleteRegistration
    private val binding get()= completeRegistrationFragmentBinding!!
    companion object {
        fun newInstance() = CompleteRegistrationFragment()
    }

    fun setOnClickListener(onNextCompleteRegistration: OnNextCompleteRegistration){
        this.onNextCompleteRegistration = onNextCompleteRegistration
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        completeRegistrationFragmentBinding = CompleteRegistrationFragmentBinding.inflate(inflater,container,false)
        val view = binding.root
        presenterImpl.inject(this)
        presenterImpl.checkIfUserHasPassport()
        completeRegistrationFragmentBinding!!.searchForSitesRegistrationButton.setOnClickListener {
            onNextCompleteRegistration.searchForSitesAndNavigateToManActivity()
        }
        return view
    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(requireActivity())
    }

    override fun showPassportButton(value: Boolean) {
        if (value) {
            completeRegistrationFragmentBinding!!.createPassportRegistrationButton.visibility = View.VISIBLE
            completeRegistrationFragmentBinding!!.createPassportRegistrationButton.setOnClickListener {
                onNextCompleteRegistration.createPassportAndNavigateToMainActivity()
            }
        } else{
            completeRegistrationFragmentBinding!!.createPassportRegistrationButton.visibility = View.GONE
            completeRegistrationFragmentBinding!!.passportCreateDescriptionTitle.visibility = View.GONE
        }

    }

    interface OnNextCompleteRegistration {
        fun createPassportAndNavigateToMainActivity()
        fun searchForSitesAndNavigateToManActivity()
    }

}
