package au.com.signonsitenew.ui.main;

public interface SignedOnActivityNavigationListener {
    void navigateToTab(Integer index);
}
