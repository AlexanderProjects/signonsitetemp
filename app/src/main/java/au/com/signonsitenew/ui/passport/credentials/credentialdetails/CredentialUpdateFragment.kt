package au.com.signonsitenew.ui.passport.credentials.credentialdetails

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.utilities.Builder
import au.com.signonsitenew.domain.utilities.CredentialValidator
import au.com.signonsitenew.domain.utilities.Mapper
import au.com.signonsitenew.ui.adapters.UpdateCredentialAdapter
import au.com.signonsitenew.utilities.*
import com.bumptech.glide.Glide
import dagger.android.support.DaggerFragment
import java.io.File
import javax.inject.Inject

class CredentialUpdateFragment : DaggerFragment(), CredentialUpdateView {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var sessionManager: SessionManager
    @Inject
    lateinit var featureFlagsManager: FeatureFlagsManager

    private lateinit var presenterImpl: CredentialUpdatePresenterImpl
    private lateinit var credential: Credential
    private lateinit var credentialUpdateToolbar: Toolbar
    private lateinit var updateCredentialFieldsList: RecyclerView
    private lateinit var adapter: UpdateCredentialAdapter
    private lateinit var frontPhotoImageView: ImageView
    private lateinit var backPhotoImageView: ImageView
    private lateinit var frontPhotoTextView: TextView
    private lateinit var backPhotoTextView: TextView
    private lateinit var saveUpdatedInfoProgressBar: ProgressBar
    private lateinit var updateCredentialSaveButton: Button
    private var frontPhotoFile: File? = null
    private var backPhotoFile: File? = null
    private lateinit var frontPhotoStatus: TextView
    private lateinit var backPhotoStatus: TextView
    private var isFrontPhoto = false
    private lateinit var photoUri: Uri
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_credential_update, container, false)
        credential = requireArguments().getParcelable(Constants.CREDENTIAL_FLAG)!!
        CredentialValidator.setCredential(credential)
        updateCredentialFieldsList = view.findViewById(R.id.update_credential_fields_list)
        frontPhotoImageView = view.findViewById(R.id.credential_update_form_front_photo_imageView)
        backPhotoImageView = view.findViewById(R.id.credential_update_form_back_photo_imageView)
        frontPhotoTextView = view.findViewById(R.id.front_photo_update_text_view)
        backPhotoTextView = view.findViewById(R.id.back_photo_update_text_view)
        frontPhotoStatus = view.findViewById(R.id.status_update_front_photo)
        backPhotoStatus = view.findViewById(R.id.status_update_back_photo)
        updateCredentialSaveButton = view.findViewById(R.id.update_credential_from_save_button)
        credentialUpdateToolbar = view.findViewById(R.id.credential_update_form_toolbar)
        saveUpdatedInfoProgressBar = view.findViewById(R.id.savingUpdatedInfoProgressBar)
        if (CredentialValidator.validateCredentialName() != null) credentialUpdateToolbar.title = CredentialValidator.validateCredentialName()
        (activity as AppCompatActivity?)!!.setSupportActionBar(credentialUpdateToolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        updateCredentialFieldsList.addItemDecoration(DividerItemDecoration(updateCredentialFieldsList.context, DividerItemDecoration.VERTICAL))
        adapter = UpdateCredentialAdapter(Builder.buildFieldsFromCredential(credential), credential)
        updateCredentialFieldsList.adapter = adapter
        frontPhotoImageView.setOnClickListener {
            PermissionManager.checkForCameraPermissions(activity) { startCamera(Constants.IMAGE_CAPTURE_FRONT_FLAG_CODE) }
            PermissionManager.checkForStoragePermissions(activity)
            isFrontPhoto = true
        }
        backPhotoImageView.setOnClickListener {
            PermissionManager.checkForCameraPermissions(activity) { startCamera(Constants.IMAGE_CAPTURE_BACK_FLAG_CODE) }
            PermissionManager.checkForStoragePermissions(activity)
            isFrontPhoto = false
        }
        if (credential.front_photo_temporary_url != null) {
            Glide.with(requireActivity())
                    .load(credential.front_photo_temporary_url)
                    .placeholder(android.R.drawable.picture_frame)
                    .centerCrop()
                    .into(frontPhotoImageView)
            frontPhotoTextView.visibility = View.GONE
            frontPhotoStatus.visibility = View.GONE
        } else {
            frontPhotoStatus.visibility = View.GONE
            frontPhotoImageView.visibility = View.GONE
            frontPhotoTextView.visibility = View.GONE
        }
        if (credential.back_photo_temporary_url != null || credential.credential_type?.back_photo.equals("required") || credential.credential_type?.back_photo.equals("optional")) {
            Glide.with(requireActivity())
                    .load(credential.back_photo_temporary_url)
                    .placeholder(android.R.drawable.picture_frame)
                    .centerCrop()
                    .into(backPhotoImageView)
            backPhotoTextView.visibility = View.GONE
            backPhotoStatus.visibility = View.GONE
        } else {
            backPhotoStatus.visibility = View.GONE
            backPhotoImageView.visibility = View.GONE
            backPhotoTextView.visibility = View.GONE
        }
        if (CredentialValidator.validateFrontPhoto() != null) frontPhotoStatus.text = "(" + " " + credential.front_photo + " " + ")"
        if (CredentialValidator.validateBackPhoto() != null) backPhotoStatus.text = "(" + " " + credential.back_photo + " " + ")"
        updateCredentialSaveButton.setOnClickListener {
            saveUpdatedInfoProgressBar.visibility = View.VISIBLE
            updateCredentials()
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenterImpl = ViewModelProvider(this, viewModelFactory).get(CredentialUpdatePresenterImpl::class.java)
        presenterImpl.inject(this)
    }

    private fun startCamera(captureFlag: Int) {
        photoUri = ImageUtil.getOutputPhotoFileUri(requireContext(), credential.name,isFrontPhoto)

        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri)

        val chooserIntent = Intent.createChooser(galleryIntent, Constants.IMAGE_SOURCE_PICKER_LABEL)
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(cameraIntent, galleryIntent))
        startActivityForResult(chooserIntent, captureFlag)
    }

    private fun updateCredentials() {
        val credentialCreateUpdateRequest = adapter.credentialFields?.let { Mapper.mapCredentialFieldToRequest(it , CredentialValidator.validateIdentifierName()) }?.let { Builder.buildMissingFieldsForUpdateRequest(it, credential) }
        adapter.credentialFields?.let {
            presenterImpl.updateCredentialDetails(frontPhotoFile, backPhotoFile, credentialCreateUpdateRequest!!)
        }

    }

    override fun showNetworkErrors() {
        AlertDialogMessageHelper.networkErrorMessage(activity,
                Constants.NETWORK_MESSAGE_ERROR,
                Constants.NETWORK_MESSAGE_TITLE) { updateCredentials() }
    }

    override fun goBackToCredentials() {
        saveUpdatedInfoProgressBar.visibility = View.GONE
        credentialsLoader!!.refreshListOfCredentials()
        adapter.notifyDataSetChanged()
        requireActivity().supportFragmentManager.popBackStackImmediate()
        sessionManager.editingBackPhoto = false
        sessionManager.editingFrontPhoto = false
    }

    override fun showDataError(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),errorMessage)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(menu.findItem(R.id.share_button) != null)
            menu.findItem(R.id.share_button).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_delete,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            R.id.action_delete ->{
                context?.let { AlertDialogMessageHelper.deleteCredentialAlertDialog(it,action = { presenterImpl.deleteCredential(credential.id.toString()) }) }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // intent data will have photoUri in it if picked from gallery.
        photoUri = if (data?.data == null) photoUri else data.data!!

        when (requestCode) {
            1 -> if (resultCode == Activity.RESULT_OK) {
                frontPhotoTextView.visibility = View.GONE
                frontPhotoStatus.visibility = View.GONE
                frontPhotoFile = ImageUtil.convertUriToFile(requireContext(),photoUri)
                Glide.with(requireActivity()).load(photoUri).into(frontPhotoImageView)
                sessionManager.editingFrontPhoto = true
            }
            2 -> if (resultCode == Activity.RESULT_OK) {
                backPhotoTextView.visibility = View.GONE
                backPhotoStatus.visibility = View.GONE
                backPhotoFile = ImageUtil.convertUriToFile(requireContext(),photoUri)
                Glide.with(requireActivity()).load(photoUri).into(backPhotoImageView)
                sessionManager.editingBackPhoto = true
            }
            else -> DarkToast.makeText(activity, Constants.CAPTURE_CAMERA_CANCELED)
        }
    }

    companion object {
        private var credentialsLoader: CredentialsLoader? = null
        @JvmStatic
        fun newInstance(credential: Credential?): CredentialUpdateFragment {
            val credentialUpdateFragment = CredentialUpdateFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constants.CREDENTIAL_FLAG, credential)
            credentialUpdateFragment.arguments = bundle
            return credentialUpdateFragment
        }

        fun setCredentialsLoader(updateCredentialList: CredentialsLoader?) {
            credentialsLoader = updateCredentialList
        }
    }
}