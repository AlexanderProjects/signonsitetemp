package au.com.signonsitenew.ui.documents.briefingslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.BriefingListRecyclerViewAdapter
import au.com.signonsitenew.models.Briefing

class BriefingContentListFragment : Fragment(), BriefingDataListener {
    private var recyclerView: RecyclerView? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        BriefingListTabbedFragment.setContentDataListener(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_briefing_content_list, container, false)
        recyclerView = view.findViewById(R.id.briefing_content_list)
        return view
    }

    override fun updateData(briefingList: List<Briefing>) {
        recyclerView!!.adapter = BriefingListRecyclerViewAdapter(briefingList, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(): BriefingContentListFragment {
            return BriefingContentListFragment()
        }
    }
}