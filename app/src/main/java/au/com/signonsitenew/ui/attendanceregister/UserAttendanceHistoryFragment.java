package au.com.signonsitenew.ui.attendanceregister;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.adapters.VisitHistoryRecyclerViewAdapter;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.utilities.Constants;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A fragment displaying a user's attendance history for the day.
 */
public class UserAttendanceHistoryFragment extends Fragment {

    private static final String TAG = UserAttendanceHistoryFragment.class.getSimpleName();

    protected static final String ARG_ATTENDEE_ID = "attendeeId";
    private long mUserId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UserAttendanceHistoryFragment() {
    }

    public static UserAttendanceHistoryFragment newInstance(long visitorId) {
        UserAttendanceHistoryFragment fragment = new UserAttendanceHistoryFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_ATTENDEE_ID, visitorId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mUserId = getArguments().getLong(ARG_ATTENDEE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_attendance_history_list, container, false);

        setHasOptionsMenu(true);
        Toolbar toolbar = view.findViewById(R.id.attendance_history_toolbar);
        toolbar.setTitle(Constants.VISITS_TITLE);
        ((AttendanceActivity)getActivity()).setSupportActionBar(toolbar);
        ((AttendanceActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Retrieve data
        List<AttendanceRecord> attendances = new ArrayList<>();
        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());

        // Retrieve the visits and sort them so they appear in the correct order in the list. This
        // would usually be descending, however this will be reversed as they're added to the ArrayList
        // so sort by ASCENDING instead.
        RealmResults<AttendanceRecord> userVisits = realm.where(AttendanceRecord.class)
                                              .equalTo("userId", mUserId)
                                              .sort("id", Sort.ASCENDING)
                                              .findAll();

        // Store visits in a list so that they can be passed between threads
        for (AttendanceRecord visit : userVisits) {
            attendances.add(visit);
        }
        realm.close();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setAdapter(new VisitHistoryRecyclerViewAdapter(attendances));

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem viewAllUserItem = menu.findItem(R.id.view_all_users);
        MenuItem addAttendeeItem = menu.findItem(R.id.add_attendee);
        viewAllUserItem.setVisible(false);
        addAttendeeItem.setVisible(false);
    }
}
