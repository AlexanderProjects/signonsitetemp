package au.com.signonsitenew.ui.prelogin.registration.details

import au.com.signonsitenew.domain.models.EmailVerificationResponse
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.EmailValidationUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.SimpleTextValidationUseCaseImpl
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
interface RegisterDetailsPresenter {
    fun inject(registerDetailsDisplay: RegisterDetailsDisplay)
    fun validateEmail(email:String, firstName:String, lastName:String)
    fun validateEmailFormat(email: String): Boolean
    fun validateFirstNameFormat(firstName: String): Boolean
    fun validateLastNameFormat(lastName:String):Boolean
}

class RegisterDetailsPresenterImpl @Inject constructor(private val emailValidationUseCaseImpl: EmailValidationUseCaseImpl,
                                                       private val sessionManager: SessionManager,
                                                       private val logger: Logger,
                                                       private val analyticsEventDelegateService: AnalyticsEventDelegateService,
                                                       private val simpleTextValidationUseCaseImpl: SimpleTextValidationUseCaseImpl,
                                                       private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl): BasePresenter(), RegisterDetailsPresenter {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var registerDetailsDisplay: RegisterDetailsDisplay

    override fun inject(registerDetailsDisplay: RegisterDetailsDisplay){
        this.registerDetailsDisplay = registerDetailsDisplay
    }

    override fun validateEmail(email:String, firstName:String, lastName:String) {
        disposables.add(
                internetConnectionUseCaseImpl.isInternetConnected().flatMap {
                    when (it) {
                        true -> emailValidationUseCaseImpl.validateEmail(email)
                        else -> Single.just(registerDetailsDisplay.showNetworkErrors())
                    }
                }.subscribe({ response ->
                    if (response is EmailVerificationResponse)
                        if (!response.in_use)
                            when (response.email_status) {
                                Constants.JSON_EMAIL_BAD -> registerDetailsDisplay.showNoValidEmailAlertDialog(Constants.BAD_EMAIL)
                                Constants.JSON_EMAIL_OKAY -> {
                                    sessionManager.updateUserDetail(Constants.USER_EMAIL, email)
                                    sessionManager.updateUserDetail(Constants.USER_FIRST_NAME, firstName)
                                    sessionManager.updateUserDetail(Constants.USER_LAST_NAME, lastName)
                                    registerDetailsDisplay.navigateToNextFragment()
                                    analyticsEventDelegateService.registrationNameAndEmailProvided()
                                }
                            }
                        else registerDetailsDisplay.showNoValidEmailAlertDialog(Constants.EMAIL_IN_USE)
                }, {
                    registerDetailsDisplay.showNetworkErrors()
                    logger.e(this::class.java.name, attributes = mapOf(this::validateEmail.name to it.message))
                }))
    }

    override fun validateEmailFormat(email: String): Boolean = emailValidationUseCaseImpl.isValidEmailAddress(email)
    override fun validateFirstNameFormat(firstName: String): Boolean = simpleTextValidationUseCaseImpl.validateFirstName(firstName)
    override fun validateLastNameFormat(lastName:String):Boolean = simpleTextValidationUseCaseImpl.validateLastName(lastName)

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}