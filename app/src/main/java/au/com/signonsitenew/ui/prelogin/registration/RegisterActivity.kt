package au.com.signonsitenew.ui.prelogin.registration

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.UserRegistrationResponse
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.ui.prelogin.registration.confirmation.RegisterConfirmationFragment
import au.com.signonsitenew.ui.prelogin.registration.complete.CompleteRegistrationFragment
import au.com.signonsitenew.ui.prelogin.registration.details.RegisterDetailsFragment
import au.com.signonsitenew.ui.prelogin.registration.employer.RegisterEmployerFragment
import au.com.signonsitenew.ui.prelogin.registration.password.RegisterPasswordFragment
import au.com.signonsitenew.ui.prelogin.registration.phone.RegisterPhoneFragment
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * RegisterActivity is used for registering new user accounts. It involves three fragments:
 * - Details Fragment where they input their name, and validate a phone number and email address
 * - Password Fragment where they input a password of the specified requirements
 * - Employer Fragment where they input their employer
 *
 * This class handles data flow between fragments and provides a global method for displaying error
 * popups.
 */
class RegisterActivity : DaggerAppCompatActivity(), RegisterEmployerFragment.ClickOnNextListener,
        RegisterDetailsFragment.OnNextRegisterDetailsClickListener,
        RegisterPhoneFragment.OnNextRegisterPhoneClickListener,
        RegisterPasswordFragment.OnNextRegisterPasswordClickListener,
        RegisterConfirmationFragment.OnNextRegisterConfirmation, CompleteRegistrationFragment.OnNextCompleteRegistration{

    @Inject
    lateinit var router: Router

    private lateinit var mSession: SessionManager
    private lateinit var mTitleTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        mTitleTextView = toolbar.findViewById(R.id.toolbar_title_text_view)
        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        mSession = SessionManager(this)
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        var fragment: Fragment? = null
        val title = ""
        if (savedInstanceState == null) {
            // If we're being restored from a previous state, then we don't need to do anything and
            // should return or else we could end up with overlapping fragments.
            fragment = RegisterDetailsFragment()
        }

        // Hide the action bar for UI reasons.
        actionBar.hide()

        // Don't add to backStack, or else we will get to a blank screen on back input
        fragmentTransaction.add(R.id.register_container, fragment!!).commit()
        mTitleTextView.text = title
    }

    private fun loadNextFragment(currentFragment: String) {
        var nextFragment: Fragment? = null
        when (currentFragment) {
            Constants.FRAGMENT_PHONE -> nextFragment = RegisterPhoneFragment()
            Constants.FRAGMENT_PASSWORD -> nextFragment = RegisterPasswordFragment()
            Constants.FRAGMENT_EMPLOYER -> nextFragment = RegisterEmployerFragment()
            Constants.FRAGMENT_CONFIRMATION -> nextFragment = RegisterConfirmationFragment()
        }
        val fm = supportFragmentManager
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right)
        fragmentTransaction.replace(R.id.register_container, nextFragment!!)
        fragmentTransaction.addToBackStack(null).commit()
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if(fragment is RegisterEmployerFragment)
            fragment.setOnClickNextListener(this)
        if(fragment is RegisterDetailsFragment)
            fragment.setOnClickNextListener(this)
        if(fragment is RegisterPhoneFragment)
            fragment.setOnClickListener(this)
        if(fragment is RegisterPasswordFragment)
            fragment.setOnClickListener(this)
        if(fragment is RegisterConfirmationFragment)
            fragment.setOnClickListener(this)
        if(fragment is CompleteRegistrationFragment)
            fragment.setOnClickListener(this)
    }

    override fun navigateToRegisterPhone() {
        loadNextFragment(Constants.FRAGMENT_PHONE)
    }

    override fun navigateToRegisterConfirmation() {
        loadNextFragment(Constants.FRAGMENT_CONFIRMATION )
    }

    override fun navigateToRegisterPassword() {
        loadNextFragment(Constants.FRAGMENT_PASSWORD)
    }

    override fun navigateToRegisterEmployers() {
        loadNextFragment(Constants.FRAGMENT_EMPLOYER)
    }

    override fun navigateToCompleteRegistration() {
        router.navigateToCompleteRegistration(this)
    }

    override fun createPassportAndNavigateToMainActivity() {
        router.navigateToPassport(this)
    }

    override fun searchForSitesAndNavigateToManActivity() {
        router.navigateToSiteList(this)
    }

}