package au.com.signonsitenew.ui.passport.personal

import au.com.signonsitenew.domain.models.User

interface PersonalDataListener {
    fun showData(user: User?)
}