package au.com.signonsitenew.ui.documents.permits.current

import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.state.CurrentPermitsFragmentState
import au.com.signonsitenew.domain.usecases.permits.PermitsUseCase
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.utilities.BasePresenter
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.applySchedulers
import au.com.signonsitenew.utilities.toJson
import com.datadog.android.log.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

interface CurrentPermitsFragmentPresenter{
    fun getCurrentPermits()
    fun inject(display: CurrentPermitsDisplay)
    fun observeStates()
    fun sortPermitListByDate(permits:List<Permit>):List<Permit>
    fun navigateToPermitDetails(permit: Permit)
    fun navigateToTemplatePermit()
    fun buildPermitDocument(): Document
}

class CurrentPermitsFragmentPresenterImpl @Inject constructor(private val permitUseCase: PermitsUseCase,
                                                              private val logger: Logger): BasePresenter(), CurrentPermitsFragmentPresenter {

    lateinit var display: CurrentPermitsDisplay
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val disposable: io.reactivex.rxjava3.disposables.CompositeDisposable = io.reactivex.rxjava3.disposables.CompositeDisposable()
    private val uiStateObservable = PublishSubject.create<CurrentPermitsFragmentState>()

    override fun getCurrentPermits() {
        disposables.add(permitUseCase.retrieveCurrentPermitsAsync()
            .compose(applySchedulers())
            .doOnSubscribe { uiStateObservable.onNext(CurrentPermitsFragmentState.ShowProgressView) }
            .doAfterTerminate { uiStateObservable.onNext(CurrentPermitsFragmentState.RemoveProgressView) }
            .subscribe({
           if (it.status == Constants.JSON_SUCCESS){
               uiStateObservable.onNext(CurrentPermitsFragmentState.ShowPermitList(it.permits))
           }else{
               uiStateObservable.onNext(CurrentPermitsFragmentState.ShowError)
               logger.w(this@CurrentPermitsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CurrentPermitsFragmentPresenterImpl::getCurrentPermits.name to toJson(it)))
           }
        },{
            uiStateObservable.onNext(CurrentPermitsFragmentState.ShowError)
            logger.e(this@CurrentPermitsFragmentPresenterImpl::class.java.name, attributes = mapOf(this@CurrentPermitsFragmentPresenterImpl::getCurrentPermits.name to it.message))
        }))
    }

    override fun inject(display: CurrentPermitsDisplay) {
        this.display = display
    }

    override fun observeStates() {
        disposable.add(uiStateObservable.subscribe{
            when(it){
                is CurrentPermitsFragmentState.ShowPermitList -> display.showPermitList(it.permits)
                is CurrentPermitsFragmentState.ShowError -> display.showPermitsError()
                is CurrentPermitsFragmentState.ClickOnNewPermit -> display.navigateToTemplatePermitFragment()
                is CurrentPermitsFragmentState.NavigatePermitDetails -> display.navigateToPermitDetails(it.permit)
                is CurrentPermitsFragmentState.NavigateTemplatePermit -> display.navigateToTemplatePermitFragment()
                is CurrentPermitsFragmentState.ShowProgressView -> display.showProgressView()
                is CurrentPermitsFragmentState.RemoveProgressView -> display.removeProgressView()
            }
        })
    }

    override fun sortPermitListByDate(permits: List<Permit>): List<Permit> = permitUseCase.sortPermitListByIdDescending(permits)
    override fun navigateToPermitDetails(permit: Permit) = uiStateObservable.onNext(CurrentPermitsFragmentState.NavigatePermitDetails(permit))
    override fun navigateToTemplatePermit() = uiStateObservable.onNext(CurrentPermitsFragmentState.NavigateTemplatePermit)
    override fun buildPermitDocument(): Document = permitUseCase.buildPermitDocument()
    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        disposables.dispose()
    }
}