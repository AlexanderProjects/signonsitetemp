package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.credentials.credentialtypes.CredentialTypesPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CredentialTypesViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CredentialTypesPresenterImpl::class)
    abstract fun credentialTypesViewModel(credentialTypesPresenterImpl: CredentialTypesPresenterImpl): ViewModel
}