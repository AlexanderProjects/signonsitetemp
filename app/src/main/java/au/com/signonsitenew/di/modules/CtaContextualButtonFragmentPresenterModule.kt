package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.cta.CtaContextualButtonFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CtaContextualButtonFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(CtaContextualButtonFragmentPresenterImpl::class)
    abstract fun ctaContextualButtonFragmentPresenter(ctaContextualButtonFragmentPresenterImpl: CtaContextualButtonFragmentPresenterImpl): ViewModel
}