package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.attendanceregister.attendance.AttendanceRegisterPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class NewAttendanceRegisterViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AttendanceRegisterPresenterImpl::class)
    abstract fun newAttendanceRegisterViewModel(attendanceRegisterPresenterImpl: AttendanceRegisterPresenterImpl): ViewModel
}