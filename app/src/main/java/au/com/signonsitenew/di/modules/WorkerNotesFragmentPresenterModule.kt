package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.attendanceregister.workernotes.WorkerNotesFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class WorkerNotesFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(WorkerNotesFragmentPresenterImpl::class)
    abstract fun workerNotesFragmentPresenter(workerNotesFragmentPresenterImpl: WorkerNotesFragmentPresenterImpl): ViewModel
}