package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.details.PermitDetailsFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PermitDetailsFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(PermitDetailsFragmentPresenterImpl::class)
    abstract fun permitDetailsFragmentPresenterModule(permitDetailsFragmentPresenterImpl: PermitDetailsFragmentPresenterImpl): ViewModel
}