package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.main.SignOnActivityPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SignOnActivityViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SignOnActivityPresenterImpl::class)
    abstract fun signOnActivityViewModel(signOnActivityPresenterImpl: SignOnActivityPresenterImpl): ViewModel
}