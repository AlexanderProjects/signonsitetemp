package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.createbriefings.CreateBriefingFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CreateBriefingFragmentViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CreateBriefingFragmentViewModel::class)
    abstract fun creationBriefingsViewModel(createBriefingFragmentViewModel: CreateBriefingFragmentViewModel): ViewModel
}