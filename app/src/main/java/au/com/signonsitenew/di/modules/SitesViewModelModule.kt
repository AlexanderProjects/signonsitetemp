package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.main.SitesFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SitesViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SitesFragmentPresenterImpl::class)
    abstract fun sitesFragmentViewModel(sitesFragmentViewModelImpl: SitesFragmentPresenterImpl): ViewModel
}