package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.details.team.TeamFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PermitTeamTabFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(TeamFragmentPresenterImpl::class)
    abstract fun permitTeamTabPresenterModule(teamFragmentPresenterImpl: TeamFragmentPresenterImpl): ViewModel
}