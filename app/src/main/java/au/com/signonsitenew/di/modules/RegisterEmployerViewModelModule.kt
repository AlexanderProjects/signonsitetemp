package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.prelogin.registration.employer.RegisterEmployerPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RegisterEmployerViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RegisterEmployerPresenterImpl::class)
    abstract fun registerEmployerViewModel(registerEmployerPresenterImpl: RegisterEmployerPresenterImpl): ViewModel
}