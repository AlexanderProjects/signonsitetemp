package au.com.signonsitenew.di.modules

import au.com.signonsitenew.ui.account.TroubleshootActivity
import au.com.signonsitenew.ui.attendanceregister.AttendanceActivity
import au.com.signonsitenew.ui.documents.inductions.InductionActivity
import au.com.signonsitenew.ui.documents.briefings.BriefingsActivity
import au.com.signonsitenew.ui.documents.permits.PermitsActivity
import au.com.signonsitenew.ui.evacuation.EmergencyActivity
import au.com.signonsitenew.ui.evacuation.EmergencyProgressActivity
import au.com.signonsitenew.ui.main.FirstSignOnActivity
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.ui.prelogin.SplashActivity
import au.com.signonsitenew.ui.prelogin.StartActivity
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [BriefingActivityViewModelModule::class])
    abstract fun contributesBriefingsActivity(): BriefingsActivity
    @ContributesAndroidInjector(modules = [SignOnActivityViewModelModule::class])
    abstract fun contributesSignedOnActivity(): SignedOnActivity
    @ContributesAndroidInjector(modules = [MainActivityViewModelModule::class])
    abstract fun contributesMainActivity(): MainActivity
    @ContributesAndroidInjector(modules = [EmergencyProgressActivityPresenterModule::class])
    abstract fun contributesEmergencyProgressActivity(): EmergencyProgressActivity
    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): RegisterActivity
    @ContributesAndroidInjector
    abstract fun contributeAttendanceActivity(): AttendanceActivity
    @ContributesAndroidInjector
    abstract fun contributeInductionActivity(): InductionActivity
    @ContributesAndroidInjector
    abstract fun contributeEmergencyActivity(): EmergencyActivity
    @ContributesAndroidInjector
    abstract fun contributeTroubleshootActivity(): TroubleshootActivity
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity
    @ContributesAndroidInjector
    abstract fun contributeFirstSignOnActivity(): FirstSignOnActivity
    @ContributesAndroidInjector
    abstract fun contributeStartActivity(): StartActivity
    @ContributesAndroidInjector
    abstract fun contributePermitActivity(): PermitsActivity
}