package au.com.signonsitenew.di.modules

import au.com.signonsitenew.domain.models.PermitType
import au.com.signonsitenew.domain.models.RequesteeStates
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.Util
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
open class NetModule {
    @Provides
    @Singleton
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
                .setLenient()
                .create()
    }

    @Provides
    @Singleton
    @Named("gsonWithNulls")
    fun provideGsonSerializeNullsBuilder(): Gson {
        return GsonBuilder()
                .serializeNulls()
                .setLenient()
                .create()
    }

    @Provides
    @Singleton
    fun provideMoshiSerializerBuilder(): Moshi {
        return Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .add(PermitDoneState::class.java,EnumJsonAdapter.create(PermitDoneState::class.java))
                .add(PermitType::class.java,EnumJsonAdapter.create(PermitType::class.java))
                .add(PermitState::class.java,EnumJsonAdapter.create(PermitState::class.java))
                .add(RequesteeStates::class.java, EnumJsonAdapter.create(RequesteeStates::class.java))
                .add(Util.SerializeNulls.JSON_ADAPTER_FACTORY)
                .add(Date::class.java, Rfc3339DateJsonAdapter())
                .build()
    }


    @Provides
    @Singleton
    open fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val interceptorFor500Error = Interceptor { chain: Interceptor.Chain ->
            val request = chain.request()
            val response = chain.proceed(request)
            if (response.code == 500)  response
            response
        }
        val headerInterceptor = Interceptor { chain: Interceptor.Chain ->
            val request = chain.request()
            if (request.url.toString().contains(Constants.ANALYTICS_URLS)) {
                val cacheControl = CacheControl.Builder()
                        .onlyIfCached()
                        .maxAge(10, TimeUnit.DAYS)
                        .build()
                request.newBuilder().cacheControl(cacheControl)
            }
            chain.proceed(request)
        }
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(interceptor)
                .addInterceptor(headerInterceptor)
                .addInterceptor(interceptorFor500Error)
                .build()
    }

    @Provides
    @Singleton
    open fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .baseUrl(Constants.BASE)
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    @Named("retrofitNulls")
    fun provideRetrofitWithNulls(client: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi).withNullSerialization().asLenient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .baseUrl(Constants.BASE)
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    @Named("retrofitForMapResponse")
    fun provideRetrofitForMapResponse(client: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi).withNullSerialization().asLenient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .baseUrl(Constants.BASE)
                .client(client)
                .build()
    }
}