package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.prelogin.registration.details.RegisterDetailsPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RegisterDetailsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RegisterDetailsPresenterImpl::class)
    abstract fun registerDetailsViewModel(registerDetailsPresenterImpl: RegisterDetailsPresenterImpl): ViewModel
}