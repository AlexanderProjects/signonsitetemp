package au.com.signonsitenew.di.modules

import au.com.signonsitenew.data.factory.datasources.notifications.remote.fcm.SosFcmListenerService
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService
import au.com.signonsitenew.locationengine.LocationService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilderModule {
    @ContributesAndroidInjector
    abstract fun contributeSoSGcmListenerService(): SosFcmListenerService
    @ContributesAndroidInjector
    abstract fun contributeRegionFetcherJobService(): RegionFetcherJobService
    @ContributesAndroidInjector
    abstract fun contributeLocationService(): LocationService
}