package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.details.checks.ChecksFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ChecksTabFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(ChecksFragmentPresenterImpl::class)
    abstract fun checksFragmentPresenter(checksFragmentPresenter: ChecksFragmentPresenterImpl): ViewModel
}