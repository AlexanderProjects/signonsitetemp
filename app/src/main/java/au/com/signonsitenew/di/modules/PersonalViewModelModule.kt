package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.personal.PersonalPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PersonalViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(PersonalPresenterImpl::class)
    abstract fun personalTabViewModel(passportViewModel: PersonalPresenterImpl): ViewModel
}