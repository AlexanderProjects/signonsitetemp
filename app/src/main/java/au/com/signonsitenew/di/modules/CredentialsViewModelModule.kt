package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.credentials.CredentialsPassportPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CredentialsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CredentialsPassportPresenterImpl::class)
    abstract fun credentialsTabViewModel(credentialsPassportPresenterImpl: CredentialsPassportPresenterImpl): ViewModel
}