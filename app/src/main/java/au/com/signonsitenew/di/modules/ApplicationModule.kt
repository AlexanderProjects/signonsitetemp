package au.com.signonsitenew.di.modules

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import au.com.signonsitenew.SOSApplication
import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.data.factory.DataFactory
import au.com.signonsitenew.data.factory.datasources.device.DeviceService
import au.com.signonsitenew.data.factory.datasources.net.RestService
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateServiceImpl
import au.com.signonsitenew.domain.usecases.analytics.AnalyticsEventUseCase
import au.com.signonsitenew.realm.services.BriefingService
import au.com.signonsitenew.realm.services.SiteInductionService
import au.com.signonsitenew.realm.services.SiteSettingsService
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.FeatureFlagsManager
import au.com.signonsitenew.utilities.Mapper
import au.com.signonsitenew.utilities.SessionManager
import com.segment.analytics.Analytics
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
open class ApplicationModule {

    @Provides
    @Singleton
    open fun provideContext(application: SOSApplication): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSharedPref(application: SOSApplication): SharedPreferences {
        return application.getSharedPreferences(Constants.PREF_NAME,MODE_PRIVATE)
    }

    @Provides
    @Singleton
    open fun provideRestService(retrofit: Retrofit, @Named("retrofitNulls") retrofitWithNulls: Retrofit, @Named("retrofitForMapResponse") retrofitForMapResponse: Retrofit): RestService {
        return RestService(retrofit, retrofitWithNulls, retrofitForMapResponse)
    }

    @Provides
    @Singleton
    open fun provideSessionManager(application: SOSApplication): SessionManager {
        return SessionManager(application)
    }

    @Provides
    @Singleton
    open fun provideUserService(): UserService {
        return UserService(Realm.getDefaultInstance())
    }

    @Provides
    @Singleton
    fun provideBriefingService(): BriefingService {
        return BriefingService(Realm.getDefaultInstance())
    }

    @Provides
    @Singleton
    fun provideSiteInductionService(): SiteInductionService {
        return SiteInductionService(Realm.getDefaultInstance())
    }

    @Provides
    @Singleton
    open fun provideDataRepository(factory: DataFactory): DataRepository {
        return SosDataRepository(factory)
    }

    @Provides
    @Singleton
    open fun providesDataFactory(retrofit: Retrofit, moshi: Moshi,@Named("retrofitNulls") retrofitWithNulls: Retrofit, @Named("retrofitForMapResponse") retrofitForMapResponse: Retrofit, segAnalytics: Analytics, sharedPreferences: SharedPreferences, context: Context): DataFactory {
        return DataFactory(retrofit, retrofitWithNulls, retrofitForMapResponse,context, segAnalytics, sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideFeatureFlagManager(): FeatureFlagsManager {
        return FeatureFlagsManager()
    }

    @Provides
    @Singleton
    fun provideSiteSettingsService(): SiteSettingsService {
        return SiteSettingsService(Realm.getDefaultInstance())
    }

    @Provides
    @Singleton
    fun provideRouter(): Router {
        return Router()
    }

    @Provides
    @Singleton
    fun provideAnalyticsEventDelegateService(analyticsEventUseCase: AnalyticsEventUseCase): AnalyticsEventDelegateService {
        return AnalyticsEventDelegateServiceImpl(analyticsEventUseCase)
    }

    @Provides
    @Singleton
    fun provideMapper(): Mapper {
        return Mapper()
    }

    @Provides
    @Singleton
    fun provideDeviceService(context: Context, sharedPreferences: SharedPreferences): DeviceService {
        return DeviceService(context,sharedPreferences)
    }
}