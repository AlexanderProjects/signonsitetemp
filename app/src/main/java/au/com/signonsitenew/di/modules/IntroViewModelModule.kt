package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.intro.IntroPassportPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class IntroViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(IntroPassportPresenterImpl::class)
    abstract fun introPassportFragmentViewModel(introPassportPresenterImpl: IntroPassportPresenterImpl): ViewModel
}