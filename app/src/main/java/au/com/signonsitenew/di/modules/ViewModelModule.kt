package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.di.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}