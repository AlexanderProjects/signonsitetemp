package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.prelogin.registration.complete.CompleteRegistrationPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CompleteRegistrationViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CompleteRegistrationPresenterImpl::class)
    abstract fun completeRegistrationViewModel(completeRegistrationPresenterImpl: CompleteRegistrationPresenterImpl): ViewModel
}