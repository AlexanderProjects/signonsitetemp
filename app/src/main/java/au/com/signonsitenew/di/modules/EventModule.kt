package au.com.signonsitenew.di.modules

import au.com.signonsitenew.events.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class EventModule {
    @Provides
    @Singleton
    fun provideRxBusPassport(): RxBusPassport {
        return RxBusPassport()
    }

    @Provides
    @Singleton
    fun provideRxBusSignOn(): RxBusSigOnActivity {
        return RxBusSigOnActivity()
    }

    @Provides
    @Singleton
    fun provideRxBusMainActivity(): RxBusMainActivity {
        return RxBusMainActivity()
    }

    @Provides
    @Singleton
    fun provideRxBusTeamMember(): RxBusSelectedTeamMember{
        return RxBusSelectedTeamMember()
    }

    @Provides
    @Singleton
    fun provideRxBusAddTeamMember(): RxBusAddUnSelectedTeamMember{
        return RxBusAddUnSelectedTeamMember()
    }

    @Provides
    @Singleton
    fun provideRxBusUpdateCtaState():RxBusUpdateCtaState{
        return RxBusUpdateCtaState()
    }

    @Provides
    @Singleton
    fun provideRxUpdatePermitDetails():RxBusUpdatePermitDetails{
        return RxBusUpdatePermitDetails()
    }

    @Provides
    @Singleton
    fun provideRxUpdatePermitTeamTab():RxBusUpdateTeamTab{
        return RxBusUpdateTeamTab()
    }

    @Provides
    @Singleton
    fun provideRxLocationPermissionResult():RxBusLocationPermissionResult{
        return RxBusLocationPermissionResult()
    }

    @Provides
    @Singleton
    fun provideRxTaskTab():RxBusTaskTab{
        return RxBusTaskTab()
    }

    @Provides
    @Singleton
    fun provideRxChecksTab():RxBusChecksTab{
        return RxBusChecksTab()
    }
    @Provides
    @Singleton
    fun provideRxTaskContentTypeComponent():RxBusTaskContentTypeComponent{
        return RxBusTaskContentTypeComponent()
    }
    @Provides
    @Singleton
    fun provideRxChecksContentTypeComponent():RxBusChecksContentTypeComponent{
        return RxBusChecksContentTypeComponent()
    }
    @Provides
    @Singleton
    fun provideRxImageUri():RxBusTaskImageUri{
        return RxBusTaskImageUri()
    }
    @Provides
    @Singleton
    fun provideRxImageUriInChecks():RxBusChecksImageUri{
        return RxBusChecksImageUri()
    }
    @Provides
    @Singleton
    fun provideRxSaveButtonState():RxBusSaveButtonState{
        return RxBusSaveButtonState()
    }
    @Provides
    @Singleton
    fun provideRxHasUserUnSavedResponses():RxBusUserPermitResponses{
        return RxBusUserPermitResponses()
    }
}