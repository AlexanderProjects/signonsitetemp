package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.main.menu.MenuFragmentPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MenuViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MenuFragmentPresenter::class)
    abstract fun menuFragmentViewModel(menuFragmentPresenter: MenuFragmentPresenter): ViewModel
}