package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.attendanceregister.workerdetails.WorkerDetailsFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class WorkerDetailsFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(WorkerDetailsFragmentPresenterImpl::class)
    abstract fun workerDetailsFragmentPresenter(workerDetailsFragmentPresenterImpl: WorkerDetailsFragmentPresenterImpl): ViewModel
}