package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CurrentPermitsPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(CurrentPermitsFragmentPresenterImpl::class)
    abstract fun currentPermitsPresenterModule(currentPermitsFragmentPresenter: CurrentPermitsFragmentPresenterImpl): ViewModel
}