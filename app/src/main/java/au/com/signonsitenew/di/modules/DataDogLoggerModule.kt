package au.com.signonsitenew.di.modules

import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.empty
import com.datadog.android.Datadog
import com.datadog.android.log.Logger
import dagger.Module
import dagger.Provides


@Module
class DataDogLoggerModule {

    @Provides
    fun provideDatadogLogger(sessionManager: SessionManager):Logger{
        val userId = if(!sessionManager.userId.isNullOrBlank()) sessionManager.userId else String().empty()
        Datadog.setUserInfo(userId)
        return  Logger.Builder()
                .setNetworkInfoEnabled(true)
                .setLogcatLogsEnabled(true)
                .setDatadogLogsEnabled(true)
                .setBundleWithTraceEnabled(true)
                .setLoggerName("Android Logger")
                .build()

    }
}