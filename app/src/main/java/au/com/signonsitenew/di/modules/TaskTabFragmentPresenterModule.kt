package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.details.task.TaskFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TaskTabFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(TaskFragmentPresenterImpl::class)
    abstract fun taskFragmentPresenter(taskFragmentPresenterImpl: TaskFragmentPresenterImpl): ViewModel
}