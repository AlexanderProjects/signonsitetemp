package au.com.signonsitenew.di.modules

import android.content.Context
import au.com.signonsitenew.utilities.Constants
import com.segment.analytics.Analytics
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AnalyticsModule {
    @Provides
    @Singleton
    fun provideAnalyticsBuilder(context: Context): Analytics {
        val analytics = Analytics.Builder(context, Constants.SEGMENT_WRITE_KEY)
                .logLevel(Analytics.LogLevel.DEBUG)
                .trackApplicationLifecycleEvents()
                .build()
        Analytics.setSingletonInstance(analytics)
        return Analytics.with(context)
    }
}