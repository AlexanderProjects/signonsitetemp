package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.briefingdetails.BriefingsPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BriefingViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(BriefingsPresenterImpl::class)
    abstract fun briefingsViewModel(briefingsPresenterImpl: BriefingsPresenterImpl): ViewModel
}