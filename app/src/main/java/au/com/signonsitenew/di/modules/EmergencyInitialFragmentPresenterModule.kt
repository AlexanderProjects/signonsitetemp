package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.evacuation.EmergencyInitialFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class EmergencyInitialFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(EmergencyInitialFragmentPresenterImpl::class)
    abstract fun emergencyInitialFragmentPresenter(emergencyInitialFragmentPresenterImpl: EmergencyInitialFragmentPresenterImpl): ViewModel
}