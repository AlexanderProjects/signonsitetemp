package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.PassportPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PassportViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(PassportPresenterImpl::class)
    abstract fun passportTabViewModel(passportPresenterImpl: PassportPresenterImpl): ViewModel
}