package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.utilities.phonepicker.PhonePickerViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PhonePickerViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(PhonePickerViewModel::class)
    abstract fun pickerPhoneViewModel(phonePickerViewModel: PhonePickerViewModel): ViewModel
}