package au.com.signonsitenew.di.components

import au.com.signonsitenew.SOSApplication
import au.com.signonsitenew.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ActivityBuilderModule::class,
    ViewModelModule::class,
    FragmentBuilderModule::class,
    AnalyticsModule::class,
    EventModule::class,
    NetModule::class,
    ServiceBuilderModule::class,
    LocationBuilderModule::class,
    DataDogLoggerModule::class,
    UseCasesModule::class])
interface ApplicationComponent : AndroidInjector<SOSApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: SOSApplication): Builder
        fun build(): ApplicationComponent
    }
}