package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.prelogin.registration.confirmation.RegisterConfirmationPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RegisterConfirmationViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(RegisterConfirmationPresenterImpl::class)
    abstract fun registerConfirmationViewModel(registerConfirmationPresenterImpl: RegisterConfirmationPresenterImpl): ViewModel
}