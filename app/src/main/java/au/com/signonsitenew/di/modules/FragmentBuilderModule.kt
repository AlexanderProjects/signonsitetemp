package au.com.signonsitenew.di.modules

import au.com.signonsitenew.ui.account.SettingsFragment
import au.com.signonsitenew.ui.attendanceregister.FormViewFragment
import au.com.signonsitenew.ui.attendanceregister.UserDetailFragment
import au.com.signonsitenew.ui.attendanceregister.attendance.AttendanceRegisterFragment
import au.com.signonsitenew.ui.attendanceregister.workerdetails.WorkerDetailsFragment
import au.com.signonsitenew.ui.attendanceregister.workernotes.WorkerNotesFragment
import au.com.signonsitenew.ui.documents.inductions.InductionFragment
import au.com.signonsitenew.ui.documents.briefingdetails.BriefingsFragment
import au.com.signonsitenew.ui.documents.briefingslist.BriefingListTabbedFragment
import au.com.signonsitenew.ui.documents.createbriefings.CreateBriefingFragment
import au.com.signonsitenew.ui.documents.permits.cta.CtaContextualButtonFragment
import au.com.signonsitenew.ui.documents.permits.template.TemplatePermitFragment
import au.com.signonsitenew.ui.documents.permits.current.CurrentPermitsFragment
import au.com.signonsitenew.ui.documents.permits.details.PermitDetailsFragment
import au.com.signonsitenew.ui.documents.permits.details.checks.ChecksFragment
import au.com.signonsitenew.ui.documents.permits.details.task.TaskFragment
import au.com.signonsitenew.ui.documents.permits.details.team.TeamTabFragment
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMembersFragment
import au.com.signonsitenew.ui.evacuation.EmergencyInitialFragment
import au.com.signonsitenew.ui.main.ManagerFragment
import au.com.signonsitenew.ui.main.SitesFragment
import au.com.signonsitenew.ui.main.documents.DocumentsFragment
import au.com.signonsitenew.ui.main.menu.MenuFragment
import au.com.signonsitenew.ui.passport.PassportFragment
import au.com.signonsitenew.ui.passport.connections.ConnectionsFragment
import au.com.signonsitenew.ui.passport.connections.connectiondetails.ConnectionDetailsFragment
import au.com.signonsitenew.ui.passport.credentials.CredentialsPassportFragment
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment
import au.com.signonsitenew.ui.passport.credentials.credentialdetails.CredentialUpdateFragment
import au.com.signonsitenew.ui.passport.credentials.credentialtypes.CredentialTypesFragment
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoFragment
import au.com.signonsitenew.ui.passport.intro.IntroPassportFragment
import au.com.signonsitenew.ui.passport.personal.PersonalFragment
import au.com.signonsitenew.ui.passport.share.SharePassportFragment
import au.com.signonsitenew.ui.prelogin.registration.complete.CompleteRegistrationFragment
import au.com.signonsitenew.ui.prelogin.registration.confirmation.RegisterConfirmationFragment
import au.com.signonsitenew.ui.prelogin.registration.details.RegisterDetailsFragment
import au.com.signonsitenew.ui.prelogin.registration.employer.RegisterEmployerFragment
import au.com.signonsitenew.ui.prelogin.registration.password.RegisterPasswordFragment
import au.com.signonsitenew.ui.prelogin.registration.phone.RegisterPhoneFragment
import au.com.signonsitenew.utilities.camera.CameraXFragment
import au.com.signonsitenew.utilities.camera.ImagePreviewFragment
import au.com.signonsitenew.utilities.phonepicker.PhonePickerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector(modules = [BriefingListTabViewModelModule::class])
    abstract fun contributeBriefingListTabbedFragment(): BriefingListTabbedFragment
    @ContributesAndroidInjector(modules = [PassportViewModelModule::class])
    abstract fun contributePassportFragment(): PassportFragment
    @ContributesAndroidInjector(modules = [PersonalViewModelModule::class])
    abstract fun contributePersonalFragment(): PersonalFragment
    @ContributesAndroidInjector(modules = [EmergencyInfoViewModelModule::class])
    abstract fun contributeEmergencyInfoFragment(): EmergencyInfoFragment
    @ContributesAndroidInjector(modules = [CredentialsViewModelModule::class])
    abstract fun contributeCredentialsPassportFragment(): CredentialsPassportFragment
    @ContributesAndroidInjector(modules = [PhonePickerViewModelModule::class])
    abstract fun contributePhonePickerFragment(): PhonePickerFragment
    @ContributesAndroidInjector(modules = [CredentialUpdateViewModelModule::class])
    abstract fun contributeCredentialDetailsFragment(): CredentialUpdateFragment
    @ContributesAndroidInjector(modules = [CredentialTypesViewModelModule::class])
    abstract fun contributeCredentialTypesFragment(): CredentialTypesFragment
    @ContributesAndroidInjector(modules = [CredentialCreationViewModelModule::class])
    abstract fun contributeCredentialCreationFragment(): CredentialCreationFormFragment
    @ContributesAndroidInjector(modules = [ConnectionsViewModelModule::class])
    abstract fun contributeConnectionsFragment(): ConnectionsFragment
    @ContributesAndroidInjector(modules = [ConnectionDetailsViewModelModule::class])
    abstract fun contributeConnectionsDetailsFragment(): ConnectionDetailsFragment
    @ContributesAndroidInjector(modules = [SharePassportViewModelModule::class])
    abstract fun contributeSharePassportFragment(): SharePassportFragment
    @ContributesAndroidInjector(modules = [SitesViewModelModule::class])
    abstract fun contributeSitesFragment(): SitesFragment
    @ContributesAndroidInjector(modules = [MenuViewModelModule::class])
    abstract fun contributeMenuFragment(): MenuFragment
    @ContributesAndroidInjector(modules = [IntroViewModelModule::class])
    abstract fun contributeIntroPassportFragment(): IntroPassportFragment
    @ContributesAndroidInjector(modules = [RegisterConfirmationViewModelModule::class])
    abstract fun contributeRegisterConfirmationFragment(): RegisterConfirmationFragment
    @ContributesAndroidInjector(modules = [RegisterDetailsViewModelModule::class])
    abstract fun contributeRegisterDetailsFragment(): RegisterDetailsFragment
    @ContributesAndroidInjector(modules = [RegisterEmployerViewModelModule::class])
    abstract fun contributeRegisterEmployerFragment(): RegisterEmployerFragment
    @ContributesAndroidInjector(modules = [RegisterPhoneViewModelModule::class])
    abstract fun contributeRegisterPhoneFragment(): RegisterPhoneFragment
    @ContributesAndroidInjector(modules = [CompleteRegistrationViewModelModule::class])
    abstract fun contributeCompleteRegistrationFragment(): CompleteRegistrationFragment
    @ContributesAndroidInjector
    abstract fun contributeRegisterPasswordFragment(): RegisterPasswordFragment
    @ContributesAndroidInjector(modules = [DocumentsViewModelModule::class])
    abstract fun contributeDocumentsFragment(): DocumentsFragment
    @ContributesAndroidInjector(modules = [BriefingViewModelModule::class])
    abstract fun contributeBriefingsFragment(): BriefingsFragment
    @ContributesAndroidInjector(modules = [CreateBriefingFragmentViewModelModule::class])
    abstract fun contributeCreateBriefingFragment(): CreateBriefingFragment
    @ContributesAndroidInjector(modules = [NewAttendanceRegisterViewModelModule::class])
    abstract fun contributeAttendanceRegisterFragment(): AttendanceRegisterFragment
    @ContributesAndroidInjector
    abstract fun contributeInductionFragment(): InductionFragment
    @ContributesAndroidInjector
    abstract fun contributeFormViewFragment(): FormViewFragment
    @ContributesAndroidInjector
    abstract fun contributeUserDetailFragment(): UserDetailFragment
    @ContributesAndroidInjector(modules = [EmergencyInitialFragmentPresenterModule::class])
    abstract fun contributeEmergencyInitialFragment(): EmergencyInitialFragment
    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment
    @ContributesAndroidInjector
    abstract fun contributeFragmentManager(): ManagerFragment
    @ContributesAndroidInjector(modules = [WorkerDetailsFragmentPresenterModule::class])
    abstract fun contributeWorkerDetailsFragment(): WorkerDetailsFragment
    @ContributesAndroidInjector(modules = [WorkerNotesFragmentPresenterModule::class])
    abstract fun contributeWorkerNotesFragment(): WorkerNotesFragment
    @ContributesAndroidInjector(modules = [CurrentPermitsPresenterModule::class])
    abstract fun contributeCurrentPermitFragment(): CurrentPermitsFragment
    @ContributesAndroidInjector(modules = [TemplateFragmentPresenterModule::class])
    abstract fun contributeTemplatePermitFragment(): TemplatePermitFragment
    @ContributesAndroidInjector(modules = [PermitDetailsFragmentPresenterModule::class])
    abstract fun contributePermitDetails():PermitDetailsFragment
    @ContributesAndroidInjector(modules = [PermitTeamTabFragmentPresenterModule::class])
    abstract fun contributeTeamTabFragment():TeamTabFragment
    @ContributesAndroidInjector(modules = [AddPermitTeamTabPresenterModule::class])
    abstract fun contributeAddTeamTabFragment():AddTeamMembersFragment
    @ContributesAndroidInjector(modules = [CtaContextualButtonFragmentPresenterModule::class])
    abstract fun contributeCtaContextualButtonFragment():CtaContextualButtonFragment
    @ContributesAndroidInjector(modules = [TaskTabFragmentPresenterModule::class])
    abstract fun contributeTaskTabFragment():TaskFragment
    @ContributesAndroidInjector(modules = [ChecksTabFragmentPresenterModule::class])
    abstract fun contributeChecksTabFragment():ChecksFragment
    @ContributesAndroidInjector
    abstract fun contributeCameraXFragment():CameraXFragment
    @ContributesAndroidInjector
    abstract fun contributePreviewImageFragment():ImagePreviewFragment
}