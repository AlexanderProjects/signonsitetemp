package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.connections.connectiondetails.ConnectionDetailsPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ConnectionDetailsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ConnectionDetailsPresenterImpl::class)
    abstract fun connectionDetailsViewModel(connectionDetailsPresenterImpl: ConnectionDetailsPresenterImpl): ViewModel
}