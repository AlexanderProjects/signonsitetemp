package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.credentials.credentialdetails.CredentialUpdatePresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CredentialUpdateViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CredentialUpdatePresenterImpl::class)
    abstract fun credentialDetailsViewModel(credentialsDetailsViewModel: CredentialUpdatePresenterImpl): ViewModel
}