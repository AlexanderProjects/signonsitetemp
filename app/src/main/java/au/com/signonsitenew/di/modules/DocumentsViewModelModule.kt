package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.main.documents.DocumentsFragmentPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DocumentsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(DocumentsFragmentPresenter::class)
    abstract fun documentsFragmentViewModel(documentsFragmentPresenter: DocumentsFragmentPresenter): ViewModel
}