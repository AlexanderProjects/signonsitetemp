package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.share.SharePassportPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SharePassportViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SharePassportPresenterImpl::class)
    abstract fun sharePassportViewModel(sharePassportPresenterImpl: SharePassportPresenterImpl): ViewModel
}