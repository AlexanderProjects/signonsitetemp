package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.briefings.BriefingsActivityPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BriefingActivityViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(BriefingsActivityPresenter::class)
    abstract fun briefingsActivityViewModel(briefingsActivityPresenter: BriefingsActivityPresenter): ViewModel
}