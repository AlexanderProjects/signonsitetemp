package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.evacuation.EmergencyProgressActivityPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class EmergencyProgressActivityPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(EmergencyProgressActivityPresenterImpl::class)
    abstract fun emergencyInitialFragmentPresenter(emergencyProgressActivityPresenter: EmergencyProgressActivityPresenterImpl): ViewModel
}