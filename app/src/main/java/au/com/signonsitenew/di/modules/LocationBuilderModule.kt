package au.com.signonsitenew.di.modules

import android.content.Context
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.jobs.offline.SignOffJob
import au.com.signonsitenew.jobs.offline.SignOnJob
import au.com.signonsitenew.locationengine.*
import au.com.signonsitenew.locationengine.LocationFilter.LocationFilterDelegate
import au.com.signonsitenew.utilities.SessionManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocationBuilderModule {
    @Provides
    @Singleton
    fun provideLocationEngineBridge(context: Context, geofenceManager: GeofenceManager, networkAutoSignOnOff: NetworkAutoSignOnOff): LocationEngineBridge {
        return LocationEngineBridge(context, geofenceManager, networkAutoSignOnOff)
    }

    @Provides
    @Singleton
    fun provideLocationFilter(context: Context, locationEngineBridge: LocationEngineBridge, delegate: LocationFilterDelegate): LocationFilter {
        return LocationFilter(context, locationEngineBridge, delegate)
    }

    @Provides
    @Singleton
    fun provideLocationManager(context: Context, geofenceManager: GeofenceManager, locationEngineBridge: LocationEngineBridge): LocationManager {
        return LocationManager(context, geofenceManager, locationEngineBridge)
    }

    @Provides
    fun provideGeoFenceAdder(context: Context, sessionManager: SessionManager): GeofenceAdder {
        return GeofenceAdder(context, sessionManager)
    }

    @Provides
    @Singleton
    fun provideGeoFenceManager(context: Context, geofenceAdder: GeofenceAdder): GeofenceManager {
        return GeofenceManager(context, geofenceAdder)
    }

    @Provides
    @Singleton
    fun provideSignOffJob(notificationUseCase: NotificationUseCase): SignOffJob {
        return SignOffJob(notificationUseCase)
    }

    @Provides
    @Singleton
    fun provideNetworkAutoSignOff(signOffJob: SignOffJob, signOnJob: SignOnJob, context: Context, sessionManager: SessionManager, notificationUseCase: NotificationUseCase, analyticsEventDelegateService: AnalyticsEventDelegateService): NetworkAutoSignOnOff {
        return NetworkAutoSignOnOff(context, sessionManager, notificationUseCase, signOffJob, signOnJob, analyticsEventDelegateService)
    }

    @Provides
    @Singleton
    fun provideSignOnJob(notificationUseCase: NotificationUseCase): SignOnJob {
        return SignOnJob(notificationUseCase)
    }
}