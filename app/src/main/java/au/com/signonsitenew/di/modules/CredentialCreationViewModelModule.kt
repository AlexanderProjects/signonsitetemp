package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class CredentialCreationViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(CredentialCreationPresenterImpl::class)
    abstract fun credentialCreationViewModel(credentialCreationPresenterImpl: CredentialCreationPresenterImpl): ViewModel
}