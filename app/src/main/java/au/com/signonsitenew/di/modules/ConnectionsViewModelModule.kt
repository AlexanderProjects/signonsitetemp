package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.passport.connections.ConnectionsPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ConnectionsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ConnectionsPresenterImpl::class)
    abstract fun connectionsViewModel(connectionsPresenterImpl: ConnectionsPresenterImpl): ViewModel
}