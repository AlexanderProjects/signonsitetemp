package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.details.team.addTeamMembers.AddTeamMemberFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AddPermitTeamTabPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(AddTeamMemberFragmentPresenterImpl::class)
    abstract fun permitAddPermitTeamTabPresenterModule(addTeamMemberFragmentPresenter: AddTeamMemberFragmentPresenterImpl): ViewModel
}