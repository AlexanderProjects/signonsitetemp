package au.com.signonsitenew.di.modules

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.di.scope.ViewModelKey
import au.com.signonsitenew.ui.documents.permits.template.TemplatePermitFragmentPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TemplateFragmentPresenterModule {
    @Binds
    @IntoMap
    @ViewModelKey(TemplatePermitFragmentPresenterImpl::class)
    abstract fun createPermitFragmentPresenterModule(createPermitFragmentPresenter: TemplatePermitFragmentPresenterImpl):ViewModel
}