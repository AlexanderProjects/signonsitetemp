package au.com.signonsitenew.di.modules

import android.content.Context
import au.com.signonsitenew.data.factory.datasources.device.DeviceService
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.analytics.AnalyticsEventUseCase
import au.com.signonsitenew.domain.usecases.analytics.AnalyticsEventUseCaseImpl
import au.com.signonsitenew.domain.usecases.attendance.AttendanceRegisterUseCase
import au.com.signonsitenew.domain.usecases.attendance.AttendanceRegisterUseCaseImpl
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCase
import au.com.signonsitenew.domain.usecases.briefings.BriefingsUseCaseImpl
import au.com.signonsitenew.domain.usecases.connections.ConnectionListUseCaseImpl
import au.com.signonsitenew.domain.usecases.connections.UpdateEnrolmentUseCaseImpl
import au.com.signonsitenew.domain.usecases.credentials.CredentialsUseCaseImpl
import au.com.signonsitenew.domain.usecases.documents.DocumentsUseCase
import au.com.signonsitenew.domain.usecases.emergency.PassportEmergencyInfoUseCaseImpl
import au.com.signonsitenew.domain.usecases.evacuation.EvacuationUseCase
import au.com.signonsitenew.domain.usecases.evacuation.EvacuationUseCaseImpl
import au.com.signonsitenew.domain.usecases.fcm.RegisterFcmTokenUseCase
import au.com.signonsitenew.domain.usecases.fcm.RegisterFcmTokenUseCaseImpl
import au.com.signonsitenew.domain.usecases.featureflag.FeatureFlagsUseCase
import au.com.signonsitenew.domain.usecases.inductions.InductionUseCaseImpl
import au.com.signonsitenew.domain.usecases.inductions.InductionsUseCase
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.location.LocationServiceUseCase
import au.com.signonsitenew.domain.usecases.location.LocationServiceUseCaseImpl
import au.com.signonsitenew.domain.usecases.menu.MenuUseCase
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.domain.usecases.notifications.NotificationsUseCaseImpl
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCase
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCaseImpl
import au.com.signonsitenew.domain.usecases.passport.ShowUserPassportUseCaseImpl
import au.com.signonsitenew.domain.usecases.permits.*
import au.com.signonsitenew.domain.usecases.personal.PassportPersonalInfoUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.*
import au.com.signonsitenew.domain.usecases.sharepassport.SharePassportUseCaseImpl
import au.com.signonsitenew.domain.usecases.signonstatus.SignOnStatusUseCaseImpl
import au.com.signonsitenew.domain.usecases.siteinformation.GetCompaniesForSiteUseCaseImpl
import au.com.signonsitenew.domain.usecases.siteinformation.GetListOfSitesNearByUseCaseImpl
import au.com.signonsitenew.domain.usecases.workerdetails.WorkerDetailsUseCase
import au.com.signonsitenew.domain.usecases.workerdetails.WorkerDetailsUseCaseImpl
import au.com.signonsitenew.domain.usecases.workernotes.WorkerNotesUseCase
import au.com.signonsitenew.domain.usecases.workernotes.WorkerNotesUseCaseImpl
import au.com.signonsitenew.events.RxBusTaskTab
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.SessionManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class UseCasesModule {
    @Provides
    fun provideConnectionListUseCase(repository: DataRepository, userService: UserService, sessionManager: SessionManager): ConnectionListUseCaseImpl {
        return ConnectionListUseCaseImpl(repository, userService, sessionManager)
    }

    @Provides
    @Singleton
    fun provideUpdateEnrolmentUseCase(repository: DataRepository, sessionManager: SessionManager): UpdateEnrolmentUseCaseImpl {
        return UpdateEnrolmentUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideFeatureFlagUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): FeatureFlagsUseCase {
        return FeatureFlagsUseCase(repository, sessionManager, userService)
    }

    @Provides
    @Singleton
    open fun provideSharePassportUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): SharePassportUseCaseImpl {
        return SharePassportUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    @Singleton
    fun providePersonalInfoUseCase(repository: DataRepository,sessionManager: SessionManager, userService: UserService): PassportPersonalInfoUseCaseImpl {
        return PassportPersonalInfoUseCaseImpl(repository ,sessionManager, userService)
    }

    @Provides
    @Singleton
    fun provideEmergencyInfoUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): PassportEmergencyInfoUseCaseImpl {
        return PassportEmergencyInfoUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    fun provideShowPassportUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): ShowUserPassportUseCaseImpl {
        return ShowUserPassportUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    @Singleton
    fun provideCompaniesForSite(repository: DataRepository, sessionManager: SessionManager): GetCompaniesForSiteUseCaseImpl {
        return GetCompaniesForSiteUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideNearByListOfSites(repository: DataRepository, sessionManager: SessionManager, userService: UserService): GetListOfSitesNearByUseCaseImpl {
        return GetListOfSitesNearByUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    @Singleton
    fun provideEmailValidationUseCase(repository: DataRepository): EmailValidationUseCaseImpl {
        return EmailValidationUseCaseImpl(repository)
    }

    @Provides
    @Singleton
    fun provideEmployersUseCase(repository: DataRepository): EmployersUseCaseImpl {
        return EmployersUseCaseImpl(repository)
    }

    @Provides
    @Singleton
    fun providePhoneValidationUseCase(repository: DataRepository, sessionManager: SessionManager): PhoneValidationUseCaseImpl {
        return PhoneValidationUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideUserRegisterUseCase(repository: DataRepository, sessionManager: SessionManager): UserRegisterUseCaseImpl {
        return UserRegisterUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideSimpleTextValidationUseCase(): SimpleTextValidationUseCaseImpl {
        return SimpleTextValidationUseCaseImpl()
    }

    @Provides
    @Singleton
    fun provideCheckForInternetConnectionUseCase(context: Context): CheckForInternetConnectionUseCaseImpl {
        return CheckForInternetConnectionUseCaseImpl(context)
    }

    @Provides
    @Singleton
    fun provideAttendanceRegisterUseCase(repository: DataRepository, sessionManager: SessionManager, analyticsEventDelegateService: AnalyticsEventDelegateService): AttendanceRegisterUseCase {
        return AttendanceRegisterUseCaseImpl(repository, sessionManager, analyticsEventDelegateService)
    }

    @Provides
    @Singleton
    fun provideCompleteRegistrationUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): CompleteRegistrationUseCaseImpl {
        return CompleteRegistrationUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    fun provideCredentialsUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): CredentialsUseCaseImpl {
        return CredentialsUseCaseImpl(repository, sessionManager, userService)
    }

    @Provides
    fun provideAnalyticsEventUseCase(repository: DataRepository, deviceService: DeviceService, sessionManager: SessionManager): AnalyticsEventUseCase {
        return AnalyticsEventUseCaseImpl(repository, deviceService, sessionManager)
    }

    @Provides
    @Singleton
    fun provideNotificationUseCase(repository: DataRepository, analyticsEventDelegateService: AnalyticsEventDelegateService): NotificationUseCase {
        return NotificationsUseCaseImpl(repository, analyticsEventDelegateService)
    }

    @Provides
    @Singleton
    fun provideSignedOnStatus(repository: DataRepository, sessionManager: SessionManager): SignOnStatusUseCaseImpl {
        return SignOnStatusUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideInductionUseCase(repository: DataRepository, sessionManager: SessionManager): InductionsUseCase {
        return InductionUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideBriefingUseCase(repository: DataRepository, sessionManager: SessionManager): BriefingsUseCase {
        return BriefingsUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideRegisterFcmTokenUseCase(repository: DataRepository, sessionManager: SessionManager): RegisterFcmTokenUseCase {
        return RegisterFcmTokenUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideEvacuationUseCase(repository: DataRepository, sessionManager: SessionManager): EvacuationUseCase {
        return EvacuationUseCaseImpl(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun provideLocationServiceUseCase(context: Context): LocationServiceUseCase {
        return LocationServiceUseCaseImpl(context)
    }

    @Provides
    fun provideMenuUseCase(repository: DataRepository, sessionManager: SessionManager, userService: UserService): MenuUseCase {
        return MenuUseCase(repository, userService, sessionManager)
    }

    @Provides
    fun provideDocumentUseCase(repository: DataRepository, sessionManager: SessionManager): DocumentsUseCase {
        return DocumentsUseCase(repository, sessionManager)
    }

    @Provides
    @Singleton
    fun providePermitsUseCase(repository: DataRepository, sessionManager: SessionManager):PermitsUseCase{
        return PermitsUseCaseImpl(sessionManager,repository)
    }

    @Provides
    @Singleton
    fun provideWorkerDetailsUseCase(repository: DataRepository, sessionManager: SessionManager, analyticsEventDelegateService: AnalyticsEventDelegateService): WorkerDetailsUseCase {
        return WorkerDetailsUseCaseImpl(repository, sessionManager, analyticsEventDelegateService)
    }

    @Provides
    @Singleton
    fun provideTemplatePermitsUseCase(repository: DataRepository, sessionManager: SessionManager): PermitTemplateUseCase { return  PermitTemplateUseCaseImpl(sessionManager,repository) }

    @Provides
    @Singleton
    fun provideWorkerNotesUseCase(): WorkerNotesUseCase { return WorkerNotesUseCaseImpl() }

    @Provides
    @Singleton
    fun providePermitTeamTabUseCase(sessionManager: SessionManager, repository: DataRepository):PermitTeamTabUseCase { return PermitTeamTabUseCaseImpl(sessionManager,repository) }

    @Provides
    @Singleton
    fun provideCtaPermitContextualUseCase(sessionManager: SessionManager, repository: DataRepository):PermitCtaContextualUseCase { return PermitCtaContextualUseCaseImpl(sessionManager, repository)}

    @Provides
    @Singleton
    fun providePermitTaskTabUseCase(sessionManager: SessionManager, repository: DataRepository, rxBusTaskTab: RxBusTaskTab):PermitTaskTabUseCase { return PermitTaskTabUseCaseImpl(sessionManager, repository ,rxBusTaskTab) }

    @Provides
    @Singleton
    fun provideIntroPassportUseCase(sessionManager: SessionManager, repository: DataRepository, userService: UserService):IntroPassportUseCase { return IntroPassportUseCaseImpl(repository,sessionManager,userService)
    }

    @Provides
    @Singleton
    fun providePermitsChecksTabUseCase(sessionManager: SessionManager, repository: DataRepository):PermitsChecksTabUseCase{ return PermitsChecksTabUseCaseImpl(sessionManager,repository) }
}