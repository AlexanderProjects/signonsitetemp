package au.com.signonsitenew.utilities;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import androidx.core.content.FileProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import au.com.signonsitenew.R;

/**
 * Created by Krishan Caldwell on 12/07/2016. And modified by Alexander Parra 08/06/2020
 */
public class ImageUtil {

    private static final String LOG = ImageUtil.class.getSimpleName();


    public static Bitmap  getScaledBitmap(Context context, Uri uri, int maxImageResolution, boolean hasRotation) {
        InputStream inputStream = null;

        try {
//            final int IMAGE_MAX_SIZE = 1200000; // 1.2 MP
            final int IMAGE_MAX_SIZE = maxImageResolution;
            inputStream = context.getContentResolver().openInputStream(uri);
            File extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File file = File.createTempFile(Constants.PROFILE_FILE_PHOTO + Calendar.getInstance().getTimeInMillis(), ".jpg", extStorageDirectory);
            ExifInterface newExif = new ExifInterface(file.getPath());


            // Decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, options);
            inputStream.close();

            int scale = 1;
            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) > IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.i(LOG, "scale = " + scale + ", orig-width: " + options.outWidth + ", orig-height " + options.outHeight);

            Bitmap bitmap = null;
            inputStream = context.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;

                // Scale to max possible inSampleSize that still yields an image larger than the target
                options = new BitmapFactory.Options();
                options.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(inputStream, null, options);

                if(newExif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0") && hasRotation) {
                    bitmap= rotate(bitmap, 90);
                }

                // Resize to desired dimensions
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                Log.i(LOG, "1th scale operation dimensions - witdth: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x, (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;
                FileOutputStream outStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, outStream);

                outStream.flush();
                outStream.close();

                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(inputStream);
            }
            inputStream.close();

            Log.i(LOG, "bitmap size - width: " + bitmap.getWidth() + ", height: " + bitmap.getHeight());

            return bitmap;
        } catch (IOException e) {
            SLog.e(LOG, "IO Exception occurred: " + e.getMessage());
            return null;
        }
    }

    public static File convertUriToFile(Context context, Uri imageUri) throws IOException {
        Bitmap bitmap = getScaledBitmap(context,imageUri,Constants.IMAGE_MEDIUM,true);
        return convertBitmapToFile(bitmap,getFileName(context,imageUri));
    }


    public static File convertExistingUriToFile(Context context, Uri imageUri){
        OutputStream outStream;
        File file = new File(imageUri.getPath());
        try {
            ExifInterface newExif = new ExifInterface(file.getAbsolutePath());
            int orientation = newExif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver() , imageUri);
            outStream = new FileOutputStream(file);
            if(orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                bitmap= rotate(bitmap, 90);
            }
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }

    public static File convertBitmapToFile(Bitmap bitmap, String photoFileName) throws IOException {
        File extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        OutputStream outStream;
        File file = File.createTempFile(photoFileName + Calendar.getInstance().getTimeInMillis(), ".jpg", extStorageDirectory);
        if (file.exists()) {
            file.delete();
            file = File.createTempFile(photoFileName + Calendar.getInstance().getTimeInMillis(), ".jpg", extStorageDirectory);
        }
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }

    private static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf(File.separator);
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static Uri getOutputPhotoFileUri(Context context, String credentialType, Boolean mTakingFrontPic) {
        if (isExternalStorageAvailable()) {
            String appName = context.getString(R.string.app_name);
            File mediaStorageDir = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appName);

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    SLog.e(LOG, "Failed to create storage directory");
                    return null;
                }
            }

            File mediaFile;
            Date now = new Date();
            String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(now);

            String path = mediaStorageDir.getPath() + File.separator;

            String side;
            if (mTakingFrontPic) {
                side = "front_";
            } else {
                side = "back_";
            }

            mediaFile = new File(path + credentialType + side + "_" + "IMG_" + timestamp + ".jpg");

            Log.d(LOG, "File: " + FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", mediaFile));

            return FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", mediaFile);
        } else {
            return null;
        }
    }

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    private static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

}