package au.com.signonsitenew.utilities.camera

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import au.com.signonsitenew.databinding.FragmentImagePreviewBinding
import au.com.signonsitenew.domain.models.state.UsingCameraX
import au.com.signonsitenew.events.RxBusChecksImageUri
import au.com.signonsitenew.events.RxBusTaskImageUri
import au.com.signonsitenew.ui.navigation.Router
import au.com.signonsitenew.utilities.Constants
import com.bumptech.glide.Glide
import com.datadog.android.log.Logger
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class ImagePreviewFragment : DaggerFragment(){

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var logger: Logger

    @Inject
    lateinit var rxBusTaskImageUri: RxBusTaskImageUri

    @Inject
    lateinit var rxBusChecksImageUri: RxBusChecksImageUri

    private var _binding: FragmentImagePreviewBinding? = null
    private val binding get() = _binding!!
    private lateinit var imageUri:Uri
    private lateinit var usingCameraX:UsingCameraX

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentImagePreviewBinding.inflate(inflater, container, false)
        arguments?.let {
            imageUri = it.get(Constants.PERMIT_IMAGE_URL) as Uri
            usingCameraX = it.getParcelable(Constants.CAMERAX_PASSING_OBJECT)!!
            Glide.with(this).load(imageUri).into(binding.photoView)
        }
        binding.imagePreviewToolbar.setOnClickListener {
            router.navigateToPreviousView(requireActivity())
        }
        binding.usePhotoImageButton.setOnClickListener{
            sendUriToRequesterFragment(imageUri)
            router.navigateToPreviousView(requireActivity())
        }
        binding.editPhotoButton.setOnClickListener{
            CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).start(requireContext(),this)
        }
        binding.retakePhotoButton.setOnClickListener { requireActivity().supportFragmentManager.popBackStack() }
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                sendUriToRequesterFragment(result.uri)
                router.navigateToPreviousView(requireActivity())
            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                logger.e(this::class.java.name, attributes = mapOf(this::onActivityResult.name to result.error.message))
            }
        }
    }

    private fun sendUriToRequesterFragment(uri:Uri){
        when(usingCameraX){
            UsingCameraX.TaskFragment -> rxBusTaskImageUri.sendImageUri(uri)
            UsingCameraX.ChecksFragment -> rxBusChecksImageUri.sendImageUriInChecks(uri)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

}