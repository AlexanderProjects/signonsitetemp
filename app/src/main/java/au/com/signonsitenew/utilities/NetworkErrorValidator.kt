package au.com.signonsitenew.utilities

import au.com.signonsitenew.domain.models.ApiResponse

object NetworkErrorValidator {

    fun isValidResponse(apiResponse: ApiResponse): Boolean = when(apiResponse.status){
        Constants.PERSONAL_INFO_SUCCESS -> true
        else -> false
    }

    fun getErrorMessage(apiResponse: ApiResponse): String = when(apiResponse.status){
        Constants.PERSONAL_INFO_UNAUTHORISED -> Constants.PERSONAL_INFO_UNAUTHORISED_MESSAGE
        Constants.PERSONAL_INFO_UNAUTHENTIC -> Constants.PERSONAL_INFO_UNAUTHENTIC_MESSAGE
        Constants.PERSONAL_INFO_BAD_INPUT -> Constants.PERSONAL_INFO_BAD_INPUT_MESSAGE
        Constants.RESPONSE_CREDENTIAL_NOT_MATCH -> Constants.RESPONSE_CREDENTIAL_NOT_MATCH_MESSAGE
        Constants.RESPONSE_SHARED_PASSPORT_BAD_EMAIL -> Constants.RESPONSE_SHARED_PASSPORT_BAD_EMAIL_MESSAGE
        Constants.RESPONSE_NO_PASSPORT -> Constants.RESPONSE_NO_PASSPORT_MESSAGE
        Constants.ALREADY_REGISTERED -> Constants.ALREADY_REGISTERED_MESSAGE
        Constants.BAD_AUTH -> Constants.BAD_AUTH_MESSAGE
        Constants.BAD_PASSWORD -> Constants.BAD_PASSWORD_MESSAGE
        Constants.BAD_PHONE_NUMBER -> Constants.BAD_PHONE_NUMBER_MESSAGE
        Constants.RATE_LIMITED -> Constants.RATE_LIMIT_ERROR_MESSAGE
        Constants.RESPONSE_NOT_SIGNED_ON_TO_BRIEFINGS_SITE -> Constants.RESPONSE_NOT_SIGNED_ON_TO_BRIEFINGS_SITE_MESSAGE
        Constants.RESPONSE_SITE_ARCHIVED -> Constants.RESPONSE_SITE_ARCHIVED_MESSAGE
        Constants.RESPONSE_START_DATE_BEFORE_TODAY -> Constants.RESPONSE_START_DATE_BEFORE_TODAY_MESSAGE
        Constants.RESPONSE_END_DATE_BEFORE_START_DATE -> Constants.RESPONSE_END_DATE_BEFORE_START_DATE_MESSAGE
        else -> Constants.UNKNOWN_NETWORK_ERROR_MESSAGE
    }
}