package au.com.signonsitenew.utilities

data class FeatureFlagsManager(var hasUserPassportEnable: Boolean = false, var hasUserPassportShareEnable: Boolean = false )