package au.com.signonsitenew.utilities

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateFormatUtil {
    private val LOG = DateFormatUtil::class.java.simpleName
    @SuppressLint("SimpleDateFormat")
    @JvmStatic
    fun format(dateString: String?): String {
        val sosServerFormat = SimpleDateFormat("yyyy-MM-dd") as DateFormat
        val targetFormat = SimpleDateFormat("dd MMM yyyy")
        var date: Date? = Date()
        try {
            date = sosServerFormat.parse(dateString)
        } catch (e: ParseException) {
            SLog.e(LOG, "Parse Exception: " + e.message)
        }
        return targetFormat.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateFormatResponse(selectedDate: String?): String? {
        val datePattern = "\\d{4}-\\d{2}-\\d{2}"
        if (selectedDate!!.matches(datePattern.toRegex())) {
            var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            try {
                val date = simpleDateFormat.parse(selectedDate)
                simpleDateFormat = SimpleDateFormat("dd MMM yyyy")
                return simpleDateFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return selectedDate
    }

    @SuppressLint("SimpleDateFormat")
    fun isValidateIssuedDate(selectedDate: String?): Boolean {
        if(selectedDate != null && selectedDate != Constants.REQUIRED) {
            if (selectedDate.isNotEmpty()) {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                return try {
                    simpleDateFormat.parse(convertDateFormatToRequestFormat(selectedDate)).before(Calendar.getInstance().time)
                } catch (e: ParseException) {
                    e.printStackTrace()
                    false
                }
            }
        }
        return false
    }

    @SuppressLint("SimpleDateFormat")
    fun isValidateExpiryDate(selectedDate: String?): Boolean {
        return if(selectedDate != null  && selectedDate != Constants.REQUIRED) {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            try {
                simpleDateFormat.parse(convertDateFormatToRequestFormat(selectedDate)).after(Calendar.getInstance().time)
            } catch (e: ParseException) {
                e.printStackTrace()
                false
            }
        }else
            false
    }


    @SuppressLint("SimpleDateFormat")
    private fun convertDateFormatToRequestFormat(selectedDate: String?): String? {
        var simpleDateFormat = SimpleDateFormat("dd MMM yyyy")
        try {
            if (selectedDate != null) {
                val date = simpleDateFormat.parse(selectedDate)
                simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                return simpleDateFormat.format(date)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateFormatResponseForUser(selectedDate: Date?): String? {
        if(selectedDate != null) {
            try {
                var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                return simpleDateFormat.format(selectedDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }
        return null
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeDiff(dateTime:String?): Long {
        return if(dateTime != null){
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val oldDate = simpleDateFormat.parse(dateTime)
            Calendar.getInstance().time.time - oldDate.time
        }else{
            0
        }
    }

}