package au.com.signonsitenew.utilities

import au.com.signonsitenew.domain.models.UpdatePermitInfoRequest
import au.com.signonsitenew.models.Document
import com.segment.analytics.Properties

fun properties(block: Properties.() -> Unit):Properties = Properties().apply(block)
fun document(block:Document.()->Unit): Document = Document().apply(block)
fun updatePermitInfoRequest(block: UpdatePermitInfoRequest.() -> Unit): UpdatePermitInfoRequest = UpdatePermitInfoRequest().apply(block)