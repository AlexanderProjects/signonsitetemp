package au.com.signonsitenew.utilities;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.TagConstraint;

import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.jobscheduler.DeviceInfoJobService;
import au.com.signonsitenew.jobscheduler.RegionFetcherJobService;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.locationengine.LocationManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.realm.User;
import au.com.signonsitenew.realm.UserAbilities;
import au.com.signonsitenew.ui.prelogin.StartActivity;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by kcaldwell on 21/10/17.
 */

public class LogoutUtil {
    private static final String LOG = LogoutUtil.class.getSimpleName();

    public static void logoutUser(Context context, LocationManager locationManager) {
        Log.d(LOG, "Logout user was called");
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<User> userResults = realm.where(User.class).findAll();
        userResults.deleteAllFromRealm();
        RealmResults<UserAbilities> abilities = realm.where(UserAbilities.class).findAll();
        abilities.deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();

        // Stop background jobs
        JobManager jobManager = SOSApplication.getInstance().getJobManager();
        jobManager.cancelJobsInBackground(null, TagConstraint.ALL);

        // Stop the location engine
        locationManager.stopServices();

        // Stop alarms
        RegionFetcherJobService.cancel(context);
        DeviceInfoJobService.cancel(context);

        // Clear stored session data
        SessionManager session = new SessionManager(context);
        session.logoutUser();

        // After logout, redirect user to LoginActivity.
        Intent intent = new Intent(context, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.LOGOUT, null, null);
        Log.d(LOG, "User has been logged out");
    }
}
