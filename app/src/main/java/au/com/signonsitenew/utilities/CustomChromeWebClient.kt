package au.com.signonsitenew.utilities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity


open class CustomChromeWebClient constructor(val activity:FragmentActivity) :WebChromeClient() {

    private var mCustomView: View? = null
    private var mCustomViewCallback: CustomViewCallback? = null
    private var mOriginalOrientation = 0
    private var mOriginalSystemUiVisibility = 0


    override fun getDefaultVideoPoster(): Bitmap? {
        return if (mCustomView == null) {
            null
        } else BitmapFactory.decodeResource(activity.applicationContext.resources, 2130837573)
    }

    @Suppress("DEPRECATION")
    override fun onHideCustomView() {
        (activity.window.decorView as FrameLayout).removeView(mCustomView)
        mCustomView = null
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            activity.window.setDecorFitsSystemWindows(false)
        else
            activity.window.decorView.systemUiVisibility =  mOriginalSystemUiVisibility
        activity.requestedOrientation = mOriginalOrientation
        mCustomViewCallback!!.onCustomViewHidden()
        mCustomViewCallback = null
    }

    override fun onShowCustomView(paramView: View?, paramCustomViewCallback: CustomViewCallback?) {
        if (mCustomView != null) {
            onHideCustomView()
            return
        }
        mCustomView = paramView
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            activity.window.setDecorFitsSystemWindows(true)
        else
            mOriginalSystemUiVisibility = activity.window.decorView.systemUiVisibility
        mOriginalOrientation = activity.requestedOrientation
        mCustomViewCallback = paramCustomViewCallback
        (activity.window.decorView  as FrameLayout).addView(mCustomView, FrameLayout.LayoutParams(-1, -1))
        activity.window.decorView.systemUiVisibility = 3846
    }

    override fun onConsoleMessage(message: String?, lineNumber: Int, sourceID: String?) {
        if (message != null) {
            Log.d("Webview",message)
        }
    }
}