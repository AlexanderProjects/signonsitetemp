package au.com.signonsitenew.utilities

import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest

class Mapper {

    fun mapUserToUserInfoUpdateRequest(user: User): UserInfoUpdateRequest {
        val userRequest = UserInfoUpdateRequest()
        userRequest.first_name = user.first_name
        userRequest.last_name = user.last_name
        userRequest.phone_number = user.phone_number
        userRequest.company_name = user.company_name
        userRequest.post_code = user.post_code
        userRequest.date_of_birth = DateFormatUtil.convertDateFormatResponseForUser(user.date_of_birth)
        userRequest.gender = user.gender
        userRequest.is_experienced = user.is_experienced
        userRequest.is_english_native = user.is_english_native
        userRequest.is_apprentice = user.is_apprentice
        userRequest.is_indigenous = user.is_indigenous
        userRequest.has_passport = user.has_passport
        userRequest.trade = user.trade
        userRequest.medical_information = user.medical_information
        userRequest.is_interpreter_required = user.is_interpreter_required
        userRequest.headshot_photo = user.headshot_photo
        userRequest.primary_contact = user.primary_contact
        userRequest.secondary_contact = user.secondary_contact
        return userRequest
    }
}