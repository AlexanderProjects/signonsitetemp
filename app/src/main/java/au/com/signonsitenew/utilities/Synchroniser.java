package au.com.signonsitenew.utilities;

import java.util.concurrent.Semaphore;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Abstract Class for synchronising operations.
 */

/*
    Example usage:
    Synchroniser<String> myBlockingThing = new Synchroniser<>() {
        private void pretendThisIsAsynchronous() {
            // do stuff for a while
            // then
            setResult("A GOOD RESULT");
        }

        @Override
        public void work() {
            pretendThisIsAsynchronous();
            return; // immediately
        }
    }

    String result = myBlockingThing.sync();
    assert(result.equals("A GOOD RESULT");
 */
public abstract class Synchroniser<Result> {
    public static final String LOG = Synchroniser.class.getSimpleName();
    Result r;
    private Semaphore sync;

    public abstract void work();

    public void setResult(Result r) {
        this.r = r;
        sync.release();
    }

    public Result sync() {
        sync = new Semaphore(0);
        r = null;
        // dont really want this on main thread
        // maybe we shouldnt be doing this here though
        new Thread() {
            @Override
            public void run() {
                work();
            }
        }.start();

        try {
            sync.acquire();
        } catch (Exception e) {
            SLog.i(LOG, "An exception occurred: " + e.getMessage());
        }
        return r;
    }
}
