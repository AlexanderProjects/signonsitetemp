package au.com.signonsitenew.utilities;


import androidx.viewpager.widget.ViewPager;

public class ViewPagerOnPageListener implements ViewPager.OnPageChangeListener {

    private CallAction callActionPersonal;
    private CallAction callActionEmergency;

    public ViewPagerOnPageListener(CallAction callActionPersonal, CallAction callActionEmergency) {
        this.callActionPersonal = callActionPersonal;
        this.callActionEmergency =callActionEmergency;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

        if(callActionPersonal != null)
            callActionPersonal.call(i);

        if(callActionEmergency != null)
            callActionEmergency.call(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }



    public interface CallAction{
        void call(Integer position);
    }
}
