package au.com.signonsitenew.utilities

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class CustomEditTextWatcher(private val editText: EditText,
                            private val updateFlags: () -> Unit,
                            private val showNotProvided: () -> Unit,
                            private val clearActions: () -> Unit) : TextWatcher {

    private lateinit var textBeforeChange:String

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        textBeforeChange = s.toString()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (s != null) {
            if(editText.hasWindowFocus() && s.toString() != textBeforeChange) {
                updateFlags()
            }
        }
        if(s.toString().isEmpty())
            showNotProvided()
        if(s.toString().isNotEmpty())
            clearActions()
    }
}