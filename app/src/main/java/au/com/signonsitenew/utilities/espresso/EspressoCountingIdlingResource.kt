package au.com.signonsitenew.utilities.espresso

import androidx.test.espresso.IdlingResource
import au.com.signonsitenew.utilities.Constants
import java.util.concurrent.atomic.AtomicInteger

class EspressoCountingIdlingResource(private val resourceName:String):IdlingResource {

    private val counter = AtomicInteger(0)

    @Volatile
    private var resourceCallback:
            IdlingResource.ResourceCallback? = null


    override fun getName(): String = resourceName + hashCode().toString()

    override fun isIdleNow(): Boolean = counter.get() == 0

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {this.resourceCallback = callback }

    fun increment(){ counter.getAndIncrement()}

    fun decrement(){
        val counterVal = counter.decrementAndGet()
        if (counterVal == 0) {
            resourceCallback?.onTransitionToIdle()
        } else if (counterVal < 0) {
            throw IllegalStateException(Constants.COUNTER_IDLING_EXCEPTION_MESSAGE)
        }
    }

}