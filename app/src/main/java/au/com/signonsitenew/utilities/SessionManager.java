package au.com.signonsitenew.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import au.com.signonsitenew.domain.models.NearSite;
import au.com.signonsitenew.ui.prelogin.StartActivity;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 * <p>
 * Helper class for persisting data into Shared Preferences.
 */
public class SessionManager {

    private static final String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    private SharedPreferences sharedPreferences;

    // Editor for Shared Preferences.
    private SharedPreferences.Editor editor;

    // Context
    private Context mContext;

    // Shared Preferences Mode
    private int PRIVATE_MODE = 0;

    // This boolean hold the value when the user is editing info in personal
    private Boolean isEditingInPersonalInfoPassport = false;

    // This boolean hold the value when the user is editing info in emergency
    private Boolean isEditingInEmergencyPassport = false;

    // This boolean hold the value when the user is editing front photo
    private Boolean isEditingFrontPhoto = false;

    // This boolean hold the value when the user is editing back photo
    private Boolean isEditingBackPhoto = false;

    // Shared Preferences file name
    private static final String PREF_NAME = "SignOnSitePref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    private static final String IS_SIGNON = "IsSignedOn";

    // Site is currently in Emergency Mode
    private static final String IS_EMERGENCY = "evacuation";

    // User email
    private static final String KEY_EMAIL = "email";

    // Register password
    private static final String KEY_PASS = "password";

    // ID for the site the user is currently signed in
    private static final String SITE_ID = "site_id";

    private static final String NEAR_SITE = "near_site";

    private static final String HAS_PASSPORT = "has_passport";


    // Constructor
    public SessionManager(Context context) {
        mContext = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    /**
     * Create login session.
     */
    public void createSession(String email, String firstName, String lastName, String phoneNumber, String token, String userId) {

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putBoolean(IS_SIGNON, false);

        // Storing email in sharedPreferences.
        editor.putString(KEY_EMAIL, email);
        editor.putString(Constants.USER_FIRST_NAME, firstName);
        editor.putString(Constants.USER_LAST_NAME, lastName);
        editor.putString(Constants.USER_PHONE, phoneNumber);
        editor.putString(Constants.AUTH_TOKEN, token);
        editor.putString(Constants.USER_ID, userId);

        // Commit changes.
        editor.commit();
    }

    /**
     * Store a JWT token in local storage for making API requests.
     */
    public void storeToken(String token) {
        editor.putString(Constants.AUTH_TOKEN, token);
        editor.commit();
    }

    /**
     * Returns the JWT token for API calls.
     *
     * @return the encrypted JWT.
     */
    public String getToken() {
        return sharedPreferences.getString(Constants.AUTH_TOKEN, null);
    }

    public String getUserId() {
        return sharedPreferences.getString(Constants.USER_ID, null);
    }

    /**
     * Method to check the current user's login status.
     * If false, it will redirect user to LoginActivity.
     * Else do nothing.
     */
    public void checkLogin() {

        // Check login status
        if (!this.isLoggedIn()) {
            // User is not logged in, redirect him to LoginActivity).
            navigateToStartScreen();
        }
    }

    /**
     * Get stored session data.
     */
    public HashMap<String, String> getCurrentUser() {
        HashMap<String, String> user = new HashMap<>();

        // User email
        user.put(KEY_EMAIL, sharedPreferences.getString(KEY_EMAIL, null));

        // User first name
        user.put(Constants.USER_FIRST_NAME, sharedPreferences.getString(Constants.USER_FIRST_NAME, null));

        // User last name
        user.put(Constants.USER_LAST_NAME, sharedPreferences.getString(Constants.USER_LAST_NAME, null));

        // User phone number
        user.put(Constants.USER_PHONE, sharedPreferences.getString(Constants.USER_PHONE, null));

        // User most recent employer
        user.put(Constants.USER_EMP, sharedPreferences.getString(Constants.USER_COMPANY_ID, null));

        // User current company ID
        user.put(Constants.USER_COMPANY_ID, sharedPreferences.getString(Constants.USER_COMPANY_ID, null));

        // User most recent employer name
        user.put(Constants.USER_COMPANY, sharedPreferences.getString(Constants.USER_COMPANY, null));

        user.put(Constants.AUTH_TOKEN, sharedPreferences.getString(Constants.AUTH_TOKEN, null));

        // Return the user.
        return user;
    }

    public void setGeofenceIds(ArrayList<String> geofenceIdsList) {
        String[] idsList = geofenceIdsList.toArray(new String[geofenceIdsList.size()]);
        // the comma like character used below is not a comma it is the SINGLE
        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
        // separating the items in the list
        editor.putString(Constants.USER_SITE_IDS, TextUtils.join("‚‗‚", idsList));
        editor.commit();
    }

    public ArrayList<String> getGeofenceIds() {
        // the comma like character used below is not a comma it is the SINGLE
        // LOW-9 QUOTATION MARK unicode 201A and unicode 2017 they are used for
        // seprating the items in the list
        String[] idsList = TextUtils.split(sharedPreferences.getString(Constants.USER_SITE_IDS, ""), "‚‗‚");
        ArrayList<String> geofenceIdsList = new ArrayList<>(Arrays.asList(idsList));
        return geofenceIdsList;
    }

    public HashMap<String, String> getRegistrationDetails() {
        HashMap<String, String> details = new HashMap<>();

        details.put(Constants.REGISTER_CARD_NUMBER, sharedPreferences.getString(Constants.REGISTER_CARD_NUMBER, null));
        details.put(Constants.REGISTER_CARD_DATE, sharedPreferences.getString(Constants.REGISTER_CARD_DATE, null));
        details.put(Constants.REGISTER_STATE, sharedPreferences.getString(Constants.REGISTER_STATE, null));
        details.put(Constants.REGISTER_PASS, sharedPreferences.getString(KEY_PASS, null));
        details.put(Constants.REGISTER_LOCALE, sharedPreferences.getString(Constants.REGISTER_LOCALE, null));
        return details;
    }

    public int getSiteId() {
        return sharedPreferences.getInt(SITE_ID, 0);
    }

    public void setSiteId(int siteId) {
        editor.putInt(SITE_ID, siteId);
        editor.apply();
    }

    public void setCompanyId(int companyId) {
        sharedPreferences.edit().putInt(Constants.CONTEXT_COMPANY_ID, companyId).apply();
    }

    public int getCompanyId() {
        return sharedPreferences.getInt(Constants.CONTEXT_COMPANY_ID, 0);
    }

    public void setCompanyName(String companyName) {
        sharedPreferences.edit().putString(Constants.USER_COMPANY, companyName).apply();
    }

    public String getCompanyName() {
        return sharedPreferences.getString(Constants.USER_COMPANY, "");
    }


    public NearSite getNearSite() {
        String json = sharedPreferences.getString(NEAR_SITE, "");
        return new Gson().fromJson(json, NearSite.class);
    }

    public void setNearSite(NearSite site) {
        editor.putString(NEAR_SITE, new Gson().toJson(site));
        editor.commit();
    }


    public void clearSiteId() {
        editor.remove(SITE_ID);
        editor.commit();
    }

    public void setSignOnTime() {
        // Get time details
        Calendar calendar = Calendar.getInstance();
        Log.i(TAG, calendar.toString());
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        Log.i(TAG, "hours: " + hours);
        int mins = calendar.get(Calendar.MINUTE);
        Log.i(TAG, "mins: " + mins);
        String minute;
        String hour;
        String meridiem;
        if (hours < 12) {
            // Hours > 12 ensures that 12pm does not become '00:mins pm'
            meridiem = "am";
            if (hours == 0) {
                hour = "00";
            } else {
                hour = String.valueOf(hours);
            }
        } else {
            meridiem = "pm";

            if (hours - 12 == 0) {
                hour = "12";
            } else {
                hour = String.valueOf(hours - 12);
            }
        }

        // if mins are less than 10, they display "6" instead of "06"... "11:6am" looks funny...
        if (mins < 10) {
            minute = "0" + String.valueOf(mins);
        } else {
            minute = String.valueOf(mins);
        }

        // (1) get today's date
        Date today = Calendar.getInstance().getTime();

        // (2) create a date "formatter" (the date format we want)
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yy");
        String date = formatter.format(today);

        String time = "Signed on at " + hour + ":" + minute + meridiem;

        editor.putString(Constants.SIGN_ON_TIME, time);
        editor.putString(Constants.SIGN_ON_DATE, date);
        editor.commit();
    }

    public HashMap<String, String> getSignOnTime() {
        HashMap<String, String> timeDetails = new HashMap<>();
        timeDetails.put(Constants.SIGN_ON_TIME, sharedPreferences.getString(Constants.SIGN_ON_TIME, null));
        timeDetails.put(Constants.SIGN_ON_DATE, sharedPreferences.getString(Constants.SIGN_ON_DATE, null));

        return timeDetails;
    }

    public void setPlayStoreVersion(int buildCode) {
        editor.putInt(Constants.APP_VERSION_CODE, buildCode);
        editor.commit();
    }

    public int getPlayStoreVersion() {
        return sharedPreferences.getInt(Constants.APP_VERSION_CODE, 0);
    }

    public boolean getAutoSignOnEnabled() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        return settings.getBoolean(Constants.SETTINGS_AUTO_SIGNON, true);
    }

    public boolean diagnosticsIsEnabled() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        return settings.getBoolean(Constants.SETTINGS_DIAGNOSTICS, false);
    }

    public void setDiagnosticsOperation(boolean enabled) {
        editor.putBoolean(Constants.SETTINGS_DIAGNOSTICS, enabled);
        editor.commit();
    }

    public String getTempUserEmail() {
        return sharedPreferences.getString(Constants.USER_EMAIL, "");
    }

    /**
     * Updates the current user details
     */
    public void updateUserDetail(String field, String updatedValue) {
        editor.putString(field, updatedValue);
        editor.commit();
    }

    /**
     * Stores register password
     */
    public void updateRegisterPass(String pass) {
        editor.putString(KEY_PASS, pass);
        editor.commit();
    }

    /**
     * Deletes register password
     */
    public void deleteRegisterPass() {
        editor.remove(KEY_PASS);
        editor.commit();
    }

    /**
     * Logout the current user and clear session details.
     */
    public void logoutUser() {
        // Clearing all the data from SharedPreferences.
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login status
     */
    public boolean isLoggedIn() {
        boolean loggedInFlag = sharedPreferences.getBoolean(IS_LOGIN, false);
        String token = sharedPreferences.getString(Constants.AUTH_TOKEN, null);
        boolean hasToken = false;
        if (token != null && !token.isEmpty()) {
            hasToken = true;
        }

        return (loggedInFlag && hasToken);
    }

    public boolean isEmergency() {
        return sharedPreferences.getBoolean(IS_EMERGENCY, false);
    }

    public void setEmergencyStatus(boolean status) {
        editor.putBoolean(IS_EMERGENCY, status);
        editor.commit();
    }

    /**
     * Quick method to navigate to LoginActivity.
     */
    public void navigateToStartScreen() {
        Intent intent = new Intent(mContext, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    /**
     * This method gets the value for isEditingInPersonalInfoPassport
     */
    public Boolean getEditingInPersonalInfoPassport() {
        return isEditingInPersonalInfoPassport;
    }

    /**
     * This method sets the value for isEditingInPersonalInfoPassport
     */
    public void setEditingInPersonalInfoPassport(Boolean editingInPersonalInfoPassport) {
        isEditingInPersonalInfoPassport = editingInPersonalInfoPassport;
    }

    /**
     * This method gets the value for isEditingInEmergencyPassport
     */
    public Boolean getEditingInEmergencyPassport() {
        return isEditingInEmergencyPassport;
    }

    /**
     * This method sets the value for isEditingInEmergencyPassport
     */
    public void setEditingInEmergencyPassport(Boolean editingInEmergencyPassport) {
        isEditingInEmergencyPassport = editingInEmergencyPassport;
    }

    public Boolean getEditingFrontPhoto() {
        return isEditingFrontPhoto;
    }

    public void setEditingFrontPhoto(Boolean editingFrontPhoto) {
        isEditingFrontPhoto = editingFrontPhoto;
    }

    public Boolean getEditingBackPhoto() {
        return isEditingBackPhoto;
    }

    public void setEditingBackPhoto(Boolean editingBackPhoto) {
        isEditingBackPhoto = editingBackPhoto;
    }

    public Boolean getHasUserPassport() {
        return sharedPreferences.getBoolean(HAS_PASSPORT, false);
    }

    public void setHasUserPassport(Boolean hasUserPassport) {
        editor.putBoolean(HAS_PASSPORT, hasUserPassport).apply();
    }
}
