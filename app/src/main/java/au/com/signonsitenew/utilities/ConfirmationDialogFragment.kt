package au.com.signonsitenew.utilities

import android.content.Context
import androidx.fragment.app.DialogFragment
import java.lang.Exception


class ConfirmationDialogFragment : DialogFragment() {
    // Use this instance of the interface to deliver action events
    internal lateinit var mListener: ConfirmationDialogListener

    /** The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    interface ConfirmationDialogListener {
        fun onDialogPositiveClick(dialog: DialogFragment)
    }

    // Override the Fragment.onAttach() method to instantiate the ConfirmationDialogListener
    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ConfirmationDialogListener so we can send events to the host
            mListener = context as ConfirmationDialogListener
        } catch (e: Exception) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException((context.toString() +
                    " must implement ConfirmationDialogListener"))
        }
    }
}