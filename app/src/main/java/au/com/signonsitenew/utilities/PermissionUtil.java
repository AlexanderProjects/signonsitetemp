package au.com.signonsitenew.utilities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;

import au.com.signonsitenew.ui.main.MainActivity;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Krishan Caldwell on 7/07/2016.
 */
public class PermissionUtil {

    public static boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked
        if (grantResults.length < 1) {
            return false;
        }

        if(grantResults.length < 3)
            if(grantResults.length == 2)
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                    return true;
                else
                    return false;
            if(grantResults.length == 1)
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    return true;
                else
                    return false;

        if(grantResults.length == 3)
            if(grantResults[2] == PackageManager.PERMISSION_DENIED && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                return true;

        // Verify that each required permission has been granted, otherwise return false
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    public static boolean verifyBackgroundPermissions(int[] grantResults, String[] permissions){
        if(permissions[0].equals(Constants.BACKGROUND_LOCATION) && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }

    public static boolean hasLocationPermissions(DaggerAppCompatActivity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }else{
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
              return true;

            } else {
                return false;
            }
        }
    }

}
