package au.com.signonsitenew.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import au.com.signonsitenew.R;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class NetworkUtil {

    /**
     * Tests for network connectivity; returning true if available, false if not.
     */
    public static boolean networkIsConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static void displayNoNetworkToast(Context context) {
        DarkToast.makeText(context, context.getResources().getString(R.string.error_no_internet));
    }

    public static String determineCountryCode(Context context) {
        TelephonyManager tm = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();

        if (countryCodeValue != null) {
            Log.i("Country Code", countryCodeValue);
            return countryCodeValue;
        }
        else {
            SLog.e("TelephonyManager", "Unable to determine alpha2");

            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("error", "cannot determine alpha2");
            }
            catch (JSONException e) {
                SLog.e("NetworkUtil", "A JSON Exception occurred");
            }
            DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.ALPHA_2_ERROR, null, jsonData.toString());
            return "";
        }
    }

}
