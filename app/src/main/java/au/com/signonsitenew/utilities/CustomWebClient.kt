package au.com.signonsitenew.utilities

import android.webkit.WebView
import android.webkit.WebViewClient

class CustomWebClient(val setContent:(webView: WebView?)->Unit): WebViewClient() {
    override fun onPageFinished(view: WebView?, url: String?) = setContent(view)
}