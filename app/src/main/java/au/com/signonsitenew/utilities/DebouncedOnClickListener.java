package au.com.signonsitenew.utilities;

import android.os.SystemClock;
import android.view.View;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by Krishan Caldwell on 22/07/2016.
 *
 * A Debounced OnClickListener
 * Rejects clicks that are too close together in time.
 * This class is safe to use as an OnClickListener for multiple views, and will debounce each one separately.
 */
public abstract class DebouncedOnClickListener implements View.OnClickListener {

    private final long mMinInterval;
    private Map<View, Long> mLastClickMap;

    /**
     * Implement this in your subclass instead of onClick
     * @param v The view that was clicked
     */
    public abstract void onDebouncedClick(View v);

    /**
     * Default constructor that initiates a 300ms debounce delay
     */
    public DebouncedOnClickListener() {
        mMinInterval = 300; // 300 milliseconds
        mLastClickMap = new WeakHashMap<>();
    }

    /**
     * Default constructor that allows for a minimum interval time to be specified.
     * This could be used if a certain animation were to be played between events potentially.
     * @param minInterval The minimum allowed time between clicks - any click sooner than this after a previous click will be rejected
     */
    public DebouncedOnClickListener(long minInterval) {
        mMinInterval = minInterval;
        mLastClickMap = new WeakHashMap<>();
    }

    @Override public void onClick(View clickedView) {
        Long previousClickTimestamp = mLastClickMap.get(clickedView);
        long currentTimestamp = SystemClock.uptimeMillis();

        mLastClickMap.put(clickedView, currentTimestamp);
        if(previousClickTimestamp == null || (currentTimestamp - previousClickTimestamp > mMinInterval)) {
            onDebouncedClick(clickedView);
        }
    }
}