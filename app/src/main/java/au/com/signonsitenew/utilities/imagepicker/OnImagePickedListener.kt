package au.com.signonsitenew.utilities.imagepicker

import android.net.Uri

interface OnImagePickedListener {
    fun onImagePicked(imageUri: Uri?)
}