package au.com.signonsitenew.utilities;


import android.util.Log;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Helper class that allows for simple changes to be implemented to logging.
 */

public class SLog {
    public static void d(String tag, String message) {
//        Bugfender.d(tag, message);
        Log.d(tag, message);
    }
    public static void e(String tag, String message) {
//        Bugfender.e(tag, message);
        Log.e(tag, message);
    }
    public static void i(String tag, String message) {
//        Bugfender.d(tag, message);
        Log.i(tag, message);
    }
    public static void v(String tag, String message) {
//        Bugfender.d(tag, message);
        Log.v(tag, message);
    }
    public static void w(String tag, String message) {
//        Bugfender.w(tag, message);
        Log.w(tag, message);
    }
    public static void wtf(String tag, String message) {
//        Bugfender.e(tag, message);
        Log.wtf(tag, message);
    }
}


