package au.com.signonsitenew.utilities

import androidx.appcompat.widget.SearchView

class QueryTextListener(private val addData: (String)->Unit) : SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(s: String): Boolean {
        return false
    }

    override fun onQueryTextChange(text: String): Boolean {
        if (text != null) addData(text)
        return true
    }

}