package au.com.signonsitenew.utilities;

import android.content.Context;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Contains helpers for validation to ensure consistent validation checks throughout the app.
 */

public class Validation {

    public static boolean validateName(Context context, String name) {
        if (name == null || name.isEmpty()) {
            DarkToast.makeText(context, "Please enter both a first and last name");
            return false;
        }
        else if (!onlyAsciiLetters(name)) {
            DarkToast.makeText(context, "Please make sure the workers name only contains letters, hyphens or apostrophes");
            return false;
        }

        return true;
    }

    /**
     * Checks to see if a String is in a phone number format. This can include standard forms, or
     * international forms including a single '+'
     * @param phoneNumber input phone number
     * @return true or false
     */
    public static boolean validPhoneNumber(Context context, String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            DarkToast.makeText(context, "Please enter a phone number");
            return false;
        }
        else if (phoneNumber.length() < 8) {
            DarkToast.makeText(context, "It appears that your phone number is too short. " +
                    "Please make sure your mobile number has at least 10 numbers.");
            return false;
        }
        else if (phoneNumber.length() >= 13) {
            DarkToast.makeText(context, "It appears that your phone number is too long. " +
                    "Please make sure your mobile number is less than 13 numbers.");
            return false;
        }
        else if (!phoneNumber.matches("^[+]?[0-9]{8,13}$")) {
            DarkToast.makeText(context, "Please enter a valid phone number");
            return false;
        }

        return true;
    }

    /**
     * Checks if the input only contains letters, dashes or hyphens. The limitation is that this only
     * works for standard Roman Numerals. Not for accented letters or non-latin based alphabets.
     * @param string input string
     * @return true or false
     */
    private static boolean onlyAsciiLetters(String string) {
        return string.matches("[a-zA-Z' -]+");
    }
}
