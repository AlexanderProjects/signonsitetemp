package au.com.signonsitenew.utilities

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.EditText

class EditTextWatcher : TextWatcher {
    private var callAction: CallAction? = null
    private var showNotProvided: CallAction? = null
    private var clearActions: CallAction? = null
    private var addAction: AddAction? = null
    private var editText:EditText? = null
    private lateinit var textBeforeChange:String
    private var isEditing = false

    constructor(editText: EditText,callAction: CallAction?) {
        this.callAction = callAction
        this.editText = editText
        editText.requestFocus()
        registerListener()
    }

    constructor(editText: EditText,addAction: AddAction?) {
        this.addAction = addAction
        this.editText = editText
        editText.requestFocus()
        registerListener()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        textBeforeChange = s.toString()
    }
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        if (callAction != null && isEditing && s.toString() != textBeforeChange) {
            callAction?.call()
            isEditing = false
        }
        if (s.toString().isEmpty() && showNotProvided != null)
            showNotProvided!!.call()
        if (s.toString().isNotEmpty() && showNotProvided != null && clearActions != null)
            clearActions!!.call()
        if (addAction != null && editText!!.hasFocus() && s.toString() != textBeforeChange) {
            addAction!!.call(s.toString())
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun registerListener(){
        editText?.setOnTouchListener { v: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                v.performClick()
                isEditing = true
            }
            false
        }
    }

    override fun afterTextChanged(s: Editable) {}
    interface CallAction {
        fun call()
    }

    interface AddAction {
        fun call(text: String?)
    }
}