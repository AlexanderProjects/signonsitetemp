package au.com.signonsitenew.utilities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.attendanceregister.UserDetailFragment


class ProgressViewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_progress_view, container, false)
        val progressView = view.findViewById<ImageView>(R.id.progress_spinner_image_view)
        val progressTexView = view.findViewById<TextView>(R.id.progress_action_text_view)
        val progressInformation = view.findViewById<TextView>(R.id.progress_information_text_view)
        val animation = AnimationUtils.loadAnimation(context, R.anim.rotate_around_center)
        if (arguments != null) {
            if(!requireArguments().getBoolean(Constants.PROGRESS_VIEW_HIDE_ACTION_BAR)){
                (activity as AppCompatActivity?)!!.supportActionBar!!.show()
                progressTexView.text = Constants.LOADING_PLAIN_TITLE
                progressInformation.visibility = View.GONE
            }else{
                (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
                progressTexView.text = requireArguments().getString(Constants.LOADING_TITLE_KEY)
                progressInformation.text = requireArguments().getString(Constants.LOADING_MESSAGE_KEY)
            }
        }else{
            progressTexView.text = Constants.LOADING_PLAIN_TITLE
            progressInformation.visibility = View.GONE
        }

        progressView.startAnimation(animation)
        progressView.visibility = View.VISIBLE
        return view
    }

    companion object {
        fun newInstance() = ProgressViewFragment()
    }
}
