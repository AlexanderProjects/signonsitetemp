package au.com.signonsitenew.utilities.phonepicker

import au.com.signonsitenew.domain.models.PhoneNumber

interface PhoneContactSelectedListener {
    fun phoneNumberValidated(phoneNumber: PhoneNumber?)
}