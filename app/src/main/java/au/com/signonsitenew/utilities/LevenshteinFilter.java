package au.com.signonsitenew.utilities;

import android.util.Log;
import android.widget.Filter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * A filter that orders the results list by the item's respective Levenshtein Distance relative to
 * an input search term.
 */

public class LevenshteinFilter extends Filter {
    private static final String LOG = LevenshteinFilter.class.getSimpleName();
    private ArrayList<String> mCompanyNames;
    private ArrayList<Company> mResults;
    private int mThreshold;

    public LevenshteinFilter(List<String> companies) {
        mCompanyNames = new ArrayList();
        synchronized (this) {
            mCompanyNames.addAll(companies);
        }
    }

    public LevenshteinFilter(List<String> companies, int distanceThreshold) {
        mThreshold = distanceThreshold;
        mCompanyNames = new ArrayList();
        synchronized (this) {
            mCompanyNames.addAll(companies);
        }
    }

    /**
     * Using the constraint, calculates out the Levenshtein distance for all items in the list.
     * Sorts items in the list by their Levenshtein score.
     * @param constraint input from the search bar
     * @return
     */
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        String filterSequence = constraint.toString();
        FilterResults results = new FilterResults();

        if (!filterSequence.isEmpty()) {
            // Assign Levenshtein distances and order list by distances
            mResults = assignLevenshteinScore(filterSequence);
        }

        // Strip the company names out
        for (Company company : mResults) {
            results.values = company.name;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

    }

    /**
     * Depending on if a threshold is set, this function will either check the Levenshtein distance
     * for every item in mCompanyNames and returns the same items ordered by their respective distances,
     * else it will check all items, and return all items that are above the threshold distance.
     * @return
     */
    private ArrayList<Company> assignLevenshteinScore(String filterString) {
        ArrayList<Company> results = new ArrayList<>(mCompanyNames.size());

        for (Object companyName : mCompanyNames) {
            int levenshteinDistance = StringUtils.getLevenshteinDistance(companyName.toString(), filterString);
            Company company = new Company();
            company.name = companyName.toString();
            company.levenshteinDistance = levenshteinDistance;
            results.add(company);
        }

        mResults = results;
        Log.i(LOG, mResults.subList(0, 10).toString());
        // Sort the results array
        Collections.sort(mResults, new CompanyLevenshteinDistanceComparator());
        Log.i(LOG, mResults.subList(0, 10).toString());

        return mResults;
    }

    private class Company {
        public String name;
        public int levenshteinDistance;
    }

    private class CompanyLevenshteinDistanceComparator implements Comparator<Company> {
        @Override
        public int compare(Company firstCompany, Company secondCompany) {
            if (firstCompany.levenshteinDistance > secondCompany.levenshteinDistance) {
                return +1;
            }
            else if (firstCompany.levenshteinDistance < secondCompany.levenshteinDistance) {
                return -1;
            }
            else {
                return 0;
            }
        }
    }
}
