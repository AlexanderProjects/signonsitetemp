package au.com.signonsitenew.utilities.phonepicker

interface PhonePickerView {
    fun goBack()
    fun showValidationError()
    fun showDataError(errorMessage:String)
}