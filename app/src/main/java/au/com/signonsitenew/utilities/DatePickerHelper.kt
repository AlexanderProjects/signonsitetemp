package au.com.signonsitenew.utilities

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.widget.DatePicker
import androidx.fragment.app.FragmentActivity
import java.util.*

object DatePickerHelper {
    fun datePicker(activity: FragmentActivity, datePicker: (String) -> Unit, updateFields: (String) -> Unit) {
        val currentDate = Calendar.getInstance()
        val year = currentDate[Calendar.YEAR]
        val month = currentDate[Calendar.MONTH]
        val day = currentDate[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(activity, OnDateSetListener { _: DatePicker?, year1: Int, month1: Int, dayOfMonth: Int ->
            val dateSelected = "${checkDigit(dayOfMonth)}-${checkDigit(month1 + 1)}-$year1"
            datePicker("$year1-${checkDigit( month1 + 1)}-${checkDigit(dayOfMonth)}")
            updateFields(dateSelected)
        }, year, month, day)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.setTitle(Constants.SELECT_DATE_OF_BIRTH)
        datePickerDialog.show()
    }

    @SuppressLint("DefaultLocale")
    fun datePickerYYMMDD(context: Context, callAction: (String) -> Unit) {
        val currentDate = Calendar.getInstance()
        val year = currentDate[Calendar.YEAR]
        val month = currentDate[Calendar.MONTH]
        val day = currentDate[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(context, OnDateSetListener { _: DatePicker?, year1: Int, month1: Int, dayOfMonth: Int ->
            val dateSelected = "$year1-${checkDigit( month1 + 1)}-${checkDigit(dayOfMonth)}"
            callAction(dateSelected)
        }, year, month, day)
        datePickerDialog.show()
    }

    private fun checkDigit(number: Int): String? {
        return if (number <= 9) "0$number" else number.toString()
    }

}