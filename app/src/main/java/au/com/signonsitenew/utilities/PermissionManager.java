package au.com.signonsitenew.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.activity.result.ActivityResultLauncher;
import androidx.core.app.ActivityCompat;
import static au.com.signonsitenew.utilities.Constants.PERMISSIONS_STORAGE;
import static au.com.signonsitenew.utilities.Constants.REQUEST_CAMERA;
import static au.com.signonsitenew.utilities.Constants.REQUEST_STORAGE_ACCESS;

public class PermissionManager {

    private static String[] locationPermissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private static String[] locationPermissionsAndroidQ ={Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION};
    private static int requestLocation = 1;


    public static void checkForCameraPermissions(Activity activity, CallAction callAction){
        if ((ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED))) {
            AlertDialogMessageHelper.requestCameraPermissionAlert(activity,()->ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_CAMERA));
        }else if ((ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            AlertDialogMessageHelper.requestStoragePermissionAlert(activity, () -> ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_STORAGE_ACCESS));
        }else {
            callAction.call();
        }
    }

    public static  void checkForStoragePermissions(Activity activity){
        if ((ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            AlertDialogMessageHelper.requestStoragePermissionAlert(activity, () -> ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_STORAGE_ACCESS));
        }
    }

    public static void checkForCameraPermissionsInPassportProfilePhoto(Activity activity, ActivityResultLauncher<String[]> checkLocationPermission, CallAction callAction){
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            checkLocationPermission.launch(new String[]{ Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE });
        }else{
            callAction.call();
        }
    }

    public static Boolean checkForAndroidQAbovePermission(Activity activity){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }

    public static void requestLocationPermissionInAndroidQAbove(Activity activity, int[] grantResults){
        String[] backgroundLocationPermissions = {Manifest.permission.ACCESS_BACKGROUND_LOCATION};
        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.Q && !areLocationPermissionsResultDeniedForAndroidQ(grantResults)){
            if(!checkForAndroidQAbovePermission(activity)) {
                ActivityCompat.requestPermissions(activity, locationPermissionsAndroidQ, requestLocation);
            }
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && !areLocationPermissionsResultDeniedForAndroidR(grantResults)) {
                ActivityCompat.requestPermissions(activity, locationPermissions, requestLocation);
            }
            else if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED && !areLocationPermissionsResultDeniedForAndroidR(grantResults)){
                ActivityCompat.requestPermissions(activity, backgroundLocationPermissions, requestLocation);
            }
        }
    }
    private static boolean areLocationPermissionsResultDeniedForAndroidQ(int[] grantResults){
        return grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED && grantResults[2] == PackageManager.PERMISSION_DENIED;
    }

    private static boolean areLocationPermissionsResultDeniedForAndroidR(int[] grantResults){
        return grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED;
    }


    public interface CallAction{
        void call();
    }
}
