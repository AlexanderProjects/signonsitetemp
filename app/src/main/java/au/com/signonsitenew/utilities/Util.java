package au.com.signonsitenew.utilities;

import android.text.Annotation;
import android.text.InputFilter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonQualifier;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.annotation.Retention;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


public class Util {

    public static <T> List<T> filter(Collection<T> target, IPredicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T element: target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public interface IPredicate<T> { boolean apply(T type); }


    public static InputFilter[] emojiFilter()
    {
        InputFilter EMOJI_FILTER = (source, start, end, dest, dstart, dend) -> {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || type==Character.NON_SPACING_MARK
                        || type==Character.OTHER_SYMBOL) {
                    return Constants.NOT_PROVIDED_HINT;
                }
            }
            return null;
        };
        return new InputFilter[]{EMOJI_FILTER};
    }
    @Retention(RUNTIME)
    @JsonQualifier
    public @interface SerializeNulls {
        JsonAdapter.Factory JSON_ADAPTER_FACTORY = new JsonAdapter.Factory() {
            @javax.annotation.Nullable
            @Override
            public JsonAdapter<?> create(Type type, Set<? extends java.lang.annotation.Annotation> annotations, Moshi moshi) {
                Set<? extends Annotation> nextAnnotations =
                        (Set<? extends Annotation>) Types.nextAnnotations(annotations, SerializeNulls.class);
                if (nextAnnotations == null) {
                    return null;
                }
                return moshi.nextAdapter(this, type, (Set<? extends java.lang.annotation.Annotation>) nextAnnotations).serializeNulls();
            }
        };
    }
}
