package au.com.signonsitenew.utilities

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.media.AudioManager
import android.media.RingtoneManager
import android.net.Uri
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.constraintlayout.widget.Group
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.databinding.Observable
import au.com.signonsitenew.R
import au.com.signonsitenew.data.factory.datasources.notifications.remote.RemoteNotificationService
import au.com.signonsitenew.domain.models.ComponentTypeForm
import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.UpdatePermitInfoRequest
import com.google.gson.Gson
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException



inline fun <reified T : Observable> T.addOnPropertyChanged(crossinline callback: (T) -> Unit): Disposable =
            object :Observable.OnPropertyChangedCallback(){
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) =
                    callback(sender as T)
            }.also { addOnPropertyChangedCallback(it) }.let { Disposables.fromAction{removeOnPropertyChangedCallback(it)} }

fun String.empty(): String = ""
fun String.emptySpace():  String = " "


fun <T> Single<T>.mapNetworkErrors(): Single<T> =
        this.onErrorResumeNext{ error -> when (error) {
            is SocketTimeoutException -> Single.error(NoNetworkException(error))
            is UnknownHostException -> Single.error(ServerUnreachableException(error))
            is HttpException -> Single.error(HttpCallFailureException(error))
            else -> Single.error(error)
        }
}


fun <T> Maybe<T>.mapNetworkErrors(): Maybe<T> =
        this.onErrorResumeNext{ error: Throwable ->
        when (error) {
            is SocketTimeoutException -> Maybe.error(NoNetworkException(error))
            is UnknownHostException -> Maybe.error(ServerUnreachableException(error))
            is HttpException -> Maybe.error(HttpCallFailureException(error))
            else -> Maybe.error(error)
        }
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.onDrawableEndClick(action: () -> Unit) {
    setOnTouchListener { v, event ->
        if (event.action == MotionEvent.ACTION_UP) {
            v as EditText
            val end = if (v.resources.configuration.layoutDirection == View.LAYOUT_DIRECTION_RTL)
                v.left else v.right
            if (event.rawX >= (end - v.compoundPaddingEnd)) {
                action.invoke()
                return@setOnTouchListener true
            }
        }
        return@setOnTouchListener false
    }
}

fun buildNotification(
    context: Context,
    message: String,
    contentIntent: PendingIntent,
    notificationId: Int,
    channelId: String
){
    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val mBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.logo_white)
            .setContentText(message)
            .setContentIntent(contentIntent)
            .setAutoCancel(false)
            .setColor(ContextCompat.getColor(context, R.color.orange_primary))
            .setLights(Color.YELLOW, 1000, 1000)
            .setVibrate(longArrayOf(250, 250, 250, 250))
            .setSound(alertSound)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )

    val notification = mBuilder.build()
    notificationManager.notify(notificationId, notification)
}

fun buildEmergencyNotification(
    context: Context,
    message: String,
    contentIntent: PendingIntent,
    notificationId: Int,
    channelId: String
){

    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val alarmSound = Uri.parse(
        "android.resource://"
                + context.packageName + "/" + R.raw.siren
    )

    val mBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.logo_white)
            .setContentTitle(context.getString(R.string.title_evacuation))
            .setContentText(message)
            .setColor(ContextCompat.getColor(context, R.color.emergency_primary))
            .setLights(Color.RED, 250, 250) // fast flash, long vibrate
            .setVibrate(longArrayOf(100, 1000, 100, 1000, 100, 1000, 100, 1000, 100, 1000))
            .setSound(alarmSound)
            .setContentIntent(contentIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )

    val notification = mBuilder.build()

    // Override the user's sound settings to force alarm to play at max even if phone on silent
    try {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION)
        audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
        audioManager.setStreamVolume(
            AudioManager.STREAM_NOTIFICATION,
            maxVol,
            AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
        )
    } catch (e: SecurityException) {
        SLog.d(
            RemoteNotificationService::class.java.name,
            "Security Exception occurred: " + e.message
        )
    } catch (e: java.lang.Exception) {
        SLog.e(
            RemoteNotificationService::class.java.name,
            "An unexpected exception occurred: " + e.message
        )
    }

    notificationManager.notify(notificationId, notification)
}

fun buildNotificationWithGroupKey(
    context: Context,
    message: String,
    contentIntent: PendingIntent,
    notificationId: Int,
    channelId: String,
    groupKey: String
){
    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val mBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.logo_white)
            .setContentText(message)
            .setContentIntent(contentIntent)
            .setGroup(groupKey)
            .setAutoCancel(false)
            .setColor(ContextCompat.getColor(context, R.color.orange_primary))
            .setLights(Color.YELLOW, 1000, 1000)
            .setVibrate(longArrayOf(250, 250, 250, 250))
            .setSound(alertSound)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(message)
            )

    val notification = mBuilder.build()
    notificationManager.notify(notificationId, notification)
}

fun <T> merge(first: List<T>, second: List<T>): List<T> {
    return object : ArrayList<T>() {
        init {
            addAll(first)
            addAll(second)
        }
    }
}

fun toJson(value: Any):String = Gson().toJson(value)

fun String.deleteSpacesAndSlash():String = this.replace("\\s".toRegex(), String().empty()).replace(
    "/".toRegex(),
    String().empty()
)

fun <T> applySchedulers(): SingleTransformer<T, T> {
    return SingleTransformer <T, T> { observable ->
        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}

inline fun<reified T> isEqual(first: List<T>, second: List<T>): Boolean {

    if (first.size != second.size) {
        return false
    }

    return first.toTypedArray() contentEquals second.toTypedArray()
}

inline fun<reified T> contains(first: List<T>, second: List<T>): Boolean {
    return first.toTypedArray() contentEquals second.toTypedArray()
}

fun Group.setAllOnClickListener(listener: View.OnClickListener?) {
    referencedIds.forEach { id ->
        rootView.findViewById<View>(id).setOnClickListener(listener)
    }
}

fun View.setAllEnabled(enabled: Boolean) {
    isEnabled = enabled
    if (this is ViewGroup) children.forEach { child -> child.setAllEnabled(enabled) }
}

fun List<ComponentTypeForm>.containElements(other: List<ContentTypeItem>) = any { p -> other.any { it.id == p.form_input_id && it.responses.isNotEmpty()} }

fun List<ContentTypeItem>.filterMandatoryFieldsById(componentTypeFormList: List<ComponentTypeForm>) = filter { p -> componentTypeFormList.any { it.form_input_id == p.id }}

fun List<ContentTypeItem>.filterByMandatoryField() = filter { p -> p.is_mandatory && p.responses.isEmpty() }

inline fun <T: Any> Collection<T>.emptyList(emptyCallback:()->Unit) { if(isEmpty()) emptyCallback() }

inline fun <T: Any> Collection<T>.notEmptyList(notEmptyCallback:(Collection<T>)->Unit) { if(isNotEmpty()) notEmptyCallback(this) }

fun isKeyboardVisible(rootView: View):Boolean{
    val visibleBounds = Rect()
    rootView.getWindowVisibleDisplayFrame(visibleBounds)
    val heightDiff = rootView.height - visibleBounds.height()
    return heightDiff > 0
}






