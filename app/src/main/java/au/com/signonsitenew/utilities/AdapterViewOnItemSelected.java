package au.com.signonsitenew.utilities;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

public class AdapterViewOnItemSelected implements AdapterView.OnItemSelectedListener {

    private CallAction actionVisible;
    private CallAction actionInvisible;
    private CallAction actionFlag;
    private Spinner spinner;

    public AdapterViewOnItemSelected(Spinner spinner,CallAction actionVisible, CallAction actionInvisible, CallAction actionFlag) {
        this.actionVisible = actionVisible;
        this.actionInvisible = actionInvisible;
        this.actionFlag = actionFlag;
        this.spinner = spinner;
        this.spinner.setFocusableInTouchMode(true);
        this.spinner.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP && !v.hasFocus()) {
                v.performClick();
            }
            return false;
        });
    }

    public AdapterViewOnItemSelected(Spinner spinner,CallAction actionFlag){
        this.actionFlag = actionFlag;
        this.spinner = spinner;
        this.spinner.setFocusableInTouchMode(true);
        this.spinner.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP && !v.hasFocus()) {
                v.performClick();
            }
            return false;
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
       if(actionInvisible != null && actionVisible != null) {
           if (position == 4)
               actionVisible.call();
           else
               actionInvisible.call();
       }
       if(actionFlag != null && position != 0 && spinner.hasFocus()) {
           actionFlag.call();
       }
       spinner.clearFocus();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }

    public interface CallAction{
        void call();
    }
}
