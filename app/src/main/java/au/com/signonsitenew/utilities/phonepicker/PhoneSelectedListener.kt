package au.com.signonsitenew.utilities.phonepicker

import au.com.signonsitenew.domain.models.PhoneNumber

interface PhoneSelectedListener {
    fun phoneNumberValidated(phoneNumber: PhoneNumber?)
}