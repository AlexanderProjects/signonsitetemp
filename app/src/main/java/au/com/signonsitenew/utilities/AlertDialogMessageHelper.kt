package au.com.signonsitenew.utilities

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.Gravity
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.NearSite
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object AlertDialogMessageHelper {
    fun messageForInfoIcon(context: Context?, message: String?, title: String?) {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        }?.show()
    }

    fun messageValidationForm(context: Context?) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(Constants.FIELDS_VALIDATION_ALERT_MESSAGE)
                .setTitle(Constants.FIELDS_VALIDATION_ALERT_TITLE)
        builder.setPositiveButton(Constants.FIELDS_VALIDATION_ALERT_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    inline fun networkErrorMessage(context: Context?, message: String?, title: String?, crossinline action: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message).setTitle(title)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.setNegativeButton(Constants.RETRY) { dialog: DialogInterface, _: Int ->
            action()
            dialog.dismiss()
        }
        builder.show()
    }

    inline fun permitErrorMessage(context: Context, crossinline action: () -> Unit) {
        val builder = MaterialAlertDialogBuilder(context!!)
                .setMessage(context.getString(R.string.permit_error_message))
                 .setTitle(context.getString(R.string.permit_error_title))
                .setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
                .setNegativeButton(Constants.RETRY) { dialog: DialogInterface, _: Int ->
            action()
            dialog.dismiss()
        }
        builder.show()
    }

    fun rateLimitErrorMessage(context: Context?, message: String?, title: String?){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message).setTitle(title)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    @JvmStatic
    fun dataErrorMessage(context: Context, message: String){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
        builder.setTitle(context.resources.getString(R.string.error_generic_title))
        builder.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    @JvmStatic
    fun networkErrorMessage(context: Context){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(Constants.NETWORK_MESSAGE_ERROR)
        builder.setTitle(Constants.NETWORK_MESSAGE_TITLE)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    @JvmStatic
    inline fun networkErrorMessage(context: Context, crossinline action: () -> Unit){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(Constants.NETWORK_MESSAGE_ERROR)
        builder.setTitle(Constants.NETWORK_MESSAGE_TITLE)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.setNegativeButton(Constants.RETRY) { dialog: DialogInterface, _: Int ->
            action()
            dialog.dismiss()
        }
        builder.show()
    }

    @JvmStatic
    inline fun networkDocumentsErrorMessage(context: Context, crossinline action: () -> Unit){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(context.resources.getString(R.string.documents_retrieve_error))
        builder.setTitle(Constants.NETWORK_MESSAGE_TITLE)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.setNegativeButton(Constants.RETRY) { dialog: DialogInterface, _: Int ->
            action()
            dialog.dismiss()
        }
        builder.show()
    }

    fun registrationErrorMessage(context: Context, message: String){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
        if(message.contentEquals(Constants.RATE_LIMIT_ERROR_MESSAGE)){
            builder.setTitle(context.getString(R.string.error_rate_limit_title))
            builder.setPositiveButton(context.getString(R.string.dismiss_button_title)) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
            builder.show()
        }else {
            builder.setTitle(context.resources.getString(R.string.error_generic_title))
            builder.setPositiveButton(android.R.string.ok) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
            builder.show()
        }
    }


    inline fun saveBeforeChangeScreenAlert(context: Context?, message: String?, title: String?, crossinline fun1: () -> Unit, crossinline fun2: () -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message).setTitle(title)
        builder.setPositiveButton(Constants.ALERT_DIALOG_PROCEED_OPTION) { dialog: DialogInterface, _: Int ->
            fun1()
            dialog.dismiss()
        }
        builder.setNegativeButton(Constants.ALERT_DIALOG_GO_BACK_OPTION) { dialog: DialogInterface, _: Int ->
            fun2()
            dialog.dismiss()
        }
        builder.show()
    }

    fun networkValidationErrorMessage(context: Context?, message: String?, title: String?) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message).setTitle(title)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    fun formatValidationErrorMessage(context: Context?, message: String?, title: String?) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message).setTitle(title)
        builder.setPositiveButton(Constants.ALERT_DIALOG_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        builder.show()
    }

    @JvmStatic
    fun showAlertDialogBeforeSingOn(context: Context,site: NearSite,  fun1: () -> Unit){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(firstSignOnMessage(site)).setTitle(Constants.FIRST_SITE_ON_TITLE)
        builder.setPositiveButton(Constants.FIRST_SITE_ON_POSITIVE_BUTTON) { dialog: DialogInterface, _: Int ->
            fun1()
            dialog.dismiss()
        }
        builder.setNegativeButton(Constants.FIRST_SITE_ON_NEGATIVE_BUTTON) { dialog: DialogInterface, _: Int ->
            dialog.dismiss()
        }
        builder.show()
    }

    @JvmStatic
    fun requestCameraPermissionAlert(context: Context?, actionCallable: ActionCallable) {
        val builder = AlertDialog.Builder(context)
                .setTitle(Constants.REQUEST_CAMERA_PERMISSION_ALERT_TITLE)
                .setMessage(Constants.REQUEST_CAMERA_PERMISSION_ALERT_MESSAGE)
                .setPositiveButton(Constants.REQUEST_PERMISSION_POSITIVE_BUTTON_TITLE) { _: DialogInterface?, _: Int -> actionCallable.call() }
                .setNegativeButton(Constants.REQUEST_PERMISSION_NEGATIVE_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        val dialog = builder.create()
        dialog.show()
    }

    @JvmStatic
    fun requestStoragePermissionAlert(context: Context?, actionCallable: ActionCallable) {
        val builder = AlertDialog.Builder(context)
                .setTitle(Constants.REQUEST_STORAGE_PERMISSION_ALERT_TITLE)
                .setMessage(Constants.REQUEST_STORAGE_PERMISSION_ALERT_MESSAGE)
                .setPositiveButton(Constants.REQUEST_PERMISSION_POSITIVE_BUTTON_TITLE) { _: DialogInterface?, _: Int -> actionCallable.call() }
                .setNegativeButton(Constants.REQUEST_PERMISSION_NEGATIVE_BUTTON_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        val dialog = builder.create()
        dialog.show()
    }

    interface ActionCallable {
        fun call()
    }

    inline fun deleteCredentialAlertDialog(context: Context, crossinline action:()->Unit){
        val builder = AlertDialog.Builder(context)
                .setTitle(Constants.DELETE_ALERT_DIALOG_TITLE)
                .setMessage(Constants.DELETE_ALERT_DIALOG_MESSAGE)
                .setPositiveButton(Constants.DELETE_ALERT_BUTTON_POSITIVE_TITLE) { _: DialogInterface?, _: Int -> action() }
                .setNegativeButton(Constants.DELETE_ALERT_BUTTON_NEGATIVE_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        val dialog = builder.create()
        dialog.show()
    }

    inline fun removeConnectionsAlertDialog(context: Context, crossinline action:()->Unit){
        val builder = AlertDialog.Builder(context)
                .setTitle(Constants.REMOVE_SITE_CONNECTION_ALERT_DIALOG_TITLE)
                .setMessage(Constants.REMOVE_SITE_CONNECTION_ALERT_DIALOG_MESSAGE)
                .setPositiveButton(Constants.REMOVE_SITE_CONNECTION_ALERT_BUTTON_POSITIVE_TITLE) { _: DialogInterface?, _: Int -> action() }
                .setNegativeButton(Constants.REMOVE_SITE_CONNECTION_ALERT_BUTTON_NEGATIVE_TITLE) { dialog: DialogInterface, _: Int -> dialog.dismiss() }
        val dialog = builder.create()
        dialog.show()
    }

    inline fun shareConfirmationMessage(context: Context, email:String, crossinline action:()->Unit){
        val builder = AlertDialog.Builder(context)
                .setTitle(Constants.SHARE_PASSPORT_ALERT_DIALOG_TITLE)
                .setMessage(Constants.SHARE_PASSPORT_ALERT_DIALOG_MESSAGE + email)
                .setPositiveButton(Constants.SHARE_PASSPORT_ALERT_BUTTON_POSITIVE_TITLE) { _: DialogInterface?, _: Int -> action() }
        val dialog = builder.create()
        dialog.show()
    }

    inline fun permitAction(context: Context, optionList: Array<String>,crossinline redoAction: () -> Unit,crossinline callAction: () -> Unit,crossinline removeAction:() -> Unit,crossinline dismissAction: (DialogInterface) -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
                .setTitle(context.resources.getString(R.string.dialog_action))
                .setItems(optionList) { dialog, which ->
                    when(optionList[which]){
                        "Redo" -> redoAction()
                        "Remove" -> removeAction()
                        "Call" -> callAction()
                        "Dismiss" -> dismissAction(dialog)
                    }
                }
                .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }

    inline fun permitRequesterAlertDialog(context: Context, optionList: Array<String>, crossinline callAction: () -> Unit, crossinline dismissAction: (DialogInterface) -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
                .setTitle(context.resources.getString(R.string.dialog_action))
                .setItems(optionList) { dialog, which ->
                    when(optionList[which]){
                        "Call" -> callAction()
                        "Dismiss" -> dismissAction(dialog)
                    }
                }
                .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }
    inline fun permitDateBeforeNowError(context: Context){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.permit_start_time_error_title))
            .setMessage(context.resources.getString(R.string.permit_start_time_error_message))
            .setPositiveButton(context.resources.getString(R.string.dismiss_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun permitObtainApprovalInRequestMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.permit_acknowledge_trained_qualified_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun permitDoneInRequestMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.permit_acknowledge_start_work_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun permitApproveRejectInPendingApproval(context: Context, crossinline callYesAction: () -> Unit, crossinline callRejectAction:()->Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.permit_acknowledge_trained_qualified_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callYesAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .setNeutralButton(context.resources.getString(R.string.reject_button_title)){_: DialogInterface?, _: Int -> callRejectAction() }
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun permitDoneInProgressMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.permit_acknowledge_complete_work_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun permitSendForClosureInProgressMessage(context: Context,crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.permit_acknowledge_complete_request_work_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.BOTTOM) }
    }
    inline fun cancelPermitInRequestMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.cancel_permit_in_request_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }
    inline fun cancelPermitInProgressMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.cancel_permit_in_request_message))
            .setPositiveButton(context.resources.getString(R.string.yes_button_title)) { _: DialogInterface?, _: Int -> callAction()}
            .setNegativeButton(context.resources.getString(R.string.no_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }
    fun showUserDoesNotHavePhoneNumberError(context: Context){
        val alertDialog = MaterialAlertDialogBuilder(context)
            .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
            .setMessage(context.resources.getString(R.string.user_phone_number_does_not_exist_error_message))
            .setPositiveButton(context.resources.getString(R.string.dismiss_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
            .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }
    inline fun showSendToTeamWarningMessage(context: Context, crossinline callAction: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
                .setTitle(context.resources.getString(R.string.important_alert_dialog_title))
                .setMessage(context.resources.getString(R.string.send_to_team_warning_message))
                .setPositiveButton(context.resources.getString(R.string.ok_button_title)) { _: DialogInterface?, _: Int -> callAction()}
                .setNegativeButton(context.resources.getString(R.string.dismiss_button_title)) {dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
                .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }
    inline fun showUnSavedPermitInfoInPermits(context: Context, crossinline proceed: () -> Unit){
        val alertDialog = MaterialAlertDialogBuilder(context)
                .setTitle(context.resources.getString(R.string.unsaved_permit_changes_title))
                .setMessage(context.resources.getString(R.string.unsaved_permit_changes_message))
                .setPositiveButton(context.resources.getString(R.string.go_back_and_save_button_label)) { dialog: DialogInterface?, _: Int -> dialog?.dismiss()}
                .setNegativeButton(context.resources.getString(R.string.proceed_button_label)) {_ :DialogInterface?, _: Int -> proceed()}
                .show()
        alertDialog.window?.let { it.setGravity(Gravity.NO_GRAVITY) }
    }


    private fun firstSignOnMessage(site: NearSite): String {
        return "\n" +
                "Site Name: ${site.name}\n" +
                "Company Name: ${site.principal_company_name}\n" +
                "\n" +
                "Your name and company will be posted on the site's attendance register.\n" +
                "If you have a SignOnSite passport it will be shared with the company that manages the site.\n" +
                "\n" +
                "End your connection with the site by going to the passport 'connections' tab.\n" +
                "\n" +
                "Would you like to confirm your sign on?\n" +
                "\n"
    }

}