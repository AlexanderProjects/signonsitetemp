package au.com.signonsitenew.utilities.imagepicker

import android.content.Intent
import java.io.File

interface ImagePickerContract {
    fun setWithImageCrop(aspectRatioX: Int, aspectRatioY: Int): SoSImagePicker
    fun choosePicture(includeCamera: Boolean)
    fun openCamera()
    fun getImageFile(): File?
    fun handlePermission(requestCode: Int, grantResults: IntArray?)
    fun handleActivityResult(resultCode: Int, requestCode: Int, data: Intent?)
}