package au.com.signonsitenew.utilities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import androidx.core.app.ActivityCompat;
import org.json.JSONException;
import org.json.JSONObject;
import au.com.signonsitenew.diagnostics.DiagnosticsManager;
import au.com.signonsitenew.realm.DiagnosticLog;
import au.com.signonsitenew.ui.main.SignedOnActivity;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class PermissionCheckers {

    private static final String TAG = PermissionCheckers.class.getSimpleName();

    public static boolean locationPermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }else{
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }
    }

    public static boolean isLocationPermissionGrantedForAndroidTen(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }else {
            return false;
        }
    }

    public static boolean ignoreBattOptsGranted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = context.getPackageName();
            PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            boolean granted = powerManager.isIgnoringBatteryOptimizations(packageName);

            if (granted) {
                Log.i(TAG, "Ignored batt opt permission: " + granted);
                return true;
            }
            else {
                Log.i(TAG, "Ignore batt opt permission: " + granted);
                return false;
            }
        }
        // Android device is below M and does not have Doze. Can assume all should be ok...
        return true;
    }

    /**
     * Displays an AlertDialog requesting the user to allow SignOnSite to continue to run in the
     * background without being affected by Battery Management solutions such as Android Doze or
     * Samsung Smart Manager and other similar applications.
     */
    public static void checkBatteryOptimisationSettings(final Context context) {
        // Battery optimisation (Doze) was only introduced in Android M (SDK 23)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Check battery optimisation settings
            String packageName = context.getPackageName();
            PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
                // Display the whitelist permission
                if(context instanceof SignedOnActivity) {
                    SLog.i(TAG, "Displaying battery optimisation whitelist permission info dialog");
                    AlertDialog.Builder builder = new AlertDialog.Builder(context)
                            .setTitle("Let SignOnSite run in the background")
                            .setMessage("In a moment you will be asked to allow SignOnSite to ignore " +
                                    "battery optimisations. Please allow this, as it will allow SignOnSite " +
                                    "continue to sign you on and off site in the background. Using our " +
                                    "specially developed location algorithm, this will not drain your " +
                                    "phone\'s battery")
                            .setPositiveButton("Give Access", (dialog, which) -> {
                                // User accepted at the initial screen - open proper permission dialog
                                requestIgnoreBatteryOptimisationPermission(context);
                                dialog.dismiss();
                            })
                            .setNegativeButton("Not Now", (dialog, which) -> {
                                JSONObject jsonData = new JSONObject();
                                try {
                                    jsonData.put("state", "soft_deny");
                                } catch (JSONException e) {
                                    SLog.e(TAG, "JSON Exception occurred: " + e.getMessage());
                                }
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.BATT_OPT_IGNORE_REQUEST, null, jsonData.toString());
                                dialog.dismiss();
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
            else {
                SLog.i(TAG, "Device is ignoring battery optimisations and allowing SignOnSite to run in the background");
            }
        }

    }

    public static void checkDoNotDisturbPermission(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // LG has a bug where checking for the permission ACCESS_NOTIFICATION_POLICY will cause a crash
            // Alternative is to ask for BIND_NOTIFICATION_LISTENER_SERVICE which grants both permissions but is invasive in
            // terms of privacy - something our users are already very conscious of. As LG only has a
            // small market share we will leave this be for now.
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                if (!notificationManager.isNotificationPolicyAccessGranted()) {
                    // Show a popup to talk about this
                    AlertDialog.Builder builder = new AlertDialog.Builder(context)
                            .setTitle("Evacuation Notifications")
                            .setMessage("During an evacuation, SignOnSite will override your volume " +
                                    "settings to raise the alarm. If you want to be alerted even when " +
                                    "the phone is in Do Not Disturb mode, please toggle SignOnSite to " +
                                    "'On' in the following menu.")
                            .setPositiveButton("Go to Settings", (dialogInterface, i) -> {
                                Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                                try {
                                    context.startActivity(intent);
                                }
                                catch (ActivityNotFoundException e) {
                                    Log.i(TAG, "Activity Not Found");
                                }
                            })
                            .setNegativeButton("Not Now", (dialogInterface, i) -> {
                                JSONObject jsonData = new JSONObject();
                                try {
                                    jsonData.put("state", "soft_deny");
                                }
                                catch (JSONException e) {
                                    SLog.e(TAG, "JSON Exception occurred: " + e.getMessage());
                                }
                                DiagnosticsManager.logEvent(context, DiagnosticLog.Tag.DND_OVERRIDE, null, jsonData.toString());
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
            catch (Exception e) {
                SLog.e(TAG, "Checking for do not disturb permission threw an error. " + e.getMessage());
            }
        }
    }

    /**
     * Requests the Battery Optimisation Ignore permission. If the permission has been denied
     * previously, a SnackBar will prompt the user to grant the permission, otherwise it is
     * requested directly.
     */
    private static void requestIgnoreBatteryOptimisationPermission(Context context) {
        SLog.i(TAG, "User accepted initial dialog, Requesting IGNORE BATTERY OPTIMISATIONS permission.");

        // Ignore Battery Optimisations permissions not granted yet. Request them directly.
        String packageName = context.getPackageName();
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
        intent.setData(Uri.parse("package:" + packageName));
        context.startActivity(intent);
    }

    public static void requestManageExternalStoragePermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        }
    }

}
