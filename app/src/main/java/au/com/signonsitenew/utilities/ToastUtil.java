package au.com.signonsitenew.utilities;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Krishan Caldwell on 23/06/2016.
 */
public class ToastUtil {

    public static void displayGenericErrorToast(Context context) {
        Toast.makeText(context, "An error occurred, please try again", Toast.LENGTH_LONG).show();
    }

    public static void displayToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
