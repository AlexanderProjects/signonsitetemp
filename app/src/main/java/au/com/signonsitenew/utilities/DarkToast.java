package au.com.signonsitenew.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import com.google.android.material.snackbar.Snackbar;

import au.com.signonsitenew.R;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * The default Toast on Android is a light colour and difficult to see in the SOS Application.
 * This is a class that modifies the text and background colours for a Toast before setting it.
 */
public class DarkToast {
    public static void makeText(Context context, @NonNull CharSequence text) {
        if (context == null) {
            return;
        }
        Handler mainHandler = new Handler(context.getMainLooper());
        Runnable myRunnable = () -> {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                if(context instanceof  Activity){
                    Snackbar snackbar = Snackbar.make(((Activity)context).getWindow().getDecorView(),text,Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else {
                    Toast toast = Toast.makeText(context, HtmlCompat.fromHtml("<font color='#000000;'>"+text+"</font>", HtmlCompat.FROM_HTML_MODE_LEGACY), Toast.LENGTH_LONG);
                    toast.show();
                }
            }else {
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);

                // Insert the desired background colour for the Toast
                View view = toast.getView();
                view.getBackground().setColorFilter(context.getResources().getColor(R.color.grey_primary), PorterDuff.Mode.SRC_IN);

                // Set the text colour to white
                TextView textView = view.findViewById(android.R.id.message);
                textView.setTextColor(context.getResources().getColor(R.color.white));

                // Apply!
                toast.setView(view);
                toast.show();
            }

        };
        mainHandler.post(myRunnable);

    }

    public static void displayGenericErrorToast(Context context) {
        makeText(context, "An error occurred, please try again");
    }
}
