package au.com.signonsitenew.utilities.phonepicker

import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import au.com.signonsitenew.R
import au.com.signonsitenew.di.factory.ViewModelFactory
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.utilities.AlertDialogMessageHelper
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.NetworkUtil
import com.hbb20.CountryCodePicker
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class PhonePickerFragment : DaggerFragment(), PhonePickerView {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: PhonePickerViewModel
    private lateinit var phoneNumberTexView: TextView
    private lateinit var countryCodePicker: CountryCodePicker
    private lateinit var newPhoneNumber: EditText
    private lateinit var submitButton: Button
    private lateinit var user: User
    private var isPrimaryContact = false
    private var isSecondaryContact = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.phone_picker_fragment, container, false)
        if(requireArguments().getParcelable<User>(Constants.USER_FLAG) != null)
            user = requireArguments().getParcelable(Constants.USER_FLAG)!!
        isPrimaryContact = requireArguments().getBoolean("primary_contact")
        isSecondaryContact = requireArguments().getBoolean("secondary_contact")
        val toolbar: Toolbar = view.findViewById(R.id.phone_picker_toolbar)
        toolbar.title = Constants.TOOL_BAR_PICKER_TITLE
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
        phoneNumberTexView = view.findViewById(R.id.passport_phone_number)
        countryCodePicker = view.findViewById(R.id.passport_ccp)
        newPhoneNumber = view.findViewById(R.id.new_passport_phone_number)
        submitButton = view.findViewById(R.id.passport_new_number_button)
        countryCodePicker.setFlagBorderColor(ContextCompat.getColor(requireActivity(), R.color.hint_text_colour))
        val countryCode = NetworkUtil.determineCountryCode(activity)
        countryCodePicker.setDefaultCountryUsingNameCode(countryCode)
        if (user.phone_number != null && !isSecondaryContact && !isPrimaryContact) {
            phoneNumberTexView.text = user.phone_number?.number
            countryCodePicker.setCountryForNameCode(user.phone_number!!.alpha2)
        }
        if (user.primary_contact != null && isPrimaryContact) {
            phoneNumberTexView.text = user.primary_contact?.phone_number?.number
            countryCodePicker.setCountryForNameCode(user.primary_contact?.phone_number?.alpha2)
        }
        if (user.secondary_contact != null && isSecondaryContact) {
            phoneNumberTexView.text = user.secondary_contact?.phone_number?.number
            countryCodePicker.setCountryForNameCode(user.secondary_contact?.phone_number?.alpha2)
        }
        submitButton.setOnClickListener { viewModel.validatePhoneNumber(user) }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PhonePickerViewModel::class.java)
        viewModel.inject(this)
    }

    override fun goBack() {
        if (newPhoneNumber.text.toString().isNotEmpty()) {
            if (isSecondaryContact || isPrimaryContact) {
                phoneContactSelectedListener!!.phoneNumberValidated(createObject(countryCodePicker.selectedCountryNameCode, newPhoneNumber.text.toString()))
            }
            if (!isSecondaryContact && !isPrimaryContact) {
                phoneSelectedListener!!.phoneNumberValidated(createObject(countryCodePicker.selectedCountryNameCode, newPhoneNumber.text.toString()))
            }
        }
        requireActivity().supportFragmentManager.popBackStack()
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }

    override fun showValidationError() {
        AlertDialogMessageHelper.networkValidationErrorMessage(activity, Constants.VALIDATION_PHONE_MESSAGE, Constants.VALIDATION_PHONE_TITLE)
    }

    override fun showDataError(errorMessage: String) {
        AlertDialogMessageHelper.dataErrorMessage(requireContext(),errorMessage)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if(menu.findItem(R.id.share_button) != null)
            menu.findItem(R.id.share_button).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun createObject(countryCode: String, phoneNumber: String): PhoneNumber {
        val phoneNumberObj = PhoneNumber()
        phoneNumberObj.number = phoneNumber
        phoneNumberObj.alpha2 = countryCode
        return phoneNumberObj
    }

    companion object {
        private var phoneSelectedListener: PhoneSelectedListener? = null
        private var phoneContactSelectedListener: PhoneContactSelectedListener? = null
        @JvmStatic
        fun newInstance(user: User?, primaryContact: Boolean?, secondaryContact: Boolean?): PhonePickerFragment {
            val phonePickerFrag = PhonePickerFragment()
            val bundle = Bundle()
            bundle.putParcelable(Constants.USER_FLAG, user)
            bundle.putBoolean("primary_contact", primaryContact!!)
            bundle.putBoolean("secondary_contact", secondaryContact!!)
            phonePickerFrag.arguments = bundle
            return phonePickerFrag
        }

        fun setPhoneSelectedListener(listener: PhoneSelectedListener?) {
            phoneSelectedListener = listener
        }

        fun setPhoneContactSelectedListener(listener: PhoneContactSelectedListener?) {
            phoneContactSelectedListener = listener
        }
    }
}