package au.com.signonsitenew.utilities.phonepicker

import androidx.lifecycle.ViewModel
import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.usecases.internet.CheckForInternetConnectionUseCaseImpl
import au.com.signonsitenew.domain.usecases.registration.PhoneValidationUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PhonePickerViewModel @Inject constructor(private val phoneValidationUseCaseImpl: PhoneValidationUseCaseImpl,
                                               private val internetConnectionUseCaseImpl: CheckForInternetConnectionUseCaseImpl) : ViewModel() {
    private val disposables = CompositeDisposable()
    private var phonePickerView: PhonePickerView? = null
    fun validatePhoneNumber(user: User) {
        disposables.add(
                internetConnectionUseCaseImpl.isInternetConnected()
                        .flatMap {
                            if(it) return@flatMap phoneValidationUseCaseImpl.validatePhoneNumber(user.phone_number!!.alpha2!!, user.phone_number!!.number!!)
                            else return@flatMap  io.reactivex.Single.just(phonePickerView!!.showValidationError())
                        }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response ->
                            if (response is ApiResponse){
                                if(phoneValidationUseCaseImpl.isValidResponse(response))
                                    phonePickerView!!.goBack()
                                else
                                    phonePickerView!!.showDataError(phoneValidationUseCaseImpl.getErrorMessage(response))
                            }

                        }) { phonePickerView!!.showValidationError() })
    }

    fun inject(phonePickerView: PhonePickerView?) {
        this.phonePickerView = phonePickerView
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}