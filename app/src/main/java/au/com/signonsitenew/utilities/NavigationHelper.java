package au.com.signonsitenew.utilities;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import au.com.signonsitenew.R;
import au.com.signonsitenew.domain.models.Credential;
import au.com.signonsitenew.domain.models.CredentialType;
import au.com.signonsitenew.domain.models.User;
import au.com.signonsitenew.ui.main.MainActivity;
import au.com.signonsitenew.ui.passport.credentials.credentialcreation.CredentialCreationFormFragment;
import au.com.signonsitenew.ui.passport.credentials.credentialdetails.CredentialUpdateFragment;
import au.com.signonsitenew.ui.passport.credentials.credentialtypes.CredentialTypesFragment;
import au.com.signonsitenew.utilities.phonepicker.PhonePickerFragment;

public class NavigationHelper {

    public static void navigateToPhonePicker(FragmentActivity activity, User user, Boolean isPrimaryContact, Boolean isSecondaryContact){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment phonePickerFragment = PhonePickerFragment.newInstance(user, isPrimaryContact, isSecondaryContact);
        if(activity instanceof MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container,phonePickerFragment,Constants.FRAGMENT_PHONE_PICKER_BACK_STACK);
        else
            fragmentTransaction.replace(R.id.signon_container, phonePickerFragment, Constants.FRAGMENT_PHONE_PICKER_BACK_STACK);
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_PHONE_PICKER_BACK_STACK);
        fragmentTransaction.commit();
    }

    public static void navigateToCredentialUpdate(FragmentActivity activity, Credential credential){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment credentialUpdateFragment = CredentialUpdateFragment.newInstance(credential);
        if(activity instanceof MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container,credentialUpdateFragment, Constants.FRAGMENT_CREDENTIAL_UPDATE_BACK_STACK);
        else
            fragmentTransaction.replace(R.id.signon_container, credentialUpdateFragment, Constants.FRAGMENT_CREDENTIAL_UPDATE_BACK_STACK);
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_CREDENTIAL_UPDATE_BACK_STACK);
        fragmentTransaction.commit();
    }

    public static void navigateToListOfCredentials(FragmentActivity activity){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment credentialTypesFragment = CredentialTypesFragment.newInstance();
        if(activity instanceof  MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container, credentialTypesFragment,Constants.FRAGMENT_CREDENTIAL_TYPES_BACK_STACK);
        else
            fragmentTransaction.replace(R.id.signon_container, credentialTypesFragment, Constants.FRAGMENT_CREDENTIAL_TYPES_BACK_STACK);
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_CREDENTIAL_TYPES_BACK_STACK);
        fragmentTransaction.commit();
    }

    public static void navigateToCreateCredential(FragmentActivity activity, CredentialType credentialType){
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment credentialCreationFragment = CredentialCreationFormFragment.newInstance(credentialType);
        if(activity instanceof MainActivity)
            fragmentTransaction.replace(R.id.main_activity_container, credentialCreationFragment, Constants.FRAGMENT_CREDENTIAL_CREATION_BACK_STACK);
        else
            fragmentTransaction.replace(R.id.signon_container, credentialCreationFragment, Constants.FRAGMENT_CREDENTIAL_CREATION_BACK_STACK);
        fragmentTransaction.addToBackStack(Constants.FRAGMENT_CREDENTIAL_CREATION_BACK_STACK);
        fragmentTransaction.commit();
    }



}
