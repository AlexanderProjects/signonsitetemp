package au.com.signonsitenew.utilities;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import au.com.signonsitenew.R;
import au.com.signonsitenew.ui.evacuation.EmergencyActivity;
import au.com.signonsitenew.ui.prelogin.SplashActivity;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 *
 * Utility class to quickly create and display local notifications.
 */
public class NotificationUtil {
    private static final String LOG = NotificationUtil.class.getSimpleName();
    private static final boolean DEBUG_ENABLED = Constants.DEBUG_MODE;
    private static NotificationManager mNotificationManager;
    private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
    public static final int NOTIFICATION_ATTENDANCE_ID = 1;
    public static final int NOTIFICATION_BRIEFING_ID = 8;
    public static final int NOTIFICATION_UPDATE_ID = 9;
    public static final int NOTIFICATION_INDUCTION_ID = 27;
    public static final int NOTIFICATION_REGION_ID = 666;
    public static final int NOTIFICATION_GENERAL_ID = 777;
    public static final int NOTIFICATION_LOCATION_SERVICE = 789;
    public static final int NOTIFICATION_STATE_UPDATE_ID = 999;
    public static final int NOTIFICATION_EVACUATION_ID = 999999;
    public static final int NOTIFICATION_OTHER_ID = 234;
    private static final String BG_CHANNEL_ID = "background_channel";
    private static final String BRIEF_CHANNEL_ID = "briefing_channel";
    private static final String EMERGENCY_CHANNEL_ID = "emergency_channel";
    private static final String GENERAL_CHANNEL_ID = "general_channel";
    private static final String INDUCTION_CHANNEL_ID = "induction_channel";
    private static final String LOCATION_CHANNEL_ID = "location_channel";
    private static final String OTHER_WORK_ID = "other_channel";
    private static final String DEBUG_CHANNEL_ID = "debug_channel";
    // Deprecated Channels
    private static final String OLD_LOC_CHANNEL_ID = "loc_eng_channel";
    private static final String OLD_EVAC_CHANNEL_ID = "evac_channel";



    private static void deleteOldServiceChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                notificationManager.deleteNotificationChannel(OLD_LOC_CHANNEL_ID);
            }
            catch (NullPointerException e) {
                Log.e(LOG, "Null pointer: " + e.getMessage());
            }
        }
    }

    private static void deleteOldEvacChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                notificationManager.deleteNotificationChannel(OLD_EVAC_CHANNEL_ID);
            }
            catch (NullPointerException e) {
                Log.e(LOG, "Null pointer: " + e.getMessage());
            }
        }
    }


    public static void createInductionRejectedNotification(Context context) {
        setupInductionChannel(context);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        String title = "Induction rejected";
        String message = "Your induction was rejected by site staff. Please ask the staff for their feedback.";

        // Set the intent. Use a try catch block in case the user does not have the Play Store installed.
        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra(Constants.TAB_INDEX, Constants.TAB_DOCS); // Index for documents tab
        intent.putExtra(Constants.TAB_FUNCTION, Constants.SITE_INDUCTION);
        intent.putExtra(Constants.HAS_USER_TAPPED_ON_NOTIFICATION,true);
        intent.putExtra(Constants.IS_REMOTE_NOTIFICATION,true);

        // Set the PendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get the default alert sound
        Uri alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, INDUCTION_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                .setLights(Color.YELLOW, 1000, 1000)
                .setVibrate(new long[] {250, 250, 250, 250})
                .setSound(alertSound)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                );

        Notification notification = mBuilder.build();

        mNotificationManager.notify(NOTIFICATION_INDUCTION_ID, notification);

        SLog.i(LOG, "Site Induction Information notification created");
    }

    public static void createUpdateNotification(Context context) {
        setupGeneralChannel(context);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        String title = "Update Available!";
        String message = "An update is available! Press here to go to the Play Store.";
        int notificationId = NOTIFICATION_UPDATE_ID;

        // Set the intent. Use a try catch block in case the user does not have the Play Store installed.
        final String appPackageName = context.getPackageName();
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + appPackageName));
        }
        catch (ActivityNotFoundException activityNotFoundException) {
            intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }

        // Set the PendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get the default alert sound
        Uri alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GENERAL_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                .setLights(Color.GREEN, 1000, 1000)
                .setSound(alertSound)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                );

        Notification notification = mBuilder.build();

        mNotificationManager.notify(notificationId, notification);

        SLog.i(LOG, "Update notification created");
    }

    public static void createEvacuationNotification(Context context, String message) {
        setupEmergencyChannel(context);
        NotificationManager notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, EmergencyActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

        //Sound to be played
        Uri alarmSound = Uri.parse("android.resource://"
                + context.getPackageName() + "/" + R.raw.siren);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, EMERGENCY_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle(context.getString(R.string.title_evacuation))
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, R.color.emergency_primary))
                .setLights(Color.RED, 250, 250) // fast flash, long vibrate
                .setVibrate(new long[]{100, 1000, 100, 1000, 100, 1000, 100, 1000, 100, 1000})
                .setSound(alarmSound)
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                );

        Notification notification = mBuilder.build();

        // Override the user's sound settings to force alarm to play at max even if phone on silent
        try {
            final AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
            int maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, maxVol, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        }
        catch (SecurityException e) {
            SLog.d(LOG, "Security Exception occurred: " + e.getMessage());
        }
        catch (Exception e) {
            SLog.e(LOG, "An unexpected exception occurred: " + e.getMessage());
        }

        notificationManager.notify(NOTIFICATION_EVACUATION_ID, notification);
    }

    public static void createNotification(Context context, String title, String message) {
        setupGeneralChannel(context);
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = NOTIFICATION_GENERAL_ID;

        // Get the default alert sound
        Uri alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GENERAL_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                .setLights(Color.GREEN, 1000, 1000)
                .setVibrate(new long[] {250, 250, 250, 250})
                .setSound(alertSound)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                );

        Notification notification = mBuilder.build();

        notificationManager.notify(notificationId, notification);

        SLog.i(LOG, "Custom notification created");
    }

    /**
     * This method returns a generic notification to be used for Foreground Services.
     * @param context - The context of the Foreground Service, since the notification should stick around for the life of the service.
     * @return the notification to be displayed.
     */
    public static Notification buildLocationEngineServiceNotification(Context context) {
        setupServiceChannel(context);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Set the PendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, LOCATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle("SignOnSite Assistant")
                .setContentText(context.getString(R.string.location_service_message))
                .setContentIntent(contentIntent)
                .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(context.getString(R.string.location_service_message))
                );

        return mBuilder.build();
    }

    /**
     * This method returns a generic notification to be used for Foreground Services.
     * @param context - The context of the Foreground Service, since the notification should stick around for the life of the service.
     * @return the notification to be displayed.
     */
    public static Notification buildRegionFetcherNotification(Context context) {
        setupBackgroundWorkChannel(context);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Set the PendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, BG_CHANNEL_ID)
                .setSmallIcon(R.drawable.logo_white)
                .setContentTitle("Updating sites")
                .setContentText("Updating the sites that you should sign onto automatically.")
                .setContentIntent(contentIntent)
                .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Updating the sites that you should sign onto automatically.")
                );

        return mBuilder.build();
    }

    public static void cancelNotifications(Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }

    /**
     ***********************************************************************************************
     * Debugging notifications
     ***********************************************************************************************
     */

    public static void createRegionNotification(Context context, String message) {
        if (DEBUG_ENABLED) {
            setupDebugChannel(context);
            mNotificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);

            String title = "Region Event";

            int notificationId = NOTIFICATION_REGION_ID;

            // Get the default alert sound
            Uri alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GENERAL_CHANNEL_ID)
                    .setSmallIcon(R.drawable.logo_white)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                    .setLights(Color.YELLOW, 1000, 1000)
                    .setVibrate(new long[] {250, 250, 250, 250})
                    .setSound(alertSound)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message)
                    );

            Notification notification = mBuilder.build();

            mNotificationManager.notify(notificationId, notification);
        }
    }

    public static void createDebugNotification(Context context, String title, String message) {
        if (DEBUG_ENABLED) {
            setupDebugChannel(context);
            mNotificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);

            int notificationId = NOTIFICATION_STATE_UPDATE_ID;

            // Get the default alert sound
            Uri alertSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GENERAL_CHANNEL_ID)
                    .setSmallIcon(R.drawable.logo_white)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(context, R.color.orange_primary))
                    .setLights(Color.YELLOW, 1000, 1000)
                    .setVibrate(new long[]{250, 250, 250, 250})
                    .setSound(alertSound)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message)
                    );

            Notification notification = mBuilder.build();

            mNotificationManager.notify(notificationId, notification);

            SLog.i(LOG, "Custom notification created");
        }
    }

    /**********************************************************************************************
     * Setup Notification Channels. These include:
     *   1. Attendance
     *   2. Briefing Notices
     *   3. Location Engine Background Service
     *   4. Evacuation Notices
     *   5. Update Available
     *   6. General Notices
     *   7. Debug
     *   8. PromptedSignOnSignOff
     * TODO: SiteInductionInformation
     **********************************************************************************************/


    private static void setupInductionChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = "Induction status notifications";

            // The user-visible descriptions and importance of the channels.
            String description = "Site Induction notifications";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel channel = new NotificationChannel(INDUCTION_CHANNEL_ID, name, importance);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            channel.setLightColor(Color.YELLOW);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[] { 250, 250, 250, 250 });
            channel.canShowBadge();
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    private static void setupServiceChannel(Context context) {
        // Delete the old channel
        deleteOldServiceChannel(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = "Location Engine operations";

            // The user-visible descriptions and importance of the channels.
            String description = "Location Engine notifications. This channel is required to be on for Automatic Sign On to work correctly.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(LOCATION_CHANNEL_ID, name, importance);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setVibrationPattern(null);
            channel.setSound(null, null);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    private static void setupBackgroundWorkChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Delete the old background channel if it exists
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "loc_eng_channel";
            try {
                notificationManager.deleteNotificationChannel(channelId);
            }
            catch (Exception e) {
                Log.i(LOG, "Exception caught. Old channel did not exist");
            }

            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = context.getString(R.string.region_service_title);

            // The user-visible descriptions and importance of the channels.
            String description = "These notifications appear when we update sites that you should be signed onto automatically.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(BG_CHANNEL_ID, name, importance);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setVibrationPattern(null);
            channel.setSound(null, null);
            channel.setShowBadge(false);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    private static void setupEmergencyChannel(Context context) {
        // Delete old channel if it exists
        deleteOldEvacChannel(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = "Emergency notifications";

            // The user-visible descriptions and importance of the channels.
            String description = "Emergency notifications";
            int importance = NotificationManager.IMPORTANCE_HIGH; // Importance max is not allowed?!

            NotificationChannel channel = new NotificationChannel(EMERGENCY_CHANNEL_ID, name, importance);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build();

            //Sound to be played
            Uri alarmSound = Uri.parse("android.resource://"
                    + context.getPackageName() + "/" + R.raw.siren);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[] { 100, 1000, 100, 1000, 100, 1000, 100, 1000, 100, 1000 });
            channel.setSound(alarmSound, audioAttributes);
            channel.canBypassDnd();
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    private static void setupGeneralChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = "General notifications";

            // The user-visible descriptions and importance of the channels.
            String description = "General notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(GENERAL_CHANNEL_ID, name, importance);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            channel.setLightColor(Color.YELLOW);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[] { 250, 250, 250, 250 });
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    private static void setupDebugChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // The user-visible name of the channel.
            CharSequence name = "Debugging";

            // The user-visible descriptions and importance of the channels.
            String description = "Debugging notifications";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel channel = new NotificationChannel(DEBUG_CHANNEL_ID, name, importance);

            // Configure the notification channel.
            channel.setDescription(description);
            channel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            channel.setLightColor(Color.GREEN);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[] { 250, 250, 250, 250 });
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static boolean appNotificationsEnabled(Context context) {

        ApplicationInfo appInfo = context.getApplicationInfo();

        String pkg = context.getApplicationContext().getPackageName();

        int uid = appInfo.uid;

        Class appOpsClass = null; /* Context.APP_OPS_MANAGER */

        try {
            AppOpsManager mAppOps = (AppOpsManager)context.getSystemService(Context.APP_OPS_SERVICE);

            appOpsClass = Class.forName(AppOpsManager.class.getName());

            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (int)opPostNotificationValue.get(Integer.class);

            return ((int)checkOpNoThrowMethod.invoke(mAppOps,value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

        }
        // Return true for catch clauses. They only can occur in Android 4.1. Users cannot change
        // notification settings on this version of Android.
        catch (ClassNotFoundException e) {
            SLog.e(LOG, "Class Not Found Exception: " + e.getMessage());
            return true;
        }
        catch (NoSuchMethodException e) {
            SLog.e(LOG, "No Such Method Exception: " + e.getMessage());
            return true;
        }
        catch (NoSuchFieldException e) {
            SLog.e(LOG, "No Such Field Exception: " + e.getMessage());
            return true;
        }
        catch (InvocationTargetException e) {
            SLog.e(LOG, "Invocation Target Exception: " + e.getMessage());
            return true;
        }
        catch (IllegalAccessException e) {
            SLog.e(LOG, "Illegal Access Exception: " + e.getMessage());
            return true;
        }
        catch (NoClassDefFoundError e) {
            SLog.e(LOG, "No Class Def Found Error: " + e.getMessage());
            return true;
        }
    }

}
