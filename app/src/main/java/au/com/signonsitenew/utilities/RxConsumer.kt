package au.com.signonsitenew.utilities

import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.domain.models.UserInfoResponse
import io.reactivex.functions.Consumer

class RxConsumer {
    fun onSuccessResponse(updateCallback:()->Unit, errorCallback:(apiResponse: ApiResponse)->Unit): Consumer<Any> = Consumer{
        when (it) {
            is UserInfoResponse -> if (NetworkErrorValidator.isValidResponse(it)) {
                updateCallback()
            }
            is ApiResponse -> if (!NetworkErrorValidator.isValidResponse(it))
                errorCallback(it)
        }
    }
    fun consumerOnError(errorCallback: () -> Unit): Consumer<Throwable> = Consumer{
        errorCallback()
    }
}