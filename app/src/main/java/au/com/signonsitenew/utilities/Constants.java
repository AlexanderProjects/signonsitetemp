package au.com.signonsitenew.utilities;

import android.Manifest;
import au.com.signonsitenew.BuildConfig;


/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 *
 * Class for all constants associated with the SOS Application.
 */
public class Constants {

    // Debug Mode
    public static final boolean DEBUG_MODE = false;

    // Server Auth
    public static final String BASE = BuildConfig.BASE_URL;
    private static final String BASE_URL = BASE + "api/";
    public static final String AUTH_STRING = "authentication";
    public static final String AUTH_KEY = "GpulXj5KyMKkLIDILrt7htPtyGw86PvUvN3exB72XcNXplvTf1suqvwBqQpnvz6";
    public static final String AUTH_TOKEN = "auth_token";
    public static final String BEARER_HEADER = "bearer ";

    //Shared pref store key
    public static final String SOS_SHARED_PREF = "SOSharePref";
    public static final String PREF_NAME = "SignOnSitePref";

    // Segment Key
    public static final String SEGMENT_WRITE_KEY = BuildConfig.SEGMENT_WRITE_KEY;

    // Image Size Constants
    public static final int IMAGE_LARGE  = 1200000; // 1.2MP{"alpha2":"AU","company_name":"SignOnSite","diagnostics_enable":false,"email":"carlitos@me.com","first_name":"Carlos","last_name":"Vives","password":"Carlitos2019","phone_number":"0434242730"}
    public static final int IMAGE_MEDIUM = 600000;  // 0.6MP

    // Region Radii Constants
    public static final double MIN_INNER_GEOFENCE = 100.0;
    public static final double GEOFENCE_HYSTERESIS = 160.0;
    public static final double GEOFENCE_TO_REGION_BUFFER = 30.0;
    public static final double MAX_UNCERTAINTY = 50.0;

    // Location Service Timer Lengths
    public static final long FIVE_MINS_MILLIS = 5 * 60 * 1000;
    public static final long TWO_MINS_MILLIS  = 2 * 60 * 1000;

    // Location Fields
    public static final String USER_LATITUDE = "lat";
    public static final String USER_LONGITUDE = "long";

    //Team Member Status
    public static final String PERMIT_TEAM_STATUS_PREREQUEST = "prerequest";

    //Firebase token key
    public static final String FIREBASE_PUSH_NOTIFICATION_TOKEN = "firebase_token";

    // Document States
    public static final String DOC_BRIEFING_ACKNOWLEDGED = "acknowledged";
    public static final String DOC_BRIEFING_UNACKNOWLEDGED = "unacknowledged";
    public static final String DOC_INDUCTION_INCOMPLETE = "required";
    public static final String DOC_INDUCTION_PENDING = "pending";
    public static final String DOC_INDUCTION_COMPLETE = "acknowledged";
    public static final String DOC_INDUCTION_REJECTED = "rejected";
    public static final String DOC_INDUCTION_VISITOR ="visitor";

    // Shared Prefs Fields
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_OLD_PASSWORD = "old_password";
    public static final String USER_NEW_PASSWORD_1 = "password1";
    public static final String USER_NEW_PASSWORD_2 = "password2";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_LAST_NAME = "last_name";
    public static final String USER_PHONE = "phone_number";
    public static final String USER_COMPANY = "company";
    public static final String USER_COMPANY_ID = "company_id";
    public static final String IS_EMERGENCY = "is_emergency";
    public static final String NUM_SITES_REQUESTED = "max_sites";
    public static final String USER_EMP = "emp"; //TODO: Fuck this off
    public static final String USER_COMPANY_NAME = "company_name";
    public static final String USER_SITE_IDS = "geofences";
    public static final String USER_LOCALE = "alpha2";
    public static final String SIGN_ON_TIME = "signon_time";
    public static final String SIGN_ON_DATE = "signon_date";
    public static final String BRIEFING_ID = "briefing_id";
    public static final String BRIEFING = "briefing";
    public static final String BRIEFING_DATE = "briefing_date";
    public static final String BRIEFING_AUTHOR = "briefing_name";
    public static final String CONTEXT_COMPANY_ID = "context_company_id";
    public static final String WORKER_NOTES_KEY = "worker_notes";
    public static final String PERMITS_KEY = "permits";
    public static final String BRIEFING_SUBTYPE = "Daily";

    // JSON Constants
    public static final String JSON_LOGGED_IN = "loggedIn";
    public static final String JSON_MESSAGE = "msg";
    public static final String JSON_REGIONS = "sites";
    public static final String JSON_RESPONSE_STATUS = "status";
    public static final String JSON_COMPANY_ID = "company_id";
    public static final String JSON_SIGNED_OFF = "signedOff";
    public static final String JSON_SITE_ID = "site_id";
    public static final String JSON_SITE_TIMEZONE = "site_timezone";
    public static final String JSON_BRIEFING_ID = "briefing_id";
    public static final String JSON_BRIEFING_TEXT = "briefing";
    public static final String JSON_BRIEFING_PUBLISHED = "briefing_date";
    public static final String JSON_BRIEFING_NAME = "briefing_name";
    public static final String JSON_BRIEFING_NEEDS_ACK = "briefing_needs_acknowledgement";
    public static final String JSON_EMAIL_BAD = "bad";
    public static final String JSON_EMAIL_OKAY = "okay";
    public static final String JSON_UPDATE_OK = "update_ok";
    public static final String JSON_COMPANIES = "companies";
    public static final String JSON_REGISTERED_COMPANY_ID = "id";
    public static final String JSON_REGISTERED_COMPANY_NAME = "name";
    public static final String JSON_EMPLOYER = "employer1";
    public static final String JSON_REGISTERED = "registered";
    public static final String JSON_AUTOMATIC = "automatic";
    public static final String JSON_EVENT_LOG = "logs";
    public static final String JSON_ABILITIES = "site_abilities";
    public static final String JSON_EVACUATION_ABILITY = "can_evacuate";
    public static final String JSON_VISITOR_REGISTER_ABILITY = "can_visitor_register";
    public static final String JSON_MANAGE_USER_INDUCTIONS_ABILITY = "can_manage_user_inductions";
    public static final String JSON_MANAGE_BRIEFINGS_ABILITY = "can_manage_briefings";
    public static final String JSON_MANAGE_PERMITS = "can_manage_permits";
    public static final String JSON_USER_ID = "user_id";
    public static final String JSON_VISITS = "visits";
    public static final String JSON_VISIT_ID = "visit_id";
    public static final String JSON_VISIT_USER_ID = "user_id";
    public static final String JSON_VISIT_SITE_ID = "site_id";
    public static final String JSON_VISIT_CHECKIN = "check_in_time";
    public static final String JSON_VISIT_CHECKOUT = "check_out_time";
    public static final String JSON_ENROLLED_USERS = "users";
    public static final String JSON_VISITORS = "visitors";
    public static final String JSON_VISITOR_ID = "user_id";
    public static final String JSON_VISITOR_FIRST_NAME = "first_name";
    public static final String JSON_VISITOR_LAST_NAME = "last_name";
    public static final String JSON_VISITOR_COMPANY = "company";
    public static final String JSON_VISITOR_COMPANY_NAME = "company_name";
    public static final String JSON_VISITOR_HAS_ACTIVE_ENROLMENT = "has_active_enrolment";
    public static final String JSON_VISITOR_PHONE = "phone_number";
    public static final String JSON_VISITOR_CHECKIN_TIME = "check_in_time";
    public static final String JSON_VISITOR_LOGGED = "logged";
    public static final String JSON_BAD_PHONE = "bad_phone_number";
    public static final String JSON_RAW_NUMBER = "raw";
    public static final String JSON_GET_COMPANIES = "get_companies";
    public static final String JSON_MANAGE_ALL_COMPANIES = "attendance_companies_all";
    public static final String JSON_MANAGED_COMPANIES = "attendance_companies";
    public static final String JSON_STATUS = "status";
    public static final String JSON_SUCCESS = "success";
    public static final String JSON_IN_PROGRESS = "already_in_progress";
    public static final String JSON_INDUCTION = "induction";
    public static final String JSON_INDUCTION_FORM_AVAILABLE = "induction_form_available";
    public static final String JSON_IND_FORM_AVAILABLE = "form_available";
    public static final String JSON_INDUCTION_ID = "id";
    public static final String JSON_INDUCTION_TYPE = "type";
    public static final String JSON_INDUCTION_STATE = "state";
    public static final String JSON_IND_STATE_STRING = "as_string";
    public static final String JSON_IND_STATE_AT = "set_at";
    public static final String INDUCTED_VISITOR = "visitor";
    public static final String JSON_SIGNON_BLOCKED = "block";
    public static final String JSON_SIGNOFF_BLOCKED = "block";
    public static final String JSON_SIGNON_PROMPT = "prompt";
    public static final String JSON_SIGNOFF_PROMPT = "prompt";
    public static final String JSON_MAN_COMPANY_REGISTRATION = "manual_company_registration";

    // Job Priority
    public static int MID = 500;
    public static int HIGH = 1000;

    // Event Bus Fields
    public static final String EVENT_SIGN_ON = "signed_on";
    public static final String EVENT_SIGN_OFF = "signed_off";
    public static final String EVENT_NEW_BRIEFING = "new_briefing";

    // Realm Fields
    public static final String REALM_CONFIG_NAME = "sosConfig";
    public static final long REALM_SCHEMA_VERSION = 22;

    // Realm Model Fields
    public static final String REALM_SITE_ID = "id";

    // Endpoint URLs
    public static final String URL_LOGIN = BASE_URL + "login_exec";
    public static final String URL_EDIT_PERSONAL_DETAILS = BASE_URL + "edit_personal_details";
    public static final String URL_FORGOT_PASSWORD = "https://app.signonsite.com.au/password/reset";
    public static final String URL_TERMS_CONDITIONS = "https://documents.signonsite.com.au/app_terms_and_conditions.pdf";
    public static final String URL_PRIVACY_POLICY = "https://documents.signonsite.com.au/privacy_policy.pdf";
    public static final String URL_HELP_LINK = BASE + "support";
    public static final String URL_CURRENT_EMPLOYER = BASE_URL + "mostRecentEmp_exec";
    public static final String URL_INDUCTED_REGION_LIST = BASE_URL + "region_list";
    public static final String URL_NEARBY_REGION_LIST = BASE_URL + "get_region_list";
    public static final String URL_SIGN_OFF = BASE_URL + "signoff_exec";
    public static final String URL_SIGN_ON = BASE_URL + "signon";
    public static final String URL_UPDATE_GCM_TOKEN = BASE_URL + "update_notification_token_exec";
    public static final String URL_EMAIL_IN_USE = BASE_URL + "email_in_use";
    public static final String URL_CHANGE_PASSWORD = BASE_URL + "change_user_password";
    public static final String URL_GET_WHITECARD_INFO = BASE_URL + "get_whitecard_info";
    public static final String URL_EDIT_WHITECARD_INFO = BASE_URL + "edit_whitecard_info";
    public static final String URL_GET_ALL_EMPLOYERS = BASE_URL + "get_all_employers";
    public static final String URL_EDIT_USER_COMPANIES = BASE_URL + "edit_user_companies";
    public static final String URL_REGISTER_USER = BASE_URL + "register_exec";
    public static final String URL_USER_REGISTRATION = BASE + "mobile/api/v2/users";
    public static final String URL_ACKNOWLEDGE_BRIEFING = BASE_URL + "briefing_read";
    public static final String URL_DEVICE_INFO = BASE_URL + "versions/android";
    public static final String URL_DIAGNOSTIC_LOGS = BASE_URL + "diagnostics/log";
    public static final String URL_START_EVACUATION = BASE_URL + "start_evacuation";
    public static final String URL_STOP_EVACUATION = BASE_URL + "stop_evacuation";
    public static final String URL_CURRENT_VISITORS = BASE_URL + "current_visitors";
    public static final String URL_EVACUATION_VISITORS = BASE_URL + "evacuation_visitors";
    public static final String URL_IS_EMERGENCY = BASE_URL + "is_emergency";
    public static final String URL_TODAYS_VISITS = BASE_URL + "todays_visits";
    public static final String URL_ATTENDEES = "mobile/api/v2/sites/{site_id}/attendees";
    public static final String URL_TODAYS_VISITS_WITH_BASE = "api/todays_visits";
    public static final String URL_ENROLLED_USERS = BASE_URL + "enrolled_users";
    public static final String URL_VISITOR_SIGN_ON = BASE_URL + "visitor_sign_on";
    public static final String URL_VISITOR_SIGN_OFF = BASE_URL + "visitor_sign_off";
    public static final String URL_VALIDATE_PHONE = BASE_URL + "validate_phone";
    public static final String URL_REGISTER_VISITOR = BASE_URL + "register_visitor";
    public static final String URL_SITE_BRIEFING_LIST = BASE + "mobile/api/v2/sites/{site_id}/briefing";
    public static final String URL_SITE_USER_INFO = "mobile/api/v2/users/{user_id}";
    public static final String URL_SITE_INDUCTION_INFO = BASE_URL + "site_induction_information";
    public static final String URL_SITE_INDUCTION_INFORMATION = "api/site_induction_information";
    public static final String URL_SITE_REGISTER_FCM_TOKEN = "api/update_notification_token_exec";
    public static final String URL_GET_COMPANIES_REGISTERED = BASE_URL + "companies_for_site";
    public static final String URL_SITE_INDUCTION_BASE = BASE + "sites/";
    public static final String URL_SITE_INDUCTION_FORM = "/site_induction_forms/active";
    public static final String URL_INDUCTION_FORM_BASE = BASE + "site_inductions/";
    public static final String URL_INDUCTION_REVIEW = "/review";
    public static final String URL_INDUCTION_VIEW = "/form";
    public static final String URL_INDUCTION_VIEW_DOCS = "/files";
    public static final String URL_PHONE_VALIDATION = "api/validate_phone";
    public static final String URL_CREDENTIALS = "mobile/api/v2/users/{user_id}/credentials";
    public static final String URL_CREDENTIALS_TYPES = "mobile/api/v2/credential_types";
    public static final String URL_DELETE_CREDENTIALS = "mobile/api/v2/credentials/{credential_id}";
    public static final String URL_CONNECTIONS_ENROLMENTS ="mobile/api/v2/users/{user_id}/enrolments";
    public static final String URL_CONNECTIONS_ENROLMENTS_END_CONNECTION = "mobile/api/v2/enrolments/{enrolment_id}";
    public static final String URL_USER_FEATURE_FLAG = "mobile/api/v2/users/{user_id}/feature_flags";
    public static final String URL_SITE_FEATURE_FLAG = "mobile/api/v2/sites/{site_id}/feature_flags";
    public static final String URL_NEAR_BY_SITES = "mobile/api/v2/users/{user_id}/sites";
    public static final String URL_SHARE_PASSPORT = "mobile/api/v2/users/{user_id}/shares";
    public static final String URL_QUILL_EDITOR = "mobile/api/v2/quill_editor";
    public static final String URL_ACTIVE_BRIEFINGS = "mobile/api/v2/sites/{site_id}/active_briefing";
    public static final String URL_ACKNOWLEDGE_BRIEFINGS = "mobile/api/v2/briefings/{briefing_id}/acknowledgements";
    public static final String URL_ANALYTICS_USER_CONTEXT = "mobile/api/v2/analytics/contexts/users/{user_id}";
    public static final String URL_ANALYTICS_SITE_CONTEXT = "mobile/api/v2/analytics/contexts/sites/{site_id}";
    public static final String URL_ANALYTICS_COMPANIES_CONTEXT = "mobile/api/v2/analytics/contexts/companies/{company_id}";
    public static final String URL_SEEN_BRIEFING = "mobile/api/v2/briefings/{briefing_id}/seens";
    public static final String OLD_URL_UPDATE_BRIEFING = "api/briefing_update";
    public static final String PHOTO_URL = "files";
    public static final String NEAR_SITE = "near_site";
    public static final String ANALYTICS_URLS = "/analytics/contexts/";
    public static final String URL_IS_USER_SIGNED_ON = "api/is_signed_on";
    public static final String URL_CURRENT_PERMITS = "mobile/api/v2/sites/{site_id}/current_permits";
    public static final String URL_SITE_PERMIT_TEMPLATE = "mobile/api/v2/sites/{site_id}/site_permit_templates";
    public static final String URL_GET_PERMIT_INFO = "mobile/api/v2/permits/{permit_id}";
    public static final String URL_SET_MEMBER_STATUS_FOR_PERMIT = "mobile/api/v2/permits/{permit_id}/requestee_users/{user_id}/status";
    public static final String URL_UPDATE_MEMBERLIST_STATE_FOR_PERMIT = "mobile/api/v2/permits/{permit_id}";
    public static final String URL_SAVE_PERMIT = "mobile/api/v2/sites/{site_id}/permits";
    public static final String URL_SAVE_RESPONSES = "mobile/api/v2/permits/{permit_id}/form_inputs/responses";

    // Firebase (FCM) Constants
    public static final String GCM_TOKEN = "device_token";
    public static final String GCM_DEVICE_TYPE = "device_type";
    public static final String FCM_ANDROID = "android";
    public static final String GCM_SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String GCM_REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_MESSAGE = "message";
    public static final String FCM_NOTIFICATION = "notification";
    public static final String FCM_NOTIFICATION_BODY = "body";
    public static final String FCM_FINISH_EVAC = "finish_evacuation";
    public static final String FCM_START_EVAC = "start_evacuation";
    public static final String FCM_BRIEFING_UNACKNOWLEDGED = "briefing_unacknowledged";
    public static final String FCM_INDUCTION_REJECTED = "INDUCTION_REJECTED";
    public static final String FMC_NEW_ACTIVE_BRIEFING = "NEW_ACTIVE_BRIEFING";
    public static final String FCM_BRIEFING_REMINDER = "BRIEFING_REMINDER";
    public static final String FCM_EVACUATION_STARTED = "EVACUATION_STARTED";
    public static final String FCM_EVACUATION_ENDED = "EVACUATION_ENDED";
    public static final String FCM_WORKER_NOTE_ALERT = "WORKER_NOTE_ALERT";
    public static final String FCM_NOTIFICATION_NAME_KEY = "notification_name";
    public static final String FCM_REGISTRATION_TOKEN_ERROR = "JSON Exception occurred with ";

    // Tab Shortcut Constants
    public static final String TAB_INDEX = "tab_index";
    public static final int TAB_DOCS = 1;
    public static final int TAB_MENU = 4;
    public static final String TAB_FUNCTION = "function";
    public static final String SITE_INDUCTION = "site_ind"; // Constant for site induction section
    public static final String HAS_UNACKNOWLEDGED_BRIEFING_DOC = "unacknowledged_briefing";
    public static final String HAS_USER_TAPPED_ON_NOTIFICATION = "user_has_tabbed_on_notification";
    public static final String IS_LOCAL_NOTIFICATION = "is_local_notification";
    public static final String IS_REMOTE_NOTIFICATION = "is_remote_notification";
    public static final String HAS_METADATA = "has_metadata";
    public static final String INDUCTION_SUBMITTED = "induction_submitted";
    public static final String INDUCTION_NOTIFICATION = "induction_notification";
    public static final String BRIEFING_NOTIFICATION = "briefing_notification";
    public static final String BRIEFING_NOTIFICATION_TYPE = "briefing_notification_type";
    public static final String EMERGENCY_NOTIFICATION = "emergency_notification";
    public static final String EMERGENCY_STARTED_NOTIFICATION_TYPE = "emergency_started_notification_type";
    public static final String EMERGENCY_ENDED_NOTIFICATION_TYPE = "emergency_end_notification_type";
    public static final String NOTIFICATION_SENT_AT = "notification_sent_at";
    public static final String AUTO_SIGN_ON_OFF_NOTIFICATION = "auto_notification_on_off_notification";
    public static final String RISKY_WORKER_NOTIFICATION_TYPE = "risky_worker_notification_type";
    public static final String SHOW_PROGRESS_VIEW_KEY = "progress_view_key";

    // Document Types
    public static final String EMPTY_DOCS = "empty_docs";
    public static final String DOC_BRIEFING = "Briefing";
    public static final String DOC_EMPTY_BRIEFING = "empty_briefing";
    public static final String DOC_EMPTY_INDUCTION = "empty_induction";
    public static final String DOC_INDUCTION = "Induction";
    public static final String DOC_BRIEFING_LIST = "Briefing_List";
    public static final String DOC_ERROR = "document_error";
    public static final String DOC_PERMITS = "Permits";
    public static final String ACTIVE_BRIEFING_KEY = "active_briefing";
    public static final String INDUCTION_INCOMPLETE = "Press here to submit a site induction";
    public static final String INDUCTION_PENDING_REVIEW = "Pending Review";
    public static final String INDUCTION_COMPLETE = "Your induction has been accepted!";
    public static final String INDUCTION_REJECTED = "Rejected - please complete again";
    public static final String BRIEFING_ACKNOWLEDGED = "Acknowledged";
    public static final String BRIEFING_UNACKNOWLEDGED = "Unacknowledged";
    public static final String ACCESS_PERMITS = "Access my Permits";

    // Bundle Extras Data
    public static final String BUNDLE_EMERGENCY_INITIATOR = "initiator";

    // Attendance Register
    public static final String ATTENDANCE_REGISTER_TITLE = "Attendance Register";
    public static final String SEARCH_FOR_WORKER = "Search for a Worker";
    public static final String ADD_A_PERSON = "Add a person";
    public static final String REVIEW_INDUCTION ="Review Induction";
    public static final String REVIEW_OPTIONS ="Review Options";
    public static final String INDUCTION_STATUS = "Induction Status";
    public static final String VISITS_TITLE = "Visits";

    // Register Fragment Names
    public static final String FRAGMENT_PHONE = "phone";
    public static final String FRAGMENT_PASSWORD = "password";
    public static final String FRAGMENT_EMPLOYER = "employer";
    public static final String FRAGMENT_CONFIRMATION = "confirmation";

    // Register Constants
    public static final String REGISTER_CARD_DATE = "issue_date";
    public static final String REGISTER_CARD_NUMBER = "card_number";
    public static final String REGISTER_STATE = "state";
    public static final String REGISTER_PASS = "password";
    public static final String REGISTER_LOCALE = "alpha2";

    // Local BroadcastReceiver Constants
    public static final String BROADCAST_EVAC_START = "start_evacuation";
    public static final String BROADCAST_EVAC_STOP = "finish_evacuation";

    // Settings Constants
    public static final String SETTINGS_AUTO_SIGNON = "pref_key_auto_signon";
    public static final String SETTINGS_DIAGNOSTICS = "pref_key_diagnostics";

    // Parameter Constants
    public static final String APP_VERSION_CODE = "android_app_version_code";
    public static final String PHONE_BRAND = "android_brand";
    public static final String DATA_ENABLED = "android_data_enabled";
    public static final String PHONE_ROOTED = "android_jailbroken";
    public static final String CAN_BLOCK_AUTO_SIGNON = "feature_can_block_automatic_signon";
    public static final String SITE_ID_FOR_REALM = "siteId";
    public static final String USER_ID_FOR_REALM = "userId";

    // Pagination parameters
    public static final String PAGINATION_OFFSET = "offset";
    public static final String PAGINATION_LIMIT_NUMBER = "10";

    // Request Default Retry Policy Constants
    public static final int INITIAL_TIMEOUT = 5000;    // 5 seconds in milliseconds
    public static final int RETRY_ATTEMPTS = 2; // Times the request will be re-attempted - 3 in total including the initial request
    public static final float BACKOFF_MULTIPLIER = 1.5f; // The multiplier applied to timeout each RETRY attempt

    // Request Default Retry Policy Constants
    public static final int INITIAL_TIMEOUT_FOR_EMERGENCY = 120000;    // 120 seconds in milliseconds
    public static final int RETRY_ATTEMPTS_FOR_EMERGENCY = 0; // Times the request will be re-attempted - 1 attempts 0 RETRY
    public static final float BACKOFF_MULTIPLIER_FOR_EMERGENCY = 1.0f; // The multiplier applied to timeout each RETRY attempt

    // Additional information for passports
    public static final String INDIGENOUS_TITLE = "Indigenous Status";
    public static final String INDIGENOUS_MESSAGE = "Indigenous status can be provided to enable project based INDIGENOUS participation statistics to be calculated";
    public static final String POSTCODE_TITLE = "Post Code";
    public static final String POSTCODE_MESSAGE = "Post Code can be provided to assist in determining distance traveled to site for emissions ratings";
    public static final String GENDER_TITLE = "Gender";
    public static final String GENDER_MESSAGE = "Gender can be provided to enable project based statistics to be calculated";
    public static final String APPRENTICE_TITLE = "Apprentice";
    public static final String APPRENTICE_STATUS_MESSAGE = "Apprentice status can be provided to enable project based statistics to be calculated";
    public static final String DATE_OF_BIRTH_TITLE = "Date Of Birth";
    public static final String DATE_OF_BIRTH_MESSAGE = "Date of birth status can be provided to enable project based statistics to be calculated";
    public static final String EMERGENCY_MESSAGE = "Emergency contact information will be provided to site management on projects you work on, in case of emergency.";
    public static final String EMERGENCY_TITLE ="Emergency Info";
    public static final String MEDICAL_INFORMATION_TITLE ="Medical Information";
    public static final String MEDICAL_INFORMATION_MESSAGE = "Medical information will be provided to site management on projects you work on, in case of emergency.";
    public static final String CONSTRUCTION_EXPERIENCE_QUESTION_TITLE = "Construction Experience";
    public static final String CONSTRUCTION_EXPERIENCE_QUESTION = "Have you worked in the construction industry at any time in the last 2 years?";
    public static final String ENGLISH_NATIVE_QUESTION_TITLE = "English Native language";
    public static final String ENGLISH_NATIVE_QUESTION = "Is English your native language?";
    public static final String INTERPRETER_REQUIRED_QUESTION_TITLE = "Interpreter required";
    public static final String INTERPRETER_REQUIRED_QUESTION = "Do you require an interpreter to complete this induction?";

    //Errors
    public static final String NETWORK_MESSAGE_ERROR = "Please re-connect to wifi or mobile data and try again";
    public static final String UNKNOWN_NETWORK_ERROR_MESSAGE = "An unexpected error occurred.\\nPlease try again.";
    public static final String NETWORK_MESSAGE_TITLE = "No network available";
    public static final String ERROR_RX_LOG_TITLE = "RX Error";

    //Passport - Personal Constants
    public static final String NOT_PROVIDED = "Not provided";
    public static final String ALERT_DIALOG_BUTTON_TITLE = "Dismiss";
    public static final String SPINNER_CUSTOM_OPTION = "custom";
    public static final String SELECT_DATE_OF_BIRTH = "Select date of Birth";
    public static final String AU_CODE = "AU";
    public static final String RETRY = "Retry";
    public static final String INDIGENOUS = "Indigenous";
    public static final String NOT_INDIGENOUS = "Not Indigenous";
    public static final String APPRENTICE = "Apprentice";
    public static final String NOT_APPRENTICE = "Not Apprentice";

    //AlertDialogs
    public static final String ALERT_DIALOG_TITLE_FOR_SAVE = "Unsaved changes will be lost";
    public static final String ALERT_DIALOG_MESSAGE_FOR_SAVE = "Are you sure you want to proceed";
    public static final String ALERT_DIALOG_PROCEED_OPTION = "Proceed";
    public static final String ALERT_DIALOG_GO_BACK_OPTION = "Go back and save";
    public static final String ALERT_DIALOG_PERMISSION_INDUCTION_TITLE = "Let SignOnSite Access Camera and take photos";
    public static final String ALERT_DIALOG_PERMISSION_INDUCTION_MESSAGES = "SignOnSite needs to access the camera and phone storage to allow you " +
            "to take photos and upload them with your induction form.";
    public static final String ALERT_DIALOG_PERMISSION_POSITIVE_BUTTON_TITLE = "Give Access";
    public static final String ALERT_DIALOG_PERMISSION_NEGATIVE_BUTTON_TITLE = "Not Now";
    public static final String ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_TITLE = "Congratulations! You're ready to go";
    public static final String ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_MESSAGE = "Click on the photo buttons to get started!";
    public static final String ALERT_DIALOG_GRANTED_PERMISSION_INDUCTION_POSITIVE_BUTTON_TITLE = "Let\'s do this";


    //PhonePicker
    public static final String VALIDATION_PHONE_TITLE = " Error phone number validation";
    public static final String VALIDATION_PHONE_MESSAGE = "We were not able to validate this phone number. Please check if it is a real phone number or contact support.";
    public static final String FRAGMENT_PHONE_PICKER_BACK_STACK = "phone_picker";
    public static final String TOOL_BAR_PICKER_TITLE = "Change Phone Number";
    public static final String USER_FLAG = "user";

    //credentials
    public static final String CREDENTIAL_FLAG = "credential";
    public static final String CONNECTION_FLAG = "connection_flag";
    public static final String CREDENTIAL_CREATION_FLAG = "credential_creation";
    public static final String FRAGMENT_CREDENTIAL_UPDATE_BACK_STACK = "credential_update";
    public static final String FRAGMENT_CREDENTIAL_TYPES_BACK_STACK = "credential_types";
    public static final String FRAGMENT_CREDENTIAL_CREATION_BACK_STACK = "credential_types";
    public static final String FRAGMENT_CONNECTION_LIST_BACK_STACK = "connections_list";
    public static final String FRAGMENT_SHARE_BACK_STACK = "share_passport";
    public static final Integer IMAGE_CAPTURE_FRONT_FLAG_CODE = 1;
    public static final Integer IMAGE_CAPTURE_BACK_FLAG_CODE = 2;
    public static final String REQUEST_CAMERA_PERMISSION_ALERT_TITLE = "Let SignOnSite allow you to take Photos";
    public static final String REQUEST_CAMERA_PERMISSION_ALERT_MESSAGE = "This allows you to use your phone's camera with SignOnSite to save your competencies and tickets on your device.";
    public static final String REQUEST_PERMISSION_POSITIVE_BUTTON_TITLE = "Give Access";
    public static final String REQUEST_PERMISSION_NEGATIVE_BUTTON_TITLE = "Not Now";
    public static final String REQUEST_STORAGE_PERMISSION_ALERT_TITLE ="Let SignOnSite save your Credentials to your Phone";
    public static final String REQUEST_STORAGE_PERMISSION_ALERT_MESSAGE = "This allows SignOnSite to save images of your tickets and competencies so that you can easily show them to Site Managers.";
    public static final String CAPTURE_CAMERA_CANCELED = "The camera capture was canceled!";
    public static final String FIELDS_VALIDATION_ALERT_TITLE ="Details";
    public static final String FIELDS_VALIDATION_ALERT_MESSAGE ="Mandatory fields for this credential have not been populated. Also please check expiry date and issued date have been selected correctly.";
    public static final String FIELDS_VALIDATION_ALERT_BUTTON_TITLE = "Continue";
    public static final String DELETE_ALERT_DIALOG_TITLE = "Delete";
    public static final String DELETE_ALERT_DIALOG_MESSAGE = "Deleting this credential will remove it from your passport.\n" + "\n" + "Would you like to delete this credential?";
    public static final String DELETE_ALERT_BUTTON_POSITIVE_TITLE = "Yes, delete";
    public static final String DELETE_ALERT_BUTTON_NEGATIVE_TITLE = "No";
    public static final String REMOVE_SITE_CONNECTION_ALERT_DIALOG_TITLE = "Remove site connection";
    public static final String REMOVE_SITE_CONNECTION_ALERT_DIALOG_MESSAGE ="Ending connection with this site will mean you will no longer automatically sign on, and you wont see it in your connections list until you manually sign on again. \n \n Do you want to remote this connection?";
    public static final String REMOVE_SITE_CONNECTION_ALERT_BUTTON_POSITIVE_TITLE = "Yes";
    public static final String REMOVE_SITE_CONNECTION_ALERT_BUTTON_NEGATIVE_TITLE = "No";
    public static final String SHARE_PASSPORT_ALERT_DIALOG_TITLE = "Success";
    public static final String SHARE_PASSPORT_ALERT_DIALOG_MESSAGE ="You have successfully shared your passport with ";
    public static final String SHARE_PASSPORT_ALERT_BUTTON_POSITIVE_TITLE = "Continue";
    public static final int REQUEST_CAMERA = 2;
    public static final String FRONT_PHOTO = "FrontPhoto";
    public static final String BACK_PHOTO = "BackPhoto";
    public static final String HEAD_SHOT_PHOTO = "HeadShotPhoto";
    public static final int REQUEST_STORAGE_ACCESS = 3;
    public static final String IMAGE_BITMAP_FLAG = "data";
    public static final String[] PERMISSIONS_STORAGE = { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
    public static final String REQUIRED_FIELD = "required";
    public static final String NONE_FIELD = "none";
    public static final String OPTIONAL = "optional";
    public static final String NAME = "Name";
    public static final String ID_NUMBER = "Id Number";
    public static final String ISSUE_DATE = "Issue date";
    public static final String EXPIRY_DATE = "Expiry date";
    public static final String ISSUED_BY = "Issued By";
    public static final String RTO = "RTO";
    public static final String REGISTRATION_NUMBER = "Registration Number";
    public static final String REFERENCE = "Reference";
    public static final String PERSONAL_TAB_NOTIFICATION = "personal_tab_notification";
    public static final String EMERGENCY_TAB_NOTIFICATION = "emergency_tab_notification";
    public static final String CREDENTIAL_TAB_NOTIFICATION = "credential_tab_notification";
    public static final String CONNECTION_TAB_NOTIFICATION = "connection_tab_notification";
    public static final String DOCUMENTS_SIGNED_MAIN_TAB_NOTIFICATION = "documents_signed_main_tab_notification";
    public static final String PASSPORT_SIGNED_MAIN_TAB_NOTIFICATION = "passport_signed_main_tab_notification";
    public static final String CUSTOM_LICENSE = "Custom Licence";
    public static final String NOT_PROVIDED_HINT = "Not Provided";
    public static final String NOT_SELECTED = "Not selected";
    public static final String CREDENTIAL_TYPES_NAME = "Credential Types";
    public static final String REQUIRED ="required";
    public static final String SHARE_PASSPORT = "Share Passport";
    public static final String POSITIVE_CONFIRMATION = "Yes";
    public static final String NEGATIVE_CONFIRMATION = "No";
    public static final String FIRST_SITE_ON_TITLE = "First Sign On";

    //Passport
    public static final String PERSONAL_INFO_SUCCESS ="success";
    public static final String PERSONAL_INFO_UNAUTHORISED ="unauthorised";
    public static final String PERSONAL_INFO_UNAUTHENTIC ="unauthentic";
    public static final String PERSONAL_INFO_BAD_INPUT = "bad_input";
    public static final String PERSONAL_INFO_BAD_PHONE_NUMBER = "bad_phone_number";
    public static final String PERSONAL_INFO_NO_PASSPORT ="no_passport";
    public static final String PERSONAL_INFO_UNAUTHORISED_MESSAGE ="unauthorised user info";
    public static final String PERSONAL_INFO_UNAUTHENTIC_MESSAGE ="unauthentic user info";
    public static final String PERSONAL_INFO_BAD_INPUT_MESSAGE ="bad user input";
    public static final String PERSONAL_INFO_BAD_PHONE_NUMBER_MESSAGE ="bad phone number";
    public static final String PERSONAL_INFO_NO_PASSPORT_MESSAGE = "user has not passport";

    //Status state
    public static final String RESPONSE_SUCCESS ="success";
    public static final String RESPONSE_UNAUTHORISED ="unauthorised";
    public static final String RESPONSE_UNAUTHENTIC ="unauthentic";
    public static final String RESPONSE_BAD_INPUT = "bad_input";
    public static final String RESPONSE_NOT_SIGNED_ON_TO_BRIEFINGS_SITE = "user_not_signed_on_to_briefings_site";
    public static final String RESPONSE_SITE_ARCHIVED = "site_archived";
    public static final String RESPONSE_UNAUTHORISED_MESSAGE ="unauthorised user info";
    public static final String RESPONSE_UNAUTHENTIC_MESSAGE ="unauthentic user info";
    public static final String RESPONSE_BAD_INPUT_MESSAGE ="bad user input";
    public static final String RESPONSE_NOT_SIGNED_ON_TO_BRIEFINGS_SITE_MESSAGE = "user not signed on to briefings site";
    public static final String RESPONSE_SITE_ARCHIVED_MESSAGE = "this site is archived";
    public static final String RESPONSE_START_DATE_BEFORE_TODAY = "start_date_before_today";
    public static final String RESPONSE_START_DATE_BEFORE_TODAY_MESSAGE = "start date before today";
    public static final String RESPONSE_END_DATE_BEFORE_START_DATE = "end_date_before_start_date";
    public static final String RESPONSE_END_DATE_BEFORE_START_DATE_MESSAGE = "end date before start date";
    public static final String RESPONSE_CREDENTIAL_NOT_MATCH ="credential_does_not_match_type";
    public static final String RESPONSE_CREDENTIAL_NOT_MATCH_MESSAGE ="Credential does not match type";
    public static final String RESPONSE_SHARED_PASSPORT_BAD_EMAIL = "bad_email";
    public static final String RESPONSE_SHARED_PASSPORT_BAD_EMAIL_MESSAGE = "Bad email";
    public static final String RESPONSE_NO_PASSPORT = "no_passport";
    public static final String RESPONSE_NO_PASSPORT_MESSAGE = "No passport";


    public static final String FIRST_SITE_ON_POSITIVE_BUTTON = "Connect";
    public static final String FIRST_SITE_ON_NEGATIVE_BUTTON = "Cancel";

    //Feature Flags
    public static final String PASSPORTS ="passports";
    public static final String PASSPORT_SHARE = "passport_share";

    //ProgressView Messages
    public static final String CREATING_PASSPORT ="Creating passport";
    public static final String PASSPORT_TAB_TITLE = "Passport";
    public static final String LICENSES_TAB_TITLE = "Licenses";

    //Registration Messages
    public static final String BAD_EMAIL = "The entered email address appears to have a mistake in it, please check it carefully and try again.";
    public static final String EMAIL_IN_USE ="This email address is already in use.";
    public static final String DATA_FORMAT_ERROR_TITLE ="Format error";
    public static final String PHONE_DATA_FORMAT_ERROR = "Please make sure your number only contains numbers.";
    public static final String PHONE_DATA_LENGTH_ERROR = "Please enter a valid phone number.";
    public static final String PASSWORD_EMPTY_ERROR = "Password cannot be empty!";
    public static final String PASSWORD_MAX_LENGTH_ERROR = "Your password cannot be more than 72 characters!";
    public static final String PASSWORD_MIN_LENGTH_ERROR = "Your password must be at least 6 characters!";
    public static final String PASSWORDS_DO_NOT_MATCH = "Oops! Your passwords do not match.";

    //Registration Errors
    public static final String ALREADY_REGISTERED = "already_registered";
    public static final String RATE_LIMITED = "rate_limited";
    public static final String ALREADY_REGISTERED_MESSAGE = "Account is already registered with that email and a different password";
    public static final String BAD_AUTH = "bad_auth";
    public static final String BAD_AUTH_MESSAGE = "bad email verification token provided";
    public static final String BAD_PASSWORD = "bad_password:";
    public static final String BAD_PASSWORD_MESSAGE = "Password must be 6 characters or more";
    public static final String BAD_PHONE_NUMBER = "bad_phone_number";
    public static final String BAD_PHONE_NUMBER_MESSAGE = "Please check your phone number carefully.";
    public static final String BAD_PHONE_NUMBER_VALIDATION_ERROR = "There was an error with your phone number, please insert your country code and try again.";
    public static final String RATE_LIMIT_ERROR_MESSAGE = "You have made too many log in or registration attempts, too quickly. Please contact SignOnSite if you require support.";
    public static final String BAD_FIRST_NAME_ALERT_DIALOG_ERROR = "Please make sure your first \" +\n" +
            "                            \"name contains only letters, hyphens or apostrophes";
    public static final String BAD_LAST_NAME_ALERT_DIALOG_ERROR = "Please make sure your first \\\" +\\n\" +\n" +
            "            \"                            \\\"name contains only letters, hyphens or apostrophes";
    public static final String BAD_EMAIL_ALERT_DIALOG_ERROR = "Please enter a valid email address";
    public static final String EMPTY_FIRST_NAME = "Please enter your first name";
    public static final String EMPTY_LAST_NAME = "Please enter your last name";
    public static final String EMPTY_EMAIL_TEXT = "Please enter your email";
    public static final String EMPTY_EMPLOYER = "Please enter an employer";

    //Rich text briefings
    public static final String RICH_TEXT = "rich";
    public static final String PLAIN_TEXT = "plain";
    public static final String BRIEFINGS_TOOLBAR_TITLE = "Daily Briefings";

    //Evacuation
    public static final String EVACUATION_IN_PROGRESS = "Evacuation in progress";

    //SharedPreferences
    public static final String SAVE_USER_ID = "save_user_id";

    //Reachability
    public static final String WIFI = "wifi";
    public static final String CELLULAR = "cellular";
    public static final String NOT_CONNECTED = "not_connected";
    public static final String UNKNOWN_CONNECTION = "unknown";

    //Service background options for segment
    public static final String AVAILABLE = "available";
    public static final String DENIED = "denied";
    public static final String ALWAYS = "always";

    //Device context
    public static final String MOBILE = "mobile";
    public static final String DEVICE_APP = "device_app";
    public static final String DEVICE_AUTO_SIGN_ON = "device_auto_sign_on";
    public static final String DEVICE_BACKGROUND_APP_REFRESH = "device_background_app_refresh";
    public static final String DEVICE_BATTERY_LEVEL = "device_battery_level";
    public static final String DEVICE_LOCATION_ENABLE = "device_location_enabled";
    public static final String DEVICE_LOCATION_PERMISSION = "device_location_permission";
    public static final String DEVICE_PUSH_NOTIFICATION_PERMISSION = "device_push_notification_permission";
    public static final String DEVICE_REACHABILITY = "device_reachability";
    public static final String DEVICE_WIFI_ENABLE = "device_wifi_enabled";
    public static final String DEVICE_PUSH_TOKEN = "device_push_token";
    public static final String DEVICE_LOW_POWER_MODE = "device_low_power_mode";
    public static final String DEVICE_IGNORING_BATTERY_OPTIMISATION = "device_ignoring_battery_optimisation";
    public static final String ANDROID = "android";

    //User context
    public static final String SEG_USER_COMPANY_ID = "user_company_id";
    public static final String SEG_TARGETED_USER_ID = "targetted_user_id";


    //Company context
    public static final String SEG_COMPANY_ID = "company_id";

    //Context errors
    public static final String SEG_USER_CONTEXT_ERROR = "User Context Error";
    public static final String SEG_SITE_CONTEXT_ERROR = "Site Context Error";
    public static final String SEG_COMPANY_CONTEXT_ERROR = "Company Context Error";

    //Segment Events
    public static final String ANDROID_APP_REGISTRATION_COMPLETED = "ANDROID_APP_REGISTRATION_COMPLETED";
    public static final String ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_STARTED = "ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_STARTED";
    public static final String ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_FILE_UPLOADED = "ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_FILE_UPLOADED";
    public static final String ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SIGNATURE_UPLOADED = "ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SIGNATURE_UPLOADED";
    public static final String ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SUBMITTED = "ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SUBMITTED";
    public static final String ANDROID_APP_PASSPORT_CREATED = "ANDROID_APP_PASSPORT_CREATED";
    public static final String ANDROID_APP_PASSPORT_SHARE_SENT = "ANDROID_APP_PASSPORT_SHARE_SENT";
    public static final String ANDROID_APP_PASSPORT_CREDENTIALS_UPDATED = "ANDROID_APP_PASSPORT_CREDENTIALS_UPDATED";
    public static final String ANDROID_APP_PASSPORT_CREDENTIALS_REMOVED = "ANDROID_APP_PASSPORT_CREDENTIALS_REMOVED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REVIEWED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REVIEWED";
    public static final String ANDROID_APP_MANAGER_REGISTER_WORKER_NOTES_VIEWED = "ANDROID_APP_MANAGER_REGISTER_WORKER_NOTES_VIEWED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_NEW_WORKER_SIGN_ON_PRESSED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_NEW_WORKER_SIGN_ON_PRESSED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_ON_PRESSED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_ON_PRESSED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_OFF_PRESSED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_OFF_PRESSED ";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_VIEWED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_VIEWED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_VIEWED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_VIEWED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_SELECTED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_SELECTED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_NEW_PRESSED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_NEW_PRESSED";
    public static final String ANDROID_APP_MANAGER_EVACUATION_DRILL_TRIGGERED = "ANDROID_APP_MANAGER_EVACUATION_DRILL_TRIGGERED";
    public static final String ANDROID_APP_MANAGER_EVACUATION_EMERGENCY_TRIGGERED = "ANDROID_APP_MANAGER_EVACUATION_EMERGENCY_TRIGGERED";
    public static final String ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_VIEWED = "ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_VIEWED";
    public static final String ANDROID_APP_MANAGER_EVACUATION_COMPLETED = "ANDROID_APP_MANAGER_EVACUATION_COMPLETED";
    public static final String ANDROID_APP_SITE_DOCS_BRIEFING_OPENED = "ANDROID_APP_SITE_DOCS_BRIEFING_OPENED";
    public static final String ANDROID_APP_SITE_DOCS_BRIEFING_ACKNOWLEDGED = "ANDROID_APP_SITE_DOCS_BRIEFING_ACKNOWLEDGED";
    public static final String ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADING_STARTED = "ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADING_STARTED";
    public static final String ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADED = "ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADED";
    public static final String ANDROID_APP_SIGNED_ON = "ANDROID_APP_SIGNED_ON";
    public static final String ANDROID_APP_SIGNED_OFF = "ANDROID_APP_SIGNED_OFF";
    public static final String ANDROID_APP_NOTIFICATION_RECEIVED = "ANDROID_APP_NOTIFICATION_RECEIVED";
    public static final String ANDROID_APP_NOTIFICATION_CLICKED = "ANDROID_APP_NOTIFICATION_CLICKED";
    public static final String ANDROID_APP_REGISTRATION_STARTED = "ANDROID_APP_REGISTRATION_STARTED";
    public static final String ANDROID_APP_REGISTRATION_NAME_AND_EMAIL_PROVIDED = "ANDROID_APP_REGISTRATION_NAME_AND_EMAIL_PROVIDED";
    public static final String ANDROID_APP_REGISTRATION_PHONE_PROVIDED = "ANDROID_APP_REGISTRATION_PHONE_PROVIDED";
    public static final String ANDROID_APP_REGISTRATION_PASSWORD_PROVIDED = "ANDROID_APP_REGISTRATION_PASSWORD_PROVIDED";
    public static final String ANDROID_APP_REGISTRATION_COMPANY_NAME_PROVIDED = "ANDROID_APP_REGISTRATION_COMPANY_NAME_PROVIDED";
    public static final String ANDROID_APP_MENU_HELP_OPENED = "ANDROID_APP_MENU_HELP_OPENED";


    //Segment Properties
    public static final String ANALYTICS_METADATA = "analytics_metadata";
    public static final String NOTIFICATION_NAME = "notification_name";
    public static final String ANALYTICS_NOTIFICATION_TYPE = "notification_type";
    public static final String ANALYTICS_NOTIFICATION_SENT_AT = "utc_sent_at";
    public static final String ANALYTICS_DELAY_SECONDS = "delay_seconds";
    public static final String ANALYTICS_LOCAL_NOTIFICATION = "local";
    public static final String ANALYTICS_REMOTE_NOTIFICATION = "remote";
    public static final String NEW_ACTIVE_BRIEFING = "NEW_ACTIVE_BRIEFING";
    public static final String UNIQUE_ID = "unique_id";
    public static final String IS_NEW_FORM = "is_new_form";
    public static final String TYPE = "type";
    public static final String FIRST = "first";
    public static final String COMPANY_INPUT_TYPE = "company_input_type";
    public static final String CUSTOM_COMPANY_NAME = "custom_company_name";
    public static final String CUSTOM_COMPANY_PROPERTY = "custom";
    public static final String AUTO_COMPANY_PROPERTY = "auto";
    public static final String FROM_LIST_COMPANY_PROPERTY = "from_list";
    public static final String RECEIVER_EMAIL = "receiver_email";

    //Webview Events
    public static final String SITE_INDUCTION_FORM_FILE_UPLOADED = "SITE_INDUCTION_FORM_FILE_UPLOADED";
    public static final String SITE_INDUCTION_FORM_SIGNATURE_UPLOADED = "SITE_INDUCTION_FORM_SIGNATURE_UPLOADED";
    public static final String SITE_INDUCTION_FORM_SUBMITTED = "SITE_INDUCTION_FORM_SUBMITTED";
    public static final String MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED = "MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED";
    public static final String MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED = "MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED";
    public static final String SITE_INDUCTION_FORM_LOADED_FOR_SUBMISSION = "SITE_INDUCTION_FORM_LOADED_FOR_SUBMISSION";

    //Webview interface
    public static final String WEBVIEW_JAVASCRIPT_INTERFACE = "signonsite_message_channel";


    //Notifications titles and messages
    public static final String BRIEFING_REMOTE_ACTION = "Briefing";
    public static final String SIGNON_PROMPT_NOTIFICATION_MESSAGE = "You may be near a work site. You are not signed on yet - press here to open the app and then sign on.";
    public static final String SIGNOFF_PROMPT_NOTIFICATION_MESSAGE = "You may have left the site boundary for site . You are still signed on - To sign off, press here to open the app and sign off.";
    public static final String SIGNONOFF_LOCAL_NOTIFICATION_INTENT_ACTION = "SignOnOff";
    public static final String ATTENDANCE_SIGNED_ON_NOTIFICATION_MESSAGE_START = "You have signed on to ";
    public static final String ATTENDANCE_SIGNED_OFF_NOTIFICATION_MESSAGE_COMPLETE = "You have just left site and been signed off.";
    public static final String ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_BRIEFING_END = ". Click here to read the briefing.";
    public static final String ATTENDANCE_SIGNED_ON_NOTIFICATION_NO_INDUCTION_END = ". Click here to complete induction.";
    public static final String ATTENDANCE_SIGNED_ON_NOTIFICATION_INCOMPLETE_INDUCTION_INCOMPLETE_BRIEFING_MESSAGE_END = ". Click here to complete induction and read the briefing. ";
    public static final String PROMPTED_SIGN_ON = "PROMPTED_SIGN_ON";
    public static final String PROMPTED_SIGN_OFF = "PROMPTED_SIGN_OFF";
    public static final String AUTO_SIGN_ON = "AUTO_SIGN_ON";
    public static final String AUTO_SIGN_OFF = "AUTO_SIGN_OFF";
    public static final String EVACUATION_STARTED = "EVACUATION_STARTED";
    public static final String EVACUATION_ENDED = "EVACUATION_ENDED";


    //Notifications Id's
    public static final int NOTIFICATION_DEFAULT_ID = 1;
    public static final int NOTIFICATION_ATTENDANCE_ID = 2;
    public static final int NOTIFICATION_BRIEFING_ID = 8;
    public static final int NOTIFICATION_INDUCTION_ID = 27;
    public static final int NOTIFICATION_RISKY_WORKER_ID = 441;
    public static final int NOTIFICATION_EVACUATION_ID = 999999;
    public static final int NOTIFICATION_END_EVACUATION_ID = 111111;
    public static final int NOTIFICATION_PROMPT_SIGNON_ID = 39;
    public static final String SIGNON_OFF_GROUP_NOTIFICATIONS = "SIGN_ON_OFF_GROUP";
    public static final String SIGNON_ON_GROUP_NOTIFICATIONS = "SIGN_ON_ON_GROUP";


    //Notifications Channels
    public static final String ATT_CHANNEL_ID = "attendance_channel";
    public static final String BG_CHANNEL_ID = "background_channel";
    public static final String BRIEF_CHANNEL_ID = "briefing_channel";
    public static final String DEFAULT_CHANNEL_ID = "default_channel";
    public static final String DEFAULT_CHANNEL_NAME = "Default Notification";
    public static final String DEFAULT_CHANNEL_DESCRIPTION = "Unhandled messages";
    public static final String EMERGENCY_CHANNEL_ID = "emergency_channel";
    public static final String EMERGENCY_END_CHANNEL_ID ="emergency_end_channel";
    public static final String INDUCTION_CHANNEL_ID = "induction_channel";
    public static final String RISKY_WORKER_CHANNEL_ID = "risky_worker_channel";
    public static final String PROMPT_SIGNON_CHANNEL_ID = "prompted_signon_channel";
    public static final String BRIEFING_CHANNEL_NAME = "Briefing notifications";
    public static final String BRIEFING_CHANNEL_DESCRIPTION = "Notifications for when the site has a briefing set.";
    public static final String INDUCTION_CHANNEL_NAME = "Induction status notifications";
    public static final String INDUCTION_CHANNEL_DESCRIPTION = "Site Induction notifications.";
    public static final String RISKY_WORKER_CHANNEL_NAME = "Risky worker notification";
    public static final String RISKY_WORKER_CHANNEL_DESCRIPTION = "Risky worker notification description";
    public static final String EMERGENCY_CHANNEL_NAME = "Emergency notifications";
    public static final String EMERGENCY_CHANNEL_DESCRIPTION = "Emergency notifications";
    public static final String ATTENDANCE_CHANNEL_NAME = "Attendance notifications";
    public static final String ATTENDANCE_CHANNEL_DESCRIPTION = "Site attendance notifications";
    public static final String PROMPTED_SIGN_ON_OFF_CHANNEL_NAME = "Prompted signOn notifications";
    public static final String PROMPTED_SIGN_ON_OFF_DESCRIPTION = "Site prompted signOn notifications.";
    // Deprecated Channels
    public static final String OLD_EVAC_CHANNEL_ID = "evac_channel";

    //Progressbar titles and messages
    public static final String LOADING_PLAIN_TITLE = "Loading..";
    public static final String LOADING_TITLE_KEY = "loading_title";
    public static final String LOADING_MESSAGE_KEY = "loading_message";
    public static final String PROGRESS_BAR_FRAGMENT_BACKSTACK = "progress_bar";
    public static final String PROGRESS_VIEW_HIDE_ACTION_BAR = "hide_action_bar";

    //SOS versions
    public static final Integer SOS_VERSION = 147;

    //Test constants
    public static final int USER_TEST_ID = 1;
    public static final String USER_TEST_EMAIL = "alexanderparra@me.com";
    public static final int SITE_TEST_ID = 1;
    public static final String COUNTER_IDLING_EXCEPTION_MESSAGE = "Counter has been corrupted!";

    //Permissions Constants
    public static final String BACKGROUND_LOCATION = "android.permission.ACCESS_BACKGROUND_LOCATION";

    //Registration logs
    public static final String USER_REGISTRATION_EMAIL = "User_email";
    public static final String USER_REGISTRATION_ID = "User Id";
    public static final String USER_DETAILS_REGISTRATION_STEP_ONE = "User details step 1";
    public static final String USER_PHONE_NUMBER_REGISTRATION_STEP_TWO ="User phone number step 2";
    public static final String USER_PASSWORD_STEP_THREE = "User password step 3";
    public static final String USER_COMPANY_NAME_STEP_FOUR = "User company name step 4";
    public static final String USER_CLICK_ON_REGISTER_BUTTON_STEP_FIVE = "User click on register button step 5";
    public static final String USER_COMPLETE_REGISTRATION_STEP_SIX ="User complete registration step 6";

    //Permits Description
    public static final String PERMIT_STATE_REQUEST = " is being drafted";
    public static final String PERMIT_STATE_PENDING_APPROVAL = " requires approval";
    public static final String PERMIT_STATE_APPROVED = " has been approved";
    public static final String PERMIT_STATE_CLOSING = " is pending closure";

    //Permits
    public static final String PERMIT_OBJECT = "permit_object";
    public static final String FULL_PERMIT_OBJECT = "full_permit_object";
    public static final String PERMIT_TEMPLATE_OBJECT = "permit_template_object";
    public static final String PERMIT_INFO_OBJECT = "permit_info_object";
    public static final String PERMIT_IMAGE_URL = "permit_image_url";
    public static final String PERMIT_FINISHED_WORK = "finished_work";
    public static final String PERMIT_FINISHED_REQUEST = "finished_request";
    public static final String PERMIT_PENDING_APPROVAL = "pending_approval";
    public static final String PERMIT_REQUESTER_CANCELLED = "requester_cancelled";
    public static final String PERMIT_APPROVER_CANCELLED = "approver_cancelled";

    //List permits titles
    public static final String PERMIT_REQUEST_TITLE = "Request";
    public static final String PERMIT_PENDING_APPROVAL_TITLE = "Pending Approval";
    public static final String PERMIT_APPROVED_TITLE = "Approved";
    public static final String PERMIT_CLOSING_TITLE = "Closing";
    public static final String PERMIT_CONNECTOR = " permit ";

    //Navigation Tag
    public static final String CURRENT_PERMITS_TAG = "current_permits_tag";
    public static final String ADD_TEAM_MEMBERS_TAG = "add_team_members_tag";
    public static final String PERMIT_DETAILS_TAG = "permit_details_tag";
    public static final String CTA_CONTEXTUAL_FRAGMENT_TAG = "selector_fragment_tag";
    public static final String PROFILE_FILE_PHOTO = "profilePhoto";

    //Permit states
    public static final String PERMIT_REQUEST_STATE = "request";
    public static final String PERMIT_PENDING_APPROVAL_STATE = "pending_approval";
    public static final String PERMIT_IN_PROGRESS_STATE = "in_progress";
    public static final String PERMIT_PENDING_CLOSURE_STATE = "pending_closure";
    public static final String PERMIT_CLOSED_STATE = "closed";
    public static final String PERMIT_REQUESTER_CANCELLED_STATE = "requester_cancelled";
    public static final String PERMIT_APPROVER_CANCELLED_STATE = "approver_cancelled";
    public static final String PERMIT_SUSPENDED_STATE = "suspended";
    public static final String PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB = "hh:mm a, dd MMMM yyyy";
    public static final String PERMIT_CALENDAR_DATE_TIME = "EEE MMM dd HH:mm:ss 'GMT'Z yyyy";
    public static final String PERMIT_CONTENT_TYPE_PLAIN_TEXT = "fixed_text";
    public static final String PERMIT_CONTENT_TYPE_CHECKBOX = "checkbox";
    public static final String PERMIT_CONTENT_TYPE_PHOTO = "photo";
    public static final String PERMIT_CONTENT_TYPE_TEXT_INPUT = "text_input";

    //CameraX constants
    public static final String CAMERAX_PASSING_OBJECT = "camerax_passing_object";

    // Image Source Picker
    public static final String IMAGE_SOURCE_PICKER_LABEL = "Select Source";

}
