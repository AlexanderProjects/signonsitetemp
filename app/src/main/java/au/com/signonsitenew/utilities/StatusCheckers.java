package au.com.signonsitenew.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.provider.Settings;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class StatusCheckers {

    public static boolean gpsEnabled(Context context) {
        android.location.LocationManager locationMan = (android.location.LocationManager)context
                .getSystemService(Context.LOCATION_SERVICE);

        return locationMan.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
    }

    public static boolean wifiEnabled(Context context) {
        WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

        return wifi.isWifiEnabled();
    }

    public static float getBatteryLevel(Context context) {
        Intent batteryStatus = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        return ((float)level / (float)scale) * 100.0f;
    }


    /**
     * Prompts for the user to modify their settings
     */
    public static void displayPromptForEnablingGps(final Context context) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Please enable GPS to use SignOnSite. " +
                "Tap OK to go to settings.";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                context.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public static void displayPromptForEnablingWiFi(final Context context) {
        final AlertDialog.Builder builder =
                new AlertDialog.Builder(context);
        final String action = Settings.ACTION_WIFI_SETTINGS;
        final String message = "Please enable WiFi to help increase your GPS accuracy. " +
                "Tap OK to go to settings.";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                context.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

}
