package au.com.signonsitenew.utilities

import android.annotation.SuppressLint
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.EditText

class PhoneNumberTextWatcher(private val phoneNumberEditText: EditText, private val callActionOnPhoneEditText: ()->Unit) : PhoneNumberFormattingTextWatcher() {
    private var isEditing = false
    private lateinit var textBeforeChange:String

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        textBeforeChange = s.toString()
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val value = s.toString().replace("\\s".toRegex(), "")
        if(isEditing && count > 0 && value != textBeforeChange){
            callActionOnPhoneEditText()
            isEditing = false
        }
    }

    init { registerListener() }

    @SuppressLint("ClickableViewAccessibility")
    fun registerListener(){
        phoneNumberEditText.setOnTouchListener { v: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP && !v.hasFocus()) {
                v.performClick()
                isEditing = true
            }
            false
        }
    }
}