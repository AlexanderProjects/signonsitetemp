package au.com.signonsitenew.models;

public class BadgeNotification {

    private Integer badgeNumber;
    private String notifier;


    public BadgeNotification(Integer badgeNumber, String notifier) {
        this.badgeNumber = badgeNumber;
        this.notifier = notifier;
    }

    public Integer getBadgeNumber() {
        return badgeNumber;
    }

    public String getNotifier() {
        return notifier;
    }

}
