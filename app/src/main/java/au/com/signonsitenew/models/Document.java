package au.com.signonsitenew.models;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * Generic model that briefings, inductions, etc are passed to with high-level information to then
 * pass to the DocumentRecyclerViewAdapter for display.
 */

public class Document {
    private String docType;
    private String subtype;
    private String state;

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
