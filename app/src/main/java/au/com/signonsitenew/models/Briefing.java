package au.com.signonsitenew.models;

import java.util.Date;

public class Briefing {

    private Integer id;
    private Integer number;
    private Date start_date;
    private Date end_date;
    private Boolean active;
    private Integer acknowledgement_count;
    private Integer seen_count;
    private String type;
    private String content;
    private String content_text;
    private String signature;
    private transient String reminders;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getAcknowledgement_count() {
        return acknowledgement_count;
    }

    public void setAcknowledgement_count(Integer acknowledgement_count) {
        this.acknowledgement_count = acknowledgement_count;
    }

    public Integer getSeen_count() {
        return seen_count;
    }

    public void setSeen_count(Integer seen_count) {
        this.seen_count = seen_count;
    }

    public String getType() {
        return type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getReminders() {
        return reminders;
    }

    public void setReminders(String reminders) {
        this.reminders = reminders;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_text() {
        return content_text;
    }

    public void setContent_text(String content_text) {
        this.content_text = content_text;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }


}
