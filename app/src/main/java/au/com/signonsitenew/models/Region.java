package au.com.signonsitenew.models;

import android.location.Location;

import au.com.signonsitenew.utilities.Constants;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class Region {
    private static final String LOG = "LocEng: " + Region.class.getSimpleName();
    public final double MIN_INNER_GEOFENCE = Constants.MIN_INNER_GEOFENCE;
    public final double GEOFENCE_HYSTERESIS = Constants.GEOFENCE_HYSTERESIS;
    public final double GEOFENCE_TO_REGION_BUFFER = Constants.GEOFENCE_TO_REGION_BUFFER;
    public final double MAX_UNCERTAINTY = Constants.MAX_UNCERTAINTY;
// FIXME Change these fields to private and use getter/setter methods in location engine classes
    public long id;
    public String address;
    public Location center;
    public double radius;
    public String name;
    public String manager;
    public String points;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Location getCenter() {
        return center;
    }

    public void setCenter(Location center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public double getGeofenceRadius(Integer currentSite) {
        double base = Math.max(radius,MIN_INNER_GEOFENCE);
        if(currentSite != null && currentSite == id) {
            // apply hysteresis
            base += GEOFENCE_HYSTERESIS;
        }
        return base;
    }

    public boolean sortOfIntersects(Location location, Integer currentSiteId) {
        double radius = getGeofenceRadius(currentSiteId);

        if(currentSiteId != null && currentSiteId == id) {
            // intersect boundary is within geofence radius
            radius -= GEOFENCE_TO_REGION_BUFFER;
        }
        else {
            radius += GEOFENCE_TO_REGION_BUFFER;
        }

        radius += Math.min(location.getAccuracy(), MAX_UNCERTAINTY);

        final double distance = center.distanceTo(location);

        final boolean intersects = distance < radius;

        return intersects;
    }
}