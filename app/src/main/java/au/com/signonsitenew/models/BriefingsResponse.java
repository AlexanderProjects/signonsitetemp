package au.com.signonsitenew.models;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.annotation.Nullable;

import au.com.signonsitenew.domain.models.ApiResponse;

public class BriefingsResponse extends ApiResponse {

    @Nullable
    private String status;
    private Integer total;
    private Integer per_page;
    private Integer current_page;
    private Integer last_page;
    private List<Briefing> data;

    public BriefingsResponse(@NotNull String status) {
        super(status);
    }

    @org.jetbrains.annotations.Nullable
    public String getStatus() {
        return status;
    }

    public void setStatus(@org.jetbrains.annotations.Nullable String status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPer_page() {
        return per_page;
    }

    public void setPer_page(Integer per_page) {
        this.per_page = per_page;
    }

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public void setLast_page(Integer last_page) {
        this.last_page = last_page;
    }

    public List<Briefing> getData() {
        return data;
    }

    public void setData(List<Briefing> data) {
        this.data = data;
    }
}
