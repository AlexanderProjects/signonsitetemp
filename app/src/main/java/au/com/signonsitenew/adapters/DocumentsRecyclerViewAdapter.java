package au.com.signonsitenew.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import au.com.signonsitenew.BuildConfig;
import au.com.signonsitenew.R;
import au.com.signonsitenew.domain.models.WorkerBriefing;
import au.com.signonsitenew.models.Document;
import au.com.signonsitenew.models.Site;
import au.com.signonsitenew.ui.main.documents.DocumentsFragment.OnListFragmentInteractionListener;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * {@link RecyclerView.Adapter} that can display a {@link Site} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class DocumentsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Document> mDocuments;
    private OnListFragmentInteractionListener mListener;
    private WorkerBriefing workerBriefing;
    private boolean isManager;

    public DocumentsRecyclerViewAdapter(List<Document> documents, WorkerBriefing workerBriefing, OnListFragmentInteractionListener listener ,boolean isUserManager) {
        mDocuments = documents;
        mListener = listener;
        isManager = isUserManager;
        this.workerBriefing = workerBriefing;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new ViewHolderWorker(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document, parent, false));
            case 1:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document_manager, parent, false);
                return  new ViewHolderManager(view, new ViewHolderManager.HolderManagerListener() {
                    @Override
                    public void onSeeAllButtonClick(Document doc) { mListener.onClickSeeAllButton(doc); }

                    @Override
                    public void onNewButtonClick(Document doc) { mListener.onClickNewButton(doc); }
                });
            case 2: return new ViewHolderEmptyDocs(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_documents,parent,false));
            case 3: return new ViewHolderEmptyDocs(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_documents,parent,false));
            case 4: return new ViewHolderEmptyDocs(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_documents,parent,false));
            case 5: return new ViewHolderEmptyDocs(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_documents,parent,false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()){
            case 0 :
                ViewHolderWorker viewHolderWorker = ((ViewHolderWorker)holder);
                viewHolderWorker.mDocument = mDocuments.get(position);
                String docType = viewHolderWorker.mDocument.getDocType();
                String statusWorkerMsg = "";

                // Parameters to set colours
                Context imageViewWorkerContext = viewHolderWorker.mImage.getContext();
                int resolvedWorkerColour       = ContextCompat.getColor(imageViewWorkerContext, R.color.green_highlight);
                int pendingWorkerColour        = ContextCompat.getColor(imageViewWorkerContext, R.color.orange_secondary);
                int actionRequiredWorkerColour = ContextCompat.getColor(imageViewWorkerContext, R.color.red_highlight);
                int permitColor = ContextCompat.getColor(imageViewWorkerContext, android.R.color.black);

                String docState = viewHolderWorker.mDocument.getState();
                if (docType.equals(Constants.DOC_BRIEFING)) {
                    if (docState.equals(Constants.DOC_BRIEFING_UNACKNOWLEDGED)) {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_announcement);
                        viewHolderWorker.mImage.setColorFilter(actionRequiredWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.BRIEFING_UNACKNOWLEDGED;
                    }
                    else {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_check_circle);
                        viewHolderWorker.mImage.setColorFilter(resolvedWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.BRIEFING_ACKNOWLEDGED;
                    }
                }
                else if (docType.equals(Constants.DOC_INDUCTION)) {
                    if (docState.equals(Constants.DOC_INDUCTION_INCOMPLETE)) {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_assignment_late);
                        viewHolderWorker.mImage.setColorFilter(actionRequiredWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.INDUCTION_INCOMPLETE;
                    }
                    else if (docState.equals(Constants.DOC_INDUCTION_PENDING)) {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_assignment_turned_in);
                        viewHolderWorker.mImage.setColorFilter(pendingWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.INDUCTION_PENDING_REVIEW;
                    }
                    else if (docState.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_assignment_turned_in);
                        viewHolderWorker.mImage.setColorFilter(resolvedWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.INDUCTION_COMPLETE;
                    }
                    else if (docState.equals(Constants.DOC_INDUCTION_REJECTED)) {
                        viewHolderWorker.mImage.setImageResource(R.drawable.ic_assignment_late);
                        viewHolderWorker.mImage.setColorFilter(actionRequiredWorkerColour, PorterDuff.Mode.SRC_IN);
                        statusWorkerMsg = Constants.INDUCTION_REJECTED;
                    }
                }
                else if (docType.equals(Constants.DOC_PERMITS)){
                    viewHolderWorker.mImage.setImageResource(R.drawable.ic_permit_icon);
                    viewHolderWorker.mImage.setColorFilter(permitColor, PorterDuff.Mode.SRC_IN);
                    statusWorkerMsg = Constants.ACCESS_PERMITS;
                }
                viewHolderWorker.mDocumentType.setText(viewHolderWorker.mDocument.getDocType());
                viewHolderWorker.mStatus.setText(statusWorkerMsg);
                viewHolderWorker.mView.setOnClickListener(new DebouncedOnClickListener() {
                    @Override
                    public void onDebouncedClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onDocumentSelected(viewHolderWorker.mDocument, workerBriefing);
                        }
                    }
                });
                break;

            case 1:

                ViewHolderManager viewHolderManager = ((ViewHolderManager)holder);
                viewHolderManager.mDocument = mDocuments.get(position);
                String type = viewHolderManager.mDocument.getDocType();
                String statusMsg = "";

                // Parameters to set colours
                Context imageViewContext = viewHolderManager.mImage.getContext();
                int resolvedColour       = ContextCompat.getColor(imageViewContext, R.color.green_highlight);
                int pendingColour        = ContextCompat.getColor(imageViewContext, R.color.orange_secondary);
                int actionRequiredColour = ContextCompat.getColor(imageViewContext, R.color.red_highlight);

                String state = viewHolderManager.mDocument.getState();
                if (type.equals(Constants.DOC_BRIEFING)) {
                    if (state.equals(Constants.DOC_BRIEFING_UNACKNOWLEDGED)) {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_announcement);
                        viewHolderManager.mImage.setColorFilter(actionRequiredColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Unacknowledged";
                    }
                    else {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_check_circle);
                        viewHolderManager.mImage.setColorFilter(resolvedColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Acknowledged";
                    }
                }
                else if (type.equals(Constants.DOC_INDUCTION)) {
                    if (state.equals(Constants.DOC_INDUCTION_INCOMPLETE)) {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_assignment_late);
                        viewHolderManager.mImage.setColorFilter(actionRequiredColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Incomplete";
                    }
                    else if (state.equals(Constants.DOC_INDUCTION_PENDING)) {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_assignment_turned_in);
                        viewHolderManager.mImage.setColorFilter(pendingColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Pending Manager Review";
                    }
                    else if (state.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_assignment_turned_in);
                        viewHolderManager.mImage.setColorFilter(resolvedColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Complete";
                    }
                    else if (state.equals(Constants.DOC_INDUCTION_REJECTED)) {
                        viewHolderManager.mImage.setImageResource(R.drawable.ic_assignment_late);
                        viewHolderManager.mImage.setColorFilter(actionRequiredColour, PorterDuff.Mode.SRC_IN);
                        statusMsg = "Rejected - please complete again";
                    }
                }

                viewHolderManager.mDocumentType.setText(viewHolderManager.mDocument.getSubtype() + " " + viewHolderManager.mDocument.getDocType());
                viewHolderManager.mStatus.setText(statusMsg);
                viewHolderManager.mView.setOnClickListener(new DebouncedOnClickListener() {
                    @Override
                    public void onDebouncedClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.onDocumentSelected(viewHolderManager.mDocument,workerBriefing);
                        }
                    }
                });
                break;

            case 2:
                ViewHolderEmptyDocs viewHolderEmptyDocs = ((ViewHolderEmptyDocs)holder);
                viewHolderEmptyDocs.mDocument = mDocuments.get(position);
                break;
            case 3:
                ViewHolderEmptyDocs viewHolderEmptyInduction = ((ViewHolderEmptyDocs)holder);
                viewHolderEmptyInduction.mDocument = mDocuments.get(position);
                viewHolderEmptyInduction.mDocumentType.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.empty_induction_title));
                viewHolderEmptyInduction.mStatus.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.empty_induction_message));
                break;

            case 4:
                ViewHolderEmptyDocs viewHolderEmptyBriefing = ((ViewHolderEmptyDocs)holder);
                viewHolderEmptyBriefing.mDocument = mDocuments.get(position);
                viewHolderEmptyBriefing.mDocumentType.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.empty_briefing_title));
                viewHolderEmptyBriefing.mStatus.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.empty_briefing_message));
                break;
            case 5:
                ViewHolderEmptyDocs viewHolderDocumentsError = ((ViewHolderEmptyDocs)holder);
                viewHolderDocumentsError.mDocument = mDocuments.get(position);
                viewHolderDocumentsError.mDocumentType.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.documents_retrieve_error_title));
                viewHolderDocumentsError.mStatus.setText(((ViewHolderEmptyDocs) holder).mView.getResources().getString(R.string.documents_retrieve_error));
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(isManager && BuildConfig.BRIEFING_LIST_ENABLE){ return 1; }
        if(mDocuments.get(position).getDocType().equals(Constants.EMPTY_DOCS)) return 2;
        if(mDocuments.get(position).getDocType().equals(Constants.DOC_EMPTY_INDUCTION)) return 3;
        if(mDocuments.get(position).getDocType().equals(Constants.DOC_EMPTY_BRIEFING)) return 4;
        if(mDocuments.get(position).getDocType().equals(Constants.DOC_ERROR)) return 5;
        return 0;
    }

    @Override
    public int getItemCount() {
        return mDocuments.size();
    }

    public void refill(List<Document> documents,WorkerBriefing workerBriefing) {
        mDocuments = documents;
        this.workerBriefing = workerBriefing;
        notifyDataSetChanged();
    }

    public class ViewHolderWorker extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mImage;
        final TextView mDocumentType;
        final TextView mStatus;
        Document mDocument;

        ViewHolderWorker(View view) {
            super(view);
            mView = view;
            mImage = view.findViewById(R.id.document_status_image_view);
            mDocumentType = view.findViewById(R.id.document_name_text_view);
            mStatus = view.findViewById(R.id.document_status_text_view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDocumentType.getText() + "' - '" + mStatus.getText() + "'";
        }
    }
    public class  ViewHolderEmptyDocs extends RecyclerView.ViewHolder{
        final View mView;
        final TextView mDocumentType;
        final TextView mStatus;
        Document mDocument;

        ViewHolderEmptyDocs(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            mDocumentType = itemView.findViewById(R.id.document_name_text_view);
            mStatus = itemView.findViewById(R.id.document_status_text_view);
        }
    }
   public static class ViewHolderManager extends RecyclerView.ViewHolder implements View.OnClickListener{

       final View mView;
       final ImageView mImage;
       final TextView mDocumentType;
       final TextView mStatus;
       final ImageButton seeAllButton;
       final ImageButton newButton;
       Document mDocument;
       HolderManagerListener holderManagerListener;

        ViewHolderManager(View view, HolderManagerListener listener){
            super(view);
            holderManagerListener = listener;
            mView = view;
            mImage = view.findViewById(R.id.document_status_image_view);
            mDocumentType = view.findViewById(R.id.document_name_text_view);
            mStatus = view.findViewById(R.id.document_status_text_view);
            seeAllButton = view.findViewById(R.id.document_see_all_image_button_view);
            newButton =  view.findViewById(R.id.document_new_briefing_image_button);

            seeAllButton.setOnClickListener(this);
            newButton.setOnClickListener(this);
        }

       @Override
       public String toString() {
           return super.toString() + " '" + mDocumentType.getText() + "' - '" + mStatus.getText() + "'";
       }

       @Override
       public void onClick(View view) {
            switch (view.getId()){
                case R.id.document_see_all_image_button_view:
                    holderManagerListener.onSeeAllButtonClick(mDocument);
                    break;
                case R.id.document_new_briefing_image_button:
                    holderManagerListener.onNewButtonClick(mDocument);
            }

       }

       public interface HolderManagerListener{
           void onSeeAllButtonClick(Document doc);
           void onNewButtonClick(Document doc);
       }

   }

}
