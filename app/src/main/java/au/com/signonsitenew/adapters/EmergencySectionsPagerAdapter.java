package au.com.signonsitenew.adapters;



import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import au.com.signonsitenew.ui.evacuation.EmergencyCompaniesLogFragment;
import au.com.signonsitenew.ui.evacuation.EmergencyIndividualsLogFragment;

/**
 * Created by Krishan Caldwell on 4/07/2016.
 */
public class EmergencySectionsPagerAdapter extends FragmentPagerAdapter {

    public EmergencySectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new EmergencyIndividualsLogFragment();
            case 1:
                return new EmergencyCompaniesLogFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Individuals";
            case 1:
                return "Companies";
        }
        return null;
    }
}
