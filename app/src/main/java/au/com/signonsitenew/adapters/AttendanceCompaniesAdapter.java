package au.com.signonsitenew.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.realm.RealmManager;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;
import io.realm.Sort;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class AttendanceCompaniesAdapter extends BaseExpandableListAdapter {

    private static final String LOG = AttendanceCompaniesAdapter.class.getSimpleName();

    private Context mContext;
    private boolean mInductionsEnabled;
    private List<String> mCompanies;
    private HashMap<String, List<SiteAttendee>> mAttendance;

    public AttendanceCompaniesAdapter(Context context,
                                      boolean inductionsEnabled,
                                      List<String> companies,
                                      HashMap<String, List<SiteAttendee>> attendance) {
        mContext = context;
        mInductionsEnabled = inductionsEnabled;
        mCompanies = companies;
        mAttendance = attendance;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mAttendance.get(mCompanies.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        SiteAttendee visitor = (SiteAttendee)getChild(groupPosition, childPosition);
        long userId = visitor.getUserId();

        // Find the user's most recent attendanceRecord
        Realm realm = Realm.getInstance(RealmManager.getRealmConfiguration());
        AttendanceRecord attendanceRecord = realm.where(AttendanceRecord.class)
                .equalTo("userId", userId)
                .sort("id", Sort.DESCENDING)
                .findFirst();

        String personName = visitor.getName();
        String personInductionState = visitor.getInductionStatus();
        String timeIn = attendanceRecord.getCheckInTime();

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_attendee, null);
        }

        ImageView alertImageView = convertView.findViewById(R.id.alert_image_view);
        TextView visitorNameTextView = convertView.findViewById(R.id.attendee_name_text_view);
        TextView timeInTextView = convertView.findViewById(R.id.attendee_time_in);
        TextView to = convertView.findViewById(R.id.attendee_to);
        TextView timeOutTextView = convertView.findViewById(R.id.attendee_time_out);

        visitorNameTextView.setText(personName);

        // Set the alert ImageView appropriately.
        if (mInductionsEnabled) {
            if (personInductionState == null || personInductionState.isEmpty()) {
                alertImageView.setVisibility(View.GONE);
            }
            else if (personInductionState.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                alertImageView.setVisibility(View.GONE);
            }
            else if (personInductionState.equals(Constants.DOC_INDUCTION_PENDING)) {
                int pendingColour = ContextCompat.getColor(alertImageView.getContext(), R.color.orange_secondary);
                alertImageView.setVisibility(View.VISIBLE);
                alertImageView.setColorFilter(pendingColour, PorterDuff.Mode.SRC_IN);
            }
            else {
                int alertColour = ContextCompat.getColor(alertImageView.getContext(), R.color.emergency_primary);
                alertImageView.setVisibility(View.VISIBLE);
                alertImageView.setColorFilter(alertColour, PorterDuff.Mode.SRC_IN);
            }
        }
        else {
            alertImageView.setVisibility(View.GONE);
        }

        // Calculate times
        String formattedTime = formatTime(timeIn);
        timeInTextView.setText(formattedTime);

        if (attendanceRecord.getCheckOutTime() != null && attendanceRecord.getCheckOutTime().equals("null")) {
            timeOutTextView.setText("present");

            // Set colour
            visitorNameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            timeInTextView.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            to.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            timeOutTextView.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
        }
        else {
            String checkOutTime = attendanceRecord.getCheckOutTime();
            String formattedCheckOut = formatTime(checkOutTime);
            timeOutTextView.setText(formattedCheckOut);

            // Set colour
            visitorNameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            timeInTextView.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            to.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            timeOutTextView.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
        }

        realm.close();

        return convertView;
    }

    private String formatTime(String serverTimeFormat) {
        // Convert the sign on time stored into something actually nice...
        DateFormat uglyFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat targetFormat = new SimpleDateFormat("hh:mma");

        Date time = new Date();
        try {
            time = uglyFormat.parse(serverTimeFormat);
        }
        catch (ParseException e) {
            SLog.e(LOG, "Parse Exception: " + e.getMessage());
        }

        // Calculate if sign on time was today
        long timeInMillis = time.getTime();
        String formattedTime;

        boolean today = DateUtils.isToday(timeInMillis);
        if (today) {
            // Format the time stamp
            formattedTime = targetFormat.format(time);
        }
        else if (isYesterday(timeInMillis)) {
            formattedTime = "Yesterday";
        }
        else {
            // Determine if yesterday or longer ago
            long now = new Date().getTime();
            formattedTime = DateUtils.formatSameDayTime(timeInMillis, now, DateFormat.SHORT, DateFormat.SHORT).toString();
        }

        return formattedTime;
    }

    private static boolean isYesterday(long date) {
        Calendar now = Calendar.getInstance();
        Calendar cDate = Calendar.getInstance();
        cDate.setTimeInMillis(date);

        now.add(Calendar.DATE, -1);

        return now.get(Calendar.YEAR) == cDate.get(Calendar.YEAR)
                && now.get(Calendar.MONTH) == cDate.get(Calendar.MONTH)
                && now.get(Calendar.DATE) == cDate.get(Calendar.DATE);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mAttendance.get(mCompanies.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mCompanies.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mCompanies.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String groupTitle = (String)getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_attendee_header, null);
        }

        LinearLayout companyHeaderLayout = convertView.findViewById(R.id.company_header_cell);
        TextView companyHeaderTextView = convertView.findViewById(R.id.company_header_text_view);
        TextView companyCountTextView = convertView.findViewById(R.id.company_visitor_count_text_view);
        TextView companyExpandHintTextView = convertView.findViewById(R.id.company_expand_hint);

        int companyNumber = getChildrenCount(groupPosition);

        companyHeaderTextView.setText(groupTitle);

        if (groupPosition == 0) {
            companyExpandHintTextView.setVisibility(View.VISIBLE);
            companyExpandHintTextView.setText("(touch to expand)");
        }
        else {
            companyExpandHintTextView.setVisibility(View.GONE);
        }

        // Change header background and text colour depending on if open or not
        if (isExpanded) {
            // Set colour to orange, font to white
            companyHeaderLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.custom_list_header_border_orange));
            companyHeaderTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            companyCountTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));

            if (groupPosition == 0) {
                // Remove the hint text
                companyExpandHintTextView.setVisibility(View.GONE);
            }
        }
        else {
            // Set colour to white, font to grey
            companyHeaderLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.custom_list_header_border_grey));
            companyHeaderTextView.setTextColor(ContextCompat.getColor(mContext, R.color.grey_primary));
            companyCountTextView.setTextColor(ContextCompat.getColor(mContext, R.color.grey_primary));
        }

        companyCountTextView.setText(String.valueOf(companyNumber));
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void refill(List<String> groupHeaders, HashMap<String, List<SiteAttendee>> visitors) {
        mCompanies = groupHeaders;
        mAttendance = visitors;
        notifyDataSetChanged();
    }
}
