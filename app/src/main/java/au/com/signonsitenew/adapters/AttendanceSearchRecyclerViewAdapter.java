package au.com.signonsitenew.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.SiteAttendee;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.ui.attendanceregister.AttendancePeopleFragment.OnVisitorInteractionListener;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import au.com.signonsitenew.utilities.SLog;
import co.moonmonkeylabs.realmsearchview.RealmSearchAdapter;
import co.moonmonkeylabs.realmsearchview.RealmSearchViewHolder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class AttendanceSearchRecyclerViewAdapter
        extends RealmSearchAdapter<SiteAttendee, AttendanceSearchRecyclerViewAdapter.ViewHolder> {

    private static final String LOG = AttendanceSearchRecyclerViewAdapter.class.getSimpleName();

    private Context mContext;
    private Realm mRealm;
    private boolean mInductionsEnabled;
    private OnVisitorInteractionListener mListener;

    public AttendanceSearchRecyclerViewAdapter(Context context, Realm realm, String filterColumnName, boolean inductionsEnabled, OnVisitorInteractionListener listener) {
        super(context, realm, filterColumnName);
        mContext = context;
        mRealm = realm;
        mInductionsEnabled = inductionsEnabled;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateRealmViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_visitor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindRealmViewHolder(final ViewHolder holder, int position) {
        holder.mVisitor = realmResults.get(position);
        holder.mPersonName.setText(realmResults.get(position).getName());

        Log.i(LOG, "Inducted status: " + realmResults.get(position).getInductionStatus());
        // Set the alert ImageView appropriately.
        if (mInductionsEnabled) {
            String status = realmResults.get(position).getInductionStatus();
            if (status == null || status.isEmpty()) {
                holder.mAlert.setVisibility(View.GONE);
            }
            else if (status.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                holder.mAlert.setVisibility(View.GONE);
            }
            else if (status.equals(Constants.DOC_INDUCTION_PENDING)) {
                int pendingColour = ContextCompat.getColor(holder.mAlert.getContext(), R.color.orange_secondary);
                holder.mAlert.setVisibility(View.VISIBLE);
                holder.mAlert.setColorFilter(pendingColour, PorterDuff.Mode.SRC_IN);
            }
            else {
                int alertColour = ContextCompat.getColor(holder.mAlert.getContext(), R.color.emergency_primary);
                holder.mAlert.setVisibility(View.VISIBLE);
                holder.mAlert.setColorFilter(alertColour, PorterDuff.Mode.SRC_IN);
            }
        }
        else {
            holder.mAlert.setVisibility(View.GONE);
        }


        long userId = realmResults.get(position).getUserId();

        AttendanceRecord visit = mRealm.where(AttendanceRecord.class)
                                           .equalTo("userId", userId)
                                           .sort("checkOutTime", Sort.DESCENDING)
                                           .findFirst();

        String timeOn = "";
        if (visit != null) {
            timeOn = visit.getCheckInTime();
        }

        // Convert the sign on time stored into something actually nice...
        String formattedTime = formatTime(timeOn);
        holder.mTimeOn.setText(formattedTime);

        if (visit.getCheckOutTime() != null && visit.getCheckOutTime().equals("null")) {
            holder.mTimeOff.setText("present");

            // Set colour
            holder.mPersonName.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            holder.mTimeOn.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            holder.mTo.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
            holder.mTimeOff.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
        }
        else {
            String checkOutTime = visit.getCheckOutTime();
            String formattedCheckOut = formatTime(checkOutTime);
            holder.mTimeOff.setText(formattedCheckOut);

            // Set colour
            holder.mPersonName.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            holder.mTimeOn.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            holder.mTo.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
            holder.mTimeOff.setTextColor(ContextCompat.getColor(mContext, R.color.text_grey_secondary));
        }

        holder.mView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Send the user clicked on back to the host activity
                if (mListener != null) {
                    mListener.onVisitorSelected(holder.mVisitor);
                }
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // Implement quick menu to sign on/off
                return false;
            }
        });
    }

    private String formatTime(String serverTimeFormat) {
        if (serverTimeFormat == null) {
            return "";
        }
        // Convert the sign on time stored into something actually nice...
        Log.i(LOG, "Server time: " + serverTimeFormat);
        DateFormat uglyFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat targetFormat = new SimpleDateFormat("hh:mma");

        Date time = new Date();
        try {
            time = uglyFormat.parse(serverTimeFormat);
            Log.i(LOG, "time: " + time);
        }
        catch (ParseException e) {
            SLog.e(LOG, "Parse Exception: " + e.getMessage());
        }

        // Calculate if sign on time was today
        long timeInMillis = time.getTime();
        String formattedTime;

        boolean today = DateUtils.isToday(timeInMillis);
        if (today) {
            // Format the time stamp
            formattedTime = targetFormat.format(time);
        }
        else if (isYesterday(timeInMillis)) {
            formattedTime = "Yesterday";
        }
        else {
            // Determine if yesterday or longer ago
            long now = new Date().getTime();
            formattedTime = DateUtils.formatSameDayTime(timeInMillis, now, DateFormat.SHORT, DateFormat.SHORT).toString();
        }

        return formattedTime;
    }

    private static boolean isYesterday(long date) {
        Calendar now = Calendar.getInstance();
        Calendar cDate = Calendar.getInstance();
        cDate.setTimeInMillis(date);

        now.add(Calendar.DATE, -1);

        return now.get(Calendar.YEAR) == cDate.get(Calendar.YEAR)
                && now.get(Calendar.MONTH) == cDate.get(Calendar.MONTH)
                && now.get(Calendar.DATE) == cDate.get(Calendar.DATE);
    }

    @Override
    public void filter(String input) {
        RealmQuery<SiteAttendee> where = mRealm.where(SiteAttendee.class);

        if (!input.isEmpty()) {
            where = where.beginGroup()
                        .contains("firstName", input, Case.INSENSITIVE)
                        .or()
                        .contains("lastName", input, Case.INSENSITIVE)
                        .or()
                        .contains("company", input, Case.INSENSITIVE)
                        .or()
                        .contains("phoneNumber", input, Case.INSENSITIVE)
                    .endGroup();
        }

        RealmResults<SiteAttendee> visitors = where.sort("firstName", Sort.ASCENDING).findAll();

        this.updateRealmResults(visitors);
    }

    public class ViewHolder extends RealmSearchViewHolder {
        public final View mView;
        public final ImageView mAlert;
        public final TextView mPersonName;
        public final TextView mTimeOn;
        public final TextView mTo;
        public final TextView mTimeOff;
        public SiteAttendee mVisitor;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mAlert = view.findViewById(R.id.alert_image_view);
            mPersonName = view.findViewById(R.id.visitor_name);
            mTimeOn = view.findViewById(R.id.visitor_time_in);
            mTo = view.findViewById(R.id.visitor_to);
            mTimeOff = view.findViewById(R.id.visitor_time_out);
        }
    }
}
