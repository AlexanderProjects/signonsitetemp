package au.com.signonsitenew.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.EnrolledUser;
import au.com.signonsitenew.utilities.Constants;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import co.moonmonkeylabs.realmsearchview.RealmSearchAdapter;
import co.moonmonkeylabs.realmsearchview.RealmSearchViewHolder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 */
public class EnrolledWorkerSearchRecyclerViewAdapter
        extends RealmSearchAdapter<EnrolledUser, EnrolledWorkerSearchRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = EnrolledWorkerSearchRecyclerViewAdapter.class.getSimpleName();

    private Realm mRealm;
    private boolean mInductionsEnabled;
    private OnUserSelectedListener mListener;
    private LinearLayout mButtonLayout;

    public interface OnUserSelectedListener {
        void onUserSelected(long userId, String companyName);
    }

    public EnrolledWorkerSearchRecyclerViewAdapter(Context context, Realm realm, String filterColumnName, boolean inductionsEnabled, OnUserSelectedListener listener, LinearLayout layout) {
        super(context, realm, filterColumnName);
        mRealm = realm;
        mInductionsEnabled = inductionsEnabled;
        mListener = listener;
        mButtonLayout = layout;
    }

    @Override
    public ViewHolder onCreateRealmViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_enrolled_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindRealmViewHolder(final ViewHolder holder, int position) {
        holder.mEnrolledUser = realmResults.get(position);
        holder.mPersonName.setText(realmResults.get(position).getName());
        holder.mCompanyName.setText(realmResults.get(position).getCompanyName());

        if (mInductionsEnabled) {
            String inductionStatus = holder.mEnrolledUser.getInductionStatus();
            if (inductionStatus == null || inductionStatus.isEmpty()) {
                holder.mAlert.setVisibility(View.GONE);
            }
            else if (inductionStatus.equals(Constants.DOC_INDUCTION_COMPLETE)) {
                holder.mAlert.setVisibility(View.GONE);
            }
            else if (inductionStatus.equals(Constants.DOC_INDUCTION_PENDING)) {
                int pendingColour = ContextCompat.getColor(holder.mAlert.getContext(), R.color.orange_secondary);
                holder.mAlert.setVisibility(View.VISIBLE);
                holder.mAlert.setColorFilter(pendingColour, PorterDuff.Mode.SRC_IN);
            }
            else {
                int alertColour = ContextCompat.getColor(holder.mAlert.getContext(), R.color.emergency_primary);
                holder.mAlert.setVisibility(View.VISIBLE);
                holder.mAlert.setColorFilter(alertColour, PorterDuff.Mode.SRC_IN);
            }
        }
        else {
            holder.mAlert.setVisibility(View.GONE);
        }


        holder.mView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Get ID and load the visitor details fragment
                long userId = holder.mEnrolledUser.getId();
                String userCompany = holder.mEnrolledUser.getCompanyName();

                if (mListener != null) {
                    mListener.onUserSelected(userId, userCompany);
                }
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // Implement quick menu to sign on/off
                return false;
            }
        });
    }

    @Override
    public void filter(String input) {
        RealmQuery<EnrolledUser> where = mRealm.where(EnrolledUser.class);

        if (!input.isEmpty()) {
            where = where.beginGroup()
                        .contains("firstName", input, Case.INSENSITIVE)
                        .or()
                        .contains("lastName", input, Case.INSENSITIVE)
                        .or()
                        .contains("companyName", input, Case.INSENSITIVE)
                        .or()
                        .contains("phoneNumber", input, Case.INSENSITIVE)
                    .endGroup();
        }

        RealmResults<EnrolledUser> users = where.sort("companyName").findAll();

        if (users.size() < 1) {
            // Display the button to add a visitor
            mButtonLayout.setVisibility(View.VISIBLE);
        }
        else {
            mButtonLayout.setVisibility(View.GONE);
        }

        this.updateRealmResults(users);
    }

    public class ViewHolder extends RealmSearchViewHolder {
        public final View mView;
        public final ImageView mAlert;
        public final TextView mPersonName;
        public final TextView mCompanyName;
        public EnrolledUser mEnrolledUser;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mAlert = view.findViewById(R.id.alert_image_view);
            mPersonName = view.findViewById(R.id.visitor_name);
            mCompanyName = view.findViewById(R.id.visitor_company);
        }
    }
}
