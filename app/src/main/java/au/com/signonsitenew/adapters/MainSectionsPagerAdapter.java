package au.com.signonsitenew.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import au.com.signonsitenew.ui.main.menu.MenuFragment;
import au.com.signonsitenew.ui.main.SitesFragment;



/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * The {@link androidx.viewpager.widget.PagerAdapter} that will provide fragments for each of the
 * sections. We use a {@link FragmentPagerAdapter} derivative, which will keep every loaded fragment
 * in memory. If this becomes too memory intensive, it may be best to switch to a
 * {@link androidx.fragment.app.FragmentStatePagerAdapter}.
 */
public class MainSectionsPagerAdapter extends FragmentStatePagerAdapter {

    protected Context mContext;
    public  Boolean Prompt;
    private Fragment passportOrIntroFragment;

    public MainSectionsPagerAdapter(Context mContext,@NonNull FragmentManager fm, Boolean prompt, Fragment passportOrIntroFragment) {
        super(fm);
        this.mContext = mContext;
        Prompt = prompt;
        Log.i("adapter prompt value", String.valueOf(Prompt));
        this.passportOrIntroFragment = passportOrIntroFragment;
    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        switch (position) {
            case 0:
                Fragment fragment = new SitesFragment();
                Bundle bundle=new Bundle();
                bundle.putString("prompt", String.valueOf(this.Prompt)); //key and value
                fragment.setArguments(bundle);
                return fragment;
            case 1:
                return passportOrIntroFragment;
            case 2:
                return new MenuFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // Set the total number of tabs here
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Site";
            case 1:
                return "Passport";
            case 2:
                return "Menu";
        }
        return null;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
