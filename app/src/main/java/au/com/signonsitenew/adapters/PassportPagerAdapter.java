package au.com.signonsitenew.adapters;



import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import au.com.signonsitenew.ui.passport.connections.ConnectionsFragment;
import au.com.signonsitenew.ui.passport.credentials.CredentialsPassportFragment;
import au.com.signonsitenew.ui.passport.emergencyinfo.EmergencyInfoFragment;
import au.com.signonsitenew.ui.passport.personal.PersonalFragment;

public class PassportPagerAdapter extends FragmentPagerAdapter {

    public PassportPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PersonalFragment();
            case 1:
                return new EmergencyInfoFragment();
            case 2:
                return new CredentialsPassportFragment();
            case 3:
                return new ConnectionsFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Personal";
            case 1:
                return "Emergency";
            case 2:
                return "Credentials";
            case 3:
                return "Connections";
        }

        return null;
    }


}
