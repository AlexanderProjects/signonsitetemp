package au.com.signonsitenew.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.SiteAttendee;

/**
 * Copyright © 2016 SignOnSite. All rights reserved.
 */
public class EmergencyChecklistAdapter extends BaseExpandableListAdapter {

    private static final String LOG = EmergencyChecklistAdapter.class.getSimpleName();

    private Context mContext;
    private List<String> mGroupHeaders;
    private HashMap<String, List<SiteAttendee>> mVisitors;

    public EmergencyChecklistAdapter(Context context,
                                     List<String> parents,
                                     HashMap<String, List<SiteAttendee>> visitors) {
        mContext = context;
        mGroupHeaders = parents;
        mVisitors = visitors;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mVisitors.get(mGroupHeaders.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        SiteAttendee visitor = (SiteAttendee)getChild(groupPosition, childPosition);
        String personName = visitor.getName();

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_emergency_attendee, null);
        }

        TextView visitorNameTextView = (TextView)convertView.findViewById(R.id.attendee_name_text_view);
        ImageView visitorCheckedView = (ImageView)convertView.findViewById(R.id.attendee_checked_image_view);
        visitorNameTextView.setText(personName);

        if (visitor.isMarkedSafe()) {
            visitorCheckedView.setImageResource(R.drawable.green_tick);
            visitorNameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.green_acknowledgement));
        }
        else {
            visitorCheckedView.setImageResource(R.drawable.red_circle);
            visitorNameTextView.setTextColor(ContextCompat.getColor(mContext, R.color.emergency_background));
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mVisitors.get(mGroupHeaders.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroupHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mGroupHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String groupTitle = (String)getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_emergency_header, null);
        }

        TextView emergencyHeaderTextView = (TextView)convertView.findViewById(R.id.emergency_header_text_view);
        emergencyHeaderTextView.setText(groupTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void refill(List<String> groupHeaders, HashMap<String, List<SiteAttendee>> visitors) {
        mGroupHeaders = groupHeaders;
        mVisitors = visitors;
        notifyDataSetChanged();
    }
}
