package au.com.signonsitenew.adapters;

import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.AttendanceRecord;
import au.com.signonsitenew.utilities.SLog;

/**
 * {@link RecyclerView.Adapter} that can display a {@link AttendanceRecord}
 */
public class VisitHistoryRecyclerViewAdapter extends RecyclerView.Adapter<VisitHistoryRecyclerViewAdapter.ViewHolder> {

    private static final String LOG = VisitHistoryRecyclerViewAdapter.class.getSimpleName();

    private final List<AttendanceRecord> mAttendanceRecords;

    public VisitHistoryRecyclerViewAdapter(List<AttendanceRecord> attendanceRecords) {
        mAttendanceRecords = attendanceRecords;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendance_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mAttendanceRecord = mAttendanceRecords.get(position);

        String checkInTime = mAttendanceRecords.get(position).getCheckInTime();
        String checkOutTime = null;
        String formattedCheckIn = formatTime(checkInTime);
        holder.mTimeOn.setText(formattedCheckIn);

        if (holder.mAttendanceRecord.getCheckOutTime() != null && holder.mAttendanceRecord.getCheckOutTime().equals("null")) {
            holder.mTimeOff.setText("present");
        }
        else {
            checkOutTime = holder.mAttendanceRecord.getCheckOutTime();
            String formattedCheckOut = formatTime(checkOutTime);
            holder.mTimeOff.setText(formattedCheckOut);
        }

        // Calculate time spent on site
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date on;
        Date off;

        try {
            on = format.parse(checkInTime);
            if (checkOutTime != null) {
                off = format.parse(checkOutTime);
            }
            else {
                off = new Date();
            }

            // in milliseconds
            long diff = off.getTime() - on.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffMins = diff / (60 * 1000) % 60;

            String duration = "Duration: " + diffHours + "h " + diffMins + "m";
            Log.i(LOG, "Duration: " + duration);

            holder.mDuration.setText(duration);
        }
        catch (ParseException e) {
            SLog.e(LOG, "Parse Exception occurred: " + e.getMessage());
        }
    }

    private String formatTime(String serverTimeFormat) {
        // Convert the sign on time stored into something actually nice...
        Log.i(LOG, "Server time: " + serverTimeFormat);
        DateFormat uglyFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat targetFormat = new SimpleDateFormat("hh:mma");

        Date time = new Date();
        try {
            time = uglyFormat.parse(serverTimeFormat);
            Log.i(LOG, "time: " + time);
        }
        catch (ParseException e) {
            SLog.e(LOG, "Parse Exception: " + e.getMessage());
        }

        // Calculate if sign on time was today
        long timeInMillis = time.getTime();
        String formattedTime;

        boolean today = DateUtils.isToday(timeInMillis);
        if (today) {
            // Format the time stamp
            formattedTime = targetFormat.format(time);
        }
        else if (isYesterday(timeInMillis)) {
            formattedTime = "Yesterday";
        }
        else {
            // Determine if yesterday or longer ago
            long now = new Date().getTime();
            formattedTime = DateUtils.formatSameDayTime(timeInMillis, now, DateFormat.SHORT, DateFormat.SHORT).toString();
        }

        return formattedTime;
    }

    private static boolean isYesterday(long date) {
        Calendar now = Calendar.getInstance();
        Calendar cDate = Calendar.getInstance();
        cDate.setTimeInMillis(date);

        now.add(Calendar.DATE, -1);

        return now.get(Calendar.YEAR) == cDate.get(Calendar.YEAR)
            && now.get(Calendar.MONTH) == cDate.get(Calendar.MONTH)
            && now.get(Calendar.DATE) == cDate.get(Calendar.DATE);
    }

    @Override
    public int getItemCount() {
        return mAttendanceRecords.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDuration;
        public final TextView mTimeOn;
        public final TextView mTimeOff;
        public AttendanceRecord mAttendanceRecord;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDuration = (TextView)view.findViewById(R.id.visit_total_duration);
            mTimeOn = (TextView)view.findViewById(R.id.visit_time_on);
            mTimeOff = (TextView)view.findViewById(R.id.visit_time_off);
        }
    }
}
