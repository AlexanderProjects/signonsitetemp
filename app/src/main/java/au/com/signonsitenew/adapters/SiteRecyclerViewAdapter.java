package au.com.signonsitenew.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import au.com.signonsitenew.R;
import au.com.signonsitenew.domain.models.NearSite;
import au.com.signonsitenew.models.Site;
import au.com.signonsitenew.ui.main.SitesFragment.OnListFragmentInteractionListener;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;

/**
 * Copyright © 2017 SignOnSite. All rights reserved.
 *
 * {@link RecyclerView.Adapter} that can display a {@link Site} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class SiteRecyclerViewAdapter extends RecyclerView.Adapter<SiteRecyclerViewAdapter.ViewHolder> {

    private List<NearSite> mSites;
    private OnListFragmentInteractionListener mListener;

    public SiteRecyclerViewAdapter(List<NearSite> sites, OnListFragmentInteractionListener listener) {
        mSites = sites;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_site, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mSite = mSites.get(position);
        holder.mSiteName.setText(mSites.get(position).getName());
        holder.mCompany.setText(mSites.get(position).getPrincipal_company_name());
        holder.mAddress.setText(mSites.get(position).getAddress());

        holder.mView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onSiteSelected(holder.mSite);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mSites != null) {
            return mSites.size();
        }
        return 0;
    }

    public void refill(List<NearSite> sites) {
        mSites = sites;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mSiteName;
        final TextView mCompany;
        final TextView mAddress;
        NearSite mSite;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mSiteName = view.findViewById(R.id.site_name_text_view);
            mCompany = view.findViewById(R.id.site_company_text_view);
            mAddress = view.findViewById(R.id.site_address_text_view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mSiteName.getText() + "' - '" + mCompany.getText() + "' - '" + mAddress.getText() + "'";
        }
    }
}
