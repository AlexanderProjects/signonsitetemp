package au.com.signonsitenew.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import au.com.signonsitenew.R;
import au.com.signonsitenew.realm.Company;
import au.com.signonsitenew.ui.main.SiteCompanySelectFragment.OnCompanyInteractionListener;
import au.com.signonsitenew.utilities.DebouncedOnClickListener;
import co.moonmonkeylabs.realmsearchview.RealmSearchAdapter;
import co.moonmonkeylabs.realmsearchview.RealmSearchViewHolder;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Copyright © 2018 SignOnSite. All rights reserved.
 */
public class CompanySearchRecyclerViewAdapter
        extends RealmSearchAdapter<Company, CompanySearchRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = CompanySearchRecyclerViewAdapter.class.getSimpleName();

    private Context mContext;
    private Realm mRealm;
    private OnCompanyInteractionListener mListener;

    public CompanySearchRecyclerViewAdapter(Context context, Realm realm, String filterColumnName, OnCompanyInteractionListener listener) {
        super(context, realm, filterColumnName);
        mContext = context;
        mRealm = realm;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateRealmViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_company, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindRealmViewHolder(final ViewHolder holder, int position) {
        holder.mCompany = realmResults.get(position);
        holder.mCompanyName.setText(realmResults.get(position).getName());

        Log.i(TAG, "Company ID: " + realmResults.get(position).getCompanyId());

        holder.mView.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                // Send the user clicked on back to the host activity
                if (mListener != null) {
                    mListener.onCompanySelected(holder.mCompany);
                }
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // Implement quick menu to sign on/off
                return false;
            }
        });
    }

    @Override
    public void filter(String input) {
        RealmQuery<Company> where = mRealm.where(Company.class);

        if (!input.isEmpty()) {
            where = where.beginGroup()
                        .contains("name", input, Case.INSENSITIVE)
                    .endGroup();
        }

        RealmResults<Company> companies = where.sort("name", Sort.ASCENDING).findAll();
        this.updateRealmResults(companies);
    }

    public class ViewHolder extends RealmSearchViewHolder {
        public final View mView;
        public final TextView mCompanyName;
        public Company mCompany;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCompanyName = view.findViewById(R.id.company_name_text_view);
        }
    }
}
