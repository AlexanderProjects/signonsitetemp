package au.com.signonsitenew.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import au.com.signonsitenew.R;

/**
 * Created by Krishan Caldwell on 11/06/2016.
 */
public class CredentialInputsAdapter extends BaseAdapter {

    private static final String LOG = CredentialInputsAdapter.class.getSimpleName();

    protected Context mContext;
    protected String[] mCredentialInputs;

    public CredentialInputsAdapter(Context context, String[] credentialInputs) {
        super();
        mContext = context;
        mCredentialInputs = credentialInputs;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_credential_input, null);
            holder = new ViewHolder();
            holder.credentialLabel = (EditText)convertView.findViewById(R.id.credential_input_edit_text);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }

        String optionTitle = mCredentialInputs[position];
        holder.credentialLabel.setHint(optionTitle);

        return convertView;
    }

    @Override
    public int getCount() {
        return mCredentialInputs.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mCredentialInputs[position];
    }

    public void refill(String[] credentialInputs) {
        mCredentialInputs = credentialInputs;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        EditText credentialLabel;
    }
}
