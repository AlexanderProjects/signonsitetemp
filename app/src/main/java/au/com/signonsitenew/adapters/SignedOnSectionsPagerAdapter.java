package au.com.signonsitenew.adapters;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import au.com.signonsitenew.realm.services.SiteSettingsService;
import au.com.signonsitenew.realm.services.UserAbilitiesService;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.ui.main.documents.DocumentsFragment;
import au.com.signonsitenew.ui.main.menu.MenuFragment;
import au.com.signonsitenew.ui.main.SignedOnFragment;
import au.com.signonsitenew.ui.main.ManagerFragment;
import au.com.signonsitenew.utilities.SLog;
import io.realm.Realm;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SignedOnSectionsPagerAdapter extends FragmentStatePagerAdapter {

    private static final String LOG = SignedOnSectionsPagerAdapter.class.getSimpleName();
    private boolean mHasManagementPermissions;
    private Fragment fragment;


    public SignedOnSectionsPagerAdapter(FragmentManager fm, Fragment fragment) {
        super(fm);
        SLog.i(LOG, "Adapter initialised");
        Realm realm = Realm.getDefaultInstance();
        UserAbilitiesService abilitiesService = new UserAbilitiesService(realm);
        SiteSettingsService settingsService = new SiteSettingsService(realm);
        UserService userService = new UserService(realm);
        long siteId = settingsService.getSiteId();
        long userId = userService.getCurrentUserId();
        mHasManagementPermissions = abilitiesService.hasManagerPermissions(userId, siteId);
        realm.close();
        this.fragment = fragment;
    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        if (mHasManagementPermissions) {
            switch (position) {
                case 0:
                    return new SignedOnFragment();
                case 1:
                    return new DocumentsFragment();
                case 2:
                    return fragment;
                case 3:
                    return new ManagerFragment();
                case 4:
                    return new MenuFragment();
            }
        }
        else {
            switch (position) {
                case 0:
                    return new SignedOnFragment();
                case 1:
                    return new DocumentsFragment();
                case 2:
                    return fragment;
                case 3:
                    return new MenuFragment();
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 4 or 5 total pages depending on User Role.
        if (mHasManagementPermissions) {
            return 5;
        }
        else {
            return 4;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mHasManagementPermissions) {
            switch (position) {
                case 0:
                    return "Site";
                case 1:
                    return "Site Docs";
                case 2:
                    return "Passport";
                case 3:
                    return "Manager";
                case 4:
                    return "Menu";
            }
        }
        else {
            switch (position) {
                case 0:
                    return "Site";
                case 1:
                    return "Site Docs";
                case 2:
                    return "Passport";
                case 3:
                    return "Menu";
            }
        }
        return null;
    }
    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

}