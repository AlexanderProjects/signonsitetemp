package au.com.signonsitenew.adapters;


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import au.com.signonsitenew.R;
import au.com.signonsitenew.models.Briefing;
import java.text.SimpleDateFormat;
import java.util.List;


public class BriefingListRecyclerViewAdapter extends RecyclerView.Adapter<BriefingListRecyclerViewAdapter.ViewHolder> {

    private final List<Briefing> briefings;
    private boolean isSortedByDate;

    public BriefingListRecyclerViewAdapter(List<Briefing> items, boolean isDateSortedByFilterOn) {
        briefings = items;
        this.isSortedByDate = isDateSortedByFilterOn;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_briefinglist_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.id.setText(String.valueOf(briefings.get(position).getId()));
        holder.seenCount.setText(String.valueOf(briefings.get(position).getSeen_count()));
        holder.ackCount.setText(String.valueOf(briefings.get(position).getAcknowledgement_count()));
        if(isSortedByDate){
            if(briefings.get(position).getEnd_date() != null){
                holder.contentOrDate.setText(new SimpleDateFormat("yyyy-MM-dd")
                        .format(briefings.get(position).getStart_date())+ " To "+ new SimpleDateFormat("yyyy-MM-dd")
                        .format(briefings.get(position).getEnd_date()));
            }else {
                holder.contentOrDate.setText(new SimpleDateFormat("yyyy-MM-dd")
                        .format(briefings.get(position).getStart_date()));
            }
        }else {
            if(briefings.get(position).getContent_text() != null) {
                holder.contentOrDate.setText(briefings.get(position).getContent_text());
            }else {
                if(briefings.get(position).getContent().length() > 18){
                    holder.contentOrDate.setText(briefings.get(position).getContent().substring(0,18));
                }else {
                    holder.contentOrDate.setText(briefings.get(position).getContent());
                }

            }
        }

        holder.mView.setOnClickListener(v -> {
            //Todo: No implemented yet
        });
    }

    @Override
    public int getItemCount() {
        return briefings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView id;
        public final TextView contentOrDate;
        public final TextView seenCount;
        public final TextView ackCount;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            id = view.findViewById(R.id.id);
            contentOrDate = view.findViewById(R.id.content_date);
            seenCount = view.findViewById(R.id.seen);
            ackCount = view.findViewById(R.id.ack);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + id.getText() + "'";
        }
    }
}
