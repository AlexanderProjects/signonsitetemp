package au.com.signonsitenew.domain.models.state

sealed class CtaContextualButtonFragmentState{
    object ShowSendToTeamButton: CtaContextualButtonFragmentState()
    object ShowSendToTeamButtonWithWarningMessage:CtaContextualButtonFragmentState()
    object ShowSendToTeamDisableButton: CtaContextualButtonFragmentState()
    object ShowObtainApprovalButton : CtaContextualButtonFragmentState()
    object ShowObtainApprovalDisableButton : CtaContextualButtonFragmentState()
    object ShowDoneInRequestButton: CtaContextualButtonFragmentState()
    object ShowDoneInProgressButton: CtaContextualButtonFragmentState()
    object ShowApproveRejectButton: CtaContextualButtonFragmentState()
    object ShowApproveRejectDisableButton: CtaContextualButtonFragmentState()
    object HideCtaButton:CtaContextualButtonFragmentState()
    object ShowSendForClosureButton: CtaContextualButtonFragmentState()
    object ShowSendForClosureDisableButton: CtaContextualButtonFragmentState()
    object ShowRejectClosureButton: CtaContextualButtonFragmentState()
    object ShowApproveRejectClosureButton:CtaContextualButtonFragmentState()
    object ShowPermitStartDateBeforeNowMessage:CtaContextualButtonFragmentState()
    object ShowProgressView:CtaContextualButtonFragmentState()
    object RemoveProgressView:CtaContextualButtonFragmentState()
}
