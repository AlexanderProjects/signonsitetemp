package au.com.signonsitenew.domain.usecases.personal

import android.annotation.SuppressLint
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PassportPersonalBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.domain.models.UpdateInfoResponse
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import java.text.ParseException
import java.text.SimpleDateFormat
import javax.inject.Inject

interface PassportPersonalInfoUseCase {
    fun setRequest(updateRequest: UserInfoUpdateRequest)
    fun convertDateFormat(selectedDate: String): String?
    fun convertDateFormatResponse(selectedDate: String?): String?
    fun createPhoneNumberToBeUpdated(phoneText: String?, alpha2: String?): PhoneNumber
    fun convertApprenticeToString(status: Boolean): String
    fun convertIndigenousStatusToString(status: Boolean): String
    fun convertHasExperienceToString(status:Boolean):String
    fun convertNativeEnglishToString(status:Boolean):String
    fun convertNeedsInterpreterToString(status:Boolean):String
    fun validateEmptyAndNotProvidedValues(value:String, valid: ()->Unit, empty:()->Unit)
    fun validateEmptyAndNotProvidedValuesForMandatoryFields(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForGender(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForApprentice(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForIndigenous(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForConstructionExperience(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForNativeSpeaker(value: String, valid: () -> Unit)
    fun validateNotProvidedValuesOnSpinnerForInterpreterRequired(value: String, valid: () -> Unit)
    fun validateIndigenousStatus(option: String): Boolean
    fun validateApprenticeStatus(option: String): Boolean
    fun convertConfirmationToBoolean(value: String) :Boolean
    fun validateGender(gender:String,valid: ()->Unit, invalid:()->Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position:Int, callAction:() -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position:Int, callAction:() -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit)
    fun getErrorMessage(userInfoResponse: UserInfoResponse): String
    fun getErrorMessage(updateUserInfoResponse: UpdateInfoResponse): String
    fun validSpinnerSelection(userValue: Boolean?, isValid:()->Unit, isInValid:()->Unit)

}

class PassportPersonalInfoUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService): PassportPersonalInfoUseCase, PassportPersonalBaseUseCase<UserInfoResponse, ApiResponse>() {


    private lateinit var updateRequest: UserInfoUpdateRequest

    override fun setRequest(updateRequest: UserInfoUpdateRequest){
        this.updateRequest = updateRequest
    }

    override fun buildUpdateAndGetPersonalInfoAsyncCall(): Flowable<ApiResponse>  = Single.concat(repository.updatePersonalInfo(updateRequest, userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token),
            repository.getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token))

    override fun buildGetPersonalInfoAsyncCall(): Single<UserInfoResponse> = repository.getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token)


    @SuppressLint("SimpleDateFormat")
    override fun convertDateFormat(selectedDate: String): String?{
        var simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        try {
            val date = simpleDateFormat.parse(selectedDate)
            simpleDateFormat = SimpleDateFormat("dd MMM yyyy")
            return simpleDateFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    @SuppressLint("SimpleDateFormat")
    override fun convertDateFormatResponse(selectedDate: String?): String? {
        var simpleDateFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy")
        try {
            val date = simpleDateFormat.parse(selectedDate)
            simpleDateFormat = SimpleDateFormat("dd MMM yyyy")
            return simpleDateFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    override fun createPhoneNumberToBeUpdated(phoneText: String?, alpha2: String?):PhoneNumber {
        val phoneNumber = PhoneNumber()
        if (alpha2 != null) phoneNumber.alpha2 = alpha2 else phoneNumber.alpha2 = Constants.AU_CODE
        phoneNumber.number = phoneText
        return phoneNumber
    }

    override fun convertApprenticeToString(status: Boolean):String {
        return if (status) {
            Constants.APPRENTICE
        } else {
            Constants.NOT_APPRENTICE
        }
    }

    override fun convertIndigenousStatusToString(status: Boolean):String {
        return if (status) {
            Constants.INDIGENOUS
        } else {
            Constants.NOT_INDIGENOUS
        }
    }

    override fun convertHasExperienceToString(status:Boolean):String {
        return if(status){
            Constants.POSITIVE_CONFIRMATION
        } else{
            Constants.NEGATIVE_CONFIRMATION
        }
    }

    override fun convertNativeEnglishToString(status:Boolean):String {
        return if(status){
            Constants.POSITIVE_CONFIRMATION
        } else{
            Constants.NEGATIVE_CONFIRMATION
        }
    }

    override fun convertNeedsInterpreterToString(status:Boolean):String {
        return if(status){
            Constants.POSITIVE_CONFIRMATION
        } else{
            Constants.NEGATIVE_CONFIRMATION
        }
    }

    override inline fun validateEmptyAndNotProvidedValues(value:String, valid: ()->Unit, empty:()->Unit){
        if(value.isNotEmpty())
            valid()
        else
            empty()
    }

    override inline fun validateEmptyAndNotProvidedValuesForMandatoryFields(value: String, valid: () -> Unit) {
        if (value.isNotEmpty())
            valid()
    }


    override inline fun validateNotProvidedValuesOnSpinnerForGender(value: String, valid: () -> Unit){
        if(value != Constants.NOT_PROVIDED)
            valid()
    }

    override inline fun validateNotProvidedValuesOnSpinnerForApprentice(value: String, valid: () -> Unit){
        if(value != Constants.NOT_PROVIDED)
            valid()
    }

    override inline fun validateNotProvidedValuesOnSpinnerForIndigenous(value: String, valid: () -> Unit){
        if(value !=  Constants.NOT_PROVIDED)
            valid()
    }

    override inline fun validateNotProvidedValuesOnSpinnerForConstructionExperience(value: String, valid: () -> Unit){
        if(value != Constants.NOT_PROVIDED)
            valid()
    }

    override inline fun validateNotProvidedValuesOnSpinnerForNativeSpeaker(value: String, valid: () -> Unit){
        if(value != Constants.NOT_PROVIDED)
            valid()
    }

    override inline fun validateNotProvidedValuesOnSpinnerForInterpreterRequired(value: String, valid: () -> Unit){
        if(value != Constants.NOT_PROVIDED)
            valid()
    }

    override fun validateIndigenousStatus(option: String): Boolean {
        return option == Constants.INDIGENOUS
    }

    override fun validateApprenticeStatus(option: String): Boolean {
        return option == Constants.APPRENTICE
    }

    override fun convertConfirmationToBoolean(value: String) :Boolean = value == Constants.POSITIVE_CONFIRMATION

    override inline fun validateGender(gender:String, valid: ()->Unit, invalid:()->Unit) {
        if (gender.isNotEmpty()) {
           valid()
        } else {
            invalid()
        }
    }

    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInMainActivity(position:Int, callAction:() -> Unit){
        if (position != 1) {
            if (sessionManager.editingInPersonalInfoPassport)
                callAction()
        }
    }
    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTabInSignOnActivity(position:Int, callAction:() -> Unit){
        if (position != 2) {
            if (sessionManager.editingInPersonalInfoPassport)
                callAction()
        }
    }

    override fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit){
        if (position != 0) {
            if (sessionManager.editingInPersonalInfoPassport)
                callAction()
        }
    }

    override fun getErrorMessage(userInfoResponse: UserInfoResponse): String = when(userInfoResponse.status){
        Constants.PERSONAL_INFO_UNAUTHORISED -> Constants.PERSONAL_INFO_UNAUTHORISED_MESSAGE
        Constants.PERSONAL_INFO_UNAUTHENTIC -> Constants.PERSONAL_INFO_UNAUTHENTIC_MESSAGE
        Constants.PERSONAL_INFO_BAD_INPUT -> Constants.PERSONAL_INFO_BAD_INPUT_MESSAGE
        else -> Constants.UNKNOWN_NETWORK_ERROR_MESSAGE
    }

    override fun getErrorMessage(updateUserInfoResponse: UpdateInfoResponse): String = when(updateUserInfoResponse.status){
        Constants.PERSONAL_INFO_UNAUTHORISED -> Constants.PERSONAL_INFO_UNAUTHORISED_MESSAGE
        Constants.PERSONAL_INFO_UNAUTHENTIC -> Constants.PERSONAL_INFO_UNAUTHENTIC_MESSAGE
        Constants.PERSONAL_INFO_BAD_INPUT -> Constants.PERSONAL_INFO_BAD_INPUT_MESSAGE
        Constants.PERSONAL_INFO_BAD_PHONE_NUMBER -> Constants.PERSONAL_INFO_BAD_PHONE_NUMBER_MESSAGE
        Constants.PERSONAL_INFO_NO_PASSPORT -> Constants.PERSONAL_INFO_NO_PASSPORT_MESSAGE
        else -> Constants.UNKNOWN_NETWORK_ERROR_MESSAGE
    }

    override inline fun validSpinnerSelection(userValue: Boolean?, isValid:()->Unit, isInValid:()->Unit){
        if(userValue != null ){
            isValid()
        }else
            isInValid()
    }

}