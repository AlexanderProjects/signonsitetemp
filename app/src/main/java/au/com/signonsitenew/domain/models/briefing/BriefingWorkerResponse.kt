package au.com.signonsitenew.domain.models.briefing

import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.domain.models.ApiResponse

data class BriefingWorkerResponse (override val status:String, val briefing: WorkerBriefing?): ApiResponse(status)