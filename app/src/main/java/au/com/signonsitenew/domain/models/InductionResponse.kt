package au.com.signonsitenew.domain.models

import au.com.signonsitenew.utilities.empty

data class InductionResponse(override val status: String, val form_available: Boolean?, var induction: Induction?): ApiResponse(status){
    constructor() : this(String().empty(),null,null)
}