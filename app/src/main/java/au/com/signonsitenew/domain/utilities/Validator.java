package au.com.signonsitenew.domain.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.signonsitenew.domain.models.Credential;


public class Validator {

    /**
     * This method validate the expiry date in for credentials
     * @param credential input phone number
     * @return true or false
     */

    public static Boolean validateExpiryCredential(Credential credential){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date expiryDate = null;
        try {
            expiryDate = sdf.parse(credential.getExpiry_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (new Date().after(expiryDate)) {
            return true;
        }
        else{
            return false;
        }
    }

}
