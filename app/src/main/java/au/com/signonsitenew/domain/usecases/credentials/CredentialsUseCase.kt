package au.com.signonsitenew.domain.usecases.credentials

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.CredentialsBaseUseCase
import au.com.signonsitenew.models.UploadImageResponse
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import java.io.File
import java.util.*
import javax.inject.Inject


interface CredentialsUseCase {
    fun setCredentialParameters(frontPhotoFile: File?, backPhotoFile: File?, request: CredentialCreateUpdateRequest)
    fun setDeleteCredential(credentialId: String):CredentialsUseCaseImpl
    fun retrieveTypeOfCredentials(): Single<CredentialTypesResponse>
}

class CredentialsUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService) : CredentialsBaseUseCase<CredentialCreateUpdateResponse,DeleteCredentialResponse,CredentialsResponse>(), CredentialsUseCase {

    private lateinit var credentialId:String
    private var frontPhotoFile: File? = null
    private var backPhotoFile:File? = null
    private lateinit var request: CredentialCreateUpdateRequest


    override fun setCredentialParameters(frontPhotoFile: File?, backPhotoFile: File?, request: CredentialCreateUpdateRequest){
        this.frontPhotoFile = frontPhotoFile
        this.backPhotoFile = backPhotoFile
        this.request = request
    }

    override fun setDeleteCredential(credentialId: String):CredentialsUseCaseImpl{
        this.credentialId = credentialId
        return this
    }

    override fun retrieveTypeOfCredentials(): Single<CredentialTypesResponse> =
            repository.getCredentialTypes(Constants.BEARER_HEADER + sessionManager.token)
            .observeOn(AndroidSchedulers.mainThread())


    private fun addSOSUrl(imageUrl: String, request: CredentialCreateUpdateRequest, isFrontPhoto: Boolean, isBackPhoto: Boolean): CredentialCreateUpdateRequest {
        if (isFrontPhoto) {
            request.front_photo = imageUrl
        }
        if (isBackPhoto) {
            request.back_photo = imageUrl
        }
        return request
    }

    private fun addBothSOSUrl(image1Url: String, image2Url: String, request: CredentialCreateUpdateRequest): CredentialCreateUpdateRequest {
        request.front_photo = image1Url
        request.back_photo = image2Url
        return request
    }

    private fun createNameFile(photoLocationName: String): String {
        return userService.currentUserId.toString() + photoLocationName + Calendar.getInstance().time
    }

    override fun buildCreateUpdateCredentialAsyncCall(): Single<CredentialCreateUpdateResponse> {
        val userId = userService.currentUserId.toString()
        val token = Constants.BEARER_HEADER + sessionManager.token

        if (sessionManager.editingFrontPhoto && !sessionManager.editingBackPhoto)
            return repository.uploadImages(frontPhotoFile!!, createNameFile(Constants.FRONT_PHOTO))
                    .flatMap { uploadImageResponse: UploadImageResponse -> repository.createCredentials(addSOSUrl(uploadImageResponse.access_key, request, isFrontPhoto = true, isBackPhoto = false), userId, token) }
                    .observeOn(AndroidSchedulers.mainThread())
        if (sessionManager.editingBackPhoto && !sessionManager.editingFrontPhoto)
            return repository.uploadImages(backPhotoFile!!, createNameFile(Constants.BACK_PHOTO))
                    .flatMap { uploadImageResponse: UploadImageResponse -> repository.createCredentials(addSOSUrl(uploadImageResponse.access_key, request, isFrontPhoto = false, isBackPhoto = true), userId, token) }
                    .observeOn(AndroidSchedulers.mainThread())
        if (!sessionManager.editingBackPhoto && !sessionManager.editingFrontPhoto)
            return repository.createCredentials(request, userId, token)
                    .observeOn(AndroidSchedulers.mainThread())

        return Single.zip(repository.uploadImages(frontPhotoFile!!, createNameFile(Constants.FRONT_PHOTO)),
                repository.uploadImages(backPhotoFile!!, createNameFile(Constants.BACK_PHOTO)),
                { uploadImageResponse: UploadImageResponse, uploadImageResponse2: UploadImageResponse -> addBothSOSUrl(uploadImageResponse.access_key, uploadImageResponse2.access_key, request) })
                .flatMap { credentialCreateRequest: CredentialCreateUpdateRequest? -> repository.createCredentials(credentialCreateRequest!!, userId, token) }
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun buildDeleteCredentialAsyncCall(): Single<DeleteCredentialResponse> = repository.deleteCredential(credentialId, Constants.BEARER_HEADER + sessionManager.token)

    override fun buildGetCredentialsAsyncCall(): Single<CredentialsResponse> = repository.getCredentials(userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token)


}