package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SitePermitTemplate

sealed class TemplatePermitFragmentState {
    data class ShowTemplateList(val templateList:List<SitePermitTemplate>):TemplatePermitFragmentState()
    object ShowError:TemplatePermitFragmentState()
    data class NavigateToPermitDetails(val permitInfo: PermitInfo):TemplatePermitFragmentState()
    object ShowProgressView:TemplatePermitFragmentState()
    object RemoveProgressView:TemplatePermitFragmentState()
}