package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.ApiResponse

sealed class EmergencyInitialFragmentState {
    object GetEvacuationVisitor:EmergencyInitialFragmentState()
    object NavigateToEmergencyProgress:EmergencyInitialFragmentState()
    data class Error(val error: Throwable):EmergencyInitialFragmentState()
    data class TriggerError(val apiResponse: ApiResponse):EmergencyInitialFragmentState()
}