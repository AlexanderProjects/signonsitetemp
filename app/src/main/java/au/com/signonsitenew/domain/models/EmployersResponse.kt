package au.com.signonsitenew.domain.models

data class EmployersResponse (var msg:String, var companies:Map<Int,String>, var status:String)