package au.com.signonsitenew.domain.models.adapters

import android.os.Parcelable
import au.com.signonsitenew.domain.models.attendees.Attendance
import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import au.com.signonsitenew.domain.models.state.AttendanceAlertState
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AttendanceChildAdapterModel (val workerName:String,
                                        val userId:Int,
                                        val phoneNumber:String?,
                                        var timeIn:String,
                                        val timeOut:String?,
                                        val inductionState:String?,
                                        val needsToDoInduction:Boolean,
                                        val inductionId:Int?,
                                        val briefingStatus:String,
                                        val inductionType:String?,
                                        val companyName:String,
                                        val attendanceAlertState: AttendanceAlertState,
                                        val attendances:List<Attendance>,
                                        val workerNotes:List<WorkerNotes>?):Parcelable