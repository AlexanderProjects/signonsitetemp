package au.com.signonsitenew.domain.models.attendees

data class SiteInduction(val id:Int?,val type:String?,val number:Int?,val state: String?)