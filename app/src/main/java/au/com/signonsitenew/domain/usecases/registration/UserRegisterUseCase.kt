package au.com.signonsitenew.domain.usecases.registration

import au.com.signonsitenew.domain.models.UserRegistrationRequest
import au.com.signonsitenew.domain.models.UserRegistrationResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface UserRegisterUseCase {
    fun registration(): Single<UserRegistrationResponse>
}

class UserRegisterUseCaseImpl @Inject constructor(private val dataRepository: DataRepository, private val sessionManager: SessionManager):UserRegisterUseCase {

    override fun registration(): Single<UserRegistrationResponse> =
            dataRepository.userRegistration(buildRequest())
                    .observeOn(AndroidSchedulers.mainThread())

    private fun buildRequest():UserRegistrationRequest{
        return UserRegistrationRequest(email = sessionManager.currentUser[Constants.USER_EMAIL]
                ,email_verification_token = null,
                password = sessionManager.registrationDetails[Constants.REGISTER_PASS]!!,
                first_name = sessionManager.currentUser[Constants.USER_FIRST_NAME]!!,
                last_name = sessionManager.currentUser[Constants.USER_LAST_NAME]!!,
                phone_number = sessionManager.currentUser[Constants.USER_PHONE]!!,
                alpha2 = sessionManager.registrationDetails[Constants.REGISTER_LOCALE]!!,
                company_name = sessionManager.currentUser[Constants.USER_COMPANY]!!,
                diagnostics_enable = false
        )
    }

}