package au.com.signonsitenew.domain.models

data class StopEvacuationRequest (val user_id:Long,val logged:Boolean)