package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.ContentTypeItem
import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.state.*
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.models.UploadImageResponse
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import org.joda.time.format.ISODateTimeFormat
import java.io.File
import javax.inject.Inject

interface PermitsChecksTabUseCase {
    fun setPermitInfo(permitInfo: PermitInfo)
    fun getPermitInfo(): PermitInfo
    fun mapComponentState(checksState: PermitChecksState): PermitContentTypeState
    fun mapStringTypeToContentType(contentTypeItem: ContentTypeItem):PermitContentType
    fun getRequestChecksSectionState():PermitContentTypeState
    fun getInProgressChecksSectionState():PermitContentTypeState
    fun getClosingChecksSectionState():PermitContentTypeState
    fun checkForRequestChecksComponents(values:()->Unit,empty:()->Unit)
    fun checkForInProgressChecksComponents(values:()->Unit,empty:()->Unit)
    fun checkForClosingChecksComponents(values:()->Unit,empty:()->Unit)
    fun areComponentsEmpty():Boolean
    fun getLastContentTypeResponse(responses:List<PermitContentTypeResponses>): PermitContentTypeResponses
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun getLocalFullDateName():String
}
class PermitsChecksTabUseCaseImpl @Inject constructor(private val sessionManager: SessionManager, private val repository: DataRepository):PermitsChecksTabUseCase {

    private lateinit var permitInfo:PermitInfo
    private val isManager:Boolean get() = repository.hasManagerPermission(sessionManager.userId.toLong(), sessionManager.siteId.toLong())
    private val isRequester:Boolean get() =  permitInfo.requester_user.id == sessionManager.userId.toInt()
    private val isPermitInRequestState:Boolean get() = permitInfo.state == PermitState.Request
    private val isPermitInPendingApprovalState:Boolean get() = permitInfo.state == PermitState.PendingApproval
    private val isPermitInApprovedState:Boolean get() = permitInfo.state == PermitState.InProgress
    private val isPermitInClosingState:Boolean get() = permitInfo.state == PermitState.PendingClosure
    private val isPermitInClosedState get() = permitInfo.state == PermitState.Closed || permitInfo.state == PermitState.Suspended || permitInfo.state == PermitState.RequesterCancelled

    override fun setPermitInfo(permitInfo: PermitInfo) { this.permitInfo = permitInfo }

    override fun getPermitInfo(): PermitInfo  = this.permitInfo

    override fun mapComponentState(checksState: PermitChecksState): PermitContentTypeState { return checkRulesForComponentState(checksState) }

    override fun mapStringTypeToContentType(contentTypeItem: ContentTypeItem): PermitContentType {
        return when(contentTypeItem.type){
            Constants.PERMIT_CONTENT_TYPE_PLAIN_TEXT -> PermitContentType.PlainText
            Constants.PERMIT_CONTENT_TYPE_CHECKBOX -> PermitContentType.CheckBox
            Constants.PERMIT_CONTENT_TYPE_TEXT_INPUT -> PermitContentType.TextInput
            Constants.PERMIT_CONTENT_TYPE_PHOTO -> PermitContentType.Photo
            else -> PermitContentType.EmptyType
        }
    }

    override fun getRequestChecksSectionState(): PermitContentTypeState {
        return when {
            isPermitInRequestState ->{
                return when {
                    isRequester -> PermitContentTypeState.ReadableAndEditable
                    checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo, PermitDoneState.FinishedRequest) -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.ReadableAndEditable
                }
            }
            isPermitInPendingApprovalState -> {
                return when{
                    isRequester -> PermitContentTypeState.Readable
                    isManager  -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }
            }
            isPermitInApprovedState && isStartDateBefore(permitInfo.start_date) -> {
                return when {
                    isRequester -> PermitContentTypeState.Readable
                    isManager -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }

            }
            isPermitInApprovedState && isStartDateAfter(permitInfo.start_date) -> {
                return when{
                    isRequester -> PermitContentTypeState.Readable
                    isManager  -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }
            }
            isPermitInClosingState ->{
                return when {
                    isRequester -> PermitContentTypeState.Readable
                    isManager -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }
            }
            isPermitInClosedState ->{
                return when {
                    isRequester   -> PermitContentTypeState.Readable
                    isManager  -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }
            }
            else -> PermitContentTypeState.Readable
        }
    }

    override fun getInProgressChecksSectionState(): PermitContentTypeState {
         return when{
             isPermitInRequestState->{
                 return when{
                     isRequester  -> PermitContentTypeState.GreyedOut
                     checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo, PermitDoneState.FinishedRequest) -> PermitContentTypeState.GreyedOut
                     else -> PermitContentTypeState.GreyedOut
                 }
             }
             isPermitInPendingApprovalState  -> {
                 return when{
                     isRequester -> PermitContentTypeState.GreyedOut
                     isManager  -> PermitContentTypeState.GreyedOut
                     else -> PermitContentTypeState.GreyedOut
                 }
             }
             isPermitInApprovedState && isStartDateBefore(permitInfo.start_date) ->{
                 return when{
                     isRequester -> PermitContentTypeState.Readable
                     isManager  -> PermitContentTypeState.Readable
                     else -> PermitContentTypeState.Readable
                 }
             }
             isPermitInApprovedState && isStartDateAfter(permitInfo.start_date) -> {
                 return when{
                     isRequester -> PermitContentTypeState.Readable
                     isManager  -> PermitContentTypeState.ReadableAndEditable
                     else -> PermitContentTypeState.ReadableAndEditable
                 }
             }
             isPermitInClosingState ->{
                 when {
                     isRequester -> PermitContentTypeState.Readable
                     isManager -> PermitContentTypeState.Readable
                     else -> PermitContentTypeState.Readable
                 }
             }
             isPermitInClosedState ->{
                 when {
                     isRequester -> PermitContentTypeState.Readable
                     isManager -> PermitContentTypeState.Readable
                     else -> PermitContentTypeState.Readable
                 }
             }
             else -> PermitContentTypeState.Readable
        }

    }

    override fun getClosingChecksSectionState(): PermitContentTypeState {
        return when{
            isPermitInRequestState -> {
                when{
                    isRequester  -> PermitContentTypeState.GreyedOut
                    isManager-> PermitContentTypeState.GreyedOut
                    else  -> PermitContentTypeState.GreyedOut
                }
            }
            isPermitInPendingApprovalState -> {
                return when{
                    isRequester -> PermitContentTypeState.GreyedOut
                    isManager  -> PermitContentTypeState.GreyedOut
                    else -> PermitContentTypeState.GreyedOut
                }
            }
            isPermitInApprovedState && isStartDateBefore(permitInfo.start_date) ->{
                return when{
                    isRequester -> PermitContentTypeState.GreyedOut
                    isManager  -> PermitContentTypeState.GreyedOut
                    else -> PermitContentTypeState.GreyedOut
                }
            }
            isPermitInApprovedState && isStartDateAfter(permitInfo.start_date) -> {
                return when{
                    isRequester -> PermitContentTypeState.GreyedOut
                    isManager  -> PermitContentTypeState.GreyedOut
                    else -> PermitContentTypeState.GreyedOut
                }
            }
            isPermitInClosingState ->{
                when {
                    isRequester -> PermitContentTypeState.Readable
                    isManager -> PermitContentTypeState.ReadableAndEditable
                    else -> PermitContentTypeState.Readable
                }
            }
            isPermitInClosedState ->{
                when {
                    isRequester -> PermitContentTypeState.Readable
                    isManager -> PermitContentTypeState.Readable
                    else -> PermitContentTypeState.Readable
                }
            }
            else -> PermitContentTypeState.GreyedOut
        }
    }

    override fun checkForRequestChecksComponents(values: () -> Unit, empty: () -> Unit) {
        if(permitInfo.request_checks.isNotEmpty()){
            values()
        }else{
            empty()
        }
    }

    override fun checkForInProgressChecksComponents(values: () -> Unit, empty: () -> Unit) {
        if(permitInfo.in_progress_checks.isNotEmpty()){
            values()
        }else{
            empty()
        }
    }

    override fun checkForClosingChecksComponents(values: () -> Unit, empty: () -> Unit) {
        if(permitInfo.pending_closure_checks.isNotEmpty()){
            values()
        }else{
            empty()
        }
    }

    override fun areComponentsEmpty():Boolean {
        return permitInfo.request_checks.isEmpty() && permitInfo.in_progress_checks.isEmpty() && permitInfo.pending_closure_checks.isEmpty()
    }

    override fun getLastContentTypeResponse(responses: List<PermitContentTypeResponses>): PermitContentTypeResponses {
        val sortedList = responses.sortedByDescending { it.utc_date_time }
        return  sortedList.first()
    }

    override fun uploadImages(file: File, fileName: String): Single<UploadImageResponse> {
        return repository.uploadImages(file, fileName)
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getLocalFullDateName(): String {
        return repository.getLocalUserFullName()
    }

    private fun checkRulesForComponentState(checksState: PermitChecksState):PermitContentTypeState{
        return when(checksState) {
            PermitChecksState.RequestChecks -> getRequestChecksSectionState()
            PermitChecksState.InProgressChecks -> getInProgressChecksSectionState()
            else -> getClosingChecksSectionState()
        }
    }

    private fun checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo: PermitInfo, permitDoneState: PermitDoneState):Boolean{
        val filterList = permitInfo.requestee_users.filter { it.status == permitDoneState }
        return filterList.isNotEmpty()
    }

    private fun isStartDateBefore(startDateTime: String?): Boolean {
        return if(startDateTime != null)
            ISODateTimeFormat.dateTimeParser().parseDateTime(startDateTime).isBeforeNow
        else
            false
    }

    private fun isStartDateAfter(startDateTime: String?):Boolean {
        return if(startDateTime != null)
            ISODateTimeFormat.dateTimeParser().parseDateTime(startDateTime).isAfterNow
        else
            false
    }

}