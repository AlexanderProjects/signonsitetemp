package au.com.signonsitenew.domain.models.state

 sealed class BriefingsFragmentState {
     object ShowRichTypeText: BriefingsFragmentState()
     object ShowPlainTypeText: BriefingsFragmentState()
     object ShowProgressLayout : BriefingsFragmentState()
     object HideProgressLayout : BriefingsFragmentState()
     object Error: BriefingsFragmentState()
     data class DataError(val error:String): BriefingsFragmentState()
     data class NeedsAcknowledge(var isAcknowledge: Boolean):BriefingsFragmentState()
     data class ShouldShowSignature(var hasSignature:Boolean): BriefingsFragmentState()
}