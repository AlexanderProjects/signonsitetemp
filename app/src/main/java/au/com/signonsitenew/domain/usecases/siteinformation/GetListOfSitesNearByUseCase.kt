package au.com.signonsitenew.domain.usecases.siteinformation

import au.com.signonsitenew.domain.models.NearSitesResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface GetListOfSitesNearByUseCase {
    fun getNearBySites(latitude:Float, longitude:Float,uncertainty:Float): Single<NearSitesResponse>
}

class GetListOfSitesNearByUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService):GetListOfSitesNearByUseCase {
    override fun getNearBySites(latitude:Float, longitude:Float, uncertainty:Float): Single<NearSitesResponse> =
            repository.getNearBySites(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token,latitude = latitude, longitude = longitude, uncertainty = uncertainty)
                    .observeOn(AndroidSchedulers.mainThread())

}