package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.DisposableSubscriber

abstract class DocumentsBaseUseCase<T> protected constructor() {
    val disposables = CompositeDisposable()

    abstract fun buildGetActiveDocumentsAsyncCall() : Flowable<T>

    fun getActiveDocumentsAsync(asyncCall: DisposableSubscriber<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetActiveDocumentsAsyncCall()
                .doOnComplete { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}