package au.com.signonsitenew.domain.models


data class CredentialCreateUpdateRequest (
        var edits_credential_id: Int?,
        var credential_type_id: Int?,
        var identifier: String?,
        var name: String?,
        var front_photo: String?,
        var back_photo: String?,
        var issue_date: String?,
        var expiry_date: String?,
        var issued_by: String?,
        var rto: String?,
        var registration_number: String?,
        var reference: String?){
    constructor():this(null,null,null,null,null,null,null,null,null,null,null,null)
}