package au.com.signonsitenew.domain.usecases.registration

interface SimpleTextValidationUseCase {
    fun validateFirstName(firstName:String):Boolean
    fun validateLastName(lastName:String):Boolean
}
class SimpleTextValidationUseCaseImpl : SimpleTextValidationUseCase {
    override fun validateFirstName(firstName:String):Boolean = firstName.matches("[a-zA-Z' -]+".toRegex())
    override fun validateLastName(lastName:String):Boolean = lastName.matches("[a-zA-Z' -]+".toRegex())
}