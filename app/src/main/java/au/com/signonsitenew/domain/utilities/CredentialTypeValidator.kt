package au.com.signonsitenew.domain.utilities

import android.text.TextUtils
import au.com.signonsitenew.domain.models.CredentialField
import au.com.signonsitenew.domain.models.CredentialType
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.DateFormatUtil
import java.io.File

object CredentialTypeValidator {
    private var credentialType: CredentialType? = null
    @JvmStatic
    fun setCredentialType(type: CredentialType?) {
        credentialType = type
    }

    fun validateName(): String? {
        if (credentialType!!.name != null) {
            if (credentialType!!.name.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.name
            if (credentialType!!.name.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.identifier.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.name
        }
        return null
    }

    @JvmStatic
    fun validateIdentifier(): String? {
        if (credentialType!!.identifier != null) {
            if (credentialType!!.identifier.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.identifier
            if (credentialType!!.identifier.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.identifier.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.identifier
        }
        return null
    }

    fun validateFrontPhoto(): String? {
        if (credentialType!!.front_photo != null) {
            if (credentialType!!.front_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.front_photo
            if (credentialType!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.front_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.front_photo
        }
        return null
    }

    fun validateBackPhoto(): String? {
        if (credentialType!!.back_photo != null) {
            if (credentialType!!.back_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.back_photo
            if (credentialType!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.back_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.back_photo
        }
        return null
    }

    @JvmStatic
    fun validateCredentialIssueDate(): String? {
        if (credentialType!!.issue_date != null) {
            if (credentialType!!.issue_date.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.issue_date
            if (credentialType!!.issue_date.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.issue_date.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.issue_date
        }
        return null
    }

    @JvmStatic
    fun validateCredentialIssueBy(): String? {
        if (credentialType!!.issued_by != null) {
            if (credentialType!!.issued_by.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.issued_by
            if (credentialType!!.issued_by.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.issued_by.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.issued_by

        }
        return null
    }

    @JvmStatic
    fun validateCredentialExpiryDate(): String? {
        if (credentialType!!.expiry_date != null) {
            if (credentialType!!.expiry_date.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.expiry_date
            if (credentialType!!.expiry_date.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.expiry_date.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.expiry_date
        }
        return null
    }

    @JvmStatic
    fun validateCredentialRto(): String? {
        if (credentialType!!.rto != null) {
            if (credentialType!!.rto.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.rto
            if (credentialType!!.rto.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.rto.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.rto
        }
        return null
    }

    @JvmStatic
    fun validateCredentialReferenceNumber(): String? {
        if (credentialType!!.registration_number != null) {
            if (credentialType!!.registration_number.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.registration_number
            if (credentialType!!.registration_number.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.registration_number.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.registration_number
        }
        return null
    }

    @JvmStatic
    fun validateCredentialReference(): String? {
        if (credentialType!!.reference != null) {
            if (credentialType!!.reference.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return credentialType!!.reference
            if (credentialType!!.reference.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            if (credentialType!!.reference.equals(Constants.OPTIONAL, ignoreCase = true)) return credentialType!!.reference
        }
        return null
    }

    fun isValidForm(credentialFieldList: List<CredentialField>, frontPhoto: File?, backPhoto: File?): Boolean {
        val response = arrayListOf<Boolean>()
        for (field in credentialFieldList) {
            if (TextUtils.isEmpty(field.value) || field.value.equals("required", ignoreCase = true) || !isValidFrontPhoto(frontPhoto) || !isValidBackPhoto(backPhoto))
                response.add(false)
            if (field.title == Constants.ISSUE_DATE && !field.value.equals(Constants.OPTIONAL, ignoreCase = true)) {
                response.add(DateFormatUtil.isValidateIssuedDate(field.value))
            }
            if(field.title == Constants.ISSUED_BY && field.value.equals(Constants.REQUIRED, ignoreCase = true))
                response.add(false)

            if (field.title == Constants.EXPIRY_DATE && !field.value.equals(Constants.OPTIONAL, ignoreCase = true)) {
                response.add(DateFormatUtil.isValidateExpiryDate(field.value))
            }
        }
        for (result in response) if(!result) return false
        return true
    }

    private fun isValidFrontPhoto(photo: File?): Boolean {
        if (credentialType!!.front_photo != null) {
            if (credentialType!!.front_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true) && photo != null) return true
            if (credentialType!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true) && photo != null) return false else if (credentialType!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true) && photo == null) return true
            if (credentialType!!.front_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
        }
        return false
    }

    private fun isValidBackPhoto(photo: File?): Boolean {
        if (credentialType!!.back_photo != null) {
            if (credentialType!!.back_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true) && photo != null) return true
            if (credentialType!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true) && photo != null) return false else if (credentialType!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true) && photo == null) return true
            if (credentialType!!.back_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
        }
        return false
    }
}