package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SiteCompany(val name:String):Parcelable