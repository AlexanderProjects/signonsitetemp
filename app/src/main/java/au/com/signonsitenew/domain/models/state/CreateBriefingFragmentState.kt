package au.com.signonsitenew.domain.models.state

sealed class CreateBriefingFragmentState {
    data class IsAdvancedBriefingLayout(val isAdvancedLayout:Boolean):CreateBriefingFragmentState()
}