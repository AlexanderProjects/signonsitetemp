package au.com.signonsitenew.domain.models.state

sealed class AttendanceNotificationState {
    object BriefingExistInductionNoExistBriefingUnAck:AttendanceNotificationState()
    object BriefingNoExistInductionExistInductionIncomplete:AttendanceNotificationState()
    object BriefingNoExistInductionNoExist:AttendanceNotificationState()
    object InductionExistBriefingExistInductionCompleteBriefingUnAck:AttendanceNotificationState()
    object InductionExistBriefingExistInductionInCompleteBriefingUnAck:AttendanceNotificationState()
    object InductionExistBriefingExistInductionInCompleteBriefingAck:AttendanceNotificationState()
    object InductionExistBriefingExistInductionCompleteBriefingAck:AttendanceNotificationState()
    object AutoSignOff:AttendanceNotificationState()

}