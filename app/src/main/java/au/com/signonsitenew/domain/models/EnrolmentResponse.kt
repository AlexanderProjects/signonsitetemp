package au.com.signonsitenew.domain.models


data class EnrolmentResponse(override val status: String, val enrolments :MutableList<Enrolment>): ApiResponse(status)