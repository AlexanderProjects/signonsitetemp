package au.com.signonsitenew.domain.models.attendees

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreatedByUser(val id:Int,val first_name:String,val last_name:String) : Parcelable