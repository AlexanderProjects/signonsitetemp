package au.com.signonsitenew.domain.models

data class CompanyFeatureFlagResponse (override val status:String, val owner_company_feature_flags: List<FeatureFlag>): ApiResponse(status)