package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhoneNumber(var alpha2: String?,
                       var number: String?) : Parcelable{
    constructor() : this(null, null)
}