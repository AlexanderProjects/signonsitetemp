package au.com.signonsitenew.domain.usecases.workerdetails

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.base.WorkerDetailsBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.ui.attendanceregister.FormViewFragment
import au.com.signonsitenew.ui.attendanceregister.InductionOptionsFragment
import au.com.signonsitenew.ui.attendanceregister.InductionReviewFragment
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import javax.inject.Inject

interface WorkerDetailsUseCase {
    fun getInductionIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getBriefingIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getWorkerNotesIconState(workerNotesList: List<WorkerNotes>?):Int
    fun getWorkerNotesCellState(workerNotesList: List<WorkerNotes>?):Int
    fun getSignButtonState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getInductionCellState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
    fun getFragmentAccordingToInductionState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Fragment?
    fun canAccessUserInductions(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int
    fun analyticsForWorkerNotes()
    fun attendanceRegisterWorkerSignOnPressedAnalytics(targetted_user_id:Int)
    fun attendanceRegisterWorkerSignOffPressedAnalytics(targetted_user_id: Int)
    fun canAccessToWorkerNotes(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int
}


class WorkerDetailsUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val analyticsEventDelegateService: AnalyticsEventDelegateService) : WorkerDetailsUseCase, WorkerDetailsBaseUseCase<ApiResponse, ApiResponse>() {


    override fun getInductionIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel):Int = when  {
        attendanceChildAdapterModel.inductionType == Constants.DOC_INDUCTION_VISITOR -> R.drawable.ic_visitor_green_24dp
        attendanceChildAdapterModel.inductionState == Constants.DOC_INDUCTION_PENDING -> R.drawable.ic_review_red_24dp
        attendanceChildAdapterModel.inductionState == Constants.DOC_INDUCTION_INCOMPLETE -> R.drawable.ic_red_cross_24dp
        attendanceChildAdapterModel.inductionState == Constants.DOC_INDUCTION_REJECTED -> R.drawable.ic_red_cross_24dp
        attendanceChildAdapterModel.inductionState == Constants.DOC_INDUCTION_COMPLETE -> R.drawable.ic_check_green_24
        attendanceChildAdapterModel.needsToDoInduction -> R.drawable.ic_red_cross_24dp
        else -> R.drawable.ic_n_a
    }

    override fun getBriefingIconState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int  = when (attendanceChildAdapterModel.briefingStatus) {
        Constants.DOC_BRIEFING_ACKNOWLEDGED -> R.drawable.ic_check_green_24
        Constants.DOC_BRIEFING_UNACKNOWLEDGED -> R.drawable.ic_red_cross_24dp
        else -> R.drawable.ic_n_a
    }

    override fun getWorkerNotesIconState(workerNotesList: List<WorkerNotes>?):Int {
        workerNotesList?.forEach {
            when(it.importance){
                "alert" -> return R.drawable.ic_danger
                "warning" -> return R.drawable.ic_warn
                "info" -> return R.drawable.ic_info
            }
        }
        return R.drawable.ic_n_a
    }

    override fun getWorkerNotesCellState(workerNotesList: List<WorkerNotes>?): Int = when {
        !workerNotesList.isNullOrEmpty() -> View.VISIBLE
        else ->   View.INVISIBLE
    }

    override fun getSignButtonState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int {
        return if(attendanceChildAdapterModel.timeOut.equals("null"))
            R.string.sign_off
        else
            R.string.sign_on
    }

    override fun getInductionCellState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int {
        return if(attendanceChildAdapterModel.needsToDoInduction)
            View.VISIBLE
        else
            View.INVISIBLE
    }


    override fun getFragmentAccordingToInductionState(attendanceChildAdapterModel: AttendanceChildAdapterModel): Fragment? {
        var fragment: Fragment? = null
        val args = Bundle()
        when (attendanceChildAdapterModel.inductionState) {
            Constants.DOC_INDUCTION_PENDING -> {
                analyticsEventDelegateService.managerAttendanceRegisterSiteInductionReviewed(attendanceChildAdapterModel.userId)
                fragment = InductionReviewFragment()
            }
            Constants.DOC_INDUCTION_INCOMPLETE -> fragment = InductionOptionsFragment()
            Constants.DOC_INDUCTION_COMPLETE -> {
                fragment = FormViewFragment()
                if (attendanceChildAdapterModel.needsToDoInduction) {
                    when (attendanceChildAdapterModel.inductionType) {
                        "user_form" -> args.putString(ARG_ACTION, "view")
                        "forced_with_documents" -> args.putString(ARG_ACTION, "view_docs")
                    }
                }
                attendanceChildAdapterModel.inductionId?.let { args.putLong(INDUCTION_ID, it.toLong()) }
            }
            null -> fragment = InductionOptionsFragment()
        }

        if(checkForForcedInduction(attendanceChildAdapterModel))
            return null

        if (fragment != null) {
            args.putLong(ATTENDEE_ID, attendanceChildAdapterModel.userId.toLong())
            fragment.arguments = args
            return fragment
        }
        return null
    }

    override fun canAccessUserInductions(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int {
        return when {
            attendanceChildAdapterModel.inductionType == Constants.DOC_INDUCTION_VISITOR -> View.GONE
            repository.canAccessUserInductions(sessionManager.userId.toLong(), sessionManager.siteId.toLong()) -> View.VISIBLE
            else -> View.GONE
        }
    }

    override fun analyticsForWorkerNotes() {
        analyticsEventDelegateService.workerNotes()
    }

    override fun attendanceRegisterWorkerSignOnPressedAnalytics(targetted_user_id: Int) = analyticsEventDelegateService.attendanceRegisterWorkerSignOnPressed(targetted_user_id)
    override fun attendanceRegisterWorkerSignOffPressedAnalytics(targetted_user_id: Int) = analyticsEventDelegateService.attendanceRegisterWorkerSignOffPressed(targetted_user_id)

    override fun canAccessToWorkerNotes(attendanceChildAdapterModel: AttendanceChildAdapterModel): Int {
        return when {
            attendanceChildAdapterModel.inductionType == Constants.DOC_INDUCTION_VISITOR -> View.GONE
            attendanceChildAdapterModel.workerNotes?.isEmpty() == true -> View.GONE
            repository.canAccessUserInductions(sessionManager.userId.toLong(), sessionManager.siteId.toLong()) -> View.VISIBLE
            else -> View.GONE
        }
    }

    override fun buildAttendeeSignOnAsyncCall(attendeeId: String): Single<ApiResponse> = repository.attendeeSignOn(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, sessionManager.siteId.toString(), attendeeId)

    override fun buildAttendeeSignOffAsyncCall(attendeeId: String): Single<ApiResponse> = repository.attendeeSignOff(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, sessionManager.siteId.toString(), attendeeId)

    private fun checkForForcedInduction(attendanceChildAdapterModel: AttendanceChildAdapterModel): Boolean {
        if (attendanceChildAdapterModel.needsToDoInduction) {
            if (attendanceChildAdapterModel.inductionType != null && attendanceChildAdapterModel.inductionType == "forced") {
                return true
            }
            return false
        }
        return false
    }

    companion object {
        private const val ARG_ACTION = "action"
        private const val INDUCTION_ID = "inductionId"
        const val ATTENDEE_ID = "attendeeId"
    }

}