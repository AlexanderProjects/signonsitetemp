package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

abstract class MenuBaseUseCase<T> protected constructor()  {
    val disposables = CompositeDisposable()

    abstract fun buildGetPersonalInfoAsyncCall() : Single<T>

    fun getPersonalInfoAsync(asyncCall: DisposableSingleObserver<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetPersonalInfoAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}