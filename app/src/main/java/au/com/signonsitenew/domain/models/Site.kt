package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Site (val id:Int,
                 val name: String,
                 val address: String,
                 val timezone:String,
                 val is_site_induction_forms_enabled: Boolean,
                 val principal_company : Company):Parcelable