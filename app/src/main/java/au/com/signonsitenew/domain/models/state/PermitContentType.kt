package au.com.signonsitenew.domain.models.state
sealed class PermitContentType{
    object  PlainText:PermitContentType()
    object  CheckBox:PermitContentType()
    object  Photo:PermitContentType()
    object  TextInput:PermitContentType()
    object EmptyType:PermitContentType()
}
