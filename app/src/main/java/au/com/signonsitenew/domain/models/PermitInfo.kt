package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.domain.models.state.PermitState
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PermitInfo(var id:Int,
                      var human_id:String,
                      var state: PermitState,
                      var permit_template:PermitTemplate,
                      var requester_user:RequesterUser,
                      var requestee_users:List<RequesteeUser>,
                      var start_date:String?,
                      var end_date:String?,
                      var tasks:List<ContentTypeItem>,
                      var request_checks:List<ContentTypeItem>,
                      var in_progress_checks:List<ContentTypeItem>,
                      var pending_closure_checks:List<ContentTypeItem>):Parcelable