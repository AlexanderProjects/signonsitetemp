package au.com.signonsitenew.domain.models

data class CredentialField (var title: String?, var value: String?){
    constructor() : this(null,null)
}