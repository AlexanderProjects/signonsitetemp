package au.com.signonsitenew.domain.usecases.notifications

import au.com.signonsitenew.domain.models.state.AttendanceNotificationState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.utilities.Constants
import javax.inject.Inject

interface NotificationUseCase {
    fun showBriefingRemoteNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun evacuationStartedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun evacuationEndedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun showInductionRejectNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun showDefaultNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun remoteRiskyWorkerNotification(message: String?, userId: String, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase
    fun hasPrompt(value: Boolean): NotificationUseCase
    fun isUserSignedOn(value: Boolean): NotificationUseCase
    fun buildSiteNotifications(): NotificationUseCase
    fun cancelNotifications(notificationId: Int): NotificationUseCase
    fun setSiteId(siteId: Int): NotificationUseCase
    fun setState(attendanceNotificationState: AttendanceNotificationState): NotificationUseCase
    fun convertDocumentBooleansToAttendanceState(needAckBriefing: Boolean, isCompleteInduction: Boolean, briefingExist: Boolean, inductionExist: Boolean): AttendanceNotificationState
}

class NotificationsUseCaseImpl @Inject constructor(private val repository: DataRepository,
                                                   private val analyticsEventDelegateService: AnalyticsEventDelegateService) : NotificationUseCase {
    private var isPrompt = false
    private var isUserSignedOn = false
    private var siteId: Int = 0
    private lateinit var attendanceNotificationState: AttendanceNotificationState

    override fun showBriefingRemoteNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.remoteBriefingNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun evacuationStartedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.evacuationStartedNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun evacuationEndedNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.evacuationEndedNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun showInductionRejectNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.remoteInductionNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun showDefaultNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.defaultNotification(message, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun remoteRiskyWorkerNotification(message: String?, userId: String, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?): NotificationUseCase {
        repository.remoteRiskyWorkerNotification(message, userId, extraProperties, notificationName, utcNotificationSentAt, analyticsEventDelegateService)
        return this
    }

    override fun hasPrompt(value: Boolean): NotificationUseCase {
        isPrompt = if (value)
            value
        else
            value
        return this
    }

    override fun isUserSignedOn(value: Boolean): NotificationUseCase {
        isUserSignedOn = if (value)
            value
        else
            value
        return this
    }

    override fun buildSiteNotifications(): NotificationUseCase {
        if (isPrompt)
            if (isUserSignedOn)
                repository.localSignOnPromptNotification(Constants.PROMPTED_SIGN_ON, analyticsEventDelegateService, HashMap(), siteId)
            else
                repository.localSignOffPromptNotification(Constants.PROMPTED_SIGN_OFF, analyticsEventDelegateService, HashMap(), siteId)
        else {
            if (isUserSignedOn) {
                val attendanceState = if (::attendanceNotificationState.isInitialized) attendanceNotificationState else AttendanceNotificationState.BriefingNoExistInductionNoExist
                repository.localAttendanceNotification(isUserSignedOn, repository.getCurrentSiteName(siteId), analyticsEventDelegateService, HashMap(), siteId, attendanceState)
            } else
                repository.localAttendanceNotification(isUserSignedOn, repository.getCurrentSiteName(siteId), analyticsEventDelegateService, HashMap(), siteId, AttendanceNotificationState.AutoSignOff)
        }

        return this
    }

    override fun cancelNotifications(notificationId: Int): NotificationUseCase {
        repository.cancelNotifications(notificationId)
        return this
    }

    override fun setSiteId(siteId: Int): NotificationUseCase {
        this.siteId = siteId
        return this
    }

    override fun setState(attendanceNotificationState: AttendanceNotificationState): NotificationUseCase {
        this.attendanceNotificationState = attendanceNotificationState
        return this
    }

    override fun convertDocumentBooleansToAttendanceState(needAckBriefing: Boolean, isCompleteInduction: Boolean, briefingExist: Boolean, inductionExist: Boolean): AttendanceNotificationState {
        if (!briefingExist && inductionExist && !isCompleteInduction)
            return AttendanceNotificationState.BriefingNoExistInductionExistInductionIncomplete
        if (briefingExist && !inductionExist && needAckBriefing)
            return AttendanceNotificationState.BriefingExistInductionNoExistBriefingUnAck
        if (inductionExist && briefingExist && !isCompleteInduction && needAckBriefing)
            return AttendanceNotificationState.InductionExistBriefingExistInductionInCompleteBriefingUnAck
        if (inductionExist && briefingExist && isCompleteInduction && needAckBriefing)
            return AttendanceNotificationState.InductionExistBriefingExistInductionCompleteBriefingUnAck

        return AttendanceNotificationState.InductionExistBriefingExistInductionCompleteBriefingAck
    }

}