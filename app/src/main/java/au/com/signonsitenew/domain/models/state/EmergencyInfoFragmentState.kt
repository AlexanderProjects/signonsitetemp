package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.UserInfoUpdateRequest

sealed class EmergencyInfoFragmentState {
    data class ClearButtonClicked(val value: String): EmergencyInfoFragmentState()
    object ShowSaveButton: EmergencyInfoFragmentState()
    object HideSaveButton: EmergencyInfoFragmentState()
    data class SaveButtonClicked(val userInfoUpdateRequest: UserInfoUpdateRequest) :EmergencyInfoFragmentState()
}