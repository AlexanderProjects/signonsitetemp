package au.com.signonsitenew.domain.models

data class UserRegistrationRequest(val email: String?,
                                   val email_verification_token:String?,
                                   val password:String,
                                   val first_name:String,
                                   val last_name:String,
                                   val phone_number:String,
                                   val alpha2:String,
                                   val company_name:String,
                                   val diagnostics_enable:Boolean)