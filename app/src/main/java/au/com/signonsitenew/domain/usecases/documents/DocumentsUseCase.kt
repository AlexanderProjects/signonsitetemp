package au.com.signonsitenew.domain.usecases.documents

import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.DocumentsBaseUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class DocumentsUseCase @Inject constructor(private val repository: DataRepository,
                                           private val sessionManager: SessionManager) :DocumentsBaseUseCase<Any>() {
    override fun buildGetActiveDocumentsAsyncCall(): Flowable<Any> = Single.concat(repository.getActiveInductions(Constants.BEARER_HEADER + sessionManager.token,sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY,sessionManager.siteId.toString()),
            repository.getActiveBriefings(sessionManager.siteId.toString(), Constants.BEARER_HEADER + sessionManager.token))

}