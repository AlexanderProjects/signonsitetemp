package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.utilities.Util
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
@JsonClass(generateAdapter = true)
@Parcelize
data class Gender(var tag: String?,
                  var value: String?) : Parcelable{
    constructor() : this(null, null)
}