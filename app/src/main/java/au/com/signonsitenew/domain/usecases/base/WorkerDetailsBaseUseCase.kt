package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

abstract class WorkerDetailsBaseUseCase<T,P> protected constructor() {
    val disposables = CompositeDisposable()

    abstract fun buildAttendeeSignOnAsyncCall(attendeeId:String) : Single<T>

    abstract fun buildAttendeeSignOffAsyncCall(attendeeId:String) : Single<P>

    fun signOnAttendeeAsync(attendeeId:String,asyncCall: DisposableSingleObserver<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildAttendeeSignOnAsyncCall(attendeeId)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun signOffAttendeeAsync(attendeeId:String,asyncCall: DisposableSingleObserver<P>){
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildAttendeeSignOffAsyncCall(attendeeId)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}