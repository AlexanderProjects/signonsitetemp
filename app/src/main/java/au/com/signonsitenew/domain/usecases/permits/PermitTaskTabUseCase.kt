package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.PermitContentTypeResponses
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.state.PermitContentType
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.events.RxBusTaskTab
import au.com.signonsitenew.models.UploadImageResponse
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import java.io.File
import java.util.*
import javax.inject.Inject

interface PermitTaskTabUseCase {
    fun parseIsoFormatDates(date:String):String
    fun parseCalendarDates(date:String):String
    fun isEnableToEdit(permitInfo: PermitInfo):Boolean
    fun setPermitInfo(permitInfo: PermitInfo)
    fun getPermitInfo():PermitInfo
    fun sendPermitInfoUpdate(permitInfo: PermitInfo)
    fun convertUIDateTimeToISODateTimeFormat(dateTime:String):String
    fun convertIsoFormatToReadableUserFormat(dateTime: String):String
    fun convertCalendarFormatToUIFormat(dateTime: String):String
    fun isStartDateAfterNow(dateTime: String):Boolean
    fun isEndDateIsAfterStartDate(startDateTime:String, endDateTime:String):Boolean
    fun mapStringTypeToPermitContentType(contentType:String):PermitContentType
    fun getLastContentTypeResponse(responses:List<PermitContentTypeResponses>):PermitContentTypeResponses
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun getLocalUserFullName():String
}

class PermitTaskTabUseCaseImpl @Inject constructor(private val sessionManager: SessionManager,
                                                   private val repository: DataRepository,
                                                   private val rxBusTaskTab: RxBusTaskTab) : PermitTaskTabUseCase {

    private lateinit var permitInfo:PermitInfo

    private val calendarDateFormatter = DateTimeFormat.forPattern(Constants.PERMIT_CALENDAR_DATE_TIME).withLocale(Locale.ENGLISH).withOffsetParsed()

    override fun parseIsoFormatDates(date: String): String {
        val parsedDate = ISODateTimeFormat.dateTimeParser().parseDateTime(date)
        val formatter = DateTimeFormat.forPattern(Constants.PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB)
        return  parsedDate.toString(formatter)
    }

    override fun parseCalendarDates(date: String): String {
        val dateTime = DateTime.parse(date,calendarDateFormatter)
        val finalFormatter = DateTimeFormat.forPattern(Constants.PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB)
        return dateTime.toString(finalFormatter)
    }

    override fun isEnableToEdit(permitInfo: PermitInfo): Boolean {
        return permitInfo.state == PermitState.Request && permitInfo.requester_user.id == sessionManager.userId.toInt()
    }

    override fun setPermitInfo(permitInfo: PermitInfo) {
        this.permitInfo = permitInfo
    }

    override fun getPermitInfo(): PermitInfo {
      return this.permitInfo
    }

    override fun sendPermitInfoUpdate(permitInfo: PermitInfo) {
        rxBusTaskTab.sendSelectedMembers(permitInfo)
    }

    override fun convertUIDateTimeToISODateTimeFormat(dateTime: String): String {
        val formatter = DateTimeFormat.forPattern(Constants.PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB)
        val dateTimeFormatted = DateTime.parse(dateTime, formatter)
        return dateTimeFormatted.toDateTimeISO().toString()
    }

    override fun convertIsoFormatToReadableUserFormat(dateTime: String): String {
        val formatter = DateTimeFormat.forPattern(Constants.PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB)
        return DateTime.parse(dateTime, formatter).withZone(DateTimeZone.getDefault()).toInstant().toString(formatter)
    }

    override fun convertCalendarFormatToUIFormat(dateTime: String): String {
        val formatter = DateTimeFormat.forPattern(Constants.PERMIT_DISPLAY_DATE_TIME_FORMAT_IN_TASK_TAB)
        return DateTime.parse(dateTime, formatter).toString(formatter)
    }

    override fun isStartDateAfterNow(dateTime: String): Boolean {
        val dateTimeFormatted = DateTime.parse(dateTime,calendarDateFormatter)
        return ISODateTimeFormat.dateTimeParser().parseDateTime(dateTimeFormatted.toString()).isAfterNow
    }

    override fun isEndDateIsAfterStartDate(startDateTime:String, endDateTime:String): Boolean {
        val startDateTimeFormatted = DateTime.parse(startDateTime,calendarDateFormatter)
        val endDateTimeFormatted = DateTime.parse(endDateTime, calendarDateFormatter)
        val startDate = ISODateTimeFormat.dateTimeParser().parseDateTime(startDateTimeFormatted.toString())
        val endDate = ISODateTimeFormat.dateTimeParser().parseDateTime(endDateTimeFormatted.toString())
        return endDate.isAfter(startDate)
    }

    override fun mapStringTypeToPermitContentType(contentType: String): PermitContentType {
        return when (contentType){
            Constants.PERMIT_CONTENT_TYPE_CHECKBOX -> PermitContentType.CheckBox
            Constants.PERMIT_CONTENT_TYPE_TEXT_INPUT -> PermitContentType.TextInput
            Constants.PERMIT_CONTENT_TYPE_PHOTO -> PermitContentType.Photo
            else -> PermitContentType.PlainText
        }
    }

    override fun getLastContentTypeResponse(responses: List<PermitContentTypeResponses>): PermitContentTypeResponses {
        val sortedList = responses.sortedByDescending { it.utc_date_time }
        return  sortedList.first()
    }

    override fun uploadImages(file: File, fileName: String): Single<UploadImageResponse> {
        return repository.uploadImages(file, fileName)
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getLocalUserFullName(): String {
        return repository.getLocalUserFullName()
    }

}