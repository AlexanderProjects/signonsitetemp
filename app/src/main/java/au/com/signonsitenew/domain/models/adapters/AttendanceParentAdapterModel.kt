package au.com.signonsitenew.domain.models.adapters

import au.com.signonsitenew.domain.models.state.AttendanceAlertState

data class AttendanceParentAdapterModel (val companyName:String, val total:String, val childList:List<AttendanceChildAdapterModel>, var attendanceAlertState: AttendanceAlertState, var isExpanded:Boolean = false)