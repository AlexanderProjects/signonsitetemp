package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PermitTeamTabBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.updatePermitInfoRequest
import io.reactivex.Single
import javax.inject.Inject

interface PermitTeamTabUseCase{
    fun isPermitReadOnly(permitState:PermitState, permitInfo: PermitInfo):Boolean
    fun filterRequesteeList(requesteeList: List<RequesteeUser>,permitInfo: PermitInfo):List<RequesteeUser>
    fun getUserFirstAndLastName():String
    fun getUserCompany():String
    fun saveUnSelectedTeamMembers(teamMembers:List<RequesteeUser>)
    fun retrieveUnSelectedTeamMemberList():List<RequesteeUser>
    fun saveSelectedTeamMembers(teamMembers:List<RequesteeUser>)
    fun retrieveSelectedTeamMemberList():List<RequesteeUser>
    fun removeSelectedUser(requesteeUser:RequesteeUser)
    fun removeUnSelectedListOfMembers(teamMembers: List<RequesteeUser>)
    fun clearUnSelectedListOfMembers()
    fun removeSelectedListOfMembers(teamMembers:List<RequesteeUser>)
    fun clearSelectedListOfMembers()
    fun mapEnrolledUserToRequesteeUser(enrolledUserList:List<EnrolledUser>):List<RequesteeUser>
    fun mapPermitUserState(permitUserState:PermitDoneState, showTick:()->Unit, hideTick:()->Unit)
    fun retrievePermitInfoAsync(permitId: String): Single<PermitInfoResponse>
    fun updatePermitInfoAsync(permitId: String): Single<PermitInfoResponse>
    fun retrieveEnrolledUsersAsync():Single<EnrolledUsersResponse>
    fun updateListOfMembersForAPermitAsync(permitInfo: PermitInfo):Single<ApiResponse>
    fun setTeamMemberStatus(userId: String, status: String):Single<ApiResponse>
    fun setUserClickMenu(requesteeUser: RequesteeUser,permitInfo: PermitInfo):Array<String>
    fun isRequesteeListNotEmpty(requesteeList: List<RequesteeUser>, callback:()->Unit)
    fun setPermitInfo(permitInfo: PermitInfo)
    fun retrievePermitInfo():PermitInfo

}

class PermitTeamTabUseCaseImpl @Inject constructor (private val sessionManager: SessionManager, private val repository: DataRepository): PermitTeamTabUseCase, PermitTeamTabBaseUseCase<PermitInfoResponse, EnrolledUsersResponse, ApiResponse, ApiResponse>() {

    private var unSelectedTeamMembers = mutableListOf<RequesteeUser>()
    private var selectedTeamMembers = mutableListOf<RequesteeUser>()
    private lateinit var permitInfo: PermitInfo

    override fun buildGetPermitInfoAsyncCall(permitId: String): Single<PermitInfoResponse> {
        return repository.getPermitInfo(permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildGetEnrolledUsersAsyncCall(): Single<EnrolledUsersResponse> {
        return repository.getEnrolledUsers(Constants.BEARER_HEADER + sessionManager.token,true,sessionManager.siteId.toString(),sessionManager.currentUser[Constants.USER_EMAIL]!!)
    }

    override fun buildSetTeamMemberStatusAsyncCall(userId: String, status: String): Single<ApiResponse> {
        return repository.setTeamMemberStatus(permitInfo.id.toString(),userId,status,Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildUpdateMemberListForAPermitAsyncCall(permitId: String, updatePermitInfoRequest: UpdatePermitInfoRequest): Single<ApiResponse> {
        return repository.updatePermit(updatePermitInfoRequest, permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun isPermitReadOnly(permitState: PermitState, permitInfo: PermitInfo): Boolean = when {
        PermitState.Request == permitState && permitInfo.requester_user.id == sessionManager.userId.toInt() -> false
        else -> true
    }

    override fun filterRequesteeList(requesteeList: List<RequesteeUser>, permitInfo: PermitInfo) =  requesteeList.filter { requesteeUser -> requesteeUser.id != permitInfo.requester_user.id  }
    override fun getUserFirstAndLastName(): String = sessionManager.currentUser[Constants.USER_FIRST_NAME] + " " + sessionManager.currentUser[Constants.USER_LAST_NAME]
    override fun getUserCompany(): String = sessionManager.currentUser[Constants.USER_COMPANY]!!
    override fun saveUnSelectedTeamMembers(teamMembers:List<RequesteeUser>) {
        unSelectedTeamMembers.addAll(teamMembers)
        val uniqueMemberList = unSelectedTeamMembers.distinct().toMutableList()
        unSelectedTeamMembers = uniqueMemberList
    }
    override fun retrieveUnSelectedTeamMemberList():List<RequesteeUser> = unSelectedTeamMembers.distinct()
    override fun saveSelectedTeamMembers(teamMembers: List<RequesteeUser>) {
        selectedTeamMembers.addAll(teamMembers)
        val uniqueMemberList = selectedTeamMembers.distinct().toMutableList()
        selectedTeamMembers = uniqueMemberList
    }
    override fun retrieveSelectedTeamMemberList(): List<RequesteeUser> = selectedTeamMembers
    override fun removeSelectedUser(requesteeUser: RequesteeUser) { selectedTeamMembers.remove(requesteeUser) }
    override fun removeUnSelectedListOfMembers(teamMembers: List<RequesteeUser>) { unSelectedTeamMembers.removeAll(teamMembers) }
    override fun clearUnSelectedListOfMembers() { unSelectedTeamMembers.clear()}
    override fun removeSelectedListOfMembers(teamMembers: List<RequesteeUser>) { unSelectedTeamMembers.removeAll(teamMembers) }
    override fun clearSelectedListOfMembers() = selectedTeamMembers.clear()
    override fun mapEnrolledUserToRequesteeUser(enrolledUserList: List<EnrolledUser>): List<RequesteeUser> {
        val requesteeUserList = mutableListOf<RequesteeUser>()
        val filterList = enrolledUserList.filter { it.user_id != sessionManager.userId.toInt() }
        for (enrolledUser in filterList){
            val requesteeUser = RequesteeUser(
                    enrolledUser.user_id,
                    enrolledUser.first_name,
                    enrolledUser.last_name,
                    enrolledUser.phone_number,
                    enrolledUser.company_name,
                    PermitDoneState.PreRequest)
            requesteeUserList.add(requesteeUser)
        }
        return requesteeUserList
    }

    override fun mapPermitUserState(permitUserState: PermitDoneState, showTick: () -> Unit, hideTick: () -> Unit) {
        when  {
            PermitDoneState.FinishedRequest == permitUserState && permitInfo.state == PermitState.InProgress  -> hideTick()
            PermitDoneState.FinishedRequest == permitUserState -> showTick()
            PermitDoneState.FinishedWork == permitUserState -> showTick()
            else -> hideTick()
        }
    }
    override fun retrievePermitInfoAsync(permitId: String): Single<PermitInfoResponse> = getPermitInfoAsync(permitId)
    override fun updatePermitInfoAsync(permitId: String): Single<PermitInfoResponse> = getPermitInfoAsync(permitId)
    override fun retrieveEnrolledUsersAsync(): Single<EnrolledUsersResponse> = getEnrolledUsersAsync()
    override fun updateListOfMembersForAPermitAsync(permitInfo: PermitInfo): Single<ApiResponse> {
        val request = updatePermitInfoRequest {
            state = PermitState.Request.toString()
            requestee_users = selectedTeamMembers.map { it.id }
            start_date = null
            end_date = null
        }
        return updateMemberListForAPermitAsync(permitInfo.id.toString(), request)
    }
    override fun setTeamMemberStatus(userId: String, status: String): Single<ApiResponse> = setTeamMemberStatusForAPermitAsync(userId,status)
    override fun setUserClickMenu(requesteeUser: RequesteeUser, permitInfo: PermitInfo): Array<String> {
        val menuList = mutableListOf("Call")
        if(permitInfo.requester_user.id  == sessionManager.userId.toInt() && requesteeUser.status == PermitDoneState.FinishedRequest && permitInfo.state == PermitState.Request) {
            menuList.add(1, "Redo")
            menuList.add(2, "Remove")
            menuList.add(3, "Dismiss")
        }else if(permitInfo.requester_user.id == sessionManager.userId.toInt() && requesteeUser.status == PermitDoneState.PreRequest && permitInfo.state == PermitState.Request){
            menuList.add(1, "Remove")
            menuList.add(2, "Dismiss")
        }
        else
            menuList.add(1, "Dismiss")
        return menuList.toTypedArray()
    }

    override fun isRequesteeListNotEmpty(requesteeList: List<RequesteeUser>, callback: () -> Unit) {
        if(requesteeList.isNotEmpty())
            callback()
    }
    override fun setPermitInfo(permitInfo: PermitInfo) { this.permitInfo = permitInfo }
    override fun retrievePermitInfo(): PermitInfo = this.permitInfo

}