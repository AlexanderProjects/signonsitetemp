package au.com.signonsitenew.domain.models.analytics


data class Event(val name:String, val targetted_user_id:Int?,val is_new_form:Boolean?)