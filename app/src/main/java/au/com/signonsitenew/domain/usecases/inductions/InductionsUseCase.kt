package au.com.signonsitenew.domain.usecases.inductions

import au.com.signonsitenew.domain.models.Induction
import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.document
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface InductionsUseCase {
    fun getSiteInductions():Single<InductionResponse>
    fun hasFormAvailable(inductionResponse: InductionResponse, hasForm:()->Unit, hasNotForm:()->Unit)
    fun buildEmptyInductionDocument():Document
    fun buildInductionDocument(induction: Induction,notifierCallback:()->Unit):Document
}

class InductionUseCaseImpl @Inject constructor(private val repository: DataRepository,private val sessionManager: SessionManager):InductionsUseCase{

    override fun getSiteInductions() : Single<InductionResponse> =
            repository.getActiveInductions(Constants.BEARER_HEADER + sessionManager.token,sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY,sessionManager.siteId.toString())
                    .observeOn(AndroidSchedulers.mainThread())

    override inline fun hasFormAvailable(inductionResponse: InductionResponse, hasForm:()->Unit, hasNotForm:()->Unit) {
        return if(inductionResponse.form_available != null && inductionResponse.form_available)
            hasForm()
        else
            hasNotForm()
    }

    override fun buildEmptyInductionDocument(): Document = document { docType = Constants.DOC_EMPTY_INDUCTION }

    override inline fun buildInductionDocument(induction: Induction, notifierCallback:()->Unit): Document {
        val document = Document()
        val type = Constants.DOC_INDUCTION
        val subtype = "Site"
        var state = ""
        document.subtype = subtype
        when (induction.state?.as_string) {
            Constants.DOC_INDUCTION_INCOMPLETE -> {
                state = Constants.DOC_INDUCTION_INCOMPLETE
                notifierCallback()
            }
            Constants.DOC_INDUCTION_PENDING -> {
                state = Constants.DOC_INDUCTION_PENDING
            }
            Constants.DOC_INDUCTION_COMPLETE -> {
                state = Constants.DOC_INDUCTION_COMPLETE
            }
            Constants.DOC_INDUCTION_REJECTED -> {
                state = Constants.DOC_INDUCTION_REJECTED
                notifierCallback()
            }
        }
        document.state = state
        document.docType = type
        return document
    }

}