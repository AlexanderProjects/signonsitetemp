package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.domain.models.UpdatePermitInfoRequest
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class PermitBaseUseCase <T,Z,Q> protected constructor() {

    abstract fun buildGetCurrentPermitsAsyncCall():Single<T>

    abstract fun buildUpdatePermitAsyncCall(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<Q>

    abstract fun buildGetPermitInfoAsyncCall(permitId:String): Single<Z>

    fun getCurrentPermitsAsync():Single<T> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildGetCurrentPermitsAsyncCall()
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getPermitInfoAsync(permitId: String): Single<Z> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildGetPermitInfoAsyncCall(permitId)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun updatePermitAsync(updatePermitRequest: UpdatePermitInfoRequest, permitId: String):Single<Q>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildUpdatePermitAsyncCall(updatePermitRequest, permitId)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

}