package au.com.signonsitenew.domain.models

data class Visits (val visit_id:Int,
                   val user_id:Int,
                   val site_id:Int,
                   val check_in_time:String?,
                   val check_out_time:String?,
                   val is_briefing_acknowledged:Boolean)