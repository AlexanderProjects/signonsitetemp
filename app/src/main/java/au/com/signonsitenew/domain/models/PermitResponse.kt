package au.com.signonsitenew.domain.models

data class PermitResponse (override val status: String, val permits:List<Permit>): ApiResponse(status)