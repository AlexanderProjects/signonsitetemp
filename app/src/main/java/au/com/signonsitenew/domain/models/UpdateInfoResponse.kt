package au.com.signonsitenew.domain.models

import au.com.signonsitenew.domain.models.ApiResponse

data class UpdateInfoResponse(override var status: String) : ApiResponse(status)

