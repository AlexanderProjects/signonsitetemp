package au.com.signonsitenew.domain.usecases.workernotes

import au.com.signonsitenew.domain.models.attendees.CreatedByUser
import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import javax.inject.Inject

interface WorkerNotesUseCase{
    fun mapWorkerNotesListToListOfAny(workerNotesList: List<WorkerNotes>):List<Any>
    fun formatDateTimeForWorkerList(workerNotesList: List<WorkerNotes>):List<WorkerNotes>
}

class WorkerNotesUseCaseImpl @Inject constructor() :WorkerNotesUseCase {

    override fun mapWorkerNotesListToListOfAny(workerNotesList: List<WorkerNotes>): List<Any> {
        val sortedList = workerNotesList.sortedWith(compareBy ({ it.importance == "info"},{it.importance == "warning"},{it.importance == "alert"}))
        val groupedList = sortedList.groupBy { it.importance }
        val listOfAny = mutableListOf<Any>()
        groupedList.forEach {( importance, list) ->
            listOfAny.add(importance)
            list.forEach { workerNotes ->
                listOfAny.add(workerNotes)
            }
        }
        return listOfAny
    }

    override fun formatDateTimeForWorkerList(workerNotesList: List<WorkerNotes>):List<WorkerNotes>{
        val formattedList = mutableListOf<WorkerNotes>()
        workerNotesList.forEach {
            formattedList.add(WorkerNotes(id = it.id,
                    importance = it.importance,
                    created_by_user = CreatedByUser(it.created_by_user.id,it.created_by_user.first_name,it.created_by_user.last_name),
                    created_at = changeDateTimeFormat(it.created_at),content = it.content))
        }
        return formattedList
    }


    private fun changeDateTimeFormat(dateTime:String):String{
        val parser = ISODateTimeFormat.dateTimeParser()
        val dateTimeModified = parser.parseDateTime(dateTime)
        val formatter = DateTimeFormat.forPattern("dd MMM yyyy")
        return dateTimeModified.toString(formatter)
    }
}