package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SitePermitTemplate(val enabled:Boolean, val permit_template:PermitTemplate):Parcelable