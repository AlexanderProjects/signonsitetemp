package au.com.signonsitenew.domain.usecases.attendance

import au.com.signonsitenew.domain.models.TodaysVisitsResponse
import au.com.signonsitenew.domain.models.Visitors
import au.com.signonsitenew.domain.models.Visits
import au.com.signonsitenew.domain.models.adapters.AttendanceAdapterChildModel
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceRegisterParentAdapterModel
import au.com.signonsitenew.domain.models.attendees.Attendance
import au.com.signonsitenew.domain.models.attendees.Attendee
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.attendees.WorkerNotes
import au.com.signonsitenew.domain.models.state.AttendanceAlertState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.base.AttendanceRegisterBaseUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.empty
import au.com.signonsitenew.utilities.merge
import io.reactivex.Single
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormatterBuilder
import org.joda.time.format.ISODateTimeFormat
import javax.inject.Inject

interface AttendanceRegisterUseCase{
    fun mapAttendeeToAttendanceAdapterParentModel(attendeesResponse: AttendeesResponse):MutableList<AttendanceParentAdapterModel>
    fun mapTodaysAttendanceToNewAttendanceAdapterParentModel(todaysVisitsResponse: TodaysVisitsResponse): MutableList<AttendanceRegisterParentAdapterModel>
    fun getRickyWorker(attendeesResponse: AttendeesResponse, filterUserId: Int):AttendanceChildAdapterModel?
    fun attendanceRegisterWorkerViewedAnalytics(targetted_user_id:Int)
}

class AttendanceRegisterUseCaseImpl  @Inject constructor(private val repository: DataRepository,
                                                         private val sessionManager: SessionManager,
                                                         private val analyticsEventDelegateService: AnalyticsEventDelegateService): AttendanceRegisterUseCase,AttendanceRegisterBaseUseCase<TodaysVisitsResponse,AttendeesResponse,AttendeesResponse>() {


    override fun buildGetTodayVisitsAsyncCall(): Single<TodaysVisitsResponse> = repository.getTodaysVisits(sessionManager.siteId.toString(), sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, Constants.BEARER_HEADER + sessionManager.token)


    override fun buildGetAttendeesAsyncCall(): Single<AttendeesResponse> {
        val dateTimeToday = DateTime.now(DateTimeZone.forID(repository.getSiteTimeZone(sessionManager.siteId)))
        val utcDateTimeToday = dateTimeToday.toDateTime(DateTimeZone.UTC).withTimeAtStartOfDay()
        val utcDateTimeTomorrow = utcDateTimeToday.plusDays(1)
        val siteId = sessionManager.siteId.toString()
        return repository.attendees(siteId,Constants.BEARER_HEADER + sessionManager.token,utcDateTimeToday.toString(),utcDateTimeTomorrow.toString(),true)
    }


    override fun buildGetAttendeeWorkerAsyncCall(filterUserId: Int): Single<AttendeesResponse> {
        val dateTimeToday = DateTime.now(DateTimeZone.forID(repository.getSiteTimeZone(sessionManager.siteId)))
        val utcDateTimeToday = dateTimeToday.toDateTime(DateTimeZone.UTC).withTimeAtStartOfDay().toLocalDateTime()
        val utcDateTimeTomorrow = utcDateTimeToday.plusDays(1)
        val siteId = sessionManager.siteId.toString()
        return repository.attendees(siteId,Constants.BEARER_HEADER + sessionManager.token,filterUserId,utcDateTimeToday.toString(),utcDateTimeTomorrow.toString(),true)
    }



    override fun mapTodaysAttendanceToNewAttendanceAdapterParentModel(todaysVisitsResponse: TodaysVisitsResponse): MutableList<AttendanceRegisterParentAdapterModel> {
        val parentList = mutableListOf<AttendanceRegisterParentAdapterModel>()
        val parentFilterList = todaysVisitsResponse.visitors.groupBy{it.company_name}
        parentFilterList.forEach { it1 ->
            val childList = mutableListOf<AttendanceAdapterChildModel>()
            it1.value.forEach {
                val newAttendanceAdapterChildModel = AttendanceAdapterChildModel(
                        it.first_name+" "+it.last_name,
                        it.user_id,
                        getLastTimeIn(todaysVisitsResponse.visits,it.user_id),
                        getLastTimeOut(todaysVisitsResponse.visits,it.user_id),
                        isBriefingAcknowledge(todaysVisitsResponse.visits,it.user_id),
                        it.induction?.state?.as_string,
                        todaysVisitsResponse.has_active_site_induction_form,
                        todaysVisitsResponse.has_active_briefing,
                        it.induction?.type
                )
                childList.add(newAttendanceAdapterChildModel)
            }
            val newAttendanceAdapterParentModel = AttendanceRegisterParentAdapterModel(it1.key, getNumberOfUsersInTheSite(todaysVisitsResponse.visitors, it1.key).toString(),childList)
            parentList.add(newAttendanceAdapterParentModel)
        }
        return  parentList.sortedBy { it.companyName }.toMutableList()
    }


    override fun getRickyWorker(attendeesResponse: AttendeesResponse, filterUserId: Int): AttendanceChildAdapterModel? {
        val attendee = attendeesResponse.attendees.find { it.id == filterUserId }
        return attendee?.let {
            AttendanceChildAdapterModel(attendee.first_name + " " + attendee.last_name,
                    it.id,
                    attendee.phone_number,
                    getLastSignInTime(attendee.attendances),
                    getLastSignOffTime(attendee.attendances),
                    attendee.site_induction?.state,
                    attendee.needs_to_do_site_induction,
                    attendee.site_induction?.id,
                    getBriefingStatus(attendee.attendances),
                    attendee.site_induction?.type,
                    getCompanyName(attendee.attendances),
                    getAlertState(attendee.worker_notes, attendee),
                    attendee.attendances,
                    sortByAlert(attendee.worker_notes))
        }
    }

    override fun attendanceRegisterWorkerViewedAnalytics(targetted_user_id: Int) {
        analyticsEventDelegateService.attendanceRegisterWorkerViewed(targetted_user_id)
    }


    override fun mapAttendeeToAttendanceAdapterParentModel(attendeesResponse: AttendeesResponse): MutableList<AttendanceParentAdapterModel> {
        val childList = mutableListOf<AttendanceChildAdapterModel>()
        val parentList = mutableListOf<AttendanceParentAdapterModel>()
        attendeesResponse.attendees.forEach {attendee ->
            childList.add(AttendanceChildAdapterModel(attendee.first_name+" "+attendee.last_name,
                    attendee.id,
                    attendee.phone_number,
                    getLastSignInTime(attendee.attendances),
                    getLastSignOffTime(attendee.attendances),
                    attendee.site_induction?.state,
                    attendee.needs_to_do_site_induction,
                    attendee.site_induction?.id,
                    getBriefingStatus(attendee.attendances),
                    attendee.site_induction?.type,
                    getCompanyName(attendee.attendances),
                    getAlertState(attendee.worker_notes,attendee),
                    attendee.attendances,
                    sortByAlert(attendee.worker_notes)))
        }
        childList.groupBy { it.companyName }.forEach{(companyName,listOfAttendanceChildAdapterModel) ->
            val timeOutNullList = listOfAttendanceChildAdapterModel.filter { it.timeOut == "null" }.sortedBy { it.attendanceAlertState.order }.sortedByDescending { it.timeIn }.sortedBy { it.attendanceAlertState.order }
            val timeOutNotNullList = listOfAttendanceChildAdapterModel.filter { it.timeOut != "null"  }.sortedBy { it.attendanceAlertState.order }.sortedByDescending { it.timeIn }.sortedBy { it.attendanceAlertState.order }
            val resultList = merge(timeOutNullList,timeOutNotNullList)
            parentList.add(AttendanceParentAdapterModel(companyName,listOfAttendanceChildAdapterModel.size.toString(),getTimeFromDate(resultList),getAlertState(listOfAttendanceChildAdapterModel)))
        }
        return parentList.sortedBy { it.companyName }.sortedBy { it.attendanceAlertState.order}.toMutableList()
    }

    private fun getAlertState(listOfChild:List<AttendanceChildAdapterModel>): AttendanceAlertState {
        val alertList = mutableListOf<AttendanceAlertState>()
        listOfChild.forEach {attendanceChildAdapterModel ->
            if(attendanceChildAdapterModel.workerNotes?.isNotEmpty() == true){
                attendanceChildAdapterModel.workerNotes.sortedBy { it.importance }.forEach { workerNotes ->
                    when (workerNotes.importance) {
                        "alert" -> alertList.add(AttendanceAlertState.Alert())
                        "warning" -> alertList.add(AttendanceAlertState.Warning())
                        "info" -> alertList.add(AttendanceAlertState.Information())
                    }
                }
            }
            if(attendanceChildAdapterModel.inductionState == "pending" || attendanceChildAdapterModel.inductionState == "rejected") alertList.add(AttendanceAlertState.Warning())
            if(attendanceChildAdapterModel.needsToDoInduction) alertList.add(AttendanceAlertState.Warning())
            if(attendanceChildAdapterModel.briefingStatus == "unacknowledged") alertList.add(AttendanceAlertState.Warning())
        }
        return if(alertList.isEmpty()) AttendanceAlertState.None() else alertList.sortedBy { it.order }.first()
    }

    private fun getAlertState(workerNotes: List<WorkerNotes>?, attendee: Attendee): AttendanceAlertState{
        if(!workerNotes.isNullOrEmpty()) {
            val sortedList = workerNotes.sortedWith(compareBy({ it.importance == "info" }, { it.importance == "warning" }, { it.importance == "alert" }))
            sortedList.forEach {
                when (it.importance) {
                    "alert" -> return AttendanceAlertState.Alert()
                    "warning" -> return AttendanceAlertState.Warning()
                    "info" -> return AttendanceAlertState.Information()
                }
            }
        }
        if(attendee.needs_to_do_site_induction) return AttendanceAlertState.Warning()
        if(attendee.site_induction?.state == "pending" || attendee.site_induction?.state== "rejected") return AttendanceAlertState.Warning()
        if(getBriefingStatus(attendee.attendances) == "unacknowledged") return AttendanceAlertState.Warning()
        return AttendanceAlertState.None()
    }



    private fun getLastTimeIn(listOfVisits:List<Visits>,userId:Int):String =
            if (listOfVisits.isNotEmpty()) {
                val dateTimesStrToDate: (String?) -> DateTime = {
                    val formatter = DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd HH:mm:ss").toFormatter()
                    DateTime.parse(it,formatter)
                }
                val list = listOfVisits.filter { it.user_id == userId }
                val timeList = list.map { it.check_in_time }
                val sortedList = timeList.map(dateTimesStrToDate).sorted()
                val formatter = DateTimeFormatterBuilder().appendPattern("hh:mm a").toFormatter()
                sortedList.last().toString(formatter)
            } else {
                String().empty()
            }

    private fun getLastTimeOut(listOfVisits:List<Visits>,userId:Int):String? =
            if (listOfVisits.isNotEmpty()) {
                val list = listOfVisits.filter { it.user_id == userId }
                val sortedList = list.sortedBy { it.visit_id }
                sortedList.last().check_out_time
            } else {
                null
            }


    private fun getLastSignInTime(attendances:List<Attendance>):String =
            if (attendances.isNotEmpty()) {
                val dateTimesStrToDate: (String?) -> DateTime = {
                    val parser = ISODateTimeFormat.dateTimeParser()
                    val dateTime = parser.parseDateTime(it)
                    dateTime.withZone(DateTimeZone.forID(repository.getSiteTimeZone(sessionManager.siteId)))
                }
                val timeList = attendances.map { it.signon_at }.sorted()
                val sortedList = timeList.map(dateTimesStrToDate)
                val formatter = ISODateTimeFormat.dateTimeNoMillis()
                sortedList.last().toString(formatter)
            } else {
                String().empty()
            }

    private fun getBriefingStatus(attendances:List<Attendance>):String =
            if (attendances.isNotEmpty()) {
                val list = attendances.sortedByDescending { it.signon_at }
                list.last().briefing_status
            } else {
                String().empty()
            }

    private fun getCompanyName(attendances:List<Attendance>):String =
            if (attendances.isNotEmpty()) {
                val list = attendances.sortedByDescending { it.signon_at }
                list.last().company.name
            } else {
                String().empty()
            }

    private fun getLastSignOffTime(attendances:List<Attendance>):String? =
            if (attendances.isNotEmpty()) {
                val sortedList = attendances.sortedBy { it.signon_at }
                val temp = sortedList.last().signoff_at.toString()
                temp
            } else {
                null
            }


    private fun isBriefingAcknowledge(listOfVisits:List<Visits>,userId:Int):Boolean{
        return if(listOfVisits.isNotEmpty()) {
            val list = listOfVisits.filter { it.user_id == userId }
            val sortedList = list.sortedBy { it.visit_id }
            sortedList.last().is_briefing_acknowledged
        }else{
            false
        }
    }

    private fun getNumberOfUsersInTheSite(listOfVisitors:List<Visitors>,company_name:String):Int{
        return if(listOfVisitors.isNotEmpty()){
            val list = listOfVisitors.filter { it.company_name == company_name }
            list.size
        }else{
            0
        }
    }

    private fun sortByAlert(workerNotes:List<WorkerNotes>?):List<WorkerNotes> =
            if(!workerNotes.isNullOrEmpty()){
                workerNotes.sortedWith(compareBy ({ it.importance == "info"},{it.importance == "warning"},{it.importance == "alert"}))
            }else{
                mutableListOf()
            }

    private fun getTimeFromDate(workerNotes:List<AttendanceChildAdapterModel>):List<AttendanceChildAdapterModel> {
        return if (workerNotes.isNotEmpty()) {
            workerNotes.forEach {
                val parser = ISODateTimeFormat.dateTimeParser()
                val dateTime = parser.parseDateTime(it.timeIn)
                val formatter = DateTimeFormatterBuilder().appendPattern("hh:mm a").toFormatter()
                it.timeIn = dateTime.toString(formatter)
            }
            workerNotes
        } else {
            mutableListOf()
        }
    }

}