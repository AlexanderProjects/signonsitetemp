package au.com.signonsitenew.domain.models

import com.squareup.moshi.Json

enum class RequesteeStates {
    @Json(name = "all_uninvited")
    all_uninvited,
    @Json(name = "all_invited")
    all_invited
}