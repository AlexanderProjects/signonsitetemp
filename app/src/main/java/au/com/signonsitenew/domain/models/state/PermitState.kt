package au.com.signonsitenew.domain.models.state

import com.squareup.moshi.Json

enum class PermitState {
    @Json(name = "request")
    Request,
    @Json(name = "pending_approval")
    PendingApproval,
    @Json(name = "in_progress")
    InProgress,
    @Json(name = "pending_closure")
    PendingClosure,
    @Json(name = "closed")
    Closed,
    @Json(name = "requester_cancelled")
    RequesterCancelled,
    @Json(name = "approver_cancelled")
    ApproverCancelled,
    @Json(name = "suspended")
    Suspended,
}