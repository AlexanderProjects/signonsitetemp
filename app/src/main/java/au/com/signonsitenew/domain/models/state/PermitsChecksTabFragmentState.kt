package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.ContentTypeItem

sealed class PermitsChecksTabFragmentState {
    data class ShowRequestState(val components:List<ContentTypeItem>, val state:PermitChecksState):PermitsChecksTabFragmentState()
    data class ShowInProgressState(val components:List<ContentTypeItem>, val state:PermitChecksState):PermitsChecksTabFragmentState()
    data class ShowClosingState(val components:List<ContentTypeItem>, val state:PermitChecksState):PermitsChecksTabFragmentState()
    object ShowTextForEmptyState:PermitsChecksTabFragmentState()
}
