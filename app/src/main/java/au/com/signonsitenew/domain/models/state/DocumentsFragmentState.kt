package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse

sealed class DocumentsFragmentState {
    data class Success(val workerResponse: BriefingWorkerResponse, val inductionResponse: InductionResponse) : DocumentsFragmentState()
    data class NoData(val workerResponse: BriefingWorkerResponse, val inductionResponse: InductionResponse): DocumentsFragmentState()
    data class UpdateNotificationBadge(val counter:Int, val notifier:String):DocumentsFragmentState()
    object Error: DocumentsFragmentState()
}