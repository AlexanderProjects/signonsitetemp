package au.com.signonsitenew.domain.models.state

sealed class PermitChecksState {
    object RequestChecks:PermitChecksState()
    object InProgressChecks:PermitChecksState()
    object ClosingChecks:PermitChecksState()
}