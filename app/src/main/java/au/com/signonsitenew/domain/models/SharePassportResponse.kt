package au.com.signonsitenew.domain.models

data class SharePassportResponse (override val status:String): ApiResponse(status)