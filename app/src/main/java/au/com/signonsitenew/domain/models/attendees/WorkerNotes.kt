package au.com.signonsitenew.domain.models.attendees

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WorkerNotes(val id:Int,val importance:String,val created_by_user:CreatedByUser,val created_at:String,val content:String) : Parcelable