package au.com.signonsitenew.domain.models

data class SitePermitTemplateResponse(override val status: String, val site_permit_templates:List<SitePermitTemplate>): ApiResponse(status)