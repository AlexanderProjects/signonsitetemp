package au.com.signonsitenew.domain.models

data class RegistrationResponse (var registered:Boolean, var auth_token:String)