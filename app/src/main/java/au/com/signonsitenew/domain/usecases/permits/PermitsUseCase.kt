package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitDetailsFragmentState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PermitBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.utilities.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import org.joda.time.format.ISODateTimeFormat
import javax.inject.Inject

interface PermitsUseCase{
    fun buildPermitDocument(): Document
    fun sortPermitListByIdDescending(permits:List<Permit>):List<Permit>
    fun mapPermitStateTitle(state: PermitState):String
    fun buildSmallPermitInfoDescription(permitInfo: PermitInfo): String
    fun retrieveCurrentPermitsAsync():Single<PermitResponse>
    fun retrievePermitInfoAsync(): Single<PermitInfoResponse>
    fun updateCurrentPermitAsync(updatePermitRequest: UpdatePermitInfoRequest): Single<ApiResponse>
    fun getPermitInfo():PermitInfo?
    fun setPermitInfo(permitInfo: PermitInfo)
    fun setPermit(permit: Permit)
    fun retrievePermit():Permit
    fun mapPermitInfoToToolbarState(permitInfo: PermitInfo): PermitDetailsFragmentState
    fun mapPermitInfoToPermitDetailsState():PermitDetailsFragmentState
    fun saveUserResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest):Single<ApiResponse>
    fun setUserPermitResponses(responses:ComponentTypeForm)
    fun getUserPermitResponses():List<ComponentTypeForm>
    fun clearPermitResponses()
    fun buildPermitInfoRequest():UpdatePermitInfoRequest
    fun hasUserUnsavedResponses():Boolean
    fun setListOfMembers(listOfTeamMembers:List<RequesteeUser>?)
    fun getListOfMembers():List<RequesteeUser>
}

class PermitsUseCaseImpl @Inject constructor (private val sessionManager: SessionManager, private val repository: DataRepository): PermitsUseCase, PermitBaseUseCase<PermitResponse, PermitInfoResponse, ApiResponse>() {

    private lateinit var permitInfo: PermitInfo
    private lateinit var permit: Permit
    private var hasUnSavedResponses = mutableListOf<ComponentTypeForm>()
    private var listOfMembers = mutableListOf<RequesteeUser>()

    private val isManager:Boolean get() = repository.hasManagerPermission(sessionManager.userId.toLong(), sessionManager.siteId.toLong())

    override fun buildPermitDocument(): Document {
        val document = Document()
        document.state = String().empty()
        document.docType = Constants.DOC_PERMITS
        return document
    }

    override fun sortPermitListByIdDescending(permits: List<Permit>): List<Permit> {
        return permits.sortedByDescending { it.id }
    }

    override fun mapPermitStateTitle(state: PermitState):String = when (state){
        PermitState.Request -> Constants.PERMIT_REQUEST_TITLE
        PermitState.PendingApproval  -> Constants.PERMIT_PENDING_APPROVAL_TITLE
        PermitState.InProgress -> Constants.PERMIT_APPROVED_TITLE
        PermitState.PendingClosure -> Constants.PERMIT_CLOSING_TITLE
            else -> String().empty()
        }

    override fun buildSmallPermitInfoDescription(permitInfo: PermitInfo): String  = when(permitInfo.state){
        PermitState.Request -> permitInfo.permit_template.name  + " " + permitInfo.human_id + Constants.PERMIT_STATE_REQUEST
        PermitState.PendingApproval -> permitInfo.permit_template.name  + Constants.PERMIT_CONNECTOR  + permitInfo.human_id + Constants.PERMIT_STATE_PENDING_APPROVAL
        PermitState.InProgress ->permitInfo.permit_template.name  + Constants.PERMIT_CONNECTOR  +permitInfo.human_id + Constants.PERMIT_STATE_APPROVED
        PermitState.PendingClosure -> permitInfo.permit_template.name + Constants.PERMIT_CONNECTOR  + permitInfo.human_id + Constants.PERMIT_STATE_CLOSING
        else -> String().empty()
    }

    override fun retrieveCurrentPermitsAsync(): Single<PermitResponse> = getCurrentPermitsAsync()
    override fun retrievePermitInfoAsync(): Single<PermitInfoResponse> = getPermitInfoAsync(permit.id.toString())
    override fun updateCurrentPermitAsync(updatePermitRequest: UpdatePermitInfoRequest): Single<ApiResponse> = updatePermitAsync(updatePermitRequest,permitInfo.id.toString())

    override fun getPermitInfo(): PermitInfo? = if(::permitInfo.isInitialized)
        permitInfo
    else
        null

    override fun setPermitInfo(permitInfo: PermitInfo) {
        this.permitInfo = permitInfo
    }

    override fun setPermit(permit: Permit) {
        this.permit = permit
    }

    override fun retrievePermit(): Permit {
        return this.permit
    }

    override fun mapPermitInfoToToolbarState(permitInfo: PermitInfo): PermitDetailsFragmentState {
        val isRequester =  permitInfo.requester_user.id == sessionManager.userId.toInt()
        return when{
            permitInfo.state == PermitState.Request && isRequester -> {
                PermitDetailsFragmentState.ShowPermitToolbar
            }
            permitInfo.state == PermitState.PendingApproval && isManager ->{
                PermitDetailsFragmentState.ShowPermitToolbar
            }
            permitInfo.state == PermitState.InProgress && isPermitStartDateBeforeNow(permitInfo) && isManager ->{
                PermitDetailsFragmentState.ShowPermitToolbar
            }
            else -> PermitDetailsFragmentState.HidePermitToolbar
        }
    }

    override fun buildGetCurrentPermitsAsyncCall(): Single<PermitResponse> {
        return repository.getCurrentPermits(sessionManager.siteId.toString(), Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildGetPermitInfoAsyncCall(permitId: String): Single<PermitInfoResponse> {
        return repository.getPermitInfo(permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    private fun isPermitStartDateBeforeNow(permitInfo: PermitInfo): Boolean {
        return if(permitInfo.start_date != null) {
            ISODateTimeFormat.dateTimeParser().parseDateTime(permitInfo.start_date).isBeforeNow
        }else {
            false
        }

    }

    override fun mapPermitInfoToPermitDetailsState(): PermitDetailsFragmentState {
        return when (this.permitInfo.state) {
            PermitState.Request -> PermitDetailsFragmentState.ShowCancelAlertDialogInRequest
            else -> PermitDetailsFragmentState.ShowCancelAlertDialogInProgress
        }
    }

    override fun saveUserResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest): Single<ApiResponse>{
        return repository.saveResponses(componentSaveResponsesRequest, permitInfo.id.toString(),Constants.BEARER_HEADER + sessionManager.token)
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun setUserPermitResponses(responses: ComponentTypeForm) {
        this.hasUnSavedResponses.add(responses)
    }

    override fun getUserPermitResponses(): List<ComponentTypeForm> {
        return this.hasUnSavedResponses
    }

    override fun clearPermitResponses() {
        this.hasUnSavedResponses.clear()
    }

    override fun buildPermitInfoRequest(): UpdatePermitInfoRequest {
        return updatePermitInfoRequest {
            state = null
            requestee_users = if(!listOfMembers.isNullOrEmpty()) listOfMembers.map { it.id } else null
            start_date = if(permitInfo.start_date != null) permitInfo.start_date else null
            end_date = if(permitInfo.end_date != null) permitInfo.end_date else null
        }
    }

    override fun hasUserUnsavedResponses(): Boolean {
        return when{
            hasUnSavedResponses.containElements(permitInfo.tasks) -> {
                hasUnSavedResponses.clear()
                false
            }
            hasUnSavedResponses.containElements(permitInfo.request_checks) -> {
                hasUnSavedResponses.clear()
                false
            }
            hasUnSavedResponses.containElements(permitInfo.in_progress_checks) -> {
                hasUnSavedResponses.clear()
                false
            }
            hasUnSavedResponses.containElements(permitInfo.pending_closure_checks) -> {
                hasUnSavedResponses.clear()
                false
            }
            hasUnSavedResponses.isNotEmpty() -> true
            else -> false
        }
    }

    override fun setListOfMembers(listOfTeamMembers: List<RequesteeUser>?) {
        listOfTeamMembers?.let {
            if(it.isNotEmpty())
                listOfMembers = it.toMutableList()
        }
    }

    override fun getListOfMembers(): List<RequesteeUser> {
        return listOfMembers
    }

    override fun buildUpdatePermitAsyncCall(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<ApiResponse> {
        return repository.updatePermit(updatePermitRequest, permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

}