package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceRegisterParentAdapterModel

sealed class NewAttendanceRegisterFragmentState {
    data class Success(val listAttendance: ArrayList<AttendanceRegisterParentAdapterModel>) : NewAttendanceRegisterFragmentState()
    data class ShowWorkerDetails(val childAdapterModel: AttendanceChildAdapterModel):NewAttendanceRegisterFragmentState()
    data class SuccessResponse(val listAttendance: ArrayList<AttendanceParentAdapterModel>) : NewAttendanceRegisterFragmentState()
    data class DataError(val errorMessage:String): NewAttendanceRegisterFragmentState()
    object Error: NewAttendanceRegisterFragmentState()
    object ShowProgressView: NewAttendanceRegisterFragmentState()
    object HideProgressView:NewAttendanceRegisterFragmentState()
}