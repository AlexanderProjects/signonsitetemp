package au.com.signonsitenew.domain.models

data class CredentialCreateUpdateResponse (override val status: String, val credential_id: Int): ApiResponse(status)