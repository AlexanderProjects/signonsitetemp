package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Credential(var id: Int?,
                      var credential_type: CredentialType?,
                      var identifier: String?,
                      var name: String?,
                      var front_photo: String?,
                      var front_photo_temporary_url: String?,
                      var back_photo: String?,
                      var back_photo_temporary_url: String?,
                      var issue_date: String?,
                      var expiry_date: String?,
                      var issued_by: String?,
                      var rto: String?,
                      var registration_number: String?,
                      var reference: String? ) : Parcelable{
    constructor() : this(null,null,null,null,null,null,null,null,null,null,null,null,null,null)
}

