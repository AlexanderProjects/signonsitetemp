package au.com.signonsitenew.domain.usecases.registration

import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface PhoneValidationUseCase {
    fun validatePhoneNumber(alpha2:String,raw:String): Single<ApiResponse>
    fun validatePhoneFormat(number:String):Boolean
    fun validatePhoneLength(number:String):Boolean
    fun getErrorMessage(apiResponse: ApiResponse): String
    fun isValidResponse(apiResponse: ApiResponse): Boolean
}

class PhoneValidationUseCaseImpl @Inject constructor(private val dataRepository: DataRepository, private val sessionManager: SessionManager) :PhoneValidationUseCase {

    override fun validatePhoneNumber(alpha2:String, raw:String): Single<ApiResponse> = dataRepository.validatePhoneNumber(alpha2,raw,sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY)
            .observeOn(AndroidSchedulers.mainThread())
    override fun validatePhoneFormat(number:String):Boolean = number.matches("^[+]?[0-9]{4,15}$".toRegex())
    override fun validatePhoneLength(number:String):Boolean = number.length in 4..15

    override fun getErrorMessage(apiResponse: ApiResponse): String = when(apiResponse.status){
        Constants.RESPONSE_UNAUTHENTIC -> Constants.RESPONSE_UNAUTHENTIC_MESSAGE
        Constants.RESPONSE_UNAUTHORISED -> Constants.RESPONSE_UNAUTHORISED_MESSAGE
        else -> Constants.UNKNOWN_NETWORK_ERROR_MESSAGE
    }

    override fun isValidResponse(apiResponse: ApiResponse): Boolean = when(apiResponse.status){
        Constants.RESPONSE_SUCCESS -> true
        else -> false
    }

}