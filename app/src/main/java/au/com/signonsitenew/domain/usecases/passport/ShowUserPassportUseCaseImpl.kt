package au.com.signonsitenew.domain.usecases.passport

import android.graphics.Bitmap
import android.os.Environment
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PassportBaseUseCase
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*
import javax.inject.Inject

interface ShowUserPassportUseCase {
    fun saveCompanyName(companyName: String)
}

class ShowUserPassportUseCaseImpl @Inject constructor(private val repository: DataRepository,
                                                      private val sessionManager: SessionManager,
                                                      private val userService: UserService):PassportBaseUseCase<UserInfoResponse,Any>(),ShowUserPassportUseCase {

    private val userId = userService.currentUserId.toString()
    private val token = Constants.BEARER_HEADER + sessionManager.token

    private fun convertBitmapToFile(bitmap: Bitmap, photoFileName: String): Single<File> {
        return Single.create<File> { emitter ->
            val extStorageDirectory = Environment.getExternalStorageDirectory().toString()
            var outStream: OutputStream?
            var file = File(extStorageDirectory, photoFileName + Calendar.getInstance().timeInMillis)
            if (file.exists()) {
                file.delete()
                file = File(extStorageDirectory, photoFileName + Calendar.getInstance().timeInMillis)
            }
            try {
                outStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
                outStream.flush()
                outStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.onError(e)
            }
            emitter.onSuccess(file)
        }.subscribeOn(Schedulers.newThread())
    }

    private fun buildRequest(sosFilePath:String): UserInfoUpdateRequest {
        val request = UserInfoUpdateRequest()
        request.has_passport = true
        request.headshot_photo = sosFilePath
        return request
    }

    override fun saveCompanyName(companyName:String) { sessionManager.companyName = companyName}

    override fun buildUserInfoAndCredentialsAsyncCall(): Flowable<Any> = Single.concat(repository.getPersonalInfo(userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token), repository.getCredentials(userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token))

    override fun buildUpdatePersonalInfoAsyncCall(photo: Bitmap): Single<UserInfoResponse> = convertBitmapToFile(photo,Constants.HEAD_SHOT_PHOTO)
            .flatMap { response -> repository.uploadImages(response, Constants.HEAD_SHOT_PHOTO)
                    .flatMap { result -> repository.updatePersonalInfo(buildRequest(result.access_key), userId, token)}
                    .flatMap { repository.getPersonalInfo(userId, token) }  }
            .observeOn(AndroidSchedulers.mainThread())



}