package au.com.signonsitenew.domain.usecases.sharepassport

import au.com.signonsitenew.domain.models.SharePassportRequest
import au.com.signonsitenew.domain.models.SharePassportResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface SharePassportUseCase {
    fun isValidEmailAddress(email:String): Boolean
    fun sharePassport(sharePassportRequest: SharePassportRequest): Single<SharePassportResponse>
    fun buildShareRequest(email: String):SharePassportRequest
}

class SharePassportUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService) :SharePassportUseCase {

    override fun isValidEmailAddress(email:String): Boolean = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

    override fun sharePassport(sharePassportRequest: SharePassportRequest): Single<SharePassportResponse> = repository
            .sharePassport(sharePassportRequest,userId = userService.currentUserId.toString(), token = Constants.BEARER_HEADER + sessionManager.token)
            .observeOn(AndroidSchedulers.mainThread())

    override fun buildShareRequest(email: String):SharePassportRequest{
        val listOfEmails = mutableListOf<String>()
        listOfEmails.add(email)
        return SharePassportRequest(listOfEmails)
    }

}