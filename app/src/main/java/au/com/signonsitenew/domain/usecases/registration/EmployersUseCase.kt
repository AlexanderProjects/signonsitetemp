package au.com.signonsitenew.domain.usecases.registration

import au.com.signonsitenew.domain.models.EmployersRequest
import au.com.signonsitenew.domain.models.EmployersResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface EmployersUseCase {
    fun getListOfEmployers() : Single<EmployersResponse>
}
class EmployersUseCaseImpl @Inject constructor(private val dataRepository: DataRepository):EmployersUseCase {

    override fun getListOfEmployers() : Single<EmployersResponse> = dataRepository.getListOfEmployers(buildRequest())
            .observeOn(AndroidSchedulers.mainThread())

    private fun buildRequest():EmployersRequest {
        val request = EmployersRequest()
        request.authentication = Constants.AUTH_KEY
        return request
    }
}