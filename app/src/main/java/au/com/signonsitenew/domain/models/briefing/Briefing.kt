package au.com.signonsitenew.domain.models.briefing

import java.util.*

data class Briefing (val id:Int,
                     val number:Int,
                     val start_date: Date,
                     val end_date :Date?,
                     val current_status:String,
                     val acknowledgement_count:Int,
                     val seen_count:Int,
                     val type:String,
                     val content:String,
                     val content_text:String?,
                     val should_show_set_by_user:Boolean)