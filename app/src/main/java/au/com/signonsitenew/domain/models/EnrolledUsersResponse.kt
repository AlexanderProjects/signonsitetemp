package au.com.signonsitenew.domain.models

data class EnrolledUsersResponse (override val status: String,val users:List<EnrolledUser>): ApiResponse(status)