package au.com.signonsitenew.domain.models

data class TodaysVisitsResponse(
        val visits:List<Visits>,
        val visitors:List<Visitors>,
        val has_active_briefing:Boolean,
        val has_active_site_induction_form:Boolean,
        override val status:String): ApiResponse(status)