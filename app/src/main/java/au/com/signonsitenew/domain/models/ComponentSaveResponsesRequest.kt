package au.com.signonsitenew.domain.models

data class ComponentSaveResponsesRequest (val responses:List<ComponentTypeForm>)