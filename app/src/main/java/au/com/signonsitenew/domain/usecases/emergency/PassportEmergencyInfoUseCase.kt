package au.com.signonsitenew.domain.usecases.emergency

import au.com.signonsitenew.domain.models.Contact
import au.com.signonsitenew.domain.models.PhoneNumber
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PassportEmergencyBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject


interface PassportEmergencyInfoUseCase {
    fun setRequest(updateRequest: UserInfoUpdateRequest)
    fun validateContact(nameContact: String, phoneContact: String, alpha2: String?): Contact?
    fun isEmptyMedicalInformation(information:String?, isEmpty:()->Unit,isNotEmpty:()->Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position: Int, callAction: () -> Unit)
    fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit)
    fun clearMedicalInfo(userInfoUpdateRequest: UserInfoUpdateRequest):Flowable<ApiResponse>
    fun clearSecondaryContact(userInfoUpdateRequest: UserInfoUpdateRequest):Flowable<ApiResponse>
}

class PassportEmergencyInfoUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService):  PassportEmergencyInfoUseCase, PassportEmergencyBaseUseCase<UserInfoResponse, ApiResponse>() {

    private lateinit var updateRequest: UserInfoUpdateRequest

    override fun setRequest(updateRequest: UserInfoUpdateRequest){
        this.updateRequest = updateRequest
    }

    override fun buildGetPersonalInfoAsyncCall(): Single<UserInfoResponse>  = repository.getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token)

    override fun buildUpdateAndGetPersonalInfoAsyncCall(): Flowable<ApiResponse> = Single.concat(
            repository.updatePersonalInfo(updateRequest, userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token),
            repository.getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token))


    override fun validateContact(nameContact: String, phoneContact: String, alpha2: String?): Contact? {
        val contact = Contact()
        val phoneNumber = PhoneNumber()
        if (nameContact != Constants.NOT_PROVIDED || phoneContact != Constants.NOT_PROVIDED) {
            if (nameContact != Constants.NOT_PROVIDED && nameContact.isNotEmpty()) {
                contact.name = nameContact
            }
            if (phoneContact != Constants.NOT_PROVIDED && phoneContact.isNotEmpty()) {
                phoneNumber.number = phoneContact
                if (alpha2 != null) phoneNumber.alpha2 = alpha2 else phoneNumber.alpha2 = Constants.AU_CODE
                contact.phone_number = phoneNumber
                if (contact.name == null) contact.name = ""
            }
            return contact
        }
        return null
    }

    override inline fun isEmptyMedicalInformation(information:String?, isEmpty:()->Unit, isNotEmpty:()->Unit){
        if(information.isNullOrEmpty())
            isEmpty()
        else
            isNotEmpty()

    }

    override fun checkedFlagsBeforeSelectedAnotherOptionOnMainTab(position: Int, callAction: () -> Unit) {
        if (position != 2) {
            if (sessionManager.editingInEmergencyPassport) {
                callAction()
            }
        }
    }

    override fun checkedFlagsBeforeSelectedAnotherOptionOnPassportTab(position: Int, callAction: () -> Unit) {
        if (position != 1) {
            if (sessionManager.editingInEmergencyPassport) {
                callAction()
            }
        }
    }

    override fun clearMedicalInfo(userInfoUpdateRequest: UserInfoUpdateRequest):Flowable<ApiResponse>{
        updateRequest = userInfoUpdateRequest
        updateRequest.medical_information = null
        return updateAndGetPersonalInfoAsync()
    }

    override fun clearSecondaryContact(userInfoUpdateRequest: UserInfoUpdateRequest):Flowable<ApiResponse>{
        updateRequest = userInfoUpdateRequest
        updateRequest.secondary_contact = null
        return updateAndGetPersonalInfoAsync()
    }

}