package au.com.signonsitenew.domain.repository

import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.InductionResponse
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.SiteContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.models.attendees.AttendeesResponse
import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.domain.models.state.AttendanceNotificationState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.models.*
import au.com.signonsitenew.realm.SiteInduction
import com.segment.analytics.Properties
import io.reactivex.Maybe
import io.reactivex.Single

import retrofit2.Response
import java.io.File

import kotlin.collections.HashMap

interface DataRepository {
    fun getEnrolments(userId: String, token: String): Single<EnrolmentResponse>
    fun getCredentials(userId: String, token: String): Single<CredentialsResponse>
    fun updateCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse>
    fun createCredentials(request: CredentialCreateUpdateRequest, userId: String, bearerToken: String): Single<CredentialCreateUpdateResponse>
    fun getCredentialTypes(bearerToken: String): Single<CredentialTypesResponse>
    fun deleteCredential(credentialId: String, bearerToken: String): Single<DeleteCredentialResponse>
    fun updateEnrolments(enrolmentId: String, isAutomaticAttendanceEnabled: String, token: String): Single<EnrolmentUpdateResponse>
    fun getFeatureFlags(userId: String, token: String): Single<FeatureFlagsResponse>
    fun getCompanyFeatureFlags(siteId: String, token: String):Single<CompanyFeatureFlagResponse>
    fun sharePassport(sharePassportRequest: SharePassportRequest, userId: String, token: String): Single<SharePassportResponse>
    fun getPersonalInfo(userId: String, bearerToken: String): Single<UserInfoResponse>
    fun updatePersonalInfo(userRequest: UserInfoUpdateRequest, userId: String, bearerToken: String): Single<UpdateInfoResponse>
    fun uploadImages(file: File, fileName: String): Single<UploadImageResponse>
    fun getPhotoUrl(accessKey: String): Single<RetrieveImageResponse>
    fun getCompaniesForSite(siteId: String, userEmail: String, token: String, bearerToken: String): Single<CompaniesForSiteResponse>
    fun saveCompaniesInDb(companies: List<Company>)
    fun getNearBySites(userId: String, bearerToken: String, latitude: Float, longitude: Float, uncertainty: Float): Single<NearSitesResponse>
    fun validateEmail(emailVerificationRequest: EmailVerificationRequest): Single<EmailVerificationResponse>
    fun validatePhoneNumber(alpha2: String, raw: String, email: String, auth: String): Single<ApiResponse>
    fun getListOfEmployers(employersRequest: EmployersRequest): Single<EmployersResponse>
    fun userRegistration(userRegistrationRequest: UserRegistrationRequest): Single<UserRegistrationResponse>
    fun getListOfBriefings(siteId: String, limit: String, offSet: String, bearerToken: String): Single<BriefingsResponse>
    fun saveOffsetForBriefings(value: String)
    fun saveActiveBriefingInDb(briefing: WorkerBriefing, siteId: Int)
    fun getActiveBriefings(siteId: String, authHeader: String): Single<BriefingWorkerResponse>
    fun getOffsetValue(): String
    fun clearOffsetValue()
    fun acknowledgeBriefings(briefingId: String, authHeader: String): Single<ApiResponse>
    fun updateBriefingContent(authHeader: String, siteId: String, briefingContent: String): Single<ApiResponse>
    fun seenBriefing(briefingId: String, authHeader: String): Single<ApiResponse>
    fun getTodaysVisits(siteId: String, userEmail: String, token: String, bearerToken: String): Single<TodaysVisitsResponse>
    fun sendTrackEvents(event: String, properties: Properties?)
    fun sendScreenEvents(name: String?, properties: Properties?)
    fun callIdentity(userId: String)
    fun saveLinkedUserId(userId: Long)
    fun getSavedPreviousLikedUserId(): Long
    fun getUserContextForAnalytics(platform: String, userId: String, bearerToken: String): Single<UserContextAnalyticsResponse>
    fun getSiteContextForAnalytics(platform: String, siteId: String, bearerToken: String): Single<SiteContextAnalyticsResponse>
    fun getCompanyContextAnalytics(platform: String, companyId: String, bearerToken: String): Single<CompanyContextAnalyticsResponse>
    fun remoteBriefingNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun remoteInductionNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun defaultNotification(message: String?, extraProperties: HashMap<String, Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun localSignOnPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int)
    fun localSignOffPromptNotification(notificationName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int)
    fun localAttendanceNotification(isSignedOn: Boolean, siteName: String?, analyticsEventDelegateService: AnalyticsEventDelegateService, extraProperties: HashMap<String,Any>?, siteId:Int, attendanceNotificationState: AttendanceNotificationState)
    fun getCurrentSiteName(siteId: Int): String
    fun cancelNotifications(notificationId: Int)
    fun deleteBriefings()
    fun retrieveSignedOnStatus(bearerToken: String, userEmail: String, token: String): Maybe<Map<String, Any>>
    fun getActiveInductions(bearerToken: String, email: String, token: String, siteId: String): Single<InductionResponse>
    fun saveInduction(inductionId:Int?,userId:Long,siteId:Int,inductionType:String?,inductionState:String?,updateAt:String?)
    fun deleteInduction(siteId: Int)
    suspend fun registerFcmToken(authHeader: String, email: String, auth: String, fcmToken:String, deviceType:String):Response<RegisterFcmTokenResponse>
    fun unRegisterInstanceId()
    fun startEvacuation(bearerToken: String, email: String, token: String, siteId: String, actualEmergency:Boolean):Single<ApiResponse>
    fun stopEvacuation(bearerToken: String, email: String, token: String, siteId: String, stopEvacuationRequestList: List<StopEvacuationRequest>):Single<ApiResponse>
    fun visitorEvacuation(bearerToken: String, email: String, token: String, siteId: String):Single<EvacuationVisitorsResponse>
    fun evacuationStartedNotification(message: String?,extraProperties: HashMap<String,Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun evacuationEndedNotification(message: String?,extraProperties: HashMap<String,Any>?, notificationName: String?, utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun remoteRiskyWorkerNotification(message: String?,userId: String ,extraProperties: HashMap<String, Any>?,notificationName: String?,utcNotificationSentAt: String?, analyticsEventDelegateService: AnalyticsEventDelegateService?)
    fun saveSiteVisitorsForEvacuations(visitors:List<EvacuationVisitor>)
    fun getEvacuationVisitors():List<StopEvacuationRequest>
    fun removeSiteVisits()
    fun attendees(siteId: String, bearerToken: String, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only:Boolean): Single<AttendeesResponse>
    fun attendees(siteId: String, bearerToken: String, filterUserId: Int, filterAttendanceStartUtc: String, filterAttendanceEndUtc: String, unarchived_worker_notes_only:Boolean):Single<AttendeesResponse>
    fun attendeeSignOn(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String):Single<ApiResponse>
    fun attendeeSignOff(authHeader: String, email: String, auth: String, siteId: String, attendeeId: String):Single<ApiResponse>
    fun getSiteInductionForUser(userId: Long, siteId: Long): SiteInduction?
    fun canAccessUserInductions(userId: Long, siteId: Long): Boolean
    fun getSiteTimeZone(siteId: Int):String?
    fun getCurrentPermits(siteId: String, authHeader: String):Single<PermitResponse>
    fun getPermitTemplates(siteId: String, authHeader: String, filterEnable: Boolean): Single<SitePermitTemplateResponse>
    fun getPermitInfo(permitId:String, authHeader: String):Single<PermitInfoResponse>
    fun getEnrolledUsers(authHeader: String, getCompanies:Boolean, siteId:String, email: String):Single<EnrolledUsersResponse>
    fun setTeamMemberStatus(permitId: String,userId: String,status:String,authHeader: String):Single<ApiResponse>
    fun hasManagerPermission(userId: Long, siteId: Long): Boolean
    fun updatePermit(updatePermitInfoRequest: UpdatePermitInfoRequest, permitId:String, authHeader: String):Single<ApiResponse>
    fun createPermit(createPermitRequest: CreatePermitRequest, siteId:String, authHeader: String):Single<SavePermitResponse>
    fun saveResponses(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId:String, authHeader: String):Single<ApiResponse>
    fun getLocalUserFullName():String
}