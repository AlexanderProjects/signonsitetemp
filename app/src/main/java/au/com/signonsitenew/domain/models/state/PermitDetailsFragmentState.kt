package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.Permit
import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.SitePermitTemplate

sealed class PermitDetailsFragmentState {
    data class SetPermitDetails(val permit: Permit?, val permitInfo: PermitInfo):PermitDetailsFragmentState()
    object ShowPermitError:PermitDetailsFragmentState()
    object ShowError:PermitDetailsFragmentState()
    object ShowPermitToolbar:PermitDetailsFragmentState()
    object HidePermitToolbar:PermitDetailsFragmentState()
    object NavigateToCurrentPermits:PermitDetailsFragmentState()
    object ShowCancelAlertDialogInRequest:PermitDetailsFragmentState()
    object ShowCancelAlertDialogInProgress:PermitDetailsFragmentState()
    object ShowSaveButton:PermitDetailsFragmentState()
    object HideSaveButton:PermitDetailsFragmentState()
    object ShowProgressView:PermitDetailsFragmentState()
    object RemoveProgressView:PermitDetailsFragmentState()
}