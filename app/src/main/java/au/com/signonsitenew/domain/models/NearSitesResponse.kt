package au.com.signonsitenew.domain.models

data class NearSitesResponse(val status:String, val sites:List<NearSite>)