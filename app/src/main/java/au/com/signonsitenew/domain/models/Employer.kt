package au.com.signonsitenew.domain.models

data class Employer (val msg:String, val company_id:Int, val company_name:String, val status:String)