package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

abstract class AttendanceRegisterBaseUseCase<T,P, Z> protected constructor() {
    val disposables = CompositeDisposable()

    abstract fun buildGetTodayVisitsAsyncCall() : Single<T>

    abstract fun buildGetAttendeesAsyncCall() :Single<P>

    abstract fun buildGetAttendeeWorkerAsyncCall(filterUserId: Int) : Single<Z>

    fun getTodayVisitsAsync(asyncCall: DisposableSingleObserver<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetTodayVisitsAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun getAttendeesAsync(asyncCall: DisposableSingleObserver<P>){
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetAttendeesAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun getAttendeeWorkerAsync(filterUserId: Int,asyncCall: DisposableSingleObserver<Z>){
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetAttendeeWorkerAsyncCall(filterUserId)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}