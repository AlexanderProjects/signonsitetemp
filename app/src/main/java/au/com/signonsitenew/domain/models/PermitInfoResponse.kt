package au.com.signonsitenew.domain.models

data class PermitInfoResponse(override val status: String,
                              val permit:PermitInfo,
                              val start_date:String?,
                              val end_date:String?
): ApiResponse(status)