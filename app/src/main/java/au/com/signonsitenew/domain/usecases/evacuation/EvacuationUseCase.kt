package au.com.signonsitenew.domain.usecases.evacuation

import au.com.signonsitenew.domain.models.EvacuationVisitor
import au.com.signonsitenew.domain.models.EvacuationVisitorsResponse
import au.com.signonsitenew.domain.models.StopEvacuationRequest
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface EvacuationUseCase{
    fun starEvacuation(actualEmergency:Boolean):Single<ApiResponse>
    fun stopEvacuation(stopEvacuationRequest: List<StopEvacuationRequest>):Single<ApiResponse>
    fun visitorEvacuation():Single<EvacuationVisitorsResponse>
    fun saveEvacuationVisitors(visitors:List<EvacuationVisitor>)
    fun retrieveEvacuationVisitors():List<StopEvacuationRequest>
    fun removeSiteVisitors()
}

class EvacuationUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager):EvacuationUseCase{
    override fun starEvacuation(actualEmergency:Boolean): Single<ApiResponse> =
        repository.startEvacuation(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, sessionManager.siteId.toString(),actualEmergency)
                .observeOn(AndroidSchedulers.mainThread())

    override fun stopEvacuation(stopEvacuationRequest: List<StopEvacuationRequest>): Single<ApiResponse> =
            repository.stopEvacuation(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY,sessionManager.siteId.toString(),stopEvacuationRequest)
                    .observeOn(AndroidSchedulers.mainThread())

    override fun visitorEvacuation(): Single<EvacuationVisitorsResponse> =
            repository.visitorEvacuation(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY,sessionManager.siteId.toString())
            .observeOn(AndroidSchedulers.mainThread())

    override fun saveEvacuationVisitors(visitors: List<EvacuationVisitor>) = repository.saveSiteVisitorsForEvacuations(visitors)

    override fun retrieveEvacuationVisitors(): List<StopEvacuationRequest> = repository.getEvacuationVisitors()

    override fun removeSiteVisitors() = repository.removeSiteVisits()

}