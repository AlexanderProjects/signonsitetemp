package au.com.signonsitenew.domain.models

data class FeatureFlagsResponse (override val status:String, val user_feature_flags: List<FeatureFlag>): ApiResponse(status)