package au.com.signonsitenew.domain.services.analytics

import au.com.signonsitenew.domain.models.CompanySegmentProperties
import au.com.signonsitenew.domain.models.SignOnOffType
import au.com.signonsitenew.domain.models.analytics.Options
import au.com.signonsitenew.domain.usecases.analytics.AnalyticsEventUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.DateFormatUtil
import com.segment.analytics.Properties
import javax.inject.Inject

interface AnalyticsEventDelegateService {
    fun registrationCompleteTrackEvent()
    fun siteInductionFormStarted()
    fun siteInductionFormFileUploaded()
    fun siteInductionFormSignatureUploaded()
    fun siteInductionFormSubmitted()
    fun passportCreated()
    fun sharedPassport(receiver_email:String)
    fun passportCredentialsUpdated()
    fun passportCredentialsRemoved()
    fun managerAttendanceRegisterSiteInductionReviewed(targetted_user_id:Int)
    fun managerAttendanceRegisterSiteInductionAccepted(targetted_user_id:Int)
    fun managerAttendanceRegisterSiteInductionRejected(targetted_user_id:Int)
    fun managerEvacuationDrillTriggered()
    fun managerEvacuationEmergencyTriggered()
    fun managerAttendanceRegisterViewed()
    fun managerEvacuationCompleted()
    fun siteBriefingOpened()
    fun siteBriefingAcknowledged()
    fun signedOn(type:SignOnOffType, companySegmentProperties: CompanySegmentProperties,isFirstSignOn:Boolean)
    fun signedOff(type:SignOnOffType)
    fun notificationReceived(notificationName:String, notificationType:String, metaData:HashMap<String,Any>, notificationSentAt:String?)
    fun notificationClicked(notificationName:String, notificationType: String,metaData:HashMap<String,Any>, notificationSentAt: String?)
    fun inductionFormSubmissionStarted()
    fun inductionFormSubmissionLoaded(isNewForm:Boolean)
    fun registrationStarted()
    fun registrationNameAndEmailProvided()
    fun registrationPhoneProvided()
    fun registrationPassportProvided()
    fun registrationCompanyNameProvided()
    fun menuHelpOpened()
    fun workerNotes()
    fun attendanceRegisterNewWorkerSignOnPressed()
    fun attendanceRegisterWorkerSignOnPressed(targetted_user_id:Int)
    fun attendanceRegisterWorkerSignOffPressed(targetted_user_id:Int)
    fun attendanceRegisterWorkerViewed(targetted_user_id:Int)
    fun attendanceRegisterWorkerSearchForWorkerViewed()
    fun attendanceRegisterWorkerSearchForWorkerSelected(targetted_user_id:Int)
    fun attendanceRegisterWorkerSearchForWorkerNewPressed()
    fun setSiteId(siteId:Int):AnalyticsEventDelegateService
}
class AnalyticsEventDelegateServiceImpl @Inject constructor(private val analyticsUseCase: AnalyticsEventUseCase): AnalyticsEventDelegateService {

    override fun registrationCompleteTrackEvent() = analyticsUseCase.sendTrackEvent(
            Constants.ANDROID_APP_REGISTRATION_COMPLETED, Properties(),
            Options(hasSiteContext = false,
                    hasCompanyContext = false,
                    hasUserCompanyId = true,
                    hasSiteOwnerCompanyId = false,
                    hasMetadata = false),
            clearTargetedUserId = true)
    override fun siteInductionFormStarted() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_STARTED,
            Properties(),
            Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false),
            clearTargetedUserId = true)
    override fun siteInductionFormFileUploaded() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_FILE_UPLOADED, Properties(),
            Options(hasSiteContext = true,
                    hasUserCompanyId = false,
                    hasSiteOwnerCompanyId = true,
                    hasCompanyContext = false,
                    hasMetadata = false)
            ,clearTargetedUserId = true)
    override fun siteInductionFormSignatureUploaded() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SIGNATURE_UPLOADED, Properties(),
            Options(hasSiteContext = true,
                    hasCompanyContext = false,
                    hasSiteOwnerCompanyId = true,
                    hasUserCompanyId = false,
                    hasMetadata = false),
            clearTargetedUserId = true)
    override fun siteInductionFormSubmitted() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_SITE_INDUCTION_FORM_SUBMITTED, Properties(),
            Options(hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
                    hasMetadata = false),
            clearTargetedUserId = true)
    override fun passportCreated() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_PASSPORT_CREATED, Properties(), Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasUserCompanyId = true,
            hasSiteOwnerCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    override fun sharedPassport(receiver_email: String) {
        val prop = Properties()
        prop[Constants.RECEIVER_EMAIL] = receiver_email
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_PASSPORT_SHARE_SENT, prop, Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasUserCompanyId = true,
            hasSiteOwnerCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    }
    override fun passportCredentialsUpdated() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_PASSPORT_CREDENTIALS_UPDATED, Properties(), Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasUserCompanyId = true,
            hasSiteOwnerCompanyId = false,
            hasMetadata = false)
            ,clearTargetedUserId = true)
    override fun passportCredentialsRemoved() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_PASSPORT_CREDENTIALS_REMOVED, Properties(), Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasUserCompanyId = true,
            hasSiteOwnerCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    override fun managerAttendanceRegisterSiteInductionReviewed(targetted_user_id:Int) {
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REVIEWED, prop.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false),clearTargetedUserId = false)
    }
    override fun managerAttendanceRegisterSiteInductionAccepted(targetted_user_id:Int){
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_ACCEPTED, prop.toMap(Properties()),Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false),clearTargetedUserId = false)
    }
    override fun managerAttendanceRegisterSiteInductionRejected(targetted_user_id:Int){
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_SITE_INDUCTION_REJECTED, prop.toMap(Properties()),Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false),clearTargetedUserId = false)
    }
    override fun managerEvacuationDrillTriggered() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_EVACUATION_DRILL_TRIGGERED, Properties(), Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)

    override fun managerEvacuationEmergencyTriggered() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_EVACUATION_EMERGENCY_TRIGGERED, Properties(), Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)

    override fun managerAttendanceRegisterViewed() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_VIEWED, Properties(), Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)

    override fun managerEvacuationCompleted() =  analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_EVACUATION_COMPLETED, Properties(), Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    override fun siteBriefingOpened() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_BRIEFING_OPENED, Properties(),Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    override fun siteBriefingAcknowledged() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SITE_DOCS_BRIEFING_ACKNOWLEDGED, Properties(),Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)
    override fun signedOn(type: SignOnOffType, companySegmentProperties: CompanySegmentProperties, isFirstSignOn: Boolean) {
        val prop = Properties()
        prop[Constants.TYPE] = type
        prop[Constants.FIRST] = isFirstSignOn
        prop[Constants.COMPANY_INPUT_TYPE] = analyticsUseCase.mapCompanyPropertyTypeToString(companySegmentProperties.company_input_type)
        prop[Constants.CUSTOM_COMPANY_NAME] = companySegmentProperties.custom_company_name
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_SIGNED_ON, prop, Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false
            ), clearTargetedUserId = true
        )
    }

    override fun signedOff(type: SignOnOffType) {
        val prop = Properties()
        prop[Constants.TYPE] = type
        analyticsUseCase.sendTrackEvent(
            Constants.ANDROID_APP_SIGNED_OFF, prop, Options(
                hasSiteContext = false,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = false,
                hasUserCompanyId = true,
                hasMetadata = false
            ), clearTargetedUserId = true
        )
    }
    override fun notificationReceived(notificationName:String, notificationType:String, metaData:HashMap<String,Any>, notificationSentAt:String?) {
        val hasMetaData = metaData.isNotEmpty()
        metaData[Constants.NOTIFICATION_NAME] = notificationName
        metaData[Constants.ANALYTICS_NOTIFICATION_TYPE] = notificationType
        if(notificationSentAt != null)
            metaData[Constants.ANALYTICS_DELAY_SECONDS] = DateFormatUtil.getTimeDiff(notificationSentAt)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_NOTIFICATION_RECEIVED, metaData.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = hasMetaData), clearTargetedUserId = true)
    }
    override fun notificationClicked(notificationName:String, notificationType: String, metaData:HashMap<String,Any>, notificationSentAt: String?) {
        val hasMetaData = metaData.isNotEmpty()
        metaData[Constants.NOTIFICATION_NAME] = notificationName
        metaData[Constants.ANALYTICS_NOTIFICATION_TYPE] = notificationType
        if(notificationSentAt != null)
            metaData[Constants.ANALYTICS_DELAY_SECONDS] = DateFormatUtil.getTimeDiff(notificationSentAt)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_NOTIFICATION_CLICKED, metaData.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = hasMetaData), clearTargetedUserId = true)
    }

    override fun inductionFormSubmissionStarted() {
        val prop = HashMap<String,Any>()
        analyticsUseCase.generateUniqueIdentifier()
        prop[Constants.UNIQUE_ID] = analyticsUseCase.getUniqueIdentifier()
        analyticsUseCase
                .sendTrackEvent(Constants.ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADING_STARTED,prop.toMap(Properties()),Options(
                        hasSiteContext = true,
                        hasCompanyContext = false,
                        hasSiteOwnerCompanyId = true,
                        hasUserCompanyId = false,
                        hasMetadata = false),clearTargetedUserId = true )
    }

    override fun inductionFormSubmissionLoaded(isNewForm:Boolean){
        val prop = HashMap<String,Any>()
        prop[Constants.UNIQUE_ID] = analyticsUseCase.getUniqueIdentifier()
        prop[Constants.IS_NEW_FORM] = isNewForm
        analyticsUseCase
                .sendTrackEvent(Constants.ANDROID_APP_ENG_SITE_INDUCTION_FORM_SUBMISSION_LOADED,prop.toMap(Properties()),Options(
                        hasSiteContext = true,
                        hasCompanyContext = false,
                        hasSiteOwnerCompanyId = true,
                        hasUserCompanyId = false,
                        hasMetadata = false),clearTargetedUserId = true )
    }

    override fun registrationStarted() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_REGISTRATION_STARTED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun registrationNameAndEmailProvided() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_REGISTRATION_NAME_AND_EMAIL_PROVIDED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun registrationPhoneProvided() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_REGISTRATION_PHONE_PROVIDED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun registrationPassportProvided() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_REGISTRATION_PASSWORD_PROVIDED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun registrationCompanyNameProvided() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_REGISTRATION_COMPANY_NAME_PROVIDED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun menuHelpOpened() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MENU_HELP_OPENED,Properties(),Options(
            hasSiteContext = false,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = false,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun workerNotes() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_REGISTER_WORKER_NOTES_VIEWED, Properties(),Options(
                hasSiteContext = true,
                hasCompanyContext = false,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false),clearTargetedUserId = false)


    override fun attendanceRegisterNewWorkerSignOnPressed() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_NEW_WORKER_SIGN_ON_PRESSED, Properties(),Options(
            hasSiteContext = true,
            hasCompanyContext = true,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = false,
            hasMetadata = false),clearTargetedUserId = true)

    override fun attendanceRegisterWorkerSignOnPressed(targetted_user_id:Int) {
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_ON_PRESSED, prop.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = true,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false), clearTargetedUserId = false)
    }
    override fun attendanceRegisterWorkerSignOffPressed(targetted_user_id:Int) {
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SIGN_OFF_PRESSED, prop.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = true,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = false,
                hasMetadata = false), clearTargetedUserId = false)
    }

    override fun attendanceRegisterWorkerViewed(targetted_user_id:Int) {
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_VIEWED, prop.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = true,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = true,
                hasMetadata = false), clearTargetedUserId = false)
    }

    override fun attendanceRegisterWorkerSearchForWorkerViewed() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_VIEWED,Properties(),Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)

    override fun attendanceRegisterWorkerSearchForWorkerSelected(targetted_user_id:Int) {
        val prop = analyticsUseCase.setTargetIdUserForContext(Properties(),targetted_user_id)
        analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_SELECTED,  prop.toMap(Properties()), Options(
                hasSiteContext = true,
                hasCompanyContext = true,
                hasSiteOwnerCompanyId = true,
                hasUserCompanyId = true,
                hasMetadata = false), clearTargetedUserId = false)
    }

    override fun attendanceRegisterWorkerSearchForWorkerNewPressed() = analyticsUseCase.sendTrackEvent(Constants.ANDROID_APP_MANAGER_ATTENDANCE_REGISTER_WORKER_SEARCH_FOR_WORKER_NEW_PRESSED,Properties(),Options(
            hasSiteContext = true,
            hasCompanyContext = false,
            hasSiteOwnerCompanyId = true,
            hasUserCompanyId = true,
            hasMetadata = false),clearTargetedUserId = true)


    override fun setSiteId(siteId:Int):AnalyticsEventDelegateService{
        analyticsUseCase.setSiteId(siteId)
        return this
    }
}