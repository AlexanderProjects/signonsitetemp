package au.com.signonsitenew.domain.usecases.signonstatus

import android.util.Log
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

interface SignOnStatusUseCase {
    fun getUserStatusAsync(isUserSignedOnCallBack: (Boolean)->Unit)
    fun checkForUserStatus():Maybe<Map<String,Any>>
}

class SignOnStatusUseCaseImpl @Inject constructor(private val repository: DataRepository,
                                                  private val sessionManager: SessionManager) : SignOnStatusUseCase {

    override fun getUserStatusAsync(isUserSignedOnCallBack: (Boolean)->Unit) {
        val disposables = CompositeDisposable()
        disposables.add(repository.retrieveSignedOnStatus(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if(it["signed_on"] as Boolean){
                        sessionManager.siteId = (it["site_id"] as Double).toInt()
                    }else{
                        if(sessionManager.siteId != 0) {
                            sessionManager.clearSiteId()
                            repository.deleteBriefings()
                        }
                    }
                    isUserSignedOnCallBack(it["signed_on"] as Boolean)
                },{
                    it.message?.let { it1 -> Log.d(SignOnStatusUseCaseImpl::class.java.name, it1) }
                },{
                    disposables.clear()
                }))

    }

    override fun checkForUserStatus():Maybe<Map<String,Any>> = repository.retrieveSignedOnStatus(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!,Constants.AUTH_KEY)
            .observeOn(AndroidSchedulers.mainThread())
}