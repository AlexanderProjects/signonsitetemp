package au.com.signonsitenew.domain.models.adapters

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AttendanceAdapterChildModel(val workerName:String,
                                       val userId:Int,
                                       val timeIn:String,
                                       val timeOut:String?,
                                       val briefing:Boolean,
                                       val induction:String?,
                                       val hasActiveSiteInductionForm:Boolean,
                                       val hasActiveBriefing:Boolean,
                                       val type:String?) : Parcelable