package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Enrolment(val id: Int,
                     val is_automatic_attendance_enabled: Boolean,
                     val is_hidden: Boolean,
                     val site: Site,
                     val company: Company,
                     val utc_last_signon_at:Date?,
                     val site_induction : SiteInduction?): Parcelable{
}