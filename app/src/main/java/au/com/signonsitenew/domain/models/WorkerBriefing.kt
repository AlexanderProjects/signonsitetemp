package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.domain.models.briefing.SetByUser
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WorkerBriefing (val id:Int,
                           val type:String,
                           val content:String,
                           val needs_acknowledgement:Boolean,
                           val should_show_set_by_user:Boolean,
                           val set_by_user: SetByUser) : Parcelable