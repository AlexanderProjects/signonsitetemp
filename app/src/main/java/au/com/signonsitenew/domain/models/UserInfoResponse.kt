package au.com.signonsitenew.domain.models


class UserInfoResponse (val user: User, override val status: String): ApiResponse(status)