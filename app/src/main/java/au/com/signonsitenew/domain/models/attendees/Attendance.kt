package au.com.signonsitenew.domain.models.attendees

import android.os.Parcelable
import au.com.signonsitenew.domain.models.Company
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Attendance (val id:Int, val signon_at: String, val signoff_at:String?, val signon_channel:String, val company:Company, val briefing_status:String) : Parcelable