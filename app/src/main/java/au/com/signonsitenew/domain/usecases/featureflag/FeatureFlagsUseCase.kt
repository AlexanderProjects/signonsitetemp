package au.com.signonsitenew.domain.usecases.featureflag

import au.com.signonsitenew.domain.models.CompanyFeatureFlagResponse
import au.com.signonsitenew.domain.models.FeatureFlag
import au.com.signonsitenew.domain.models.FeatureFlagsResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.FeatureFlagsBaseUseCase
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import javax.inject.Inject

class FeatureFlagsUseCase @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService): FeatureFlagsBaseUseCase<FeatureFlagsResponse,CompanyFeatureFlagResponse>() {


    private fun createOnErrorResponse(): FeatureFlagsResponse {
        val listOfFeatureFlag = mutableListOf<FeatureFlag>()
        val featureFlag = FeatureFlag("passports", false)
        listOfFeatureFlag.add(featureFlag)
        return FeatureFlagsResponse("success",listOfFeatureFlag)
    }

    override fun buildGetUserFeatureFlagsAsyncCall(): Single<FeatureFlagsResponse> = repository
            .getFeatureFlags(userId = userService.currentUserId.toString(), token = Constants.BEARER_HEADER + sessionManager.token)

    override fun buildGetCompanyFeatureFlagsAsyncCall(): Single<CompanyFeatureFlagResponse> = repository
            .getCompanyFeatureFlags(siteId = sessionManager.siteId.toString(), token = Constants.BEARER_HEADER + sessionManager.token)


}