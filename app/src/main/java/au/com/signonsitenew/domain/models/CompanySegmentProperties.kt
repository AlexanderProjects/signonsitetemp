package au.com.signonsitenew.domain.models

data class CompanySegmentProperties (val company_input_type:CompanyPropertyType, val custom_company_name:String?)