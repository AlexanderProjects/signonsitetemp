package au.com.signonsitenew.domain.models.analytics

import au.com.signonsitenew.domain.models.ApiResponse

data class CompanyContextAnalyticsResponse(override val status: String, val context: Map<String,Any>): ApiResponse(status)