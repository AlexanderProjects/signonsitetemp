package au.com.signonsitenew.domain.models.state

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

sealed class UsingCameraX: Parcelable{
    @Parcelize object TaskFragment:UsingCameraX()
    @Parcelize object ChecksFragment:UsingCameraX()
}
