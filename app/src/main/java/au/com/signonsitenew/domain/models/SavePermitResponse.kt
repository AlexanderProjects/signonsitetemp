package au.com.signonsitenew.domain.models

data class SavePermitResponse(override val status: String, val permit_id:String): ApiResponse(status)