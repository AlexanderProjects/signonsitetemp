package au.com.signonsitenew.domain.usecases.briefings

import au.com.signonsitenew.domain.models.briefing.BriefingWorkerResponse
import au.com.signonsitenew.domain.models.WorkerBriefing
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.BriefingBaseUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.models.BriefingsResponse
import au.com.signonsitenew.models.Document
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.document
import io.reactivex.Single
import javax.inject.Inject

interface BriefingsUseCase{
    fun saveNewOffSetForBriefings(briefingsResponse: BriefingsResponse)
    fun briefingNeedsAcknowledgement(briefing: WorkerBriefing):Boolean
    fun clearOffSetValue()
    fun isRichText(briefing: WorkerBriefing):Boolean
    fun buildEmptyBriefingDocument(): Document
    fun getQuillUrl():String
    fun getHeader():MutableMap<String,String>
    fun buildBriefingDocument(workerBriefing: WorkerBriefing, notifierCallback:()->Unit, listener:(workerBriefing: WorkerBriefing)->Unit): Document
}

class BriefingsUseCaseImpl @Inject constructor(private val repository: DataRepository,
                                           private val sessionManager: SessionManager):BriefingsUseCase, BriefingBaseUseCase<BriefingsResponse,BriefingWorkerResponse, ApiResponse, ApiResponse, ApiResponse>()  {

    private lateinit var ackBriefing: WorkerBriefing
    private lateinit var seenBriefing: WorkerBriefing
    private lateinit var content:String

    fun setAcknowledgeBriefing(briefing: WorkerBriefing) {
        this.ackBriefing = briefing
    }

    fun setSeenBriefing(briefing: WorkerBriefing){
        this.seenBriefing = briefing
    }

    fun setContent(content: String){
        this.content = content
    }

    override fun buildGetListOfBriefingsAsyncCall(): Single<BriefingsResponse> = repository.getListOfBriefings(sessionManager.siteId.toString(), Constants.PAGINATION_LIMIT_NUMBER,repository.getOffsetValue(), Constants.BEARER_HEADER + sessionManager.token)

    override fun buildGetActiveBriefingsAsyncCall(): Single<BriefingWorkerResponse> = repository.getActiveBriefings(sessionManager.siteId.toString(), Constants.BEARER_HEADER + sessionManager.token)

    override fun buildAcknowledgeBriefingAsyncCall(): Single<ApiResponse> = repository.acknowledgeBriefings(ackBriefing.id.toString(),Constants.BEARER_HEADER + sessionManager.token)

    override fun buildUpdateBriefingContentAsyncCall(): Single<ApiResponse> = repository
            .updateBriefingContent(Constants.BEARER_HEADER + sessionManager.token,sessionManager.siteId.toString(),content)

    override fun buildSeenBriefingAsyncCall(): Single<ApiResponse> = repository.seenBriefing(seenBriefing.id.toString(),Constants.BEARER_HEADER + sessionManager.token)

    override fun saveNewOffSetForBriefings(briefingsResponse: BriefingsResponse){
        val total = briefingsResponse.total
        val perPage = briefingsResponse.per_page
        if(total < perPage){
            val result = total/perPage
            repository.saveOffsetForBriefings(result.toString())
        }else{
            val result = total + 1
            repository.saveOffsetForBriefings(result.toString())
        }
    }

    override fun briefingNeedsAcknowledgement(briefing: WorkerBriefing):Boolean = briefing.needs_acknowledgement

    override fun clearOffSetValue() = repository.clearOffsetValue()

    override fun isRichText(briefing: WorkerBriefing):Boolean = when(briefing.type){
        Constants.RICH_TEXT -> true
        Constants.PLAIN_TEXT -> false
        else -> false
    }
    override fun buildEmptyBriefingDocument(): Document = document { docType =  Constants.DOC_EMPTY_BRIEFING }

    override fun getQuillUrl(): String = Constants.BASE + Constants.URL_QUILL_EDITOR + "?" + "readonly=true"

    override fun getHeader(): MutableMap<String,String> = mutableMapOf("Authorization" to Constants.BEARER_HEADER + sessionManager.token)

    override fun buildBriefingDocument(workerBriefing: WorkerBriefing, notifierCallback: () -> Unit,listener:(workerBriefing: WorkerBriefing)->Unit): Document {
        val document = Document()
        val type = Constants.DOC_BRIEFING
        val state: String
        document.subtype = Constants.BRIEFING_SUBTYPE
        if (workerBriefing.needs_acknowledgement) {
            state = Constants.DOC_BRIEFING_UNACKNOWLEDGED
            notifierCallback()
        } else {
            state = Constants.DOC_BRIEFING_ACKNOWLEDGED
        }
        document.state = state
        document.docType = type
        listener(workerBriefing)
        return document
    }

}