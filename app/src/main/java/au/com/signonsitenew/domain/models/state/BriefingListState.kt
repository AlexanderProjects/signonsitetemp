package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.models.BriefingsResponse

sealed class BriefingListState {
    object Success : BriefingListState()
    data class NoData(val response:BriefingsResponse): BriefingListState()
    object Error: BriefingListState()
}