package au.com.signonsitenew.domain.models

data class EvacuationVisitorsResponse (val visits:List<EvacuationVisitor>, override val status: String): ApiResponse(status)