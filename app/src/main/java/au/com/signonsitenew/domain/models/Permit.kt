package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.domain.models.state.PermitState
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Permit(var id:Int, var human_id:String, var state: PermitState, var permit_template:PermitTemplate):Parcelable