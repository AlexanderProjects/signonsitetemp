package au.com.signonsitenew.domain.models


data class CredentialTypesResponse (val credential_types: List<CredentialType>?, override val status:String): ApiResponse(status)