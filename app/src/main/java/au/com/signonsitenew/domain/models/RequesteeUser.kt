package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.utilities.empty
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class RequesteeUser(val id:Int,
                         val first_name:String?,
                         val last_name:String?,
                         val phone_number:String?,
                         @Transient val company_name:String = String().empty(),
                         val status:PermitDoneState):Parcelable