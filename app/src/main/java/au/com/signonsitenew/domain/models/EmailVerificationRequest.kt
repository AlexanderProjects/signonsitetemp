package au.com.signonsitenew.domain.models

data class EmailVerificationRequest(var email: String?, var authentication: String?){
    constructor() : this(null,null)
}