package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.PermitInfo
import au.com.signonsitenew.domain.models.RequesteeUser

sealed class PermitTeamTabFragmentState {
    data class ReadOnlyPermit(val permitInfo: PermitInfo):PermitTeamTabFragmentState()
    data class FullAccessPermit(val permitInfo: PermitInfo):PermitTeamTabFragmentState()
    data class ShowSelectedMembers(val memberList:List<RequesteeUser>):PermitTeamTabFragmentState()
    data class SaveUnSelectedMembers(val memberList:List<RequesteeUser>):PermitTeamTabFragmentState()
    data class ShowFabButton(val isFabVisible:Boolean):PermitTeamTabFragmentState()
    object NavigateToAddTeamMember:PermitTeamTabFragmentState()
    object NewPermit:PermitTeamTabFragmentState()
    object ShowPermitError:PermitTeamTabFragmentState()
    object ShowError:PermitTeamTabFragmentState()

}