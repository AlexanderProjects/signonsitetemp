package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class PermitContentTypeResponses (val id:Int?,
                                       val value:@RawValue Any?,
                                       val utc_date_time:String?,
                                       val user: PermitContentTypeUser?):Parcelable