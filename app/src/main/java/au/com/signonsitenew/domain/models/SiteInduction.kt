package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SiteInduction (val person_type:String, val state: String):Parcelable