package au.com.signonsitenew.domain.models.state

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class AttendanceAlertState : Parcelable {
    abstract val order:Int
    @Parcelize
    class Alert : AttendanceAlertState(){ override val order: Int = 0 }
    @Parcelize
    class Warning : AttendanceAlertState(){ override val order: Int = 1 }
    @Parcelize
    class Information : AttendanceAlertState(){ override val order: Int = 2 }
    @Parcelize
    class None :AttendanceAlertState(){ override val order: Int = 3 }

}