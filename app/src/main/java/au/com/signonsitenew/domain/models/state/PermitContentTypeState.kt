package au.com.signonsitenew.domain.models.state

sealed class PermitContentTypeState{
    object Readable:PermitContentTypeState()
    object ReadableAndEditable:PermitContentTypeState()
    object GreyedOut:PermitContentTypeState()
}
