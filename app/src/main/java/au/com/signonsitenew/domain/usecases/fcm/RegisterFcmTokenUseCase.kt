package au.com.signonsitenew.domain.usecases.fcm

import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SLog
import au.com.signonsitenew.utilities.SessionManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

interface RegisterFcmTokenUseCase{
    fun setToken(fcmToken: String)
}

class RegisterFcmTokenUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager) : RegisterFcmTokenUseCase {
    override fun setToken(fcmToken: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = repository.registerFcmToken(Constants.BEARER_HEADER + sessionManager.token, sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, fcmToken, Constants.FCM_ANDROID)
                if(!response.isSuccessful)
                    SLog.i(RegisterFcmTokenUseCaseImpl::class.java.name, Constants.FCM_REGISTRATION_TOKEN_ERROR + Constants.URL_UPDATE_GCM_TOKEN)
            }catch (exception: Exception){
                SLog.i(RegisterFcmTokenUseCaseImpl::class.java.name, Constants.FCM_REGISTRATION_TOKEN_ERROR + Constants.URL_UPDATE_GCM_TOKEN)
            }
        }
    }

}