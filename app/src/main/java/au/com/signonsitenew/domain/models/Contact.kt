package au.com.signonsitenew.domain.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class Contact(var name: String?,
                   var phone_number: PhoneNumber?) : Parcelable{
    constructor() : this(null, null)
}