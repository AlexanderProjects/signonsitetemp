package au.com.signonsitenew.domain.models.briefing

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SetByUser(val first_name:String,val last_name:String,val phone_number:String?, val company:Company) : Parcelable