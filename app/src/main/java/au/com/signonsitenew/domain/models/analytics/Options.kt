package au.com.signonsitenew.domain.models.analytics


data class Options (var hasSiteContext:Boolean,
                    var hasUserCompanyId:Boolean,
                    var hasSiteOwnerCompanyId:Boolean,
                    var hasCompanyContext:Boolean,
                    var hasMetadata:Boolean)