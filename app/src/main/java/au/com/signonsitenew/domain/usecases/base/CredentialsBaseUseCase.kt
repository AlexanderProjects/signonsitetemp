package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

abstract class CredentialsBaseUseCase<P,Z,X> protected constructor() {
    val disposables = CompositeDisposable()

    abstract fun buildCreateUpdateCredentialAsyncCall() : Single<P>

    abstract fun buildDeleteCredentialAsyncCall() : Single<Z>

    abstract fun buildGetCredentialsAsyncCall() : Single<X>

    fun createUpdateCredentialAsync(asyncCall: DisposableSingleObserver<P>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildCreateUpdateCredentialAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun deleteCredentialAsync(asyncCall: DisposableSingleObserver<Z>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildDeleteCredentialAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun getCredentials(asyncCall: DisposableSingleObserver<X>){
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetCredentialsAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }

}