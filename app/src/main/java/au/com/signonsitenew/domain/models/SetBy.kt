package au.com.signonsitenew.domain.models

data class SetBy (val id:Int,val first_name:String,val last_name:String)