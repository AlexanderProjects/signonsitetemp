package au.com.signonsitenew.domain.models

data class SharePassportRequest(val email_addresses:List<String>)