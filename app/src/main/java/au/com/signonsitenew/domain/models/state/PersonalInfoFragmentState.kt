package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.UserInfoUpdateRequest

sealed class PersonalInfoFragmentState {
    data class ClearButtonClicked(val value: String): PersonalInfoFragmentState()
    object ShowSaveButton: PersonalInfoFragmentState()
    object HideSaveButton: PersonalInfoFragmentState()
    data class SaveButtonClicked(val userInfoUpdateRequest: UserInfoUpdateRequest) :PersonalInfoFragmentState()
}