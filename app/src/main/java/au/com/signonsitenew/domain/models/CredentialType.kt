package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CredentialType( var id: Int?,
                           var name: String?,
                           var identifier: String?,
                           var identifier_name: String?,
                           var front_photo: String?,
                           var back_photo: String?,
                           var issue_date: String?,
                           var expiry_date: String?,
                           var issued_by: String?,
                           var issuers: List<String>?,
                           var rto: String?,
                           var registration_number: String?,
                           var reference: String?,
                           var categories: List<String>?) : Parcelable{
    constructor() : this(null,null,null,null,null,null,null,null,null,null,null,null,null,null)
}