package au.com.signonsitenew.domain.models.adapters

data class AttendanceRegisterParentAdapterModel(val companyName:String, val total:String, val childList:List<AttendanceAdapterChildModel>, var isExpanded:Boolean = false)