package au.com.signonsitenew.domain.usecases.menu

import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.MenuBaseUseCase
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import javax.inject.Inject

class MenuUseCase@Inject constructor(private val repository: DataRepository,private val userService: UserService,  private val sessionManager: SessionManager): MenuBaseUseCase<UserInfoResponse>() {

    override fun buildGetPersonalInfoAsyncCall(): Single<UserInfoResponse> = repository
            .getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token)

}