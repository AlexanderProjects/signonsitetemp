package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.RequesteeUser

sealed class AddTeamMembersFragmentState {
    data class ShowEnrolledUsers(val userList:List<RequesteeUser>):AddTeamMembersFragmentState()
    data class UpdateTeamMemberList(val userList:List<RequesteeUser>):AddTeamMembersFragmentState()
    object ShowError:AddTeamMembersFragmentState()
}