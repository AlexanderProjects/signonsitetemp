package au.com.signonsitenew.domain.utilities

import android.text.TextUtils
import au.com.signonsitenew.domain.models.CredentialCreateUpdateRequest
import au.com.signonsitenew.domain.models.CredentialField
import au.com.signonsitenew.utilities.Constants

object Mapper {
    fun mapCredentialFieldToRequest(credentialFieldList: List<CredentialField?>, identifierName: String?): CredentialCreateUpdateRequest {
        val createRequest = CredentialCreateUpdateRequest()
        for(credentialField in credentialFieldList){
            if(credentialField?.title.equals(Constants.NAME, ignoreCase = true))createRequest.name = credentialField?.value
            if(credentialField?.title.equals(identifierName, ignoreCase = true))createRequest.identifier = credentialField?.value
            if (credentialField?.title.equals(Constants.ISSUE_DATE, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.issue_date = credentialField?.value
            if (credentialField?.title.equals(Constants.ISSUED_BY, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.issued_by = credentialField?.value
            if (credentialField?.title.equals(Constants.EXPIRY_DATE, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.expiry_date = credentialField?.value
            if (credentialField?.title.equals(Constants.RTO, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.rto = credentialField?.value
            if (credentialField?.title.equals(Constants.REFERENCE, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.reference = credentialField?.value
            if (credentialField?.title.equals(Constants.REGISTRATION_NUMBER, ignoreCase = true) && !TextUtils.isEmpty(credentialField?.value) && !credentialField?.value.equals(Constants.OPTIONAL, ignoreCase = true)) createRequest.registration_number = credentialField?.value
        }
        return createRequest
    }
}