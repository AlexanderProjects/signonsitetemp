 package au.com.signonsitenew.domain.usecases.analytics

import au.com.signonsitenew.data.factory.datasources.device.DeviceService
import au.com.signonsitenew.domain.models.CompanyPropertyType
import au.com.signonsitenew.domain.models.analytics.CompanyContextAnalyticsResponse
import au.com.signonsitenew.domain.models.analytics.Options
import au.com.signonsitenew.domain.models.analytics.UserContextAnalyticsResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.*
import com.segment.analytics.Properties
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

interface AnalyticsEventUseCase {
    fun sendTrackEvent(event:String, properties: Properties, options: Options, clearTargetedUserId:Boolean)
    fun sendScreenEvent(name:String?,properties: Properties, options: Options, clearTargetedUserId:Boolean)
    fun setTargetIdUserForContext(prop:Properties,targetedUserId:Int):MutableMap<String,Any>
    fun generateUniqueIdentifier()
    fun getUniqueIdentifier():String
    fun setSiteId(siteId:Int):AnalyticsEventUseCase
    fun mapCompanyPropertyTypeToString(company_input_type: CompanyPropertyType):String
}

class AnalyticsEventUseCaseImpl @Inject constructor (private val repository: DataRepository,
                                                     private val deviceService: DeviceService,
                                                     private val sessionManager: SessionManager) : AnalyticsEventUseCase {

    private lateinit var uuid:String
    private var siteId:Int = 0
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun sendTrackEvent(event: String, properties: Properties, options: Options, clearTargetedUserId: Boolean) {
        val prop =  buildDeviceCommonProperties(properties.toMutableMap(),sessionManager)
        sessionManager.userId?.let {
            checkForUserId()
            if (clearTargetedUserId)
                prop.remove(Constants.SEG_TARGETED_USER_ID)
            if(options.hasMetadata && prop.isNotEmpty())
                repository.sendTrackEvents(event, prop.toMap(Properties()))
            else
                buildCustomCommonPropertiesForSiteAndUser(options,prop) { repository.sendTrackEvents(event, it.toMap(Properties())) }
        }?:run {
            repository.sendTrackEvents(event, prop.toMap(Properties()))
        }
    }

    override fun sendScreenEvent(name: String?, properties: Properties, options: Options, clearTargetedUserId: Boolean) {
        val prop = buildDeviceCommonProperties(properties.toMutableMap(),sessionManager)
        sessionManager.userId?.let {
            checkForUserId()
            if(clearTargetedUserId)
                prop.remove(Constants.SEG_TARGETED_USER_ID)
            if(options.hasMetadata && prop.isNotEmpty())
                repository.sendScreenEvents(name, prop.toMap(Properties()))
            else
                buildCustomCommonPropertiesForSiteAndUser(options,prop) { repository.sendScreenEvents(name, it.toMap(Properties())) }
        }?:run{
            repository.sendScreenEvents(name, properties)
        }
    }

    override fun setTargetIdUserForContext(prop: Properties, targetedUserId: Int): MutableMap<String, Any> {
        prop[Constants.SEG_TARGETED_USER_ID] = targetedUserId
        return prop.toMutableMap()
    }

    override fun generateUniqueIdentifier(){
        uuid = UUID.randomUUID().toString()
    }
    override fun getUniqueIdentifier(): String = uuid

    override fun setSiteId(siteId: Int): AnalyticsEventUseCaseImpl {
        this.siteId = siteId
        return this
    }

    override fun mapCompanyPropertyTypeToString(company_input_type: CompanyPropertyType): String  = when(company_input_type){
        CompanyPropertyType.Custom -> Constants.CUSTOM_COMPANY_PROPERTY
        CompanyPropertyType.Auto -> Constants.AUTO_COMPANY_PROPERTY
        CompanyPropertyType.FromList -> Constants.FROM_LIST_COMPANY_PROPERTY
    }


    private fun checkForUserId(){
        if(sessionManager.userId.toLong() != repository.getSavedPreviousLikedUserId()) {
            repository.callIdentity(sessionManager.userId)
            repository.saveLinkedUserId(sessionManager.userId.toLong())
        }
    }
    private fun buildCustomCommonPropertiesForSiteAndUser(options: Options, commonProperties: MutableMap<String, Any?>, completeCallback:(properties:MutableMap<String,Any?>)->Unit) {
        if(sessionManager.userId != null) {
            getUserContext(options, commonProperties){ response ->
                when {
                    options.hasCompanyContext -> {
                        getCompanyContext(commonProperties, response.context[Constants.SEG_USER_COMPANY_ID].toString()) {
                            if (options.hasSiteContext) {
                                getSiteContext(options, commonProperties) {
                                    completeCallback(commonProperties)
                                }
                            } else {
                                completeCallback(commonProperties)
                            }
                        }
                    }
                    options.hasSiteContext -> {
                        getSiteContext(options, commonProperties) {
                            completeCallback(commonProperties)
                        }
                    }
                    else -> {
                        completeCallback(commonProperties)
                    }
                }
            }
        }else{
            completeCallback(commonProperties)
        }
    }
    private fun getUserContext(options: Options, commonProperties: MutableMap<String, Any?>, responseCallback:(UserContextAnalyticsResponse)->Unit){
        repository.getUserContextForAnalytics(Constants.ANDROID, sessionManager.userId, Constants.BEARER_HEADER + sessionManager.token)
            .subscribeOn(Schedulers.io())
            .subscribeBy (
                onSuccess = { response ->
                    if(response.status == Constants.JSON_SUCCESS){
                        commonProperties.putAll(response.context)
                        if(!options.hasUserCompanyId)
                            commonProperties.remove(Constants.SEG_USER_COMPANY_ID)
                    }else{
                        SLog.d(Constants.SEG_USER_CONTEXT_ERROR, response.status)
                    }
                    responseCallback(response)
                },onError = { error ->
                    SLog.d(Constants.SEG_USER_CONTEXT_ERROR, error.message)
                }
            ).addTo(disposables)
    }
    private fun getCompanyContext(commonProperties: MutableMap<String, Any?>, useCompanyId:String, responseCallback:(CompanyContextAnalyticsResponse)->Unit){
        repository.getCompanyContextAnalytics(Constants.ANDROID, useCompanyId, Constants.BEARER_HEADER + sessionManager.token)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = { response ->
                if(response.status == Constants.JSON_SUCCESS){
                    commonProperties.putAll(response.context)
                }else{
                    SLog.d(Constants.SEG_COMPANY_CONTEXT_ERROR, response.status)
                }
                responseCallback(response)
            }, onError = { error ->
                SLog.d(Constants.SEG_COMPANY_CONTEXT_ERROR, error.message)
            }).addTo(disposables)
    }
    private fun getSiteContext(options: Options, commonProperties: MutableMap<String, Any?>, responseCompleteCallback:()->Unit){
        val siteId = if (this.siteId == 0 && sessionManager.siteId != 0) sessionManager.siteId else this.siteId
        repository.getSiteContextForAnalytics(Constants.ANDROID, siteId.toString(), Constants.BEARER_HEADER + sessionManager.token)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onSuccess = { response ->
                if(response.status == Constants.JSON_SUCCESS){
                    commonProperties.putAll(response.context)
                    if(!options.hasSiteOwnerCompanyId)
                        commonProperties.remove(Constants.SEG_COMPANY_ID)
                }else{
                    SLog.d(Constants.SEG_SITE_CONTEXT_ERROR, response.status)
                }
                responseCompleteCallback()
            }, onError = { error ->
                SLog.d(Constants.SEG_SITE_CONTEXT_ERROR, error.message)
            }).addTo(disposables)

    }


    private fun buildDeviceCommonProperties(contextValues: MutableMap<String, Any?>, sessionManager: SessionManager): MutableMap<String, Any?> {
        contextValues[Constants.DEVICE_APP] = Constants.MOBILE
        contextValues[Constants.DEVICE_AUTO_SIGN_ON] = sessionManager.autoSignOnEnabled
        contextValues[Constants.DEVICE_BACKGROUND_APP_REFRESH] = deviceService.getBackgroundAppRefresh()
        contextValues[Constants.DEVICE_BATTERY_LEVEL] = deviceService.getCurrentBatteryLevel()
        contextValues[Constants.DEVICE_LOCATION_ENABLE] = deviceService.isLocationEnable()
        contextValues[Constants.DEVICE_LOCATION_PERMISSION] = deviceService.hasLocationPermission()
        contextValues[Constants.DEVICE_PUSH_NOTIFICATION_PERMISSION] = deviceService.getPushNotificationPermission()
        contextValues[Constants.DEVICE_REACHABILITY] = deviceService.getReachability()
        contextValues[Constants.DEVICE_WIFI_ENABLE] = deviceService.hasWifiEnable()
        contextValues[Constants.DEVICE_PUSH_TOKEN] = deviceService.getPushNotificationPermissionToken()
        contextValues[Constants.DEVICE_IGNORING_BATTERY_OPTIMISATION] = deviceService.isIgnoringBatteryOptimization()
        contextValues[Constants.DEVICE_LOW_POWER_MODE] = deviceService.hasPowerSaveMode()
        return contextValues
    }

}