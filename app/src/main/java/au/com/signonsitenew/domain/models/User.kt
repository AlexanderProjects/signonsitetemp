package au.com.signonsitenew.domain.models

import android.os.Parcelable
import au.com.signonsitenew.domain.models.Contact
import au.com.signonsitenew.domain.models.Gender
import au.com.signonsitenew.domain.models.PhoneNumber
import kotlinx.android.parcel.Parcelize
import java.util.*
@Parcelize
data class User(var id: Int?,
                var has_passport: Boolean?,
                var first_name: String?,
                var last_name: String?,
                var phone_number: PhoneNumber?,
                var company_name: String?,
                var date_of_birth: Date?,
                var is_indigenous: Boolean?,
                var is_apprentice: Boolean?,
                var is_experienced: Boolean?,
                var is_english_native: Boolean?,
                var is_interpreter_required:Boolean?,
                var trade: String?,
                var email: String?,
                var gender: Gender?,
                var post_code: String?,
                var medical_information: String?,
                var primary_contact: Contact?,
                var secondary_contact: Contact?,
                var headshot_photo:String?,
                var headshot_photo_temporary_url:String?) : Parcelable {
    constructor() : this(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
}