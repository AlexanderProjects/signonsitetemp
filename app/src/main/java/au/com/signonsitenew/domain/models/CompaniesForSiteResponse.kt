package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompaniesForSiteResponse (val companies:List<Company>,
                                     val has_signed_on_to_site_previously:Boolean,
                                     val has_locked_enrolment_for_site:Boolean,
                                     override val status:String) : Parcelable, ApiResponse(status)