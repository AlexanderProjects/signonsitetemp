package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NearSite (val id:Int,
                     val name: String,
                     val address: String,
                     val principal_company_name: String,
                     val is_connected: Boolean) : Parcelable