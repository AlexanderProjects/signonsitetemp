package au.com.signonsitenew.domain.usecases.connections

import au.com.signonsitenew.domain.models.EnrolmentUpdateResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject


interface UpdateEnrolmentUseCase {
    fun updateEnrolment(enrolmentId:String): Single<EnrolmentUpdateResponse>
}

class UpdateEnrolmentUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager) :UpdateEnrolmentUseCase {
    override fun updateEnrolment(enrolmentId:String): Single<EnrolmentUpdateResponse> =
            repository.updateEnrolments(enrolmentId,isAutomaticAttendanceEnabled = "false",token = Constants.BEARER_HEADER + sessionManager.token)
                    .observeOn(AndroidSchedulers.mainThread())
}