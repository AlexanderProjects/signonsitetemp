package au.com.signonsitenew.domain.models.attendees

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Attendee (val id:Int,
                     val first_name:String?,
                     val last_name:String?,
                     val phone_number:String?,
                     val has_active_enrolment:Boolean,
                     val needs_to_do_site_induction:Boolean,
                     val site_induction: SiteInduction?,
                     val attendances:List<Attendance>,
                     val worker_notes:List<WorkerNotes>?)