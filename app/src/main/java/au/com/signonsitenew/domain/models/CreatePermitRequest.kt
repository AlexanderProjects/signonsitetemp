package au.com.signonsitenew.domain.models

data class CreatePermitRequest(val permit_template_id:Int,
                               val requestee_user_ids:List<Int>,
                               val requestee_states:RequesteeStates,
                               val start_date:String?,
                               val end_date:String?)