package au.com.signonsitenew.domain.models

import au.com.signonsitenew.utilities.Util

data class UserInfoUpdateRequest (
        var has_passport: Boolean?,
        var first_name: String?,
        var last_name: String?,
        var phone_number: PhoneNumber?,
        var company_name: String?,
        var trade: String?,
        @Util.SerializeNulls var date_of_birth: String?,
        @Util.SerializeNulls var is_indigenous: Boolean?,
        @Util.SerializeNulls var is_apprentice: Boolean?,
        @Util.SerializeNulls var is_experienced: Boolean?,
        @Util.SerializeNulls var is_english_native: Boolean?,
        @Util.SerializeNulls var is_interpreter_required: Boolean?,
        @Util.SerializeNulls var gender: Gender?,
        @Util.SerializeNulls var post_code: String?,
        @Util.SerializeNulls var medical_information: String?,
        var primary_contact: Contact?,
        @Util.SerializeNulls var secondary_contact: Contact?,
        var headshot_photo: String?){constructor() : this(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)}