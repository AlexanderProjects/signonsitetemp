package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.domain.models.UpdatePermitInfoRequest
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class PermitTeamTabBaseUseCase<T, P, Z, Q> protected constructor() {

    abstract fun buildGetPermitInfoAsyncCall(permitId:String): Single<T>

    abstract fun buildGetEnrolledUsersAsyncCall():Single<P>

    abstract fun buildSetTeamMemberStatusAsyncCall(userId: String, status: String):Single<Z>

    abstract fun buildUpdateMemberListForAPermitAsyncCall(permitId: String, updatePermitInfoRequest: UpdatePermitInfoRequest):Single<Q>

    fun getPermitInfoAsync(permitId: String): Single<T> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildGetPermitInfoAsyncCall(permitId)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getEnrolledUsersAsync():Single<P>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildGetEnrolledUsersAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun setTeamMemberStatusForAPermitAsync(userId: String, status:String):Single<Z>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildSetTeamMemberStatusAsyncCall(userId,status)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }
    fun updateMemberListForAPermitAsync(permitId: String, updatePermitInfoRequest: UpdatePermitInfoRequest):Single<Q>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildUpdateMemberListForAPermitAsyncCall(permitId, updatePermitInfoRequest)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }
}