package au.com.signonsitenew.domain.models

data class RetrieveImageResponse(val access_url:String,
                                 val content_type:String,
                                 val fs_name:String,
                                 val status:String)