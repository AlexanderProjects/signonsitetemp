package au.com.signonsitenew.domain.models

sealed class PermitTabsContentType{
    data class PlainText(val plainTextList:List<String>):PermitTabsContentType()
    object HidePlainText:PermitTabsContentType()
}
