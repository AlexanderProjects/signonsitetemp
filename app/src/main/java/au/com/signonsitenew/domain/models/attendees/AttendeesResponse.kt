package au.com.signonsitenew.domain.models.attendees

import au.com.signonsitenew.domain.models.ApiResponse
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AttendeesResponse (
        val attendees:List<Attendee>,
        override val status:String): ApiResponse(status)