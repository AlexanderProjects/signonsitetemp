package au.com.signonsitenew.domain.utilities

import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.models.CredentialCreateUpdateRequest
import au.com.signonsitenew.domain.models.CredentialField
import au.com.signonsitenew.domain.models.CredentialType
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.domain.utilities.CredentialTypeValidator.setCredentialType
import au.com.signonsitenew.domain.utilities.CredentialValidator.setCredential
import java.util.*

object Builder {


    fun buildFieldsFromCredentialType(credentialType: CredentialType): List<CredentialField> {
        val credentialTypeList: MutableList<CredentialField> = ArrayList()
        setCredentialType(credentialType)
        if (credentialType.name.equals(Constants.CUSTOM_LICENSE, ignoreCase = true)) {
            val credentialField = CredentialField()
            credentialField.title = Constants.NAME
            credentialField.value = Constants.REQUIRED_FIELD
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateIdentifier()?.let {
            val credentialField = CredentialField()
            credentialField.title = credentialType.identifier_name
            credentialField.value = credentialType.identifier
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialIssueDate()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.ISSUE_DATE
            credentialField.value = credentialType.issue_date
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialExpiryDate()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.EXPIRY_DATE
            credentialField.value = credentialType.expiry_date
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialIssueBy()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.ISSUED_BY
            credentialField.value = CredentialTypeValidator.validateCredentialIssueBy()
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialRto()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.RTO
            credentialField.value = credentialType.rto
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialReferenceNumber()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.REGISTRATION_NUMBER
            credentialField.value = credentialType.registration_number
            credentialTypeList.add(credentialField)
        }
        CredentialTypeValidator.validateCredentialReference()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.REFERENCE
            credentialField.value = credentialType.reference
            credentialTypeList.add(credentialField)
        }
        return credentialTypeList
    }

    fun buildFieldsFromCredential(credential: Credential): List<CredentialField> {
        val credentialList: MutableList<CredentialField> = ArrayList()
        setCredential(credential)
        CredentialValidator.validateIdentifier()?.let {
            val credentialField = CredentialField()
            if (credential.credential_type != null) credentialField.title = credential.credential_type!!.identifier_name else credentialField.title = Constants.ID_NUMBER
            credentialField.value = CredentialValidator.validateIdentifier()
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialExpiryDate()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.EXPIRY_DATE
            credentialField.value = CredentialValidator.validateCredentialExpiryDate()
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialIssueDate()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.ISSUE_DATE
            credentialField.value = credential.issue_date
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialIssueBy()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.ISSUED_BY
            credentialField.value = CredentialValidator.validateCredentialIssueBy()
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialRto()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.RTO
            credentialField.value = CredentialValidator.validateCredentialRto()
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialRegistrationNumber()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.REGISTRATION_NUMBER
            credentialField.value = CredentialValidator.validateCredentialRegistrationNumber()
            credentialList.add(credentialField)
        }
        CredentialValidator.validateCredentialReference()?.let {
            val credentialField = CredentialField()
            credentialField.title = Constants.REFERENCE
            credentialField.value = CredentialValidator.validateCredentialReference()
            credentialList.add(credentialField)
        }
        return credentialList
    }

    fun customLicense(credentials: MutableList<CredentialType>): List<CredentialType> {
        val credential = CredentialType()
        credential.name = Constants.CUSTOM_LICENSE
        credential.identifier = Constants.OPTIONAL
        credential.identifier_name = Constants.ID_NUMBER
        credential.front_photo = Constants.OPTIONAL
        credential.back_photo = Constants.OPTIONAL
        credential.issue_date = Constants.OPTIONAL
        credential.expiry_date = Constants.OPTIONAL
        credential.issued_by = Constants.OPTIONAL
        credential.rto = Constants.OPTIONAL
        credential.registration_number = Constants.OPTIONAL
        credential.reference = Constants.OPTIONAL
        credentials.add(credential)
        return credentials
    }

    fun buildMissingFieldsForUpdateRequest(request: CredentialCreateUpdateRequest, credential: Credential): CredentialCreateUpdateRequest {
        if (credential.credential_type != null) {
            request.credential_type_id = credential.credential_type!!.id
        } else {
            request.credential_type_id = null
            request.name = credential.name
        }
        if (credential.back_photo != null) {
            request.back_photo = credential.back_photo
        }
        if (credential.front_photo != null) {
            request.front_photo = credential.front_photo
        }
        request.edits_credential_id = credential.id
        return request
    }

    fun addNotSelectedOption(issuers: MutableList<String>): List<String> {
        if(issuers[0] == Constants.NOT_SELECTED)
            return issuers
        else
            issuers.add(0, Constants.NOT_SELECTED)
        return issuers
    }
}