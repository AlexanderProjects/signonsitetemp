package au.com.signonsitenew.domain.models

data class EmailVerificationResponse (var msg:String, var in_use:Boolean, var email_status:String, var status:String)