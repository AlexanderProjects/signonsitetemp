package au.com.signonsitenew.domain.usecases.internet

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

interface CheckForInternetConnectionUseCase {
    fun isInternetOn(): Flowable<Boolean>
    fun isInternetConnected(): Single<Boolean>
}

class CheckForInternetConnectionUseCaseImpl @Inject constructor(private val context: Context): CheckForInternetConnectionUseCase {
    var result = false
    override fun isInternetOn(): Flowable<Boolean>{
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: Flowable.just(false)
            val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities as Network) ?: Flowable.just(false)
            if(actNw is NetworkCapabilities) {
                result = when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        return Flowable.just(result)
    }

    override fun isInternetConnected(): Single<Boolean>{
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: Single.create<Boolean> { emitter -> emitter.onSuccess(false)  }
            val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities as Network) ?: Single.create<Boolean> { emitter -> emitter.onSuccess(false)  }
            if(actNw is NetworkCapabilities) {
                result = when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        return Single.create{ emitter -> emitter.onSuccess(result ) }
    }
}