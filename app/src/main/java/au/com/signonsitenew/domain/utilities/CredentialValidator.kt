package au.com.signonsitenew.domain.utilities

import android.text.TextUtils
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.models.CredentialField
import au.com.signonsitenew.utilities.Constants
import java.io.File

object CredentialValidator {

    private var credential: Credential? = null

    fun validateCredentialName(): String? {
        if (credential!!.credential_type != null) {
            return credential!!.credential_type!!.name
        } else if (credential!!.name != null) {
            return credential!!.name
        }
        return null
    }

    fun validateIdentifierName(): String? {
        return if (credential!!.credential_type != null) {
            credential!!.credential_type!!.identifier_name
        } else {
            Constants.ID_NUMBER
        }
    }

    @JvmStatic
    fun validateIdentifier(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.identifier != null) {
                if (credential!!.credential_type!!.identifier.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.identifier.equals(Constants.OPTIONAL, ignoreCase = true)) return credential!!.identifier
                if (credential!!.credential_type!!.identifier.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            }
        } else if (credential!!.identifier != null) {
            return credential!!.identifier
        }
        return null
    }

    fun validateFrontPhoto(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.front_photo != null) {
                if (credential!!.credential_type!!.front_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.front_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return credential!!.front_photo
                if (credential!!.credential_type!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            }
        } else if (credential!!.front_photo != null) {
            return credential!!.front_photo
        }
        return null
    }

    fun validateBackPhoto(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type != null) {
                if (credential!!.credential_type!!.back_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.back_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return credential!!.back_photo
                if (credential!!.credential_type!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
            }
        } else if (credential!!.back_photo != null) {
            return credential!!.back_photo
        }
        return null
    }

    @JvmStatic
    fun validateCredentialIssueDate(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.issue_date.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.issue_date.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.issue_date != null) credential!!.issue_date else null
            }
            if (credential!!.credential_type!!.issue_date.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
        } else if (credential!!.issue_date != null) {
            return credential!!.issue_date
        }
        return null
    }

    @JvmStatic
    fun validateCredentialIssueBy(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.issued_by.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.issued_by.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.issued_by != null) credential!!.issued_by else null
            }
            if (credential!!.credential_type!!.issued_by.equals(Constants.NONE_FIELD, ignoreCase = true)) {
                return null
            }
        } else if (credential!!.issued_by != null) {
            return credential!!.issued_by
        }
        return null
    }

    @JvmStatic
    fun validateCredentialExpiryDate(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.expiry_date.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) {
                return if (credential!!.expiry_date != null) credential!!.expiry_date else null
            }
            if (credential!!.credential_type!!.expiry_date.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.expiry_date != null) credential!!.expiry_date else null
            }
            if (credential!!.credential_type!!.expiry_date.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
        } else if (credential!!.expiry_date != null) {
            return credential!!.expiry_date
        }
        return null
    }

    @JvmStatic
    fun validateCredentialRto(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.rto.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.rto.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.rto != null) credential!!.rto else null
            }
            if (credential!!.credential_type!!.rto.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
        } else if (credential!!.rto != null) {
            return credential!!.rto
        }
        return null
    }

    @JvmStatic
    fun validateCredentialRegistrationNumber(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.registration_number.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.registration_number.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.registration_number != null) credential!!.registration_number else null
            }
            if (credential!!.credential_type!!.registration_number.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
        } else if (credential!!.registration_number != null) {
            return credential!!.registration_number
        }
        return null
    }

    @JvmStatic
    fun validateCredentialReference(): String? {
        if (credential!!.credential_type != null) {
            if (credential!!.credential_type!!.reference.equals(Constants.REQUIRED_FIELD, ignoreCase = true) || credential!!.credential_type!!.reference.equals(Constants.OPTIONAL, ignoreCase = true)) {
                return if (credential!!.reference != null) credential!!.reference else null
            }
            if (credential!!.credential_type!!.reference.equals(Constants.NONE_FIELD, ignoreCase = true)) return null
        } else if (credential!!.reference != null) {
            return credential!!.reference
        }
        return null
    }

    @JvmStatic
    fun checkedForValidSpinnerOption(): Boolean{
        return if(credential!!.credential_type != null)
            credential!!.credential_type!!.issuers != null
        else
            false

    }

    private fun isValidFrontPhoto(photo: File?): Boolean {
        if (photo != null) {
            if (credential!!.credential_type != null) {
                if (credential!!.credential_type!!.front_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return true
                if (credential!!.credential_type!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return false
                if (credential!!.credential_type!!.front_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
            } else {
                return true
            }
        } else if (credential!!.front_photo != null) {
            if (credential!!.credential_type != null) {
                if (credential!!.credential_type!!.front_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return true
                if (credential!!.credential_type!!.front_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return false
                if (credential!!.credential_type!!.front_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
            } else {
                return true
            }
        }
        return false
    }

    private fun isValidBackPhoto(photo: File?): Boolean {
        if (photo != null) {
            if (credential!!.credential_type != null) {
                if (credential!!.credential_type!!.back_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true) && photo != null) return true
                if (credential!!.credential_type!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true) && photo != null) return false
                if (credential!!.credential_type!!.back_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
            } else {
                return true
            }
        } else if (credential!!.back_photo != null) {
            if (credential!!.credential_type != null) {
                if (credential!!.credential_type!!.back_photo.equals(Constants.REQUIRED_FIELD, ignoreCase = true)) return true
                if (credential!!.credential_type!!.back_photo.equals(Constants.NONE_FIELD, ignoreCase = true)) return false
                if (credential!!.credential_type!!.back_photo.equals(Constants.OPTIONAL, ignoreCase = true)) return true
            } else {
                return true
            }
        }
        return false
    }


    @JvmStatic
    fun setCredential(credential: Credential?) {
        CredentialValidator.credential = credential
    }
}