package au.com.signonsitenew.domain.models

enum class SignOnOffType {
    Auto,
    Manual
}