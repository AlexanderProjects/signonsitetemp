package au.com.signonsitenew.domain.models

data class DeleteCredentialResponse(override val status:String): ApiResponse(status)