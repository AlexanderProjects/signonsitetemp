package au.com.signonsitenew.domain.usecases.connections

import android.annotation.SuppressLint
import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.domain.models.EnrolmentResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.ConnectionListBaseUseCase
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

interface ConnectionListUseCase{
    fun filterEnrolments(enrolments: List<Enrolment>):List<Enrolment>
    fun convertTimeZoneDate(enrolment: Enrolment):String
}

class ConnectionListUseCaseImpl @Inject constructor(private val repository: DataRepository,
                                                    private val userService: UserService,
                                                    private val sessionManager: SessionManager) : ConnectionListBaseUseCase<EnrolmentResponse>(), ConnectionListUseCase {


    override fun filterEnrolments(enrolments: List<Enrolment>):List<Enrolment> =
            enrolments.filter { enrolment -> enrolment.is_automatic_attendance_enabled}
                      .filter { enrolment -> !enrolment.is_hidden }


    @SuppressLint("SimpleDateFormat")
    override fun convertTimeZoneDate(enrolment: Enrolment):String{
        val dateFormat = SimpleDateFormat("dd MMM yyyy")
        val date = setTimeZoneDate(enrolment)
        return dateFormat.format(date)
    }

    private fun setTimeZoneDate(enrolment: Enrolment):Date{
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ",Locale.getDefault())
        val stringDate = formatter.format(enrolment.utc_last_signon_at)
        if(!enrolment.site.timezone.isNullOrBlank()) {
            formatter.timeZone = TimeZone.getTimeZone(enrolment.site.timezone)
        }
        return formatter.parse(stringDate)
    }

    override fun buildGetEnrollmentsAsyncCall(): Single<EnrolmentResponse> {
        val userId = userService.currentUserId.toString()
        val token = Constants.BEARER_HEADER + sessionManager.token
        return repository.getEnrolments(userId,token)
    }

}