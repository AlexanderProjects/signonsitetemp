package au.com.signonsitenew.domain.models

enum class CompanyPropertyType {
    Auto,
    Custom,
    FromList,
}