package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber

abstract class PassportEmergencyBaseUseCase <T,P> protected constructor(){

    abstract fun buildUpdateAndGetPersonalInfoAsyncCall() : Flowable<P>

    abstract fun buildGetPersonalInfoAsyncCall() : Single<T>

    fun getPersonalInfoAsync() : Single<T> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
         return buildGetPersonalInfoAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun updateAndGetPersonalInfoAsync(): Flowable<P> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return buildUpdateAndGetPersonalInfoAsyncCall()
                .doOnComplete { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }

}