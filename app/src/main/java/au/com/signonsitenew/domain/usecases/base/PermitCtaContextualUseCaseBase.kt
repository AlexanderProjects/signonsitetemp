package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.domain.models.ComponentSaveResponsesRequest
import au.com.signonsitenew.domain.models.UpdatePermitInfoRequest
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class PermitCtaContextualUseCaseBase <T, Z, Q, P> protected constructor() {

    abstract fun buildUpdatePermitAsyncCall(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<Z>

    abstract fun buildMarkPermitAsDoneAsyncCall(userId: String, status: String):Single<Z>

    abstract fun buildGetPermitInfoAsyncCall(permitId:String): Single<Q>

    abstract fun buildSavePermitComponentTypeResponseCall(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId: String):Single<P>


    fun updatePermitAsync(updatePermitRequest: UpdatePermitInfoRequest, permitId: String):Single<Z>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildUpdatePermitAsyncCall(updatePermitRequest, permitId)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getPermitInfoAsync(permitId: String): Single<Q> {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildGetPermitInfoAsyncCall(permitId)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun markPermitAsDoneAsync(userId: String, status:String):Single<Z>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildMarkPermitAsDoneAsyncCall(userId,status)
            .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
            .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun savePermitComponentTypeResponseAsync(componentSaveResponsesRequest: ComponentSaveResponsesRequest,permitId: String):Single<P>{
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        return  buildSavePermitComponentTypeResponseCall(componentSaveResponsesRequest, permitId)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
    }


}