package au.com.signonsitenew.domain.models

data class EnrolmentUpdateResponse(override val status: String): ApiResponse(status)