package au.com.signonsitenew.domain.models

import au.com.signonsitenew.domain.models.Contact
import au.com.signonsitenew.domain.models.Gender
import au.com.signonsitenew.domain.models.PhoneNumber
import java.util.*

data class UserInfoRequest(var first_name: String?,
                           var last_name: String?,
                           var phone_number: PhoneNumber,
                           var company_name: String?,
                           var date_of_birth: Date?,
                           var is_indigenous: Boolean?,
                           var is_apprentice: Boolean?,
                           var trade: String?,
                           var gender: Gender?,
                           var post_code: String?,
                           var medical_information: String?,
                           var contact: Contact?,
                           var secondary_contact: Contact?)