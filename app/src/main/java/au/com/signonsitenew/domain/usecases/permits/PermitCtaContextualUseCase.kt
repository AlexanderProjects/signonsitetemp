package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.CtaContextualButtonFragmentState
import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PermitCtaContextualUseCaseBase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.utilities.*
import io.reactivex.Single
import org.joda.time.format.ISODateTimeFormat
import javax.inject.Inject


interface PermitCtaContextualUseCase {
    fun mapPermitStateForCta(permitInfo: PermitInfo):CtaContextualButtonFragmentState
    fun mapForNewPermitStateForCta(value: Boolean, permitInfo: PermitInfo):CtaContextualButtonFragmentState
    fun retrievePermitInfoAsync(permitId: String):Single<PermitInfoResponse>
    fun updateCurrentPermitAsync(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<ApiResponse>
    fun savePermitContentComponentsAsync(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId:String):Single<ApiResponse>
    fun buildUpdatePermitRequest(permitInfo: PermitInfo, isRejectedPermit:Boolean):UpdatePermitInfoRequest
    fun setPermitInfo(permitInfo: PermitInfo)
    fun retrievePermitInfo():PermitInfo
    fun setOriginalPermitInfo(permitInfo: PermitInfo)
    fun getOriginalPermitInfo():PermitInfo
    fun setListOfMembers(listOfTeamMembers:List<RequesteeUser>?)
    fun setPermitChecksContentTypeState(permitContentTypeState: PermitContentTypeState)
    fun retrieveListOfMembers():List<RequesteeUser>
    fun markCurrentPermitAsDoneAsync(doneState: String):Single<ApiResponse>
    fun clearListOfMembersInCta()
    fun clearListOfComponents()
    fun setComponentTypeForm(componentTypeForm: ComponentTypeForm)
    fun getComponentTypeFormList():List<ComponentTypeForm>
    fun buildComponentTypeRespondRequest():ComponentSaveResponsesRequest
    fun checkForEmptyMandatoryFields()
    fun hasPermitsAndContentTypeSameResponses():Boolean
}
class PermitCtaContextualUseCaseImpl @Inject constructor(private val sessionManager: SessionManager, private val repository: DataRepository):PermitCtaContextualUseCase, PermitCtaContextualUseCaseBase<SavePermitResponse, ApiResponse, PermitInfoResponse, ApiResponse>() {

    private val isManager:Boolean get() = repository.hasManagerPermission(sessionManager.userId.toLong(), sessionManager.siteId.toLong())
    private lateinit var permitInfo:PermitInfo
    private lateinit var originalPermitInfo: PermitInfo
    private lateinit var listOfMembers : List<RequesteeUser>
    private lateinit var contentTypeState: PermitContentTypeState
    private var componentTypeFormList = mutableListOf<ComponentTypeForm>()
    private val checkForEmptyResponsesList = mutableListOf<Boolean>()

    override fun mapPermitStateForCta(permitInfo: PermitInfo):CtaContextualButtonFragmentState {
        val isRequester =  permitInfo.requester_user.id == sessionManager.userId.toInt()
        when{
            permitInfo.state == PermitState.Request && isRequester -> {
                when {
                    checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo, PermitDoneState.FinishedRequest) -> {
                        return when {
                            hasUnSavedTeamMembers(listOfMembers, permitInfo) -> {
                                when {
                                    hasPermitStartDateTimeAndEndDateTime(permitInfo) && !hasEmptyMandatoryInputFields() -> {
                                        CtaContextualButtonFragmentState.ShowSendToTeamButtonWithWarningMessage
                                    }
                                    else -> {
                                        CtaContextualButtonFragmentState.ShowSendToTeamDisableButton
                                    }
                                }
                            }
                            else -> {
                                when {
                                    hasPermitStartDateTimeAndEndDateTime(permitInfo) && !hasEmptyMandatoryInputFields() -> {
                                        CtaContextualButtonFragmentState.ShowObtainApprovalButton
                                    }
                                    else -> {
                                        CtaContextualButtonFragmentState.ShowObtainApprovalDisableButton
                                    }
                                }
                            }
                        }
                    }
                    else -> {
                        return when {
                            hasUnSavedTeamMembers(listOfMembers, permitInfo) -> {
                                when {
                                    hasPermitStartDateTimeAndEndDateTime(permitInfo) && !hasEmptyMandatoryInputFields() -> {
                                        CtaContextualButtonFragmentState.ShowSendToTeamButton
                                    }
                                    else -> {
                                        CtaContextualButtonFragmentState.ShowSendToTeamDisableButton
                                    }
                                }
                            }
                            else -> {
                                when {
                                    hasPermitStartDateTimeAndEndDateTime(permitInfo) && !hasEmptyMandatoryInputFields() -> {
                                        CtaContextualButtonFragmentState.ShowObtainApprovalButton
                                    }
                                    else -> {
                                        CtaContextualButtonFragmentState.ShowObtainApprovalDisableButton
                                    }
                                }
                            }
                        }
                    }
                }
            }
            permitInfo.state == PermitState.Request && !isPermitInDoneFinishedRequestState() && !hasEmptyMandatoryInputFields()-> {
                return CtaContextualButtonFragmentState.ShowDoneInRequestButton
            }
            permitInfo.state == PermitState.PendingApproval && isManager && !isPermitInDoneFinishedRequestState() ->{
                return CtaContextualButtonFragmentState.ShowApproveRejectButton
            }
            permitInfo.state == PermitState.PendingApproval && !isPermitInDoneFinishedRequestState() ->{
                return CtaContextualButtonFragmentState.ShowApproveRejectDisableButton
            }
            permitInfo.state == PermitState.InProgress && isRequester && checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo, PermitDoneState.FinishedWork) && !hasEmptyMandatoryInputFields() ->{
                return CtaContextualButtonFragmentState.ShowSendForClosureButton
            }
            permitInfo.state == PermitState.InProgress && isRequester && (!checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo, PermitDoneState.FinishedWork) || hasEmptyMandatoryInputFields()) ->{
                return CtaContextualButtonFragmentState.ShowSendForClosureDisableButton
            }
            permitInfo.state == PermitState.InProgress && isPermitStartDateBeforeNow(permitInfo) && !isPermitInDoneFinishedWorkState() && !hasEmptyMandatoryInputFields() -> {
                return CtaContextualButtonFragmentState.ShowDoneInProgressButton
            }
            permitInfo.state == PermitState.PendingClosure && isManager && !hasEmptyMandatoryInputFields()->   {
                return CtaContextualButtonFragmentState.ShowApproveRejectClosureButton
            }
            permitInfo.state == PermitState.PendingClosure && isManager && hasEmptyMandatoryInputFields()->   {
                return CtaContextualButtonFragmentState.ShowObtainApprovalDisableButton
            }

        }
        return CtaContextualButtonFragmentState.HideCtaButton
    }


    override fun mapForNewPermitStateForCta(value: Boolean, permitInfo: PermitInfo): CtaContextualButtonFragmentState {
        return if(value && permitInfo.start_date != null && permitInfo.end_date != null && !hasEmptyMandatoryInputFields())
            CtaContextualButtonFragmentState.ShowSendToTeamButton
        else
            CtaContextualButtonFragmentState.ShowSendToTeamDisableButton
    }

    private fun isPermitStartDateBeforeNow(permitInfo: PermitInfo): Boolean {
        return if(permitInfo.start_date != null) {
            ISODateTimeFormat.dateTimeParser().parseDateTime(permitInfo.start_date).isBeforeNow
        }else {
            false
        }
    }

    private fun hasPermitStartDateTimeAndEndDateTime(permitInfo: PermitInfo):Boolean{
        return permitInfo.start_date != null && permitInfo.end_date != null
    }

    private fun isPermitInDoneFinishedWorkState():Boolean{
        val currentUser = permitInfo.requestee_users.find { it.id == sessionManager.userId.toInt() }
        return if (currentUser != null) {
            currentUser.status == PermitDoneState.FinishedWork
        }else {
            false
        }
    }

    private fun isPermitInDoneFinishedRequestState():Boolean{
        val currentUser = permitInfo.requestee_users.find { it.id == sessionManager.userId.toInt() }
        return if (currentUser != null) {
            currentUser.status == PermitDoneState.FinishedRequest
        }else {
            false
        }
    }

    private fun hasUnSavedTeamMembers(listOfMembers: List<RequesteeUser>, permitInfo: PermitInfo):Boolean{
        return when {
            listOfMembers.isEmpty() && permitInfo.requestee_users.isEmpty() -> false
            isEqual(permitInfo.requestee_users,listOfMembers)  -> false
            permitInfo.requestee_users.isEmpty() && listOfMembers.isNotEmpty() -> true
            else -> false
        }
    }

    private fun hasEmptyMandatoryInputFields():Boolean{
        val mandatoryList = getPermitFilteredByMandatoryFieldsList()
        return when{
            checkForEmptyResponsesList.find { it } == true -> true
            checkForEmptyResponsesList.isEmpty() && mandatoryList.isEmpty() -> false
            else -> !checkForEmptyResponsesList.any { !it }
        }
    }

    override fun retrievePermitInfoAsync(permitId: String): Single<PermitInfoResponse> = getPermitInfoAsync(permitId)
    override fun updateCurrentPermitAsync(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<ApiResponse> = updatePermitAsync(updatePermitRequest,permitId)
    override fun savePermitContentComponentsAsync(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId: String): Single<ApiResponse> = savePermitComponentTypeResponseAsync(componentSaveResponsesRequest, permitId)
    override fun buildUpdatePermitRequest(permitInfo: PermitInfo, isRejectedPermit: Boolean): UpdatePermitInfoRequest {
        return if((originalPermitInfo.state == PermitState.Request ) && !isRejectedPermit) {
            UpdatePermitInfoRequest(mapPermitStateToString(permitInfo.state), listOfMembers.map { it.id },permitInfo.start_date,permitInfo.end_date)
        } else
            UpdatePermitInfoRequest(mapPermitStateToString(permitInfo.state), null,null,null)
    }

    private fun mapPermitStateToString(permitState: PermitState):String{
        return when(permitState){
            PermitState.Request -> Constants.PERMIT_REQUEST_STATE
            PermitState.PendingApproval -> Constants.PERMIT_PENDING_APPROVAL_STATE
            PermitState.InProgress -> Constants.PERMIT_IN_PROGRESS_STATE
            PermitState.PendingClosure -> Constants.PERMIT_PENDING_CLOSURE_STATE
            PermitState.Closed -> Constants.PERMIT_CLOSED_STATE
            PermitState.RequesterCancelled -> Constants.PERMIT_REQUESTER_CANCELLED_STATE
            PermitState.ApproverCancelled -> Constants.PERMIT_APPROVER_CANCELLED_STATE
            else -> Constants.PERMIT_SUSPENDED_STATE
        }
    }

    override fun setPermitInfo(permitInfo: PermitInfo) {
        this.permitInfo = permitInfo
    }

    override fun retrievePermitInfo(): PermitInfo {
        return this.permitInfo
    }

    override fun setOriginalPermitInfo(permitInfo: PermitInfo) {
        this.originalPermitInfo = permitInfo
    }

    override fun getOriginalPermitInfo(): PermitInfo {
        return this.originalPermitInfo
    }

    override fun setListOfMembers(listOfTeamMembers: List<RequesteeUser>?) {
        listOfTeamMembers?.let {
            if(listOfTeamMembers.isNotEmpty())
                listOfMembers = it
        }
    }

    override fun setPermitChecksContentTypeState(permitContentTypeState: PermitContentTypeState) {
        this.contentTypeState = permitContentTypeState
    }

    override fun retrieveListOfMembers(): List<RequesteeUser> {
        listOfMembers = listOfMembers.distinct()
        return listOfMembers
    }

    override fun markCurrentPermitAsDoneAsync(doneState: String) : Single<ApiResponse>  = markPermitAsDoneAsync(sessionManager.userId, doneState)
    override fun clearListOfMembersInCta() {
        listOfMembers = mutableListOf()
    }

    override fun clearListOfComponents() {
        componentTypeFormList.clear()
    }

    override fun setComponentTypeForm(componentTypeForm: ComponentTypeForm) {
        val component = componentTypeFormList.find { it.form_input_id == componentTypeForm.form_input_id  }
        if(component != null) component.value = componentTypeForm.value
        componentTypeFormList.add(componentTypeForm)

    }

    override fun getComponentTypeFormList(): List<ComponentTypeForm> {
        return componentTypeFormList.distinct()
    }

    override fun buildComponentTypeRespondRequest(): ComponentSaveResponsesRequest {
        return ComponentSaveResponsesRequest(componentTypeFormList)
    }

    override fun checkForEmptyMandatoryFields() {
        val mandatoryList = getPermitFilteredByMandatoryFieldsList()
        val filteredMandatoryFields = mandatoryList.filterMandatoryFieldsById(componentTypeFormList)
        if(isEqual(filteredMandatoryFields,mandatoryList)) {
            checkForEmptyResponsesList.clear()
            checkForEmptyResponsesList.add(false)
        } else {
            checkForEmptyResponsesList.add(true)
        }
        if (mandatoryList.isEmpty()){
            checkForEmptyResponsesList.clear()
            checkForEmptyResponsesList.add(false)
        }
    }

    override fun hasPermitsAndContentTypeSameResponses(): Boolean {
        val mandatoryList = getPermitFilteredByMandatoryFieldsList().distinct()
        if(componentTypeFormList.isEmpty()) return true
        return  componentTypeFormList.distinct().containElements(mandatoryList)
    }

    private fun getPermitFilteredByMandatoryFieldsList():List<ContentTypeItem>{
        val mandatoryFieldsList = when(originalPermitInfo.state){
            PermitState.Request -> {
                val filterList = originalPermitInfo.request_checks.filterByMandatoryField() as MutableList
                filterList.addAll(originalPermitInfo.tasks.filterByMandatoryField())
                filterList
            }
            PermitState.InProgress -> originalPermitInfo.in_progress_checks.filterByMandatoryField()
            PermitState.PendingClosure -> originalPermitInfo.pending_closure_checks.filterByMandatoryField()
            else -> mutableListOf()
        }
        return mandatoryFieldsList.filter { it.type == Constants.PERMIT_CONTENT_TYPE_CHECKBOX || it.type == Constants.PERMIT_CONTENT_TYPE_TEXT_INPUT || it.type == Constants.PERMIT_CONTENT_TYPE_PHOTO}
    }

    private fun checkIfAllWorkersHaveBeenMarkedAsDone(permitInfo: PermitInfo, permitDoneState: PermitDoneState): Boolean {
        val filterList = permitInfo.requestee_users.filter { it.status == permitDoneState }
        return filterList.size == permitInfo.requestee_users.size
    }

    override fun buildUpdatePermitAsyncCall(updatePermitRequest: UpdatePermitInfoRequest, permitId: String): Single<ApiResponse> {
        return repository.updatePermit(updatePermitRequest, permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildGetPermitInfoAsyncCall(permitId: String): Single<PermitInfoResponse> {
        return repository.getPermitInfo(permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildMarkPermitAsDoneAsyncCall(userId: String, status: String): Single<ApiResponse> {
        return repository.setTeamMemberStatus(permitInfo.id.toString(),userId,status,Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildSavePermitComponentTypeResponseCall(componentSaveResponsesRequest: ComponentSaveResponsesRequest, permitId: String): Single<ApiResponse> {
        return repository.saveResponses(componentSaveResponsesRequest, permitId,Constants.BEARER_HEADER + sessionManager.token)
    }

}