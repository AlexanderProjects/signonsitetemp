package au.com.signonsitenew.domain.usecases.permits

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.base.PermitTemplateBaseUseCase
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import javax.inject.Inject

interface PermitTemplateUseCase {
    fun createPermitAsync(permitTemplate: PermitTemplate, requesteeUserList: List<RequesteeUser>, startDate:String?, endDate:String?):Single<SavePermitResponse>
    fun retrievePermitInfoAsync(permitId: String):Single<PermitInfoResponse>
    fun retrieveSitePermitTemplateAsync(): Single<SitePermitTemplateResponse>
}

class PermitTemplateUseCaseImpl @Inject constructor (private val sessionManager: SessionManager, private val repository: DataRepository): PermitTemplateBaseUseCase<SitePermitTemplateResponse, SavePermitResponse,PermitInfoResponse>(), PermitTemplateUseCase {

    override fun buildGetSitePermitTemplateAsyncCall(): Single<SitePermitTemplateResponse> {
        return repository.getPermitTemplates(sessionManager.siteId.toString(), Constants.BEARER_HEADER + sessionManager.token, true)
    }

    override fun buildCreatePermitsAsyncCall(createPermitRequest: CreatePermitRequest): Single<SavePermitResponse> {
        return repository.createPermit(createPermitRequest, sessionManager.siteId.toString(), Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun buildGetPermitInfoAsyncCall(permitId: String): Single<PermitInfoResponse> {
        return repository.getPermitInfo(permitId, Constants.BEARER_HEADER + sessionManager.token)
    }

    override fun createPermitAsync(permitTemplate: PermitTemplate, requesteeUserList: List<RequesteeUser>, startDate:String?, endDate:String?): Single<SavePermitResponse> {
        val savePermitRequest = CreatePermitRequest(permitTemplate.id, requesteeUserList.map { it.id }, RequesteeStates.all_uninvited ,startDate, endDate )
        return createNewPermitAsync(savePermitRequest)
    }

    override fun retrievePermitInfoAsync(permitId: String): Single<PermitInfoResponse> = getPermitInfoAsync(permitId)

    override fun retrieveSitePermitTemplateAsync(): Single<SitePermitTemplateResponse> = getSitePermitTemplateAsync()

}