package au.com.signonsitenew.domain.models

data class Visitors (val user_id:Int,
                     val first_name:String,
                     val last_name:String,
                     val phone_number:String,
                     val company_name:String,
                     val has_active_enrolment:Boolean,
                     val induction: Induction?)