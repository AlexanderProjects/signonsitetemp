package au.com.signonsitenew.domain.models

import com.squareup.moshi.Json

enum class PermitType {
    @Json(name = "confined_space")
    ConfinedSpace,
    @Json(name = "restricted_area")
    RestrictedArea,
    @Json(name= "excavation")
    Excavation,
    @Json(name = "harness")
    Harness,
    @Json(name = "hot")
    Hot,
    @Json(name = "crane")
    Crane,
    @Json(name = "radioactive_material")
    RadioActiveMaterial,
    @Json(name = "hazardous_substance")
    HazardousSubstance,
    @Json(name = "maintenance")
    Maintenance,
    @Json(name = "elevated_height")
    ElevatedHeight,
    @Json(name = "electrical")
    Electrical,
    @Json(name = "other")
    Other
}