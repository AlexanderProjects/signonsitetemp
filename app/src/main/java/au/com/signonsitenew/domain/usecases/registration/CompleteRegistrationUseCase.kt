package au.com.signonsitenew.domain.usecases.registration

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject


interface CompleteRegistrationUseCase {
    fun validateFirstTimeUser(): Single<ViewSelector>
    fun validateIfShowPassportButton(viewSelector: ViewSelector):Boolean
}

class CompleteRegistrationUseCaseImpl  @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val user:UserService) : CompleteRegistrationUseCase {

    override fun validateFirstTimeUser(): Single<ViewSelector> =
            Single.zip(repository.getFeatureFlags(userId = user.currentUserId.toString(), token = Constants.BEARER_HEADER + sessionManager.token),
            repository.getPersonalInfo(userId = user.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token), { featureFlagResponse: FeatureFlagsResponse, userInfoResponse: UserInfoResponse -> validateFeatureFlagAndPassportFlag(featureFlagResponse.user_feature_flags,userInfoResponse.user)})
            .observeOn(AndroidSchedulers.mainThread())

    private fun validateFeatureFlagAndPassportFlag(listOfFlags: List<FeatureFlag>, user: User):ViewSelector{
        for(feature in listOfFlags){
            if(feature.feature_flag_name.contentEquals(Constants.PASSPORTS) && feature.value && user.has_passport!!)
                return ViewSelector.PassportFragmentCreator
            if(feature.feature_flag_name.contentEquals(Constants.PASSPORTS) && !feature.value)
                return  ViewSelector.CredentialsFragmentCreator
            if(feature.feature_flag_name.contentEquals(Constants.PASSPORTS) && feature.value && !user.has_passport!!)
                return  ViewSelector.IntroPassportFragmentCreator
        }
        return  ViewSelector.CredentialsFragmentCreator
    }

    override fun validateIfShowPassportButton(viewSelector: ViewSelector):Boolean{
        return when(viewSelector){
            is ViewSelector.IntroPassportFragmentCreator -> true
            is ViewSelector.PassportFragmentCreator -> false
            else -> false
        }
    }

}