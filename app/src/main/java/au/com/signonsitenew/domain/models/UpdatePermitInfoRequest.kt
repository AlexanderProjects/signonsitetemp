package au.com.signonsitenew.domain.models


data class UpdatePermitInfoRequest(var state:String?,
                                   var requestee_users:List<Int>?,
                                   var start_date:String?,
                                   var end_date:String?){ constructor() : this(null,null,null,null) }