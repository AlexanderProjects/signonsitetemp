package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ContentTypeItem(var id:Int,
                           var name:String,
                           var type: String,
                           var is_mandatory:Boolean,
                           var responses:List<PermitContentTypeResponses>) : Parcelable
