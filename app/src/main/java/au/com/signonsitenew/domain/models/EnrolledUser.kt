package au.com.signonsitenew.domain.models

data class EnrolledUser(val user_id:Int, val first_name:String, val last_name:String, val company_name:String, val phone_number:String)