package au.com.signonsitenew.domain.models

import com.squareup.moshi.Json

data class RegisterFcmTokenResponse (
        val msg:String,
        @Json(name="token updated") val tokenUpdated:Boolean, override val status:String): ApiResponse(status)