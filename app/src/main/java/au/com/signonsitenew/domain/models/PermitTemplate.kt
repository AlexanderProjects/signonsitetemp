package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PermitTemplate (val id:Int,
                           val human_id:String,
                           val name:String,
                           val type: PermitType,
                           val custom_type:String?,
                           val updated_at:String,
                           val updated_by_user: PermitUser):Parcelable