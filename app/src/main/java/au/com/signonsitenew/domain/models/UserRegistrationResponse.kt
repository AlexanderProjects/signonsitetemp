package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserRegistrationResponse (override val status:String,
                                     val user:UserRegistration) : Parcelable, ApiResponse(status)