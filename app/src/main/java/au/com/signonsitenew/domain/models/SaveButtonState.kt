package au.com.signonsitenew.domain.models

enum class SaveButtonState {
    ShowButton, HideButton
}