package au.com.signonsitenew.domain.usecases.base

import android.graphics.Bitmap
import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.DisposableSubscriber

abstract class PassportBaseUseCase<T,P> protected constructor()  {

    val disposables = CompositeDisposable()

    abstract fun buildUserInfoAndCredentialsAsyncCall() :Flowable<P>

    abstract fun buildUpdatePersonalInfoAsyncCall(photo:Bitmap) : Single<T>

    fun getPersonalInfoAsync(photo:Bitmap,asyncCall: DisposableSingleObserver<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildUpdatePersonalInfoAsyncCall(photo)
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun getUserInfoAndCredentialsAsync(asyncCall: DisposableSubscriber<P>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildUserInfoAndCredentialsAsyncCall()
                .doOnComplete { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}