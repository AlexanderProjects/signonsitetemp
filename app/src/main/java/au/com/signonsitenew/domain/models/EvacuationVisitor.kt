package au.com.signonsitenew.domain.models

data class EvacuationVisitor(val user_id:Long,
                             val first_name:String,
                             val last_name:String,
                             val phone_number:String,
                             val company:String,
                             val company_name:String,
                             val check_in_time:String)