package au.com.signonsitenew.domain.models

data class RegistrationRequest (var authentication:String,
                                var email:String,
                                var fname:String,
                                var lname:String,
                                var phone_number:String,
                                var alpha2:String,
                                var cardnum:String,
                                var issueDate:String,
                                var state:String,
                                var password:String,
                                var employer1:String)