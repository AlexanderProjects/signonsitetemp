package au.com.signonsitenew.domain.models

data class FeatureFlag(val feature_flag_name: String, val value: Boolean)