package au.com.signonsitenew.domain.models.briefing

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Company (val name:String): Parcelable