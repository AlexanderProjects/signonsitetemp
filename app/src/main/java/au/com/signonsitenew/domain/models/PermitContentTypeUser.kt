package au.com.signonsitenew.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PermitContentTypeUser(val first_name:String, val last_name:String):Parcelable
