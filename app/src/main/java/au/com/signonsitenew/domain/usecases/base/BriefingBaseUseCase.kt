package au.com.signonsitenew.domain.usecases.base

import au.com.signonsitenew.BuildConfig
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

abstract class BriefingBaseUseCase<T,P,Q,X,Z> protected constructor() {
    val disposables = CompositeDisposable()

    abstract fun buildGetListOfBriefingsAsyncCall() : Single<T>

    abstract fun buildGetActiveBriefingsAsyncCall() : Single<P>

    abstract fun buildAcknowledgeBriefingAsyncCall() : Single<Q>

    abstract fun buildUpdateBriefingContentAsyncCall() : Single<X>

    abstract fun buildSeenBriefingAsyncCall() : Single<Z>


    fun getListOfBriefingsAsync(asyncCall: DisposableSingleObserver<T>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetListOfBriefingsAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun getActiveBriefingsAsync(asyncCall: DisposableSingleObserver<P>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildGetActiveBriefingsAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }
    fun acknowledgeBriefingAsync(asyncCall: DisposableSingleObserver<Q>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildAcknowledgeBriefingAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun updateBriefingContentAsync(asyncCall: DisposableSingleObserver<X>) {
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildUpdateBriefingContentAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun seenBriefingAsync(asyncCall: DisposableSingleObserver<Z>){
        if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.increment()
        disposables.add(buildSeenBriefingAsyncCall()
                .doOnSuccess { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement() }
                .doOnError { if (BuildConfig.IS_EXPRESSO_IDLING) TestIdlingResource.decrement()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(asyncCall))
    }

    fun dispose() {
        disposables.dispose()
    }
}