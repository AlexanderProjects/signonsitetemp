package au.com.signonsitenew.domain.utilities

import android.text.TextUtils
import au.com.signonsitenew.domain.models.Credential
import au.com.signonsitenew.domain.models.Enrolment
import au.com.signonsitenew.domain.models.User

object NotificationBadgeCounter {
    fun notifierNumberForPersonalInfo(user: User): Int {
        var passportNotificationCounter = 0
        if (user.first_name == null || TextUtils.isEmpty(user.first_name)) ++passportNotificationCounter
        if (user.last_name == null || TextUtils.isEmpty(user.last_name)) ++passportNotificationCounter
        if (user.company_name == null || TextUtils.isEmpty(user.company_name)) ++passportNotificationCounter
        if (user.trade == null || TextUtils.isEmpty(user.trade)) ++passportNotificationCounter
        if (user.email == null || TextUtils.isEmpty(user.email)) ++passportNotificationCounter
        if (user.phone_number == null || TextUtils.isEmpty(user.phone_number?.number)) ++passportNotificationCounter
        return passportNotificationCounter
    }

    fun notifierNumberForEmergencyInfo(user: User): Int {
        var emergencyNotificationCounter = 0
        if (user.primary_contact == null || TextUtils.isEmpty(user.primary_contact?.name)) ++emergencyNotificationCounter
        if (user.primary_contact == null || TextUtils.isEmpty(user.primary_contact?.phone_number?.number)) ++emergencyNotificationCounter
        return emergencyNotificationCounter
    }

    fun notifierNumberForCredential(credentials: List<Credential>): Int {
        var credentialNotificationCounter = 0
        for (credential in credentials) {
            if (credential.expiry_date != null) if (Validator.validateExpiryCredential(credential)) ++credentialNotificationCounter
        }
        return credentialNotificationCounter
    }

    fun notifierNumberForConnections(enrolmentList: List<Enrolment>): Int {
        var connectionNotificationCounter = 0
        for (enrolment in enrolmentList ){
            if(enrolment.site_induction?.state == null || enrolment.site_induction.state == "pending")
                ++connectionNotificationCounter
        }
        return connectionNotificationCounter
    }

}