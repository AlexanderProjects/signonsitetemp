package au.com.signonsitenew.domain.models.state

sealed class BriefingsActivityState {
    object StartProgressDialog : BriefingsActivityState()
    object Success : BriefingsActivityState()
    object Error: BriefingsActivityState()
    data class DataError(val error:String): BriefingsActivityState()

}