package au.com.signonsitenew.domain.usecases.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.os.Looper
import androidx.core.app.ActivityCompat
import au.com.signonsitenew.utilities.SLog
import com.google.android.gms.location.*
import javax.inject.Inject

interface LocationServiceUseCase{
    fun getCurrentLocation(locationData: (Location) -> Unit)
    fun isLocationPermissionGranted():Boolean
    fun cancelRequestingLocation()
}

class LocationServiceUseCaseImpl @Inject constructor(val context: Context) : LocationServiceUseCase {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback



    private fun createLocationRequest() : LocationRequest {
        val request = LocationRequest()
        request.interval = 5000
        request.fastestInterval = 3000
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        return request
    }

    private fun buildLocationSettingsRequest() {
        createLocationRequest()
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(createLocationRequest())
        val client = LocationServices.getSettingsClient(context)
        val task = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            SLog.d(context::class.java.name, "Successful")
        }.addOnFailureListener { e: Exception ->
            SLog.d(context::class.java.name, "Error " + e.message)
        }
    }


    override fun getCurrentLocation(locationData: (Location) -> Unit) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED) {
            buildLocationSettingsRequest()
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            locationCallback = object :LocationCallback(){
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations){
                        locationData(location)
                    }
                }
            }
            fusedLocationClient.requestLocationUpdates(createLocationRequest(),locationCallback, Looper.getMainLooper())
        }
    }

    override fun isLocationPermissionGranted(): Boolean {
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED)
    }

    override fun cancelRequestingLocation() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

}