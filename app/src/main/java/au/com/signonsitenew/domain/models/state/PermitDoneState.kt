package au.com.signonsitenew.domain.models.state

import com.squareup.moshi.Json


 enum class PermitDoneState {
    @Json(name = "finished_work")
    FinishedWork,
    @Json(name= "finished_request")
    FinishedRequest,
    @Json(name = "prerequest")
    PreRequest,
    @Json(name = "uninvited")
    UnInvited,
}
