package au.com.signonsitenew.domain.models.state

sealed class PermitTaskTabFragmentState{

    object ShowEmptyStartDate:PermitTaskTabFragmentState()
    object ReadOnlyDates:PermitTaskTabFragmentState()
    object LoadAllViewComponents:PermitTaskTabFragmentState()
    object ShowEmptyEndDate:PermitTaskTabFragmentState()
    data class ShowEndDate(val date: String):PermitTaskTabFragmentState()
    data class ShowStartDate(val date:String):PermitTaskTabFragmentState()
    data class UpdateEndDate(val date: String):PermitTaskTabFragmentState()
    data class UpdateStartDate(val date:String):PermitTaskTabFragmentState()
    data class ClickOnStartDate(val isLayoutVisible:Boolean):PermitTaskTabFragmentState()
    data class ClickOnEndDate(val isLayoutVisible:Boolean):PermitTaskTabFragmentState()
}
