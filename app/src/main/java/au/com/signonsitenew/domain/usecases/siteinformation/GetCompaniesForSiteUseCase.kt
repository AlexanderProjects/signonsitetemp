package au.com.signonsitenew.domain.usecases.siteinformation

import au.com.signonsitenew.domain.models.CompaniesForSiteResponse
import au.com.signonsitenew.domain.models.Company
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface GetCompaniesForSiteUseCase {
    fun retrieveCompaniesForSite(siteId:String) : Single<CompaniesForSiteResponse>
    fun saveCompaniesInSiteInDB(companies:List<Company>)
}

class GetCompaniesForSiteUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager) : GetCompaniesForSiteUseCase {

    override fun retrieveCompaniesForSite(siteId:String) : Single<CompaniesForSiteResponse> = repository.getCompaniesForSite(siteId,sessionManager.currentUser[Constants.USER_EMAIL]!!, Constants.AUTH_KEY, Constants.BEARER_HEADER + sessionManager.token)
            .observeOn(AndroidSchedulers.mainThread())

    override fun saveCompaniesInSiteInDB(companies:List<Company>) = repository.saveCompaniesInDb(companies)

}