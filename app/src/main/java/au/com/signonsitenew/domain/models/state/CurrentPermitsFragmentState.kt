package au.com.signonsitenew.domain.models.state

import au.com.signonsitenew.domain.models.Permit

sealed class CurrentPermitsFragmentState {
    data class ShowPermitList(val permits:List<Permit>):CurrentPermitsFragmentState()
    object ShowError:CurrentPermitsFragmentState()
    object ClickOnNewPermit:CurrentPermitsFragmentState()
    data class NavigatePermitDetails(val permit:Permit):CurrentPermitsFragmentState()
    object NavigateTemplatePermit:CurrentPermitsFragmentState()
    object ShowProgressView:CurrentPermitsFragmentState()
    object RemoveProgressView:CurrentPermitsFragmentState()
}