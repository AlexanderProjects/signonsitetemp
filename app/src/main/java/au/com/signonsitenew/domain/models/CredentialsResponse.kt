package au.com.signonsitenew.domain.models


data class CredentialsResponse (
        val credentials: List<Credential>?,
        override val status: String): ApiResponse(status)