package au.com.signonsitenew.domain.models

sealed class ViewSelector {
    object CredentialsFragmentCreator : ViewSelector()
    object PassportFragmentCreator:ViewSelector()
    object IntroPassportFragmentCreator : ViewSelector()
}