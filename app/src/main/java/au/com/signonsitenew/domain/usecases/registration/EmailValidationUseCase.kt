package au.com.signonsitenew.domain.usecases.registration

import au.com.signonsitenew.domain.models.EmailVerificationRequest
import au.com.signonsitenew.domain.models.EmailVerificationResponse
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.utilities.Constants
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface EmailValidationUseCase {
    fun validateEmail(email:String): Single<EmailVerificationResponse>
    fun isValidEmailAddress(email:String): Boolean
}

class EmailValidationUseCaseImpl @Inject constructor(private val dataRepository: DataRepository) :EmailValidationUseCase {
    override fun validateEmail(email:String): Single<EmailVerificationResponse> =
            dataRepository.validateEmail(buildRequest(email))
            .observeOn(AndroidSchedulers.mainThread())


    private fun buildRequest(email: String):EmailVerificationRequest {
        var request = EmailVerificationRequest()
        request.authentication = Constants.AUTH_KEY
        request.email = email
        return request
    }

    override fun isValidEmailAddress(email:String): Boolean = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}