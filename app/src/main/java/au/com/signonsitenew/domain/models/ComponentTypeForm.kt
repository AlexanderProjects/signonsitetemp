package au.com.signonsitenew.domain.models

data class ComponentTypeForm(val form_input_id:Int,
                             var value:Any)