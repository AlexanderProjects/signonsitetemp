package au.com.signonsitenew.domain.usecases.passport

import androidx.fragment.app.Fragment
import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.models.UpdateInfoResponse
import au.com.signonsitenew.domain.models.UserInfoResponse
import au.com.signonsitenew.domain.models.UserInfoUpdateRequest
import au.com.signonsitenew.realm.services.UserService
import au.com.signonsitenew.ui.passport.PassportFragment
import au.com.signonsitenew.ui.passport.intro.IntroPassportFragment
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

interface IntroPassportUseCase {
    fun validateFirstTimeUser(): Single<UserInfoResponse>
    fun validateFragment(hasUserPassport:Boolean?):Fragment
    fun getErrorMessage(userInfoResponse: UpdateInfoResponse): String
    fun createPassport():Single<UpdateInfoResponse>
}

class IntroPassportUseCaseImpl @Inject constructor(private val repository: DataRepository, private val sessionManager: SessionManager, private val userService: UserService) : IntroPassportUseCase {

    override fun validateFirstTimeUser(): Single<UserInfoResponse> =
            repository.getPersonalInfo(userId = userService.currentUserId.toString(), bearerToken = Constants.BEARER_HEADER + sessionManager.token)
                    .observeOn(AndroidSchedulers.mainThread())


    override fun validateFragment(hasUserPassport:Boolean?):Fragment {
        return when(hasUserPassport){
            true -> PassportFragment()
            false -> IntroPassportFragment()
            null -> PassportFragment()
        }
    }

    override fun getErrorMessage(userInfoResponse: UpdateInfoResponse): String = when(userInfoResponse.status){
        Constants.RESPONSE_UNAUTHENTIC -> Constants.RESPONSE_UNAUTHENTIC_MESSAGE
        Constants.RESPONSE_BAD_INPUT -> Constants.RESPONSE_BAD_INPUT_MESSAGE
        Constants.RESPONSE_UNAUTHORISED -> Constants.RESPONSE_UNAUTHORISED_MESSAGE
        else -> Constants.UNKNOWN_NETWORK_ERROR_MESSAGE
    }

    override fun createPassport(): Single<UpdateInfoResponse> {
        return repository.updatePersonalInfo(buildUserInfoRequest(), userService.currentUserId.toString(), Constants.BEARER_HEADER + sessionManager.token).observeOn(AndroidSchedulers.mainThread())
    }

    private fun buildUserInfoRequest(): UserInfoUpdateRequest {
        val request = UserInfoUpdateRequest()
        request.has_passport = true
        return request
    }
}