package au.com.signonsitenew.domain.models

data class Induction(val id:Int?,val type:String?,val number:Int?,val state: State?)