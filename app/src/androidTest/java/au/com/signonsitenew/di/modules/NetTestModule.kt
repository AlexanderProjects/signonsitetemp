package au.com.signonsitenew.di.modules

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit

class NetTestModule : NetModule() {

    public override fun provideOkHttpClient(): OkHttpClient {
        return super.provideOkHttpClient()
    }

    public override fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit {
        return super.provideRetrofit(client, moshi)
    }
}