package au.com.signonsitenew.di.modules

import au.com.signonsitenew.data.factory.datasources.net.RestService
import retrofit2.Retrofit
import javax.inject.Named

class ApplicationTestModule : ApplicationModule() {
    public override fun provideRestService(retrofit: Retrofit, @Named(value = "retrofitNulls") retrofitWithNulls: Retrofit, @Named(value = "retrofitForMapResponse") retrofitForMapResponse: Retrofit): RestService {
        return super.provideRestService(retrofit, retrofitWithNulls, retrofitForMapResponse)
    }
}