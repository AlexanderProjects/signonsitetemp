package au.com.signonsitenew.di.components

import au.com.signonsitenew.di.modules.ApplicationModule
import au.com.signonsitenew.di.modules.NetModule
import au.com.signonsitenew.integrationtest.services.RestServiceIntegrationTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetModule::class, ApplicationModule::class])
interface ApplicationTestComponent {
    fun inject(restServiceIntegrationTest: RestServiceIntegrationTest?)
}