package au.com.signonsitenew.helpers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.VectorDrawable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher


class DrawableMatcher(private val resourceId:Int) : TypeSafeMatcher<View>() {

    override fun describeTo(description: Description?) {
        description?.appendText("with drawable id: ")?.appendValue(resourceId)
    }
    override fun matchesSafely(item: View?): Boolean {
        if (item !is ImageView) return false

        if (resourceId == 0) return item.drawable == null

        val expectedDrawable = ContextCompat.getDrawable(item.getContext(), resourceId)
                ?: return false

        val actualDrawable = item.drawable

        if (expectedDrawable is VectorDrawable) {
            return if (actualDrawable !is VectorDrawable) false else vectorToBitmap(expectedDrawable)!!.sameAs(vectorToBitmap(actualDrawable))
        }

        if (expectedDrawable is BitmapDrawable) {
            return if (actualDrawable !is BitmapDrawable) false else expectedDrawable.bitmap.sameAs(actualDrawable.bitmap)
        }

        throw IllegalArgumentException("Unsupported drawable: " + item.drawable)
    }

    private fun vectorToBitmap(vectorDrawable: VectorDrawable): Bitmap? {
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        vectorDrawable.draw(canvas)
        return bitmap
    }
}