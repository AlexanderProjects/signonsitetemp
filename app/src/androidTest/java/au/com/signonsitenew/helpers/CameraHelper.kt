package au.com.signonsitenew.helpers

import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.intent.IntentCallback
import au.com.signonsitenew.R
import au.com.signonsitenew.utilities.Constants
import org.hamcrest.Description
import java.io.File

class CameraHelper {
    companion object{
        fun createImageCaptureActivityResultStub(activity: Activity): Instrumentation.ActivityResult {
            val bundle = Bundle()
            bundle.putParcelable(Constants.IMAGE_BITMAP_FLAG, BitmapFactory.decodeResource(activity.resources, R.drawable.camera_plus))
            // Create the Intent that will include the bundle.
            val resultData = Intent()
            resultData.putExtras(bundle)
            // Create the ActivityResult with the Intent.
            return Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)
        }

        fun intentCallback(resourceId : Int = R.mipmap.ic_launcher) : IntentCallback {
            var imageName = "No Image Name"
            return IntentCallback {
                if (it.action == MediaStore.ACTION_IMAGE_CAPTURE) {
                    it.extras?.getParcelable<Uri>(MediaStore.EXTRA_OUTPUT).run {
                        imageName = File(it.getParcelableExtra<Parcelable>(MediaStore.EXTRA_OUTPUT).toString()).name
                        val context : Context = InstrumentationRegistry.getInstrumentation().targetContext
                        val outStream = this?.let { it1 -> context.contentResolver.openOutputStream(it1) }
                        val bitmap : Bitmap = BitmapFactory.decodeResource(context.resources, resourceId)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
                    }
                }
            }
        }

        fun hasImageSet(): BoundedMatcher<View, ImageView> {
            return object : BoundedMatcher<View, ImageView>(ImageView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("has image set.")
                }

                override fun matchesSafely(imageView: ImageView): Boolean {
                    return imageView.background != null
                }
            }
        }
    }
}