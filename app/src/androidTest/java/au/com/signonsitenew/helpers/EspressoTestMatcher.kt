package au.com.signonsitenew.helpers

import android.view.View
import android.widget.TextView
import androidx.test.espresso.intent.Checks
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher


object EspressoTestMatcher {
    fun withDrawable(resourceId:Int) : Matcher<View> = DrawableMatcher(resourceId)
    fun noDrawable() : Matcher<View> = DrawableMatcher(-1)

    fun withTextColor(color: Int): Matcher<View?>? {
        Checks.checkNotNull(color)
        return object : BoundedMatcher<View?, TextView>(TextView::class.java) {
            override fun matchesSafely(warning: TextView): Boolean {
                return color == warning.currentTextColor
            }

            override fun describeTo(description: Description) {
                description.appendText("with text color: ")
            }
        }
    }
}