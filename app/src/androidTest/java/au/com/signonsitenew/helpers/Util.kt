package au.com.signonsitenew.helpers

import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isClickable
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import au.com.signonsitenew.R
import au.com.signonsitenew.domain.models.CredentialCreateUpdateRequest
import org.hamcrest.BaseMatcher
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsNull
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


object Util {
    @JvmStatic
    fun random(): String {
        val generator = Random()
        val randomStringBuilder = StringBuilder()
        val randomLength = generator.nextInt(10)
        var tempChar: Char
        for (i in 0 until randomLength) {
            tempChar = (generator.nextInt(96) + 32).toChar()
            randomStringBuilder.append(tempChar)
        }
        return randomStringBuilder.toString()
    }

    fun randomString():String {
        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return  (1..7)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

    }

    @JvmStatic
    @Throws(InterruptedException::class)
    fun setAirplaneMode(enable: Boolean) {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.openQuickSettings()
        // Find the text of your language
        val description = By.desc("Flight mode")
        // Need to wait for the button, as the opening of quick settings is animated.
        device.wait(Until.hasObject(description), 500)
        device.findObject(description).click()
        InstrumentationRegistry.getInstrumentation().context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
    }

    @JvmStatic
    fun restartSignOnSiteApp(instrumentation: Instrumentation?, activityTestRule: ActivityTestRule<*>) {
        val device = UiDevice.getInstance(instrumentation)
        device.pressHome()
        val launcherPackage = device.launcherPackageName
        MatcherAssert.assertThat(launcherPackage, IsNull.notNullValue())
        device.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)),
                5000)
        // Launch the app
        val context = activityTestRule.activity.applicationContext
        val intent = context.packageManager
                .getLaunchIntentForPackage("au.com.signonsitenew")
        intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
        device.wait(Until.hasObject(By.pkg("au.com.signonsitenew").depth(0)),
                5000)
    }

    fun <T> withMyValue(name: String?): Matcher<T> {
        return object : BaseMatcher<T>() {
            override fun matches(item: Any): Boolean {
                return item.toString().contains(name!!)
            }

            override fun describeTo(description: Description) {}
        }
    }

    fun createImageFile(fileName: String?, context: Context): File {
        lateinit var outStream: OutputStream
        val bitmap = BitmapFactory.decodeResource(context.applicationContext.resources, R.drawable.camera_plus)
        val storage = Environment.getExternalStorageDirectory().toString()
        var file = File(storage,fileName + Calendar.getInstance().timeInMillis)

        if(file.exists()){
            file.delete()
            File(storage,fileName + Calendar.getInstance().timeInMillis)
        }
        outStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
        outStream.flush()
        outStream.close()

        return file
    }

    fun buildUpdateCredentialRequest() : CredentialCreateUpdateRequest {
        val request = CredentialCreateUpdateRequest()
        request.edits_credential_id = 3579
        request.name = "Matthew boba credential"
        request.rto = "A123456789"

        return request
    }

    fun buildCreateCredential(): CredentialCreateUpdateRequest{
        val request = CredentialCreateUpdateRequest()
        request.name = "Temporary credential"
        request.identifier = "A12345"
        request.rto = "A123456789"

        return request
    }

    fun getSaltString(): String? {
        val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        val salt = java.lang.StringBuilder()
        val rnd = Random()
        while (salt.length < 10) { // length of the random string.
            val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
            salt.append(SALTCHARS[index])
        }
        return salt.toString()
    }

    fun getSaltStringNotNumbers(): String? {
        val SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        val salt = java.lang.StringBuilder()
        val rnd = Random()
        while (salt.length < 10) { // length of the random string.
            val index = (rnd.nextFloat() * SALTCHARS.length).toInt()
            salt.append(SALTCHARS[index])
        }
        return salt.toString()
    }

    fun forceClick(): ViewAction {
        return object : ViewAction {

            override fun getConstraints(): Matcher<View> {
                return allOf(isClickable(), isEnabled(), ViewMatchers.isDisplayed())
            }

            override fun getDescription(): String {
                return "force click"
            }


            override fun perform(uiController: UiController, view: View) {
                view.performClick() // perform click without checking view coordinates.
                uiController.loopMainThreadUntilIdle()
            }
        }
    }

}