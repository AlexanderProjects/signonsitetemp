package au.com.signonsitenew.uitest.briefings

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.web.assertion.WebViewAssertions.webMatches
import androidx.test.espresso.web.sugar.Web.onWebView
import androidx.test.espresso.web.webdriver.DriverAtoms.findElement
import androidx.test.espresso.web.webdriver.DriverAtoms.getText
import androidx.test.espresso.web.webdriver.Locator
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.DocumentsRecyclerViewAdapter
import au.com.signonsitenew.adapters.SiteRecyclerViewAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.ActionHelper.selectTabAtPosition
import au.com.signonsitenew.helpers.ActionHelper.waitFor
import au.com.signonsitenew.helpers.ActionHelper.waitId
import au.com.signonsitenew.helpers.EspressoTestMatcher.withDrawable
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.*
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.DEFAULT)
class BriefingDetailsTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Espresso.onView(withId(R.id.sites_button)).perform(click())
        Espresso.onView(withId(R.id.site_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        Espresso.onView(withId(R.id.site_list)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("SignOnSite HQ2"))))
        Espresso.onView((withId(R.id.site_list))).perform(RecyclerViewActions.actionOnItem<SiteRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("SignOnSite HQ2")), click()))
        Espresso.onView(withId(R.id.tabs)).perform(ActionHelper.waitUntil(isDisplayed()))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(withId(R.id.tabs)).perform(selectTabAtPosition(1))
        Espresso.onView(withId(R.id.tabs)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(isRoot()).perform(waitId(R.id.document_name_text_view, 2000))
        Espresso.onView(withId(R.id.documents_list)).perform(RecyclerViewActions.actionOnItemAtPosition<DocumentsRecyclerViewAdapter.ViewHolderWorker>(0, click()))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        onWebView().forceJavascriptEnabled()

    }


    @Test
    fun showBriefingTextTest(){
        Espresso.onView(withId(R.id.quill_webview)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(isRoot()).perform(waitFor(3000))
        onWebView().withElement(findElement(Locator.TAG_NAME, "p"))
                .check(webMatches(getText(), any(String::class.java)))
    }

    @Test
    fun acknowledgeBriefingTest(){
        Espresso.onView(withId(R.id.quill_webview)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(isRoot()).perform(waitFor(3000))
        onWebView().withElement(findElement(Locator.TAG_NAME, "p")).check(webMatches(getText(),any(String::class.java)))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(withId(R.id.briefing_acknowledged_button)).perform(click())
        Espresso.onView(isRoot()).perform(waitFor(2000))
        Espresso.onView(withDrawable(R.drawable.ic_check_circle)).perform(ActionHelper.waitUntil(isDisplayed()))
    }

    @Test
    fun showSignatureInBriefingDetailsTest(){
        Espresso.onView(withId(R.id.briefing_signature_name)).check(ViewAssertions.matches(isDisplayed()))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }
}