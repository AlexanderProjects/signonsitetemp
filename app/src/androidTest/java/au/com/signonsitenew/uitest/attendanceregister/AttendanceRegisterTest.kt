package au.com.signonsitenew.uitest.attendanceregister

import android.widget.AutoCompleteTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.SiteRecyclerViewAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.containsString
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.DEFAULT)
class AttendanceRegisterTest {
    @get:Rule
    val rule = lazyActivityScenarioRule<MainActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        onView(withId(R.id.sites_button)).perform(ViewActions.click())
        onView(withId(R.id.site_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.site_list)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alex's house"))))
        onView((withId(R.id.site_list))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        try {
            onView(withId(android.R.id.button1)).perform(ActionHelper.waitUntil(isDisplayed()))
            onView(withId(android.R.id.button1)).perform(ViewActions.click())
            onView(isRoot()).perform(ActionHelper.waitFor(1000))
            onView(withId(R.id.tabs)).perform(ActionHelper.waitUntil(isDisplayed()))
            onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(3))
            onView(isRoot()).perform(ActionHelper.waitFor(2000))
            onView(withId(R.id.manage_visitors_button)).check(ViewAssertions.matches(isDisplayed()))
            onView(withId(R.id.manage_visitors_button)).perform(ViewActions.click())
        }catch (e: NoMatchingViewException){
            onView(isRoot()).perform(ActionHelper.waitFor(1000))
            onView(withId(R.id.tabs)).perform(ActionHelper.waitUntil(isDisplayed()))
            onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(3))
            onView(isRoot()).perform(ActionHelper.waitFor(2000))
            onView(withId(R.id.manage_visitors_button)).check(ViewAssertions.matches(isDisplayed()))
            onView(withId(R.id.manage_visitors_button)).perform(ViewActions.click())
        }
    }

    @Test
    fun checkCompanyNameIsShownInTheListTest(){
        onView(withId(R.id.expandable_list_view)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.expandable_list_view)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("F"))))
    }

    @Test
    fun checkWorkerNameIsShowInTheListTest(){
        onView(withId(R.id.expandable_list_view)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.expandable_list_view))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.child_list)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun clickOnWorkerAndShowWorkerDetailsTest(){
        onView(withId(R.id.expandable_list_view)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.expandable_list_view))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.child_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.child_list))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        onView(isRoot()).perform(ActionHelper.waitFor(2000))
        onView(withId(R.id.visitor_name_text_view)).check(ViewAssertions.matches(isDisplayed()))
    }
    @Test
    fun searchByCompany(){
        onView(withId(R.id.action_search)).perform(ViewActions.click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(isAssignableFrom(AutoCompleteTextView::class.java)).perform(typeText("SignOnSite"), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.expandable_list_view)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.expandable_list_view).atPositionOnView(0, R.id.new_attendance_company_name)).check(ViewAssertions.matches(withText(containsString("Pepito"))))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }

}