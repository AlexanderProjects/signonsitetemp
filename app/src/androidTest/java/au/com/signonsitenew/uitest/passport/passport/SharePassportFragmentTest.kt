package au.com.signonsitenew.uitest.passport.passport

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.ActionHelper.waitFor
import au.com.signonsitenew.helpers.Util.random
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SharePassportFragmentTest {
    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private lateinit var randomField: String
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Espresso.onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        Espresso.onView(withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        Espresso.onView(withId(R.id.share_button)).perform(ViewActions.click())
        randomField = random()
    }

    @Test
    fun sharedPassportSuccessful(){
        Espresso.onView(withId(R.id.share_email_edit_text_view)).perform(ViewActions.typeText("alexander.parra@signonste.com.au"), ViewActions.closeSoftKeyboard())
        Espresso.onView(withId(R.id.share_action_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(5000))
        Espresso.onView(ViewMatchers.withText(Constants.SHARE_PASSPORT_ALERT_DIALOG_TITLE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(withId(android.R.id.button1)).perform(ViewActions.click())
    }

    @Test
    fun sharedPassportError(){
        Espresso.onView(withId(R.id.share_email_edit_text_view)).perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
        Espresso.onView(withId(R.id.share_action_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(5000))
        Espresso.onView(ViewMatchers.withText("Oops!")).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(withId(android.R.id.button1)).perform(ViewActions.click())
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }

}