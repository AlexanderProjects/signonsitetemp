package au.com.signonsitenew.uitest.passport.passport

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class PassportFragmentUiTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    @get:Rule
    var runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.CAMERA)
    @get:Rule
    var storagePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
    }

    @Test
    fun takePhotoHeadShotPhotoTest(){
        //update photo
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ViewActions.click())

        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.findObject(UiSelector().className("android.widget.TextView").text("Camera")).click()
        device.findObject(UiSelector().className("android.widget.ImageView").descriptionContains("Shutter")).click()
        device.findObject(UiSelector().className("android.widget.ImageButton").descriptionContains("Done")).click()
        Espresso.onView(ViewMatchers.withId(R.id.crop_image_menu_crop)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.crop_image_menu_crop)).perform(ViewActions.click())
        //validation
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun takePhotoFromGalleryHeadShotPhotoTest(){
        //update photo
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ViewActions.click())

        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        device.findObject(UiSelector().className("android.widget.TextView").text("Camera")).click()
        device.findObject(UiSelector().className("android.widget.ImageView").descriptionContains("Shutter")).click()
        device.findObject(UiSelector().className("android.widget.ImageButton").descriptionContains("Done")).click()
        Espresso.onView(ViewMatchers.withId(R.id.crop_image_menu_crop)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.crop_image_menu_crop)).perform(ViewActions.click())
        //validation
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.profile_photo_passport_imageView)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }


}