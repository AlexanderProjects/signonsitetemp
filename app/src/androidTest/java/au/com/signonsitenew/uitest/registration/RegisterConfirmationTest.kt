package au.com.signonsitenew.uitest.registration

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isChecked
import androidx.test.espresso.matcher.ViewMatchers.isNotChecked
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.Util
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class RegisterConfirmationTest {

    @get:Rule
    var activityRule = IntentsTestRule(RegisterActivity::class.java)

    @Before
    fun setup(){
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ViewActions.replaceText("${Util.getSaltStringNotNumbers()}"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_first_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ViewActions.replaceText("${Util.getSaltStringNotNumbers()}"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_last_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ViewActions.replaceText("${Util.getSaltString()}@msn.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_email_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ViewActions.replaceText("0434242730"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.submitPasswordButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitPasswordButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ViewActions.typeText("SigOnSite"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        //Espresso.onData(Matchers.instanceOf("SignOnSite"::class.java)).inRoot(RootMatchers.withDecorView(CoreMatchers.not(Matchers.`is`(activityRule.activity.window.decorView)))).perform(ViewActions.click())
        //Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString("Sos"))))

        Espresso.onView(ViewMatchers.withId(R.id.next_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.next_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun unCheckTermsAndConditionsWithAlertDialogTest(){

        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_text_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.age_consent_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_checkbox)).check(ViewAssertions.matches(isChecked())).perform(scrollTo(), click())

        Espresso.onView(ViewMatchers.withId(R.id.register_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.register_button)).perform(click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withText(activityRule.activity.getString(R.string.registration_error_terms_conditions_text))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(android.R.id.button1)).perform(click())

    }

    @Test
    fun unPrivacyPolicyWithAlertDialogTest(){

        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_text_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.age_consent_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_checkbox)).check(ViewAssertions.matches(isChecked())).perform(scrollTo(), click())

        Espresso.onView(ViewMatchers.withId(R.id.register_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.register_button)).perform(click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withText(activityRule.activity.getString(R.string.registration_error_privacy_policy_text))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(android.R.id.button1)).perform(click())

    }

    @Test
    fun unAgeConsentWithAlertDialogTest(){

        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_text_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.age_consent_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.age_consent_checkbox)).check(ViewAssertions.matches(isChecked())).perform(scrollTo(), click())

        Espresso.onView(ViewMatchers.withId(R.id.register_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.register_button)).perform(click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withText(activityRule.activity.getString(R.string.registration_error_acknowledge_age_text))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(android.R.id.button1)).perform(click())

    }

    @Test
    fun registrationProcessTest(){
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_text_view)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.terms_and_conditions_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.privacy_policy_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())
        Espresso.onView(ViewMatchers.withId(R.id.age_consent_checkbox)).check(ViewAssertions.matches(isNotChecked())).perform(scrollTo(), click())

        Espresso.onView(ViewMatchers.withId(R.id.register_button)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.register_button)).perform(click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }
}