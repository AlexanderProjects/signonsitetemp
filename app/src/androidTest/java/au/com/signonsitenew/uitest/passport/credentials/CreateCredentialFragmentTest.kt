package au.com.signonsitenew.uitest.passport.credentials


import android.app.Activity
import android.provider.MediaStore
import android.widget.DatePicker
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.intent.IntentCallback
import androidx.test.runner.intent.IntentMonitorRegistry
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.CreateCredentialAdapter
import au.com.signonsitenew.ui.adapters.TypeOfCredentialsListAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.CameraHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CreateCredentialFragmentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    @get:Rule
    var runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.CAMERA)
    @get:Rule
    var storagePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    lateinit var callback:IntentCallback

    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Intents.init()
        val imgCaptureResult = CameraHelper.createImageCaptureActivityResultStub(getActivity())
        intending(hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(imgCaptureResult)
        callback = CameraHelper.intentCallback(R.drawable.camera_plus)
        IntentMonitorRegistry.getInstance().addIntentCallback(callback)
        onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.tabs)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(2000))
        onView(withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.add_new_credential)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(2000))
    }

    /**
     * This method test site safe template
     * {
    "id": 1,
    "name": "Sitesafe",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "none",
    "expiry_date": "required",
    "issued_by": "none",
    "issuers": null,
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": []
    }
     *
     * **/

    @Test
    fun showSiteSafeTemplateAndCheckedRequiredFieldsValidationTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        //Validation
        onView(withId(R.id.status_front_photo)).check(ViewAssertions.matches(withText(containsString("( required )"))))
        onView(withId(R.id.status_back_photo)).check(ViewAssertions.matches(withText(containsString("( optional )"))))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_title)).check(ViewAssertions.matches(withText(containsString("ID Number"))))

    }

    @Test
    fun createSiteSafeTemplateCredentialAndShowOnCredentialsListTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        returnToCredentialsList()
        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createSiteSafeTemplateCredentialAndShowOnCredentialsListTestWithOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }
    /**
     * {
    "id": 2,
    "name": "Whitecard",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "required",
    "issue_date": "required",
    "expiry_date": "none",
    "issued_by": "required",
    "issuers": [
    "ACT",
    "NSW",
    "NT",
    "QLD",
    "SA",
    "TAS",
    "VIC",
    "WA"
    ],
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": []
    }
    **/

    @Test
    fun createWhiteCardTemplateCredentialAndShowOnCredentialsListTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(CoreMatchers.`is`(instanceOf<Any>(String::class.java)), CoreMatchers.`is`("NSW"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 3,
    "name": "Unique Student Identifier",
    "identifier_name": "USI Number",
    "identifier": "required",
    "front_photo": "none",
    "back_photo": "none",
    "issue_date": "none",
    "expiry_date": "none",
    "issued_by": "none",
    "issuers": null,
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": []
    }
     * **/
    @Test
    fun validateAndCreateUniqueStudentIdentifierTemplateAndCheckedRequiredFieldsTest(){
        //Validate fields
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
    }

    /**
     * {
    "id": 4,
    "name": "Australian Birth Certificate",
    "identifier_name": "Registration Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "none",
    "issue_date": "optional",
    "expiry_date": "none",
    "issued_by": "optional",
    "issuers": [
    "ACT",
    "NSW",
    "NT",
    "QLD",
    "SA",
    "TAS",
    "VIC",
    "WA"
    ],
    "rto": "none",
    "registration_number": "required",
    "reference": "none",
    "categories": [
    "Evidence of Identity"
    ]
    }**/

    @Test
    fun createAustralianBirthCertificateTemplateCredentialAndShowOnCredentialsListTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(CoreMatchers.equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2007,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf(String::class.java)),`is`("NSW"))).perform(click())
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(3, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, closeSoftKeyboard()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(scrollTo(),click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 5,
    "name": "Australian Driver's Licence",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "none",
    "expiry_date": "optional",
    "issued_by": "required",
    "issuers": [
    "ACT",
    "NSW",
    "NT",
    "QLD",
    "SA",
    "TAS",
    "VIC",
    "WA"
    ],
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": [
    "Evidence of Identity"
    ]
    }
     **/

    @Test
    fun createAustralianDriverLicenceTemplateCredentialAndShowOnCredentialsListTestWithRequiredFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf<Any>(String::class.java)), `is`("NSW"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createAustralianDriverLicenceTemplateCredentialAndShowOnCredentialsListTestWithOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf<Any>(String::class.java)), `is`("NSW"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 6,
    "name": "Medicare Card",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "none",
    "issue_date": "none",
    "expiry_date": "optional",
    "issued_by": "none",
    "issuers": null,
    "rto": "none",
    "registration_number": "none",
    "reference": "required",
    "categories": [
    "Evidence of Identity"
    ]
    }
     *
     *
     * **/

    @Test
    fun createMedicareCardTemplateCredentialAndShowOnCredentialsListTestWithOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(5, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(2, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, closeSoftKeyboard()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 7,
    "name": "Proof of Age",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "none",
    "expiry_date": "optional",
    "issued_by": "required",
    "issuers": [
    "ACT",
    "NSW",
    "NT",
    "QLD",
    "SA",
    "TAS",
    "VIC",
    "WA"
    ],
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": [
    "Evidence of Identity"
    ]
    }
     * **/

    @Test
    fun createProofOfAgeTemplateCredentialAndShowOnCredentialsListTestWithOptionalFields(){

        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(6, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf<Any>(String::class.java)), `is`("ACT"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(3000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)

    }

    /**
     *
     * {
    "id": 8,
    "name": "Police Check",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "required",
    "expiry_date": "none",
    "issued_by": "required",
    "issuers": [
    "ACT",
    "NSW",
    "NT",
    "QLD",
    "SA",
    "TAS",
    "VIC",
    "WA"
    ],
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": []
    }
     *
     * **/

    @Test
    fun createPoliceCheckTemplateCredentialAndShowOnCredentialsListTestWithRequiredFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(7, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf<Any>(String::class.java)), `is`("NSW"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createPoliceCheckTemplateCredentialAndShowOnCredentialsListTestWithOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(7, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_spinner_value)).perform(click())
        onData(AllOf.allOf(`is`(instanceOf<Any>(String::class.java)),`is`("NSW"))).perform(click())
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     *  {
    "id": 9,
    "name": "ACT - Working With Vulnerable People (WWVP)",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "none",
    "expiry_date": "required",
    "issued_by": "none",
    "issuers": null,
    "rto": "none",
    "registration_number": "none",
    "reference": "none",
    "categories": [
    "Working with Vulnerable People and Children"
    ]
    }
     * **/

    @Test
    fun createWWVPTemplateCredentialAndShowOnCredentialsListTestWithRequiredFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(8, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createWWVPTemplateCredentialAndShowOnCredentialsListTestWithRequiredAndOptionalFieldsTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(8, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 17,
    "name": "Asbestos Awareness (10675NAT)",
    "identifier_name": null,
    "identifier": "none",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "required",
    "expiry_date": "none",
    "issued_by": "optional",
    "issuers": null,
    "rto": "optional",
    "registration_number": "none",
    "reference": "none",
    "categories": [
    "Asbestos"
    ]
    }
     *
     * **/

    @Test
    fun createAsbestosAwarenessTemplateCredentialAndShowOnCredentialsListTestWithRequiredAndOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(16, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(1, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, closeSoftKeyboard()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(2, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, closeSoftKeyboard()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createAsbestosAwarenessTemplateCredentialAndShowOnCredentialsListTestWithRequiredFieldsTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(16, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.creation_credential_from_save_button)).perform(click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        returnToCredentialsList()
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    /**
     * {
    "id": 21,
    "name": "Asbestos Assessor Licence",
    "identifier_name": "ID Number",
    "identifier": "required",
    "front_photo": "required",
    "back_photo": "optional",
    "issue_date": "required",
    "expiry_date": "optional",
    "issued_by": "optional",
    "issuers": null,
    "rto": "optional",
    "registration_number": "none",
    "reference": "none",
    "categories": [
    "Asbestos"
    ]
    }
     *
     * */

    @Test
    fun createAsbestosLicenceTemplateCredentialAndShowOnCredentialsListTestWithRequiredAndOptionalFields(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(20, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))

        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(0, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(3, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, closeSoftKeyboard()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, closeSoftKeyboard()))

        onView(withId(R.id.creation_credential_from_save_button)).perform(scrollTo(),click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun createCustomLicenceTemplateCredentialAndShowOnCredentialsListTestWithRequiredAndOptionalFieldsTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(84, click()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        //Creation
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(scrollTo())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_front_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_creation_form_back_photo_imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, closeSoftKeyboard()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(1, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, closeSoftKeyboard()))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, closeSoftKeyboard()))
        onView(withId(R.id.create_credential_fields_list)).perform(RecyclerViewActions.actionOnItemAtPosition<CreateCredentialAdapter.EditTexViewHolder>(5, scrollTo()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(5, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(5, closeSoftKeyboard()))
        onView(withId(R.id.create_credential_fields_list)).perform(RecyclerViewActions.actionOnItemAtPosition<CreateCredentialAdapter.EditTexViewHolder>(6, scrollTo()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(6, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(6, closeSoftKeyboard()))
        onView(withId(R.id.create_credential_fields_list)).perform(RecyclerViewActions.actionOnItemAtPosition<CreateCredentialAdapter.EditTexViewHolder>(7, scrollTo()))
        onView(ActionHelper.withRecyclerView(R.id.create_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(clearText())
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(7, typeText("A12345")))
        onView((withId(R.id.create_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(7, closeSoftKeyboard()))

        onView(withId(R.id.creation_credential_from_save_button)).perform(scrollTo(),click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(isDisplayed()))
        Intents.intended(hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    private fun  returnToCredentialsList(){
        onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.tabs)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
    }

    private fun getActivity():Activity{
        lateinit var activity:Activity
        rule.getScenario().onActivity { activity = it }
        return activity
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
        Intents.release()
    }


}