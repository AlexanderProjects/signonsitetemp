package au.com.signonsitenew.uitest.passport.credentials

import android.app.Activity
import android.widget.DatePicker
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.TypeOfCredentialsListAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.tabs.TabLayout
import org.hamcrest.CoreMatchers.equalTo

import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CredentialListFragmentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    var activity: Activity? = null
    private var idlingResource: IdlingResource? = null


    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.tabs)).check(matches(isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.passport_tabsLayout)).check(matches(isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun viewCredentialsListTest(){
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ActionHelper.clickChildViewWithId(R.id.credential_type_name)))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(matches(isDisplayed()))
    }

    @Test
    fun viewCredentialsTypesTest(){
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ActionHelper.clickChildViewWithId(R.id.credential_type_name)))
        onView(ActionHelper.withRecyclerView(R.id.credential_list).atPositionOnView(0, R.id.credential_type_name)).check(matches(isDisplayed()))
        onView(withId(R.id.add_new_credential)).perform(click())
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(matches(isDisplayed()))
    }

    @Test
    fun viewNotificationBadgeInCredentials(){
        rule.getScenario().onActivity { activity = it }
        val tabLayout = activity?.findViewById<TabLayout>(R.id.passport_tabsLayout)
        val badge : BadgeDrawable? = tabLayout?.getTabAt(2)?.badge

        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.credential_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(ViewMatchers.withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.update_credential_from_save_button)).perform(ViewActions.scrollTo(), click())

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.credential_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        Assert.assertEquals(badge?.number, 1)// add one more notification

        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.credential_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, click()))
        onView(ViewMatchers.withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.update_credential_from_save_button)).perform(closeSoftKeyboard())
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView((withId(R.id.credential_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, click()))
        Assert.assertEquals(badge?.number, 1)// subtract one notification

    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }

}