package au.com.signonsitenew.uitest.passport.connections

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.adapters.SiteRecyclerViewAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.EspressoTestMatcher.withDrawable
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.adapters.ConnectionsRecyclerViewAdapter
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ConnectionsFragmentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Espresso.onView(ViewMatchers.withId(R.id.sites_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(ViewMatchers.hasDescendant(ViewMatchers.withText("Alex's House"))))
        Espresso.onView((ViewMatchers.withId(R.id.site_list))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(3))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun viewConnectionListTest(){
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ActionHelper.clickChildViewWithId(R.id.site_name_in_connection)))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(0, R.id.site_name_in_connection)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun viewEmptyConnectionListTest(){
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ActionHelper.clickChildViewWithId(R.id.site_name_in_connection)))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(0, R.id.site_name_in_connection)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView((ViewMatchers.withId(R.id.connection_list))).perform(RecyclerViewActions.actionOnItemAtPosition<ConnectionsRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        Espresso.onView(ViewMatchers.withId(R.id.site_connection_details_value)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.end_connection_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(android.R.id.button1)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun viewNotInductedConnectionTest(){

        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(0))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        Espresso.onView(ViewMatchers.withId(R.id.sign_off_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        Espresso.onView(ViewMatchers.withId(R.id.sites_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(ViewMatchers.hasDescendant(ViewMatchers.withText("Alex's House"))))
        Espresso.onView((ViewMatchers.withId(R.id.site_list))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(3))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, ActionHelper.clickChildViewWithId(R.id.site_name_in_connection)))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(1, R.id.site_name_in_connection)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(1, R.id.inducted_tick)).check(ViewAssertions.matches(withDrawable(R.drawable.ic_warning_red_24dp)))

    }

    @Test
    fun viewInductedConnectionTest(){
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(0))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        Espresso.onView(ViewMatchers.withId(R.id.sign_off_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        Espresso.onView(ViewMatchers.withId(R.id.sites_button)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.site_list)).perform(RecyclerViewActions.scrollTo<SiteRecyclerViewAdapter.ViewHolder>(ViewMatchers.hasDescendant(ViewMatchers.withText("Alex's House"))))
        Espresso.onView((ViewMatchers.withId(R.id.site_list))).perform(RecyclerViewActions.actionOnItemAtPosition<SiteRecyclerViewAdapter.ViewHolder>(0, ViewActions.click()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(3))
        Espresso.onView(ViewMatchers.withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.connection_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ActionHelper.clickChildViewWithId(R.id.site_name_in_connection)))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(0, R.id.site_name_in_connection)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ActionHelper.withRecyclerView(R.id.connection_list).atPositionOnView(0, R.id.inducted_tick)).check(ViewAssertions.matches(withDrawable(R.drawable.ic_check_circle_green_24dp)))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }


}