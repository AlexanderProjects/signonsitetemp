package au.com.signonsitenew.uitest.briefings

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper.clickChildViewWithId
import au.com.signonsitenew.helpers.ActionHelper.selectTabAtPosition
import au.com.signonsitenew.helpers.ActionHelper.waitId
import au.com.signonsitenew.helpers.ActionHelper.withRecyclerView
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class BriefingListForManagersUiTest {
    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup(){
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
    }
    @Test
    fun showBriefingListFragmentSortedByDate() {
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(selectTabAtPosition(1))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.documents_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickChildViewWithId(R.id.document_see_all_image_button_view)))
        Espresso.onView(ViewMatchers.withId(R.id.briefings_tabLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(withRecyclerView(R.id.briefing_date_list).atPositionOnView(0, R.id.content_date)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun showBriefingListFragmentSortedByContent() {
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(selectTabAtPosition(1))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(waitId(R.id.document_see_all_image_button_view, 1000))
        Espresso.onView(ViewMatchers.withId(R.id.documents_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickChildViewWithId(R.id.document_see_all_image_button_view)))
        Espresso.onView(ViewMatchers.withId(R.id.briefings_tabLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.briefings_tabLayout)).perform(selectTabAtPosition(1))
        Espresso.onView(ViewMatchers.withId(R.id.briefings_tabLayout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(withRecyclerView(R.id.briefing_date_list).atPositionOnView(0, R.id.content_date)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }
}