package au.com.signonsitenew.uitest.passport.credentials

import android.app.Activity
import android.provider.MediaStore
import android.widget.DatePicker
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.GrantPermissionRule
import androidx.test.runner.intent.IntentCallback
import androidx.test.runner.intent.IntentMonitorRegistry
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.CredentialsRecyclerViewAdapter
import au.com.signonsitenew.ui.adapters.TypeOfCredentialsListAdapter
import au.com.signonsitenew.ui.adapters.UpdateCredentialAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.CameraHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UpdateCredentialFragmentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    @get:Rule
    var runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.CAMERA)
    @get:Rule
    var storagePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    lateinit var callback: IntentCallback
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        Intents.init()
        val imgCaptureResult = CameraHelper.createImageCaptureActivityResultStub(getActivity())
        Intents.intending(IntentMatchers.hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(imgCaptureResult)
        callback = CameraHelper.intentCallback(R.drawable.camera_plus)
        IntentMonitorRegistry.getInstance().addIntentCallback(callback)
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.tabs)).check(matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.passport_tabsLayout)).check(matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential"))))
        onView(withText("Alexander Credential")).perform(click())
    }

    @Test
    fun viewCredentialDetailsTest(){
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(scrollTo())
        onView((withId(R.id.credential_list))).perform(RecyclerViewActions.actionOnItemAtPosition<CredentialsRecyclerViewAdapter.ViewHolder>(0, click()))
        onView(withId(R.id.update_credential_fields_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.update_credential_fields_list)).check(matches(isDisplayed()))
    }

    @Test
    fun updateFrontPhotoInExistingCredentialTest() {
        //update front photo
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(scrollTo())
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(click())
        onView(withId(R.id.credential_update_form_front_photo_imageView)).check(matches(isDisplayed()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential"))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential")).perform(click())
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_update_form_front_photo_imageView)).check(matches(CameraHelper.hasImageSet()))
        Intents.intended(IntentMatchers.hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun updateBackPhotoInExistingCredentialTest() {
        //update front photo
        onView(withId(R.id.credential_update_form_front_photo_imageView)).perform(scrollTo())
        onView(withId(R.id.credential_update_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_update_form_back_photo_imageView)).perform(click())
        onView(withId(R.id.credential_update_form_back_photo_imageView)).check(matches(isDisplayed()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())

        //Validation
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential"))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential")).perform(click())
        onView(withId(R.id.credential_update_form_back_photo_imageView)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_update_form_back_photo_imageView)).check(matches(CameraHelper.hasImageSet()))
        Intents.intended(IntentMatchers.hasAction(MediaStore.ACTION_IMAGE_CAPTURE))
        IntentMonitorRegistry.getInstance().removeIntentCallback(callback)
    }

    @Test
    fun updateExpiryDateInExistingCredential() {
        //update front photo
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(1, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2027,6,20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(withId(R.id.update_credential_fields_list)).perform(ActionHelper.waitUntil(isDisplayed()))
    }

    @Test
    fun updateIssueDateInExistingCredentialTest() {
        //update front photo
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(2, click()))
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(2017, 6, 20))
        onView(withId(android.R.id.button1)).perform(click())
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(withId(R.id.update_credential_fields_list)).perform(ActionHelper.waitUntil(isDisplayed()))
    }

    @Test
    fun updateIssuedByInExistingCredentialTest() {
        //update front photo
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(3, R.id.credential_field_value_edit_text)).perform(ViewActions.clearText())
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, ViewActions.typeText("A12345")))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(3, ViewActions.closeSoftKeyboard()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(withId(R.id.update_credential_fields_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(3, R.id.credential_field_value_edit_text)).check(matches(withText(containsString("A12345"))))
    }

    @Test
    fun updateRtoInExistingCredentialTest() {
        //update front photo
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).perform(ViewActions.clearText())
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, ViewActions.typeText("A12345")))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(4, ViewActions.closeSoftKeyboard()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(4, R.id.credential_field_value_edit_text)).check(matches(withText(containsString("A12345"))))
    }
    @Test
    fun updateRegistrationNumberInExistingCredentialTest() {
        //update front photo
        onView(withId(R.id.update_credential_fields_list)).perform(RecyclerViewActions.actionOnItemAtPosition<UpdateCredentialAdapter.EditTexViewHolder>(5, scrollTo()))
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(5, R.id.credential_field_value_edit_text)).perform(ViewActions.clearText())
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(5, ViewActions.typeText("A12345")))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(5, ViewActions.closeSoftKeyboard()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(5, R.id.credential_field_value_edit_text)).check(matches(withText(containsString("A12345"))))
    }
    @Test
    fun updateReferenceInExistingCredentialTest() {
        //update front photo
        onView(withId(R.id.update_credential_fields_list)).perform(RecyclerViewActions.actionOnItemAtPosition<UpdateCredentialAdapter.EditTexViewHolder>(6, scrollTo()))
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(6, R.id.credential_field_value_edit_text)).perform(ViewActions.clearText())
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(6, ViewActions.typeText("A12345")))
        onView((withId(R.id.update_credential_fields_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(6, ViewActions.closeSoftKeyboard()))
        onView(withId(R.id.update_credential_from_save_button)).perform(scrollTo(), click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))

        //Validation
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_list)).perform(RecyclerViewActions.scrollTo<CredentialsRecyclerViewAdapter.ViewHolder>(hasDescendant(withText("Alexander Credential "))))
        onView(withId(R.id.credential_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withText("Alexander Credential ")).perform(click())
        onView(ActionHelper.withRecyclerView(R.id.update_credential_fields_list).atPositionOnView(6, R.id.credential_field_value_edit_text)).check(matches(withText(containsString("A12345"))))
    }

    private fun getActivity(): Activity {
        lateinit var activity: Activity
        rule.getScenario().onActivity { activity = it }
        return activity
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
        Intents.release()
    }

}