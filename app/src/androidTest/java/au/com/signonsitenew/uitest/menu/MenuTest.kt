package au.com.signonsitenew.uitest.menu

import android.app.Instrumentation
import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.internal.inject.InstrumentationContext
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.Constants
import org.hamcrest.core.AllOf.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MenuTest{
    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = false)

    @Test
    fun clickOnHelpArticlesLink(){
        rule.launch()
        Intents.init()
        val expectedIntent = allOf(hasAction(Intent.ACTION_VIEW), hasData(Constants.URL_HELP_LINK))
        intending(expectedIntent).respondWith(Instrumentation.ActivityResult(0, null))

        Espresso.onView(ViewMatchers.withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(4))
        Espresso.onView(ViewMatchers.withId(R.id.tabs)).check(ViewAssertions.matches(ViewMatchers.isDisplayed())   )
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        Espresso.onView(ViewMatchers.withId(R.id.menu_help_cell)).perform(ViewActions.click())

        intended(expectedIntent)
        Intents.release()
    }
}