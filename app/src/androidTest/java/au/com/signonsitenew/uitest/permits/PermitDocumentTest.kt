package au.com.signonsitenew.uitest.permits


import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.ActionHelper.withRecyclerView
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.Matchers
import org.hamcrest.core.IsInstanceOf
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.DEFAULT)
class PermitDocumentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null
    private var tabs = Espresso.onView(ViewMatchers.withId(R.id.tabs))
    private var documentList = Espresso.onView(withRecyclerView(R.id.documents_list).atPositionOnView(2, R.id.document_name_text_view))

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        tabs.perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        tabs
                .perform(ActionHelper.selectTabAtPosition(1))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun showPermitsInDocsTabTest(){
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        documentList.check(ViewAssertions.matches(withText(Matchers.containsString("Permits"))))
    }

    @Test
    fun showCurrentPermits(){
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        val recyclerView2 = Espresso.onView(ViewMatchers.withId(R.id.documents_list))
        recyclerView2
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, ViewActions.click()))

        val textView = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.permit_status_title), withText("Closing"),
                        ViewMatchers.withParent(ViewMatchers.withParent(IsInstanceOf.instanceOf(FrameLayout::class.java))),
                        ViewMatchers.isDisplayed()))
        textView.check(ViewAssertions.matches(withText("Closing")))
    }

    @Test
    fun showPermitTemplates(){
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        documentList.check(ViewAssertions.matches(withText(Matchers.containsString("Permits"))))

        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(2000))
        val recyclerView2 = Espresso.onView(ViewMatchers.withId(R.id.documents_list))
        recyclerView2
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, ViewActions.click()))

        val fab = Espresso.onView(ViewMatchers.withId(R.id.new_permit_fab))
        fab.perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        val list = Espresso.onView(ViewMatchers.withId(R.id.permits_template_list))
        list.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }

}