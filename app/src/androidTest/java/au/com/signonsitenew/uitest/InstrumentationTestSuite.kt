package au.com.signonsitenew.uitest

import au.com.signonsitenew.uitest.attendanceregister.AttendanceRegisterTest
import au.com.signonsitenew.uitest.briefings.BriefingDetailsTest
import au.com.signonsitenew.uitest.briefings.BriefingListForManagersUiTest
import au.com.signonsitenew.uitest.menu.MenuTest
import au.com.signonsitenew.uitest.menu.TroubleshootingTest
import au.com.signonsitenew.uitest.passport.connections.ConnectionDetailsFragmentTest
import au.com.signonsitenew.uitest.passport.credentials.CreateCredentialFragmentTest
import au.com.signonsitenew.uitest.passport.credentials.CredentialListFragmentTest
import au.com.signonsitenew.uitest.passport.credentials.UpdateCredentialFragmentTest
import au.com.signonsitenew.uitest.passport.emergencyinfo.EmergencyInfoFragmentUiTest
import au.com.signonsitenew.uitest.passport.passport.SharePassportFragmentTest
import au.com.signonsitenew.uitest.passport.personalinfo.PersonalInfoFragmentTest
import au.com.signonsitenew.uitest.registration.RegistrationCompleteTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
        PersonalInfoFragmentTest::class,
        EmergencyInfoFragmentUiTest::class,
        CredentialListFragmentTest::class,
        CreateCredentialFragmentTest::class,
        UpdateCredentialFragmentTest::class,
        ConnectionDetailsFragmentTest::class,
        BriefingListForManagersUiTest::class,
        BriefingDetailsTest::class,
        AttendanceRegisterTest::class,
        MenuTest::class,
        TroubleshootingTest::class,
        RegistrationCompleteTest::class,
        SharePassportFragmentTest::class)

class InstrumentationTestSuite