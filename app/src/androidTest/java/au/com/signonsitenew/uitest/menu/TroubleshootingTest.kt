package au.com.signonsitenew.uitest.menu

import android.os.Build
import androidx.core.content.ContextCompat
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.GrantPermissionRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiScrollable
import androidx.test.uiautomator.UiSelector
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.EspressoTestMatcher
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.account.TroubleshootActivity
import org.hamcrest.CoreMatchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class TroubleshootingTest{

    @get:Rule
    val rule = lazyActivityScenarioRule<TroubleshootActivity>(launchActivity = false)

    @get:Rule
    var runtimePermissionCoarseLocationRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_COARSE_LOCATION)

    @get:Rule
    var runtimePermissionFineLocationRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION)



    @Test
    fun checkForLocationPermissionIsGranted(){
        rule.launch()
        Espresso.onView(ViewMatchers.withId(R.id.location_permission_row)).perform(ViewActions.click())
        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val appView = UiScrollable(UiSelector().scrollable(true))
        val selector = UiSelector()
        appView.getChildByText(selector,"Permissions")
        mDevice.findObject(selector.text("Permissions")).click()
        mDevice.findObject(selector.text("Location")).click()
        mDevice.findObject(selector.text("Allow all the time")).click()
        mDevice.pressBack()
        mDevice.pressBack()
        mDevice.pressBack()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("Always"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.green_highlight))))
        }else{
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("On"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.green_highlight))))
        }

    }

    @Test
    fun checkForLocationPermissionIsGrantedForeground(){
        rule.launch()
        Espresso.onView(ViewMatchers.withId(R.id.location_permission_row)).perform(ViewActions.click())
        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val appView = UiScrollable(UiSelector().scrollable(true))
        val selector = UiSelector()
        appView.getChildByText(selector,"Permissions")
        mDevice.findObject(selector.text("Permissions")).click()
        mDevice.findObject(selector.text("Location")).click()
        mDevice.findObject(selector.text("Allow only while using the app")).click()
        mDevice.pressBack()
        mDevice.pressBack()
        mDevice.pressBack()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("When using"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.orange_primary))))
        }else{
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("On"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.green_highlight))))
        }

    }

    @Test
    fun checkForLocationPermissionIsDenied(){
        rule.launch()
        Espresso.onView(ViewMatchers.withId(R.id.location_permission_row)).perform(ViewActions.click())
        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val appView = UiScrollable(UiSelector().scrollable(true))
        val selector = UiSelector()
        appView.getChildByText(selector,"Permissions")
        mDevice.findObject(selector.text("Permissions")).click()
        mDevice.findObject(selector.text("Location")).click()
        mDevice.findObject(selector.text("Deny")).click()
        mDevice.pressBack()
        mDevice.pressBack()
        mDevice.pressBack()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("Never"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.red_highlight))))
        }else{
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(containsString("Off"))))
            Espresso.onView(ViewMatchers.withId(R.id.location_permissions_granted_text_view)).check(ViewAssertions.matches(EspressoTestMatcher.withTextColor(ContextCompat.getColor(ApplicationProvider.getApplicationContext(),R.color.red_highlight))))
        }

    }
}