package au.com.signonsitenew.uitest.passport.personalinfo

import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.PerformException
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper.selectTabAtPosition
import au.com.signonsitenew.helpers.ActionHelper.waitFor
import au.com.signonsitenew.helpers.ActionHelper.waitUntil
import au.com.signonsitenew.helpers.Util.random
import au.com.signonsitenew.helpers.Util.randomString
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.Is
import org.hamcrest.core.IsNot
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * To run this test.It's necessary have a phone o emulator already login in with a selected site
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class PersonalInfoFragmentTest {

    private var randomField: String? = null
    private var idlingResource: IdlingResource? = null

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val firstName = Espresso.onView(withId(R.id.first_name_personal_info_edit_text))
    private val lastName = Espresso.onView(withId(R.id.last_name_personal_info_edit_text))
    private val company = Espresso.onView(withId(R.id.company_personal_info_edit_text))
    private val trade = Espresso.onView(withId(R.id.trade_personal_info_edit_text))
    private val email = Espresso.onView(withId(R.id.email_passport_textview))
    private val phone = Espresso.onView(withId(R.id.phone_personal_editText))
    private val pickerPhoneNumber = Espresso.onView(withId(R.id.passport_phone_number))
    private val pickerPhoneNumberCountrySelector = Espresso.onView(withId(R.id.recycler_countryDialog))
    private val pickerPhoneNumberEditText = Espresso.onView(withId(R.id.new_passport_phone_number))
    private val pickerPhoneNumberButton = Espresso.onView(withId(R.id.passport_ccp))
    private val pickerPhoneNumberNewButton = Espresso.onView(withId(R.id.passport_new_number_button))
    private val postcode = Espresso.onView(withId(R.id.postcode_editText))
    private val genderSpinner = Espresso.onView(withId(R.id.gender_spinner))
    private val customGender = Espresso.onView(withId(R.id.custom_gender))
    private val postCodeClearButton = Espresso.onView(withId(R.id.postcode_clear_button))
    private val genderClearButton = Espresso.onView(withId(R.id.gender_clear_img))
    private val indigenousClearButton = Espresso.onView(withId(R.id.indigenous_status_clear_img))
    private val indigenousSpinner = Espresso.onView(withId(R.id.indigenous_status_spinner))
    private val apprenticeSpinner =Espresso.onView(withId(R.id.apprentice_spinner))
    private val apprenticeClearButton = Espresso.onView(withId(R.id.apprentice_clear_img))
    private val dateOfBirthEditText = Espresso.onView(withId(R.id.dateOfBirth_editText))
    private val alertDialogOkButton = Espresso.onView(withId(android.R.id.button1))
    private val datePicker = Espresso.onView(ViewMatchers.withClassName(Matchers.equalTo(DatePicker::class.java.name)))
    private val dateOfBirthClearButton = Espresso.onView(withId(R.id.date_of_Birth_clear_button))
    private val indigenousStatusWarning = Espresso.onView(withId(R.id.indigenous_status_warning_img))
    private val genderWarning = Espresso.onView(withId(R.id.gender_warning_img))
    private val postcodeWarning = Espresso.onView(withId(R.id.postcode_warning_img))
    private val apprenticeWarning = Espresso.onView(withId(R.id.apprentice_warning_img))
    private val dateOfBirthWarning = Espresso.onView(withId(R.id.dateofbirth_warning_img))
    private val experienceWarning = Espresso.onView(withId(R.id.experience_warning_img))
    private val nativeEnglishSpeakerWarning = Espresso.onView(withId(R.id.native_english_speaker_warning_img))
    private val interpreterRequiredWarning = Espresso.onView(withId(R.id.interpreter_required_warning_img))
    private val experienceSpinner = Espresso.onView(withId(R.id.experience_spinner))
    private val experienceClearButton = Espresso.onView(withId(R.id.experience_clear_img))
    private val nativeEnglishSpeakerSpinner = Espresso.onView(withId(R.id.native_speaker_spinner))
    private val nativeEnglishSpeakerClearButton = Espresso.onView(withId(R.id.native_english_speaker_clear_img))
    private val interpreterSpinner = Espresso.onView(withId(R.id.interpreter_spinner))
    private val interpreterClearButton = Espresso.onView(withId(R.id.interpreter_required_clear_img))
    private val saveButton = Espresso.onView(withId(R.id.save_personal_info_fab))
    private val tabView = Espresso.onView(Matchers.allOf(ViewMatchers.withContentDescription("Passport"), childAtPosition(childAtPosition(withId(R.id.tabs), 0), 1), isDisplayed()))
    private val checkAlertDialogIsShown = Espresso.onView(ViewMatchers.withText(Constants.ALERT_DIALOG_MESSAGE_FOR_SAVE))
    private val tabPassport = Espresso.onView(withId(R.id.passport_tabsLayout))

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        tabView.perform(click())
        tabView.check(ViewAssertions.matches(isDisplayed()))
//        wait()
//        firstName.perform(scrollTo())
//        wait()
        firstName.check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun editFirstNameAndUpdateTest() {
        randomField = randomString()
        firstName
                .perform(clearText())
                .perform(waitUntil(isDisplayed()))
                .perform(typeText(randomField), closeSoftKeyboard())
                .perform(waitUntil(isDisplayed()))
        saveButton
                .perform(waitUntil(isDisplayed()))
                .perform(click())

        firstName
                .perform(waitUntil(isDisplayed()))
                .check(ViewAssertions.matches(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun editLastNameAndUpdateTest() {
        randomField = randomString()
        lastName
                .perform(clearText())
                .perform(waitUntil(isDisplayed()))
                .perform(typeText(randomField), closeSoftKeyboard())
                .perform(waitUntil(isDisplayed()))

        saveButton
                .perform(waitUntil(isDisplayed()))
                .perform(click())
        lastName
                .perform(waitUntil(isDisplayed()))
                .check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun editCompanyNameAndUpdateTest() {
        randomField = random()
        company
                .perform(clearText())
                .perform(typeText(randomField), closeSoftKeyboard())
        wait()
        saveButton
                .perform(click())
        company
                .perform(waitUntil(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun editTradeAndUpdateTest() {
        randomField = randomString()
        trade
                .perform(clearText())
                .perform(typeText(randomField), closeSoftKeyboard())
                .perform(waitUntil(isDisplayed()))
        saveButton
                .perform(click())
        trade
                .check(ViewAssertions.matches(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun showEmailTest() {
        email
                .check(ViewAssertions.matches(isDisplayed()))
    }
    @Test
    fun editPhoneNumberAndUpdateTest() {
        randomField = randomString()
        phone
                .perform(click())
        pickerPhoneNumber
                .check(ViewAssertions.matches(isDisplayed()))
        pickerPhoneNumberButton
                .perform(click())
        pickerPhoneNumberCountrySelector
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        pickerPhoneNumberEditText
                .perform(typeText("0434242730"), closeSoftKeyboard())
        pickerPhoneNumberNewButton
                .perform(click())
        phone
                .perform(waitUntil(isDisplayed()))
                .perform(closeSoftKeyboard())
                //.check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString("+61434242730"))))
                .perform(waitUntil(isDisplayed()))
        wait()
        saveButton
                .perform(click())
        phone
                .check(ViewAssertions.matches(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun editPostCodeAndUpdateTest() {
        postcode
                .perform(clearText())
                .perform(waitUntil(isDisplayed()))
                .perform(typeText(randomField), closeSoftKeyboard())
                .perform(waitUntil(isDisplayed()))
        saveButton
                .perform(click())
        postcode
                .check(ViewAssertions.matches(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    @Test
    fun editPostCodeClearTest() {
        postCodeClearButton
                .perform(click())
        postcode
                .check(ViewAssertions.matches(ViewMatchers.withText(Matchers.isEmptyString())))


    }
    @Test
    fun editGenderAndUpdateInfoForCustomTest() {
        randomField = randomString()
        genderSpinner
                .perform(click())
        Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>("custom"))).perform(click())

        customGender
                .perform(waitUntil(isDisplayed()))
        customGender
                .perform(clearText())
        customGender
                .perform(typeText(randomField), closeSoftKeyboard())
        saveButton
                .perform(click())
        genderSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("custom"))))

        customGender
                .check(ViewAssertions.matches(ViewMatchers.withText(randomField)))
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearGenderAndUpdateInfoForCustomTest() {
        try {
            genderSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>("female"))).perform(click())
            genderSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            genderSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("female"))))
        } catch (e: PerformException) {
            genderSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>("male"))).perform(click())
            genderSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            genderSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("male"))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        genderClearButton
                .perform(click())
        genderSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("Not provided"))))

        saveButton.perform(click())
        wait()
        saveButton.check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editGenderAndUpdateInfoForMaleFemaleTest() {
        try {
            genderSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf(String::class.java)), Matchers.`is`("female"))).perform(click())
            genderSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            genderSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("female"))))
        } catch (e: PerformException) {
            genderSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf(String::class.java)), Matchers.`is`("male"))).perform(click())
            genderSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            genderSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("male"))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editIndigenousStatusAndUpdateInfoForIndigenousTest() {
        try {
            indigenousSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.INDIGENOUS))).perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            indigenousSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.INDIGENOUS))))
        } catch (e: PerformException) {
            indigenousSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NOT_INDIGENOUS))).perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NOT_INDIGENOUS))))
        }
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearIndigenousStatusAndUpdateInfoForIndigenousTest() {
        try {
            indigenousSpinner.perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.INDIGENOUS))).perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.INDIGENOUS))))
        } catch (e: PerformException) {
            indigenousSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NOT_INDIGENOUS))).perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            indigenousSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NOT_INDIGENOUS))))
        }
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        indigenousClearButton
                .perform(click())
        indigenousSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("Not provided"))))
        saveButton.perform(click())
        wait()
        saveButton.check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editApprenticeStatusAndUpdateInfoForApprenticeTest() {
        try {
            apprenticeSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.APPRENTICE))).perform(click())

            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.APPRENTICE))))
        } catch (e: PerformException) {
            apprenticeSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NOT_APPRENTICE))).perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NOT_APPRENTICE))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearApprenticeStatusAndUpdateInfoForApprenticeTest() {
        try {
            apprenticeSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.APPRENTICE))).perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.APPRENTICE))))
        } catch (e: PerformException) {
            apprenticeSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NOT_APPRENTICE))).perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            apprenticeSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NOT_APPRENTICE))))
        }
        wait()
        saveButton
                .perform(waitUntil(IsNot.not(isDisplayed())))
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        apprenticeClearButton
                .perform(click())
        apprenticeSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("Not provided"))))
        saveButton
                .perform(click())
        wait()
        saveButton
            .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editDateOfBirthAndUpdateInfoForDOBTest() {
        wait(2)
        dateOfBirthEditText
                .perform(scrollTo())
                .perform(clearText())
                .perform(click())
        datePicker
                .perform(PickerActions.setDate(2007, 6, 20))
        alertDialogOkButton
                .perform(click())
        dateOfBirthEditText
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(waitUntil(isDisplayed()))
        saveButton
                .perform(click())
        dateOfBirthEditText
                .check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        dateOfBirthEditText.check(ViewAssertions.matches(ViewMatchers.withText("20 Jun 2007")))
    }

    @Test
    fun clearDateOfBirthAndUpdateInfoForDOBTest() {
        dateOfBirthEditText
                .perform(clearText())
                .perform(click())
        datePicker
                .perform(PickerActions.setDate(2007, 6, 20))
        alertDialogOkButton
                .perform(click())
        dateOfBirthEditText
                .check(ViewAssertions.matches(isDisplayed()))
                .perform(waitUntil(isDisplayed()))
        saveButton
                .perform(click())
        dateOfBirthEditText
                .check(ViewAssertions.matches(isDisplayed()))
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        dateOfBirthClearButton
                .perform(click())
        dateOfBirthEditText
                .check(ViewAssertions.matches(ViewMatchers.withText(Matchers.isEmptyString())))

        saveButton
                .perform(click())
        wait()
        saveButton
            .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun showMoreInfoIconDescriptionTest() {
        indigenousStatusWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.INDIGENOUS_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        postcodeWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.POSTCODE_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        genderWarning.perform(waitUntil(isDisplayed()))
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.GENDER_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        apprenticeWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.APPRENTICE_STATUS_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        dateOfBirthWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.DATE_OF_BIRTH_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        experienceWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.CONSTRUCTION_EXPERIENCE_QUESTION)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        nativeEnglishSpeakerWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.ENGLISH_NATIVE_QUESTION)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
        interpreterRequiredWarning
                .perform(click())
        Espresso.onView(ViewMatchers.withText(Constants.INTERPRETER_REQUIRED_QUESTION)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        alertDialogOkButton
                .perform(click())
    }

    @Test
    fun showAlertDialogWhenTheUserGoAwaysBeforeStoredData() {
        firstName
            .perform(clearText())
            .perform(typeText(randomString()), closeSoftKeyboard())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)

        saveButton
                .perform(click())
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        lastName
            .perform(clearText())
            .perform(typeText(randomString()), closeSoftKeyboard())

        wait()
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(click())

        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        company
                .perform(clearText())
                .perform(typeText(randomString()), closeSoftKeyboard())

        wait(1)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))

        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait()
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        trade
                .perform(clearText())
                .perform(typeText(randomString()), closeSoftKeyboard())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        phone
                .perform(click())
        wait(1)
        pickerPhoneNumber
                .check(ViewAssertions.matches(isDisplayed()))
        pickerPhoneNumberButton
                .perform(click())
        pickerPhoneNumberCountrySelector
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        pickerPhoneNumberEditText
                .perform(typeText("0434242730"), closeSoftKeyboard())
        pickerPhoneNumberNewButton
                .perform(click())

        wait(1)
        phone
            .perform(waitUntil(isDisplayed()))
            .perform(closeSoftKeyboard())
            //.check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString("+61434242730"))))
            .perform(waitUntil(isDisplayed()))

        wait(1)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))

        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        postcode
                .perform(clearText())
                .perform(typeText(randomString()), closeSoftKeyboard())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(waitUntil(isDisplayed()))
                .perform(click())

        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))

        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        genderSpinner
                .perform(click())
        Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>("male"))).perform(click())

        wait()
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))

        wait()
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        indigenousSpinner
                .perform(click())
        Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.INDIGENOUS))).perform(click())
        wait(2)
        tabPassport
                .perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        wait(1)
        apprenticeSpinner
                .perform(click())
        Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NOT_APPRENTICE))).perform(click())

        wait(2)
        tabPassport
                .perform(selectTabAtPosition(1))

        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))

        wait(1)
        alertDialogOkButton
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        wait(1)
        dateOfBirthEditText
                .perform(clearText())
                .perform(click())
        datePicker
                .perform(PickerActions.setDate(2007, 6, 20))
        alertDialogOkButton
                .perform(click())
        tabPassport.perform(selectTabAtPosition(1))
        checkAlertDialogIsShown
                .inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(isDisplayed()))
        wait(1)
        alertDialogOkButton
                .perform(waitUntil(isDisplayed()))
                .perform(click())
        wait(1)
        tabPassport
                .perform(selectTabAtPosition(0))
        wait(1)
        saveButton
                .perform(click())
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editConstructionExperienceStatusAndUpdateInfoTest() {
        try {
            experienceSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf(String::class.java)), Matchers.`is`(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            experienceSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        }
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearConstructionExperienceStatusAndUpdateInfoTest() {
        try {
            experienceSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            experienceSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            experienceSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        experienceClearButton
                .perform(click())
        wait(1)
        apprenticeSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(""))))
        saveButton.perform(click())
        wait(1)
        saveButton.check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editNativeSpeakerStatusAndUpdateInfoTest() {
        try {
            nativeEnglishSpeakerSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            nativeEnglishSpeakerSpinner.perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())

            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        }
        wait(1)
        saveButton.check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearNativeSpeakerStatusAndUpdateInfoTest() {
        try {
            nativeEnglishSpeakerSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
            nativeEnglishSpeakerSpinner
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            wait(1)
            nativeEnglishSpeakerSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
            wait(1)
            saveButton
                    .perform(click())
            nativeEnglishSpeakerSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        }
        wait(1)
        saveButton.check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        wait(1)
        nativeEnglishSpeakerClearButton
                .perform(click())
        wait(2)
        saveButton
                .perform(click())
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun editInterpreterRequiredStatusAndUpdateInfoTest() {
        try {
            interpreterSpinner.perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            interpreterSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton.perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }

    @Test
    fun clearInterpreterRequiredStatusAndUpdateInfoTest() {
        try {
            interpreterSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.POSITIVE_CONFIRMATION))).perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.POSITIVE_CONFIRMATION))))
        } catch (e: PerformException) {
            interpreterSpinner
                    .perform(click())
            Espresso.onData(allOf(Matchers.`is`(Matchers.instanceOf<Any>(String::class.java)), Matchers.`is`<String>(Constants.NEGATIVE_CONFIRMATION))).perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
            saveButton
                    .perform(click())
            interpreterSpinner
                    .perform(waitUntil(isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(Constants.NEGATIVE_CONFIRMATION))))
        }
        wait(1)
        saveButton
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
        interpreterClearButton
                .perform(click())
        wait(1)
        interpreterSpinner
                .check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString("Not provided"))))
        saveButton
                .perform(click())
                .check(ViewAssertions.matches(IsNot.not(isDisplayed())))
    }
    /**
     * Brent reported case - When the user is editing first name o last name and press clear button in any field the save button disappear
     * **/
    @Test
    fun clickOnClearWhileSaveButtonIsStillShowing(){
        randomField = randomString()
        firstName
                .perform(clearText())
                .perform(waitUntil(isDisplayed()))
                .perform(typeText(randomField), closeSoftKeyboard())
                .perform(waitUntil(isDisplayed()))

        firstName
                .perform(waitUntil(isDisplayed()))
                .check(ViewAssertions.matches(isDisplayed()))

        postCodeClearButton
                .perform(click())
        postcode
                .check(ViewAssertions.matches(ViewMatchers.withText(Matchers.isEmptyString())))

        saveButton
                .check(ViewAssertions.matches(Is(isDisplayed())))
    }


    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

    private fun wait(seconds:Int = 2) = Espresso.onView(ViewMatchers.isRoot()).perform(waitFor(seconds*1000.toLong()))
}