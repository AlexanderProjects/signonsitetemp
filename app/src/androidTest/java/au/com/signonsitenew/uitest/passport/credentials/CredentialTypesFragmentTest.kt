package au.com.signonsitenew.uitest.passport.credentials

import android.widget.AutoCompleteTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import au.com.signonsitenew.R
import au.com.signonsitenew.ui.adapters.TypeOfCredentialsListAdapter
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.containsString
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CredentialTypesFragmentTest {

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)
    private var idlingResource: IdlingResource? = null

    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)

        onView(withId(R.id.tabs)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.tabs)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.passport_tabsLayout)).perform(ActionHelper.selectTabAtPosition(2))
        onView(withId(R.id.passport_tabsLayout)).check(ViewAssertions.matches(isDisplayed()))
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(withId(R.id.add_new_credential)).perform(ViewActions.click())
    }

    @Test
    fun showListOfCredentialTypesTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun searchForAnyTypeOfCredentialTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.action_search)).perform(ViewActions.click())
        onView(isRoot()).perform(ActionHelper.waitFor(1000))
        onView(isAssignableFrom(AutoCompleteTextView::class.java)).perform(typeText("site"), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(ActionHelper.withRecyclerView(R.id.credential_types_list).atPositionOnView(0, R.id.credential_type_name)).check(ViewAssertions.matches(withText(containsString("Sitesafe"))))
    }

    @Test
    fun showCreateCredentialTemplateTest(){
        onView(withId(R.id.credential_types_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.credential_types_list)).check(ViewAssertions.matches(isDisplayed()))
        onView((withId(R.id.credential_types_list))).perform(RecyclerViewActions.actionOnItemAtPosition<TypeOfCredentialsListAdapter.ViewHolder>(0, ViewActions.click()))
        onView(withId(R.id.create_credential_fields_list)).perform(ActionHelper.waitUntil(isDisplayed()))
        onView(withId(R.id.create_credential_fields_list)).check(ViewAssertions.matches(isDisplayed()))
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }
}