package au.com.signonsitenew.uitest.registration

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import au.com.signonsitenew.utilities.Constants
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class RegisterPhoneTest{

    @get:Rule
    var activityRule = IntentsTestRule(RegisterActivity::class.java)

    @Before
    fun setup(){
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_first_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ViewActions.replaceText("Parra"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_last_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ViewActions.replaceText("alexanderparrap@msn.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_email_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun checkPhoneNumberIsValidLocally(){
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ViewActions.replaceText("0434242730"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_phone_number_formatted_error)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun checkPhoneNumberIsINValidLocally(){
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ViewActions.replaceText("043"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_phone_number_formatted_error)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun failApiValidationResponse(){
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ViewActions.replaceText("043"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_phone_number_formatted_error)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText(Constants.PHONE_DATA_FORMAT_ERROR)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(android.R.id.button1)).perform(ViewActions.click())
    }

}