package au.com.signonsitenew.uitest.registration

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper.waitUntil
import au.com.signonsitenew.helpers.Util
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import au.com.signonsitenew.utilities.Constants
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class RegisterDetailsTest {

    @get:Rule
    var activityRule = IntentsTestRule(RegisterActivity::class.java)

    @Test
    fun enterValidValuesFormatSuccessful(){
        onView(withId(R.id.firstNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).perform(ViewActions.typeText("Alexander"), closeSoftKeyboard())
        onView(withId(R.id.wrong_first_name_format)).check(matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.lastNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).perform(ViewActions.typeText("Parra"), closeSoftKeyboard())
        onView(withId(R.id.wrong_last_name_format)).check(matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.emailRegEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).perform(ViewActions.typeText("alexanderparrap@msn.com"), closeSoftKeyboard())
        onView(withId(R.id.wrong_email_name_format)).check(matches(not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun enterValidValuesFormatFailResponse(){
        onView(withId(R.id.firstNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).perform(ViewActions.typeText("Alexander*?"), closeSoftKeyboard())
        onView(withId(R.id.wrong_first_name_format)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.wrong_first_name_format)).check(matches((ViewMatchers.isDisplayed())))

        onView(withId(R.id.lastNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).perform(ViewActions.typeText("Parra)"), closeSoftKeyboard())
        onView(withId(R.id.wrong_last_name_format)).check(matches((ViewMatchers.isDisplayed())))

        onView(withId(R.id.emailRegEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).perform(ViewActions.typeText("alexanderparrapmsn.com"), closeSoftKeyboard())
        onView(withId(R.id.wrong_email_name_format)).check(matches((ViewMatchers.isDisplayed())))
    }

    @Test
    fun validateValuesWithNotInternet(){
        Util.setAirplaneMode(true)

        onView(withId(R.id.firstNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.firstNameEditText)).perform(replaceText("Alexander"), closeSoftKeyboard())
        onView(withId(R.id.wrong_first_name_format)).check(matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.lastNameEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lastNameEditText)).perform(ViewActions.typeText("Parra"), closeSoftKeyboard())
        onView(withId(R.id.wrong_last_name_format)).check(matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.emailRegEditText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.emailRegEditText)).perform(ViewActions.typeText("alexanderparrap@msn.com"), closeSoftKeyboard())
        onView(withId(R.id.wrong_email_name_format)).check(matches(not(ViewMatchers.isDisplayed())))

        onView(withId(R.id.submitDetailsButton)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        onView(withText(Constants.NETWORK_MESSAGE_ERROR)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(android.R.id.button1)).perform(ViewActions.click())

        Util.setAirplaneMode(false)
        Util.restartSignOnSiteApp(InstrumentationRegistry.getInstrumentation(), activityRule)
    }
}