package au.com.signonsitenew.uitest.registration

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.ui.prelogin.registration.RegisterActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class RegisterEmployerTest {
    @get:Rule
    var activityRule = IntentsTestRule(RegisterActivity::class.java)

    @Before
    fun setup(){
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.firstNameEditText)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_first_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.lastNameEditText)).perform(ViewActions.replaceText("Parra"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_last_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.emailRegEditText)).perform(ViewActions.replaceText("alexanderparrap@msn.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.wrong_email_name_format)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.phoneEditText)).perform(ViewActions.replaceText("0434242730"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitDetailsButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))

        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText1)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.newPasswordEditText2)).perform(ViewActions.replaceText("Alexander"), ViewActions.closeSoftKeyboard())

        Espresso.onView(ViewMatchers.withId(R.id.submitPasswordButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.submitPasswordButton)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
    }

    @Test
    fun selectACompanyNameTest(){
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ActionHelper.waitUntil(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).perform(ViewActions.typeText("SignOnSite"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.isRoot()).perform(ActionHelper.waitFor(1000))
        onData(Matchers.instanceOf("SignOnSite"::class.java)).inRoot(RootMatchers.withDecorView(not(Matchers.`is`(activityRule.activity.window.decorView)))).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.employer_auto_complete_text_view)).check(ViewAssertions.matches(ViewMatchers.withText(Matchers.containsString("Sos"))))
    }

}