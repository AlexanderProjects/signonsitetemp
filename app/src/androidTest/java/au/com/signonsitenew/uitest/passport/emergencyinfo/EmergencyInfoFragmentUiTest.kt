package au.com.signonsitenew.uitest.passport.emergencyinfo

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import au.com.signonsitenew.R
import au.com.signonsitenew.helpers.ActionHelper
import au.com.signonsitenew.helpers.ActionHelper.waitFor
import au.com.signonsitenew.helpers.ActionHelper.waitUntil
import au.com.signonsitenew.helpers.Util
import au.com.signonsitenew.ui.main.MainActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.core.AllOf
import org.hamcrest.core.IsNot
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class EmergencyInfoFragmentUiTest {
    private var randomField: String? = null
    private var idlingResource: IdlingResource? = null
    private val tabs = onView(withId(R.id.tabs))
    private val passportTabLayout = onView(withId(R.id.passport_tabsLayout))
    private val contactNameEmergency = onView(withId(R.id.contact_name_emergency_editText))
    private val saveButton = onView(withId(R.id.save_emergency_info_fab))
    private val contactPhoneNumber = onView(withId(R.id.contact_phone_emergency_editText))
    private val contactPhoneNumberPicker = onView(withId(R.id.passport_phone_number))
    private val contactPhoneNumberCountry = onView(withId(R.id.passport_ccp))
    private val contactCountryPicker = onView(withId(R.id.recycler_countryDialog))
    private val newContactPhoneNumber = onView(withId(R.id.new_passport_phone_number))
    private val newContactPhoneNumberButton = onView(withId(R.id.passport_new_number_button))
    private val secondContactNameEmergency = onView(withId(R.id.second_contact_name_emergency_editText))

    @get:Rule
    var activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setup() {
        randomField = Util.random()
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        wait(3)
        tabs
                .perform(ActionHelper.selectTabAtPosition(1))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        wait()

        passportTabLayout
                .perform(ActionHelper.selectTabAtPosition(1))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        wait()
    }

    @Test
    fun showPrimaryContactNameInEmergencyInfoAndUpdateContactNameTest() {
        contactNameEmergency
                .perform(ViewActions.scrollTo())
                .perform(ViewActions.clearText())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
                .perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        saveButton
                .perform(ViewActions.click())
        contactNameEmergency
                .perform(ViewActions.scrollTo())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.contact_name_emergency_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        saveButton
                .check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun showPrimaryContactPhoneNumberInEmergencyInfoAndUpdateContactPhoneNumberTest() {
        contactPhoneNumber
                .perform(ViewActions.scrollTo())
                .perform(ViewActions.click())
        contactPhoneNumberPicker
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        contactPhoneNumberCountry
                .perform(ViewActions.click())
        contactCountryPicker
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))
        newContactPhoneNumber
                .perform(ViewActions.replaceText("0434242730"), ViewActions.closeSoftKeyboard())
        newContactPhoneNumberButton
                .perform(ViewActions.click())
        contactPhoneNumber
                .perform(waitUntil(ViewMatchers.isDisplayed()))
                .perform(ViewActions.closeSoftKeyboard())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        saveButton
                .perform(ViewActions.click())
        contactPhoneNumber
                .perform(ViewActions.scrollTo())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.contact_phone_emergency_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        saveButton.check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun showSecondaryContactNameInEmergencyInfoAndUpdateContactName() {
        secondContactNameEmergency
                .perform(ViewActions.scrollTo())
                .perform(ViewActions.clearText())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
                .perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        saveButton
                .perform(ViewActions.click())
        secondContactNameEmergency
                .perform(ViewActions.scrollTo())
                .perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.second_contact_name_emergency_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        saveButton
                .check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))

    }

    @Test
    fun clearSecondaryContactNameInEmergency() {
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(ViewActions.clearText())
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.save_emergency_info_fab)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.second_contact_name_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.second_contact_name_emergency_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.second_contact_name_clear_icon)).perform(ViewActions.click())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.save_emergency_info_fab)).check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))

    }

    @Test
    fun showSecondaryContactPhoneNumberInEmergencyInfoAndUpdateContactPhoneNumberTest() {
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(ViewActions.click())
        onView(withId(R.id.passport_phone_number)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.passport_ccp)).perform(ViewActions.click())
        onView(withId(R.id.recycler_countryDialog)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))
        onView(withId(R.id.new_passport_phone_number)).perform(ViewActions.typeText("0434242730"), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.passport_new_number_button)).perform(ViewActions.click())
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.second_phone_number_emergency_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.second_phone_number_emergency_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.save_emergency_info_fab)).check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))
    }


    @Test
    fun showMedicalInformationInEmergencyInfoAndUpdateMedicalInformationTest() {
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.clearText())
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.medical_info_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.save_emergency_info_fab)).perform(waitUntil(IsNot.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun clearMedicalInformationInEmergencyInfoAndUpdateMedicalInformationTest() {
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.clearText())
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.typeText(randomField), ViewActions.closeSoftKeyboard())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(2000))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(withId(R.id.medical_info_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.medical_info_editText)).perform(waitUntil(ViewMatchers.isDisplayed()))
        onView(AllOf.allOf(withId(R.id.medical_info_editText))).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.clear_medical_info_clear_img)).perform(ViewActions.click())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.save_emergency_info_fab)).perform(ViewActions.click())
        onView(ViewMatchers.isRoot()).perform(waitFor(2000))
        onView(withId(R.id.save_emergency_info_fab)).check(ViewAssertions.matches(IsNot.not(ViewMatchers.isDisplayed())))
    }

    @Test
    fun showWarningIconInTheFieldsWhenNotData() {
        onView(withId(R.id.contact_name_emergency_editText)).perform(ViewActions.scrollTo())
        onView(withId(R.id.contact_name_emergency_editText)).perform(ViewActions.clearText())
        onView(withId(R.id.contact_name_warning_img)).perform(ViewActions.scrollTo())
        onView(withId(R.id.contact_name_warning_img)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun showMoreInfoIconDescriptionTest() {
        onView(ViewMatchers.isRoot()).perform(waitFor(2000))
        onView(withId(R.id.emergency_warning_img)).perform(ViewActions.scrollTo())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.emergency_warning_img)).perform(ViewActions.click())
        onView(ViewMatchers.withText(Constants.EMERGENCY_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(android.R.id.button1)).perform(ViewActions.click())
        onView(ViewMatchers.isRoot()).perform(waitFor(2000))
        onView(withId(R.id.medical_info_warning_img)).perform(ViewActions.scrollTo())
        onView(ViewMatchers.isRoot()).perform(waitFor(1000))
        onView(withId(R.id.medical_info_warning_img)).perform(ViewActions.click())
        onView(ViewMatchers.withText(Constants.MEDICAL_INFORMATION_MESSAGE)).inRoot(RootMatchers.isDialog()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(android.R.id.button1)).perform(ViewActions.click())
    }

    @After
    fun tearDown() {
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }

    }

    private fun wait(seconds:Int = 2) = onView(ViewMatchers.isRoot()).perform(waitFor(seconds*1000.toLong()))
}