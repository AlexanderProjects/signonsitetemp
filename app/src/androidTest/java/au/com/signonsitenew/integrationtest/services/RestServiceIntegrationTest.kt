package au.com.signonsitenew.integrationtest.services

import android.app.Activity
import android.content.Context
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.rule.GrantPermissionRule
import au.com.signonsitenew.data.factory.datasources.net.RestService
import au.com.signonsitenew.di.components.DaggerApplicationTestComponent
import au.com.signonsitenew.di.modules.NetTestModule
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.helpers.Util
import au.com.signonsitenew.helpers.lazyActivityScenarioRule
import au.com.signonsitenew.ui.main.SignedOnActivity
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.empty
import au.com.signonsitenew.utilities.espresso.TestIdlingResource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.*
import retrofit2.Retrofit
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RestServiceIntegrationTest {

    @get:Rule
    var storagePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

    @get:Rule
    val rule = lazyActivityScenarioRule<SignedOnActivity>(launchActivity = true)

    private var idlingResource: IdlingResource? = null

    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    lateinit var retrofitWithNull:Retrofit
    @Inject
    lateinit var retrofitForMap:Retrofit


    private val disposables = CompositeDisposable()
    private val latch = CountDownLatch(1)
    private val token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzMwMDg5MDMsImV4cCI6MTg4ODM2ODkwMywidCI6ImxvZ2luIiwic3ViIjozLCJpc3MiOiJodHRwczpcL1wvdGVzdC5zaWdub25zaXRlLmNvbS5hdSJ9.Cs86uYQkX_OAv8ZUMXmNpnwB6QDucuufjp-df_hJSaw"
    private val siteId = "1" // This id side correspond to SignOnSite
    private val userId = "3"
    private val email = "alexander.parra@signonsite.com.au"
    private lateinit var restService: RestService
    private lateinit var context:Context
    @Before
    fun setup() {
        idlingResource = TestIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResource)
        val component = DaggerApplicationTestComponent
                .builder()
                .netModule(NetTestModule()).build()
        component.inject(this)
        restService = RestService(retrofit, retrofitWithNull, retrofitForMap)
        context = getActivity()
    }


    @Test
    fun testUserInfoForPassportWithSuccessfulResponseRx() {
        disposables.add(restService[userId, token]
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
        }, Throwable::printStackTrace))
        latch.await(2000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun testCredentialsWithSuccessfulResponse() {
        disposables.add(restService.getCredentials(userId, token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(2000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun testCredentialTypesSuccessfulResponse(){
        disposables.add(restService.getCredentialTypes(userId)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(false,response.credential_types.isNullOrEmpty())
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(4000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun testUploadImageForCredential(){
        val fileName = "testImage"
        disposables.add(restService.uploadImages(Util.createImageFile(fileName,context),fileName)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({response ->
                    Assert.assertNotNull(response)
                    Assert.assertNotEquals(false, response.access_key.isNotEmpty())
                    Assert.assertNotEquals(false, response.access_url.isNotEmpty())
                },Throwable::printStackTrace))
    }

    @Test
    fun testCreateCredentialSuccessfulResponse(){
        disposables.add(restService.updateCredentials(Util.buildUpdateCredentialRequest(),userId, token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                },Throwable::printStackTrace))
    }

    @Test
    fun deleteCredentialSuccessfulResponseTest(){
        disposables.add(restService.createCredentials(Util.buildCreateCredential(),userId,token)
                .flatMap { resp -> restService.deleteCredential(resp.credential_id.toString(),token) }
                .observeOn(Schedulers.newThread())
                .subscribe ({response ->
                    Assert.assertEquals("success", response.status)
                    latch.countDown()
                },Throwable::printStackTrace))

        latch.await(3000, TimeUnit.MILLISECONDS)

    }

    @Test
    fun getConnectionsSuccessfulResponseTest(){
        disposables.add(restService.getEnrolments(userId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe ({ response ->
                    Assert.assertEquals("success", response.status)
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun featureFlagsResponseTest(){
        disposables.add(restService.getFeatureFlags(userId, token)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    Assert.assertEquals(Constants.PASSPORTS,response.user_feature_flags[0].feature_flag_name)
                    latch.countDown()
                }, Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun startEvacuationEndPointTest(){
        disposables.add(restService.startEvacuation(token,email,Constants.AUTH_TOKEN,siteId,true)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread()).subscribe({
                    Assert.assertNotNull(it)
                    Assert.assertEquals(it.status, "success")
                },Throwable::printStackTrace))
    }

    @Test
    fun stopEvacuationEndPointTest(){
        disposables.add(restService.stopEvacuation(token,email,Constants.AUTH_TOKEN,siteId,buildStopEvacuationRequest()).observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread()).subscribe({
                    Assert.assertNotNull(it)
                    Assert.assertEquals(it.status, "success")
                },Throwable::printStackTrace))
    }
    @Test
    fun evacuationVisitorTest(){
        disposables.add(restService.visitorEvacuation(token,email,Constants.AUTH_TOKEN,siteId)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun attendanceEndPointTest(){
        val dateTimeToday = DateTime.now(DateTimeZone.forID("Australia/Sydney"))
        val utcDateTimeToday = dateTimeToday.toDateTime(DateTimeZone.UTC).withTimeAtStartOfDay().toLocalDateTime()
        val utcDateTimeTomorrow = utcDateTimeToday.plusDays(1)
        disposables.add(restService.attendees(siteId,token,utcDateTimeToday.toString(),utcDateTimeTomorrow.toString(),true)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun getCurrentPermit(){
        disposables.add(restService.getCurrentPermits(siteId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun getGetPermitInfo(){
        val permitId = 313
        disposables.add(restService.getPermitInfo(permitId.toString(),token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }
    @Test
    fun getGetCurrentPermits(){
        disposables.add(restService.getCurrentPermits(siteId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }
    @Test
    fun getGetPermitTemplates(){
        disposables.add(restService.getPermitTemplates(siteId,token, false)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun savePermit(){
        var permitId = String().empty()
        disposables.add(restService.createPermit(CreatePermitRequest(1, listOf(1),RequesteeStates.all_invited,DateTime.now().plusHours(1).toString(), DateTime.now().plusHours(2).toString()),permitId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    permitId = response.permit_id
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }


    @Test
    fun getEnrolledUsers(){
        disposables.add(restService.getEnrolledUsers(token,true,siteId,email)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }

    @Test
    fun updatePermit(){
        var permitId = String().empty()
        disposables.add(restService.createPermit(CreatePermitRequest(1, listOf(1),RequesteeStates.all_invited,DateTime.now().plusHours(1).toString(), DateTime.now().plusHours(2).toString()),permitId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    permitId = response.permit_id
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)

        disposables.add(restService.updatePermit(UpdatePermitInfoRequest("pending_approval",null,DateTime.now().plusHours(1).toString(), DateTime.now().plusHours(2).toString()),permitId,token)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Assert.assertNotNull(response)
                    Assert.assertEquals(response.status, "success")
                    latch.countDown()
                },Throwable::printStackTrace))
        latch.await(8000, TimeUnit.MILLISECONDS)
    }




    @After
    fun destroy(){
        idlingResource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
        disposables.clear()
    }

    private fun buildStopEvacuationRequest():List<StopEvacuationRequest>{
        val requestList = arrayListOf<StopEvacuationRequest>()
        val request = StopEvacuationRequest(userId.toLong(),true)
        requestList.add(request)
        return requestList
    }

    private fun getActivity(): Activity {
        lateinit var activity: Activity
        rule.getScenario().onActivity { activity = it }
        return activity
    }

}