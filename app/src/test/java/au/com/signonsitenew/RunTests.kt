package au.com.signonsitenew

import au.com.signonsitenew.services.AnalyticsServiceUnitTest
import au.com.signonsitenew.services.RestServiceUnitTest
import au.com.signonsitenew.usecases.*
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    AnalyticsServiceUnitTest::class,
    RestServiceUnitTest::class,
    AnalyticsEventUseCaseTest::class,
    AttendanceRegisterUseCaseTest::class,
    EvacuationUseCaseTest::class,
    IntroPassportUseCaseImplTest::class,
    ListOfConnectionsUseCaseTest::class,
    NotificationUseCaseTest::class,
    PermitTemplatesUseCaseTest::class,
    PermitTeamTabUseCaseTest::class,
    PermitUseCaseTest::class,
    PermitTaskTabUseCaseTest::class,
    PermitCtaContextualUseCaseTest::class,
    PermitContentTypeItemTabUseCaseTest::class,
    PermitCheckTabUseCaseTabTest::class,
    SharePassportUseCaseImplTest::class,
    UpdateEnrolmentUseCaseImplTest::class)
class RunTests {
}