package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.usecases.notifications.NotificationUseCase
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.times


class NotificationUseCaseTest {

    private lateinit var notificationUseCase: NotificationUseCase

    @Before
    fun setup(){
        notificationUseCase = Mockito.mock(NotificationUseCase::class.java)
    }

    @Test
    fun `push remote notification briefing update`(){
        Mockito.`when`(notificationUseCase.showBriefingRemoteNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(notificationUseCase)
        notificationUseCase.showBriefingRemoteNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
        Mockito.verify(notificationUseCase, times(1)).showBriefingRemoteNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
    }

    @Test
    fun `push remote notification start evacuation`(){
        Mockito.`when`(notificationUseCase.evacuationStartedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(notificationUseCase)
        notificationUseCase.evacuationStartedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
        Mockito.verify(notificationUseCase, times(1)).evacuationStartedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
    }

    @Test
    fun `push remote notification stop evacuation`(){
        Mockito.`when`(notificationUseCase.evacuationEndedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(notificationUseCase)
        notificationUseCase.evacuationEndedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
        Mockito.verify(notificationUseCase, times(1)).evacuationEndedNotification(ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any())
    }

}