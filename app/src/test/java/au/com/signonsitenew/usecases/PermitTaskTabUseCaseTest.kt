package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.repository.DataRepository
import au.com.signonsitenew.domain.usecases.permits.PermitTaskTabUseCase
import au.com.signonsitenew.domain.usecases.permits.PermitTaskTabUseCaseImpl
import au.com.signonsitenew.events.RxBusTaskTab
import au.com.signonsitenew.utilities.SessionManager
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PermitTaskTabUseCaseTest {

    private lateinit var permitTaskTabUseCase : PermitTaskTabUseCase
    private lateinit var sessionManager: SessionManager
    private lateinit var rxBusTaskTab: RxBusTaskTab
    private lateinit var repository: DataRepository

    @Before
    fun setup(){
        sessionManager = mock()
        rxBusTaskTab = mock()
        repository = mock()
        permitTaskTabUseCase = PermitTaskTabUseCaseImpl(sessionManager ,repository ,rxBusTaskTab)
    }

    @Test
    fun `parse iso dates to String and it is formatted hh mm a, dd MMMM yyyy`(){
        val startDateTime = "2021-06-14T10:53:00+00:00"
        val endDateTime = "2021-06-14T10:58:00+00:00"
        val stringStartDateTime = permitTaskTabUseCase.parseIsoFormatDates(startDateTime)
        val stringEndDateTime = permitTaskTabUseCase.parseIsoFormatDates(endDateTime)
        Assert.assertNotNull(stringStartDateTime)
        Assert.assertNotNull(stringEndDateTime)
    }

    @Test
    fun `parse calendar dates to String and it is formatted hh mm a, dd MMMM yyyy`(){
        val startDateTime = "Wed Jun 16 07:48:45 GMT+00:00 2021"
        val endDateTime = "Wed Jun 16 07:48:45 GMT+00:00 2021"
        val stringStartDateTime = permitTaskTabUseCase.parseCalendarDates(startDateTime)
        val stringEndDateTime = permitTaskTabUseCase.parseCalendarDates(endDateTime)
        Assert.assertNotNull(stringStartDateTime)
        Assert.assertNotNull(stringEndDateTime)
    }

    @Test
    fun `convert ui datetime to iso date time format`(){
        val dateTime = "07:57 AM, 16 June 2021"
        val stringDateTime = permitTaskTabUseCase.convertUIDateTimeToISODateTimeFormat(dateTime)
        Assert.assertNotNull(stringDateTime)
    }


}