package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.CtaContextualButtonFragmentState
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.usecases.permits.PermitCtaContextualUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.util.MockResponseFileReader
import au.com.signonsitenew.util.RxTrampolineSchedulerRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import org.joda.time.DateTime
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PermitCtaContextualUseCaseTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxTrampolineSchedulerRule()

    private lateinit var permitCtaContextualUseCase : PermitCtaContextualUseCaseImpl
    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var permitInfoResponse : PermitInfoResponse
    private lateinit var permitGenericResponse : ApiResponse
    private lateinit var permitInfo: PermitInfo
    private lateinit var componentList:List<ComponentTypeForm>
    private lateinit var listOfTeamMembers:List<RequesteeUser>
    private lateinit var tasks:List<ContentTypeItem>
    private lateinit var requestChecks:List<ContentTypeItem>
    private lateinit var inProgressChecks:List<ContentTypeItem>
    private lateinit var closingChecks:List<ContentTypeItem>


    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        permitCtaContextualUseCase = Mockito.spy(PermitCtaContextualUseCaseImpl(sessionManager,repository))
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory()).build()
        val moshiPermitAdapter = moshi.adapter(PermitInfoResponse::class.java)
        val moshiGenericResponseAdapter = moshi.adapter(ApiResponse::class.java)
        permitInfoResponse = moshiPermitAdapter.fromJson(MockResponseFileReader("GetFullPermitResponse.json").content)!!
        permitGenericResponse = moshiGenericResponseAdapter.fromJson(MockResponseFileReader("PermitGenericResponse.json").content)!!

        tasks = listOf(ContentTypeItem(id = 198,name = "cool",type = "text_input",is_mandatory = true,responses = listOf()),
                ContentTypeItem(id = 199,name = "cool",type = "text_input",is_mandatory = false,responses = listOf()),
                ContentTypeItem(id = 200,name = "cool",type = "checkbox",is_mandatory = true,responses = listOf()))
        requestChecks = listOf(ContentTypeItem(id = 196,name = "cool",type = "text_input",is_mandatory = false,responses = listOf()),
                ContentTypeItem(id = 201,name = "cool",type = "text_input",is_mandatory = false,responses = listOf()),
                ContentTypeItem(id = 202,name = "cool",type = "checkbox",is_mandatory = true,responses = listOf()))
        inProgressChecks = listOf(ContentTypeItem(id = 195,name = "cool",type = "text_input",is_mandatory = true,responses = listOf()),
                ContentTypeItem(id = 203,name = "cool",type = "text_input",is_mandatory = true,responses = listOf()),
                ContentTypeItem(id = 204,name = "cool",type = "checkbox",is_mandatory = true,responses = listOf()))
        closingChecks = listOf(ContentTypeItem(id = 194,name = "cool",type = "text_input",is_mandatory = true,responses = listOf()),
                ContentTypeItem(id = 205,name = "cool",type = "text_input",is_mandatory = true,responses = listOf()),
                ContentTypeItem(id = 206,name = "cool",type = "checkbox",is_mandatory = false,responses = listOf()))

        componentList = listOf(ComponentTypeForm(form_input_id =195,value = "alex"),
                ComponentTypeForm(form_input_id =198,value = "alex"),
                ComponentTypeForm(form_input_id = 199, value = "bla bla"),
                ComponentTypeForm(form_input_id = 200,value = "alex"),
                ComponentTypeForm(form_input_id = 202, value = "bla bla"),
                ComponentTypeForm(form_input_id = 203,value = "alex"),
                ComponentTypeForm(form_input_id = 204, value = true),
                ComponentTypeForm(form_input_id =205,value = "alex"))
        listOfTeamMembers = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.PreRequest),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.PendingApproval, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
                PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), listOfTeamMembers,null,null, tasks, requestChecks, inProgressChecks, closingChecks)


    }

    @Test
    fun `Map permit state for cta test and show obtain approval button when there are not selected users`(){

        //Given
        val listOfTeamMembers = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.PreRequest),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), listOfTeamMembers ,DateTime.now().plusHours(1).toString(),DateTime.now().plusHours(2).toString(), tasks, requestChecks, inProgressChecks, closingChecks)
        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(listOfTeamMembers)

        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()
        whenever(sessionManager.userId).thenReturn("1")

        //When
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowObtainApprovalButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in request state and show send to team button when there are some selected users but user have not updated the permit`(){
        //Given
        val listOfTeamMembers = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.PreRequest),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
                PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), listOfTeamMembers,DateTime.now().plusHours(1).toString(),DateTime.now().plusHours(2).toString(), tasks, requestChecks, inProgressChecks, closingChecks)

        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(listOfTeamMembers)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()

        //When
        val state =  permitCtaContextualUseCase.mapForNewPermitStateForCta(listOfTeamMembers.isNotEmpty(),permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowSendToTeamButton)
        verify(permitCtaContextualUseCase).mapForNewPermitStateForCta(true,permitInfo)

    }

    @Test
    fun `Map permit in request state and show obtain approval disable when there are some selected users but user have not updated the permit`(){
        //Given
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
                PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), listOfTeamMembers,DateTime.now().plusHours(1).toString(),DateTime.now().plusHours(2).toString(), tasks, requestChecks, inProgressChecks, closingChecks)

        val newListOfTeamMembers = listOf(RequesteeUser(5,"Pancho","Parra","0434242730","",PermitDoneState.PreRequest),RequesteeUser(6,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(newListOfTeamMembers)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()
        whenever(sessionManager.userId).thenReturn("1")

        //When
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowObtainApprovalButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in request state and show done when the user is on the request list`(){

        //Given
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")), RequesterUser(3,"Mateo","CaraDePeo",null, SiteCompany("SOS")), listOfTeamMembers,null,null, tasks, requestChecks, inProgressChecks, closingChecks)

        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(listOfTeamMembers)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()
        whenever(sessionManager.userId).thenReturn("1")

        //When
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowDoneInRequestButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in pending approval state and show approve and reject button when the user is manager`(){

        //Given
        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(listOfTeamMembers)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()
        whenever(sessionManager.userId).thenReturn("1")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(1,1)).thenReturn(true)

        //When
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowApproveRejectButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in progress state and show send for closure button when all the workers finish work and it does not have empty mandatory fields`(){
        //Given
        val teamMembersList = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.FinishedWork),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.InProgress, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), teamMembersList,null,null, tasks, requestChecks, inProgressChecks, closingChecks)

        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(teamMembersList)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()

        whenever(sessionManager.userId).thenReturn("1")
        //When
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowSendForClosureButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in progress state and show send for closure disable button when some the worker has not finished work and it does not have empty mandatory fields`(){
        //Given
        val teamMembersList = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.FinishedWork),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedRequest))
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.InProgress, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
                PermitUser(id = 1,"Alex","Parra")), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), teamMembersList,null,null, tasks, requestChecks, inProgressChecks, closingChecks)

        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setListOfMembers(teamMembersList)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()

        whenever(sessionManager.userId).thenReturn("1")
        //When
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowSendForClosureDisableButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `Map permit in progress state and show done in progress button for a worker`(){

        //Given
        val teamMembersList = listOf(RequesteeUser(2,"Pancho","Parra","0434242730","",PermitDoneState.FinishedRequest),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork))
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.InProgress, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")), RequesterUser(3,"Mateo","CaraDePeo",null, SiteCompany("SOS")), teamMembersList,"2021-05-11T04:04:04+00:00","2021-05-11T04:04:04+00:00", tasks, requestChecks, inProgressChecks, closingChecks)
        whenever(sessionManager.userId).thenReturn("1")

        //When
        permitCtaContextualUseCase.setListOfMembers(teamMembersList)
        permitCtaContextualUseCase.setPermitInfo(permitInfo)
        permitCtaContextualUseCase.setOriginalPermitInfo(permitInfo)
        componentList.forEach { permitCtaContextualUseCase.setComponentTypeForm(it) }
        permitCtaContextualUseCase.checkForEmptyMandatoryFields()
        val state =  permitCtaContextualUseCase.mapPermitStateForCta(permitInfo)

        //Then
        assertThat(state).isEqualTo(CtaContextualButtonFragmentState.ShowDoneInProgressButton)
        verify(permitCtaContextualUseCase).mapPermitStateForCta(permitInfo)

    }

    @Test
    fun `get full permit info from the api `(){
        //Given
        Mockito.`when`(repository.getPermitInfo(any(), any())).thenReturn(Single.just(permitInfoResponse))
        //When
        val subscriber = permitCtaContextualUseCase.getPermitInfoAsync("333").test()
        //Then
        verify(permitCtaContextualUseCase, times(1)).getPermitInfoAsync("333")
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
            it.permit.id.toString() == "333"
        }
    }


    @Test
    fun `update an existing permit`(){
        val listOfTeamMembers = listOf(RequesteeUser(1,"Alexander","Parra","0434242730","",PermitDoneState.PreRequest),RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.PreRequest))
        val request = UpdatePermitInfoRequest()
        request.requestee_users = listOfTeamMembers.map { it.id }
        Mockito.`when`(repository.updatePermit(any(),any(), any())).thenReturn(Single.just(permitGenericResponse))
        //When
        val subscriber = permitCtaContextualUseCase.updateCurrentPermitAsync(request,"333").test()
        //Then
        verify(permitCtaContextualUseCase, times(1)).updateCurrentPermitAsync(request,"333")
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
        }
    }


}