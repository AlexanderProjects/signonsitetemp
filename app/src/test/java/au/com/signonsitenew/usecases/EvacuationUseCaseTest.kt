package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.EvacuationVisitorsResponse
import au.com.signonsitenew.domain.models.StopEvacuationRequest
import au.com.signonsitenew.domain.usecases.evacuation.EvacuationUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.util.RxSchedulerOverrideRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class EvacuationUseCaseTest {

    companion object
    {
        @ClassRule @JvmField
        val schedulers = RxSchedulerOverrideRule()
    }
    private lateinit var sessionManager:SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var response: ApiResponse
    lateinit var evacuationUseCase: EvacuationUseCaseImpl

    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        evacuationUseCase = EvacuationUseCaseImpl(repository, sessionManager)
        val dictionary = hashMapOf<String, String>()
        dictionary[Constants.USER_EMAIL] = Constants.USER_TEST_EMAIL
        whenever(sessionManager.currentUser).thenReturn(dictionary)
        whenever(sessionManager.siteId).thenReturn(Constants.SITE_TEST_ID)
        response = ApiResponse(status = Constants.JSON_SUCCESS)
    }

    @Test
    fun `start evacuation use case test with actual emergency set true`(){
        whenever(repository.startEvacuation(any(), any(), any(), any(), any())).thenReturn(Single.just(response))
        val subscriber =  evacuationUseCase.starEvacuation(true).test()
        subscriber.assertValue { it.status == Constants.JSON_SUCCESS }
    }

    @Test
    fun `stop evacuation use case test`(){
        whenever(repository.stopEvacuation(any(), any(), any(), any(), any())).thenReturn(Single.just(response))
        val subscriber = evacuationUseCase.stopEvacuation(buildStopEvacuationRequest()).test()
        subscriber.assertValue { it.status == Constants.JSON_SUCCESS }
    }

    @Test
    fun `evacuation visitor use case test`(){
        val response = mock<EvacuationVisitorsResponse>()
        whenever(response.status).thenReturn(Constants.JSON_SUCCESS)
        whenever(repository.visitorEvacuation(any(), any(), any(), any())).thenReturn(Single.just(response))
        val subscriber = evacuationUseCase.visitorEvacuation().test()
        subscriber.assertValue { it.status == Constants.JSON_SUCCESS }
    }

    private fun buildStopEvacuationRequest():List<StopEvacuationRequest>{
        val requestList = arrayListOf<StopEvacuationRequest>()
        val request = StopEvacuationRequest(Constants.USER_TEST_ID.toLong(), true)
        requestList.add(request)
        return requestList
    }
}