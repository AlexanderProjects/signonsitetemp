package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.EnrolledUsersResponse
import au.com.signonsitenew.domain.models.PermitInfoResponse
import au.com.signonsitenew.domain.models.RequesteeUser
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.usecases.permits.PermitTeamTabUseCaseImpl
import au.com.signonsitenew.util.MockResponseFileReader
import au.com.signonsitenew.util.RxTrampolineSchedulerRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.nhaarman.mockitokotlin2.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PermitTeamTabUseCaseTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxTrampolineSchedulerRule()

    private lateinit var permitTeamTabUseCaseImpl: PermitTeamTabUseCaseImpl
    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private var selectedUsers = mutableListOf<RequesteeUser>()
    private lateinit var permitInfoResponse: PermitInfoResponse
    private lateinit var enrolledUsersResponse: EnrolledUsersResponse
    private var permitId = 0

    @Before
    fun setup() {
        sessionManager = mock()
        repository = mock()
        permitTeamTabUseCaseImpl = Mockito.spy(PermitTeamTabUseCaseImpl(sessionManager, repository))
        val users = RequesteeUser(
            id = 1,
            first_name = "Alex",
            last_name = "Parra",
            phone_number = "0434242730",
            status = PermitDoneState.PreRequest
        )
        selectedUsers.add(users)
        permitId = 333
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory()).build()
        val moshiPermitAdapter = moshi.adapter(PermitInfoResponse::class.java)
        val moshiEnrolledUserAdapter = moshi.adapter(EnrolledUsersResponse::class.java)
        permitInfoResponse =
            moshiPermitAdapter.fromJson(MockResponseFileReader("GetFullPermitResponse.json").content)!!
        enrolledUsersResponse =
            moshiEnrolledUserAdapter.fromJson(MockResponseFileReader("EnrolledUsersSuccessfulResponse.json").content)!!
    }


    @Test
    fun `get full permit info from the api`() {
        //Given
        `when`(repository.getPermitInfo(any(), any())).thenReturn(Single.just(permitInfoResponse))
        //When
        val subscriber = permitTeamTabUseCaseImpl.getPermitInfoAsync(permitId.toString()).test()
        //Then
        verify(permitTeamTabUseCaseImpl, times(1)).getPermitInfoAsync(permitId.toString())
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
            it.permit.id == permitId
        }
    }

    @Test
    fun `get enrolled users in the site`() {
        //Given
        val dictionary = hashMapOf<String, String>()
        dictionary[Constants.USER_EMAIL] = Constants.USER_TEST_EMAIL
        `when`(sessionManager.currentUser).thenReturn(dictionary)
        `when`(sessionManager.siteId).thenReturn(Constants.SITE_TEST_ID)
        `when`(
            repository.getEnrolledUsers(
                anyString(),
                anyBoolean(),
                anyString(),
                anyString()
            )
        ).thenReturn(Single.just(enrolledUsersResponse))
        //When
        val subscriber = permitTeamTabUseCaseImpl.getEnrolledUsersAsync().test()
        //Then
        verify(permitTeamTabUseCaseImpl, times(1)).getEnrolledUsersAsync()
        subscriber.assertValue { it.status == Constants.JSON_SUCCESS }
    }

    @Test
    fun `map enrolled user to permit request list`() {
        //Given
        whenever(sessionManager.userId).thenReturn(Constants.USER_TEST_ID.toString())
        //When
        val listOfRequestees =
            permitTeamTabUseCaseImpl.mapEnrolledUserToRequesteeUser(enrolledUsersResponse.users)
        //Then
        Assert.assertEquals(selectedUsers[0].first_name, listOfRequestees[1].first_name)
        Assert.assertEquals(selectedUsers[0].last_name, listOfRequestees[1].last_name)
    }
}