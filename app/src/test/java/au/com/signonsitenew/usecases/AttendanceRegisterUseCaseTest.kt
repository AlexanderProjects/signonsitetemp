package au.com.signonsitenew.usecases

import au.com.signonsitenew.SOSApplicationTest
import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.Company
import au.com.signonsitenew.domain.models.adapters.AttendanceChildAdapterModel
import au.com.signonsitenew.domain.models.adapters.AttendanceParentAdapterModel
import au.com.signonsitenew.domain.models.attendees.*
import au.com.signonsitenew.domain.models.state.AttendanceAlertState
import au.com.signonsitenew.domain.services.analytics.AnalyticsEventDelegateService
import au.com.signonsitenew.domain.usecases.attendance.AttendanceRegisterUseCaseImpl
import au.com.signonsitenew.domain.usecases.featureflag.FeatureFlagsUseCase
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.util.RxTrampolineSchedulerRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.datadog.android.log.Logger
import com.nhaarman.mockitokotlin2.*
import com.nhaarman.mockitokotlin2.any
import io.reactivex.Single
import org.joda.time.DateTime
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.junit.MockitoJUnitRunner
import org.robolectric.annotation.Config


@RunWith(MockitoJUnitRunner::class)
@Config(sdk = [Config.OLDEST_SDK], application = SOSApplicationTest::class)
class AttendanceRegisterUseCaseTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxTrampolineSchedulerRule()

    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var response: ApiResponse
    private lateinit var attendanceRegisterUseCase: AttendanceRegisterUseCaseImpl
    private lateinit var analyticsEventDelegateService: AnalyticsEventDelegateService
    private lateinit var attendanceRegisterUseCaseMock: AttendanceRegisterUseCaseImpl
    private lateinit var list :List<Attendee>
    private lateinit var logger: Logger
    private lateinit var featureFlagsUseCase: FeatureFlagsUseCase


    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        list = mock()
        analyticsEventDelegateService = mock()
        logger = mock()
        featureFlagsUseCase = mock()
        attendanceRegisterUseCase = AttendanceRegisterUseCaseImpl(repository,sessionManager,analyticsEventDelegateService)
        attendanceRegisterUseCaseMock = mock()
        response = mock()
        val dictionary = hashMapOf<String, String>()
        dictionary[Constants.USER_EMAIL] = Constants.USER_TEST_EMAIL
        whenever(sessionManager.siteId).thenReturn(Constants.SITE_TEST_ID)
    }

    @Test
    fun `get attendees list test`(){

        val workerNotesList = mutableListOf<WorkerNotes>()
        workerNotesList.add(WorkerNotes(1,"alert", CreatedByUser(1,"Alexander","Parra"),"2020-12-01","Todos al carajo que esto va explotar"))
        val attendanceList = mutableListOf<Attendance>()
        attendanceList.add(Attendance(2, DateTime.now().toString(), DateTime.now().plusHours(8).toString(),"cualquiera", Company(3,"SignOnSite"),"acknowledge"))

        val childListAdapter = mutableListOf<AttendanceChildAdapterModel>()
        childListAdapter.add(AttendanceChildAdapterModel("Alex",1,"0434242730","13:02","17:04","pending",true,1,"acknowledge","xxx","SignOnSite",AttendanceAlertState.Alert(),attendanceList,workerNotesList))
        val parentListOfAttendee = mutableListOf<AttendanceParentAdapterModel>()
        parentListOfAttendee.add(AttendanceParentAdapterModel("SignOnSite","1",childListAdapter,AttendanceAlertState.Alert()))


        val list = mutableListOf<Attendee>()
        list.add(Attendee(1,"Alex","Parra","0434242730",true,true, SiteInduction(3,"xxx",23,"pending"),attendanceList,workerNotesList))
        val response= AttendeesResponse(list,"success")

        whenever(repository.attendees(anyString(), anyString(), any(), any(), anyBoolean())).thenReturn(Single.just(response))

        val subscriber = attendanceRegisterUseCase.buildGetAttendeesAsyncCall().test()
        subscriber.assertValue{
            it.status == Constants.JSON_SUCCESS
            attendanceRegisterUseCase.mapAttendeeToAttendanceAdapterParentModel(it).isNotEmpty()
        }

    }
}