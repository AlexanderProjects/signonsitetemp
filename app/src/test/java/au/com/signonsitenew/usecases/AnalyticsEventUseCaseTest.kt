package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.models.analytics.Options
import au.com.signonsitenew.domain.usecases.analytics.AnalyticsEventUseCase
import com.segment.analytics.Properties
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AnalyticsEventUseCaseTest {

    private lateinit var analyticsEventUseCase: AnalyticsEventUseCase
    private lateinit var options: Options

    @Before
    fun setup(){
        analyticsEventUseCase = mock(AnalyticsEventUseCase::class.java)
        options = Options(hasSiteContext = false,
                hasCompanyContext = false,
                hasUserCompanyId = true,
                hasSiteOwnerCompanyId = false,
                hasMetadata = false)

    }

    @Test
    fun `send track events use case test`(){
        val prop = Properties()
        prop["test_value"] = "value"
        Mockito.doNothing().`when`(analyticsEventUseCase).sendTrackEvent("test event",prop,options,false)
        analyticsEventUseCase.sendTrackEvent("test event", prop, options,false)
        Mockito.verify(analyticsEventUseCase, times(1)).sendTrackEvent("test event", prop,options,false)
    }

    @Test
    fun `send screen events use case test`(){
        val prop = Properties()
        prop["test_value"] = "value"
        Mockito.doNothing().`when`(analyticsEventUseCase).sendScreenEvent("test event",prop,options,true)
        analyticsEventUseCase.sendScreenEvent("test event", prop,options,true)
        Mockito.verify(analyticsEventUseCase, times(1)).sendScreenEvent("test event", prop,options,true)
    }



}