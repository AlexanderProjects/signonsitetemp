package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitChecksState
import au.com.signonsitenew.domain.models.state.PermitContentTypeState
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.usecases.permits.PermitsChecksTabUseCase
import au.com.signonsitenew.domain.usecases.permits.PermitsChecksTabUseCaseImpl
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PermitCheckTabUseCaseTabTest {

    private lateinit var permitChecksUseCase: PermitsChecksTabUseCase
    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var permitInfo: PermitInfo

    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        permitChecksUseCase = Mockito.spy(PermitsChecksTabUseCaseImpl(sessionManager, repository))
        val requestChecks = mutableListOf<ContentTypeItem>()
        requestChecks.add(ContentTypeItem(1,"alsdkjfalsdfkj", Constants.PERMIT_CONTENT_TYPE_PLAIN_TEXT, false, mutableListOf()))
        val inProgressChecks = mutableListOf<ContentTypeItem>()
        inProgressChecks.add(ContentTypeItem(2, "asdmflasdkfmlas", Constants.PERMIT_CONTENT_TYPE_TEXT_INPUT, false, mutableListOf()))
        val closingChecks = mutableListOf<ContentTypeItem>()
        inProgressChecks.add(ContentTypeItem(2, "asdmflasdkfmlas", Constants.PERMIT_CONTENT_TYPE_CHECKBOX, false, mutableListOf()))
        permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test", PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
                PermitUser(id = 1,"Alex","Parra")), RequesterUser(2,"Alexander","Parra",null, SiteCompany("SOS")), mutableListOf(),null,null, mutableListOf(), requestChecks, inProgressChecks, closingChecks)
    }

    @Test
    fun `In permit state Request get request check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.ReadableAndEditable)
    }

    @Test
    fun `In permit state Request get request check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.ReadableAndEditable)
    }
    @Test
    fun `In permit state Request get request check state for a worker with work done true`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        val requesteelist = mutableListOf<RequesteeUser>()
        requesteelist.add(RequesteeUser(1,"Alex", "parra", "0434","SOS",status = PermitDoneState.FinishedRequest))
        permitInfo.requestee_users = requesteelist
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }

    @Test
    fun `In permit state Request get in progress check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }

    @Test
    fun `In permit state Request get in progress check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state Request get in progress check state for a worker with work done true`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        val requesteelist = mutableListOf<RequesteeUser>()
        requesteelist.add(RequesteeUser(1,"Alex", "parra", "0434","SOS",status = PermitDoneState.FinishedRequest))
        permitInfo.requestee_users = requesteelist
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state Request get closing check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state Request get closing check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state Request get closing check state for a worker with work done true`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        val requesteelist = mutableListOf<RequesteeUser>()
        requesteelist.add(RequesteeUser(1,"Alex", "parra", "0434","SOS",status = PermitDoneState.FinishedRequest))
        permitInfo.requestee_users = requesteelist
        //When
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get request check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pendingApproval get request check state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingApproval
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pendingApproval get request check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pendingApproval get in progress check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get in progress check state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingApproval
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get in progress check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get closing check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get closing state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingApproval
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pendingApproval get closing check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingApproval
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.GreyedOut)
    }
    @Test
    fun `In permit state pending closure get request check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get request check state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingClosure
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get request check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getRequestChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get in progress check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get in progress check state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingClosure
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get in progress check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getInProgressChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get closing check state for a requester`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }
    @Test
    fun `In permit state pending closure get closing check state for a manager`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(2,1)).thenReturn(true)
        permitInfo.state = PermitState.PendingClosure
        permitInfo.requester_user = RequesterUser(3,"pepito","perez","0435242730",SiteCompany("SOS"))
        permitChecksUseCase.setPermitInfo(permitInfo)
        //When
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.ReadableAndEditable)
    }
    @Test
    fun `In permit state pending closure get closing check state for a worker`(){
        //Given
        whenever(sessionManager.userId).thenReturn("2")
        permitInfo.requester_user = RequesterUser(1,"pepito","perez","0435242730",SiteCompany("SOS"))
        //When
        permitInfo.state = PermitState.PendingClosure
        permitChecksUseCase.setPermitInfo(permitInfo)
        val permitContentTypeState = permitChecksUseCase.getClosingChecksSectionState()
        //Then
        Truth.assertThat(permitContentTypeState).isEqualTo(PermitContentTypeState.Readable)
    }

}