package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitDetailsFragmentState
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.models.state.PermitState
import au.com.signonsitenew.domain.usecases.permits.PermitsUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.util.MockResponseFileReader
import au.com.signonsitenew.util.RxTrampolineSchedulerRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import au.com.signonsitenew.utilities.Util
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*


@RunWith(MockitoJUnitRunner::class)
class PermitUseCaseTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxTrampolineSchedulerRule()

    private lateinit var permitUseCase : PermitsUseCaseImpl
    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var response:PermitResponse
    private lateinit var errorResponse:String
    private lateinit var errorThrowable: ApiResponse
    private lateinit var authErrorResponse: ApiResponse
    private lateinit var permitGenericResponse : ApiResponse

    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        permitUseCase = Mockito.spy(PermitsUseCaseImpl(sessionManager,repository))
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(PermitDoneState::class.java, EnumJsonAdapter.create(PermitDoneState::class.java))
            .add(PermitType::class.java, EnumJsonAdapter.create(PermitType::class.java))
            .add(PermitState::class.java, EnumJsonAdapter.create(PermitState::class.java))
            .add(Util.SerializeNulls.JSON_ADAPTER_FACTORY)
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .build()
        val moshiAdapter = moshi.adapter(PermitResponse::class.java)
        val errorAdapter = moshi.adapter(ApiResponse::class.java)
        val moshiGenericResponseAdapter = moshi.adapter(ApiResponse::class.java)
        response = moshiAdapter.fromJson(MockResponseFileReader("CurrentPermitsResponse.json").content)!!
        errorResponse = MockResponseFileReader("permitErrorResponse.json").content
        errorThrowable = errorAdapter.fromJson(MockResponseFileReader("permitErrorResponse.json").content)!!
        authErrorResponse = errorAdapter.fromJson(MockResponseFileReader("permitAuthErrorResponse.json").content)!!
        permitGenericResponse = moshiGenericResponseAdapter.fromJson(MockResponseFileReader("PermitGenericResponse.json").content)!!
    }

    @Test
    fun `Get current permits test with success response`(){
        whenever(repository.getCurrentPermits(any(), any())).thenReturn(Single.just(response))
        val subscriber = permitUseCase.retrieveCurrentPermitsAsync()
                   .test()
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
        }
        subscriber.assertValue { it.permits.isNotEmpty() }
    }

    @Test
    fun `Get current permits with fail response`(){
        whenever(repository.getCurrentPermits(any(), any())).thenReturn(Single.error(Throwable(message = errorThrowable.status)))
        val subscriber = permitUseCase.getCurrentPermitsAsync().test()
        subscriber.assertError{ !it.message.isNullOrEmpty()}
        subscriber.assertError{ it.message == "404_not_found"}
    }

    @Test
    fun `Get  authentication error response`(){
        whenever(repository.getCurrentPermits(any(), any())).thenReturn(Single.error(Throwable(message = authErrorResponse.status)))
        val subscriber = permitUseCase.getCurrentPermitsAsync().test()
        subscriber.assertError{ !it.message.isNullOrEmpty() }
        subscriber.assertError{ it.message == "bad_auth"}
    }

    @Test
    fun `Sort permit list by id when the last id is first`(){
        val sortedList = permitUseCase.sortPermitListByIdDescending(response.permits)
        Assert.assertEquals(sortedList.first().id,sortedList[0].id)
    }

    @Test
    fun `map permit state to toolbar permit state and show toolbar when is permit request and the user is requester`(){
        //Given
        val listOfTeamMembers = listOf(
            RequesteeUser(1,"Alexander","Parra","0434242730","",PermitDoneState.PreRequest),
            RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork)
        )
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.Request, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")
        ), RequesterUser(1,"Alexander","Parra",null, SiteCompany("SOS")), listOfTeamMembers,null,null, mutableListOf(), mutableListOf(), mutableListOf(), mutableListOf())
        whenever(sessionManager.userId).thenReturn("1")

        //When
        val state = permitUseCase.mapPermitInfoToToolbarState(permitInfo)

        //Then
        Truth.assertThat(state).isEqualTo(PermitDetailsFragmentState.ShowPermitToolbar)
        verify(permitUseCase).mapPermitInfoToToolbarState(permitInfo)
    }

    @Test
    fun `map permit state to toolbar permit state and show toolbar when is permit pending approval and the user is manager`(){
        //Given
        val listOfTeamMembers = listOf(
            RequesteeUser(1,"Alexander","Parra","0434242730","",PermitDoneState.PreRequest),
            RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork)
        )
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.PendingApproval, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")
        ), RequesterUser(2,"Pancho","Parra",null, SiteCompany("SOS")), listOfTeamMembers,null,null, mutableListOf(), mutableListOf(), mutableListOf(), mutableListOf())
        whenever(sessionManager.userId).thenReturn("1")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(1,1)).thenReturn(true)

        //When
        val state = permitUseCase.mapPermitInfoToToolbarState(permitInfo)

        //Then
        Truth.assertThat(state).isEqualTo(PermitDetailsFragmentState.ShowPermitToolbar)
        verify(permitUseCase).mapPermitInfoToToolbarState(permitInfo)
    }

    @Test
    fun `map permit state to toolbar permit state and show toolbar when is permit in progress and the user is manager`(){
        //Given
        val listOfTeamMembers = listOf(
            RequesteeUser(1,"Alexander","Parra","0434242730","",PermitDoneState.PreRequest),
            RequesteeUser(3,"Pancho2","Parra","0434242730","",PermitDoneState.FinishedWork)
        )
        val permitInfo = PermitInfo(id = 1, human_id = "P1-1", state = PermitState.InProgress, permit_template = PermitTemplate(id = 1, "P1-1","test",PermitType.Crane,null,"2021-03-30T04:04:04+00:00",
            PermitUser(id = 1,"Alex","Parra")
        ), RequesterUser(2,"Pancho","Parra",null, SiteCompany("SOS")), listOfTeamMembers,"2021-05-11T04:04:04+00:00",null, mutableListOf(), mutableListOf(), mutableListOf(), mutableListOf())
        whenever(sessionManager.userId).thenReturn("1")
        whenever(sessionManager.siteId).thenReturn(1)
        whenever(repository.hasManagerPermission(1,1)).thenReturn(true)

        //When
        val state = permitUseCase.mapPermitInfoToToolbarState(permitInfo)

        //Then
        Truth.assertThat(state).isEqualTo(PermitDetailsFragmentState.ShowPermitToolbar)
        verify(permitUseCase).mapPermitInfoToToolbarState(permitInfo)
    }

    @Test
    fun `update permit state when the permit state is request and the user clicks on cancel button in the toolbar`(){
        whenever(repository.updatePermit(any(), any(), any())).thenReturn(Single.just(permitGenericResponse))
        permitUseCase.setPermitInfo(mock())
        val subscriber = permitUseCase.updateCurrentPermitAsync(UpdatePermitInfoRequest(permitUseCase.mapPermitStateTitle(PermitState.Request),null,null,null))
            .test()
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
        }
    }

    @Test
    fun `update permit state when the permit state is in progress and the user clicks on cancel button in the toolbar`(){
        whenever(repository.updatePermit(any(), any(), any())).thenReturn(Single.just(permitGenericResponse))
        permitUseCase.setPermitInfo(mock())
        val subscriber = permitUseCase.updateCurrentPermitAsync(UpdatePermitInfoRequest(permitUseCase.mapPermitStateTitle(PermitState.InProgress),null,null,null))
            .test()
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
        }
    }


}