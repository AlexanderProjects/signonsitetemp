package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.models.User
import au.com.signonsitenew.domain.usecases.passport.IntroPassportUseCaseImpl
import au.com.signonsitenew.domain.models.UserInfoResponse
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class IntroPassportUseCaseImplTest {

    private lateinit var introPassportUseCaseImpl: IntroPassportUseCaseImpl
    private val token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzMwMDg5MDMsImV4cCI6MTg4ODM2ODkwMywidCI6ImxvZ2luIiwic3ViIjozLCJpc3MiOiJodHRwczpcL1wvdGVzdC5zaWdub25zaXRlLmNvbS5hdSJ9.Cs86uYQkX_OAv8ZUMXmNpnwB6QDucuufjp-df_hJSaw"
    private val userId = "3"
    private val enrolmentId = "40"

    @Before
    fun setup(){
        introPassportUseCaseImpl = mock(IntroPassportUseCaseImpl::class.java)
    }

    @Test
    fun `validate first time user, this test check if the user has passport enable and also feature flag enable`(){
        //setup
        val user = User()
        user.has_passport = true
        val response = UserInfoResponse(status = "success",user = user)
        Mockito.`when`(introPassportUseCaseImpl.validateFirstTimeUser()).thenReturn(Single.just(response))
        //validate
        val subscriber = introPassportUseCaseImpl.validateFirstTimeUser().test()
        subscriber.assertValue{
            it.user.has_passport == true
        }
    }


}