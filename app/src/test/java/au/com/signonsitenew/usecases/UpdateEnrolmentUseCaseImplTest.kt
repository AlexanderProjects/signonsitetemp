package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.models.EnrolmentUpdateResponse
import au.com.signonsitenew.domain.usecases.connections.UpdateEnrolmentUseCaseImpl
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock


class UpdateEnrolmentUseCaseImplTest {

    private lateinit var updateEnrolmentUseCaseImpl: UpdateEnrolmentUseCaseImpl
    private val token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzMwMDg5MDMsImV4cCI6MTg4ODM2ODkwMywidCI6ImxvZ2luIiwic3ViIjozLCJpc3MiOiJodHRwczpcL1wvdGVzdC5zaWdub25zaXRlLmNvbS5hdSJ9.Cs86uYQkX_OAv8ZUMXmNpnwB6QDucuufjp-df_hJSaw"
    private val userId = "3"
    private val enrolmentId = "40"

    @Before
    fun init() {
        updateEnrolmentUseCaseImpl = mock(UpdateEnrolmentUseCaseImpl::class.java)
    }

    @Test
    fun `update enrolment`(){

        val updateEnrolmentUpdateResponse = EnrolmentUpdateResponse(status = "successful")

        Mockito.`when`(updateEnrolmentUseCaseImpl.updateEnrolment(enrolmentId)).thenReturn(Single.just(updateEnrolmentUpdateResponse))

        val subscriber = updateEnrolmentUseCaseImpl.updateEnrolment(enrolmentId).test()
        subscriber.assertValue {
            it.status === "successful"
        }

    }
}