package au.com.signonsitenew.usecases

import au.com.signonsitenew.data.SosDataRepository
import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.models.state.PermitDoneState
import au.com.signonsitenew.domain.usecases.permits.PermitTemplateUseCaseImpl
import au.com.signonsitenew.domain.models.ApiResponse
import au.com.signonsitenew.util.MockResponseFileReader
import au.com.signonsitenew.util.RxTrampolineSchedulerRule
import au.com.signonsitenew.utilities.Constants
import au.com.signonsitenew.utilities.SessionManager
import com.nhaarman.mockitokotlin2.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PermitTemplatesUseCaseTest {
    @Rule
    @JvmField
    var testSchedulerRule = RxTrampolineSchedulerRule()

    private lateinit var permitTemplateUseCase : PermitTemplateUseCaseImpl
    private lateinit var sessionManager: SessionManager
    private lateinit var repository: SosDataRepository
    private lateinit var response: SitePermitTemplateResponse
    private lateinit var errorResponse:String
    private lateinit var errorThrowable: ApiResponse
    private lateinit var authErrorResponse: ApiResponse
    private lateinit var savePermitResponse: SavePermitResponse
    private lateinit var permitInfoResponse : PermitInfoResponse

    @Before
    fun setup(){
        sessionManager = mock()
        repository = mock()
        permitTemplateUseCase = PermitTemplateUseCaseImpl(sessionManager,repository)
        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory()).build()
        val moshiAdapter = moshi.adapter(SitePermitTemplateResponse::class.java)
        val errorAdapter = moshi.adapter(ApiResponse::class.java)
        val moshiPermitAdapter = moshi.adapter(PermitInfoResponse::class.java)
        val moshiSavePermitAdapter = moshi.adapter(SavePermitResponse::class.java)
        response = moshiAdapter.fromJson(MockResponseFileReader("permitTemplateSuccessfulResponse.json").content)!!
        errorResponse = MockResponseFileReader("permitErrorResponse.json").content
        errorThrowable = errorAdapter.fromJson(MockResponseFileReader("permitErrorResponse.json").content)!!
        authErrorResponse = errorAdapter.fromJson(MockResponseFileReader("permitAuthErrorResponse.json").content)!!
        savePermitResponse = moshiSavePermitAdapter.fromJson(MockResponseFileReader("SavePermitResponse.json").content)!!
        permitInfoResponse = moshiPermitAdapter.fromJson(MockResponseFileReader("GetFullPermitResponse.json").content)!!
    }

    @Test
    fun `Get template permits test with success response`(){
        whenever(repository.getPermitTemplates(any(), any(), any())).thenReturn(Single.just(response))
        val subscriber = permitTemplateUseCase.getSitePermitTemplateAsync()
                .test()
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
        }
        subscriber.assertValue { it.site_permit_templates.isNotEmpty() }
    }

    @Test
    fun `Get template permits with fail response`(){
        whenever(repository.getPermitTemplates(any(), any(), any())).thenReturn(Single.error(Throwable(message = errorThrowable.status)))
        val subscriber = permitTemplateUseCase.getSitePermitTemplateAsync().test()
        subscriber.assertError{ !it.message.isNullOrEmpty()}
        subscriber.assertError{ it.message == "404_not_found"}
    }

    @Test
    fun `Get  authentication error response`(){
        whenever(repository.getPermitTemplates(any(), any(), any())).thenReturn(Single.error(Throwable(message = authErrorResponse.status)))
        val subscriber = permitTemplateUseCase.getSitePermitTemplateAsync().test()
        subscriber.assertError{ !it.message.isNullOrEmpty() }
        subscriber.assertError{ it.message == "bad_auth"}
    }

    @Test
    fun `Create a new permit once the user clicks on a specific template with a successful response`(){
        //Given
        Mockito.`when`(repository.createPermit(any(),any(), any())).thenReturn(Single.just(savePermitResponse))
        //When
        val template = PermitTemplate(1,"P1-1","nice template", PermitType.ConfinedSpace,null,"2021-03-30T04:04:04+00:00",
            PermitUser(1,"Alex","Parra")
        )
        val listOfTeamMembers = listOf(
            RequesteeUser(1,"Alexander","Parra","0434242730","",
                PermitDoneState.PreRequest),
            RequesteeUser(3,"Pancho2","Parra","0434242730","", PermitDoneState.PreRequest)
        )
        val subscriber = permitTemplateUseCase.createPermitAsync(template, listOfTeamMembers,null,null).test()

        //Then
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
            it.permit_id == "333"
        }
    }

    @Test
    fun `retrieve permit info from the api `(){
        //Given
        Mockito.`when`(repository.getPermitInfo(any(), any())).thenReturn(Single.just(permitInfoResponse))
        //When
        val subscriber = permitTemplateUseCase.retrievePermitInfoAsync("333").test()
        //Then
        subscriber.assertValue {
            it.status == Constants.JSON_SUCCESS
            it.permit.id.toString() == "333"
        }
    }

}