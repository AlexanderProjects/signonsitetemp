package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.models.*
import au.com.signonsitenew.domain.usecases.connections.ConnectionListUseCaseImpl
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import java.util.*

class ListOfConnectionsUseCaseTest{

    private lateinit var connectionListUseCaseImpl: ConnectionListUseCaseImpl

    private val token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzMwMDg5MDMsImV4cCI6MTg4ODM2ODkwMywidCI6ImxvZ2luIiwic3ViIjozLCJpc3MiOiJodHRwczpcL1wvdGVzdC5zaWdub25zaXRlLmNvbS5hdSJ9.Cs86uYQkX_OAv8ZUMXmNpnwB6QDucuufjp-df_hJSaw"
    private val userId = "3"

    @Before
    fun init() {
        connectionListUseCaseImpl = mock(ConnectionListUseCaseImpl::class.java)
    }

//    @Test
//    fun `get list of enrolments`(){
//        val company = Company(name = "SignOnSite",id = 12345)
//        val site = Site(id = 1234,name = "SignOnSite",address = "1 Moore St", principal_company = company,timezone = "Australia/Sydney",is_site_induction_forms_enabled = true)
//        val siteInduction = SiteInduction(person_type = "worker", state = "pending")
//        val enrolmentObject = Enrolment(id = 123,company = company,is_automatic_attendance_enabled = true, site = site, utc_last_signon_at = Calendar.getInstance().time,is_hidden = true, site_induction = siteInduction)
//        val listOfEnrolment = mutableListOf<Enrolment>()
//        listOfEnrolment.add(enrolmentObject)
//        val enrolmentResponse = EnrolmentResponse(status = "successful",enrolments = listOfEnrolment)
//
//        Mockito.`when`(connectionListUseCase.buildGetEnrollmentsAsyncCall().thenReturn(Single.just(enrolmentResponse))
//
//        val subscriber = connectionListUseCase.retrieveListOfEnrolments(userId, token).test()
//        subscriber.assertValue {
//            it.status === "successful"
//            it.enrolments[0].company.name === "SignOnSite"
//            it.enrolments[0].site.name === "SignOnSite"
//        }
//    }

    @Test
    fun `list of enrolments only has to show the sites with is_automatic_attendance_enabled equal true and is hidden equal false values`(){
        val company = Company(name = "SignOnSite",id = 12345)
        val site = Site(id = 1234,name = "SignOnSite",address = "1 Moore St", principal_company = company,timezone = "Australia/Sydney",is_site_induction_forms_enabled = true)
        val siteInduction = SiteInduction(person_type = "worker", state = "pending")
        val enrolmentObject = Enrolment(id = 123,company = company,is_automatic_attendance_enabled = true, site = site, utc_last_signon_at = Calendar.getInstance().time,is_hidden = false, site_induction = siteInduction)
        val listOfEnrolments = mutableListOf<Enrolment>()
        listOfEnrolments.add(enrolmentObject)

        Mockito.`when`(connectionListUseCaseImpl.filterEnrolments(listOfEnrolments)).thenReturn(listOfEnrolments)

        val enrolmentsList = connectionListUseCaseImpl.filterEnrolments(listOfEnrolments)

        verify(connectionListUseCaseImpl).filterEnrolments(listOfEnrolments)
        Assert.assertEquals(1, enrolmentsList.size)
        Assert.assertEquals(true, enrolmentsList[0].is_automatic_attendance_enabled)
        Assert.assertEquals(false, enrolmentsList[0].is_hidden)
    }

    @Test
    fun `set time zone according with the api response`(){
        val company = Company(name = "SignOnSite",id = 12345)
        val site = Site(id = 1234,name = "SignOnSite",address = "1 Moore St", principal_company = company,timezone = "Australia/Sydney",is_site_induction_forms_enabled = true)
        val siteInduction = SiteInduction(person_type = "worker", state = "pending")
        val enrolmentObject = Enrolment(id = 123,company = company,is_automatic_attendance_enabled = true, site = site, utc_last_signon_at = Calendar.getInstance().time,is_hidden = false, site_induction = siteInduction)
        val date = "Thu Jan 30 13:40:17 GMT+11:00 2020"

        Mockito.`when`(connectionListUseCaseImpl.convertTimeZoneDate(enrolmentObject)).thenReturn(date)

        val dateString = connectionListUseCaseImpl.convertTimeZoneDate(enrolmentObject)

        verify(connectionListUseCaseImpl).convertTimeZoneDate(enrolmentObject)
        Assert.assertEquals(date, dateString)

    }


}