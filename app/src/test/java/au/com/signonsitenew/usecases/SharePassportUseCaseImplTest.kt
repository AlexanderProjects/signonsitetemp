package au.com.signonsitenew.usecases

import au.com.signonsitenew.domain.models.SharePassportRequest
import au.com.signonsitenew.domain.models.SharePassportResponse
import au.com.signonsitenew.domain.usecases.sharepassport.SharePassportUseCaseImpl
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock



class SharePassportUseCaseImplTest {

    private lateinit var sharePassportUseCaseImpl: SharePassportUseCaseImpl


    @Before
    fun init() {
        sharePassportUseCaseImpl = mock(SharePassportUseCaseImpl::class.java)
    }

    @Test
    fun `is valid email address test with a successful response`(){
        val email = "alexanderparrap@gmail.com"
        Mockito.`when`(sharePassportUseCaseImpl.isValidEmailAddress(email)).thenReturn(true)

        val isValidEmail = sharePassportUseCaseImpl.isValidEmailAddress(email)
        Assert.assertEquals(true, isValidEmail)
    }

    @Test
    fun `share passport with successful response`(){
        val listOfEmails = mutableListOf<String>()
        listOfEmails.add("alexanderparrap@gmail.com")
        val request = SharePassportRequest(listOfEmails)
        val response = SharePassportResponse(status = "successful")

        Mockito.`when`(sharePassportUseCaseImpl.sharePassport(request)).thenReturn(Single.just(response))

        val subscriber = sharePassportUseCaseImpl.sharePassport(request).test()
        subscriber.assertValue {
            it.status == "successful"
        }
    }

    @Test
    fun `build share request with successful response`(){
        val email = "alexanderparrap@gmail.com"
        val listOfEmails = mutableListOf<String>()
        listOfEmails.add("alexanderparrap@gmail.com")

        val request = SharePassportRequest(listOfEmails)
        Mockito.`when`(sharePassportUseCaseImpl.buildShareRequest(email)).thenReturn(request)

        Assert.assertEquals(request.email_addresses[0], email)
    }

}