package au.com.signonsitenew

import net.danlew.android.joda.JodaTimeAndroid
import org.robolectric.Shadows
import org.robolectric.Shadows.shadowOf


class SOSApplicationTest : SOSApplication() {
    override fun onCreate() {
        setTheme(R.style.AppTheme)
        val shadowApp = Shadows.shadowOf(this)
        shadowApp.grantPermissions("android.permission.INTERNET")
        shadowOf(getMainLooper()).idle()
        JodaTimeAndroid.init(this)
    }
}