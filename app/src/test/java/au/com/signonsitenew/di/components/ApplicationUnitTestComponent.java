package au.com.signonsitenew.di.components;

import javax.inject.Singleton;

import au.com.signonsitenew.di.modules.AnalyticsModule;
import au.com.signonsitenew.di.modules.UseCasesModule;
import au.com.signonsitenew.services.RestServiceUnitTest;
import au.com.signonsitenew.di.modules.ApplicationModule;
import au.com.signonsitenew.di.modules.NetModule;
import au.com.signonsitenew.usecases.ListOfConnectionsUseCaseTest;
import au.com.signonsitenew.usecases.PermitUseCaseTest;
import au.com.signonsitenew.usecases.SharePassportUseCaseImplTest;
import au.com.signonsitenew.usecases.UpdateEnrolmentUseCaseImplTest;
import dagger.Component;

@Singleton
@Component(modules = {NetModule.class,ApplicationModule.class, UseCasesModule.class, AnalyticsModule.class})
public interface ApplicationUnitTestComponent {
    void inject(RestServiceUnitTest restServiceUnitTest);
    void inject(ListOfConnectionsUseCaseTest listOfConnectionsUseCaseTest);
    void inject(UpdateEnrolmentUseCaseImplTest updateEnrolmentUseCaseTest);
    void inject(SharePassportUseCaseImplTest sharePassportUseCaseTest);
    void inject(PermitUseCaseTest permitUseCaseTest);
}
