package au.com.signonsitenew.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import com.segment.analytics.Analytics;
import au.com.signonsitenew.SOSApplication;
import au.com.signonsitenew.data.factory.datasources.net.RestService;
import au.com.signonsitenew.data.factory.DataFactory;
import au.com.signonsitenew.domain.repository.DataRepository;
import au.com.signonsitenew.realm.services.UserService;
import au.com.signonsitenew.utilities.SessionManager;
import io.realm.Realm;
import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;


public class  ApplicationUnitTestModule  extends ApplicationModule {


    @Override
    public RestService provideRestService(Retrofit retrofit, Retrofit retrofitWithNulls, Retrofit retrofitForMap) {
        return mock(RestService.class);
    }

    @Override
    public DataRepository provideDataRepository(DataFactory factory) {
        return mock(DataRepository.class);
    }

    @Override
    public Context provideContext(SOSApplication application) {
        return mock(Context.class);
    }

    @Override
    public SessionManager provideSessionManager(SOSApplication application) {
        return  mock(SessionManager.class);
    }

    @Override
    public UserService provideUserService() {
        return mock(UserService.class);
    }

}