package au.com.signonsitenew.di.modules;


import com.squareup.moshi.Moshi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

public class NetUnitTestModule extends NetModule {

    @Override
    public OkHttpClient provideOkHttpClient() {
        return mock(OkHttpClient.class);
    }

    @Override
    public Retrofit provideRetrofit(OkHttpClient client, Moshi moshi) {
        return mock(Retrofit.class);
    }
}
