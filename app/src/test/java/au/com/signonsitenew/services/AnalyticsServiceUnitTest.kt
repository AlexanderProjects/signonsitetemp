package au.com.signonsitenew.services

import au.com.signonsitenew.data.factory.datasources.analytics.AnalyticsService
import com.segment.analytics.Properties
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AnalyticsServiceUnitTest {

    @Mock
    lateinit var analyticsService: AnalyticsService

    @Test
    fun `send track event test`(){
        val prop = Properties()
        prop["test_value"] = "value"
        Mockito.doNothing().`when`(analyticsService).sendTrackEvent("test event",prop)
        analyticsService.sendTrackEvent("test event",prop)
        Mockito.verify(analyticsService,times(1))?.sendTrackEvent(ArgumentMatchers.anyString(), ArgumentMatchers.any())
    }

    @Test
    fun `send screen events test`(){
        val prop = Properties()
        prop["test_value"] = "value"
        Mockito.doNothing().`when`(analyticsService).sendScreenEvent("test event",prop)
        analyticsService.sendScreenEvent("test event",prop)
        Mockito.verify(analyticsService,times(1))?.sendScreenEvent(ArgumentMatchers.anyString(), ArgumentMatchers.any())
    }

    @Test
    fun `call identity test`(){
        Mockito.doNothing().`when`(analyticsService).callIdentity("123")
        analyticsService.callIdentity("123")
        Mockito.verify(analyticsService, times(1)).callIdentity(ArgumentMatchers.anyString())
    }

}